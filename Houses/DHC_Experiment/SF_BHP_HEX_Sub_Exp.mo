﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo.Houses.DHC_Experiment;
model SF_BHP_HEX_Sub_Exp "House with BHP and TES, simplified heating and cooling demand"
	GreenCity.Interfaces.Environment.EnvironmentConditions environmentConditions1 "Environment Conditions Connector" annotation(Placement(
		transformation(extent={{485,25},{505,45}}),
		iconTransformation(extent={{290,340},{310,360}})));
	GreenCity.Interfaces.Electrical.LV3Phase lV3Phase1 "Electrical Low-Voltage AC Three-Phase Connector" annotation(Placement(
		transformation(extent={{190,-345},{210,-325}}),
		iconTransformation(extent={{340,340},{360,360}})));
	Modelica.Blocks.Interfaces.BooleanOutput HeatingDemand "If true, heating demand" annotation(Placement(
		transformation(extent={{460,-180},{480,-160}}),
		iconTransformation(
			origin={396.7,149.7},
			extent={{-10,-9.699999999999999},{10,10.3}})));
	Modelica.Blocks.Interfaces.BooleanOutput CoolingDemand "If true cooling demand in the house" annotation(Placement(
		transformation(extent={{460,-200},{480,-180}}),
		iconTransformation(
			origin={396.7,200.3},
			extent={{10,-9.699999999999999},{-10,10.3}},
			rotation=180)));
	Modelica.Blocks.Interfaces.RealOutput qv_Set_Pro(quantity="Thermics.VolumeFlow") "Volume flow setpoint from prosumer to the hydraulic interface pumps" annotation(Placement(
		transformation(extent={{155,-245},{175,-225}}),
		iconTransformation(extent={{386.7,-10},{406.7,10}})));
	Modelica.Blocks.Interfaces.RealInput qv_In_Pro_Act(quantity="Thermics.VolumeFlow") "Actual flow rate in from the Hydraulic Interface pumps" annotation(Placement(
		transformation(extent={{-135,-345},{-95,-305}}),
		iconTransformation(extent={{416.7,-270},{376.7,-230}})));
	Storage.HeatStorageCombined WolfSPU2_800(
		VStorage=0.72,
		dStorage=0.79,
		QlossRate=24,
		LinearProfile=TSLinearProfile,
		TupInit(displayUnit="K")=TSTupInit,
		TlowInit(displayUnit="K")=TSTlowInit,
		TLayerVector(displayUnit="K")=TSTLayerVector,
		TMax(displayUnit="K")=Tmax_TS,
		alphaMedStatic=0.5,
		use1=true,
		iFlow1=10,
		iReturn1=7,
		use2=true,
		HE2=false,
		iFlow2=7,
		AHeatExchanger2=25,
		VHeatExchanger2=0.0165,
		use4=true,
		iFlow4=10,
		iReturn4=5,
		use5=false,
		use6=true,
		iFlow6=10) "Heat storage with variable temperature profile" annotation(Placement(transformation(extent={{245,-129},{280,-55}})));
	GreenCity.GreenBuilding.HeatingSystem.HeatingUnitFlowTemperature heatingUnitFlowTemperature1(qvMaxPump(displayUnit="l/min")=0.00025) "Flow temperature controller wiring for heating system" annotation(Placement(transformation(extent={{320,-75},{340,-55}})));
	GreenCity.Utilities.Electrical.Grid grid1(
		OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
		useA=true,
		useB=false,
		useC=false,
		useD=false,
		useE=true,
		useF=false) "Electrical power grid for connection of maximum six 3-phase AC components" annotation(Placement(transformation(extent={{180,-300},{220,-260}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap1 "Connection of 1-phase to 3-phase AC components" annotation(Placement(transformation(extent={{255,-285},{265,-275}})));
	Modelica.Blocks.Logical.Switch switch3 "Switch between two Real signals" annotation(Placement(transformation(extent={{-60,-80},{-40,-60}})));
	Modelica.Blocks.Sources.RealExpression HPin_StandardControl(y=HPModulation_StandardControl) "Set output signal to a time varying Real expression" annotation(Placement(transformation(extent={{-95,-60},{-75,-40}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow input_from_grid "Specifies a Thermal Volume Flow Connector" annotation(Placement(transformation(
		origin={100,-155},
		extent={{-10,-10},{10,10}},
		rotation=90)));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow output_to_grid annotation(Placement(transformation(
		origin={130,-155},
		extent={{-10,10},{10,-10}},
		rotation=-90)));
	Distribution.SwitchPort Switch_BHP_Port "Change between inlet ports of thermal storage for different temperature levels of BHP" annotation(Placement(transformation(extent={{180,-130},{200,-100}})));
	Modelica.Blocks.Logical.Switch switch1 annotation(Placement(transformation(extent={{-60,-150},{-40,-130}})));
	Modelica.Blocks.Sources.RealExpression AUXin_StandardControl(y=AUXinStandardControl) annotation(Placement(transformation(extent={{-95,-130},{-75,-110}})));
	Modelica.Blocks.Logical.LogicalSwitch logicalSwitch1 annotation(Placement(transformation(extent={{-60,60},{-40,80}})));
	Modelica.Blocks.Sources.BooleanExpression SwitchSignal_StandardControl(y=HP3WVinStandardControl) "Set output signal to a time varying Boolean expression" annotation(Placement(transformation(extent={{-115,85},{-95,105}})));
	GreenCity.Utilities.Thermal.MeasureThermal TI_DHW "Temperature indicator for DHW consumption" annotation(Placement(transformation(extent={{365,-105},{375,-115}})));
	Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold1(threshold=TurRatHP) annotation(Placement(transformation(extent={{-20,-65},{-10,-55}})));
	Distribution.SwitchPort Switch_Heating_Cooling "This switch dictates if cold water from the HEX or the BHP is coming in." annotation(Placement(transformation(extent={{135,-85},{155,-55}})));
	GreenCity.Utilities.Thermal.MeasureThermal VI_HEX1 "Volume flow indicator for HEX consumption" annotation(Placement(transformation(extent={{370,-85},{360,-75}})));
	GreenCity.Utilities.Thermal.MeasureThermal VI_HEXDC_R "Volume flow and temperature indicator for HEX consumption (return)" annotation(Placement(transformation(extent={{220,-30},{210,-20}})));
	GreenCity.Utilities.Thermal.MeasureThermal VI_HEXDC_F "Volume flow and temperature indicator for HEX consumption (flow)" annotation(Placement(transformation(extent={{195,0},{205,-10}})));
	Modelica.Blocks.Sources.BooleanExpression HPMode(y=HPMode_StandardControl) "Heat pump mode (true heating, false cooling), depends on HPMode_StandardControl which is based on a when function." annotation(Placement(transformation(extent={{-100,10},{-80,30}})));
	Modelica.Blocks.Sources.BooleanExpression HEXDCOn(y=DirectHEX_StandardControl) "Turning direct cooling heat exchanger on or off depending on if cooling is requested" annotation(Placement(transformation(extent={{-90,-195},{-70,-175}})));
	Modelica.Blocks.Logical.Not not1 "Logical 'not': y = not u" annotation(Placement(transformation(extent={{100,-5},{110,5}})));
	Modelica.Blocks.Sources.RealExpression TRefHEXDC(y=TFlowCooling) "Set output signal to a time varying Real expression" annotation(Placement(transformation(extent={{10,-240},{30,-220}})));
	Modelica.Blocks.Logical.LogicalSwitch logicalSwitch2 annotation(Placement(transformation(extent={{-55,-215},{-35,-195}})));
	Modelica.Blocks.Logical.LogicalSwitch logicalSwitch3 annotation(Placement(transformation(extent={{-60,-10},{-40,10}})));
	Modelica.Blocks.Interfaces.RealInput qv_RefCooling "'input Real' as connector" annotation(Placement(
		transformation(extent={{490,-55},{450,-15}}),
		iconTransformation(
			origin={-350,350},
			extent={{-20,-20},{20,20}},
			rotation=-90)));
	Modelica.Blocks.Interfaces.RealInput qv_RefHeating "'input Real' as connector" annotation(Placement(
		transformation(extent={{490,-115},{450,-75}}),
		iconTransformation(
			origin={-300,350},
			extent={{-20,-20},{20,20}},
			rotation=-90)));
	Modelica.Blocks.Interfaces.RealInput T_RefHeating "'input Real' as connector" annotation(Placement(
		transformation(extent={{490,-85},{450,-45}}),
		iconTransformation(
			origin={-250,350},
			extent={{-20,-20},{20,20}},
			rotation=-90)));
	GreenCity.Interfaces.Thermal.VolumeFlowIn Flow_Cooling "Thermal Volume Flow Input Connector" annotation(Placement(
		transformation(extent={{385,-10},{395,0}}),
		iconTransformation(extent={{-10,340},{10,360}})));
	GreenCity.Interfaces.Thermal.VolumeFlowOut Return_Cooling "Thermal Volume Flow Output Connector" annotation(Placement(
		transformation(extent={{385,-30},{395,-20}}),
		iconTransformation(extent={{40,340},{60,360}})));
	GreenCity.Interfaces.Thermal.VolumeFlowIn Flow_Heating "Thermal Volume Flow Input Connector" annotation(Placement(
		transformation(extent={{385,-65},{395,-55}}),
		iconTransformation(extent={{90,340},{110,360}})));
	GreenCity.Interfaces.Thermal.VolumeFlowOut Return_Heating "Thermal Volume Flow Output Connector" annotation(Placement(
		transformation(extent={{385,-85},{395,-75}}),
		iconTransformation(extent={{140,340},{160,360}})));
	GreenCity.Interfaces.Thermal.VolumeFlowIn Flow_DHW "Thermal Volume Flow Input Connector" annotation(Placement(
		transformation(extent={{385,-115},{395,-105}}),
		iconTransformation(extent={{190,340},{210,360}})));
	GreenCity.Interfaces.Thermal.VolumeFlowOut Return_DHW "Thermal Volume Flow Output Connector" annotation(Placement(
		transformation(extent={{385,-135},{395,-125}}),
		iconTransformation(extent={{240,340},{260,360}})));
	Modelica.Blocks.Interfaces.BooleanInput SpaceHeating "'input Boolean' as connector" annotation(Placement(
		transformation(extent={{480,-140},{455,-115}}),
		iconTransformation(
			origin={-200,350},
			extent={{-20,-20},{20,20}},
			rotation=-90)));
	Modelica.Blocks.Interfaces.BooleanInput SpaceCooling "'input Boolean' as connector" annotation(Placement(
		transformation(extent={{480,-160},{455,-135}}),
		iconTransformation(
			origin={-150,350},
			extent={{-20,-20},{20,20}},
			rotation=-90)));
	Generator.DigitalTwins.RatiothermWPGridwithHX_GC_Exp_Controller ratiothermWPGridwithHX_GC_Exp_Controller1 annotation(Placement(transformation(extent={{45,-100},{90,-55}})));
	Modelica.Blocks.Interfaces.RealInput T_Cold_Pipe(quantity="Thermics.VolumeFlow") "Actual flow rate in from the Hydraulic Interface pumps" annotation(Placement(
		transformation(extent={{-135,-380},{-95,-340}}),
		iconTransformation(extent={{416.7,-170},{376.7,-130}})));
	Modelica.Blocks.Interfaces.BooleanOutput HEXDC_On "If true cooling demand in the house" annotation(Placement(
		transformation(extent={{460,-220},{480,-200}}),
		iconTransformation(
			origin={396.7,250.3},
			extent={{10,-9.699999999999999},{-10,10.3}},
			rotation=180)));
	Modelica.Blocks.Tables.CombiTable1Ds Load(
		tableOnFile=true,
		tableName="Consumption",
		fileName=FilePower,
		columns={2,3,4}) annotation(Placement(transformation(extent={{-5,25},{-25,45}})));
	parameter String FilePower=CoSES_ProHMo.LibraryPath + "Data\\5GDHC_Experiment\\Cst_BHP.txt" "File with consumption timeseries" annotation(
		HideResult=false,
		Dialog(tab="Standard Control"));
	parameter Real time_HeatingCoolingSeason(quantity="Basics.Time")=0 "Time taken to switch/stop a heating or cooling season." annotation(Dialog(
		group="Consumption",
		tab="Standard Control"));
	Real HPon_StandardModControl(quantity="Basics.RelMagnitude") "For the Mod Factor, testing that it is not in a case where it should be off" annotation(Dialog(
		group="Booster Heat Pump",
		tab="Standard Control",
		visible=false));
	Modelica.Blocks.Interfaces.BooleanInput StandardControl_BHP "If true, standard control will be used to control the BHP" annotation(
		Placement(
			transformation(extent={{-135,-90},{-95,-50}}),
			iconTransformation(
				origin={-400,50},
				extent={{-20,-20},{20,20}})),
		Dialog(
			group="Booster Heat Pump",
			tab="Standard Control",
			visible=false));
	parameter Boolean ControlType_BHP=true "If standard control: True on/off control for BHP, False control with modulation factor." annotation(Dialog(
		group="Booster Heat Pump",
		tab="Standard Control"));
	protected
		Real HPin "Input Signal of BHP" annotation(
			HideResult=false,
			Dialog(
				group="Booster Heat Pump",
				tab="Standard Control"));
	public
		parameter Real HPRampDownTime(quantity="Basics.Time")=75 "Time for the HP to ramp down after switching off" annotation(
			HideResult=false,
			Dialog(
				group="Booster Heat Pump",
				tab="Standard Control",
				visible=false));
		Boolean HPinRampDown "If true HP is in ramping down mode" annotation(Dialog(
			group="Booster Heat Pump",
			tab="Standard Control",
			visible=false));
		parameter Real TStartHPhigh(quantity="Basics.Temp")=328.15 "Minimum Temperature, at the top, at which the heat pump turns on" annotation(
			HideResult=false,
			Dialog(
				group="Booster Heat Pump",
				tab="Standard Control"));
		parameter Real TStopHPhigh(quantity="Basics.Temp")=333.15 "Temperature at the top, at which the heat pump stops" annotation(Dialog(
			group="Booster Heat Pump",
			tab="Standard Control"));
		parameter Real TStartHPlow(quantity="Basics.Temp")=313.15 "Minimum Temperature, at the bottom, at which the heat pump turns on" annotation(
			HideResult=false,
			Dialog(
				group="Booster Heat Pump",
				tab="Standard Control"));
		parameter Real TStopHPlow(quantity="Basics.Temp")=318.15 "Temperature at the bottom, at which the heat pump stops" annotation(Dialog(
			group="Booster Heat Pump",
			tab="Standard Control"));
		Real HPModulation_StandardControl(quantity="Basics.RelMagnitude") "Standard control value of the heat pump modulation" annotation(Dialog(
			group="Booster Heat Pump",
			tab="Standard Control",
			visible=false));
		Boolean HPMode_StandardControl "Switching heat pump from heating to cooling mode dpending on the demand of the house" annotation(Dialog(
			group="Booster Heat Pump",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardControl_BHP_Aux "If true, standard control will be used to control the BHP auxiliary heater (electric)" annotation(
			Placement(
				transformation(extent={{-135,-160},{-95,-120}}),
				iconTransformation(
					origin={-400,150},
					extent={{-20,-20},{20,20}})),
			Dialog(
				group="Auxillary Heater",
				tab="Standard Control",
				visible=false));
		parameter Real TStopAUXhigh(quantity="Basics.Temp")=328.15 "Minimum Temperature, at the bottom, at which the auxiliary heater turns on" annotation(Dialog(
			group="Auxillary Heater",
			tab="Standard Control"));
		parameter Real TStartAUXhigh(quantity="Basics.Temp")=323.15 "Minimum Temperature, at the top, at which the auxiliary heater turns on" annotation(Dialog(
			group="Auxillary Heater",
			tab="Standard Control"));
		parameter Real TStartAUXlow(quantity="Basics.Temp")=308.15 "Minimum Temperature, at the top, at which the auxiliary heater stops" annotation(Dialog(
			group="Auxillary Heater",
			tab="Standard Control"));
		parameter Real TStopAUXlow(quantity="Basics.Temp")=313.15 "Minimum Temperature, at the bottom, at which the auxiliary heater stops" annotation(Dialog(
			group="Auxillary Heater",
			tab="Standard Control"));
		Real AUXinStandardControl(quantity="Basics.RelMagnitude") "Standard control value of the auxiliary heater modulation" annotation(Dialog(
			group="Auxillary Heater",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardControl_HEX "If true, standard control will be used to control the decision between using the direct heat exchanger or heat pump" annotation(
			Placement(
				transformation(extent={{-135,-225},{-95,-185}}),
				iconTransformation(
					origin={-400,200},
					extent={{-20,-20},{20,20}})),
			Dialog(
				group="Auxillary Heater",
				tab="Standard Control",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardControl_BHPSwitch "Standard Control for the 3-way valve to switch the connection to the thermal storage" annotation(
			Placement(
				transformation(extent={{-135,50},{-95,90}}),
				iconTransformation(
					origin={-400,250},
					extent={{-20,-20},{20,20}})),
			Dialog(
				group="Heat Pump 3-way valve",
				tab="Standard Control",
				visible=false));
		Real deltaTLowDHW(quantity="Thermics.TempDiff")=2 "Required temperature difference between upper layer and DHW set temperature" annotation(Dialog(
			group="Heat Pump 3-way valve",
			tab="Standard Control"));
		Boolean HP3WVinStandardControl "Switches between the inlet of the HP into the TS (true: upper storage connection)" annotation(Dialog(
			group="Heat Pump 3-way valve",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardControl_BHPMode "Standard Control for the booster heat pump mode" annotation(
			Placement(
				transformation(extent={{-135,-20},{-95,20}}),
				iconTransformation(
					origin={-400,100},
					extent={{-20,-20},{20,20}})),
			Dialog(
				group="Heat Pump 3-way valve",
				tab="Standard Control",
				visible=false));
		parameter Boolean ConstVSource=true "Set to true of it's a fixed flowrate, variable DT control system" annotation(Dialog(
			group="Pumps",
			tab="Standard Control"));
		parameter Boolean ConstVHouse(quantity="Basics.Unitless")=true "Set to true of it's a fixed flowrate on the supply side of the BHP, variable DT control system" annotation(Dialog(
			group="Pumps",
			tab="Standard Control"));
		parameter Real VPumpSourceConst(quantity="Thermics.VolumeFlow")=0.0001666666666666667 "If ConstVSource is true, this is the value of the flowrate going through the BHP" annotation(Dialog(
			group="Pumps",
			tab="Standard Control"));
		parameter Real VPumpSourceMin(quantity="Basics.RelMagnitude")=0.2 "When in operation (cooling required and HEX is on), the minimum flowrate setting for the grid pumps as a percentage of total rating" annotation(Dialog(
			group="Pumps",
			tab="Standard Control"));
		parameter Real VPumpSourceMax(quantity="Thermics.VolumeFlow")=0.0003333333333333334 "When in operation (cooling required and HEX is on), the minimum flowrate setting for the grid pumps as a percentage of total rating" annotation(Dialog(
			group="Pumps",
			tab="Standard Control"));
		parameter Real VPumpHouseConst(quantity="Thermics.VolumeFlow")=0.0003333333333333334 "If ConstVHouse is true, this is the value of the flowrate going through the BHP on the supply side" annotation(Dialog(
			group="Pumps",
			tab="Standard Control"));
		parameter Real VPumpHouseMin(quantity="Basics.RelMagnitude")=0.2 "When in operation (cooling required and HEX is on), the minimum flowrate setting for the grid pumps as a percentage of total rating" annotation(Dialog(
			group="Pumps",
			tab="Standard Control"));
		parameter Real VPumpHouseMax(quantity="Thermics.VolumeFlow")=0.0003333333333333334 "When in operation (cooling required and HEX is on), the minimum flowrate setting for the grid pumps as a percentage of total rating" annotation(Dialog(
			group="Pumps",
			tab="Standard Control"));
		parameter Real TRefGridHot(quantity="Basics.Temp")=293.15 "Reference supply temperature of the hot pipe (grid side) for Grid DeltaT control regime" annotation(Dialog(
			group="Pumps",
			tab="Standard Control"));
		Boolean DirectHEX_StandardControl "Switching the direct cooling heat exchanger on or off" annotation(Dialog(
			group="HEX Direct Cooling",
			tab="Standard Control",
			visible=false));
		Real time_off_HP(quantity="Basics.Time") "Used for turning the HEXDC back on after the ramp down time of the BHP" annotation(
			HideResult=true,
			Dialog(
				group="HEX Direct Cooling",
				tab="Standard Control",
				visible=false));
		Real time_on_HEX(quantity="Basics.Time") "Used for not turning off the HEXDC back off instanteneously " annotation(Dialog(
			group="HEX Direct Cooling",
			tab="Standard Control"));
		Boolean HEX_On_MinOperation "Whether we are in the minimum operation time for HEX" annotation(Dialog(
			group="HEX Direct Cooling",
			tab="Standard Control",
			visible=false));
		parameter Real HEXMinOnTime(quantity="Basics.Time")=100 "Minimum time the HEX is on for (at least 100s to account for the ramp down time of the HP)" annotation(Dialog(
			group="HEX Direct Cooling",
			tab="Standard Control"));
		Modelica.Blocks.Interfaces.RealInput HPIn_Modulation(quantity="Basics.Power") "Water source heat pump modulation in percent" annotation(
			Placement(
				transformation(extent={{-135,-120},{-95,-80}}),
				iconTransformation(extent={{-420,-270},{-380,-230}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput T_Out_Pro(quantity="Basics.Temp") "Temperature after the booster heat pump, sent to the hydraulic interface" annotation(
			Placement(
				transformation(extent={{155,-220},{175,-200}}),
				iconTransformation(extent={{386.7,-60},{406.7,-40}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput T_In_Pro(quantity="Basics.Temp") "Temperature of fluid in the prosumer, from the hydraulic interface" annotation(
			Placement(
				transformation(extent={{-135,-315},{-95,-275}}),
				iconTransformation(extent={{416.7,-120},{376.7,-80}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput HP_Aux_In_Modulation(quantity="Basics.Power") "Auxiliary heater of water source heat pump modulation in percent" annotation(
			Placement(
				transformation(extent={{-135,-190},{-95,-150}}),
				iconTransformation(extent={{-420,-220},{-380,-180}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput BHPSwitch "Switches between the inlet of the HP into the TS (true: upper storage connection)" annotation(
			Placement(
				transformation(extent={{-135,25},{-95,65}}),
				iconTransformation(
					origin={-400,-150},
					extent={{-20,-20},{20,20}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput T_Out_Pro_Act(quantity="Basics.Temp") "Actual temperature out of the prosumer back the the grid (taking into account thermal intertia)." annotation(
			Placement(
				transformation(extent={{-135,-285},{-95,-245}}),
				iconTransformation(extent={{416.7,-220},{376.7,-180}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput DirectHEX "Switches between the direct heat exchanger or booster heat pump (true: direct heat exchanger)" annotation(
			Placement(
				transformation(extent={{-135,-255},{-95,-215}}),
				iconTransformation(
					origin={-400,-100},
					extent={{-20,-20},{20,20}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput BHPMode "Booster heat pump mode: heating (true) or cooling (false)" annotation(
			Placement(
				transformation(extent={{-135,-50},{-95,-10}}),
				iconTransformation(
					origin={-400,-50},
					extent={{-20,-20},{20,20}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput T_Flow_Cooling(quantity="Basics.Temp") "Temperature after the booster heat pump, sent to the hydraulic interface" annotation(
			Placement(
				transformation(extent={{460,0},{480,20}}),
				iconTransformation(extent={{386.7,40},{406.7,60}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		parameter Boolean UseHP(start=true) "Use heat pump" annotation(
			HideResult=false,
			Dialog(
				group="BHP",
				tab="Heat Generator Parameters"));
		parameter Real PAuxMax(quantity="Basics.Power")=9000 "Maximum power of the auxiliary heater" annotation(Dialog(
			group="BHP",
			tab="Heat Generator Parameters"));
		parameter Real PHeatNom(quantity="Basics.Power")=16400 "Nominal heat output at W10/W55 (not maximum heat output)." annotation(Dialog(
			group="BHP",
			tab="Heat Generator Parameters"));
		parameter Real PCoolingNom(quantity="Basics.Power")=6010 "Nominal cooling output at W35/W15 (not maximum cooling output)." annotation(Dialog(
			group="BHP",
			tab="Heat Generator Parameters"));
		parameter Real COPNom=3.33 "Nominal COP at W10/W55." annotation(Dialog(
			group="BHP",
			tab="Heat Generator Parameters"));
		parameter Real EERNom=3.33 "Nominal EER at W35/W15." annotation(Dialog(
			group="BHP",
			tab="Heat Generator Parameters"));
		parameter Real TurRatHP(quantity="Basics.RelMagnitude")=0.25 "Turndown ratio of the HP indicating the minimum modulation of the unit. Below that, no power output" annotation(Dialog(
			group="BHP",
			tab="Heat Generator Parameters"));
		parameter Real k_HX=0.001 "Gain of controller" annotation(Dialog(
			group="BHP",
			tab="Heat Generator Parameters"));
		parameter Real Ti_HX=1 "Time constant of Integrator block" annotation(Dialog(
			group="BHP",
			tab="Heat Generator Parameters"));
		parameter Real Tmax_TS(quantity="Basics.Temp")=353.15 "Maximum temperature within the thermal storage" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Boolean TSLinearProfile=true "If true, the temperature profile at simulation begin within the storage is linear, if false the profile is defined by a temperature vector" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTupInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=311.15 if TSLinearProfile "Temperature of upmost heat storage layer at simulation begin" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTlowInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=303.15 if TSLinearProfile "Temperature of lowmost heat storage layer at simulation begin" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTLayerVector[20](quantity="Basics.Temp")={313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15} if not TSLinearProfile "Vector of temperature profile of the layers at simulation begin, element 1 is at lowest layer" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real T0_TS(quantity="Basics.Temp")=313.15 "Temperature level to calculate the stored energy (e.g. return temperature of consumption)" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Integer nPeople(quantity="Basics.Unitless")=6 "Number of people living in the building" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Integer nFloors=2 "nFloors" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Integer nApartments=1 "nApartments" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Real ALH(
			quantity="Geometry.Area",
			displayUnit="m²")=300 "Heated (living) area (e.g. 50m² per person)" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Boolean UseStandardHeatNorm=false "If true, use standard area-specific heating power, else define it manually" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real QHeatNormLivingArea(quantity="Thermics.HeatFlowSurf")=15 if not UseStandardHeatNorm "Area-specific heating power - modern radiators: 14 - 15 W/m²; space heating: 15 W/m²" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real n=1.1 "Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TFlowHeatNorm(quantity="Basics.Temp")=313.15 "Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 - 45°C" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TReturnHeatNorm(quantity="Basics.Temp")=303.15 "Normal return temperature - radiator: 45 - 65°C; floor heating: 28 - 35°C" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TRef(quantity="Basics.Temp")=294.15 "Reference indoor temperature" annotation(
			HideResult=false,
			Dialog(
				group="Heating System",
				tab="Consumption Parameters"));
		parameter Real qvMaxLivingZone(quantity="Thermics.VolumeFlow")=0.00025 "Maximumg flow rate in Living Zone" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TLiving_Init(quantity="Basics.Temp")=294.15 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TRoof_Init(quantity="Basics.Temp")=274.65 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TCellar_Init(quantity="Basics.Temp")=280.15 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Boolean ActivateCooling=true "If true, cooling system is activated" annotation(
			HideResult=false,
			Dialog(
				group="Cooling System",
				tab="Consumption Parameters",
				visible=false));
		parameter Real TRefCooling(quantity="Basics.Temp")=296.15 "Reference indoor temperature (has to be higher than the reference heating temperature!)" annotation(
			HideResult=false,
			Dialog(
				group="Cooling System",
				tab="Consumption Parameters"));
		parameter Real TFlowCooling(quantity="Basics.Temp")=289.15 "Supply temperature for cooling" annotation(Dialog(
			group="Cooling System",
			tab="Consumption Parameters"));
		parameter Boolean UseUnixTimeIndividual=false "If true, use unix time for individual presence and electric consumption" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean UseIndividualPresence=true "If the presence is used, individual presence data has to be provided, else standart presence is used" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String PresenceFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF1_WholeYear.txt" if UseIndividualPresence "File with presence timeseries (presence in %; 0% - no one is at home; 100% - everyone is at home)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean UseIndividualElecConsumption=true "If individual electricity consumption is used, individual consumption data ha to be provided, else standart load profiles are used" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String ElConsumptionFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF1_WholeYear.txt" if UseIndividualElecConsumption "File with electric consumption time series (consumption in kW)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String ElConsumptionTable="Pel" if UseIndividualElecConsumption "Table with electric consumption time series (consumption in W)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Real YearlyElecConsumption_kWh=6500 if UseIndividualElecConsumption "YearlyElecConsumption_kWh" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Real ElFactor=YearlyElecConsumption_kWh/5175 if UseIndividualElecConsumption "ElFactor" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean ActivateNightTimeReduction=false "If true, night time reduction is activated, else temperature is constant" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real NightTimeReductionStart(
			quantity="Basics.Time",
			displayUnit="h")=82800 if ActivateNightTimeReduction "NightTimeReductionStart" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real NightTimeReductionEnd(
			quantity="Basics.Time",
			displayUnit="h")=25200 if ActivateNightTimeReduction "NightTimeReductionEnd" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real Tnight(quantity="Basics.Temp")=291.15 if ActivateNightTimeReduction "Temperature at night" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Boolean VariableTemperatureProfile=false "If true, presence will be used to define the temperature (if less people are at home, less rooms are heated and the average temperature will decrease)" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real TMin(quantity="Basics.Temp")=292.15 if VariableTemperatureProfile "Minimum temperature, when noone is at home (TRefSet = TMin + (TRef - TMin) * Presence(t))" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Boolean UseUnixTimeDHW=false "If true, use unix time for DHW consumption" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Boolean WeeklyData=false "If true: DHW consumption data repeats weekly" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter String File=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF1_WholeYear.txt" "DHW Data File" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter String Table="V_DHW" "DHW Table Name" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real V_DHWperDay_l=nPeople*50 "V_DHWperDay_l" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real DHWfactor=V_DHWperDay_l/300 "Factor, with which the DHW consumption gets multiplied" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		Real HP_S_TM_VL(quantity="Basics.Temp") "BHP flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results BHP",
			visible=false));
		Real HP_S_TM_RL(quantity="Basics.Temp") "BHP return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results BHP",
			visible=false));
		Real HP_S_FW_HC(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow BHP" annotation(Dialog(
			group="Volume Flow",
			tab="Results BHP",
			visible=false));
		Real ModFactor_SetHeating(quantity="Basics.RelMagnitude") "Set heating BHP modulation factor" annotation(Dialog(
			group="Power",
			tab="Results BHP",
			visible=false));
		Real ModFactor(quantity="Basics.RelMagnitude") "Modulation factor for the power setpoint of the HP. It is a function of temperature in the TES" annotation(Dialog(
			group="Power",
			tab="Results BHP",
			visible=false));
		Real P_ExpHeating(quantity="Basics.Power") "Heating power output of BHP" annotation(Dialog(
			group="Power",
			tab="Results BHP",
			visible=false));
		Real SetModFactor(quantity="Basics.RelMagnitude") "Modulation factor for the power setpoint of the BHP if setpoint is used (no variation)" annotation(Dialog(
			group="Power",
			tab="Results BHP",
			visible=false));
		Real VarModFactor(quantity="Basics.RelMagnitude") "Modulation factor for the power setpoint of the BHP if variation is used" annotation(Dialog(
			group="Power",
			tab="Results BHP",
			visible=false));
		Real HP_P_heat_is(quantity="Basics.Power") "Heat output power of BHP" annotation(Dialog(
			group="Power",
			tab="Results BHP",
			visible=false));
		Real HP_P_heat_Tot(quantity="Basics.Power") "Total heat output power of the heat pump" annotation(Dialog(
			group="Power",
			tab="Results BHP",
			visible=false));
		Real HP_P_heat_Aux(quantity="Basics.Power") "Heat output power of the auxiliary heater" annotation(Dialog(
			group="Power",
			tab="Results BHP",
			visible=false));
		Real HP_P_elec_is "Total electricity demand of heat pump" annotation(Dialog(
			group="Power",
			tab="Results BHP",
			visible=false));
		Real HP_E_heat_HP(quantity="Basics.Energy") "Heat output of the heat pump " annotation(Dialog(
			group="Energy",
			tab="Results BHP",
			visible=false));
		Real HP_E_heat_Aux(quantity="Basics.Energy") "Heat output power of the auxiliary heater" annotation(Dialog(
			group="Energy",
			tab="Results BHP",
			visible=false));
		Real HP_E_heat_Tot(quantity="Basics.Energy") "Total heat output power of the heat pump" annotation(Dialog(
			group="Energy",
			tab="Results BHP",
			visible=false));
		Real HP_E_elec_consumed(quantity="Basics.Energy") "Consumed electric energy of heat pump" annotation(Dialog(
			group="Energy",
			tab="Results BHP",
			visible=false));
		Real HP_COP(quantity="Basics.RelMagnitude") "COP (Thermal efficiency)" annotation(Dialog(
			group="Efficiency And Operaion",
			tab="Results BHP",
			visible=false));
		Integer countHP(start=0) "Counts number of turn ons for the HP" annotation(Dialog(
			group="Efficiency And Operaion",
			tab="Results BHP",
			visible=false));
		Real TS_P_heat_to_TES(quantity="Basics.Power") "Power transfered to storage" annotation(Dialog(
			group="Power",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_HW_VL(quantity="Basics.Temp") "Flow temperature to fresh water station" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_HW_RL(quantity="Basics.Temp") "Return temperature to fresh water station" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_1(quantity="Basics.Temp") "Thermal storage temperature 1" annotation(Dialog(
			group="Storage Temperatures",
			__esi_groupCollapsed=true,
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_2(quantity="Basics.Temp") "Thermal storage temperature 2" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_3(quantity="Basics.Temp") "Thermal storage temperature 3" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_4(quantity="Basics.Temp") "Thermal storage temperature 4" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_5(quantity="Basics.Temp") "Thermal storage temperature 5" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_6(quantity="Basics.Temp") "Thermal storage temperature 6" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_7(quantity="Basics.Temp") "Thermal storage temperature 7" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_8(quantity="Basics.Temp") "Thermal storage temperature 8" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_9(quantity="Basics.Temp") "Thermal storage temperature 9" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_10(quantity="Basics.Temp") "Thermal storage temperature 10" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_E_Storage_BT(
			quantity="Basics.Energy",
			displayUnit="kWh") "Energy in thermal storage" annotation(Dialog(
			group="Energy",
			tab="Results TS",
			visible=false));
		Real TS_S_FW_HC "Volume flow from storage" annotation(Dialog(
			group="Group",
			tab="Results TS",
			visible=false));
		Real TS_SOC_BT(quantity="Basics.RelMagnitude") "State of charge of the thermal storage" annotation(Dialog(
			group="State of charge",
			tab="Results TS",
			visible=false));
		Boolean CoolingSeason "If true, standard control is set to heating season (definition: heating was required within the last 24 hours)" annotation(Dialog(
			tab="Consumption Results",
			visible=false));
		Real time_cooling_off(quantity="Basics.Time") "time since heating was used the last time" annotation(Dialog(
			tab="Consumption Results",
			visible=false));
		Boolean HeatingSeason "If true, standard control is set to heating season (definition: heating was required within the last 24 hours)" annotation(Dialog(
			tab="Consumption Results",
			visible=false));
		Real time_heating_off(quantity="Basics.Time") "time since heating was used the last time" annotation(Dialog(
			tab="Consumption Results",
			visible=false));
		Real HS_S_TM_VL_bM(quantity="Basics.Temp") "Heat Sink flow temperature before mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Results HCS",
			visible=false));
		Real HS_S_TM_VL_aM(quantity="Basics.Temp") "Heat Sink flow temperature after mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Results HCS",
			visible=false));
		Real HS_S_TM_RL(quantity="Basics.Temp") "Heat sink return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HCS",
			visible=false));
		Real CS_S_TM_VL(quantity="Basics.Temp") "Cold Sink flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HCS",
			visible=false));
		Real CS_S_TM_RL(quantity="Basics.Temp") "Cold sink return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HCS",
			visible=false));
		Real HS_S_TM_HW_VL(quantity="Basics.Temp") "Heat sink flow temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Results HCS",
			visible=false));
		Real HS_S_TM_HW_RL(quantity="Basics.Temp") "Heat sink return temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Results HCS",
			visible=false));
		Real HS_S_FW_HC_aM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow after mixing unit for heat sink" annotation(Dialog(
			group="Volume Flow",
			tab="Results HCS",
			visible=false));
		Real HS_S_FW_HC_bM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow before mixing unit for heat sink" annotation(Dialog(
			group="Volume Flow",
			tab="Results HCS",
			visible=false));
		Real CS_S_FW_HC(quantity="Thermics.VolumeFlow") "Volume flow for cold sink" annotation(Dialog(
			group="Volume Flow",
			tab="Results HCS",
			visible=false));
		Real HS_S_FW_HW_VL(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow domestic hot water consumption" annotation(Dialog(
			group="Volume Flow",
			tab="Results HCS",
			visible=false));
		Real HS_P_DemHeatHC_is(quantity="Basics.Power") "Heating power" annotation(Dialog(
			group="Power",
			tab="Results HCS",
			visible=false));
		Real CS_P_DemColdHC_is(quantity="Basics.Power") "Parameter" annotation(Dialog(
			group="Power",
			tab="Results HCS",
			visible=false));
		Real HS_P_DemHeatHW_is(quantity="Basics.Power") "Domestic hot water power" annotation(Dialog(
			group="Power",
			tab="Results HCS",
			visible=false));
		Real HS_E_DemHeatHC_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heating Energy" annotation(Dialog(
			group="Energy",
			tab="Results HCS",
			visible=false));
		Real CS_E_DemColdHC_consumed(quantity="Basics.Energy") "Parameter" annotation(Dialog(
			group="Energy",
			tab="Results HCS",
			visible=false));
		Real HS_E_DemHeatHW_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Domestic hot water energy" annotation(Dialog(
			group="Energy",
			tab="Results HCS",
			visible=false));
		Modelica.Blocks.Interfaces.RealInput UnixTime(quantity="Basics.Time") "Unix time stamp" annotation(
			Placement(
				transformation(extent={{30,15},{70,55}}),
				iconTransformation(
					origin={-50,350},
					extent={{-20,-20},{20,20}},
					rotation=-90)),
			Dialog(
				tab="Results",
				visible=false));
	equation
		// Heating or cooling season check
		// when there is existing heating/cooling to ensure we do not continuously switch between the two.
		when not SpaceHeating then
		    time_heating_off = time;  // Store the time when heating is turned off
		end when;
		
		// Check if it has been more than 86400 seconds (1 day) since heating was turned off
		// and that we are not in the starting phase
		if (time - time_heating_off) > time_HeatingCoolingSeason or not (time_heating_off > time.start) then
		    HeatingSeason = false;  // If more than a day has passed, it's not the heating season
		else
		    HeatingSeason = true;   // Otherwise, it's still the heating season
		end if;
		
		// Similar logic for cooling season
		when not SpaceCooling then
		    time_cooling_off = time;  // Store the time when cooling is turned off
		end when;
		
		if (time - time_cooling_off) > time_HeatingCoolingSeason or not (time_cooling_off > time.start) then
		    CoolingSeason = false;  // If more than a day has passed, it's not the cooling season
		else
		    CoolingSeason = true;   // Otherwise, it's still the cooling season
		end if;
		
		//Input from experiment for power modulation
		ModFactor_SetHeating = max(0, Load.y[1]);
		
		//3WV position for BHP
		if ModFactor_SetHeating>0.5 then
			HP3WVinStandardControl = true;//Put here an if function based on a table from the txt file
		else
			HP3WVinStandardControl = false;
		end if;
		
		//HEXDC
		// Control the use of direct heat exchanger for cooling based on temperature and HP mode
		when (T_Cold_Pipe < TRefHEXDC.y + 2) and (SpaceCooling and not HP3WVinStandardControl) then
		    time_on_HEX = time;  
		end when;
		
		// Account for the ramp down period of the HP for the transition back to cooling
		if (time - time_on_HEX) > HEXMinOnTime or not (time_on_HEX > time.start)then 
			HEX_On_MinOperation = false;
		else
			HEX_On_MinOperation = true;
		end if;
		
		when (T_Cold_Pipe < TRefHEXDC.y + 2) and (SpaceCooling and not HP3WVinStandardControl) then //SpaceCooling or CoolingSeason and
			// Use direct heat exchanger for cooling, when grid temperature is low enough
			// In this case, the limit is the cooling temperature for the house (usually 16C) plus 2 degrees.
			DirectHEX_StandardControl=true;
		elsewhen HP3WVinStandardControl or SpaceHeating then //or (SpaceCooling and T_Cold_Pipe > TRefHEXDC.y - 2)
			// Do not use direct heat exchanger for heating or when grid temperature is too high for direct cooling
			// In the second case, the HP in cooling mode will be used
			DirectHEX_StandardControl=false;
		elsewhen SpaceCooling and (not HEX_On_MinOperation) and (T_Cold_Pipe > TRefHEXDC.y + 2 or T_Cold_Pipe < TRefHEXDC.y - 5) then
			// Do not use direct heat exchanger when grid temperature is too low (13C) or too high (18C) for direct cooling 
			// The HP in cooling mode will be used.
			DirectHEX_StandardControl=false;
		end when;
		
		// Transition period for DHW in cooling
		// Store the time when we exit the DHW mode during cooling
		when not (HP3WVinStandardControl and (SpaceCooling or CoolingSeason)) then
		    time_off_HP = time;  
		end when;
		
		// Account for the ramp down period of the HP for the transition back to cooling
		if (time - time_off_HP) > HPRampDownTime or not (time_off_HP > time.start)then 
			HPinRampDown = false;
		else
			HPinRampDown = true;
		end if;
		
		// Standard Control Heat Pump mode
		// Heating mode if Heating Season or top of thermal storage is too cold
		// Cooling if Cooling Season and DHW in the thermal storage is hot enough
		when (HP3WVinStandardControl or HeatingSeason or SpaceHeating) and (not HPinRampDown) then  
			HPMode_StandardControl = true;//Heating Mode
		elsewhen not HP3WVinStandardControl and (CoolingSeason or SpaceCooling) and (not HPinRampDown) then
			HPMode_StandardControl = false;//Cooling Mode
		end when;
		
		// Demand signal to the grid (hydraulic interface)
		// Determine if there is heating or cooling demand for the prosumer, as boolean arguments. 
		// Ensure that HP is not in ramp down.
		
		when HP3WVinStandardControl or (HPModulation_StandardControl>0.0001 and not qv_RefCooling>0) and not HPinRampDown then
			// either DHW or HP on but not for cooling (no demand for cooling) and not HPin RampDown
			HeatingDemand = true;
		elsewhen not HP3WVinStandardControl and (not HPModulation_StandardControl>0.0001 or qv_RefCooling>0) and not HPinRampDown then
			HeatingDemand = false;
		end when;
		
		when not HeatingDemand and qv_RefCooling>0 and not HPinRampDown then
			CoolingDemand = true;
		elsewhen HeatingDemand or not qv_RefCooling>0 then
			CoolingDemand = false;
		end when;
		
		
		AUXinStandardControl = 0;//that is always true
		
		/*
		We dictate the control
		BHP
		
		// Standard Control Switch_BHP_Port
		when TS_S_TM_BT_9 < TStartHPhigh then
		    // When the temperature on the 9th level of the TES (T_TS9) is below the starting setpoint for the HP (TStartHigh):
		    // open the 3WV so that it charges the top of the TES
		    HP3WVinStandardControl = true;
		elsewhen ((TS_S_TM_BT_7 > TStopHPhigh) or (TS_S_TM_BT_4 < TStartHPlow)) and (TS_S_TM_BT_9 > (TStopHPhigh-0.1)) then
		    // When the T_TS9 > StopHPhigh and either T_TS7 is below TStopHPhigh or T_TS4 is less than TStartHPlow: 
		    // close the 3WV so that it charges the middle of the TES
		    // This gives priority to charging the top of the TES to not continuously start and stop
		    HP3WVinStandardControl = false;
		end when;
		
		And of the AUX
		
		// Control the auxiliary heating  to supplement the HP operation
		when ((TS_S_TM_BT_9 < TStartAUXhigh) or ((TS_S_TM_BT_4 < TStartAUXlow) and HeatingSeason)) then
			// Switch auxiliary heater on when:
			// T_TS9 < TStartHigh
			// T_TS4 < TStartLow during heating season
			AUXinStandardControl = 1;
		elsewhen not HP3WVinStandardControl and ((TS_S_TM_BT_2 > TStopAUXlow) or not HeatingSeason) then
			// Switch auxiliary heater off when:
			// T_TS2 > TStopHigh and T_TS7 > TStopHighLow
			// T_TS7 > TStopHigh if no heating season
			// or StartCondition is not valid (Minimum Control reset, when standard control is deactivated)
			AUXinStandardControl = 0;
		end when;
		
		*/
	equation
		// Unit Modulation for Standard Control
		// BHP under setpoint control
		
		//3WV position for BHP
		if ModFactor_SetHeating>0.3 then
			HPon_StandardModControl = 1;//Put here an if function based on a table from the txt file
		else
			HPon_StandardModControl = 0;
		end if;
		
		ModFactor=ModFactor_SetHeating;////from txt file. No need for VarModFactor,SetModFactor
		
		// Minimum power modulation
		// When there is a modualtion factor larger than zero but the ModFactor is lower than the minimum power operation (TurRatHP) of the BHP
		// use the minimum power setpoint
		if ModFactor>0.001 then
			HPModulation_StandardControl = max(TurRatHP, ModFactor);
		else 
			HPModulation_StandardControl = 0;
		end if;
		
		/*
		when ControlType_BHP and(HP3WVinStandardControl)then
			// High modulation factor when HP3WV is open - heating mode
			SetModFactor = 0.6;
		elsewhen ControlType_BHP and(not HP3WVinStandardControl and (not (SpaceCooling or CoolingSeason) and TS_S_TM_BT_4 < TStartHPlow ))then
				// Medium modulation factor during heating for space heating
			SetModFactor = 0.4;
		elsewhen ControlType_BHP and (not HP3WVinStandardControl and (SpaceCooling or CoolingSeason) and qv_RefCooling>0.00001667) then
				// Modulation factor during cooling season if no 
			SetModFactor = 0.7;
		elsewhen ControlType_BHP and (not HP3WVinStandardControl and (not (SpaceCooling or CoolingSeason) and (TS_S_TM_BT_2 > TStopHPlow or TS_S_TM_BT_9 > (TStopHPhigh+0.1)))) then
			// Modulation factor is 0 when not in any active mode
			SetModFactor = 0;
		end when;
		
		if ControlType_BHP and (SpaceCooling) and not qv_RefCooling>0.00001667 and not HP3WVinStandardControl then
			// Modulation factor is 0 when not in any active mode
			HPon_StandardModControl = 0;
		else  
			HPon_StandardModControl = 1;
		end if;
		
		//BHP under varrying modulation factor control
		//BHP Modulation with modulation factor
		if not ControlType_BHP and HP3WVinStandardControl then
			// Top section of TES modulation factor based on the temperature difference between T_TS7 and setpoints
			VarModFactor = max(0,min(1,(1-(TS_S_TM_BT_7 - TStartHPhigh)/(TStopHPhigh-TStartHPhigh))));
		elseif not ControlType_BHP and (HeatingSeason or SpaceHeating) then
			// Low section of TES modulation factor based on the temperature difference between T_TS4 and setpoints
			VarModFactor = (max(0,min(1,(1-(TS_S_TM_BT_4 - TStartHPlow)/(TStopHPlow-TStartHPlow)))));
		elseif not ControlType_BHP and (CoolingSeason or SpaceCooling) then
			// Cooling mode modulation factor based on ratio of required flowrate and max flowrate. 
			VarModFactor = qv_RefCooling / ratiothermWPGridwithHX_GC_Exp_Controller1.VPumpHouseMax;
		else
			// Modulation factor is 0 when not in any active mode 
			VarModFactor = 0;
		end if;	
		
		//Final modulation factor is the maximum of the two and has a check with the cases where it should be switched off
		ModFactor=min(HPon_StandardModControl,max(VarModFactor,SetModFactor)); 
		
		*/
	equation
		// Heat Pump
		HP_S_TM_VL = ratiothermWPGridwithHX_GC_Exp_Controller1.TFlow;
		HP_S_TM_RL = ratiothermWPGridwithHX_GC_Exp_Controller1.TReturn;
		HP_S_FW_HC = ratiothermWPGridwithHX_GC_Exp_Controller1.ratiothermWPGrid1.VSource_set;
		HP_P_heat_is = ratiothermWPGridwithHX_GC_Exp_Controller1.QHeat_HP;
		HP_P_heat_Aux = ratiothermWPGridwithHX_GC_Exp_Controller1.QHeat_Aux;
		HP_P_heat_Tot = ratiothermWPGridwithHX_GC_Exp_Controller1.QHeat_HP + ratiothermWPGridwithHX_GC_Exp_Controller1.QHeat_Aux;
		HP_P_elec_is = ratiothermWPGridwithHX_GC_Exp_Controller1.PEl_HP + ratiothermWPGridwithHX_GC_Exp_Controller1.PEl_Aux;
		HP_E_heat_HP = ratiothermWPGridwithHX_GC_Exp_Controller1.EHeat_HP;
		HP_E_heat_Aux = ratiothermWPGridwithHX_GC_Exp_Controller1.EHeat_Aux;
		HP_E_heat_Tot = ratiothermWPGridwithHX_GC_Exp_Controller1.EHeat_HP + ratiothermWPGridwithHX_GC_Exp_Controller1.EHeat_Aux;
		HP_E_elec_consumed = ratiothermWPGridwithHX_GC_Exp_Controller1.EEl_HP + ratiothermWPGridwithHX_GC_Exp_Controller1.EEl_Aux;
		HP_COP = ratiothermWPGridwithHX_GC_Exp_Controller1.COP;
		P_ExpHeating=Load.y[2]*1000;
		//Check with variables complete for HP
		
		//Thermal Storage
		TS_S_TM_HC_HW_VL = WolfSPU2_800.FlowOut6.T;
		TS_S_TM_HC_HW_RL = WolfSPU2_800.ReturnIn6.T;
		TS_S_TM_BT_1 = WolfSPU2_800.TStorage[1];
		TS_S_TM_BT_2 = WolfSPU2_800.TStorage[2];
		TS_S_TM_BT_3 = WolfSPU2_800.TStorage[3];
		TS_S_TM_BT_4 = WolfSPU2_800.TStorage[4];
		TS_S_TM_BT_5 = WolfSPU2_800.TStorage[5];
		TS_S_TM_BT_6 = WolfSPU2_800.TStorage[6];
		TS_S_TM_BT_7 = WolfSPU2_800.TStorage[7];
		TS_S_TM_BT_8 = WolfSPU2_800.TStorage[8];
		TS_S_TM_BT_9 = WolfSPU2_800.TStorage[9];
		TS_S_TM_BT_10 = WolfSPU2_800.TStorage[10];
		TS_S_FW_HC = WolfSPU2_800.FlowIn1.qv + WolfSPU2_800.FlowIn2.qv;
		TS_E_Storage_BT = WolfSPU2_800.cpMed*WolfSPU2_800.rhoMed*WolfSPU2_800.VStorage*((WolfSPU2_800.TLayer[1]+WolfSPU2_800.TLayer[2]+WolfSPU2_800.TLayer[3]+WolfSPU2_800.TLayer[4]+WolfSPU2_800.TLayer[5]+WolfSPU2_800.TLayer[6]+WolfSPU2_800.TLayer[7]+WolfSPU2_800.TLayer[8]+WolfSPU2_800.TLayer[9]+WolfSPU2_800.TLayer[10])/10-T0_TS);
		TS_SOC_BT = TS_E_Storage_BT/(WolfSPU2_800.cpMed*WolfSPU2_800.rhoMed*WolfSPU2_800.VStorage*(Tmax_TS - T0_TS));
		//Check with variables complete for TS but
		// not above: TS_P_heat_to_TES
		TS_P_heat_to_TES = 0;
		
		// Heat Sink (Heating)
		HS_S_TM_VL_bM = heatingUnitFlowTemperature1.FlowSupply.T;
		HS_S_TM_VL_aM = heatingUnitFlowTemperature1.FlowSink.T;
		HS_S_TM_RL = heatingUnitFlowTemperature1.ReturnSupply.T;
		HS_S_FW_HC_aM = - heatingUnitFlowTemperature1.FlowSupply.qv;
		HS_S_FW_HC_bM = heatingUnitFlowTemperature1.FlowSink.qv;
		//HS_P_DemHeatHC_is = buildingLoads1.P_Heating;
		//HS_E_DemHeatHC_consumed = buildingLoads1.E_Heating;
		
		//Cold Sink (Cooling)
		CS_S_TM_VL = VI_HEXDC_F.TMedium;
		CS_S_TM_RL = VI_HEXDC_R.TMedium;
		CS_S_FW_HC = VI_HEXDC_F.qvMedium;
		//CS_P_DemColdHC_is = buildingLoads1.P_Cooling;
		//CS_E_DemColdHC_consumed = buildingLoads1.E_Cooling;
		
		// Heat Sink (Domestic Hot Water)
		HS_S_TM_HW_VL =Flow_DHW.T;
		HS_S_TM_HW_RL = Return_DHW.T;
		HS_S_FW_HW_VL = Return_DHW.qv;
		//HS_P_DemHeatHW_is = buildingLoads1.P_DHW;
		//HS_E_DemHeatHW_consumed = buildingLoads1.E_DHW;
		
		//Turn ons
		//Measure the number of turns of the BHP 
		//CHANGE so that is depended on HPOn rather than the modulation factor since the two are not entirely tied.
		when ModFactor<0.0001 then
			countHP = 1 + pre(countHP);
		end when;
	initial equation
		//Initial Equations Section
		
		// Check if heating or cooling season
		time_heating_off = time.start;
		time_cooling_off = time.start;
		time_off_HP = time.start; 
		time_on_HEX = time.start; 
		
		// Standard Control HP mode
		if  HP3WVinStandardControl or HeatingSeason or SpaceHeating then  
			HPMode_StandardControl = true;//Heating Mode
		else
			HPMode_StandardControl = false;//Cooling Mode
		end if;
		
		// Standard Control Direct Cooling HEX  
		if (T_Cold_Pipe < TRefHEXDC.y + 2) and (SpaceCooling or CoolingSeason and not HP3WVinStandardControl) then
			// use direct heat exchanger for cooling, when grid temperature is low enough
			DirectHEX_StandardControl=true;
		else
			// do not use direct heat exchanger for heating or when grid temperature is too low for direct cooling
			DirectHEX_StandardControl=false;
		end if;
		
		
		//Check demands for hydraulic interface
		if HP3WVinStandardControl or HeatingSeason or SpaceHeating then 
			HeatingDemand = true;
			CoolingDemand = false;
		elseif not HP3WVinStandardControl and (SpaceCooling or CoolingSeason) then
			HeatingDemand = false;
			CoolingDemand = true;
		else 
			HeatingDemand = false;
			CoolingDemand = false;
		end if;
		
		/*
		// Standard Control Switch_BHP_Port
		if TS_S_TM_BT_9 < TStartHPhigh then 
			// T_TS9 < TStartHigh
			HP3WVinStandardControl = true;
		else
			// T_TS7 > TStopHigh
			// or StartCondition is not valid (Minimum Control reset, when standard control is deactivated)
			HP3WVinStandardControl = false;
		end if;
		
		// BHP modulation for the set factor
		if ControlType_BHP and HP3WVinStandardControl then
			// High modulation factor when HP3WV is open - heating mode
			SetModFactor = 0.6;
		elseif ControlType_BHP and TS_S_TM_BT_4 < TStartHPlow and not (TS_S_TM_BT_2 > TStopHPlow) then
			// Medium modulation factor during heating season
			SetModFactor = 0.4;
		elseif ControlType_BHP and (SpaceCooling or CoolingSeason) then
			// Medium modulation factor during cooling season
			SetModFactor = 0.6;
		else
			// Modulation factor is 0 when not in any active mode
			SetModFactor = 0;
		end if;
		
		// Standard Control Aux  
		if ((TS_S_TM_BT_9 < TStartAUXhigh) or ((TS_S_TM_BT_4 < TStartAUXlow) and HeatingSeason)) then
			// T_TS9 < TStartHigh
			// T_TS4 < TStartLow during heating season
			AUXinStandardControl = 1;
		else
			// T_TS2 > TStopHigh and // T_TS7 > TStopHighLow
			// T_TS7 > TStopHigh if no heating season
			// or StartCondition is not valid (Minimum Control reset, when standard control is deactivated)
			AUXinStandardControl = 0;
		end if;
		
		*/
	equation
		connect(phaseTap1.Grid1,heatingUnitFlowTemperature1.Grid1) annotation(Line(
			points={{265,-280},{270,-280},{322.7,-280},{322.7,-80},{322.7,-75}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid1.LVGridE,phaseTap1.Grid3) annotation(Line(
			points={{220,-280},{225,-280},{250,-280},{255,-280}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid1.LVMastGrid,lV3Phase1) annotation(
			Line(
				points={{200,-300},{200,-305},{200,-335}},
				color={247,148,29},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(WolfSPU2_800.FlowOut4,heatingUnitFlowTemperature1.FlowSupply) annotation(Line(
			points={{280,-60},{285,-60},{315,-60},{320,-60}},
			color={190,30,45},
			thickness=0.0625));
		connect(WolfSPU2_800.ReturnIn4,heatingUnitFlowTemperature1.ReturnSupply) annotation(Line(
			points={{280,-70},{285,-70},{315,-70},{320,-70}},
			color={190,30,45},
			thickness=0.015625));
		connect(SwitchSignal_StandardControl.y,logicalSwitch1.u1) annotation(Line(
			points={{-94,95},{-89,95},{-67,95},{-67,78},{-62,78}},
			color={255,0,255},
			thickness=0.0625));
		connect(AUXin_StandardControl.y,switch1.u1) annotation(Line(
			points={{-74,-120},{-69,-120},{-67,-120},{-67,-132},{-62,-132}},
			color={0,0,127},
			thickness=0.0625));
		connect(HPIn_Modulation,switch3.u3) annotation(Line(
			points={{-115,-100},{-110,-100},{-67,-100},{-67,-78},{-62,-78}},
			color={0,0,127},
			thickness=0.0625));
		connect(HP_Aux_In_Modulation,switch1.u3) annotation(Line(
			points={{-115,-170},{-110,-170},{-67,-170},{-67,-148},{-62,-148}},
			color={0,0,127},
			thickness=0.0625));
		connect(HPin_StandardControl.y,switch3.u1) annotation(Line(
			points={{-74,-50},{-69,-50},{-67,-50},{-67,-62},{-62,-62}},
			color={0,0,127},
			thickness=0.0625));
		connect(WolfSPU2_800.FlowOut6,TI_DHW.PipeIn) annotation(Line(
			points={{280,-110.3},{285,-110.3},{360,-110.3},{360,-110},{365,-110}},
			color={190,30,45}));
		connect(logicalSwitch1.y,Switch_BHP_Port.SwitchPort) annotation(Line(
			points={{-39,70},{-34,70},{190,70},{190,-95},{190,-100}},
			color={255,0,255},
			thickness=0.0625));
		connect(Switch_Heating_Cooling.Out2,Switch_BHP_Port.In) annotation(Line(
			points={{154.7,-75},{159.7,-75},{175,-75},{175,-105},{180,-105}},
			color={190,30,45}));
		connect(Switch_BHP_Port.Out1,WolfSPU2_800.FlowIn1) annotation(
			Line(
				points={{200,-105},{205,-105},{210,-105},{210,-60},{245,-60}},
				color={190,30,45}),
			__esi_AutoRoute=false);
		connect(Switch_BHP_Port.In1,WolfSPU2_800.ReturnOut1) annotation(
			Line(
				points={{199.6666564941406,-110},{204.7,-110},{220,-110},{220,-70},{245,-70}},
				color={190,30,45},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(Switch_BHP_Port.Out2,WolfSPU2_800.FlowIn2) annotation(
			Line(
				points={{199.6666564941406,-120},{204.7,-120},{235,-120},{235,-85},{245,-85.33332824707031}},
				color={190,30,45}),
			__esi_AutoRoute=false);
		connect(Switch_BHP_Port.In2,WolfSPU2_800.ReturnOut2) annotation(Line(
			points={{199.7,-125},{204.7,-125},{240,-125},{240,-95.3},{245,-95.3}},
			color={190,30,45},
			thickness=0.0625));
		connect(VI_HEXDC_F.PipeIn,Switch_Heating_Cooling.Out1) annotation(Line(
			points={{195,-5},{190,-5},{160,-5},{160,-60},{155,-60}},
			color={190,30,45},
			thickness=0.0625));
		connect(Switch_BHP_Port.Out,Switch_Heating_Cooling.In2) annotation(Line(
			points={{180,-125},{175,-125},{159.7,-125},{159.7,-80},{154.7,-80}},
			color={190,30,45}));
		connect(output_to_grid.TMedium,T_Out_Pro) annotation(Line(
			points={{135,-165},{135,-170},{135,-210},{160,-210},{165,-210}},
			color={0,0,127},
			thickness=0.0625));
		connect(T_In_Pro,input_from_grid.TMedium) annotation(Line(
			points={{-115,-295},{-110,-295},{95,-295},{95,-170},{95,-165}},
			color={0,0,127},
			thickness=0.0625));
		connect(qv_In_Pro_Act,input_from_grid.qvMedium) annotation(Line(
			points={{-115,-325},{-110,-325},{105,-325},{105,-170},{105,-165}},
			color={0,0,127},
			thickness=0.0625));
		connect(not1.y,Switch_Heating_Cooling.SwitchPort) annotation(Line(
			points={{110.3,0},{115.3,0},{145,0},{145,-50},{145,-55}},
			color={255,0,255},
			thickness=0.0625));
		connect(switch3.y,greaterEqualThreshold1.u) annotation(Line(
			points={{-39,-70},{-34,-70},{-26,-70},{-26,-60},{-21,-60}},
			color={0,0,127},
			thickness=0.0625));
		connect(heatingUnitFlowTemperature1.ReturnSink,VI_HEX1.PipeOut) annotation(Line(
			points={{340,-70},{345,-70},{355,-70},{355,-80},{360,-80}},
			color={190,30,45},
			thickness=0.0625));
		connect(BHPSwitch,logicalSwitch1.u3) annotation(Line(
			points={{-115,45},{-110,45},{-67,45},{-67,62},{-62,62}},
			color={255,0,255},
			thickness=0.0625));
		connect(StandardControl_BHPSwitch,logicalSwitch1.u2) annotation(Line(
			points={{-115,70},{-110,70},{-67,70},{-62,70}},
			color={255,0,255},
			thickness=0.0625));
		connect(StandardControl_BHP,switch3.u2) annotation(Line(
			points={{-115,-70},{-110,-70},{-67,-70},{-62,-70}},
			color={255,0,255},
			thickness=0.0625));
		connect(StandardControl_BHP_Aux,switch1.u2) annotation(Line(
			points={{-115,-140},{-110,-140},{-67,-140},{-62,-140}},
			color={255,0,255},
			thickness=0.0625));
		connect(DirectHEX,logicalSwitch2.u3) annotation(Line(
			points={{-115,-235},{-110,-235},{-62,-235},{-62,-213},{-57,-213}},
			color={255,0,255},
			thickness=0.0625));
		connect(logicalSwitch2.u2,StandardControl_HEX) annotation(Line(
			points={{-57,-205},{-62,-205},{-110,-205},{-115,-205}},
			color={255,0,255},
			thickness=0.0625));
		connect(logicalSwitch2.u1,HEXDCOn.y) annotation(Line(
			points={{-57,-197},{-62,-197},{-64,-197},{-64,-185},{-69,-185}},
			color={255,0,255},
			thickness=0.0625));
		connect(BHPMode,logicalSwitch3.u3) annotation(Line(
			points={{-115,-30},{-110,-30},{-67,-30},{-67,-8},{-62,-8}},
			color={255,0,255},
			thickness=0.0625));
		connect(StandardControl_BHPMode,logicalSwitch3.u2) annotation(Line(
			points={{-115,0},{-110,0},{-67,0},{-62,0}},
			color={255,0,255},
			thickness=0.0625));
		connect(logicalSwitch3.y,not1.u) annotation(Line(
			points={{-39,0},{-34,0},{94,0},{99,0}},
			color={255,0,255},
			thickness=0.0625));
		connect(HPMode.y,logicalSwitch3.u1) annotation(Line(
			points={{-79,20},{-74,20},{-67,20},{-67,8},{-62,8}},
			color={255,0,255},
			thickness=0.0625));
		connect(Switch_Heating_Cooling.In1,VI_HEXDC_R.PipeOut) annotation(Line(
			points={{154.7,-65},{159.7,-65},{205,-65},{205,-25},{210,-25}},
			color={190,30,45},
			thickness=0.0625));
		connect(VI_HEXDC_F.PipeOut,Flow_Cooling) annotation(Line(
			points={{205,-5},{210,-5},{385,-5},{390,-5}},
			color={190,30,45}));
		connect(VI_HEXDC_R.PipeIn,Return_Cooling) annotation(Line(
			points={{220,-25},{225,-25},{385,-25},{390,-25}},
			color={190,30,45},
			thickness=0.0625));
		connect(heatingUnitFlowTemperature1.FlowSink,Flow_Heating) annotation(Line(
			points={{340,-60},{345,-60},{385,-60},{390,-60}},
			color={190,30,45}));
		connect(TI_DHW.PipeOut,Flow_DHW) annotation(Line(
			points={{375,-110},{380,-110},{385,-110},{390,-110}},
			color={190,30,45}));
		connect(WolfSPU2_800.ReturnIn6,Return_DHW) annotation(
			Line(
				points={{280,-120.6666717529297},{285,-120.7},{290,-120.7},{290,-130},{390,-130}},
				color={190,30,45},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(VI_HEX1.PipeIn,Return_Heating) annotation(Line(
			points={{370,-80},{375,-80},{385,-80},{390,-80}},
			color={190,30,45},
			thickness=0.0625));
		connect(ratiothermWPGridwithHX_GC_Exp_Controller1.Flow_Water,Switch_Heating_Cooling.In) annotation(Line(
			points={{89.7,-60},{94.7,-60},{130,-60},{135,-60}},
			color={190,30,45}));
		connect(Switch_Heating_Cooling.Out,ratiothermWPGridwithHX_GC_Exp_Controller1.Return_Water) annotation(Line(
			points={{135,-80},{130,-80},{94.7,-80},{94.7,-70},{89.7,-70}},
			color={190,30,45}));
		connect(ratiothermWPGridwithHX_GC_Exp_Controller1.Grid_Out,output_to_grid.Pipe) annotation(Line(
			points={{89.7,-85},{94.7,-85},{130,-85},{130,-140},{130,-145}},
			color={190,30,45}));
		connect(ratiothermWPGridwithHX_GC_Exp_Controller1.Grid_In,input_from_grid.Pipe) annotation(Line(
			points={{89.7,-95},{94.7,-95},{100,-95},{100,-140},{100,-145}},
			color={190,30,45},
			thickness=0.0625));
		connect(ratiothermWPGridwithHX_GC_Exp_Controller1.VGrid_set,qv_Set_Pro) annotation(
			Line(
				points={{89.66667175292969,-80},{89.7,-90},{115,-90},{115,-235},{165,-235}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(grid1.LVGridA,ratiothermWPGridwithHX_GC_Exp_Controller1.lV3Phase) annotation(Line(
			points={{180,-265},{175,-265},{80,-265},{80,-104.7},{80,-99.7}},
			color={247,148,29},
			thickness=0.0625));
		connect(TRefHEXDC.y,ratiothermWPGridwithHX_GC_Exp_Controller1.TRefHeatExchanger) annotation(
			Line(
				points={{31,-230},{36,-230},{35,-230},{35,-85},{45,-85}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(T_Out_Pro_Act,ratiothermWPGridwithHX_GC_Exp_Controller1.TSourceOutReal) annotation(Line(
			points={{-115,-265},{-110,-265},{40,-265},{40,-90},{45,-90}},
			color={0,0,127},
			thickness=0.0625));
		connect(logicalSwitch2.y,ratiothermWPGridwithHX_GC_Exp_Controller1.DirectHeatExchanger) annotation(
			Line(
				points={{-34,-205},{-29,-205},{30,-205},{30,-80},{45,-80}},
				color={255,0,255},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(switch1.y,ratiothermWPGridwithHX_GC_Exp_Controller1.HPAuxModulation) annotation(
			Line(
				points={{-39,-140},{-34,-140},{25,-140},{25,-75},{45,-75}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(ratiothermWPGridwithHX_GC_Exp_Controller1.HPModulation,switch3.y) annotation(Line(
			points={{45,-70},{40,-70},{-34,-70},{-39,-70}},
			color={0,0,127},
			thickness=0.0625));
		connect(greaterEqualThreshold1.y,ratiothermWPGridwithHX_GC_Exp_Controller1.HPOn) annotation(Line(
			points={{-9.699999999999999,-60},{-4.7,-60},{40,-60},{45,-60}},
			color={255,0,255},
			thickness=0.0625));
		connect(environmentConditions1,ratiothermWPGridwithHX_GC_Exp_Controller1.environmentConditions) annotation(Line(
			points={{495,35},{490,35},{80,35},{80,-50},{80,-55}},
			color={192,192,192},
			thickness=0.0625));
		connect(ratiothermWPGridwithHX_GC_Exp_Controller1.VPumpHXhouse,qv_RefCooling) annotation(Line(
			points={{70,-55},{70,-50},{70,-35},{465,-35},{470,-35}},
			color={0,0,127},
			thickness=0.0625));
		connect(logicalSwitch3.y,ratiothermWPGridwithHX_GC_Exp_Controller1.HPMode) annotation(Line(
			points={{-39,0},{-34,0},{40,0},{40,-65},{45,-65}},
			color={255,0,255},
			thickness=0.0625));
		connect(T_RefHeating,heatingUnitFlowTemperature1.TRef) annotation(
			Line(
				points={{470,-65},{450,-65},{450,-45},{327.7,-45},{327.6666564941406,-55}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(qv_RefHeating,heatingUnitFlowTemperature1.qvRef) annotation(
			Line(
				points={{470,-95},{445,-95},{445,-50},{335,-50},{335,-55}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(logicalSwitch2.y,HEXDC_On) annotation(Line(
			points={{-34,-205},{-29,-205},{465,-205},{465,-210},{470,-210}},
			color={255,0,255},
			thickness=0.0625));
		connect(VI_HEXDC_F.TMedium,T_Flow_Cooling) annotation(Line(
			points={{200,0},{200,5},{200,10},{465,10},{470,10}},
			color={0,0,127},
			thickness=0.0625));
		connect(Load.u,UnixTime) annotation(Line(
			points={{-3,35},{2,35},{45,35},{50,35}},
			color={0,0,127},
			thickness=0.0625));
	annotation(
		__esi_neoTower2(
			Return(viewinfo[0](
				tabGroupAlignment=589834,
				typename="ModelInfo")),
			Flow(viewinfo[0](
				tabGroupAlignment=589834,
				typename="ModelInfo")),
			cHP_DZ1(
				ReturnCHP(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				FlowCHP(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				FromReturn(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
				ToFlow(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
			measureThermal1(
				PipeOut(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				PipeIn(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				FromPipe(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
				ToPipe(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
			measureThermal2(
				PipeOut(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				PipeIn(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				FromPipe(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
				ToPipe(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))))),
		__esi_simpleHeatingCoolingBuilding1(
			PipeIn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeOut(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			cellar(
				ToReturn(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromFlow(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				HeatFlow(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				HeatReturn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
			roof(
				ToReturn(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromFlow(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				HeatFlow(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				HeatReturn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
			distributorCellar(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			merge_Cellar(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			MT_Flow(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			MT_Return(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			distributorLivingZoneRoof(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergeLivingZoneRoof(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			heatingAndCoolingSystem1(
				Flow(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				Return(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				measureThermal2(
					PipeOut(
						viewinfo[0](
							tabGroupAlignment=589834,
							typename="ModelInfo"),
						viewinfo[0](
							tabGroupAlignment=589834,
							typename="ModelInfo")),
					PipeIn(
						viewinfo[0](
							tabGroupAlignment=589834,
							typename="ModelInfo"),
						viewinfo[0](
							tabGroupAlignment=589834,
							typename="ModelInfo")),
					FromPipe(Pipe(
						viewinfo[0](
							tabGroupAlignment=589834,
							typename="ModelInfo"),
						viewinfo[0](
							tabGroupAlignment=589834,
							typename="ModelInfo"))),
					ToPipe(Pipe(
						viewinfo[0](
							tabGroupAlignment=589834,
							typename="ModelInfo"),
						viewinfo[0](
							tabGroupAlignment=589834,
							typename="ModelInfo")))),
				ToReturn(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromFlow(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))))),
		__esi_protocols(transient={
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						Protocol(var=time),
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						Protocol(var=statAnaRefVar)}),
		__esi_solverOptions(
			solver="CVODE",
			typename="ExternalCVODEOptionData"),
		statAnaRefVar(__esi_flags=2),
		__esi_solver(
			bEffJac=false,
			bSparseMat=false,
			bSplitCodeGen=false,
			typename="CVODE"),
		Icon(
			coordinateSystem(extent={{-400,-350},{400,350}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAT8AAAD3CAIAAADkCMSRAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAWJQAAFiUBSVIk8AAAAB50RVh0QXV0aG9yAE9yZXN0aXMgQW5nZWxpZGlzIChQR1Ipb28eZQAA
ACB0RVh0Q3JlYXRpb25UaW1lADIwMjM6MDY6MTQgMTA6MzQ6MzH0mtOaAACj9klEQVR4Xu1dBZgd
RdYd3GVxWHQX1xgEdwkQiLvrJJNJJk58NO4hCXEl7u4Z1+ev3fvZSAwIDrs7/6muSe8Q5CdLBiaT
d776+qtXXV1y6566t1pexZRHUVPwr3/96z//+c+PP/7473//Gz8Rp+l2BBkAGo+iBiDK3hoCMNZm
KfDdd9999tlniHz//fc0BRm++eYbGgfDaSSKcxpR9tYcgKg2Vym+/vprGvniiy9++OEHO04jUZzr
iLK35oA6zABsrE3jr776ikYAmNzK9jmKcx1R9tYo0GUtKArSgszffvstfp44cYIS27bAtgsdxTmN
KHtrCEBLSlGseDMyMsLhMOIHDx6kfjL4bFtm5Iyue2sGouytUTh+/PikSZOuvfbaBx98sGvXrldc
ccX777/PcRxOff7555S0p62Nozh3EWVvzQE8ZDD2wgsvjLFw2WWX0fj999+/atUqmqfyMjiKcx1R
9p6TgPdrP7mFUcVRluXHH3+c8vaCCy6oVavW0KFDEb/00ktxvPzyy2fNmoVs9g1nejmOdG0Mfzv6
KPicQ5S95xjg99r3nE6ePEmfCe3Zs+fqq6+GpaXGtlu3bvn5+aqqLl68+Morr7z11lsJp2Ni+vTp
g8xYANscpreg6a0s+nw4inMIUfaeewB7KetgMHEcNmzYLbfcAnKCwODqRx99VFZWJgiC2+0GgQ8e
PFinTh3KXqB+/fr0hhY4bN/HAkpLS3GM3os+txBlbzUF+Ekp+nPYTi985g4dOsArBi2vueaaBx98
cNGiRcePHwdvPR6P3+83TVMURUTi4uIoe5H5pptu2rZtG0qA3Qb/7Vqib3Gcc4iyt5ri5+ylKbZ5
BCfr1auHZS1WuXCYsdAtKCiAyYW95XlekqRAIIC4oig+nw/HxMRESmDgiiuuSElJoeXQdS9FdOl7
biHK3mqKn1MXjq5tdQ8cOHDVVVdVcDEmBhYYxhaM1TQNR6/XW1JSghSn0ynLcjAYRKLD4Vi9evXf
/vY35L/oootw7NWrl104XfqeVmkU1RxR9p4bAK9+/PFHcAymcvTo0bfddtv1118PBt5+++2pqamh
UIhhmOLiYlAURKUEhu1FOsuyIDBsMogNI5yZmfnKK6+AvdTffuGFF3BVRR1RnGuIsvfcAAwvqPv9
99/36NEDrKOoU6fOsmXLYGPhIYOfcI+xysWRWmCkgNLgMEiLOJgMZxsZkLl58+a4/OKLL8bxnnvu
2b59O6rABBH1nM8tRNn7FwOEsd9bPM1x/frrr+kpHAGw7tFHH6XPb4H33ntv//798JBBVBhYy+Ke
AcaMGWMXhciECRMwO9gNoI50FNUcUfZWF4A5MK3U+oGr9B0M4Msvv4S3DE8YNKPr1QsvvLBt27Z5
eXlY0MKcgrqmaVaQ8ncDFht2my6D6bFv376oFzWePHmSVm1/YGhHoqhWiLL3rwcYe5rVPQ3Tp08H
u4ArrrgCC92UlBQ4w3CPAXpjmd5kPiMYhoH1cEZGRr169VAy9aLffvttON6oEY66/fJG9L3oaoso
e/9iwEe13VR75YkjfZUCFMIalVILFvKpp56aP38+1q6RSAQEBglBNixudV2nnPz9gNHGtQDiXbp0
QflXX301jvfff/+WLVuoEQZgdX97ZoniL0SUvdUFIAmoQplMOQyX+PHHHwd16f3h2rVrZ2VlYaEL
3uIUjKcgCBzHwfZSQp4RcDksNkqgkaFDh2L1S1+0BJYuXYoGwGknLYv+k051RZS91QVwUO1XF7/5
5pvDhw/fdtttINJll12GY8eOHZ1OZ1lZGagLSwuAwKAuuAf/Gea3gpS/G3C56VMlULewsDAUCtGX
olHXDTfcgKP9NDiKaosoe/9iUCcZR9u+gVozZ868+eabQSEYXpjEOXPmgGbgGOjq8XjC4TBsJlxf
+M9Iyc/PRwrl5O8HZoFgMAgCI15aWgoOo2T8fPrppy+44ALqRTdo0ACGHe5A9K5V9USUvX8x7FUl
Nbxg1JAhQ8Ac4KKLLnrkkUdWrFgB0yrLMtaoONoeL3WYkYIIflqUPAOA8C6XC2WapomJgJaMilAa
fRpMvWgsg/ft20dbGEV1Q5S9fxKoabW5at+dwtG2ug6H49lnn7WYS9CiRYsDBw5QcgIgGHhbQb4q
A/jsdruTkpKuuOIK2ozrrrsuLS2NthCg70XTjvziehin7G5GUaWIsvdPBdQdjigIYN9nxnIXOHny
JBzXO+64A2y55pprcBw4cOCGDRtAXbo0pe9IgcAVJKsywBRjRY1Kp0+fjgUwXQmDye3bt0drberS
jxMB9IjSFRGAJkbx5yDK3j8PoChVejjJ9FMh+4OhadOmgSRwla+99lqsOSdPngxvVtf1UCgERsGh
he2lT3eqGqAupglMJXQljGUwGgZgTnnxxRdLSkpogylAZuo+AJTAdpxGoqhSRNn7J8H+b0dYWppC
zS9sXatWrehdIhwfeuihJUuW2M+BQCHEQV3EYX5BLYtiVQgshouLi1E7qgN7kdK1a1d63xu48cYb
MzIy0OyjR49anfgFomJuihrhPwdR9v55oPeW7QgMF+hRv359eKf0FlG9evXy8vLoMyGQx3aYNU2D
z4w4pXGVgr59CQ4jUlZWhvYYhtG/f380j37VBAdh0qRJtEd0AqLOP01B1wAaj6KqEWXvnwTbwwSo
w3zo0KE777zTfkGiZ8+eBQUFICroCsLQO8yUUSAtvV8FVtOUqgN4C3cAMwg1+8eOHaM+/KJFi667
7jr61tdVV10VFxdnv4kNUAuMI/gcZe+fhih7/1TYH9MmJibST3wuueQS2N4pU6aAKvQBLI6gMeUS
eAtvGetecAlkBrdpetUBVYTDYcwdqBGkdTgckUgEbYDlLywsfO6556yphvzDDpbBpaWl9icNABxm
St0ogf8cRNn7J8G+SVtSUtKxY0cQAC4oTNmDDz64adMmkBYmF1ylRA2FQuAqUqgFxhFEQuRPWPei
DSCqHQeZMa3Qh8xIQfM6dOhAl8Fo/E033ZSbm3vixAnaNdt/tu/GRVGliLL37ANOMl0QwgTRRywA
vZHjdDpt8wWfuU6dOmAFYHMGFKV0tehTjQCvAbYXTjXmkcGDB9PHWhdccAGO06ZNo31EfymBqe21
b2hBIJUXDlGcLUTZe5Zh09V+u9B+ypKfn3/ttddC3WGycOzatWteXh54S6lrWzxKYBqvPoCN/fLL
L9FU9AK+/ezZs2+//Xb0AhYYHI6Pj8f0ROlKfenK97FsUxy9F312EWXv2QeUmPrJtvFBZOTIkVji
0s/r4XmOGzcOJhd+KeWtjQquVD/A6mI1jiPWuh7rv3j27NnzxhtvoDsUr732GlhNra6938oX0X2D
qxJR9p5l2OtbgCouFo3du3enT3SBevXqLVu2DIlYxJ62jq1g8J/yZOhMgabSbyGKiorQPNAYs4/P
52vXrt1VV11F70XffPPNO3fupH2P7hv8JyDK3rMP6jpipQdDhLXis88+S5UbBEZ89+7dMF9YAAcC
AeokA5QhNnWrIXvRJLoaxxFTDyJgL7qAZXDv3r3RO/pdIZwL+5+io/sGVzWi7K0S0Nuw8C3pq8sU
HTt2dDgclK5QehgxuJo2dYHqzF7aTlAX3j6lMdjLMAxNWbBgwZVXXkmfgSESGxtrv1tGCQyAwNF1
79lFlL1nGdBRWBioLFa2NnXvuuuuSZMmQdeLi4vhgsJ2wfPE6hEpFjUIKHVtVKRWG2C6QavAVcws
brcb1MXUg9UvTqFHSMfE9PDDD6Oz9M5cdN/gPwFR9p59HD9+vGfPnvZLVC+99NK8efOg7tR80eUi
OAC3E0pPufpzWJSpRkDjKXURCYVCIDM6gnbSyQg98nq9mJK6du2KLtOVQnTf4KpGlL1/CLC09pNM
+uYgLGqtWrWgu5S9L7/8cm5uLnzLChLUOFBK2y+WgMYjR45Ex6kXfXl03+CqRJS9/yPgB9r3YE7+
bB/dSy65BIrbvn37rKwsmCm/31+h7DUOsMD0rznAXvzE0gAO87Jly7D6je4bXNWIsvd/B9hLtZA+
JbL30YXZAXXxE75xJBKBaarBthe8BV0xQ8H8ulwu+kkDuhzdN/hPQJS9/w/AT0rRn8N2Aivvo4vj
3XffDeOD1a/D4aCqDANVoew1DmAvvfOMRS96CiYjEUcsicHk6L7BVYooe/8f/Jy9NMU2F9DRyvvo
Pvnkk0VFRTC2SLdvKdN7szUS6BrmJtAV1AWTweFgMAgyA1gD4xjdN7jqEGXv/4OfUxeOn211f76P
LpQYKmsYBtxIqPXRo0c9Hg8scIWy1ziAsXRuwlQFoPuwuug40kFjpMMBie4bXEWIsvfMAD378f/b
RxfqC1sE3S0uLqZ/mEz9yRoJ9BdHEBjuBuYsRDB5YcHPRvcNrnpE2XtmgOEFdb//9X10w+FwQUEB
qAt7C82G/ww1rcHrXnpDDlylcUTAVcgBBEYE7MWMBiFgRkNidN/gs4soeysABbLf4zvNkfu6KvfR
Pd8Q3Tf4LCLK3tMBTYJppdYAXK3qfXTPN8BDie4bfLYQZe9/AcaeZnVPQ1Xso3u+AWtjrIej+waf
FUTZWwH4bLbbZq/EcKSvFlTdPrrnG+CkQFYA4tF9g/8gouw9HVAaqA5lMuUwXOKq20f3fAPEBQ8F
EqOR6L7BfwRR9p4OOGz2q3zfVP0+uucbsMTwRvcNPkuIsrcC1EnG0Z7voWp/wj665xsw6wWj+waf
JUTZWwF7lUUNLzTsz9lH93wDJjhXdN/gs4Tzjr3UtNpcte9O4WhbXUc12Ef3fAP4HN03+Exxntpe
DD8cMyiEfZ8Zy13gZLXZR/d8A0wxG903+AxxPrIXFKVKACeZfipkfzBUffbRPd8A6mJaxNRJV8LR
fYN/D8479tr/dQhLS1Oo+cXcX6320T3fgMVwcXTf4DPE+Wh76b1lO4KJHOpS3fbRPd9A3zYFhxGJ
7hv8O3Hesdf2uADqMFfPfXTPN4C3cH8wY1I3J7pv8O/BeXrXyv64tNruo3u+ASINR/cNPkOcd+y1
b1pW8310zzdA5iCqHQeZMY3Sh+pIwXBE9w3+OWoye+Ek0wUSpmT6yAGgNzbO3X10zzfAS4ruG/xr
qLHstelqv21XM/bRPd8Q3Tf4N1CTbS8GlfrJ9mSMyLm+j+75Bljd6L7Bv4Yay157fQvQgcQiqgbs
o3u+AUNDv/2I7hv8c9Rk20tdKax8MDHXmH10zzdgCOjdBxwx1SIC9mLIsAyO7htck9kL1Lx9dM83
0HEBdbG6oTQGe5novsEWaix7MWaYcTGENWwf3fMNmF4xCuAqZtLovsGnoSbb3hq5j+75BgwWpS4i
oei+wT9FDWEvLK39ZO/83Ef3fAOltP0iDWh8Hu4bfM6zF36RfU/i5Hm8j+75Blhg+tccYC9+YikE
h/l82ze4JthesJeOCn1KdH7uo3u+AbwFXTEjw/y6ztd9g88Z9oKflKI/h+0Unc/76J5vAHvpnWcs
ejGyYDISccSSGEw+T/YNPofZS1Ps6RNjdj7vo3u+AUOJuRh0BXXBZHA4eP7tG3wusbciZgE/4QjZ
Vje6j+75BjCWzsWYmgEMN6wuBhrpoDHS4XDV+H2Dz9V1L+T+Y3Qf3fMYGF8cQWC4V5ijEcFkHTjP
9g0+V9kLwwvqfh/dR/d8Bb0BCa7SOCLgKsYdBEYE7MUMjkHHDI7EmrpvcLVjLwRqv9d2mmPzdXQf
3Sj+V9TIfYOrr+2FZGFa6ewIrkb30Y3ijwAeWc3bN7g6sheMPc3qnoboPrpRnCmwNsZ6uIbtG1zt
2AsfxnZj7JUJjvRRe3Qf3Sj+N8Apg24AiNeYfYOrtecMUVImUw7DJY7uoxvF/waoBzwyaAiN1Ix9
g6sve+HA2K+2fRPdRzeKPwYsqbw1bt/gasde6iTjaM9/EH10H90o/iAwywdr3L7B1Y699qqDGl5I
PLqPbhR/HJjQXTVu3+C/jL3UtNpcte9O4WhbXUd0H90oqhjg87m7b/BfbHshDjgqEJB9nxnLXeBk
dB/dKP4UwBSz5+y+wX8le0FRKhQ4yfRTIfuDoeg+ulH8OQB1YQZgKuhK+NzaN/gvY6/933+wtDSF
ml/MhdF9dKP404DFcPE5u2/wX2l76b1lO4KJDeKL7qMbxZ8J+nYtOIzIObdv8F/GXtsDAajDHN1H
N4o/H+At3D1YCOrWnVv7Bv/Fd63sjy2j++hG8ZcAKhQ+Z/cN/svYa9/Ei+6jG8VfCOgYiGrHQWaY
DfoSAVKgftV53+A/g71wkumCAVMUvQUP0IV+dB/dKKon4BVW/32Dq5y9Nl3tt8+i++hGUf1xTuwb
/GfYXnSS+sn25IRIdB/dKKozYHWr/77BVc5ee30L0I5hURHdRzeKag6oIv3WpTrvG/xn2F7qWmAl
gIkquo9uFOcEoHL0bguOMC2IgL1QUSyDq8++wX8Ge4HoPrpRnFugegjqYjVHaQz2MtVs3+AqZy/6
gBkIXYruoxvFOQSYE2gduArLUW33Df4zbG90H90ozjlAOSl1EQlV132DzzJ7YWntJ10gLY6wqNF9
dKOoMaCUtl8cAo3pvsH08+BLLrlk5syZUHv7QSmlA4405buzum/wWWMvvAXby7f/TS66j24UNQyw
wPSvOcBe/MTSDw7zsmXLoOH0f9eAuLg4KD/WwJQFNI4j/XkW9w0+y7YXrbRpPHjw4BtvvBGdweIe
1I3uoxtFDQB4C7rCAsH8uirtGwyPkn4bfIGFevXqYZ0MFvzbgkUOgrO7b/AZsxf8pBPJaaCJOKJl
sL0dOnSgiwHwNrqPbhQ1BmAvvfOMRS80GUxGIo5YG4PJ9q5aUPu//e1vmzZtAilABzjMNmvO4lsc
Z4G9NIX+Fy6mGfTkhRdeQAcwA1122WXRfXSjqEmA6sL2QMlBXTAZHIaNBZmh2zDF0POUlBRKYODa
a6+t0n2D/xf2VsQs4CcYa7cGC136TTOA5W7btm3RSfTNiO6jG0WNABhLbQ/oCkC9xVP/GwHzC1WH
rVq3bp29Bgaqbt/gP7ruRTuwykWbTp48iWkGC136Dspdd921ZMkSdAk9xBFzFaYoLPHpH+pSfyOK
KM45QJ9xBIFhZmGTEAFj6cNO2GGWZcFkMBxnmzRpAiLAhcaxivYN/qPsheEFdb///vsuXbrQzyDh
MNeuXZv+KS5YGj7DfXRFSakIFe9cCfJPA1LoCZqNxqsYv/amF2q3gqhVhIqU8wiyWBGsOO17dZYA
aRvaWbnZZwR6wxUspXFEfNa+wfRRMHhL/Ur6qkZsbCwYQW8AVcW+wb/KXlRAbx0Dpxl6SldEQF0s
wTHlPProo3YrX3/9dfpiM1bCOEV6/LsBNnKyzsgmjrwMQYOunCayhuhHQAQBKUhHTl7WEKqawKIs
iDJnHenEIZwaeGiAJgu6LJiSELRCWBJNi8PnCyAHVVBogEBUQasQCzmSUJHPktspAVLRVUmgddkj
Zf08rTrSNrSzUrNprj8K8NaOgMkWUQhBFixYAFIA9HO66dOnI50yCxSjjjRQeVX8+/H/215QF1yl
swVqtav56quvcAq2FOtbtAwmF40bPHjwkSNHKKuRh7oZvx+gIqh7ir0azC+MrSoSAtOAOIJlgSvy
V2mgtdhmXxXtICFUaIOgSyKCKSJImHerdjapViDsPRUok+m8Rk9ZsFgkcwiiwmIepKSiZ6vgaE21
CktrrKjdAs5agbSQ8LZSs88Kfo29IMLevXvvvvtusIN+x969e3d61n4fy36AdKZPkn6LvWDsaVa3
MlD3pEmT6L9XA1imL1++HDxHi2kGXH6m61sQhlrUnwTJYvIpOtl8NgRC5l++5CwFGFKD0wIsgoJg
cpLJCwaCgAbQeQQBusJCY3gSiJWmfTk/QJwjSIAKgbhFMmGOxR8ik1PBXylYHCbuTBUcf17XqWA1
jLSNtpO2mfpxFV35Y/g19uIIgwcn9J133gFNqAVGHDlxChyx/2fHdnV/P36VvbDptlm3PXUcMT18
9913YGnTpk0t2pJ/3Ktbt+7mzZtpZmSgF4LeWO7SLv1OnGImiEFkKkqgLkxx0K8EcYRNBpNBV8yX
yAP2EkdarEILjKna4HSTJQE0JoFXNAFzNrHANnUlmegKr/pB4LOlDecIiF2lzLEDfp4ebBaRn8Sz
rcpwWnU/CZUbWRGvYvaCO7a72qdPH/CFPpS57777tmzZQmkF2Fw7I/wuz9l+85FWlpubW6tWrWuv
vZb+2U/t2rXpGyRgNXUGkI3mPNMnQ2BjMeco5fKLOVeA92sC1rcaeOtTIh6lBAERRg6D0kgnLhA1
wtaquCqCZdvJDFIp6Kd8AdsdsF1l6MF5RV0CSICXfzLlVQ7/vZ9XETD5Vm2wVjE/qfS0JtFgt7mi
G38Yv8ZeShzbh508eTJdY1J8+umnSLTf36C3k34//n/2VnaGYVfT09NvvfVWVEwbMXToUPo1AuiK
JiJnZQfAZ+1n//sBKpZwhUfYvBLOEeK9cFDBDU4K+uWIj7C3DEfEkWKNEyFw1bJXYnlZYlSFUbRT
QceanCzLK6mLde9Kt+6I0NvO5w9AALK0oaKwAqFopWDKgh3ovb2gKFbVkQZUZFdauTG0bbSdp9p8
1sbr19hLIwBoTJe127Ztu+WWW8Ad+mw1NjaWZvgf8KvsBRtRN442G9GymTNnUnt7+eWXw4NftmwZ
0pGNvmhVebcIaoT9Z/g1AmxdgPeGOTcMr8mzsL3wXS1ZY8kZpAFxpFjUJS40IMpVF+CGeUXVTY6n
1mzgszVtV6gp1RKVRwjiSO/ZnDcg1tUSxalAlhtkFqsIgq4iEOFQ+RBeUUpXxRGB1lJRI51VK02s
5GiF/7a5itlL6WP/CzR9ZRK+6ssvvwweXXLJJThiGSwIAhzsM136/ip7bVtP5w+sYO19dC+77LLH
HnssLy8P6fZ9M3uaoW4AdRjsLp0Jfu5uUbn/N1S+YQgnlquygHWUKrsNqVCTHarsUiWvLPsrLZYq
mmprKlUR69R5BIxCRfjJnXlrjKynMhqvWUFHsJ4qaVV0RKC1VNT433vL9D5FxdFu8FnEb9te0IGu
JSkoS7t06QIHlt73vffee+nT4DMCYS8tq7LPXbkmwOl0Pvnkk6iDfqPbrl07tBLpYDjmEkTsVlLY
l9P+/H7AI2UDXzjUEw7lKGMcZ5WInxF5Vgiokuz3KH6XwfvCshhSyF0rledVWREUnVWNKgoYibDg
K+HdEd6DSFBgTKy0JV6ReY/HxQmsrJLbJJzMCCrHq35O9Ok62ZcU44ejYn0CijhQ0cOaBgGiMDUx
aMiGymF2U0SPLpOgSV4SrKf0WAGRwAtwpsjASdbDvyo4onBUYT8UsFLQAD9aoss+TfGoogeNRFPR
YDQbja/qu1Y08osA76ZNmwYflr6PBUyYMAHp9C4XXbECth39OQh7K9cBW0qtPIqmbnpmZia9S0Zf
pRoxYgRtnE3R06gO/BH25nJht/kVE/rSLRf7pZBmRkzdEBh/xNQMcIHcYdZYyfCIAZcU8eplRUqk
UIkUySVVcUQVjBDk+SDLBxHxCSGvGPJIIZccDH7xDRMIF/C8S+IYU/LrrFt2e3g3y5M3Q+EIsRbQ
KdUC7WDNg8gLMs8hKAKrS5yugEheiXfJgtsKXlnwyzyDoGCWIzkZklJFgVRkNYbURStCAxDcaBIa
huZpMqdi2rWyofEV3fjD+B/YS1mKZfCNN95I7eK1117bqVMnJNKlKACjDfbad61PQ4XnjBzguu12
Hzt2jEaSk5NRKPXO77777oULF1YuqPIllfE/s9e6GahxIlweXVF1/AQBOB6ENXkt4pQjuWJZlvpl
euDf+0Ple0Lle8Plh8Ll6eH/ZISq5IjC9xaX7you330q4Oe+4vL9xeXrfEf361/ll3xTGP6swIi4
AkF/MMgHTT8sjKbp1r8i0cfdsMCI0w7WNIgaz+iKEDHUEl0JY50Jt1RXtXCQ7FxjAZ6IKku6JBuy
FJAlHHVFUqsooHBUYdVFKrJqrwCahIaheVjgmHIYDUaz0XhrXXYWgCrsyO9hL06BPnSBaRgGXQZT
rr300ktHjhwBgelZevxFxNCd0UBCSjlEqO0tKSl57733rrjiCrjm11133eOPP24/0cXKlnIY5VJ/
+zQO/8/sVUUurLAm79J4N3whQeC8nOgWA+7AZ9vdoZm7fbFz9rw9ctUTfRbc2+2Tv3ddcHePhQ/0
mPNgj5kPdv+4Ko7/6PnJbb2X3xS36ubeq26OW3lr75W391p2Z69F9/b85LXkTc8PXtRywoZZh5X9
5rcH1S8O8GWF6hFGJBoDAgPgLTpFPyijHaxpENHJMkU8KvEl8FAYRvP6eC/LMQLPyuRZPCvLfkX2
y5pP0XyygQgrqf996nq2AwondckGqvPLiKtoAG0JmoSGoXloJJqKBqPZaPxfxV5KHBxBJfAIORMS
EkBd+joHlsH0SwGg8t6Fp4HYXqxdaTV0EQukp6e/9tproC416IjDGtMbVPZtKjuCuk9znv9n9hqi
33Qd/FIrOKY6RV++oKjK8W8PyCfGb/e/M2ZN7UGr7uq19MYey6/vtfb6+C03Ddh9+9CD1/fdcX3f
7VV0vDphxyUDd10weM+Fg/ZcNHDPJQP3XD5g15UJOLv10o6LHx6687GB6+9sM/21j1bPzPzsUGn5
IfVHv1rMWMAowmEGdYGa6jmTO7dKiYcxnF6FVQJGSZlxpFgIK36Dc2lep47gLyKBKyRBKNIEp8a4
NH8VBRSOKlARqivSGQQ0AM1AY9AkNAzNQyPRVDQYzUbjSRfOBs6UvQBoYm8PdPToUZjAKVOmXH31
1dTVxUJ11KhR9KxNzNNA2EvXt3Ce6WtSDoeD/qMNxejRo0Fdar5/cQH988T/mb2a6C/hc0r4LENw
CpriDX+2mT0av6LgiQErbu655Joeq66MXX9d3x1/S9h1TfzWK3quu7Tb2qvid1wZv+OqPruq5Nh3
22UJmy/tv/nShK0IF/ffdmnCjsv6kRpvGrj36l4bbuix5u+9193eZdEDscu7LZd3hsrZ8JcYP6x7
0R2MIjx/xOEa0Q7WMIiyxBsyYwpsQPCHmHwxd49j69aCtdud63Z41m3zrtnqXYew2bthk2/TJu+W
zd4t2zwbcKqKAgpHFVZFm1AprR3NwCk0CQ1D89BINJUNcmg2Go8unBWcKXvBW9CEMqWydd20aRP9
Npi+T9GiRYuKE7+EGPs9D+r99u/fnz5KxsUAyqLp1EenREVltFZMCfbkURn0LED7cyYQImHDzXjy
BL2w9Iel3s8/nHn45tjll3Zbff2QQ1cNOHB1v11/67fttvj1d8Ut+0fcoof6Lrmn18K7ei++p9fS
qjjeF7vgoZ6zHu0xE8d/xs5FXXfELb0t7tOb+6y9se+GK3p8elXs6lsGbb1j8Nab+q67bcCGZ4d9
esBH7lTT/1sAdWF4Sa9q6L/J86qfjeRmqZu2Fi2YvztpxPyOnVJebTPm2U4TXmw25qmmY55qnEhC
o6TaHyTV/SDp6UaJdZuOqd189FNVFFA4qmiY9DSqQ6W0djQDjUGTSMNSXkUj0VQ0OEvdgsajCxWd
+WM4U/ZSWPwlZKE0pDerwuHwiy++ePHFF1911VVXXnnl888/jzJJ7p+B2F77BldRUREuAHXhMD/6
6KNOp5Ouge06AJuutjVHE39t3YuJjVEVzlr+0cdrvFzx3hL9BsB6TGc/yCUvtflkyaGGior/tU0r
775GubHX2pguq28ck3dht/X3DTvQZKmWUvCvFWb51kj5rsC/dpvf7wmV7wqV7whb4VQEKXag6dvp
2cohXL47VL4nSI7ItjVcviVCsiFOE5EHx0zj+2zj24OB/yARp5ABOTdHyhdr5ePd/2qzir9/8Kqr
O318Ta/F1w/afEf8kqS1WQeLOD1QLIqyzy9IakjWwh4vRx9TW938yRPIs/vU8Q+DvryNuQZDRl4k
tE0TRscKSBesl7q9jO72BHO2iwtTtvZok/bihx/VbjHm2TbJz7ceU7fFqMdaJz3aMvmhFikPNE99
qEnaw43SHv8wtXaTlNotkmq1THryVHjcCsj5MDKfCg8jpUUywuMtkp5EfnIkPx8+FR5HwIXWkaRb
x8dbJtZqnkSqaEQqqoXQNLlWcyugkKajn2iZWLtNSr2WiXWbfPRkh9TnJ2zusYdf6AvkoCPoDjpF
um/podVNQCBv7FiiQCCD9etfNZwpe+nCExmo5wvQdxZhJimb6KbWwOWXX56WlmZlOR0xNtMQASHn
z58P0r/xxhvU2P6/k8cvwi4T1C0yNYdBaAmVxdGv6kVGsMgI++WIJIQNNmiyJnmIz5saF5REkwuF
8rRIjlmeuv3IP+J3Xd579xUjcy7ss/GDj/OmH4xkmeVFwX/lKmVFWog1TUZTUaBHDTq0kiLtaIF+
rEA/irhbjXjUsEeNOLUSh5Wer58o0Ego0o4VWRGkM3KQl3QcnepRsPSw+WO+dtKtlliJJs3PKLpf
MXOMkxnm9znGVw71mF8hX034Ql84ir/fI3/+qbfsg7SVd/eYfn3CupsGbn0wflnKerd55BtJUBmp
xCMfk8LfuX1BjdcCnERfIQhwQojjAjx5EAoO01GvDoB20oe0ZCYVTcyw5MUymUwxAakYYyP5JUy3
arEjV97BfpE7P3fCu5NrN5hVu+HUF95LfeWDUW+3HtOo8+gPOo14q/voF3skPtN77LO9J77UZewr
rca80nrM+13GtuqW3Kh7YsOuo9+zQoNuiW91T36tR8ornZPqd02p32P8Sz3Hv9ot9fUOia93GPNO
56QPO4xp0iHxgw7Jb3RIebFjyvMdcUx6s2Nig06j3+kw5q3OSa90TH6pU+KrSOk6pkWXpFbI3C6p
Ibkk8YOOo5tUhDGN2o9p2CH1vfZpb7dIeu6DUU9+mPRYi0m1usx4dVHWeObznBxpuxIpUhVe9IlE
J8UQukzfqyPvtIsmBEKfG1sE/gX8b7b3F2FfNWnSJLC3devWv/XECLntCzAl7Ny5s6ysDNTFHPCL
C93/FzZ7YXWdhubWyWSmCZS9mkMPOrUwL0ZUPmxwhLrECAs64qqg+1Ulxyhd5/6h6WT3Dd12XtJz
16V9tz47LXMd/3Wh8hmrHPcxhpcVZGQ1OVV2Q6Cq5IeGoa5TL0gRLVStL/ggTetlLMLGis+Grfcu
wVhR0gO8P8y7QrwXzQMtC7TPMadglrES3ZxsgqVElSUWU0Ou8blbLRMlLcy5izmXtW+/5hBN4fgP
qx16y5k7ruq+6NI+W7Agf3vioXQ2wvEiI4cZ9ahPPsrLxQavGHCiyef7JkQB6pK3QQV/dWOvIbkM
0YvhEMUgEZ1ClJiostcwueKIEuJ5JxdKZz/bP2nrkKbjXmw0u/4LYx98eUyt9rNapm5MWn546e6i
zbnsLj6wL59ZtS9/1t6iuenc6gxl635x9x7/jsPM9nRma7p/e5Z/V6ZvZ6Z/e4ZvU7pvXZ60IZP7
9KB36T7PssPMWtApR9qbxe/PlDIz5EOZ6rZMbX2mvjpLW5+l7MiS9x70bT3AbsoQ1mXI6zPkTYf5
7fvcB3YV7cElWcLubH5vNr8/mztEQxZ/IN9I3+FaveTw1ClbB/Wb17hx4pMNR9/fckLdlqkvoyPo
DjqFrqGD6CY6a7OXaJQYJPqJVMlV1eyl3KEeLkiLlZd9e/jnqHjeS0FNNr1BhVJQ92ns/Z1kttlL
/A1MWgL5MtbgyQdc0AY4z0gMsHqAJe8VMqrgNji/xpk8sUiCyDjDJ5L3RO6J23Rt92239N7y5MCV
MzINX9nXYqCYk8IMjJYUlPQgq8ke3kM/16xwcqxABG6xlwoaU4ZlQ4j9J8K13vew3pfWTZ4FFUFU
TRB8SgksMFgNOlH2IhvyYMxUiYVBhtH2KRFMNJS9Cs9Isl7EiFzJF77PyydnmPcNXHdZvx0xAw5f
Hbdm5t5Cn8yhZlgqr89pGnA9oAqK5QuUwF+AZmgSph73r2nDXwI0xmoVYS+ETGZDi73kFGsqTDCg
GKLsdBu7Ps2c2GzkS41HP//mwEe6THlr6o6he7iV7vBhLlQoBzxmkPN6cxTVHSzmtGIfEyhwmTnO
UJ67OIcpyeKKs7hIER/y8EGeN0VOl3iNZ2HWDUYO+GTDoQZcZsSnBbx+pciru1ym0x3IdQfT3cFD
3kC618zzGkVixMeGi5hgrj+UzUUK+IgbEyYbZFg9j9OzOS2X0/IEtUBQikhQC3g9n9Ez/MFD/sie
dH7Z7K2Deo97o2HCY81HvdR81MursiZ5zN2S4gqqpsqGFC6ALvMKh+7b7CVeya+P19li72m3h3+7
hJjKS1b7vhRWwojYRVD8POXXYLMXVDE5JcQqYUYKsVjpkcdyOIKoxYwQZmF2BJ/OOQKsz4Ah8haz
XpVx+iInms7Pv6jr6uvj9/wjfkP8rN2+wOcejvPxfhhPQS9VlBJW0EEvjyRidiTLFWtNgpLBNBos
KVdQmsjU+nIYQQOrRQwJ+dYPcbAUXIUNBJ9heJFosxfZgADmFtEPIwzDC1sKKw1bDQKbIh8wTEaQ
M51+R/DzXcHyZovdVw3YEfNRfkyP1bELNnl0SdEZRXJKfL6pkoHnFaFIj+SbJW5d92twmq2vxqvT
R4VEhrIbvgwmKfJliMVeKDFOqVJYF4MC7+eV3H3OJT1S32k87PlOY9/rO6HZ+sNTpdJDamkOI6Vz
Yq4keQTGL/h5nhUYjvXwbp/uZIudnpLcvMBuV9leR8megsD+XDU9X853KF6fLnCmjMlODalaUGF4
l8Od6/M7RYnIh9U5xvAxpoc13Kzh4XUEBoGVvX7Z4xUdHqHALRV6FYdP8zG6V9Lciuq0gltTEDxW
cKuyKxD06kGHoGWKwQzG2Ld2z7SBk9qiC02Gv9AzrcF+11JBzRMFxpBC6Cy6bLMXooBAIBYIp6rZ
S2Hfivrss8+wnqWU/jkq2Itq/t+a/gf2ojMqMbxKiV8q9XMhjnzNBynAHUUclhZ04lTOp7OQO9yS
Yr4w6M72B0ofHrU+pt+2qwcevLfHso17/SWKFpBZUfL5FBVEwoKR8Up+LMcCRMOIMZcETWTBfwQU
DvZWoi4hNs7CiuJsgEcGDiwFgUFU8PYUVxUkWutzQmkE60VZxMFtL075lTAuQTY4vSHOr7J+Q5KK
gwG3nyuQQq4vysflfnFt3LqYobkXD9j+1ui5BViWm5KPLSwOsRqfExDzNcnl0xSnbpI3bnVEdNCY
3tKrLoCTLJMvMSy/A+OmcIpA/zBE4HVNDUiqz8HvHr+kZ6fEl1uOfrn18Dd3pS8SuIPhoEPiszlf
rqmxQV1jMRmKumIElXBACAkuIzdd2LG5aMGS9HEjV3cauLxN/PyWPWe16D2z/YBPeo1eMihtxaiV
hxbsdm0t1HK8AQ8fkuVwSDKLydvm5J1GhpOJ51bx+QOCKGNqkGVV14K6EcBaHI1kVR+neMiESNwx
Eug8br3tzMqcz4QRFb0OR7afLTRDIsM7thz6tNXwd1qMwsr55XGLexRxu0TFqyomj5GXScfRfQjB
+qxNg1ggHPJWyC/hbLGX3gw+jWi/xrsKzxl8o5d9Y/11Bk38n2GzF+Sh2gnbW+qDsQXBIAI3pGAI
LHxpVSDeKda68J/hmYR5R5k/hzGL/9Z/ScyojEs+yvh7l0Vef0nE5SpWZQjUJcGrUzVR10Xy/gO5
g22tZsFekBN2EgHshYhxyiI2cgkYQou65Cxoiaope+G3U/NLuQpjiwD2opEgOWkhZhnejThyWtwm
S3TyHrz12j3vdpbokmloLl4rKv5xpVh+W9zqmL4Hrht2sFbv8ZlqxGWEHKyvrNhQmIKQUGiIbszl
WPnTI+wwvGg0nox5dYEgkY+oMFGScSGWRyXTK7wGCR6txy2H3One1S0SanUf+2qz4S9+vCVRUnI1
plDnnCGVDwdUXVdBXC/vd0s+t+Ep0nN3uNbP2JCUMLVNx6Q326a+0mLCc43G1X0vuVaD0U+9O7ru
h0n1W417teOEt+HBdp3QaPSShEX7Z+/37y8yGZepexRFAH81VpENBcZfKNOFEl0kXoDklyVGkxhy
9wDEFaALAZ8a8TMw5oqfkUkE9pmHyyO5BcmN0YJGmEEtGA4YAVPSdE6CR8RM35zWdNgL3dJead7v
qcOeVVLQVeR2kY9SwVur78T7oF8CW3+i8mu+0tllLwVMLpa+Npt+DsLeynT/H+r7Oez60HOHKbkN
SRPgOUsBSEPy86qb08AHgdwaEcoY8ZhHPuaXSzBbw6yVMQVMsOT6gStjRmbFfJR5W6+VrPpl2Cew
jOISdSfoqkqmIhQrQkDgOIbFShUEtigHErrAT8wYlNVYXiICJoPPYI5NYHASg4EMGBWIGz8JV8nH
KPAFiBNOrS4CCIxTUBCkg8C4yiIw+RIwoGgK4zX8zpCMVojZYtlOs/yB+E8vit1x6+D9T3SZcFg6
4dJPeKQgNI8SXhWhB6g0CAOOWQCuuFs5Wv3YS79h/gl7YYIUM5Trycc6c/6WMR/GP9hyRJ0+M1sy
J3IjZSKGA6YR8vHxUj7jK5S9vjKGPelc7fjko6Wdu0x6s03qC01H1Ws0rE7T0c80TKzzYerTTcfX
bzHp+RaT6zedgJ+PvZ/0YLNxjzYc9eAHwx9vm/b6gLldp26dsNO/mz/B+/U8QXHC2Ip8WGGOIGBd
qrJGiVYckIpVrkTiimUxhNmVVXOdykFXuMARKXKGi5yRAlc43xXJ9YRzvaFcrtTpNguccqFTdrlk
xiELXt3gynTPZwXoCLrzQZ8H0DV0MMedJxtBYnUrsZfcN6HvYlYxewGQ9quvvrK95d8ohLCXvuoB
9oJ1Z5e9YFpRgBCYOLci8W/h2MBJZjQyu3NimUP+Pkctz9DLc7QffdLn5E9Vvb4c88S1g9bF9N8f
M/DwzQkbCpUvw3LIy5Wy5udiOMwqDOvPU/wOYrolEw4tfGmLvbC9LpAQdflULFMjHhVjUMFeapBB
bIulLAYD7AXzLQtM7K3lXxH2WkNw6hGfKFlmlpxCHNSly2MsgEVBK8PC11cU5LCs0p3q0Yxw+YM9
Fl4Zt+fmhAN1esxKl7/2BL5ljM/yXYqoH/MoR53K5wXq1275c1kwNV7jxQgnRcikXo1ABggeI2Ev
QiUNJu8MB7Bu39ct8c3uac81H/bk3H3J+YHDPsFFRhbLI1n3GYY3ohYUuw+H9o5a1bvztLcaJz3e
LPWJlql1mo6q0ybppZ5TGo1c0nPs2v4zdo+en542Lz1pyq6E0WvaDVz6YetxT7ZMe7z12FqtUuo0
+qhWk2HPDZnXddmhqf7gAV7PI9477C1fJnNlGnlOocmMLDOKwoY1pQy15zoyV22dMW5h/5SVQ8es
Gpa4amjiqoFJqwYkrUpIWZWQ+mlC2vIBacuHpC0ZOX3t5E1ZW5wm6w1p2ZIzJ5w+e39yi+FP9Rj7
fPektx3Gfj4oMgrxj2jfIYRT0rD+ba+K2VvZ0lKDimPlm1OV8ZN7zmcLdgvguPp0waNLPlX3aDqJ
6OQ7WGJ4xWK/9mOtNmMf6Db7/riFD3ed4QiXu+R/ObQv9h8vv7TfppjhhTGDcy7vtSo39KMkRTz8
5375M9003N6scKnk4z2iGBaVE16plNciXqejWBMFb5GiKIW8UaQddQQ+y8d46wFFxZrNp5uaR1a9
iqFoKsv6ZT3AgeFikAsehXn0S4aq6hzr11VNVjQ/rG+g1APrLRmKagoMG1AlVVaQzgY/y1NOFKrH
fVIY6RHeUyx4GY/XLZfmRMrv7/LJxbH7bxqS91CnGVmB/xSpJ/OFEq74u0PCyac7T3q008wnui+o
33EiIx/TWCEswvpjDUcHvVqALvYsfSX//GTFWVghzMJuOA7F0ubc+T3SXmsy+P42o+se0jYUBQvk
kOnyCuSpuK4WqF7/CW4bv77n9GYt055rMbZ208RHm45+rPvEN8av67vFuahIP8BpBUYAs6eTkRxq
mGODDn8wt9DcnaGsXp6ePHROo26p9XuOf7b96CebD3mw85hnJq/sLZSlu5VCF8/Ac/F6xeJwCUZK
k+DAyAGz1O0mQizkMjv1f7vryNdbJ73WPPWV5qkvNU97vkXaM61S67VOrdcmpV7bpPqd0l5rPfzV
bklNP9k2pTCYwx5hCsyCguKcg/qGtmPqNR3yQM+0NzflLxBKZI8ko8vWmp/OZVQaFbcAKoT1U5xF
2/v7UbXsRWfQWzJzK6ZHM9265tHJ7E68LH9Jvlb+WNyym/ptvLzfxpv6rvmULz8YKN9ZXD5bL790
wL6YQfkxHxX8bfCWjNC3Xlbj1W8Y7ojC+wUujw1LRcFwofZdHv+tUzpihI6Rt5fgXAN62BH8Ksv8
Li/4nTt80guPS2I1Q/VrRqZSekgq8ZklUiDsFxSfbDCBYw79eLZ8LFOIsHqJYoY5XmQljQ0ccejH
suUjSPfrR1SjGItccqfEPFJgfLFf/W6f+JV05AdZC+o+R6nkN1XNrx/LMv79WOzCawfnXdY/8/b2
M9KPlOcV/3ufeHyX+NWeo+V39lpxfezGW/tse6D7fIf2pcIJxRIT4r0w+3TUqwNEWSL21mIvuedn
/TMjUhhVccuyL8SsypzRJeWFFsMe7Jz2fLa502G6nKygmWW8Ae/Dwxxxrcic0y75zU4TXm+RWLfx
iCdjp7w9f++oHH0LW5rtDxYwkktyeUp4s5gPGIwmMuQDLCUgiGGvL5jBHD3AfLZzh3fqqAWNu6Y8
0yWlbtsRT3VLeXnIjDa58j7juOqRfIzEMbzHMEWWcwkio+kmubUR4Av4g3GjG3cc8WrL0S82T3y+
eeKzzZOebp5U8WpX68RarUbWbj/qudZDXuiZ1Hj+lon58kF/2OnQ8jEBoSNdxr7QbPj9XVNfRAf9
YRbrbXQZHacSIGLBopmwl9wK/UXUQPaCOuTvFARJ5cmGA3QphcUDmcmC3+2Sy2/ttSYmYU/MsPSY
hO2PDN95T88Ft8YtuXPMgZg+h2IGFcUMzLq276qDapkZDIhCieg3I4K7rETcxTDbg99sj5Tv0ssP
e4tZQYfvCuOplXy+3xfaa/xrV7B8jfso93l5IcP5GK8cDO91S/tLynO+Lt/qDGgnvivyCQ5W8odP
7vBEDkfKXSfLtzsM4zhNV3zhL7d57fSAcfwHj5f1MDzyb/aW7Skpz/uqfH2eyhmlMuMy4Tlrhl8/
cVD69tGe82I6b7hkSPo9A1YuNcrXhct3fla+LFC+5Ivyiwfsjkk4fOnAQ7f1XpYZ/JqFD6+SF5t+
7QnEXwLrLiMJlo5W3MECPeBJulXVGXAvPjC2fXK9tqMf6TP1jaJwulvH2ZBhlHkEp1BcuCl7Vq+U
1zqOebrF4CfbDX8haWGv3UVrxBK/EhJ9jBtTa1gPHhNDpX4j5NOCnK4JiqYp+Z6sdXuXL9o2Yx+7
znlkr/eL/bnFWz7Zn9Jrcov2iW81H16vydC64z4dmCnt8gaLfLpTDHg5xaWZnGrwvMhwKuuWCvK4
vTNWDEtZ0GssPOQV/dNW9EtbGZe2stfYlbFjV/YYtyJ23NKeE5b0Hf9J/4+XpezN3GiVANstuQ1P
Yehw3JTXW41+EF1DB9FNdNZ6mZfoKr1T9VPJ/AJqInslcrcmwEkhfzDAhMkbkaIgy5C+5NA/26iU
3zlkT8ygrJjB2TFDD13eZ/WNfVdclbD+ovjNFydkxsRnxwzMvWvo5kOyqSg+RQ7qgh5hi3z+ojcG
JD2UMOeePmse67nEX/wfuHeypHsY9YA38Fbc+Ke6T6vVe/YrCR8fUD7nA5HI0TKnYn7QP+2ezpOu
b576esKMDOm4YJYEj36epx55v//kBzuMu6PxqLf7TsmQj7KBMv3Y11na5w0GTEf63xuPaBA/JUs6
KhqR4NGTSH9r4Mx7O069qVlikxFzC5SygKmDwH5G9BifHZS+f7zn3KsG7rlyVPZlXT+5f9Civ/ee
fl//T67uNuPSvmtihmTGDMzD8Yb45YfCX3oxixvib3hifwkq6Sh5zEaekUisKClwjDFFFQVcCw6k
tINJTHokYVoDTyjfp0lm+Ljbz7Fa0WHXpwMnNGg7+NEBU1/tNOLp1YcmZvE7xAjLqAImPpgtVTZU
RjyqBgJeXvMxWIwoKisFfcu2zolNatNrbKt2o99LWTdwp7I6s3Sf82ThNn5n39m9Wia93HHCq81H
vDR6Ye8Mabt8xMMFXB4unzxL1zkvU6QGecZwSCUufyjdqe1z65lOI9ttpDvNQ07zgDOw323udZv7
mcBhPpDtFTP9bL6mCnCXVI58ze9TeXcwr9/Ud9okPtw2uc78/cmFptNiL3l8SJ48ESGQYTrv2EvW
tzAyvFTsC5Z6gljpBnhWlR1+3V9gHtugld8zeM+FA7JiBmbHDAZ7V9zQb/G1CZ9e1mfdFX33Xdjn
8IX9c+8dsjlb1EzZ6fV7BEEISn5GD97feexVPZde0Wf7vX1XFZlfMz5WUwM+rSwvUl63+9S7us7/
R++l4N5e9SsXRljXDvq12p3H3BK/5o7BO+7tNHmv+o1LDrJacB8Trt0p6b5eSx5KWIf8e9RvC5US
l3F0O3v88c7j7+297OGENQ+3TzugfOWUQ169BOmPdplwV9yy+wes+UfLkRnaZ2a4RJFFD296g98c
UP9dJ37R1QnbY/psvaLXspviF90+YPnVveZf03fZzcnpMUNzYgblops391mUHvrCo0i+QMCjWXfF
qw0s9sJFRBQDx1pvobKSqHEyVj26I+xZlJHaIa0uFL3vlHc9AQc8Vx8u0fxeLX3ElLadhtbpM+65
NkMenb9jYGFgmzeU6w34XDLDG3KwLACauXxZPF8kik5Zd8lhR5GypzC0M21VfIsxz78x4OGO095s
NRGh4fRD0w6V5R4u9e4JZcXP79g06dk2KS83GvTM7M2J8glHjvegEuAYjjx69TOOYLHoFXJZrUCK
eBjdBW3jDEHQcWQ508+ZXs704OgXndAF8rBR0gJSWPMHZZehCRFO1t1mUfzkBq3HPNQ+tc7C9JSi
kNulkS5bb2gQIUAUEIjF3v9+tnEaaiZ7RXKnVwgwZtgfJA+NeK8h54mqi40c266V3xa77qL4QzGD
c2L67Xxw1Ja7e398e9wnD47cdnWvDVclHIqJz0GGPMksCfjEoMwFDF3XPeFv7+25/KoBBy8efPC2
hE9zlOPkzlKgOJeL7AuUP9xz/u291t/U49Mn+iw4XFzuNyK8Gcg2jj/Za9o1gw5cMTjrn/3WwIX2
GaW8GczWv6jTc/LfE7bc2n/nI/HL9pWUFwW+cAY/32f++7Fec+5I2HpbwnZSDlxo84Q7eHxv4N+P
9J5z64Attw3Y9HCPmQeMbz0YYM1g9SOOwHe7hB9q911+db9dFw04ePOgbXckrLorYen13T/+W++F
F/ZaHTM4L2ZgQczgzFvjlmUFvoTSs0bIr2DZVi3ZK5O31gzRS95zAHuloFsznRHv4sw0sBeKHj/x
XS/cHU3P5/LUz5iZq0e36v9cx5HPdk6qP3Ftdz9WJMcPbHOtmrFhyqg5wwZPTUhZMHRDzgLxeB6v
FwYivBhw+828QmOf77NDSWt6dZz4WvPUuu+NfPT9MU81Sn6+efLbHy0fsInb6vrOtV5eEr+wRbMx
9RsNqttnQtNNWQv9epFsfS6twXazTqxanN5ML1/gl3w+geFV2Qoiq/OszrE6Q4Obc5E/XFLIZ14B
OWjyAdVn6GKYl4IezdlnQgN4zugaOohuulQDXSbP9sVTUxh95/m8Yi/8DTiH5NEZeafCxKJfVlwh
KSMk5nGMmGf85/74NVf23x8zYP/VCVuXieX7QuSu1SK1/O9xK67ps+PShKK7+u50GccFrqBQFzJ1
tVAwYeJu77HtkoTcmEH7buwzP1sqDqqqqATT2bIdwfJ/9Fp5Q9+dN8XveLD34i3Kdy7Z9MpKhvHF
Q3FzYvpnxvTJua3v5rVqeZES8cpqlvHFk71mXNtn24U9t98Vv2a1Vg7fOFc9tjtQjsuv6rPr4p7b
/hm3bJPybyTmqkd2hsr/2WfhVfFbL+m5/pG+i/aH/53DyIJqOOWSwsD3O8V/PRy7+IqEnAv6F17S
fvmD8cvuaz/uidipdQcuvi3u05gBeTH9PTGDXDf3Wp9rfCfykiFjHUHeD6k+gGqSWxL0Mw+JPCTX
RL8skOdkYC9s78L05PaptbDu7TuxoU9jvKpcFMre5lvWZvg7zT96pWPKW4MXdCj8YkfOiS1pmxO6
z2jSNPHlVuNebzPxlcbJdVpOqNtvVtN9uVvonM4oXibgVT/nRs7t2ynp/WbDnh7wScOeU19sk/h4
z4n12416ou/k1zb7p3j+deBT7uNOk95om/hi249eGTGlp192cAIvkVfIQS5fQCfvh4UjpgB2qqZf
ZXy6y2s4vUYRgtssoIELu6QSr1ctcHI5ouLVTU6SfCzrB3t9mg/daTfmUXQNHUQ3PTpWe/TLhAoh
UPZCOOcVe8k9TEaVfKoJLxHLJF71m1J+SHBoouE2vn+gw5RbY1fckrDu77ELs0rK87R/5ZhfHzpW
fkfswusS4Dzn3dB1gztwUlEYhy4WhoKewNGDZvkdvXfHxOfFDDpwy4AlBcZxXRSx8iwyPt93vPy2
7ksu7r75xgF7Hu63fKv6PRc+6tUD+/Wv7u8zn1j4QYV3Dti2TitnwicYNXBIO/lo79lX9d97ScKh
e/tvWquVO4u/LTSO7zDL/xm35MqEQ5f3PXR/wtqNarkn8mWhcWxroPwf8Yuv6r/7qr47b+/y8aEj
5U6thFf0XL/iifyw3yy/p8vCC+OyY3rn/GPAzo3h8l3qj4fNH3dqP64Jl18Xv/2C+MNwKO7uuTxP
/07k1JCoVrcnRgCGjCzFicfEWs/JWUIS0fRrBta98w4kt06u0zrx8X5TPoTtdWpM4WcZfea36Tzp
/YaDn+0xpcWB8MZdwTUzs5JbTnilUWLd90Y8+eGYOk1S6n6Q+sT74x9qkVZ34PiO+7O3wyryAcml
MU7DGZvYodPwdzuOeGVT0fgtrvEDpr/V7qNHO41+qGvy/X1nPrddm3eobOO8jJROqW83TngmNrnp
ks0z5QgWopIi6RoWU7AQ/kKHI/uThQsmzJ4wduFHKUv6ISRbIXFp38SlOPZLXBSfumRQyrwBU5Ym
bs9Y7VXzBHjUio+TVL/uTZjapFXyE21S6s4/mOIOuFmd7Hpje87EASFLX7C3Qko/Rw1lL7nVTm57
+FUSrOfg5B1GvxzJ5Uvf7JZYv/OYel2SXuqZdkD6skj93KOGD5lf3dpn3YUJWRf2y7s9bmOO9gVk
h1mAN3QYzPTAD7f2WnfxgOyYhIM3xn+aY5wUyYYpgsss2RIsv7vP0iv7bru277b7e87bHyh3qmUu
vXRf4Md7es6+bOD+ixL23hm3cHeo3KOU+NXiA+YP9/WYdcXA3Zf223Vv7FI43sjvNIp3hX64K3b2
VQkHLo/ff0/scuQn737okV3B8rti512bsOOa+K339lqwN1JeqBVTb8qlFB8Il6OQy/pmXRqfCRcA
U0ChecKphXO1soPB75+Km/WP7rMfiZ1bP3ZqlngcKwFNDgh+vlrZXrrctewMGTXr3pUAf5MQWMVs
61qVP6d54rMtkuv0nPSeJ5TvKnEuY+a9lVy/9eRX3xn4xLK8qVll25e5ZmH52mRs3YbDHuk58d3U
5X0nrhsxaEG3JmlPf5DyaJuk2n0mfrDTs9lzRMgL+Pf6DrQa+FaH4a+0H/yMp2RTrrIpW946ec3g
jikvfjD8jnYTHhm0qGVm8XbPiZweaa27pjaEee826f284p3esIOVGFNQsRxT/IUCn9Wxb+NOI99r
nVq3+fiHm49/tOn4x6zwRNPxTzUfVxueecvk51qPea1T4odzt071hd1cRPSprCCxPqOg57RGjVJr
t0x7cdHBSbzhJgwkn6bRLzQE8ofyAnnXCJKpkNPPUAPZi85YUkD470e25NN2JezTS/jwZ5/uzll3
0IGwfHe+N/KN2yhDZji0t/TZGNO/4IIEsHdDtv4lkaAoqCp5ATcz+PVtvVZePDA9pl/6DfHrs42v
BYHonNsMbw3+++74RZf33XZ1320P9pxrsfeoUz+2N1AO9l7Tf+fl/bbe23v2ntC/yH5IainWsXfH
zr5s0M5LE3bc33PBIbPcY72cvCv83V29Zl2TsP/KPvuxxt4TLEeb/VpgN2aHnvOuJ+zdDPbuKSbs
tfxM1qsEwN57ei29vG/65X0OPthrwT79O58exqTjkdUCvWzqjoJJu32z9vGTN2QxxV+7YXpkQ1er
2/9dCeQDI5nF9Epu6VrvqJPnOoIiirJbdazInt1kzHPN0p7uPvVdT0l2QShj6Ka4BmOfbp76dPzM
hgfUldnFW5PWJTRJerF1Wv34We9uyp2jHXP5A+7D3L65h8a1HFu/8eh7myc9Mmxp3AHjQFa4cMrm
yR1GvtU18ZWkee39kT1FcoZwhN/t2z5+49BmaY80Tvlnx3GvbnQuLApnrjg8r/XIN9qPJU74Bn5m
rnnILbqCvFkqBMKCh2EO9Bndqt3o15onP9kk7YEmaQ81In/ogfAoQpPUx8n/bIyp03rUi92SP5iz
aWKRmgfP3y9CoXw+M7vbjEYfpD7dIu3VJQemSppbg9qS21T00bekCmRzBvK4xPpwjUrqNNRM9p4C
+ZsV+sYZpbGbfOIX8GrFXr0MwaUdKZSKXYIJO3b22PufP529i+FsXx6/9+Fe8w7oX8EBwylGkmCB
s82T2SXljqPlmfIJIfK5lyOvjgbJ/xZUCKh6gHzRAQvzU/aSwPO8Sy1cljWjSWL9Zml1e0xr4C3L
PMhvaZX2WvOxz7QcUWfhrqH88X37fEs7J7/XfMRLTYc8/fHm4YX8/lCxoqg6b8iFZt6kTR+1Tnui
SdL9zcbUX+GYudfc0HtGq04pr7cb/tycTcOd5iGH4uVLAs6Ifyuzrsfst1umPtlqVP0Zm0fkGXt9
x7JjJzRqm/bieyMfHbu1j7ssjzV5gzNNr6b6XA7n/pmfpiUtGjRh5bAJK4cijP8UYfDYVYPGrk4Y
v7rvhE/jxi3tNXlpv1nLR+zJWCWrbkVkDGhllL2noTJ74W/YgXYPNCZP0vSAl5M4NQgHyMEqrBZ2
8ZpgBOGtnQX29ttSReyF5/zb7L2s377L++56uNdsLKoF1cDSEY49ZivyUpf6eb7+uUM76pcCgihr
WFUIflss1QO/xl6B51m3VrAkY2rTJLC3Ts8Zb7tLDq3PXths5NOtk+t2Hv3SAfcn8rH9CzaPbDnk
+fZjXuuW8q635IAc8HjcRZqo8/A8SxXP0eyBCxs1TX34w9GPD1nd6uPMoR0mvtxidL02o148LG6Q
T3BsOFCkqq5SqeBIbv/FrZuMfrJj8iupS3oXmfu8Zekztw5pk/x8k6SnOk19K8c4yBicxgUDXjMo
wj3w+IuLCgI5bt0Hf9irITBuw+M2nG4zz2tm88VZrHFI0A/52YO64ozoouJhAuQ2FBNl709gsxeq
qVrfxFcOlMkquIgOWxsBCSJWFjrdPwGqc26zN2HvZf12PNz740P6F6KqBXivxvlhZjnjaIFYUsCH
WK2Y5QRDl0yVEVmHpRDVB2S8iOL+hL1k/yFR5H2mY3HGpGbJzzRNe6LH9DecxfvmbkrumPpS8xFP
DZvZ2qlv9ga3D5nSvO3o59qlvvTRgs7y10VKiY9hncGA5mfcbtntCOcsL5rQfGxdrEUbjL6v65xn
W014ouFHDyWvinUWpxcZznxZzBF5VymXEdrXa07jxqOeaj/i+RlrBnmDBwu0nTu8czuNfbFpypNN
Ep/Z5lrjVf2GEA6yEY0VGN7BHmHcEcYnB1jhiB9BLPbJIeuf2WW/yvhlB6+5jACjyb6gJId4QylQ
QlxQFdgoe3+C09hLHppVCpTDLOOTRF7gWevjIxER/EQiBHQus3fhJf33Xpqw7cFK7A0wXvKPVkoJ
KxfzWomih0SB0WWfqXp/478a/iL8Cnsx5yo8E3QsSh+PJW7TtMe6T3+1MLQjcX5cp/GvfvDRE9M3
DPMVH8iU1nQc9XLbsc80Ta2z3D01I7K9UD3IaLmCmsvq2fJxX3Zwn+e7w70WNm4/6/nXht/SIPm2
JhP+2Ty51k55cVFpTp5W6C+WmSPyIWXfhE3DPxhWu/24F9sMeX5TxiyvcZgJZ6TLK3tPa/D+yIda
pD7/afYCr+7ThIDuN3mX188VFej5hUGXR9P9cgSB3FtRg6e+jWEdAvn/Ddn0ixJjyHpEKVY9oYhY
BnpG2fsTVPKcSYcpXU8LWHLYx8o/wYRzmr0X9997ccKOB+Jm7zeI5xzg/QHGTx4u8gFRDKtqWBRl
XeJk3qEpbkXx/cZtzL8CNnvJvYmfshdubdGCQ6nNU+s2HftIjxmvFoa39Z/Srlnqcw3HPD3v4LSC
ksNb/IuaJz7bMO2Rd8Y/NGpP7IgNvccs7ZWyuNfE5bGzNw9YeCjp44Mpa7kl/Vd0azHl2UYTHno3
7Y5G4++Pm//ugdAaz/F8d0mhK5K7j90wa+uIdkkvvN737g5jXxw9p0uhsMOnZCtHXIWBLQNmN240
5ok241+eu2uKU3YovBaSwoYslJzQc/VM59FCR3GuM5KP4IgUFBYj5BeW5BaWZLuP5LiLc1yB3Dwu
C6bYCBVzXEDgTEmM2t6f4qd3rdBbdPv0oGsK63WFTU3i/AjhgMp5XSb5Q8hzmL139V584YC9F/Tf
9Y8+c/eY37Ba0OQ5g2HAA5P8J1ZIEQ3W6ysOKOT/zWQ3L3h+TRv+IvycvXTbTgieYUIF8w4kN0+t
Y7H35cLwll7jmr83uk6ziS8vyJx92DywsmhOi7QXXxl1V4Mpj7ye8mSrKW+2SX692aA6Pce90Dm1
7ruDH+g49Z2Go15vP6PpS4PvbTrt0ffH3t1o/IOjN3VZ4Zg5f++0jzeN+2hml/bDXmo78pm2iXXa
j32u69i3tmTOl0IOv1gkhFzs0fRBc1u2nfhcy7QXp21KdcqF8NvCcpD3OXxSTtqSkUnLB45a1nXU
8vajlnccsbzr8OXdhq0gAZGRy3uOXNIreUn/cctGbsxY4zcZv6oImoZ+Rdn7E1RmL71HVTnQTgI/
5TMsMDme6+y9YMC+mAF70Iyd5o8+vUQVJZ0RDE6ThaAkBGURSiCp1rvv5D/Nq9lXCpacf5m9uiE5
lIwlmeNbpNX9MPmB3rNfzzHW90ht9GFyveaTXlnv+zS/OGfa9pTOkxq8k/jgO+MfaZDyzAdJL3ce
937s+AZthj8ZO+nZd4c+0DiVJL486IkmE2u9k3ZX46kPvjXm3g9G1eswrmGrj97uMPzt2OTXeyY/
32rooy0+emLI/DYrM2ZwoSJB9qm65hGcTvPAxA0JTZLqNE95PvXT4dIRwdBVyUvWIG7hYLvBDTsk
Y754pFXaPa1S/9Ey9cEWqY80S3uMhNQnWqQ81TKpXuvRz3dOfHvulrHeSA5b7PVrLkH2RNn7E9js
BVcr/pegUrD+X8ZiMumvTWbyl9+Uxuc4ew/EDNh7d/zi7YFyt35UEuFjKBqv/4S9YIhMvnpHsIRQ
ffBz9lZ4zqomONWspVkTmqXUAXvj5ryVrW/okdy4+dhn3hnxxMqcT/IDh1cdntsp6f0mo+q1nfp6
/PyOgxf0Tlo4MHV+n6R5HUfNbzV0aZuuM99tNvbFJuPqNZvy2JvJtzea+I8Gif9oNOaZBgnPtBnx
VpvBL7UdVLvziLoDpzaYsTFhl381c8TtV328KGhqkFcEfyQ3aXlcy5T6TZPqD5sf7zIKBd6vsj5D
dRT5tncb1bTtqFfaJj3SNuW+tsn3t0l+uFXyYy1TniAh+am2qfXaJj3dccwLPVPfnbcl1akd8Oq5
PrlQlF1R9v4Ep7EXqlApVGYyOHw6k8919l7cf98F/ffe3WfpdrPcaRxHlwVeJf/gJQbJ3wKTXTZI
H9EjhvwXH+149YHN3tPvWmF97tbylmVPbppc98Okh/rMfSdb29QrpWmHcc82Hv7YhuyP+XA2p2cu
Wj9++d5JG/MXFJgZHtPFqh5Ocughp0PZ4/8se37m2JZj6zcd+/D7aX9/M/FvH46/u/HYR3rN/bD/
3HZDZnZOXdh7zvoBaw+kpjPLfJEMf5h1KmQPUV5QZTFC/keu1DlgVru2414Ae2dsS+FLvaLgD4q8
JuS7mV0fr08av7L/tFV9Z6yKn7YqYeqqAZNXD5pEw5oBU9YMnLSi/7Tlgz9eOWZP5mrRcImyR9PQ
2ajt/Sn+y14ZYqhMVzsQ3v6EvVYgnT+37zmTJ0YXJ+y5L24x2FtkHgcHyP9YWxs44Ih1GumIxRCG
7OdEPiKlo1498CvshYgFzmsULcue2nhM3UbJj8bNbpCtbR00qX3zEU+0TXxq5a5kv7QnYDp8zGEl
5FRKfLzuEyRe4TXofUBTWc15WNgzfEmvNqn1Gwy/q+XkfzaZePf7KXe2mPD4qDWdDwfWHRbXFSrb
fOYu1tzLBXPEEMPqpkcMKHpIEDWy4Z+meUO5PcY1ajvxhSaJ9ebsHu8JFPp9rrAM4RZqZq6vON0V
OsiYOZyRz+mFfsOB4DMdyOYJ5rHhfK+RxWg5bj5TVn0BUxX9vCEp0ee9p+Ond63+i/8S1bK0tr2t
BCKgc5e998YuvKrv7ivjd/yz96Id5r9hewl7RWv5YP1ZNJYG9LEZItQZqXbs/fW3NcCEZVnTmyQ+
0zj5qV6zGuQZu1IX928xplaXCU9PXRHrF7fLQo4p+QTeR/6zRlM4L2swgRBfIjKyT/FOXz++TdK7
LRNfaD223sT9HQeseuPd0X9vmvpo37mNncd3y58XiSVFgkF2P2B4l4dhWTGkB08oRhCrC5ZVJUPJ
l/d3Tm7QatwzH4yuvSxzltssknmuVDVVn0Pgc4SSIl+okNxKsP76k4oXvfCril8T3KKHkf0a+Q9C
wZB1QwiKDi0kFivR572noRJ7Ldadekp06gbVTwLyVHpbECnnMHvvi114Xfz26+K2Ptxr3m7jR6dZ
Rgws+d/pik9DwQ1DJNsXgQ+nDPI5wV6yianXcC7JmNY06dmmqXV6TGtQFDqwYNekFmn1Wox7YsC0
hpy5KxT0hnSFcZNNzAxdFRi/yerFaonXxxxw7us2qcV7w+o3GfbKmE97Zxxd+Un+oOZjH2+R8lSL
0fUX7E1z65keoYATfQHDDAWKVSXAcQbP6zDgnOAHgdWQsrPo044pb2LuaJryzH5lI1fiC2qBYimk
er0i62ADLvjqgspjkhQUnce8qRqMpjOayuiiCx6y4lUDsihzumoE1WLFawblSJS9p8NmL8gJO2N9
JEkCNTs0VKI0vdtsh3OYvf/sueDGuC039d74eM/Z+/Tv3EYJp4AN1gdS5A/NyR9Zkf+UJn/1zkHF
QeCfeR9/LX7rKwWP5lx0aFrz5BdbjKvfZdLbnpLsrc5PGyTXeW30P1uMfmZn0XwsJhVFIX/Uq8HR
9cm6RybmTiiS8qZuTn0/5cn3x9bqPqnLmrw1nq9z9gSW91/UCI508+HPxE9o7ZQzOZlVZFMXw+Qd
DMnQVFlRecH6O3VZFVjV9fH6MW0TX2yU/Hiv+e8dNrYWyvmqoBluPcypYUP1yj6HWgjPmQlk+AM5
vkCeJ5DvDtKQ5y/J84WRmFPAHuRUZyCscBxjfZ0Wfc/5p6jMXpulmNQtJpN/XbZ2PCAqUqQdyza+
yjS/ztdP+Ky/X2ZU5ZD51S3x62L658T0B3vX5WtHwXwiOE33SUp68Jvbei+/dOChmH77b4v7NFf/
7xeCm4Lld8YvJeztu+2fsfP2BcsdWplTP7LP/Pc9PeZaXwjuuStuLtjrl0tYufSgYX05OGjHpQkk
/0GwVynxaMW7Qj/eFTv7moS9V/bZS74QDILVYb8W2o3Zoec8zAJX9t12T2/C3iItbD3yEVxKeF+4
HFPAzXGbbu61/tHY/7KXkQQPlmXGsWzzZL5+zKeENWu7BlVQJJHsw0ZHvXqgQkExENRZwAgavALd
hY/g1Bzz0ic1G/dCy/HPdB7/NlviyBD2t53yRvMpT7cf9/InO5Ly+XS/wuuhYr/I+/l8I+hiuWxG
yclXD3Wf0uq1lIc/nPzClE2zCjV/YTjXeeLgxK0JrZKfa534coeR764/vKKIdwaCxZocEX2Swcrk
dhTv1hW/ovlF3Z/L7Bs0o22zkXWapT6VurlnYWm6S/aG1GOqO6JjVe3Kn71yyph5A8bM7z56QedR
C7uOWNh1+KL/hmHzOw//pMuYeb3GLxq0+dAyLIw5xUMWNCLr1/N6TG/0QVptdG3hoUms4VbAQcsf
hKHG+GKkME3QZyKWlH4BNZC9GHIs+ejyA1yFWhiiu5TPKuFzNJkrDH11V5dZ18Rtuq7/rttil+Yc
KXcYxXmB0kPHy2/qveySQYdjErKu776MM8OlQi7k58FKJliKFdJ9gzZc1Gf7NQN3wwDmicURw/D4
haLAF5vC5Xf3X3th3LZrBu69s9eCHeFyf/Fxj1my3/z2793mxAzMjxnmuLnPgt3hf+nmcU0qzVR/
uK/L9MsHbr1y6M57Y+eAvULoS59+Ymeg/O/dPr6m347rBx74e+zy7eFyJvKFzyzZa/5wZ7eZVyTs
uXLIoTviFh84Xp7HCoYp+0XRZZ7YHSr/R/d5N4K9fTfd2XHa4SPl+VKI0zWfomWaX8Gpvj52/R19
1j/edRqjhgzWrZEtjasVdSnQJMJb6y+NMOFyAVbTeOKIOsPO2VkpTSbWaZL6aO+pH/g1DwgwcUWv
2IkvNu7/VLfkRt4yR5FW5BTdWggTQF5J0B00C93c7l3uNU1GvttwQoP3U97fnr83fKS4SMjmPys4
pK/pOvndZqNfaJv6br9ZPXe5tzEBP/zkgCgF/ZyZX/S5rAZh+0RWCftW7psFw9s8uVartLqb2DlO
M5fTTEU4yXtO+DnVreW1+Oj5duOfbZZSr3FqvcZpdRqNrf3huCc+HPdYo3GPNB77WJPkx2DnW4yo
3y2x4cKtE/1mnlEqCHCMeI1VPegO+ZBwYp3ZWcmukBNsRZfJtAUnkey5ghkNVIZJ/tXZtmayl1BX
Nil7iRMi+Yv5vLCQz3D+9NCPdyRsiRlUEDPQecmAg4uV8n3F/95VUr5QK/9bvzUX9N8bM6Tgjv4b
WN0M+Q7JquRWAmygZI928vY+K2Nit1zad/sd3ea6jONgL8YvW/18g1l+e69lF/Vcf+2AnWDvtnB5
nhxwa2Z28Q/39/okpl9OTO/0m+IX7Qz928dGZL6kIFj+UOycSxI2xMSuAnv3meVFwhGn8vn+0vJ7
ey+6ss+WC7utvTN22bYQeFji1sJZxf95oNfHl8Vvj+m5+XZ42kfLCxmMrtfLWf/sES5/uM/Sv/Xb
cXnvzX+P/3TjkfLMkv8clo4cEI/sOV5+eb99MQlFFw0owHTg0o7onFtVVUxt1YzAcAfIXk3khqJM
/lbC5Fmw1+BMsNcRcc7KSWw06clGaQ/HTf2AkxlJ5Hflzusx5qVuyW91S2s0aX2y75iLLfZ6+YKg
zmlYi7oPlxz1rs/8pOGQl95OfqnXvO68KfAeXlY4h5Ceq+9cmjWl+ZhXPhj5/DuDnh27/iNncaZc
6pEkV0SRThohOd9lchLDOl364fhJzduPe7H1+PpdJ79VVLLHIRVxQkgTvpS4Y35FzuT3tEt6oWni
4y1Sn22e9nzTsc82HVevyfinmkx4oun4J5qPewqcb5/2UpuRr/RObb548zSnmAV7zrKswgXQEXSH
fAw86cmZOYnoJjqLLqPj6D7GF6IgAiHs/dWVTg1kLzoD7aQOGAwvIiAwln84CuEje9Wv7o5ddFn8
zphBeTHx+x4Zsf2e2E9ui1t07+jdF8VvvXBQZsyA/L/1WZ8nBssMDitet6TxRjgj9P0t8Wti+mdc
9FHRbf23gLRYm4AGcML3FZc/1GfRTfEb7hi89Z9x8w4cKc/mNbdq5EW+fbL3x9cMzLhoQMYdfZfu
PVqe7y/1C8czI+UP9Fl09ZDdlw/Y/kCvRRnF5VnssTz1q11l5ff2XX7jgD3X993xj95LD5bCxhpY
2eZGfniy98ybBuyEWUZTs4+X86psqj5Okp3G8d16+UNxSy/pvfOKYYUxPbfe1G/T7V3nPNL7k8f7
fHJjtyUxAwpj+vvAXkwHHq1U472iFsByGi2no149oGA1TtkL2wvFJTs5EfYS20vYm53caFItmCmo
O6v6sBjGCnPE7B4dxrzTLvntFqPe2iutdxfn+VSnKmtwaBU/FwxxS7aOazK0VrPJD/df2DAQELgs
7mjgiCix6hFG+KLoo8XdG498pnlK/cYj607Y0m8v/ylX6sz15Ch6KBw5wqt+obhw1qYRrUY9Z72n
8ezSnJmu4kJGhUIFNS6oibqs+R1KxoJdk2ZsHjVt7fAZa0biOG3tsKnrhk5ZPxiBRFZ/NO3T0ZMX
jZq7YtLBjO2i4DcUsp0s2IuOWOx9FF1DB+FiWOwl+0uj+xZ7yd/iQCxEOOcVe+nS17pl5YcIyB18
xYQdcxhHdiknnxiw7Pq+W2P6Hrywf/pVvTbcEL/+qn5bCHX7H4jplx4zqOiOoXsOiWWKzPsk1Uv+
zF3J1r+Am31B/MGY/rnX9dmYrnzO+PyabrrVCIx57V7Tb+s+79bu8/7ZdfKhsvJCOQDHO1MsfaxD
0g2xa6/vu/3O7rMPlpU7hOOM/tVO+T93dZlxRZ+11/fb+GDnWdkR2NjPCoPfrTfK7+g664bY9bfH
bfxnl1lZxeVuMSDpwXTpyCMdUm6OXYll+f3dpmEFrupSRPPquuoxju1U/nVvj0UX99mLZl84MOeS
+D03JOy8tueqG2NXYQkdM4iNGcBfOLDg7p7LYHsV3s/pISynqzd7/RZ7FaiyQDxnN/GcJ9SD52yx
14X5mAux24rWtxn5bovRrzQb81zS2rjtnpXmSfrnOqGQUup05G3PWth2xFNtJt8ZN6sOIxwMCgom
XD9XVCil5wf3bWHmDl7WovHoR5snP9558ov9Z7d0RA57g36vYhTxvC9csNe/olPK6x0nvPrWoMcG
LuzCfOHJE4tULaALhs5Ctbyy4vCqed6wo1AjD3tFLY/X8zgjjzHz4CFbIZ8NOvgA+V9YP+fWMOfK
qsZKBqcpnIGOoDvoFLqGDqKb6KzFXsViL/T2vGQvpS54a+3c50XfwF6orEMrKTCO71C+Bnv/1ncz
2HtB/0w4nNf23XJ5v90X9Nl1Mdjb9yBWqncO2bNH/pxVDZ9ezKgBnvNls3qdhEU3D9h5Wb8DN/de
kxv+nhN4w9DconyYC73Zb/xjPaf9s/u0ur0nY7nrkANyMJLlN17vkfxgjwX3xi6t03vWAf0Hp1DG
B77cLpys1efj2+OW39Vr6fM9ZuVKPzjEUk/k5Eb5iyfjZ/6zx9IHuy99JnZWuvatX9AMM5jJBF6O
HXtf7IJ7ei1+vt/H+9iIyLlDXIEhC379yAGj/P7YCvZeNDDnor77ru2/74rYTVf33nzpgMyYwVLM
ADlmoOPvsSsd+nFR4FgjUn3ZSzwmgdwxFil7NSi0K+iZkzmu6finm6Q+3nva+369iLyBFVSYo1zK
yqEdx73TIu2Z5kl1J28Z6CpO501REAyRM71eNxfK6DvhjfcHX9196kPjlnQ/4FhdoO0UvsjJMjeu
d89Y6UjpMOGZduOfbJn0aLNhj7Ye+uwhZiMX8ovBkEP07PYvHzC3cYvEp5uNqd8q6e0t/vXeEs4l
siID7vFBzhVWnAKf4/BnCWHFTXYt8aiiS5FcZBNwmfzvHEwrqzBeAc6OgPWXoii6CtIKkpcj+8gK
MjqC7pB/zxn/NDqIblrs1dBxdJ+8kS4LZIFz/tle8qQHIjAFr8Ve8hKPX9Vhe73FX+7Svr0ndv7V
fbaQv5jrs++fI/ff1nv5TXFr7hl96LLe2y8bcCimX8ZNvdfvU76SwkfcagmrBXXBxwvSmJUZvdeI
bdaGeq6VC0LfMJCoKnAC65LNscu3fLTy8OA1eUNXpOdEvivkdTioPiU8afmOoSvyB6xwjFp6yBn4
ykP+l6E02zw5bE12n9XehBWetAXpvPqVl5H9ZuBw6NjwNelDlruHLvGMWpKJ/AzDYcIG2VJX7o9f
7Y5b5RuxMrNALdE5Z4TJVTmPVw5nhMsf6LWYes4X9dh8U98t13dfdmf8mrsSNlwauzlmkDdmAHPh
gCKwt8D4ggd99ZCP3JSpVuwlf3eMQF54sJ6EkbHjiI2SZN0T8H1C7jk/1yT1Sai7z8jjZJ4PKO6A
O0PdOXxp18ajnmow5P7Y6Q2mbxqeJx0yjxiSLpLlZTDzk81Dmw+5v9nwf7QeU7fLxDf6zWnaY1qD
ZqOfbp3ybLtx9d/qf1/7sfXf7ftA3IQGMzYMzZV2utVcJSLn8QdHLmrVLPHRD0c81WzUq/MOzcrU
8qBJmDVUhg+yjOnPL8a8zeeTB1TFQSkSkFRGUd2y6pE0DxJ53ccaPsbwOQQHrKyg84LKa4YMhcEI
aKoIBx4dsdj7JLqGDqKb6Cy6jI4T9kpk0Qv2grqQTIWYfoYayV6rP/99wch6ukvEIRQJxuHQf+7r
t/pKmNmEzKv77Zmvkj9zJv/nrJTf0mfD5fF7ruh/6O5+G3ID37o5pUgMMWooKPjCss/NaVnKZ3v0
f6eHy/O1I14Ux/tMjTeDRj4rZ2knMsM/7JY+85T94OQNVlRUI1zIG1nql7nGd/lMKBg8IvtYkRf8
4dJDSvFB/ds8/QevO3xUO6L6HCKcq4iRKQfypG8dyr8z2ONi8ZcC44evq5jhLKF0l1m+J1S+hzsu
h48Xi66jYqHKOL1SEA72vV3nXt9vJ5yIe/os3VxWvresfIv+r/Vq+frPy+EpxPTLvXRANtiba36J
ZTyvB7hq9q6VTL6gIASm77HCsmKwCHutvWa8Ad+8w1NbpL3YNLk21N0byOQUD6f5nVI+dzR/fcHs
AfObNRn5VKvkp9snvzBz8+BDzCq+OMujpxfK+9O5bVPXJnZM/aDF2BfeGvlQk/G13xvzyDvD72+W
Uqd5Yv3O4xu0Hf1O4qL+u1zrHGY6V1rAhLN25S4fNr1N8xEPtB//RIsxL47fkFgY8WT4CtVIUODZ
oATPx6u68iIqx/NutVjJ5gozuCxPIN8TzPEEs9yhLGcYIcMRIcFVkuMtzSvUMnOFdL/hgjb5NY9P
crKqBx1Bd9ApdA0dRDfRWXTZ+r9e4l7QV3qtt1x/9f+3ayB7iQNmvclMXWhKXdPa3h7OcE74xzu6
fHJ13Nar+x+4qdfqw0fL4U4XGseyyspvjV1x1YCDl/bZd2vXhc7ASUy1biXM6RGdcRmuzKM6byiy
WwxiAenSI6xuCIInrDEGeTYoe/RIkXmiwPjMoR/zSqb1FY8iYrEtF/uUEkU0AoIYYLww42RjSzPo
UEoZqSzCBI76+QiTb4i5nO5iDYnjS3n+hF85xiohhWd0nth4n16SpX+TYX5fZJxkJZj1nBNSgc46
vHIo52j5nR1nXR+/5aZ+m+7tOD67rDxbLnWaZXnaUbD9zt7Lr+q95Ya+O+7tOR+2F+wVMblXs3vO
lL2qoJD94sh+0+Txr8ELBo8pWGcMZuHBmS1TX2+aXDd22vvu4CFWK+KEomCx4BAPiyfyNzvndUx5
vV3qi82GP9k57fkBMxoe4hfl6ZuZY7mFwdw8o2jm9o97zGrVdNwL74+p3TS1Xquxz3ec+PpHi7vO
2pG2h9vmLC4sDOS7igvdpZlL9k/skfpOl+RnOyQ/2nLkY6MW98ozshwqy5sBQRIxk2ItBvZK7qKI
qXr8jiLRMW7JuKEfDx46u9+Qub2HzI0d8kmPIfO6DprfEWHI/PaD57QfOb/HsI97pi0eujF9pUvF
StjHmB64zegIuoNOoWvoILppsRdzFuk+hEAel0g6eep7vrHX2kWBbA8PAhMOW+yFFw1OZYnFdXrP
uDN2yd9jlz8R90m6VMrLkqYKucaJG3quvmRw3rVDcu7rtcSjRuB4k8dOsmkIbAlXeJzJOcLmmTzZ
RNunmj5Nw+LEFF0BwaFJXsyU1u7bJR6lhOxGA7eavK2JpQuZPgMcF+b8YY5sls8rnF+DP6BrvF7q
U457/aVcfkjMkrQ8UfEiUWMjvEScW00ke3CjEBToUI8VaSfccqnH4zuueCKeTMXv0I58uUX+9709
FpE71XHr6vaemhf83KdomE14kStUIvV7Tby/+6wHei2q12t6vhgUBAEuvZcjLhkd9eoAqKYmkJcT
LPbqnPV+GNl/nLfWQEFpZebCjuPf/2BknZZjni00d/qULEVyyrxLlF1S0OML5G0tXD5yQZfGg55q
M6ZW02EPdE2rP3lz3B5hVWFpXm7Yn647c8O5e4St6wuXritYtIddnRPYVVScURTJKwo7CktcWaG8
VYUrP1oR3yzlxfcTH207oVbLYY9PX9svi9/GmPDAMJY6Jm6V9YXIcsyvsTC8LKOxGXxm15QOTYe9
23xMw+aJ7zZPeqt58qvNU55vmfJ0y9Q6rZPrtE8hnwc26Vev+8gPPt01x2cUcKbfpzo9Wk6+vr35
qGfeH16r04SGn2YtIrt8s6Au2Xgd3SdvXMk6ZS+Ec16xV6P705+yveRVFYvG1kt44SPz93lnZ4Vm
ZpfOOiDwkWOqwgdEjyPw2TW9NsckOOBngr2sEQphcrceHUN2Id4N9iKEOD+Yaf3JewV7kS0guFAR
+IZ6ESB05AH3LNedMDDA+3GhyZPXMzEwML8oGW5SiV854uOKOYcp5UtqvqS4sfIxWPJNH9ps3Xsj
JaA0Rg77lAinhjmGBXvD/pygwhVIoZ3h8rvjPoXDf1WvDY93n5gbPOnGfKHwiujDrDR3a/r0ne7p
++XJG7L0UDHP+VglwBvF1WrdW8FeePUSmSuJcGR0nJhfXTJYhd3n395sxFvtxr3SYnT9TQVzxFB2
sclGcBnr8/ldrOrz6IVb81ZMWTukU8rLrcfUbjHqiVZJ9eAYD1vSd2XeqqKjbu8RjzOSXxDILApm
uCLZ3tIcT0m2szh7t2/znJ2T+83q3iqpwQejnn1vdK13kx9unPjU4r2pGe4NYrBIMnyC6FcE3hRk
nREsfx4zn8xKjFt17Wd29xjbviWoO6Zhs8T3wN6WSS+3TKnfKqVO65Qn2yU/3mb4E7FjX+kx6s0+
oxovWDPZyWSTf0CEHoRzN+R93Gzk023SXkLX9rE70E1V0OA2o+PoPoQAUUAgEMv5yF5QCIYO9hM9
B5NhS31KmOUErF7gjLBa0Gkch5frxPjwEjiVF/7XFf0OxgxgYgbk3xO33CWbQb4AhIcDQ804CIwA
1xOFw/YyKhlEQ3TTdzBBUUwQqBr14mgtV069bi0QBmIYJBGjT671Wds7SKIOopos2VlDkv2c5uVU
jnyOy4dxOaq2ZhwyAaEoEJhMCmoATS1TvAGmIGxqe93qdjj88Rtj4g9fEr/nrm6z95eUH1JPONQQ
qAvvC7MMFzwilJ50C2ppQCL7hipBNkg+AKajXh2APsLgUPaSV7vJDtRk2iLmV4Lw+QIjt3Na8w7j
32qb+tLYFXFicZYiuANkHtJ1wVAVUzIUn+bJk9Onr09MmNGm+bBnPxz6VEtyu/j5JiOe+WBI/b5T
241eED99Y9KCXZM+2T5+4srBwz7uHD/hww6jnuucVL9DYt3mwx//8KPHOk54dezmuI3OBT69SNE5
VfdKcpHEFWq8N8CrAS6geE2Z1WUV07fPF3LkmxnTNo6btGnstM3jp20eO31L0vStI2dtHfzx1n5z
tvaZu7X3nHVxSzYNXbx65KfrJmdl74APiB6Jgl8szUlb3rt18gvtx72JrhWaeYLE62STGtJx4p2h
CgU6TNhL3k4/f95zpuwVJT3Ee8OcG+z1K2G4nQ7tqKphleooFh2yN8/v93Ka6YTDZpBd7Q8H/v23
IZkxg7kLBxfdEbdqr0dTFMa6eUD24MdESLiEiUAiNhAplh0nPjkccnJz26KoNUdWvEhMiYfFNvJY
5WicWOKXyqy/HTRhuhlFF4UICZKOWkBda8YN8yLYC50mVVv3YMmInnIiFJ5jQpJX8RequpahHFuq
l1/XZ3PMwNzLBmbcF7d4T1n5XvP7PPO4XzM4TCgKL4gMCkEbQlyRybsYNeSUyQ6UdNSrAyh7MYVh
arPYq0EUZLFDtuOTdV116EVTN6e2SHylXdrL3ca9ncmsUxSPhLnOL4e1iEn+7Ja8Nwqx+cLebUXr
J6wdHjetSbukF1qMqdVubN1uU15qn/xi+6TX2495vfXwV1sNeaX1Ry93HPVi95TnuyXV6jz6kQ4j
Huya+NTg6Q0/3jhor2clF3GIqibwCs+7FbHQkAoN3oXxNrmI4o+IbFBQZI/sZMNF3BHy0nVBIMdp
5jvNXJeZ5Qoc9AT2eAM7fYGtTGCrFNzDSDt87B5JzAlofmiIxkqaxqEL6AgmI3QKXXPoBbqOFZNk
vWXlh3dmsTcMgUAs5x170W1QCNQt5uDTChBEgfZ5kXZM0kw4yaVCfoTLUwUfMcKqwRoRRiveI3/z
4OiMCwbkXjwg+4a4dR9naGzJCVCUshdrWhCYqJcM6oahVJRap55OWes0SwUxWcJygmb0LHgrKqzF
TEwGZX7pGClBJXuj+TQFcUYqAV1hV3m6c5oSRvn0QutLekwEFTYcR4wQy7IBmeUZt09SPZ+Vj8z8
6so+Wy8YkH1F/wPPjFi3O1K+L/BDfvgrjx7yYyLXJZ7zsozHkNmgP7dU8WPd6+SN6uU5E/aylL3W
1EaWvlBiwl4GKz/Nbbh2+jd2mfj++0OfbDXquSlrBsshNxwfLAR4lsOkioUGFvSMqPhUkQmJ7ohj
H7Nx9raRCdMbthtdq8XIh5uNfqRZ0hMtk+q1SX62Q8pLHZNf6pT0XOfEuq0G/jM2sfb4hc22ZaaK
gZ0lx9y64fO4nYoUFnhdEtmA5gtpHl3wqIyosgFdKJGEoKgqHsHpUx2M4fBrHgVOvO4XdS8Jhofs
Emw4EBTdoZkuw/SaAQbTDcsWcZyLPMg1HdNWD0ZHGn70VNdJDdE1dNDQdHTWZq9P08hCqYK9xK2r
ENZPUSPZS+5a2ewFtfDTqR51q2V+htMFn84WgcPoLyPAJRW90ACz+KB4otHHOX/ru/WiXluv67Ox
+xo/jJhbI/9sBCMJ9oJUxKSLpGTIC+RERZgp8BN0pXMkIbBlgXGKvPZDtk6GU8gRrxgRsYQXjsI3
BvMZze/XWWKE5QhSSCHkmRZHUhTTes8GcdSLONiLJUDFmycoWYcB5hj49lml5Y0Wea/ot/OCvnvR
5k6fHNyvfZFtHPeEjjFYPpM/+5etNmJmVwOsIyy4VVWudvecJQFaa7GX+DX/Za/gh8UTeUGI8JnG
gQlbB78z8NG2ic91HP368l2zCoWM8FFF1vyYmzRFDRhhniM3KXyy5FH9/rCbLc11hXYdYpdsLpg8
Y1t8yurOgz9pFj+90YAZzUct7DxxdbxlaefnK+uZ4B7OOMQKWTzv1ATJVMgowyECJeDF6xJDntsJ
IhbhWOyQ/Rl0zBRksBjWQ1ZLHCvxPkHwkH8IwLqM4zHSPDx+Tvb5WSiYoPJ+0S2YnuAJ0SVlLt4y
sePI19olPf/u4McnbR+aoe9HB9HNINa9MLqUvRh6wt4wZa+1dPoF1Ez2YsEAPgR4L1aq6Dy6hxTY
N01Fd8l9V48admolsLqgRFj2It2tlkzd6aszcNlV3VZc12/rY8kHR+wL5ppHLQZiDQpvHCNKysSk
ADNolUm+dEfASKuCYvDQQkJgyl5cAicQLhCjcbzmkBS3ypsGGyZ3PkSvrObzOnlvFoVYtys4rJ8N
yUUtMJbBWPxg7iDTh/WVBc6CwAJsuxmAnsOuuvTI9H3+h4ZtuTJhx1XxW//Rb9nCgx4PTLbMw+8j
DoeKnqseNegJHHcrIV3iMG2ZMPnkO55f1oa/BIS91j8HWOwt8Wg6o4K9WI8Q9oIJXIjPD2buVJf1
mfthx3EvNR1St9Podz/emJLL7+SMPF4sgkEjf8qNlTNYgFlVVzEMfpXxyh6/jKW+9e8ZZi4fyOZD
uXykgA0XekyXS/M6FZ78c7oa9CoBTg0rZljUTPJxpcgwEsdJKvlImlVg3lWFOGqCzPh4r6JiyWME
lBLZo5VIxZqXbAhqoE5MJoqpSSWqdESRTojKCV494pYMj654wowzkp9t7J67O63tqFcbD6nVafzL
fec13q2vQNfYIOgunWKvF91HqyCKCvZi8XWesZfc9QVviVpYd33ISlKUTFXx+n1S6FihVFwgF8uh
I6KnoFT2qCwWXEf2MyUNRy27tfuCG/pvjem68uXpObvC/8kxjxbpEQwwTCL0rJQtJM+NBPIKF1nK
ktv69L+jyL3BCttrfRoBrxveMmWvqDpkxaUJWDtpAY4zRZeq5ElqIU9esiMXkpvSgsMUHcTNJl/V
kzUwqIt6oY7WW5+EvQzHqpEjDl7NlcKZ4e+aTd1xSZdlsL13J6x5cfD8HDkiq4TqmuQV/E5DlyU9
4NUiTv1IHqsFgyZmDTJxcJ7qxl5rYS9AgeHgkHWvZXsxfIqfU2XNJbj9xS7m85ylGRN7pLzXOfHN
5sOebz3qlRGfdNma9wkbOiiFMjklk1dyWa3Arzk9qsMtOz2a228yIAZr+nnDzRtFjJHvM3KdRjaW
qQ7T5QzyearkMEMOM5KvGvmKVGTwzoC/0ChyBPN8Eac/yMFjRmAMxmcUONVMoaTIqef4TZ9b8kkG
sfuhoC4ILk4rYHXI2OlXfX5F9MuqT9G8qiqWhfIUpyPidJRlL8/7ePCiju3GvfbhR3U7jXm959iG
K7KnsF/koiIn79IUXedE4iTLdN2rQRQQiLUuO588Z3SGeoboM+GtdbeJgkYo66y1q0ZuLAmYUhWO
L3UY303fo9Qa+Om13RbdMHDLZbHLX56RMSojsutoeeGxcqcSFPzuI0zucT7PEN2Wc0v+vAJHBNg8
YvbIDSpyjwpnwcBTgRUVL8yplQHtISMEU4xEcBU56YWYdGWZpIC3Ho18UwHqYt2LU6gOhAcnfRzv
Cx7LCHw1PcN8b9qhG/usj+m977oB+x/oNmX2Hpc7/AXcaT8cYxAAcwtGlDrw5DNJsn7GlELaWZ2o
S0HHCBMg2kneMbK+D0OAP6mqKpx/Bs5IwOdVitbvWPbRpLj2Sa83HvVUs9G1O6Q+Ezfl1bGr2q/K
Hr3TN31V5ritrk+WHZ6ywbFgZe7HKwvnzk+fsjB7+vKCj60wc1khCUsKP15SOHtJwdzFhfMWFyxY
XLAQxyUF85BC0slZkmdZwexlBXOtgMjH9FoSrPTl+fNW5CHMXZE/e1H2lPkZE1cWzQFF1zuWLDk0
c4tz0crMqdt9c1dmp8Bpj536eru0Z5qNearxqCfaJ7360eRea3cuQ3fQKXQNHUQ30Vl0GaOD7kMI
EAUEArFYib+MmsneMwJZtTKqJpXywR+2eT/r8kn2XT3nX9Fz+a2jD8R0X3FP4r7GK9ik9NKN4pfO
4m9F86iLN3LVI+nGiSoKh42T+43v95o/HDC+TddPZmvHUB0CIvvlzxbmh7suynmkP6aYldck7L1s
cN7VvTYMXpG93697w58VykGvFoKT7+NEXTfp2NNgqQX5yxWEcwU8b81BooglA9buwWCQYZhdGVsG
ze3cftIrLVPqth1fr+Ok2h0mPNkq8aGGA+9qNeapjmnPNfqoVsvE5xqPrNdy7EvNxr3QfvqbTcc+
T0LaC1V0bIcqxr3QfNxLH4yq1yLpuQ+H1Wo/9jk0Bk1qPeahTuOf7DKxdvtx9Vql1O046ZXBczqj
C+gIuoNOkVdoRBHdRGcruv27EWUveU2vRIRhNTixxBH4bh37bZv5uTfHLr2015qrhh68ZPDBi/vv
u6zvruvit90ct+GOXqv/Hrvytvi1t/ZdX0UBhd8Rv/bvfVYjIIJwWzwS198av/HGXmuvi11zQ59N
Nw/Yfm3cpku7rb1pwO7nxx7cpX3jDR93apEirMMD5IGQn4G6E/ZWeB/nJnsBqqBQcRyh64ZhCEE2
09wz5/C43jObNRlR78OPHms+5vEOabW7TarffCTZJr/lqNptUp75cNjjTUY98e7QBxoOf7jZmCeb
jXm8yo5Poop3hj4Au/rB8MdbpzzTfHTtVkm1m456FE1Cw1qOfrzx0MeaD68XN6PZJwfHZRt7pACL
jqA7dtdsHp4Rouwl+l0iiUGWZX2iTzvuOlq+hvu2/aKiuxNWX95z9UW9t108MP2yYQUXDsyJic+I
6Z9z5Qh3zICcqgvku8V++67ou/eyfgcuTkgnf7U1oIB8Zz+g4OLBBUghb2j333Fjj+X/jF/eYbl/
jVKeF/raY5YUippLVKVAWFB0hiE3z9A1yl4oyTnKXgCqCUDLYa84juM1nj0qFZa49kt71hYumbEr
cfCCDl3GvdE++WUcO6a83ntyoz5Tm/Sc0BDH7uPe7TnunV4TqjZ0H/9Ot/Hvxk1r0mNiQxx7TWnU
PvX1TqeaNHRBh1m7EtcVLDkg7SkqdnFHJHQBHUF30Cnau4quniHsCxE5f9kblFjN7zUUVZAD+Vyw
KPTtAf2Hifu0t1K2PDzg07/1WH5N3KZrBu63/pUuPaZfhkWzvCoKYO9VfXdf3XcnOGyxF4mOmIEu
HC8YkHv1gPTrY9f/vfv8huM2zzysZQa+c0e+zuV1t2p4ZNUryjxZKMoCx4tYQRP20jvM5yR7qc+M
iKZp5D99LAiqmeVVnOZRZyCco3O5QXd2IDczlOE8UVhQkrtf2FcQLMyUsg/6DudLhQVSoVMuLBAy
CsTDVRTyxIwipRB15cqFB/yHM6TsvGDhPnFfXmkumpQdzEDz8gLuPI1zmWG3fjTXQz5foX1Bp9A1
dBDdRGetTp8BouwlK8OgJjHuooAmhwJBh9PLCKZ57DuH9sU+4bOpu9l203e/MGL1wwNX3tFnxTU9
l13ZfdkpF7dKjnfEr74nbjkCPGd4yzf023Z9v93X9tt7fb+dN8ZtqjVqd4uPs8Zs9m/zlfJl30rB
kjyn0yNI5BFGIMBJMsOxoshrEnkOCeqe0+wlt6wsDkM7qaaSo2b6lIhc+kWeKG7M2bO5aMcW9+bV
BSvXFH66zbt5be46fxnnCwluhWU1lZVURpBFXRd1tcqC7hdkv6wymupUWE9I8B3h1uSu2+LbjCat
LVi51bV5a+GOzVl7CgVRK/mCkSKKatrdAShvaWfPCLQQGjlP2Qu1VnXFz5A9qcMGec5kcExA4AUf
I4sarwR8ekmReTwn8MWh4FcHIj8eLvk+Uz+Wq5XlqWVVcczWjh3WvzxgfLvf+H6P+a9dgf/sMst3
BspxTC8pzzC/LQx9xZee5I2wy+Nmfc6QSXb/FzVJ0cgg8hyjoAuKSP6N+Bxnr2IBvYJpgqWCggK8
5PcreUwwc9muSd0T32sz/PkOyS+0HFW32ch67ZNejZvQ9BC/0Wtmc7pDMXyqyirkflDF6xNVEngR
VSiYKMgX+Q53IPugsLHXxKZoTIsR9dCwTkkvtPvo+dgx763cOYkNZjJKHrpQ0Re4EpbzTHta0e3f
jSh7ydv8Xs5nBFRV4XmfyxT8EdEfZLzFAmv6PSGRD2pEuH5FdquqU9c9uiJaf79kvcN89o+S9Y6k
9TEDCX6FBMb6i0xBNVhRgnXledgV0cRcQ/6uwa/pImYflvNCjUBXTeINWVB4eM7nNnt9Ph+U0rbA
iGAgeMnNqIcCJ7K2Zk/pnfJa66GPd0iq1Sbx8XYpdVsMr9t7XMMiY6dUXCCqDol36TxjCqIuK+Rf
1qss6GibgEWsi9ccYklBobkTzUBj2ifXRcM6JtZqO/Tx+KTXtmVNCR7P4tRDguRGR9AddApdoxF0
1ur0GSDKXkAQFVaC70Meulrf9FmfhpN/1hWwFKZ/2kJee/Rrkk8XfHCz1YqHqFVx5GXy/UNFY+g2
CIIrxJMQENym4AXDrW8YFPpve+RrB/JFO0svqQgWb3/hrhV5FQTxcxkya4T8YiBn++EFwyd16PjR
y73S3ukz8f2Oo1/vkdx4yopRhxw7JYPTVU3GLOjRTRbeLScp3ioKGAvUpnh1iVWwiBVN7qBj56SV
o9CYTqNeR8Pi0t7pNPTlEZM67Di0QDFyzBDZOKKiL38MUfYC5AM9K5AXLeCvETdUNMn76NYOmuSN
SPL0XOIVjtP85L1lwjSds/4D4KwfQWAoBChqWFtAhARHmC8stkKYd9AdIdBmtMH6P4AIjoS9Mkz3
L1O3MntpqOj3OQuG8Qmi1ycW5Pj2ZPo35ynb8/Xt6eLWfe5N+Xw6o2ERqsA9AqNCXHGZVlbV7C3V
y4JcMaZ2InVV8ev+XCETjckUtqJhaF4WGundwwgFouhlmTO2sb+GKHsBovr0XSjyhiP5J/cgI0X8
cokVIvgJqhClh32W3bJMXpO0LqQLlao4ErKRtpD9lv4bLB6SN8nAcGp4/UrQsr0cukDNr3WVRVrr
5VAaaJkWdckrYvT3uQpRM5QjslgsK6YW1OUIx5hOn1nIR9xKKS9HBNmANovky1heCfCqSXbiJe8t
VVFA4eTTXEGFp0ZEDUfNFCU0o4xHk9AwNE8Nc0ZAV2RTEYrReGIezgai7AVAS/KGI9T6lDtqfQlo
BVAXtheshk3TJLcpOshLi9Z7uVUUKihqfYFIAlEHGqhf/ZNA0jGC1ruWCFaEXE4Ze4q3FBWvc5Jg
5TlXIeq68jnnK/V7TPhCjMC7WKdHcAoG6xU8Xs7jZ32CwOmyZJJbd4zg94At5LOhqgkonPd7FJEh
dwqxMBE5L+dDM9AYNAkNczNOTuB5NNVj8r5SQ/4cXajoyx9DlL0ElCfWHR2yoKWBcgMEICth0Us8
WCG/hM+DH0veJq9k2aoikFZZpP0pV0mTcAoUhSnGJGJ990usLqUltatWHnKtZcmpMQdqDnv9niDn
L5GEiKqEVC2g6ppMbmyRL30BciOXZ8lGRAqjql5V9Vv9taVx1oOgaH5F82LmFyWG51l6exyNIacU
8rBa0wxNjsh8hPeVMO5glL2n4w+x13qb/1QgNLZoQHxXmySmgFUo+XMcct/I+quhqgu2ZqBhNJxK
sTJUUNdmL/GWLa8YwbbVlOoVbLfUl7CXutbWz3MWsHW8qWkluhYiIqIv93Osz+VWBUWXDFVGIF9l
CDJDvg9RfdaERQVbBUcUrvp41cuhOhl01awGGGgMmmQ9dSefW8iiYqghQy0RuYpPxP84ouwlOKXx
pxSdGCjiSyOC8QCZLWtM1sP0G31IH4NRRcEiLSGeTVcMv8VVQldrAUy9a9JgyyZbMw58Nst3sAL5
e61TFvvUv3xULKQtB/ucZi8aT7rISoqX552S4NAVf0iXg6rOuljJh/VnADZZVgxOEv2Sn8HBuhFA
5XbWjygctXhlP/wxUTVQtSIG0Aw0JqzoEU02FD8aiabKsldRyd8VnS35R9lLUJm6looTLafsBSvI
l7pkMRz2qBG3WuZTyD9RnUa5sxgqs7cSdVm465S9UBqrzYSi1m1q8jc9lr9A2EspTc6e8rQt9tpd
O/dtr8wJso+XnaLsVDWPpmDN6ZE5n8IJpmQacgDkEQWD7F8gK4KmgD1W33+LgX/kSAo3SEWY4lmS
gPVvAM1AY/Cb/EWKSBqJpkqyU5DQbM/Zkv/5xV7dgs/ns5YicMDIi6aCQD4iVRTFMAyWgC6TyIrF
5/OUlJWyPMewvGqGrAlfkzQT6xqO41AULQQRiA8pFdX8buBChsHaTMXldLFE37wBb+GAeb1e0zRD
gSDL+FRFwlrO63GVlRaj/cgcDIY9Xj/xlWUVB6/fh2t0aK8ss5zAsnwwFKHWuxJIv86W6vx1QO/8
JaUhhvWIEmvosiLzEBF5wHtq+qMBYqHB7XaXlZWxMMOiGDQDkJ4kEMdXkYjYcYScGR95/4nsbyyK
pm5AyJg6ka5iQhRFaADy8yxHBgszokJe0oCoccQIwln2MX5VMzAWPh8TDhejGSJP/CYwnNoDy6Gr
CLgQZVoDLUFtqAYCVKkAxD0eD3SxuLj4N/SKlkAjKJDqf41lL/gQCARAOUgc0kGHw+EwJAXxQVI4
Ig/9dAuDSsbVeiEG4oN0EKcpiIdCIeRHOoqi6SiqpKSE1vL7Aa1Ce3BELSgEuoICK85JEkausLAQ
FaEKOq6o2u/3B4NBXEKbhNbiWnSntLQU7S8qKkIKMpBJyI9pqGYCgqLaT4VDR/A3tBzjBYkhJ2SI
cYeoIWdcTnmCCBU+jaNkRJAZcXAYssUpSBV5UBQSccRZ/ETtSIcK4RLkQRsgcwwoVSecsir/BaAQ
VIEScITa4CdaRVPotSgfg4gjOvUb/aKNoRG0gep/jWUvxgzSQQSSgpSpvCBxjC5kDalBBMhD0yE7
AIRHIiK4ig4ShhyFID+AFGTGT4gYl0OIZwRci0IoY/ETQwgloz8jkUh+fj4ahjxU1dBO8Jnq07Fj
x6i6oF5chRTEAeRHy5GCsa/B7EVP0Ud0FvKH2AEqSUuovwCMNQRFOYycAOTjdDqRSH+iBAwxxAgg
P4SMKhCxBtlANggT2Wg5kDBGBEAKGoDMiONCnKK6gVowOrSoXwQID72i5eByRFAyjrgcKYigOtSL
8m19+EWgKDuCllD9r7HshTggU0gWYw8ZWZIkQwuThSPkDhEgHQOJCKQGISIOp4sOJ4bH5XIhgnQI
GnlQGkaCXoIITp0R6NhT0cPMHj9+HAVSZcJZHFE16sJIw7TiJyrCERxG+5FIdQ48xyWUrmgzEnEK
RSFS0e0aBwgHJgvdRMeh5TQF/SUy/SUgJ6SUk5ODzJASpQpkhUHEmGIIMHZIRx6cokOPOArEcIDz
+ElJi4oQQR4MGcqE8GntAOJUl5ANwkelv9EeaBFGFnqIqtEGXIvSiC6e8qeQDqBe2hhaxc+B/HYE
Oan+11j2QrKQL8bpnXfeufDCCy+44IIYC+PHj4f4kAGCQx4IlGbGTwhl9uzZV111FbJdcsklV1xx
xeuvvw6BQl4YBow6Zkek0HJQ5hkhMTERFaGovXv3omSk0EKuvPLK22+/HcYBteMs2owmYTjRJKCg
oOC6665D/quvvprWe8stt+zevRt50CTQGE1CZtqLGgmoOAhAB4UCQrv44ouJTH8J9erVe/XVVxcs
WEAnR8gHJEQhdCqndhgyt4vC8ZNPPkE6nUkBjDgddByhP4MGDbr22mtpfmDYsGEYFwifMtbhcCDx
0ksvpbX/HHTgcKxTp07z5s2hYCgT1aFrmJfRMIwdWoi6EMEp2uufA+2xIzWfvVQi6OcLL7xgiT0G
Qw4a9OvXDzKiEznEhzyQILJhSDD1rlq1ivIccsfx+eefxymURhmCPK+99hrSL7/8clLimSAhIYFy
EgXGxcUhpbJGDh8+HKOCNqMWtAozBRqGeps2bUoz0BrRtilTptBT1ImgrYImkT7XRGCkYBttIWAQ
afzXcNFFF9nHtWvXfvXVV5An9VkgYfAN0sMp8A15IE+MAgYdZzGnY2gQoesUKAlkC/Tv35+qBC65
7LLLBg8eDM1BNuQBjhw5glNIx/HXQC+s+GEpFWrEEKN3mFPsiQN1QUNor3+O84u9EApEA6G88cYb
GHU6nMCIESMwfhABjpjUQV0AcTqhYrwpb6mWvPLKK8gDQVPZIYNte88Uo0ePxtigKFAOZd5///0w
7xXnLG3bvHkzVRfqp0GHME/j1A033EDzALAqVM9whFON3kHJqNdAe13zQKda9N3mLQboNziMCZpG
br31VhwXL15MuQFloIMI2dIMANWK5cuXIw8kj9HBGFGjTTPjOHDgQOiP7bthnkWTkI65HuOI0bRV
6xdxzTXX0AimCapayA9/CgSGikKjMHZQP0wxGEf8tDr9C6DtoZGaz14MGAgJ4WKqg8goVTDqQ4YM
QToGid4IsVUfbIEXBPbaVg7Hl156iY4THTAMKi0NUykynBE++ugj6A3lG+pasWIFHXU0DGcRefbZ
Z+HUoSVoM1oIn/mee+6hjaHZMPyHDh1CG6BedAKiGgnUYPZigCCWyrYLpgxHKtWfA6cqZ7755ptz
c3MhLhADYkeBmPvgOdOcdAg2bNhA+QPZIgNoDJHiiEsQAXtpNnAPowD9QTnUG8dVGFBaDq3958BZ
aJ3dZkpgAI40hg+1QBVBYxSII8bd6vQvAC2xIzWfvegh7ScYSAVHpYbBgNAxTjhCZMiAGZdaM8Qx
I9JslJ+UvUgHUCbiYK9dFCJwq5BODSb1e1EsUlAaRgIDTIcHF6IWjDS9RYk4ZgroAS0ER8pSMBwZ
cDmu7dKli2116dSDs0in5EcEZdIjqkaliFMfDM0AUDX0HplRKUAnDhwxNUAp6TRP9Q/VwbHEJTiF
PPhZrYAOorXovr3yBxk2btyIdAD9xRGjg45ADjt37oSrRZck1AhDvDNmzMBZmo2WSdOp5IF169Yh
kZIHgBxQFFIgH0zcdJljW3t4zrQcHFEm8lNu26WtWbOG0p5mg2DRKowdHUQcaQT5J0+ejAyoDpkR
QVF0OH4RNA+NoGSq/+cjeyFcyJTmQRzDhpw0cfXq1ZgdKZAZXKWFAMiPuM1eHDGiQ4cOhcRRAmUL
jsgGDmAkcAl+4hKkgG9QCKgC8mCOx6QLwmzZsqVevXpU1egRZWZnZ2M4bQfevr/y9NNPo0AoK0pA
4SiT0g8RJFJrjHQUjiNSKHAJpg9cQlUEDUNOgLYNJdB2IoILkQHTCgqsbkAXIAE6n1KxLFy4kPYX
Q4ZOUc8IfUTOsrIyyIoyCrj22mvfffdd5EQG9JQWiHQUBdAI/CD4wDhLpUFHE/lRLCIJCQngG4wn
zX+m7MUooFWIz5s3z/aiaS/atm0LseMUeoFBQUfoJb8IFGhHkI3q/3nH3mHDhlHFxVmAigyqABFD
0GAv8tDZESJu0KABLQRAmYjb7KUDANuLMQYwACAJItQAIj8KxCUYFVodKkI6UjDeALLhEowoCrEH
FRGUD/f4vvvuw0+sjnC87bbbcIROYApAIbgWZSKOEjAjoEbUi5JpsYjgFKpGjSgf1cHy4yr8RDrO
ogtUO5GIbJhE6HyPRLQZpSFerUAFi4nSdl+BvXv3oiPoLzpVUlKCLqA7FOjCggULkBN8o+P4yiuv
IAMdR1omEjGItkps2rQJE2tpaSnEAjpRISCO8iGfUaNG0WwUZ8peHNEFKvk333yTTtMUjz32GD1F
6Y1u0rH4ReByO4LMVP/PO/YOGjQI6RgbnALoeCMFTMARRg95bBE/88wztBAAZSJe2XMGhg8fDr2n
xIAG0NJwpN4yvRAGDWNMuQ3lgIlAZmSgbWjYsCH1ymxzAYOMY+U7JfCxcRUagEtQCwYb5aBAjD3U
C4Wj8aA0rQVlQnugB8iMiqhyIwPOogGIIBFHnMXlOBYVFSFCu4A8lvCqEdBCyBNCAGlhfukSA7Me
uonuoOXoLPqOCLqJLkAClL0UkGrnzp1RCB0OWibSIVsqXgj/008/he2FSNF9yBaF4IhyqFThrGEi
QDbqB50peyloCsw49edpLzC5oGs4i+ahItSLXtD8PwcutyMomer/+eg5Q+IgAx1RaC1GnR5BDAwk
8tDxwJi9+uqrtBAAZSJus5fO6xgPDACAsYf0AWTDwFMlQBy14CcyoFJUQVpmGWSkUELm5eVhRFEj
vddCC7fvuyAC21tQUIBCoAEoFiqLkkF+FIICaeE4UhojD3QOKciDnPgJhQDQfswytNe4Ch1BZ1EI
LsRVmF+gvriKNq9aAU1Fw2zbS2Wybds2tJyI2xIy7Q5SqANcp04diBEMAeGRH5khKzqOtEwkIgMV
NbBy5Uo6BVMBUrnRS1A+pnuajZZ2puxFhMYh+crsRX4ccTktB33EuETZWwEqFBxPY+/YsWPBNKRj
eOhgQ3DQY0gckblz515//fWUPOAnrqWFACgT8cq2F9LHuhcXYuCRASOBMvETOTESYJHb7UZdTqcT
9IBOQEWgDchAxwzahvxIgTNP5wLqKtu3Z+hkv27dOrQTmWEkUTIaiUtQFNqMeunbCEjBERmQQvUY
KagalEZ+nEJ7UCmAZhQWFiKbw+FAd8BntIe2FplxFZFddQIaiRZS60fFctNNN9GXMdAdOjpU5vn5
+atWrYKfTGlG1yNwViENWg4dRADpGEQ6jpA2Bh1FIRuEgzwAxIs4hAOBjBw5ko4LWIpL/gfbizwo
B+yt7DnjKswyyIZ0ABE6t9JLfg5ksCMomer/ecdeAKwA8agcMReCOXS8MZA2cyieffZZWgiAMhG3
2UsZjjGwxwOF0JkVoM8k8BNnMfbdu3dHCRhCcAn2E+OECB1jMAfxt956i16IuQNHaCpt0ocffgjm
Iz9qh3ohM82Pa+lLlCgNqobCURRYCuqCz5gvwE86JdEbUciGzIiXlZXB2lN3+tixY5TJuBwahlpw
CZFddQJ6h/5SmUCYGDjEQWZEqIgoMBw4S4cGZymjnnrqKUiDPjGi40jLxCnkpJmByq9S2QpgTxYU
qA7FInKm7EUcR3Rhzpw5tqrQS3r06IGzKAFix5GOAmnfLwEF2hHkpPp/3rGXjgFQeewrA8NGVQR4
4403aCEAykTcZi8dACiNHaHptmuEo41Ro0ZhYEA/6hqBSyiQzriUV3v37r3lllto2+wWYrBhIamR
wfCDZtQrBkUx6tBLuxa0we4ggLU0zoKrqA45UT4uQTmobunSpbaC4ioIYffu3XR2gA79xtz/VwFt
RsfRWnu8qMBpf5FIp9HKoCnNmjUDaSFeFIJ5k44jLRNncTktAaXRAisPGS0BhUMZMBw4RWtHzjNl
L1Y9a9eu7devHz0LU4ECkRl6Qt/QRAtRCG0hYDXwF2CfQgQlU/2Psvd0VBF7MTygB4aKchhFgSqI
4CccY4ziE088gZyVW9WoUSNkQ73IiQuRGc2AR0eVEipCs1Gvm4I627Ql2dnZuBxATuQH7rzzTisX
abytoIhgeY+zmBdoN6sV0P5fZC+GyR7TyqDyp27zCy+8cPDgQUxeEC/KsXuHU7iWXo7SaIF/kL0U
VP4AVTNaBQqxxwiZaVEtW7aEi4R5E+VA+HSMfkP+qMuOIBvV/6jn/Cd5zrgc9KClIQIyAyAkXXAm
JyfTC6kXh6LQKow6vbmKcUU2KDHGGBYVGkN9Lbs6ZLaVz1YgAPlhcqG+cLORASpVuYP4aWfu27cv
NAm1WMKrRvg1z9lqNRlW9B1yQLo9xPQsjkiHR7Nnzx5IgEqelomzyGznPyuesz0WuBCZ7ctpgysD
M8tjjz0Gl4c6RCiBrllQGoaYtvDnwFk7gqqp/kfvWv1Jd62QgbrKyIA4qEttLyJYfNIpgNZL9YDq
yj333EMz0JFGsbgWEYw30pEBqFevHipC7TDLqOjw4cNIRFG33nprfHx8aWkpCDBu3DhMIlSlFi1a
RKcATAq1a9emFaFTSIQjYAmvGoFKDI20+UDvWtF0CBAdASCBjIyMnTt3JiUlIQ+VJMXdd9+Ngabj
SMtEIuGuNY6g61m5awWghdQMUEB/0AxcgnTUgp80vWPHjliKo3CMKVZGKIreXERpgNXAX4B9ChFU
TfX/vGPvX/XECHqGKlACTUcKjgA489Zbb2HU6SRNjSGmZ5RPm4EqUAgYizJxRGk0jgJxFrjtttvw
E51C+VBipNN3ToB//OMfuLaoqOjee++lKcuWLfvss8+QGfViUsDxjjvugIcJfUU22s1qBUgMsgIB
bJKAEtu2baMSpvKETAD0FHxGZkxPNDM4A8Ei/5gxY+g40jJxCoNoq8TKX39iBJH+zidGAJ0yMBHb
E40NzKRvv/12amoqTC7KR1PpEYOFQlAaaoROolKrgb8Amo1GUDXV//OOvX/V2xpIRAkYHuTET7Cd
6t+kSZOgFtAAe25G+VRRcKTTOVqFS1A+9RpwxGCjNLh8wO23347u4Ce6cPToUZyFn4xeU61CvXv3
7r3hhhvwE4loTF5eHhJRAoALofd0Zvnyyy8LCgpQTrUCVXEqDdCDznFYUCARgFgAdARiQU70AnEI
6t1330U2gDLKvn9By0QK4a41jpDwWXlbgx6RZ86cOfZZEBJFoT1UyEhEHIlU/kjEWURQO8rB0CBu
NfAXgNLsCMqh+n/esfevelPSqo28OkvZixQM5KFDh2D6qO9KV19QgvHjxyNiAxU98MADKBbqRe0D
SkYEJdMMf/vb31A1akGlSEdLYIE//PBDXAgnPD09HbXQnHT5jTzIDKDXKASloWTaMDSJyK46gQrW
tr3oFID5CO2ncqByRjYMJY40Do7ZS3qMF4wwhE8HEaCJdByBs/KmJLW30BzMBZg9cRbNgHgB5EF+
a/wrdAlAy3GEGqAiRJCBqg0ivwhca0eQjer/+eg5Q2R0mHEWcRADOWli1X2lgLFBTqQjG07hiDVP
165drUaRtS61KiNGjIDdbt26Nf0JzaBqASMPzxbTNs5SxcIRtAfn4R7btWAWxxHx2NhYOimAuvAk
0VoU2KFDB/QRzYPGYzV+4sQJ5EQK2k+vhRCQUt2A1qIjsKLoBR2XhQsX0hkTZyEHCBaASAFEQBi4
ysgGQD433ngjInQcaYH4iaIAGvk9XykANP9ve84Yx8WLF1Mhoxy0HCUgM46IIz9tMyYLXI6hRDYo
BoYVzcap35A/HWIawbVU/89H9kJSlEuQFzLAY4GPRGe+qvtCEGeRiOGh9eIUZgo68Pbtyvvuu4/S
G8TG0pd6fQCNZGZmUrOAsacl0LN33303rqI14lrkgd60aNECp66//npctXTpUvqxYbt27dASlEBz
omGIoD105sIpyvxqBbQKTUXjK98N3rhxI50QMS5oPB0dCiSiL61atYJUbav4xBNPoBw7GxIxdvY4
npUvBJGBRtasWYOr6EhRaaMcgDaVXogW4gjJQz0w14DDyIlENB4ZfhG0KBrBtVT/azJ7MQwQDfhm
ib3CH8a6F1KjoEIEe6HHyInxwzRMMwOY5uvXrw+5Q+gYTjpf2qVh7DFg/fr1g8QxBvC4cISeVVT/
MyBDSUkJvTmMMvPz8+Ez0/G273OsXLkStWAs0Ta7JVTJkOHhhx9GXXDwsrOzkQE5aTpsL8pHZ1Ey
MqBwdOfFF19E4QB+btu2jfb9nXfeoStbXA71goNHpy2kUA78hvb8VaDKSgRRCfB1aTq6jCP6CCCO
juMnuA17S7tMATKjg+gdgL5XpJ5a/pwRe+Pj48vKyqAtyIAyURothGbAcfny5SgB10LCZ1Ge5xd7
wUyqkS+//DJ8SOgxle+AAQMwQmAI8iADHS1IBIIG9+g9ZzoeAGwv5nikIxvGA6WBFSgHpeEszCNs
LzSGmiyUAIqiqF8DLkc5yIz5AmaQerb0A0CgT58+1B4iGxoP84vaK9+IRquwBgP9jhw5QntHz/79
739HsVA4qBRmB2gh7C3VXdheqBcSH3nkEfwEMjIyMMsgM+0RqkMtt9xyCxJp8yzhVSOgm2gnJGCz
EX7Exx9/TIcDDUYGZEOcdn/evHl0dKjDDEAI8LTpTI0j8kBudE6kR0yUWEegHCpVqg8A6IcjJmiM
uF07ljAYJpoBWgQB0nSYemQDwF6cQpPQNgjf6sRZAC2TRtARqv81lr0gCUYCjKpbty6VLwWkj0T0
H1KABmDAkBlShqwxWtRztm94vPDCCyAk1Wk6GLbtpdwDbP8W42cn/hzIhqJQI2pftmzZ7bffjkT7
5vajjz6KpReMKuU2bf/hw4ehZ3TqodmguFlZWThLyQaVQqV33nkniqVqB3MKJaaZgUmTJqE6pNP/
8aDNmzVrViQSweWoAh2kncWp9PR0lFzdgGHCuJDOVALEYs+wgO0D26Dipf5RnTp1qF0F66ifZY8v
zuK4efNmpMOiQlZ0EkSliEOkkOfo0aMrlw/PGVSHJlBAjKeN+9q1a+1TKKGiG38Y5xd7MQwApA/b
S2ducAAYOnQoBgbChRQAyBcERgp1jDds2IABsDXjrbfewpDjLADuodhnn30W6RgwmgE5QUsMHtWD
3waqA+swYT/wwAP4iWtxIS5H82bOnAkGQofQHtSIPGgMzOyECRPotVQ/cGzQoAFaAg1DfnqKvhVI
QfUMJdOFLnICaDws+WOPPYYUtBM1VlZ9AA1o0qQJJEDnsmoFdJOuEdBINBs4TdSUogAGF3FkoOQE
w9FTTG1FRUXQBAgBswAEC4Fg0oQkqeeCq7BgASFhk9F9KAZqxBEjhZy4BM4aVR466GPGjEGrMPFh
gDCgOJJGWMty+tQABgAzBeUtyrE6cRaAuuxIzWcv5SfG4Omnn6byxdBiDPr27YvZFyOEocJYIgJZ
4AhxgxJgL1URSgNcS9NRDkqDBrzyyiu0tNN0iIJy7NeAJmHpC+MPrcJPm3XNmzcHY9FmmAhoDBqG
uqAfaBWqfuaZZ9AeegnF3LlzcRalIR0/6ZtAVLeQQnUIyM3NRQmYEaCXOKKb9AMmeyKglh8XogpU
BLGgAVR61QdoPKSBdtLO0sZjHHGsDIwXHTIKKq433nhj//79GDtM4hg7DCUmAoia5rGd4dWrV8MZ
odM36qLyp64WMmO6p9lofpCZagvNhsKRWLlq2F4kouXQLsrhswLUZUdqPnuhiOhnfn4++EaHnIp4
8uTJECsGBiLAzEoFjaGCfmM8sECyvVnMpvRtDWrocBbGsGHDhnQmpnmgUpjpbWr9IqUpQBJYAPue
NgUuvPvuu7EWRflATk4OGobGoFWIf/vtt2AUluI2z1HITTfdhAZgZUv1hoJqNlpCG9axY0cUAnuC
xqME9BfaCRrgEniJ1hX/xbZt25AHOY8fP465g0qv+gDzHThA7SRAKYRjZcJUBvwauModOnTYtGkT
hAnSYvggeRQFmYCN6CwtjQoNwz1nzhwMLoSDU6gLwFoDcTqBjhgxgk6INP+gQYNoOfQVVGSzb4Yj
w6233opVNK5CvTiiBNqLPw4MkB2p+eylb5AePXoUXYW4MZAYHggU60YawQBQGiMbBg/KDVljkqYe
FBIpaZEZJQCQGjgAwsMTQxzDg2w0HeUAuAQDj5+/CCxrMd6oAnkwU0ApcQmtFz9RJuKff/45akS9
AL23jNqRjkZiVYZTiENdoDQohGoG9AyZicZZD66hprTBdNWAU7gKedBstAE/cQmAa1E+SkOP0DCI
ApYHcxM6RTNUH2D+PXLkCNoGIaCDaCqajfbjJ0BJgq4hpyVm8q9dOEJK1O9F3z/77DNcggiOkNKx
Y8cgbZRDUyATjAUiAOQGgaAoSA+F4ycSCwsLMUYoFpXSPBAsTYfooGCIoBm4EInwdxCnjUTJUDyr
E2cB6JQdQRuo/tdY9kKJMVSQO6QJoMMQJY4YM/QfQ4vhAX9wCsMPIDOuwnjTFMRpCq5CfgDjAUDL
6VkccQqZUQ7ScaQkwalfBMYejUEDKM+hQLgEEaTgiHLQ4Ly8PCgB2kZnFhRLPV5E0BgcaX7koYyl
3EYigEuohUFRaC3OIkIbRi9HBqggCscR7cTshr4gggxUz5DhLGrb2QJ6gS6jbWgqmoe+QPK0+zaQ
jiOVJHiLgcBPXIgpDy4MPUtFgURIifIQhVBZAbiKdh9AOlUelIa6cBVOIY4GICd+YuwgQwB5kJlW
hwLRAMRxpFehcNKBswRUbUdQBdX/GsveKKKoSYiyN4oozlVE2RtFFOcqouyNIopzFVH2RhHFuYoo
e6OI4lxFlL1RRHGuIsreKKI4V3HOs/e0tn777bfaqY8NooiiZoPylr4WEggEvvnmmwoaVCXOJnv/
85//0Ail8ddff61bHw9EEUWNB3iLI6wuaFxcXPzjjz9SLlQpqsT2Us/5+++/Ly0tRX+iiKLGAyYX
BKYIBoNffPEF9B/uJ2VEFeGssRfUtdlrR6q69VFEUd1g3/T5E3DW2Au32W63zV7bl44iivMBUHj4
zAAoADr88MMPFSeqBmfTc7bZi9bTOCI0JYooojjrqBL2IkLjmHswG0URxfkDSoE/B2eTvT+3tH9y
Z6KIojoAag/rZa8fqw5nk71RRBHFn4koe6OI4lxFlL1RRHGuIsreKKI4VxFlbxRRnKuIsjeKKM5V
RNkbRRTnJsrL/w8VVzAV8lq1IAAAAABJRU5ErkJggg==",
								extent={{-316.7,-353.6},{319,302.4}}),
							Text(
								textString="SFp",
								fontName="TUM Neue Helvetica 55 Regular",
								textStyle={
									TextStyle.Bold},
								extent={{-180.4,325.4},{196.6,222.7}})}),
		Documentation(
			info="<HTML><HEAD>
<META http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"> 
<TITLE>WolfCHA10_GC</TITLE> 
<STYLE type=\"text/css\">
table>tbody>tr:hover,th{background-color:#edf1f3}html{font-size:90%}body,table{font-size:1rem}body{font-family:'Open Sans',Arial,sans-serif;color:#465e70}h1,h2{margin:1em 0 .6em;font-weight:600}p{margin-top:.3em;margin-bottom:.2em}table{border-collapse:collapse;margin:1em 0;width:100%;}tr{border:1px solid #beccd3}td,th{padding:.3em 1em; border-width: 1px; border-style: solid; border-color: rgb(190, 204, 211);}td{word-break:break-word}tr:nth-child(even){background-color:#fafafa}th{font-family:'Open Sans Semibold',Arial,sans-serif;font-weight:700;text-align:left;word-break:keep-all}.lib-wrap{width:70%}@media screen and (max-width:800px){.lib-wrap{width:100%}}td p{margin:.2em}
</STYLE>
   
<META name=\"GENERATOR\" content=\"MSHTML 11.00.10570.1001\"></HEAD> 
<BODY>
<H1>WolfCHA10_GC</H1>
<HR>

<DIV class=\"lib-wrap\">
<TABLE class=\"type\">
  <TBODY>
  <TR>
    <TH>Symbol:</TH>
    <TD colspan=\"3\"><IMG width=\"259\" height=\"290\" src=\"data:image/png;base64,&#10;iVBORw0KGgoAAAANSUhEUgAAAQMAAAEiCAYAAAD9IbdYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAEcgSURBVHhe7Z0HnBzFlYd1d77sQFZEKEurnHOOuwqrVc6BqCwhsQqIHIUx3J3PGAwWCJAxJgeDARsQGYwx+Ag2xtjmDDiHwwlsDO/qq543U9PTMzsrrcRq5v1/v/92T3d19+xMv69eVVf3NBGTyZSlN3ZWSpMmTXJcufONVInSlMHAZDJ5GQxMJpOXwcBkStCe2lgzoXZPak3pymBgMsVEn0G8fyBpWanJYGAyxbWn1mBgMpkiWTPBZDKVrQwGJpPJy2BgMnntkdomlUK3gA06MplMZS2Dgclk8jIYmEwx0UzIuXiQcLmx1GQwMJnSot8gt69AXepXFw0GJlNMiZlBGchgYDKZvAwGJpOXXVo0GJhMJi+DgcmUUr6MQG2ZgclUbnpjp1TGehD31Na6hkRpy2BgMsVk4wxMJlMkF/hNKndKGPrc0mzjDEymchRACPsMymDggcHAZDJ5GQxMJpOXwcBkylHyPQrWgWgylZns3gSTyRTpjZ1SW+JZQJIMBiZTTHZvgslkKmsZDEwmk5fBwGRKkP2Iislk8n0G8f6BpGWlJoOByRSX/daiyWRSWTPBZDKVrQwGJpPJy2BgMqkKPMDEnmdgMpWRCt6TYE86MpnKSIUC3mBgMpWT3pCdlQn3IDgQNGliD0Q1mcpOOZcVY89DLFUZDEwmk5fBwGQyeRkMTKaYEq8qWAeiyVROSn72odrGGZhMZaaC4w1KWAYDk8nkZTAwmXJkj0o3mUxO1kwwmUyR7FHpJpMJkRnEmwjWTDCZTGUjg4HJlKD0/Qm+84AbmCql1FsOBgOTKa70aMM9Uqs9iTYC0WQqP2WuJhgMTKby1hs7pdLftqwwsGaCyVS+8g80yVxJ0AShlGUwMJlMXgYDk8nkZTAwmRKU8+gzZ+tANJnKTHZvgslkirSn1mBgMpkiWTPBZDL5ZkKpB36SDAYmU1zWTDCZTMhuYTaZTGUtg4HJZPIyGJhMObIHoppMJicbdGQymSLZA1FNJhOyqwkmk6msZTAwmUxeBgOTKUE59yaUQY+iwcBkiinp3oRyuF/BYGAyxZXwJGSDgclUprJmgslkKlsZDEymmGwEoslkisSPqJQhDQwGJlNMNgLRZDKVtQwGJpPJy2BgMnnxDIPox1WtmWAymcpaBgOTKSafGfifZC8vGQxMpiRxeVGbCGVymdFgYDLVpT211mdgMpWv3pCdldp5WCvlkBsYDEymmKKrCdGVhXKSwcBkMnkZDEymHNnvJphMJie7a9FkMkWy300wmUzIhiObTKaylsHAZEpQ+hmIvvOAMQelf6nRYGAyxZV+OvIeqdWexIQnJpeaDAYmU0yZqwkGA5OpvMVNSv6uRYWBNRNMpvJV6uYktSYIpSyDgclk8jIYmEwmL4OByWTyMhiYylIffPCBvPnmm/Lcc8/Jj370I3n//fdTa8pXBgNT2ekvf/mLPPXUU/LYY4/JI488Io8++qg8/fTT8u6776ZKlKcMBqay08svv+wBAAhCkyWUswwGprLTE088kQMCdTnLYGAqO5EBJIHgySefTJUoT5UwDHhaTexBlumRZSj+NJvssukbVbzL73l4pax33nnHB34IArKF3/zmN6kS5akyh0Fmvb+HPT301AEgHHLmn6FvQCglAYRnn302nRGUOwiQwcDPO+k6hqGmywRiOYBIlduTfgBG7Bgm00GqEodBmOqnXEdmwDTx7jSFhc8SMk+9oTlR6nezmcpD1mcQg0RRMEjvw0kzBpPpIJc1E/x8oCKbCQYDU6nJYODnQ9XRgWgwMJWoDAZ+Pq7wd/ZwcCUhCQZt3F52pV6bTAepShgGB1B8ih93PsLZoGA6SGUwaAjxKaoNCqaDVAaDhlAIA7VBwXSQidPWtK+KgyC0QcF0kIjT1bSvigMgyQYFUyMXp6lpXxUP/EL+mPNgZ5OpkYnT07Svigd8PpMdzHb+sbPJ1MjEKdqIVcxYgXA8gJaNlicNK/b3IOQdX1BA8fEFoeJBH7dBwHQQiFO1Eat+A4cytyErJOJBr4OJDhAMDAKmg0icso1Y9YNBZl20fKeDQ9ZI4T21LlvYmb0NIwhzMotIURYRrat122XBIGs7t5VBwHSQ6yCAgQZc4KIyA7c8VpvvqWVYcbAN6xO39y+y1vknH+m+4vt178mtNgiYDmodBDDIBKRXViDGYJEAiQgAbobtQlD4RfHblQusIxNI7T/MGNSV/dwag4DpIFYJwCC23itYThA7CFCzR5V+w8AgqXOysejDDz/cK7s/zuwg2o9XanlSeZxU3i83HXQqfRi4kr7TMGkb9hVs72t8bSb4PoHMupxmQuJxD5zCgPvFL38p3/3ud71fwa+8Evm7KadefzdmXf7yKy+nzLw6e1m67MtuGU6vTzm1/KWXX4qZZZHT28asZZPW5fMLL7wg3/ve9/bph0/SQIspaXm+sqWkMoCBkwvsTE2euy6d7qf3Gyl8QnJtbSYzQNlNBbe/BhhZuDcn26uvfV9uue1WufX22+S2O25P9O231+E77/BO2latZW+7zR0H3xGZ49bl+D5ud9vjcP/eqX3feuutfpounzquzuv6Bx980P80mqlh1MhhcJCIT5HOw30YbhyC4Le//a3/HcDQb731lvfbb7/t/etf/1qeeeYZqZkxUyonT5Gp1dPzeloeV0+v8Z4Wc3WNekba06enzDyeEfdMb95PzczQs7xnJHnW7CzPjHnW7Dlpz54913vmzNmy/NjjZPfuG+T+B77un3DM5/Kzn/1MfvrTn/pp6J///Od5/Ytf/MI7/jrJv3TZVzH+1a9+5c38X//619Q3enDIYNAQ4lNU7wUUFAR/+9vf5KmnnpQ7XC159113yb333iv3fe1rke+7T+6//355AD/wgK8VN23aJD1695UefftLz34Dst034151OCxbvN0xY+Z95DPr08fr414794hZlxdyd/f/DhkxSo5fsUruvPte+cY3vuE/Dz4b//m4+cj6Wj+zzPTrbn2Wv67+etHmuGm71+yX7wjfc89X5fHHHvOQOpiaFgaDhlAIA3U9oKAnzPW7d8uw4cNl4aJFsnHjJjnW1YALFy2WRYuXyOIlS72XLlsmy5Ytl9Vr1sjChYtkwOCh0nfgEOk3aGhkXsfMMm8tE5TV8uF2ieVTy/oPHuatZTPbDEs5VTbcNu542dRr3Xf2Ol2fmnfbj5tYJb36DZTjT1rlPoe1smrValm1eo03rzOOv468Jp/Xrivaa9etj5ya37Jlq2zdts1lLcdKVVWVfO6yy+T7r712UP0eA6etaV8VB0HoAlAAAVpvPPLoIzJ42HAZMWa8rF53siw/YYUP8oqefaVb737O/aVrr35S0aO3dO3ZRyqnVkvN7Hky0AXHgIGDZcCgIW5+iAsmBwY8JGXmBw2OmWWZ5X0HZ7u/W+b36dw/5b4DB3nn26bvoOz1/QYMkv6B+7l1fVyZJOfua3BiuT4DB/p9jRs/0b+30WPHS6euPaWL+zwqvPv6zyg0n9v+NMfo7qazXBNm3fqTHayP9e9x9Lhxcvvdd8lP3nrT/2DLwSBOV9O+Kg6AJCdAQUHwvVdflSlTpzgQjJWNm7fJqWecLXMXLvEw6Dswqg37DXK1o5vvM8AF6ZDhMm/RUl9mlgPC7DnzZM7c+d6z56U8f4HMWbDQe67LICIzv9BtuzjR810Gghc4L1wUedHipbLQeYHLSvDCpcu8F6ldlpLkxUuXy5KUl6ami1keK+fLLj/We4nLhJa4mlWnujxtV3bZ8uP8/1s9fYZMnjJNuvfqK737D3Ye5N1nwJAsR59h/cxnjOOv/TL2GZhj0mzi/dRu3iqnn3GWrFi5Wnr36ycz5syWZ775TXn77bf8z8A3dnGamvZV8cAv5IRbmE89bbvMmDVDNtZulqk1sx0UJsiIsRNk8jQ6+GbK1OoaB4vpMtllA5OmVMvEydOkeuYcqXLrJ06qksqqKVI1eWqOCZbJU6e5ctX&#10;ek6vdPgJPme726zsNZ/iTWT29ZmbaNTPoAJztjjfLe/qs2d7a6UeNiGfMyfZMF7AEbdopWIWeO29BlufNd6Aq5AWLZL4z+wMuHLdmBp2N7pju88DM+9cp6+vQWjbuGrXbZ7GenvIM9xnxeVVXz5Dly4932d06GTVujGzbfqq/4nEwXPXg9DTtq+IBn89kBwnDlVetWS0nrjhJ1m442TcDOlZ097XN2AmTXLBXyng3JTUeO26CO8Em+qbEoGEjZfjocTJi5GgZOWpM2qNGj03bLxs9xmcceOTYcYkm3R6Tx2MdlMa4444en23eS+gx8fXu/fKe9X3rfCHzf9blCRMr/f82fMQov83UadO9pzhQYn2tnuZAGoIudAi90DVqQFiHgcBsBz+A3LtPP+nUucI3E7Y4CCxatlSqa6bLww8/LC+++GLq22684hQ17aviQR93HgiozjvvPDn+hONlnatN+g8YKL1695Y+ffvKyJEjXTCOlTFjXJCPGu09kkBwAKCPgCkwiAMhrwFEChTeDhDeqdejxzgwxJwEiPo6Do58jsMhbgUC/8vgIcNk2HA+H4AVeZzfzzg3HSfjHZAyHu+nEyZk5vW1X5ayvg4dLo+2HZ+93r2eONEB2n0H3Xv0ki4V3aRf/4Gy4eSTXTYzTxYsWCDf+c535IMPPkh9241XnKqmfVU8+NV1QECvIrz00kty1llnyec//3k55ZRT5PjjjpPly5bJ8ccfLytWrPBeuXKl9+o163yKTO1DUCgMGtIhQJLWJ9rV1PFl1N5Mw/0V2me8XJKBFiDo2q2H3/+iRYtl4cLIzC9atEgWL14sS5Ys8V66dGnay9xnunw5Xu597LHHeuvrfKbMgvnz/X6jfUTLjnPf03GpKVd6GBtBtnDiSStkx46LZP369XL11Vd7GBwMYw44ZU37qnpCIK5XX31VLr30Urn++uvloosukq1bt/oRj0xPO+20LJ9x5tmu1tnkA4FaV4MoXds3sDVDSFoXOqmcLivW8YwitK4ngyAj6Na9p2sGVMuWLVtk8+Za581+ns9s27Zt3qeeeqps377dm8/u9NNPlzPOOCM9PfPMM7MMkJN89tln+/2zH3197rnn+ozu/PPPlwsuuEDOOedsd7xtst0d89JLL5Hdu3fLpz/9aT9S8vnnnzcYlI32EgIqxth/5jOfkeuuu04uvPBCf1IzoIgTkBM6PKm3n3aGrF23wQcEARKHQbwWbcxWCMRf57OCYeiwET4zmDJ1msukNrnPaoObbnQAxRkw5IOCgkENGJLgoAYA7Jf96GuAcM4558i5AMH57LPOkG1b3fe1bYtc8plPOxhc72Fwyy23+Mzg/fffT33bjVcGg4bQXkJAxQ1GwOD664HBBe5E3uxPcmo8hYH6tNNPk3Uu/Rw6fISMGutgQD9CARgQRLruYHYIhAgGPQMYnJwIA80S+NwKwUCBEIeAWmHA9vpaYUB2gM8660wHgy3uWNvkkkv4LqMsj/sogAGjSxu7DAaNQCEMLrjgPHci1/qTOwkGp5++3cFgrQwdMcJfCVAY5HNSYB2MDmFAVpSBwcZGAoOz/PE41iWXXJIFA+6wtGaCqShlZwYKg5PrBYNCnXBJwdUQPpBZR/7MwGDQUDIYNAJFMOAEAgbnF4TBaaedKmvXrXEwGJ6CQW7wxx0GVfx1XdYADK3L6MyLL8dJ+6mPC+2H5UOGDo/6DKZVu8+piGbCtuiz25Zy9DkChExn4t72GYQwUPAAAzoQgQF9BgYDU9HKhQF9BsnNhL2BgZpgir9OMgGnwU22QfBxKbMPdx5yl2TP3t49e/XxA2369hvg74sgfWe/IRyS9l+X2S7ftiwvDINT/OfGZ8j8po2bZL1bfsrmTXLm2WfJGeedL6edfpZfv207HbTbHQjO3GcYsNxgYNpncTVBU0suU1GrMd6AE5ATTM2Jtn37thQMtJmQHfDaXMjXZMC6Ph5kGoAEG0HOAJoOHTtLm7bt5Zg27fw0ybquXfuOfgQeoOBGIi5/FgrsvbHCoKJr93QzYePGDWkgRFDY5P3pT++QKy+/XL5w5VVy7c6rZM/5F8me08+UB268Wf77ssvkoovPk//4z/+S7adGzQbNEOIm0BUG+lphoJcYWaaZCE0+hYE2E+xqgqkofVQw0NcEGIFLDd+5S1cf2K2PaStt23VIm0DH4bIkaxn20bFTF589MEiI4yQFd31dLAzWr18rX/zilfLE00/IK8+/Invue0Ber5kvvxkxUp66/LPy0ONPyj133SOfv/Jy2VK7OQcAofcFBpoZAIO//PJX8sraLfLCvOPkpRM3yJ9eb1z3KxgMGoGSxhkoDDjB9CTDcRjokGQN7PqY4GJbsgAyAGp4rEHdvkMnPw8YWrQ8Wo5q2kKOPKqZHHFk05Sb+ddNm7WUlq1a+3IZGERwYB8VXXu4ZsZg14wYlXXsvTEwoAORfXIT1qZNEQxCIAADXl9w/rly0X9cLJ/9/BVy91fvltfP+bT8YM1quf/qL8iNt9wm1++6RradcZps30ZTIeo/SMoQCHS+Dz5/1sdhgCkXwkA7EHWcwZ9/9nN5tnKO/Pyur8nvnn5Wfnn/Q/41841FBoNGoPrDIHM1YW9goIHFGHoPARfEbdoqBKIgbnV0ax/ohx56uHzqkMPk0MOOcMF/lAv85i7wj3brj/HT5i1aSrPmLV3ZpnL4EUf5csyzDjDQbKAW79W7jwwaMlRGu/c8ipun3HsOg7xYFwuDzZtPiZZtWC8nn7xBtrra/5ztZ8upLog3bt8umzbSQbtetrja/rTTMiDYXzB47eyL5Cc7d6e+8Uh/eOV7HgiNRQaDRiCFwfXXXys7dujVBDoQT0nDQH2aO5HXrlsnQ6llx06QYS6913sAijEBRXmG86bb+u3aS/v2DgZt20qzZgDgUDn88MPliCOOkDZt2khFRYV0795dunbt6qfdunVzJsB7S8+ePaVXr17Sp08fX659+/YOLq3lqKOO8tu3bNlSKrp0ceDp72AwWEaOHiVVUybLhEmT/PsI4VSso2aCwuBkF+zrs2AQAoGOwq3btkaA3ey8dZts9c2t6FJjvsuNhWDAsrATEbM8hAFgBwY333yzh8EzY6oTmwVP9Bvjmw+NQQaDRqD6wIB265q1LjPYCxhQq9J+py2vfQLU3sCgRfPm8qlPfcpDoF27dtKjRw8f9AR3q1at5Mgjj/QGFgT4Mccc49d17NhRurhgBwRMgcOgQYNkwIABftnRRx/tt2vdurX0HzhAxo4f50AwMXqIas1M/77qC4R6wyD4/AjWqO+lMAxCKwzYXssVCwPNDF48Yb1vGoR6/53fy2Pdh6ZeffSqJwyKeXR55vHimd8diJYn/fBI9Mjx2D6LUdZx8ynh/eaI31VI/eqSVzHbNKwyHYjXBoOOohOaGg3ryexhsIZmQjYM1HEAEGhMCSJ6+LVNr30CrY9pJ4cd5poBLhvo0KGDr+UJ8BYtWviAJ5A/+clPyiGHHOJNbd/cgYPgBhqU7dy5s58S+JRhW8AAFIYOHeoB0RKgND1Kevbu5TKDKn+fPw8p4aEljFcoFggKtDgMFAjFwiAEQl0wIBOIwyBsKigM+J7Y/8UXX5zVTKAD8f9efNk3CTQLAAR0Ir61+yb/ujFoP8Agsz7zoyQRDHKDfj/9KnJaewODA6/9CQMFAeMEgEA6G3Cmrf/xT3zKB2ovF7AENMH+iU98wk8J6qZNm8o//dM/yb/8y7/Iv//7v/vsgWVkCzQrAAgGDkCDzIJ5pjQVgMKwYTx7wAVwt65y5FFHSqcunV0gT5U586KnHY2fUOnfZzFA2J8wyAeFQjBQs6wQDLiaQGchzYKHW/fwGcFPb74zdQYgjZGwIi3m/G047VcYZNZFy/fbryLHwEC5KAsJ3w/zul/NUhRG4bLY/5D0flLH25N+P9nvs77KNBP00qKDQe0Gqd280Z+ACgSchkGsmZDPBBgZARBo37Gz7zBs27adC/Yj5RMueDt27OSbA2EG8G//9m/yr//6rz5joEnAMu1H0CBXGJAVYLKIj3/84x4EnTp18mXYJ+VpWvTt29cF8yjffPBZR5s2MslBeO&#10;DgQTJ85AiZv3ChD/ZigEAzgUFHk6dM9QDYsGFdTlMhhIHClEANYVAICKEJdIWBltV+AzXLk2CgNyrVPc4gdt55JS3bf9oLGGSCJ+08MMjODNzyWNDur19FToZBoKx9FWgm5Hs/frnCI3ovOr832l8wILCoRWkOkAkAAqYEKsFNhyABDQR4TTD/wz/8g88CAAHNAVJ/1pMd/PM//7MvQ3agQKCpQHArJPr37+87GQEA2wAS5tkfmceIESN806GV2+/HP/kJ6d23j8yeO1tmzYmeqaiZTCE3JhiQIeB8MNA+g7rvWkw6V2PL8lSU6R8XdsqJmazat7D2b59BAiT2x68iZ7+HsGz2+/UfVM6HmR8Ged9P7Hj+vdTjQ48rCQabTlnvTuiN/gQLzcnGs//rggFBxfBgLu1xxQAg4EMPO9wHPp171Po0CQjuj33sY/KP//iPfh1NBIJYAxwoUIZ1WJcDCpoITBUO/fr185Chc5H9k2UAF/bBdpQZPHiwP37zli2kbft2vg+hZuYM/8tNPEuQ958EAXXYgRjCAOd2InKvQvTZ5QOCQkGDXwGhZhnPl2AflFUYKAg0M+CeCI4BDOhA3LFjRz1hEMROLDb8OR4/l1PnXOY8TWW6qXOT5fU5LfdvMyGtYHkqcBryV5Gz30NYNve4kcL3WbowYFgwzQMyAszgoE988hBfc1ObAwJq/b//+7/3NT5pPSDQwCb1p1anLFmBmgDXfgUCniCnH4H+AWDAlQi9isB6pgBBYdDdNUtoNgweOkRatGrpgVA5uUqqJkdPeea+hkLNhf0BAxyHgLoQDHRKuSQY6KXFfc0MCsWGP//9+eeW+diKzucwYyhGBx4GPvgy9Mpax76C7fkA0kFGwAXrcn8VWf/xaP9xGGR9mFn7yg+DvO8n63922g8w0GZCCAK8bdtW10xY4yAwUka4oGEaBwGBpB2G7Tt0lI6dOsvRvoPvUw4KHX2tzLgAAvrv/u7vfApPYDNlXAF9CFwBoAlARkBgs05NWUz/ACbAMX0HvXv39jDwNb/LBgAHQMHAAGAwz7pBLkPoP6C/Bwnlx0+Y6DsIgUFSc0EBEXYgxmGQ21TIhUEcAvmcDwYs045FNcsVBjzhKMwMirs3ISl28py/XmF5zmE3787DiAlM3frwHC1CHwEMnNybzfxjuevSqVLsnyn0q8iZdS41ckSMwyCaT5Vx24bH9EHuludu45T0fmIwmDhgsjSp+mLqVf1VPxhscTBYLcNGAoNxOTDQ5gH3GET9BJ18UJMB0D9A+s48QU+GQHBrU4FlBCuXF6nhte2vtT+1Ox2LmCAHAGxP5gAwgADbARM9FusoS6ciAGJbjs/+2GbQwEF+uyPca4ZFM2R50OCh/n8g+Pl/4llCPhgwXwwMCmUHoeMwINh1eRwG7C8JBmQGDQEDf87Fz9mgAiILqNRKzZ+fLg6y4FG36gkDU5KaNDnLpdkXuNry07Jr1wuppcWrEAw4wXCxMCBwuOFImwdkBZr604GnqTpBCSRI17lkSBkCnwClzQ8QfJC65Zru04+g2QFBTsBTq9OcoKZnOzIDsgqCn+10f2QNbEd5OinZL/0P9DkAkKhJ0sL/LBvvn9ulFQjqHBgkdCAWAwOmOB78aoVAXTAIXQgG+6UDMVZR+nXpZVF2XN9k1WDQAAIG6r2BgsKAEyi6NyEDAzIgrFDYunWzrF6zyt+bMMKl1HEYaFZAZ2E00rCNr/kJRoKPeUx6T3ADBi73MQ8gtJYn0AlisgUCmOClhsf0GwAHgptanqCnnIIA6AADjsn+CHhgQSclMAJMAIEp74lMgiyEext439zpyHDpvn0HZMFAgRDBoLvvYwACGzaszQJCdr8BzzfIAFVBoNkBjsMgboJ/48bou9DXAALrfBwG117LaNL6wOCjl8GgARTCYG+gUBgG3LCUcRYMYpkBKTU3H7VrH4GArOAwF3xaixN4pPqYgCfICU7AQCbAazoSAQPLdDARTQgCn+2ZEvxkFUyx1vgEP0ENENg/mQaBzvbsE4BwxQIIABHgw/aAgm3atGkrRzVtJl26dvMm4OksbGwwwHXBIOwzMBiUkZJgoC4GCvmaCafUnuxOwOhBHWoPg9URDLhrMQ6Drt16ORjQPKiQY9q2kU9+ioFFHdOBTxCSDfBa7ztgGbCgyRAGPMHMtgQr85QDJBrANC9YRnmgQX8DQGCfBD6pP4HPPGUVHuyD/THlPTHPtl27VkjTZk2lTbu20rGii3To3MU3FwBAsTBQIIQw0MxKm1r1hUIIAy3LstAsY73BoMyVBIG4C0GhoWBAkHTo6IKoY4UfX8C9AE2bN5P2rhYnzaeGJvAIXAKVQMcELwFN0A4cONCXpdamLMBgqpDAZAWY7ShLs4GgZl/sl21pNlCGJgllyE5YDkQ0+2Ab5nk/wIg+Ct//0NI1Lbp0djDo7AcX6ZWFxggDLcO+DAamxODP54997BwZPDj7ykMIg6iZsNmPMwhhwGi6aERdraxavVKGjUzBYHgEA20iAIOOnbr6zsNPHXqIv4ZPIBLE1OKk75gmAKYmp1YmYKnFCXB6/dmGIAcElCFIAQPbUB5zOVAvQ9JHQE1PBkBfgwKEY7EMCGhmQlYCGJhqGeChHY0ArG2nDtLJ7Z9HrxH8uTDo4WGwYcN6/1QjnNtUwHx+Ub9LCAWFQRwIOASB+uST+S6iYNcyCgOm7FPXM/KQPgO+S/oM/ud//sdgUC5KCvokkx3Mnn2T/PjHv0ttGakYGGhNVwgGPXr2kvYdOkvnLt39swoOOexQDwNSc4KQgCfYNStQGGBeE5xkD6wHCABEOwCp5ZkCBAJXTa3OlD4Hgln7CjgOx9TmBhmCNi2ADq81G2E5mYF/Lw4yR7mmQmvXxOnsYECGw9UFxh8cHDDYkgOD4i4tfvRqEl2K4BJG6pKFd+wSh7/GWb/RTNmK9p89aCJSdI0/fkmlCMWu9Scr6XJNXAUGHRWppMAPnQ8CqqRmgofBKZl0Nw0Dlx2sXLUydTVhrIcBP3rKlNS5fcdOvr3drEVzH3QEGTWy1sQaeFrLU6MDAgLRp+ipGpwpVxkoo2MQ6FPgtuQhQ4bI8OHD/T0GzJMZsG86D/VyJM0AQEIzgIAnA6AM65gHEJj3gdkHJuMAJECmc2eynC5+JCWwwwCBcQjaTAAA69atyYJBNhBI78PsKrvJkAyF6BKuml9JAgbcn8A2ScDgO+MYTHkQ67XX7vIwKH7Q0UevAAaZAIgPaPCjn1zA7P0oO4VNPMii66GNCwb1VxIAcF0QUO0zDFyQ0OtOLdqhk2trV3SWw4443AchgU9wUUNTaxOgBBrWtr/W/BqYQITABQ70IbCeJgEAwAAAEGBuT+bmI8BBcBPsBDQBz/YchynQYT96GRGwkE0AIfaNOTZTyrdwMAMG+tg0/T8bGwx0m5KFQXagpYY6FizDy2jUX9YdfAyEiMZH+m1L9xbmoc71h4AqfwdicTAY6YKE3y3wg4xcAHXo0snDABBoLa2X9gg0gpXAIzgJYAKflJ1sQDMHymEgQfAT7DoyEUBws5E+vIR11OqMM+A4BDzHZUwCU5oGHBsocVyFEcfi+IBDxz9oOZZxaZQxEwQ+zz0EBABBOxAZdKQwUCAkwQBnAyEbBnEwKATorMUMAd+woTAM2K//ftIwiJoJpZUZcOKn5jM3FznVGYAKEZRaFtsmupEiODbrC72PYF3OvQl1wiBQ1r7imUER78cvz0AvAsKGekNAtc8wcEFC56EfW+Bg0K5TRzn08MN8UGugk/oTmAQgy0j9yQBYhhkQRBufm5ZYDgTYniYEtTgZgA4q4jZlBYIOP2YdWQOAIaCZMrCIoKapoKBRk6EQ9GQrHIfjkT2wL+17iGDQzQMBABwoGCgE8sEgUy6TTZQYDLT2C4LMiQBIAyBd0zvlDUCnVG2a3i4IsFK8hfnj/z5cmnQ6s94QUCkMuBxVFww46UIYDB1G5yFPOh7kgqfCBY5Lt10gEoQEGM82JPgJQJoKzAMG7dmn1qZG5koAQcugIKbacaiBjgECYAAITOlkJHAVEMCBKcch3aefgv0DHu2HIMgJetazT91OmxujR&#10;7ssx4GGMQcVFV391QRgwANaQhhwyZFmwvqgmRA2FfLBIHLUXOBzDp93kO3sZWEzIak8gGE907ADUS8tHrzNhLRYHkAiDKK9hIECJZNlFBF8bi5nXX1hkDpupGB5A8Age9/1V0PAoE9fMgMXPAxFbt9ODjv0MJ/mAwWCkdpa+wtIzQlMwMA8wUuwkkEABspTjqyA9j3NA0zQ02TQoCeoafMT1GQINBdYx3HYjiYBtT/gIGsg0MePHy9jx46ViRMn+umECRPSrydNmuRfc6w2bY5x+44uLQID7sJsOBhEGUIEA5wEg2wbDBJO8nSAEBAFbxsO9xnOR2Uz2Uewzu8z8z44Vvr4vJdgXU4z4SC+hZnfWuQZiNddp5cWIxhsSuhA5EReuXJFfhi42pRBRgQ2MNDUnyk1NU0CmgHaNmeqqXsICAKZYA9hoHclkhGQMQAHgAEMCHjW0Y/AfoAE5XlN53NNTY1MmTLFT6uqqmTatGl+uU6rq6tl8uTJHhgct3lzRj92li6uORDCgMuodCACAx6EAgDWrl2dbiokdSKGUNDPEIcwCB0PdLxhA/sCIplMItyGy4pJMNB7Ew56GGT1EaiCQPAB6bOFzG3DLNOgywRzbP8ueNKBmbAunYWEAeeUOZ57X+7DD9cnvZfsfTOfKuO2DY/p36dbnruNU9L7icFgX29hbggY9Os/2MOAwCG9p6YHBtTypP7U+PQL0ARgMJFmAJTTZgRw0LY8+yCV16AHCkwJfGBAhkDwc8mRoNflQAHYENDsh/4Ayk6dOlVmzJghc+bMkenTp8vcuXP9lNcAgXVkBWQc7JN98HxG+gb2FwwyUNh7GOjrkoBBamraB+kVhL29hVlhkD3oKBsCelJz8q5YeVIAA8YZjPbBon0GBCKZAUHFlP4Agp/efaa04zGvta9Ae/5pVpAt0N8AAAACEAAKZAe8ptbnNTBgGccDCICAPgTgwrZkIoCHrIR9jho1SmbNmiUzZ86UBQsW+On8+fN9VoC5MsGVDpooHgadXGbggr6Tayr0HzTY/64kYyq0mVBJM6GeMFAARB2JkRUCSc7AgKco0UzLHdqM6YdggBPrL7pohzsPdvkm30HVgZiamvZB8cuK9YVCLgy2uBOPUYeZkYfhSZ0NgxG+tuTSItfkuzgYEFDU+MCAqQa/Bn0IBgIWAPDwUqZcAdCMQTMCAEDQky1g9g8QMJ19ZAQsZ0rQa7ZAc4PABgbab8H+aA7MmzfPNxmAA80E+gw4HtvSyXjUUUf6KyOdgUG37jJgyFAZPmq0B19Wn8F+hEEmC9ji98t+om0yTQvKYP2uWH/RRRcaDMpVIQz2BgoNAQMChHQaGBC81PI0E5gSiAQ9Qal3CgICmgSsp4lAU4IsQpsQlNXsgKAHAGQA9BEw5TXHYUrHoTYZyAoIao7B/tiWLIJygAEgME9HI00H+hHoQORqAk0KLUOnJsOROzsQ4IFuvf6uZEPAIASCBnbcxcCA5kHURKgbBvyAyovL18i3qubIC/OPs19hLkUlwUBdDBRyxxlEP7xKWqonc3hSKwyGOxgMGTbcA8E/JNQFCGP7u7lUnVqZGpZAp8YnE2Ce4Gc5gUrgspyBQpo9UBZgaB8CHYkAgSBlnmYAEFAQ6FSbCIACiLB/IARwgASZhZZh3xybsnpZkayCcgBMr3Z06+7+HweCLm46ePgIGeYyAwWfwgAAAAMFAnBQIACDEAj6+akVBlq7qzXQ1SEMMhCJyioMWK773LHjgiwY0Gfwp5/+TJ4cMF4eat3d/4jKQ/yQSs/h9ivMpaYkCMRdCAq5MCDtzAUBJzbLk2BAkDCG3w/fdW1sfqTEDx5yAUxNS01NgGkTgMAnA+A1QKAsywAFtTKvqdUJbKYEK0FP7R2aAKaJACjo/MPAhO0BARkGxwUmBD/7IYtgnvcBcBQulAEGurx7j55S4UDQo3dfGTZylHcIA/oMPjoY0HcABPTBM4Vh8L0tZ8qedn08CEI/NawqdRZ89DIYNICSgj+f67qFOYJBdG8CVxMUApgTnNdhM2GIA4HCoF+/AS4z6OYCpZeHwiGuduauRbIBAp3AJAug74BlNB0IVJYBAYKXWpkgZzlBrtkF8wQxwUztDiAIaJoALCN7AAg0I5iSGbAdxyQL0IyEYAcq2vzgmGQjQAXwcGzKRk2UHv5qAg84oXnA/4j13oTKqsk5MFAghE2FEAoKAoJXHYdB6AgIm/0+2YfCINweh4C58MLz5ZprrpHzzz8/DYMn+o/NAQF+pNMA+xXmUlJS0CeZ7KCuW5jrC4PBrnlAEwEgRP0GLq2uiILoiKOOlDZt2/igJOAAggZomC0QpAQgAUk5anVqal0HIAAGQa1NBW0uAAQCnLJ0QnL5UpsP7J9t2Sf7Z56sg31S8xPwvA+WU5blZAcAgu352Xf+DzpHgQFDrxsDDMLgj79OggHPM3h+3nG+aZALg/6ps+Cjl8GgAZQU+KHzQUBFB2LSCMSNm6ITOrSHwYqToqcjexgMS8OAQOnRs48HAsHCY894UEgHF8AacBp8BDyBR5ATmNTkBC3Lwhqcsvy2AgChDK8JbJoXbIvZTmFDGcz29BMAEJojbMdyXmOgQNCzL6a8H2CkTYQePbq7DKe7ayr0St+xyP/HNBcGq3JgoE0FrCBQa+CqM+3+yPTXMM1Aodbvj/1E5TPBr9Z954MBv8L8WPdhWSB4rGKQvLnrhtRZ8NHLYNAASgIArgsCqoaAAR4+YqQMGDjEBUpP/3ThCpfCH+6CjdqbACfoCEjmCW4CkCCm2UAgEqBMSfWZkrJrwBL0bAsE6BPA+uvM7IPMAJBoGQKefSgQKMM6yrEv4KB9E6wHUvzQC8fyzQ/XjOjqAr6Pa/qETYSDFQZcTaCz8NGuQ9IZwdtfvjV1BqBgUFw4wK2g4qNn900GgwbQ3kJAFV5aLAoGrpnAk47oQNRmgppalB8joTORTrajWx/ja1oCnODTqwcEHZcRCULmCVJSc0BB+s44AZoEGuzsI8wgCHimbEtgpwPabU+AMw9wOC6XGtmOMkzZRgHBcTUrOOJIriK0dtlAH9/v0bVbTx/4hWCwdu0aWbNm72GQL+XXeUyAa9NDy8at+6Zs1IGYC4PCyh75Go7kzS+DQaPT3kJA1ZAwIGDoOyC95iYfgubII6MHjmgwMrBIa2gClppYswCAQFntCyCoNTugwxHr4CXmtWnA/tgH21IW8PCatF+vNGhTgf1xbCCAFTSHHHqEH1LdrXtvn9307NXX/T+ZB742RhiE+8UNBQM/DD68FyAcFu+Xp+7xSS3z4IgNk0/fj5NavpMh+6xPvc48jyM6rsGgAbS3EFDVp5nAiX3SihNzOhBDEzAMT+YZB31clsCVAAKRHnyClEAkxSfQCUaCFpPma03OVQLWadagNTi1OdtrnwHBznbarAAiZBYa5GzHcTp37uybDJpdhPtl28MP53mIZAV9PQzIDKKsIAMCsh6cBQMX/GtWr9wnIORzEgzCfYTW/SsMrr766jQMXnzxxSIeiJqbGaRZEAvyzLpYZlAIBi7os/anAHHSLMRg0AjU0DAgcLiph/kBAwf5S3/UxgSqZgjU6NTuXBUgcIEEANDRggQ0mQGBruMR2EZHJ4ZTtqc80CGzIPC1P4DlHJugZx0Dj/Q1gAIEzAMHmgUKgt59Brj/g+ZBJjM4GGFQ/NORs/sMwqRAb6QLHQVyPWAQLM95ncpCDAaNQCEMwrsWFQbxk7ouGNBM0EeLE0Q8GKRX774u6Fq6IOvrg50amva99t4TlNpXoKMECVBqf+0z4DW1OmY5U4KedQS6XnIMswPAw3ptcrCOJgOv27bh8WdRpsAPxHZ3IAAG9BkMHsLVkUxfQeikZkIxMNgbIGCAwD7ZT7g91v3qMSgbdSBmYFDvzCBfUOfIYFByamgYqMeNnyhjx03wYOBaPR2KTZu18DcX0RQgrafGp3anXU9A+448F/RkBAQx/QPaL8C8QgCIKCy0CUIzAAMUgp7llCHwgQsZAcfkWGQPrY/m9xoPlbbt2kt3Ojy702nYyzVxGFcw1gV+mcLAKQsABG/Yn5BWAgySnuthMDh4VJ8OxI0b&#10;18uJJ51QJwzIDgiciZOqvAECGQI/bKo1uTYHCFSCmuVAQDv69PZjghgDAIUANT7lyAwACM8z0Ccd8UAToKAZCPvmGFivVrRuHQ10atasuXSjs7NHb6lwIOjjmgfDhjPACGeaBgqCTDOB5xlMljUOAKtTzYR8QMgHhaTAT7LCgP3o9urs7yYXBvQZvPTSS/WGgQazNheymwrZ0Mg0G4hrLRM818NgcPBof8AAa3OhavJUbzIFAomgpEYmgAlaOg2BAADQ4Kc25zX9CkzDDkMAAAjYDwFNcOsDUnmOITceMU//ABkCQKDJwLaAg30DEx7gSkZQ0bOPc1/p2bu/8LCW/QmD6DOsX4ZwYGDw0ctg0AhUDAz0pK4PDDBA4NeIqqfPkGnVNT6QBgwY6NrlPeWII4+STp27+HsOCFDt2NMrBpjApzZnSuBjhQHlmRLwZAUAARjwnEPMcw/ZN7dVA5tj2vAbDq0dCLjXoZO//EmnYUX3ntKjdz8HAgKfgFdHIAhhgPkf/I1KwGDNGv/bk2vWrPaOwyAMVrUG8t7AINxHuE8FhcKADsTzzjvPNxOAwQcffJD6thuvDAaNQNpnUBcMOLk3uvn6wAATPGQFM2fNkZoZs2TylGm+c7H/gMHSouXR0tLV3GQItON9x55r1wMFYEBGAAB0qp2CQIAan3Jsw1UIAp+nFQGEkSNH+icbkSHQXGjbtp00bdZSmjVv6QDU1TcNCGjcq3ef9HDqEAChC8MAENB3EN2n0NAwwOw3hEHcuTDYKeeeazAw1VN1wYATbV9ggBUI8+YvlPkLFnko8JrgqugaPaoMCNARqHcgEuxcAcCaEQADzRJYrtkEZWkCABRMByVXFmhCsK82Dgatj2krDJNmdCRTAprRknWBQJ0Dg8oDA4OkzCBug4GpQZQfBhkIKBA48U448fg6YUCAxZcRRDQZ5s5bIEuXHSsLFi6WKVOrXUo/wbfz6TsACqT01Obdunbzy2gmaJNAswJAQDldzpRyPJmZR7UzZdxBt25d/dWFnj0ZO9DPX+IEBpiBURrgScGv5n/BzIcwmORgsHr1Glm1apUHAq6rz0BBEAdCISgoDPLBBXOMXBicm4aB9RmYilL9YVB3ZpAEA0wgEVA0GY497gTvefPmOyhMlQkTXKYwfITLDHq4pgMpfRT8CgdqfPoHCHIgwTxTXndwU368pbVrMvBgFW4/9g8y6d/fu2+//v65BPyiMhDgvdQFgSTnwmC1g8FKNwUIxV9NODAwyGQGBgNTUaoLBpgTUU/GYmBQyHQqYi45Llm6XFasXO32ucI3H+hkpC0+bsJ4t+8h0qVrhbRqHT1/gOYAWUCzVHag/Qc+S3BNAH4OvnuP3v5OQ4KeoGV8A2ZemwNYYaW1Pk4K/iRnwWDNKgeDFQ4EACH/SER1PIjjQEgyQMgHg3Df7IuyIQzsaoKpXjrQMNDgI0gJLq40kCGsXrNOVq5a4wCxVGbOniXTplfLqDGjZfjIER4Oo8eOceWHue0GyYCBroZ3Hjh4kFsW3Q+hIx7ZJ/dFMGVZOPiJ9eF7oUzpwuBcg4GpfgIGyZcWIwjoiZiBQd19BsWaDAEoEKRkBcuWHyer17pjueOsWrNa5i2YL7PmzpGaWTNl2owamVI9TSqnTpFJU6pkQlWljK+c5DxRxk900wmTZMLESpdZTPHjGrhqwfyYsePT2QAOj58U7HU5HwzyjTXAcRAQuGpeJ0EgNPvUzx8rAHT/uk5hsHPnTjnnnHOsmWCqnz5KGKgVCswT2DNnzZJlx7omxCoXYOvXyep1LmtYu0ZOdO3zY088UZYed6wsXr5MFi1bJguXLpGFi5fIggWLZc7c+TK9ZqYPVDKBePCHrm9GoDYY7B8ZDD5Cffjhh36qzYTcuxYzMFBzAp5wQgSDYaPH+CBOCrT6OKyxFQoMXWaegKa2nzptusyaPddfiaBvgUuUBD7LaGaQCYwdP8E3C9gX28bfmx4n7qSAz2fKZ2BQ5SBAB2LmakI0AGlVFgw0cLEGcwgDtQZ+/HUSDPR7Yao2GJj2Wo0FBoVM4AEHPGjQUP86n5MAEDoJAvnKxSGgZh3H2h8w0HW63mBgOuDK20zYmElDMzBYK8efcJyHwfD9BAMNagIvaf3eWiGQtE6tZeIOgcB7C2GwcuXKAAi5MMBxGCQ5DgV9HcIgBAsOj8M2cRhYB6KpXmqsMEhaty/WwE5apw4BELqxwSD+vTDPNgYDU71E8wDrENVcGGz2MDg5gIGeeCEMDkQz4UA6CQJJDpsJBD4diOHlRQI3DFIN4LpgkM8AQQGjIND9p71+tSu71sOA7/CLX/xiFgx4BqJ+741VBoMDKD0Z1PsKA//zavUIooZwPIAb0knHS3IEg56NDAarXFmDgalIEfxxo1deeSXWgRjBIGwmZGCwzsNg+KhRMnTUaOk3YKDv+c9nvTKgnYDpzsBUp18+a1OhWMeDOnwdLi/ksCmQzzpoCRhEdy3SgXiShwABixUG+rmFMMBJAY+1WZC0XPcbfhdZTsNgUxoGZ599tu9AfOON6DEiBgNTWvlgwM+r7dixw8Pg4osvlu3bt8mWbTyMM/uxWpzI/DIzj0ofMHiwtGrTVpq1aCUtW7XOcatWx0iro7lPoJ1323YdpF37jn6+Tdv2cnTrNnndGh/TNtG6v7TdvjD7bJuaZtwhy+3adYzs3gfmqUuF3KFjZ+nYsYub4s7pad9+A/0Ix6opU31ghgDAcQgkBXd9vGnTBr9/hQHZWRYIsMLA35twoYfB6aefLnfccYf89a9/9d9z+J03RhkMDqBCCKipKf785z/72uOqq66URQvmyeKF82TB/Nkyf+5M51myYN7sjOfPkTmzZkiXLtxN2NLfdszzCFq14v6Bo6Uldxg667TVMQ4Kx7RJ2ZUl4N2y5txroG55tHeztFtl7GATzbeM3AK3kObOLdx88+bcwtzCL2/qyjb1U2e3rGnz5pGbRWY7b8qH69PlmslR6qZNs3xkanoEt1A7c2PUpKoqWemyghNPPFZWrDheVq48wZkmQ+bGJToVMw8+4RZnF7hrCebQXBUgC4u8Yb2DhrcDifPGkzf4fbGtlsnePvKGDYBmox+GfMUVV/gnHfH8Q0QHIuY7b6wyGBxA6cmgJ0b8BLnhhhukpqZGZs+eLTNmzPDzOp0+fbrMmjVLpk6d6ud5kMjo0aP9vQOdu3WViu7dIvfoJl2wm2d5525Mu3t36d4j5Wi9Wten3T322u+DfXWVTt0qpGNFFxk3frwMGTLE383Icwu6pMp26trNuzPLulZkuYL36ZzvtV+m8+4YoTunzDr+5+49e0jPXj2levo0mTptikyZMlmmTZsq1dXVztNT02iezwvrfLQ+s5zPN/qMI9c4z6h29lO33pWZPHmyO8YUd4xpztVph/viu+I7Oumkk+S2227zfUGI/gIcfteNUQaDAyhOBj0x4kY3fOVmGVc5VabNmCNTambL5OqZUlU9IzWd6aeTptb4+ZHjJsnQUeNkyKjRMnjkyMijXHvaTYeOHCXD3HKGK2MuQWKWYZ2nHB4ywm07fERBDxo23Hvg0GF+ynMRhrEu1WcwyC1nXdpDhsrAwZEHMa92rwcMHuKmQ6LlzAfrWZf2oCHSf9Bg52h+gJtn+3ETJvnjjXD/w/wFC2XW7Dkya47LmObN86Miczxvvh85yTzTuHPKO8+eM0/muumsWXP87d4MseaBMIy4DE05zDYLFy3xd4GuWr1OHn/8Cf+9Evw0E+LfdWOUweAAihOBEwP/5S9/yTK6+557Zc78RbL02BNkyfLjZfGy42SRO7kWLsHLZMHipTJ/0VKZt3Cx8xKZy70ArvzseS4g5uIFMmPOfO/KqdNl1LiJUjXN1Wqu5lJXudps3KTJMt5Bp2pajS+nZVhXOXWaVDJVu9eT8JRqmejsp5Or3fZTZILbx8TJ02RC1VT3mn1WeY+rrJSxk6qcJ8uYiVUyekJlokeNn5T2SPdew/kRYyfkeDg3PI0Z7yDoIOfmR4+vlJFj3XYOjHi0n/I6Mq912Rh3PP//pv5n3jPL9XOYMn2mt98n27v3oe9xdMrsI+6x7v8bO3Fy6jO&#10;d4sstXHqcvPnW274J+N577/nvV7933FhlMDhA4sTgpODkwO+++643/QWY+XfeeUd+8YtfeP/85z+Xn/3sZ/LTn/5U3nrrLXnzzTflJz/5iff//u//etPP8KMfvSE//OEb8vrrP5Yf/OCH8v3XnN302eeel9POOMtBY5Ecf9IKWb12vZx6+hly3oUXyvqNp7hlK+Wzl10un/mP/5Jrd39Jvv2d78iz335evvmt5+SZb31Lnn72WXnqm9+Up575pjz59DPy+FPOTz7t/dgTT8kjjz8pjzz2hOx59HF52PmhRx6VBx/eI9946GF54MEH5b5vPCj3PvB1uee+B+Sue++TO+/5mtyV8p0Oerff/VW57S7nO++WW++4S26+7Q656dbb5Su33CY33nyrz5K+9JWbZPeNN8n1N9wo133py7LLvc+rr9stO3ddJzuvvU6+eM21ctXOa+XKL+6SK6+6Rr7gfMVVO+XzX7hKLrviSrns8i/I55wvu/xKOevc82Wyg95cl0ksO/Y4WX9yrXz2c1dI7ZatUjNztox12cZoB5hzzrtAdnzmUjn/oovdZ3WR97kX7JBzzr/Qrzv7vPPlrHPOkzPPPlfOOOscOd2Zz3m795lSu/VUf8x3383+nrF+941VBoMDJGCgwa/TP/3pT/KHP/xB/vjHP6anCoc///lP/jVl1LwOy/7xj0z/4F//4fe/d9PIQIUa6PXXX5eFC1wq7FLoZcuWCZ1kdG7tvmG33HzrLXLTLTf76csvvyy/d9v/7re/zfh30fS3eZxV7ne/C6aR/+//1P9Xp3m/hfz732P+t9T/Glg/k3z+kzOfJ5/LtxzguFLDFYJd1+x27/MdueqqK/wVHHr/L7vsMl8OaL/7bvQ9vZdyFNCpZe+FwR0t92VS3y3Lw/cXfoe8l8Yqg8EBkl41CE8MThZOcsxJT2BoMCUF4G9+8xv51a9+Jb/85S9zsoe33347bTIJ1j/rTv6pLsWn86umZqocf/xy2bDhZLnm2l1yy223yp133ymv/uA1edttT5bx4x//OOMf/Sj7dR3+kSsfulCZpHWY95DPdZXRbAmH2ZNmU/oZcXVh9uxZcuqp2+TOO++UCy44z/f8P/30074MnxufKZ8t1kyNz5zPHv/617/238Vvnf3UfTcRACOwhaALIaZgaKwyGBwghZmB1lohBLBCgBNMA58TUZsKnNwExA9/+ENf6//gBz+Q1157Tb7//e/nmAC56667ZNSo0Q4GVbJoyWypmTFF1q5d57OBc847VyZWTpJTNtfK1772Nb8Ng5/oAWfcw6uvvuqX6VTN8UKH65LWh66rDP9PPtdVhs8jyXxWmM+ODGj58uWydOkiOeHEZfL5y//LgeBy+fKXv+x7/7/pmkU0x+JwwQCFdXwPGLAADRzCIg2KFCCizCYCgsHA5KWZASeEZgQAIKz1OZG0ZuLE4yQmGBnO+h3Xpv/2t7/ta3tOWmqyp556yvvJJ59M+4knnpDHHnvMr//c5z7nYTBhwgSZM2+aTKoc4zODa6+/XjZv2SIrVq107d3TZdeuXXL77bfL3XffLffdd5889NBD8sgjj8jjjz/uzT51/3rMfOa4+fzMM8948/7rY/5n9be+9a1EP/fcc4lmne4D6HEJcfKUSlm0eI5s3bZJLrnkM/KFL3xBFi1aJP/5n/+Z/p95n2z7/PPPC7+izHgBvgcMVDDwVHACKuADrAEHwCCz4DsFCiEQGqsMBgdIYTNBswKFgWYBnDxAgFSaE4wTkJORE5mTk2Ak0B999FHvPXv2eD/88MM+gL/xjW/IAw88IPfee6/PCm688Ua5Ztc1/pd9lixdIhdeeIG/B+LSSy/1qfHu3bt9GcY3fOUrX5Fbb73Vb3fPPff4wLn//vtzzP6L8de//vUGM/9X3A8++KD/n/nfk8x6tuU9f/WrX/X/15e+9CU/1Pukk06Q+fPnyim13Edwnvz3f/+38AtIfA6Yz4Ft2JZj8JkrCBVqIaiADt8TwOY7I7siWyGjIKsDCHzPZAkAobHKYHAApVcTaC4oFDhBqDlIMzlxOIE4kTihOLE4wTjRtIbjRNSaVmtjajJqck5cApFgpqYn0DnRN23a5H/67MQTT/RDZC+//HK59tpr/Yl/0003+bKc/GQFBBCBpMEWt64rxklB3FDOBwnMOj4HgEa2w//HPQI33gjwbnGBf5WHwbnnnu2yp8/KlVde6W8So7lAOYYQ8xkCA/YHcIFvmCnxufNdKAxeeOEFn0GQLZAlkCHQlOB7BfaaHVhmYPIKRx3qmAMFhGYN1BwKCG17hs0I7cQKOxGBiLZ1SVW1acHJCgxWrFjh28rc909GwIlMkwPYcPLSjgdANEu0ky+fw/Z42FbXNr1a+weSTGodGvAVa95vaE3ZQytEASj/K6AkqAlw7iScNm2KVFR0liFDBvksgQwJcAARypIFAF4+I63tsTYTtKnA8Xn//E/8z3x+fAdAgO+F74zvEgDw/eqVhsYqg8EBFKPRME0GpiEcQudbB0B0H/EyrNNpaI4Vite6LhwIo2Cqj6NLa/U3QbG/nQRXYErgPvLIQ66mp3n1oKvNX0gHLs02TeUJYLZnP6HDZfH1HJf/Tz8fPlM+5/h31FhlMDiACgM5bj1h9tYa4OpiyqhDICS9zmfWhyd+WF5f74sL7UePU5fD/5/POUkAUtcX+32EZevrxiqDwQFU0omBORnjTipXyEknbJKTAqaQ49uF+6preVIQJ1n3EXd8n0kutE18Xe76yOGyYp30HYQu9D02VhkMDqDynSC6PHS8TH2cb/ukk7pUXeh/Dj+TYhz/bkInlcfx9eE2jVUGg49A4YnRUE466XSZed8cfqZ764NBBoMyUPyEDE/SuhwPiDBIQte1fl+t+9+fjivf8lKVwcBkMnkZDEwmk5fBwGQyeRkMTCaTl8HAZDJ5GQxMJpOXwcBkMnkZDEwmk5fBwGQyeRkMTOWpv70n739np7z3lUp5/5nPyIfvvZNaUb4yGJjKTw4E790wSt67or28e9nR8u7n28p7uwbIh79/M1WgPGUwMJWd3n9qhwNBhwgEKf/Z+S+3TE+VKE8ZDExlJ5oGIQjSdhlCOctgYCo7/fX+FYkweO/qnqkS5SmDgans9OFvX5f3dvbIBsFVFfK3V76cKlGeMhiYylIfvPWUB8C7V3X2zYO/PndZak35ymBgKk29sVMqmzSRJk0qZecb7vWeWmlSuydaF6jcryCEMhiYSlBvyM7KWiH039hZG8EgWGZKlsHAVILaI7WVO134GwzqI4OBqSS1p7aJ0CpIwyBPM8GUkcHAVKIiE6DPIOVUpmDKL4OByWTyMhiYTCYvg4GpBLVHaptkOgvpP6CpYF0GhWUwMJWgMlcT6Dis9D2IdjWhLhkMTCWpN3ZWpjoPUwBgEJJ1IhaUwcBkMnkZDEwmk5fBwFSSyjQTQlufQSEZDEwlqMzVhD21qRuVWGaXEwrKYGAqQYX3JlSmLina1YS6ZDAwlaT03oQoS0g1EywzKCiDgclk8jIYmEwmL4OBqSRlVxPqL4OBqQQVDEc2FS2DgakktafWsoD6ymBgKiEFVw4SbYAoJIOByWTyMhiYSkvpR6Q7W79BvWQwMJWQGGWow4+d7CGo9ZLBwFRCil9FcHCoteygWBkMTCUkg8G+yGBgKiHZ1YR9kcHAZDJ5GQxMJpOXwcBUUgrvSeBCgj4mPf1rzKa8MhiYSkj0GWi/AJcZm6Qek47inYumuAwGphJSdsBnnnKE7ElHdclgYCopZZ55GJP9bkKdMhiYTCYvg4GphJQaZ2BDkPdKBgNT6Yl7EuwKQr1lMDCVsKIrCh4M1l9QpwwGpvKQv7XZriYUksHAVNJKDzqyzKBOGQxMpad0n4H+kIqpGBkMTCUku5qwLzIYmEwmJ5H/ByXWe6rYdaR1AAAAAElFTkSuQmCC\"></TD></TR>
  <TR>
    <TH>Identifier:</TH>
    <TD colspan=\"3\">CoSES_Models.HeatGenerator.DigitalTwins.WolfCHA10_GC</TD></TR>
  <TR>
    <TH>Version:</TH>
    <TD colspan=\"3\">1.0</TD></TR>
  <TR>
    <TH>File:</TH>
    <TD colspan=\"3\">WolfCHA10_GC.mo</TD></TR>
  <TR>
    <TH>Connectors:</TH>
    <TD>Electrical Low-Voltage AC Three-Phase Connector</TD>
    <TD>lV3Phase</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal Volume Flow Input Connector</TD>
    <TD>Return</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal Volume Flow Output Connector</TD>
    <TD>Flow</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Environment Conditions Connector</TD>
    <TD>environmentConditions</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Modulation of the heat pump</TD>
    <TD>HPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump on or off</TD>
    <TD>HPOn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump mode: true - heating; false - cooling</TD>
    <TD>HPMode</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Modulation of the auxiliary heater</TD>
    <TD>HPAuxModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Parameters:</TH>
    <TD>On / off button of the heat pump (if switched off, no power during     
              idling, but doesn't react to commands from the control system)</TD>
    <TD>HPButton</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum power of the auxiliary heater</TD>
    <TD>PAuxMax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal heat output at A2/W35 (not maximum heat output)</TD>
    <TD>PHeatNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal COP at A2/W35</TD>
    <TD>COPNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal EER at A35/W18</TD>
    <TD>EERNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Wolf Heatpump CHA</TD>
    <TD>wolfCHA1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Adapter to Green City connectors</TD>
    <TD>greenCityConnector1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Results:</TH>
    <TD>Flow temperature</TD>
    <TD>TFlow</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature</TD>
    <TD>TReturn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Output temperature of the heat pump</TD>
    <TD>THPout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Ambient temperature</TD>
    <TD>TAmb</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of HP</TD>
    <TD>QHeat_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of the auxiliary heating system</TD>
    <TD>QHeat_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cooling output of the heat pump</TD>
    <TD>PCooling</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric consumption of the heat pump</TD>
    <TD>PEl_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric consumption of the auxiliary heater</TD>
    <TD>PEl_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Coefficiency of Performance</TD>
    <TD>COP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy Efficiency Ratio (Cooling)</TD>
    <TD>EER</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated electric energy of the HP</TD>
    <TD>EEl_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated electric energy of the auxiliary heater</TD>
    <TD>EEl_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated heat energy of the HP</TD>
    <TD>EHeat_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated heat energy of the auxiliary heater</TD>
    <TD>EHeat_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated cooling energy of the HP</TD>
    <TD>ECooling</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Modulation of the heat pump</TD>
    <TD>HPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump on or off</TD>
    <TD>HPOn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump mode: true - heating; false - cooling</TD>
    <TD>HPMode</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Modulation of the auxiliary heater</TD>
    <TD>HPAuxModulation</TD>
    <TD>&nbsp;</TD></TR></TBODY></TABLE></DIV>
<H2>Description:</H2>
<P><FONT size=\"2\">The model is not completely validated with measurements   
yet.</FONT></P>
<P><FONT size=\"2\">The model represents the CHA 10&nbsp;heat pump&nbsp;from Wolf  
and is based  on the modelica model NeoTower2.mo</FONT></P>
<P><FONT size=\"2\">The heat generator can be switched off&nbsp;completely by   
deactivating it via the parameter \"HPButton\". Otherwise,&nbsp;the heat pump   
runs  in idling mode.</FONT></P>
<P><FONT size=\"2\"><BR></FONT></P><FONT size=\"2\">
<P><FONT color=\"#465e70\" size=\"2\">Input/Control signals:</FONT></P>
<UL>
  <LI><FONT color=\"#465e70\" size=\"2\">HPOn - Boolean value to switch       
  the&nbsp;heat pump  on or off</FONT></LI>
  <LI>HPmode - Boolean value to switch between heating (true) and cooling        
   (false) mode</LI>
  <LI><FONT color=\"#465e70\" size=\"2\">HPModulation - Value of the modulation, if  
           it is below ModulationMin, it will be overwritten by that     
  value</FONT></LI>
  <LI>AUXModulation - Value of the modulation of the auxiliary heater - the aux. 
          heater can be&nbsp;controlled in&nbsp;4 stages: 0%,&nbsp;33%, 67%,     
  100%</LI></UL>
<P>The heat pump is devided into four sections and based on measurements from   
the CoSES laboratory:</P>
<UL>
  <LI>Start-up process: The     behavior is provided by a time   dependent       
  look-up     table.</LI>
  <LI>De-Icing process (only at heating): If the ambient temperature is low, the 
        air humidity can freeze on the heat exchanger of the heat pump. 
  Therefore,     the   heat pump goes into deicing if these conditions are true 
  and uses hot     water   from the house to defrost the heat exchangers. The    
   behavior is     provided by   a time   dependent look-up     table.</LI>
  <LI>Steady-state process:&nbsp; The steady-state     efficiency depends on the 
        return temperature and power   modulation and is     defined by a       
  two-dimensional look-up table. Load changes   during steady-state     are      
   modeled with a rising or falling       flank.</LI>
  <LI>Shut-down process: Similar to the     start-up process, the shut-down      
   process is provided by   a time dependent     look-up  table.</LI></UL></FONT> 
<P><BR></P></BODY></HTML>
",
			revisions="<html>
<ul>
<li>September 8, 2017 by Bram van der Heijde<br/>First implementation</li>
</ul>
</html>"),
		experiment(
			StopTime=1,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.001,
			__esi_Solver(
				bEffJac=false,
				bSparseMat=false,
				bSplitCodeGen=false,
				typename="CVODE"),
			__esi_MinInterval="9.999999999999999e-10",
			__esi_AbsTolerance="1e-6"));
end SF_BHP_HEX_Sub_Exp;
