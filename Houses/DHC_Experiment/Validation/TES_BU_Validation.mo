﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo.Houses.DHC_Experiment.Validation;
model TES_BU_Validation "NewModel1"
	Environment.Environment environment3(
		Filename=CoSES_ProHMo.LibraryPath + "Data\\5GDHC_Experiment\\Weather.txt",
		UnixTimeInit=1609455600) "Environment with user defined weather profiles" annotation(Placement(transformation(
		origin={-110,70},
		extent={{-15,-15},{15,15}},
		rotation=90)));
	parameter String File=CoSES_ProHMo.LibraryPath + "Data\\5GDHC_Experiment\\FM_Cst_BUTES_Test.txt" "File with input timeseries";
	Real qvPrim(quantity="Thermics.VolumeFlow") "Primary Volume flowrate through BU";
	Real qvSec(quantity="Thermics.VolumeFlow") "Secondary Volume flowrate";
	Real TInBU(quantity="Basics.Temp") "Primary flow temperature in BU";
	Real TOutBU(quantity="Basics.Temp") "Primary return temperature";
	Real TOutBU_Exp(quantity="Basics.Temp") "Experimental primary return temperature";
	Real TFlowSec "Secondary flow temperature";
	Real TRetSec(quantity="Basics.Temp") "Secondary return temp";
	Real TRetSec_Exp(quantity="Basics.Temp") "Experimental secondary return temperature";
	Real PHeat_Exp(quantity="Basics.Power") "Experimental Heat Power";
	Real PModSet(quantity="Basics.RelMagnitude") "Power setpoint";
	Modelica.Blocks.Tables.CombiTable1Ds Load(
		tableOnFile=true,
		tableName="Consumption",
		fileName=File,
		columns={2,3,4,5,6,7,8,9,10}) "Table look-up in one dimension (matrix/file) with one input and n outputs" annotation(Placement(transformation(extent={{-80,30},{-60,50}})));
	Storage.HeatStorageCombined5Inlets WolfBSP800(
		VStorage=0.785,
		dStorage=0.79,
		QlossRate=7,
		LinearProfile=true,
		TupInit(displayUnit="°C")=294.15,
		TlowInit(displayUnit="°C")=288.15,
		TLayerVector(displayUnit="K"),
		TAmbient=295.15,
		TMax(displayUnit="°C")=353.15,
		alphaMedStatic=0.5,
		use1=false,
		iFlow1=9,
		iReturn1=2,
		use2=false,
		HE2=true,
		iFlow2=5,
		AHeatExchanger2=2.5,
		VHeatExchanger2=0.0165,
		use4=true,
		iFlow4=9,
		iReturn4=6,
		use5=true,
		iFlow5=2,
		iReturn5=5,
		use6=true,
		iFlow6=10,
		iReturn6=1,
		use7=true,
		iFlow7=1,
		iReturn7=10,
		use8=false) "Heat storage with variable temperature profile with 5 inlet ports" annotation(Placement(transformation(extent={{-5,-28},{30,45}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow input_from_grid_C annotation(Placement(transformation(extent={{125,-20},{105,0}})));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow output_to_grid_C annotation(Placement(transformation(extent={{105,5},{125,25}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow input_from_grid_H annotation(Placement(transformation(extent={{125,35},{105,55}})));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow output_to_grid_H annotation(Placement(transformation(extent={{105,60},{125,80}})));
	Distribution.SwitchPort switchPort1 annotation(Placement(transformation(extent={{-60,-30},{-40,0}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow input_from_ASHP(qvMedium(displayUnit="l/min")) "Specifies a Thermal Volume Flow Connector" annotation(Placement(transformation(
		origin={-95,0},
		extent={{10,-10},{-10,10}},
		rotation=-180)));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow output_to_ASHP(qvMedium(displayUnit="l/min")) annotation(Placement(transformation(
		origin={-95,-30},
		extent={{10,10},{-10,-10}})));
	equation
		// enter your equations here
		// [1] Flowrate BU grid, [2] Temp bottom BU grid, [3] Temp top BU grid,
		// [4] Flowrate ASHP, [5] temp in ASHP, [6] temp out ASHP Cool, [7] temp out ASHP Heat,
		// [8] ASHP mode (1 cool, 2 heat),[9] ASHP power
		
		qvPrim = abs(Load.y[1]/(1000*60));
		
		if Load.y[1]>0 then
		//Heating
			TInBU = 273.15+Load.y[2];
		
			input_from_grid_H.qvMedium=qvPrim;
			input_from_grid_C.qvMedium=0;
			
			input_from_grid_H.TMedium = TInBU;
			input_from_grid_C.TMedium = 0;
			
			TOutBU = output_to_grid_H.TMedium;
			TOutBU_Exp = 273.15+Load.y[3];
		else
		//Cooling
			TInBU = 273.15+Load.y[3];
			
			input_from_grid_C.qvMedium=(qvPrim);//Cannot have negative value in the component
			input_from_grid_H.qvMedium=0;
			
			input_from_grid_C.TMedium = TInBU;
			input_from_grid_H.TMedium = 0;	
			
			TOutBU = output_to_grid_C.TMedium;
			TOutBU_Exp = 273.15+Load.y[2];
		end if;
		
		qvSec = abs(Load.y[4]/(1000*60));
		
		if Load.y[8]>1.5 then 
			//Heating mode
			switchPort1.SwitchPort = true;
			TFlowSec = 273.15+Load.y[7]; 
		else 
			//Cooling
			switchPort1.SwitchPort = false;
			TFlowSec = 273.15+Load.y[6];
		end if;
		
		qvSec = input_from_ASHP.qvMedium;
		TFlowSec=input_from_ASHP.TMedium;
		TRetSec = output_to_ASHP.TMedium;
		
		PHeat_Exp = Load.y[9]*1000;//Power out
		TRetSec_Exp = 273.15+Load.y[5];
	equation
		connect(environment3.UnixTime,Load.u) annotation(Line(
			points={{-105,55},{-105,50},{-105,40},{-87,40},{-82,40}},
			color={0,0,127},
			thickness=0.0625));
		connect(WolfBSP800.ReturnIn7,input_from_grid_C.Pipe) annotation(Line(
			points={{30,5.3},{35,5.3},{100,5.3},{100,-10},{105,-10}},
			color={190,30,45},
			thickness=0.0625));
		connect(output_to_grid_H.Pipe,WolfBSP800.FlowOut6) annotation(Line(
			points={{105,70},{100,70},{35,70},{35,40},{30,40}},
			color={190,30,45},
			thickness=0.0625));
		connect(WolfBSP800.ReturnIn6,input_from_grid_H.Pipe) annotation(Line(
			points={{30,30},{35,30},{100,30},{100,45},{105,45}},
			color={190,30,45},
			thickness=0.0625));
		connect(output_to_grid_C.Pipe,WolfBSP800.FlowOut7) annotation(Line(
			points={{105,15},{100,15},{35,15},{30,15}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort1.In2,WolfBSP800.ReturnOut5) annotation(Line(
			points={{-40.3,-25},{-35.3,-25},{-10,-25},{-10,-24.7},{-5,-24.7}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort1.Out2,WolfBSP800.FlowIn5) annotation(Line(
			points={{-40.3,-20},{-35.3,-20},{-10,-20},{-10,-19.7},{-5,-19.7}},
			color={190,30,45}));
		connect(switchPort1.In1,WolfBSP800.ReturnOut4) annotation(Line(
			points={{-40.3,-10},{-35.3,-10},{-10,-10},{-10,-9.699999999999999},{-5,-9.699999999999999}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort1.Out1,WolfBSP800.FlowIn4) annotation(Line(
			points={{-40,-5},{-35,-5},{-10,-5},{-10,-4.7},{-5,-4.7}},
			color={190,30,45}));
		connect(switchPort1.In,input_from_ASHP.Pipe) annotation(Line(
			points={{-60,-5},{-65,-5},{-80,-5},{-80,0},{-85,0}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort1.Out,output_to_ASHP.Pipe) annotation(Line(
			points={{-60,-25},{-65,-25},{-80,-25},{-80,-30},{-85,-30}},
			color={190,30,45}));
	annotation(
		viewinfo[0](
			showAll=false,
			hideInfinity=true,
			fMin=0,
			fMax=1000000,
			TMin=0,
			TMax=1000000,
			animGain=-10,
			animFreq=0,
			formatAbsDev="%.4lf",
			formatArgDev="%.2lf",
			formatEnergy="%.4lf",
			minNormDev=0.0001,
			minNormEnergy=1e-06,
			minAbsEnergy=1e-12,
			scaleEnergy=0.01,
			typename="AnaEFEWInfo"),
		Documentation(
			info="<HTML><HEAD>
<META http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"> 
<TITLE>WolfCHA10_GC</TITLE> 
<STYLE type=\"text/css\">
table>tbody>tr:hover,th{background-color:#edf1f3}html{font-size:90%}body,table{font-size:1rem}body{font-family:'Open Sans',Arial,sans-serif;color:#465e70}h1,h2{margin:1em 0 .6em;font-weight:600}p{margin-top:.3em;margin-bottom:.2em}table{border-collapse:collapse;margin:1em 0;width:100%;}tr{border:1px solid #beccd3}td,th{padding:.3em 1em; border-width: 1px; border-style: solid; border-color: rgb(190, 204, 211);}td{word-break:break-word}tr:nth-child(even){background-color:#fafafa}th{font-family:'Open Sans Semibold',Arial,sans-serif;font-weight:700;text-align:left;word-break:keep-all}.lib-wrap{width:70%}@media screen and (max-width:800px){.lib-wrap{width:100%}}td p{margin:.2em}
</STYLE>
   
<META name=\"GENERATOR\" content=\"MSHTML 11.00.10570.1001\"></HEAD> 
<BODY>
<H1>WolfCHA10_GC</H1>
<HR>

<DIV class=\"lib-wrap\">
<TABLE class=\"type\">
  <TBODY>
  <TR>
    <TH>Symbol:</TH>
    <TD colspan=\"3\"><IMG width=\"259\" height=\"290\" src=\"data:image/png;base64,&#10;iVBORw0KGgoAAAANSUhEUgAAAQMAAAEiCAYAAAD9IbdYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAEcgSURBVHhe7Z0HnBzFlYd1d77sQFZEKEurnHOOuwqrVc6BqCwhsQqIHIUx3J3PGAwWCJAxJgeDARsQGYwx+Ag2xtjmDDiHwwlsDO/qq543U9PTMzsrrcRq5v1/v/92T3d19+xMv69eVVf3NBGTyZSlN3ZWSpMmTXJcufONVInSlMHAZDJ5GQxMJpOXwcBkStCe2lgzoXZPak3pymBgMsVEn0G8fyBpWanJYGAyxbWn1mBgMpkiWTPBZDKVrQwGJpPJy2BgMnntkdomlUK3gA06MplMZS2Dgclk8jIYmEwx0UzIuXiQcLmx1GQwMJnSot8gt69AXepXFw0GJlNMiZlBGchgYDKZvAwGJpOXXVo0GJhMJi+DgcmUUr6MQG2ZgclUbnpjp1TGehD31Na6hkRpy2BgMsVk4wxMJlMkF/hNKndKGPrc0mzjDEymchRACPsMymDggcHAZDJ5GQxMJpOXwcBkylHyPQrWgWgylZns3gSTyRTpjZ1SW+JZQJIMBiZTTHZvgslkKmsZDEwmk5fBwGRKkP2Iislk8n0G8f6BpGWlJoOByRSX/daiyWRSWTPBZDKVrQwGJpPJy2BgMqkKPMDEnmdgMpWRCt6TYE86MpnKSIUC3mBgMpWT3pCdlQn3IDgQNGliD0Q1mcpOOZcVY89DLFUZDEwmk5fBwGQyeRkMTKaYEq8qWAeiyVROSn72odrGGZhMZaaC4w1KWAYDk8nkZTAwmXJkj0o3mUxO1kwwmUyR7FHpJpMJkRnEmwjWTDCZTGUjg4HJlKD0/Qm+84AbmCql1FsOBgOTKa70aMM9Uqs9iTYC0WQqP2WuJhgMTKby1hs7pdLftqwwsGaCyVS+8g80yVxJ0AShlGUwMJlMXgYDk8nkZTAwmRKU8+gzZ+tANJnKTHZvgslkirSn1mBgMpkiWTPBZDL5ZkKpB36SDAYmU1zWTDCZTMhuYTaZTGUtg4HJZPIyGJhMObIHoppMJicbdGQymSLZA1FNJhOyqwkmk6msZTAwmUxeBgOTKUE59yaUQY+iwcBkiinp3oRyuF/BYGAyxZXwJGSDgclUprJmgslkKlsZDEymmGwEoslkisSPqJQhDQwGJlNMNgLRZDKVtQwGJpPJy2BgMnnxDIPox1WtmWAymcpaBgOTKSafGfifZC8vGQxMpiRxeVGbCGVymdFgYDLVpT211mdgMpWv3pCdldp5WCvlkBsYDEymmKKrCdGVhXKSwcBkMnkZDEymHNnvJphMJie7a9FkMkWy300wmUzIhiObTKaylsHAZEpQ+hmIvvOAMQelf6nRYGAyxZV+OvIeqdWexIQnJpeaDAYmU0yZqwkGA5OpvMVNSv6uRYWBNRNMpvJV6uYktSYIpSyDgclk8jIYmEwmL4OByWTyMhiYylIffPCBvPnmm/Lcc8/Jj370I3n//fdTa8pXBgNT2ekvf/mLPPXUU/LYY4/JI488Io8++qg8/fTT8u6776ZKlKcMBqay08svv+wBAAhCkyWUswwGprLTE088kQMCdTnLYGAqO5EBJIHgySefTJUoT5UwDHhaTexBlumRZSj+NJvssukbVbzL73l4pax33nnHB34IArKF3/zmN6kS5akyh0Fmvb+HPT301AEgHHLmn6FvQCglAYRnn302nRGUOwiQwcDPO+k6hqGmywRiOYBIlduTfgBG7Bgm00GqEodBmOqnXEdmwDTx7jSFhc8SMk+9oTlR6nezmcpD1mcQg0RRMEjvw0kzBpPpIJc1E/x8oCKbCQYDU6nJYODnQ9XRgWgwMJWoDAZ+Pq7wd/ZwcCUhCQZt3F52pV6bTAepShgGB1B8ih93PsLZoGA6SGUwaAjxKaoNCqaDVAaDhlAIA7VBwXSQidPWtK+KgyC0QcF0kIjT1bSvigMgyQYFUyMXp6lpXxUP/EL+mPNgZ5OpkYnT07Svigd8PpMdzHb+sbPJ1MjEKdqIVcxYgXA8gJaNlicNK/b3IOQdX1BA8fEFoeJBH7dBwHQQiFO1Eat+A4cytyErJOJBr4OJDhAMDAKmg0icso1Y9YNBZl20fKeDQ9ZI4T21LlvYmb0NIwhzMotIURYRrat122XBIGs7t5VBwHSQ6yCAgQZc4KIyA7c8VpvvqWVYcbAN6xO39y+y1vknH+m+4vt178mtNgiYDmodBDDIBKRXViDGYJEAiQgAbobtQlD4RfHblQusIxNI7T/MGNSV/dwag4DpIFYJwCC23itYThA7CFCzR5V+w8AgqXOysejDDz/cK7s/zuwg2o9XanlSeZxU3i83HXQqfRi4kr7TMGkb9hVs72t8bSb4PoHMupxmQuJxD5zCgPvFL38p3/3ud71fwa+8Evm7KadefzdmXf7yKy+nzLw6e1m67MtuGU6vTzm1/KWXX4qZZZHT28asZZPW5fMLL7wg3/ve9/bph0/SQIspaXm+sqWkMoCBkwvsTE2euy6d7qf3Gyl8QnJtbSYzQNlNBbe/BhhZuDcn26uvfV9uue1WufX22+S2O25P9O231+E77/BO2latZW+7zR0H3xGZ49bl+D5ud9vjcP/eqX3feuutfpounzquzuv6Bx980P80mqlh1MhhcJCIT5HOw30YbhyC4Le//a3/HcDQb731lvfbb7/t/etf/1qeeeYZqZkxUyonT5Gp1dPzeloeV0+v8Z4Wc3WNekba06enzDyeEfdMb95PzczQs7xnJHnW7CzPjHnW7Dlpz54913vmzNmy/NjjZPfuG+T+B77un3DM5/Kzn/1MfvrTn/pp6J///Od5/Ytf/MI7/jrJv3TZVzH+1a9+5c38X//619Q3enDIYNAQ4lNU7wUUFAR/+9vf5KmnnpQ7XC159113yb333iv3fe1rke+7T+6//355AD/wgK8VN23aJD1695UefftLz34Dst034151OCxbvN0xY+Z95DPr08fr414794hZlxdyd/f/DhkxSo5fsUruvPte+cY3vuE/Dz4b//m4+cj6Wj+zzPTrbn2Wv67+etHmuGm71+yX7wjfc89X5fHHHvOQOpiaFgaDhlAIA3U9oKAnzPW7d8uw4cNl4aJFsnHjJjnW1YALFy2WRYuXyOIlS72XLlsmy5Ytl9Vr1sjChYtkwOCh0nfgEOk3aGhkXsfMMm8tE5TV8uF2ieVTy/oPHuatZTPbDEs5VTbcNu542dRr3Xf2Ol2fmnfbj5tYJb36DZTjT1rlPoe1smrValm1eo03rzOOv468Jp/Xrivaa9etj5ya37Jlq2zdts1lLcdKVVWVfO6yy+T7r712UP0eA6etaV8VB0HoAlAAAVpvPPLoIzJ42HAZMWa8rF53siw/YYUP8oqefaVb737O/aVrr35S0aO3dO3ZRyqnVkvN7Hky0AXHgIGDZcCgIW5+iAsmBwY8JGXmBw2OmWWZ5X0HZ7u/W+b36dw/5b4DB3nn26bvoOz1/QYMkv6B+7l1fVyZJOfua3BiuT4DB/p9jRs/0b+30WPHS6euPaWL+zwqvPv6zyg0n9v+NMfo7qazXBNm3fqTHayP9e9x9Lhxcvvdd8lP3nrT/2DLwSBOV9O+Kg6AJCdAQUHwvVdflSlTpzgQjJWNm7fJqWecLXMXLvEw6Dswqg37DXK1o5vvM8AF6ZDhMm/RUl9mlgPC7DnzZM7c+d6z56U8f4HMWbDQe67LICIzv9BtuzjR810Gghc4L1wUedHipbLQeYHLSvDCpcu8F6ldlpLkxUuXy5KUl6ami1keK+fLLj/We4nLhJa4mlWnujxtV3bZ8uP8/1s9fYZMnjJNuvfqK737D3Ye5N1nwJAsR59h/cxnjOOv/TL2GZhj0mzi/dRu3iqnn3GWrFi5Wnr36ycz5syWZ775TXn77bf8z8A3dnGamvZV8cAv5IRbmE89bbvMmDVDNtZulqk1sx0UJsiIsRNk8jQ6+GbK1OoaB4vpMtllA5OmVMvEydOkeuYcqXLrJ06qksqqKVI1eWqOCZbJU6e5ctX&#10;ek6vdPgJPme726zsNZ/iTWT29ZmbaNTPoAJztjjfLe/qs2d7a6UeNiGfMyfZMF7AEbdopWIWeO29BlufNd6Aq5AWLZL4z+wMuHLdmBp2N7pju88DM+9cp6+vQWjbuGrXbZ7GenvIM9xnxeVVXz5Dly4932d06GTVujGzbfqq/4nEwXPXg9DTtq+IBn89kBwnDlVetWS0nrjhJ1m442TcDOlZ097XN2AmTXLBXyng3JTUeO26CO8Em+qbEoGEjZfjocTJi5GgZOWpM2qNGj03bLxs9xmcceOTYcYkm3R6Tx2MdlMa4444en23eS+gx8fXu/fKe9X3rfCHzf9blCRMr/f82fMQov83UadO9pzhQYn2tnuZAGoIudAi90DVqQFiHgcBsBz+A3LtPP+nUucI3E7Y4CCxatlSqa6bLww8/LC+++GLq22684hQ17aviQR93HgiozjvvPDn+hONlnatN+g8YKL1695Y+ffvKyJEjXTCOlTFjXJCPGu09kkBwAKCPgCkwiAMhrwFEChTeDhDeqdejxzgwxJwEiPo6Do58jsMhbgUC/8vgIcNk2HA+H4AVeZzfzzg3HSfjHZAyHu+nEyZk5vW1X5ayvg4dLo+2HZ+93r2eONEB2n0H3Xv0ki4V3aRf/4Gy4eSTXTYzTxYsWCDf+c535IMPPkh9241XnKqmfVU8+NV1QECvIrz00kty1llnyec//3k55ZRT5PjjjpPly5bJ8ccfLytWrPBeuXKl9+o163yKTO1DUCgMGtIhQJLWJ9rV1PFl1N5Mw/0V2me8XJKBFiDo2q2H3/+iRYtl4cLIzC9atEgWL14sS5Ys8V66dGnay9xnunw5Xu597LHHeuvrfKbMgvnz/X6jfUTLjnPf03GpKVd6GBtBtnDiSStkx46LZP369XL11Vd7GBwMYw44ZU37qnpCIK5XX31VLr30Urn++uvloosukq1bt/oRj0xPO+20LJ9x5tmu1tnkA4FaV4MoXds3sDVDSFoXOqmcLivW8YwitK4ngyAj6Na9p2sGVMuWLVtk8+Za581+ns9s27Zt3qeeeqps377dm8/u9NNPlzPOOCM9PfPMM7MMkJN89tln+/2zH3197rnn+ozu/PPPlwsuuEDOOedsd7xtst0d89JLL5Hdu3fLpz/9aT9S8vnnnzcYlI32EgIqxth/5jOfkeuuu04uvPBCf1IzoIgTkBM6PKm3n3aGrF23wQcEARKHQbwWbcxWCMRf57OCYeiwET4zmDJ1msukNrnPaoObbnQAxRkw5IOCgkENGJLgoAYA7Jf96GuAcM4558i5AMH57LPOkG1b3fe1bYtc8plPOxhc72Fwyy23+Mzg/fffT33bjVcGg4bQXkJAxQ1GwOD664HBBe5E3uxPcmo8hYH6tNNPk3Uu/Rw6fISMGutgQD9CARgQRLruYHYIhAgGPQMYnJwIA80S+NwKwUCBEIeAWmHA9vpaYUB2gM8660wHgy3uWNvkkkv4LqMsj/sogAGjSxu7DAaNQCEMLrjgPHci1/qTOwkGp5++3cFgrQwdMcJfCVAY5HNSYB2MDmFAVpSBwcZGAoOz/PE41iWXXJIFA+6wtGaCqShlZwYKg5PrBYNCnXBJwdUQPpBZR/7MwGDQUDIYNAJFMOAEAgbnF4TBaaedKmvXrXEwGJ6CQW7wxx0GVfx1XdYADK3L6MyLL8dJ+6mPC+2H5UOGDo/6DKZVu8+piGbCtuiz25Zy9DkChExn4t72GYQwUPAAAzoQgQF9BgYDU9HKhQF9BsnNhL2BgZpgir9OMgGnwU22QfBxKbMPdx5yl2TP3t49e/XxA2369hvg74sgfWe/IRyS9l+X2S7ftiwvDINT/OfGZ8j8po2bZL1bfsrmTXLm2WfJGeedL6edfpZfv207HbTbHQjO3GcYsNxgYNpncTVBU0suU1GrMd6AE5ATTM2Jtn37thQMtJmQHfDaXMjXZMC6Ph5kGoAEG0HOAJoOHTtLm7bt5Zg27fw0ybquXfuOfgQeoOBGIi5/FgrsvbHCoKJr93QzYePGDWkgRFDY5P3pT++QKy+/XL5w5VVy7c6rZM/5F8me08+UB268Wf77ssvkoovPk//4z/+S7adGzQbNEOIm0BUG+lphoJcYWaaZCE0+hYE2E+xqgqkofVQw0NcEGIFLDd+5S1cf2K2PaStt23VIm0DH4bIkaxn20bFTF589MEiI4yQFd31dLAzWr18rX/zilfLE00/IK8+/Invue0Ber5kvvxkxUp66/LPy0ONPyj133SOfv/Jy2VK7OQcAofcFBpoZAIO//PJX8sraLfLCvOPkpRM3yJ9eb1z3KxgMGoGSxhkoDDjB9CTDcRjokGQN7PqY4GJbsgAyAGp4rEHdvkMnPw8YWrQ8Wo5q2kKOPKqZHHFk05Sb+ddNm7WUlq1a+3IZGERwYB8VXXu4ZsZg14wYlXXsvTEwoAORfXIT1qZNEQxCIAADXl9w/rly0X9cLJ/9/BVy91fvltfP+bT8YM1quf/qL8iNt9wm1++6RradcZps30ZTIeo/SMoQCHS+Dz5/1sdhgCkXwkA7EHWcwZ9/9nN5tnKO/Pyur8nvnn5Wfnn/Q/41841FBoNGoPrDIHM1YW9goIHFGHoPARfEbdoqBKIgbnV0ax/ohx56uHzqkMPk0MOOcMF/lAv85i7wj3brj/HT5i1aSrPmLV3ZpnL4EUf5csyzDjDQbKAW79W7jwwaMlRGu/c8ipun3HsOg7xYFwuDzZtPiZZtWC8nn7xBtrra/5ztZ8upLog3bt8umzbSQbtetrja/rTTMiDYXzB47eyL5Cc7d6e+8Uh/eOV7HgiNRQaDRiCFwfXXXys7dujVBDoQT0nDQH2aO5HXrlsnQ6llx06QYS6913sAijEBRXmG86bb+u3aS/v2DgZt20qzZgDgUDn88MPliCOOkDZt2khFRYV0795dunbt6qfdunVzJsB7S8+ePaVXr17Sp08fX659+/YOLq3lqKOO8tu3bNlSKrp0ceDp72AwWEaOHiVVUybLhEmT/PsI4VSso2aCwuBkF+zrs2AQAoGOwq3btkaA3ey8dZts9c2t6FJjvsuNhWDAsrATEbM8hAFgBwY333yzh8EzY6oTmwVP9Bvjmw+NQQaDRqD6wIB265q1LjPYCxhQq9J+py2vfQLU3sCgRfPm8qlPfcpDoF27dtKjRw8f9AR3q1at5Mgjj/QGFgT4Mccc49d17NhRurhgBwRMgcOgQYNkwIABftnRRx/tt2vdurX0HzhAxo4f50AwMXqIas1M/77qC4R6wyD4/AjWqO+lMAxCKwzYXssVCwPNDF48Yb1vGoR6/53fy2Pdh6ZeffSqJwyKeXR55vHimd8diJYn/fBI9Mjx2D6LUdZx8ynh/eaI31VI/eqSVzHbNKwyHYjXBoOOohOaGg3ryexhsIZmQjYM1HEAEGhMCSJ6+LVNr30CrY9pJ4cd5poBLhvo0KGDr+UJ8BYtWviAJ5A/+clPyiGHHOJNbd/cgYPgBhqU7dy5s58S+JRhW8AAFIYOHeoB0RKgND1Kevbu5TKDKn+fPw8p4aEljFcoFggKtDgMFAjFwiAEQl0wIBOIwyBsKigM+J7Y/8UXX5zVTKAD8f9efNk3CTQLAAR0Ir61+yb/ujFoP8Agsz7zoyQRDHKDfj/9KnJaewODA6/9CQMFAeMEgEA6G3Cmrf/xT3zKB2ovF7AENMH+iU98wk8J6qZNm8o//dM/yb/8y7/Iv//7v/vsgWVkCzQrAAgGDkCDzIJ5pjQVgMKwYTx7wAVwt65y5FFHSqcunV0gT5U586KnHY2fUOnfZzFA2J8wyAeFQjBQs6wQDLiaQGchzYKHW/fwGcFPb74zdQYgjZGwIi3m/G047VcYZNZFy/fbryLHwEC5KAsJ3w/zul/NUhRG4bLY/5D0flLH25N+P9nvs77KNBP00qKDQe0Gqd280Z+ACgSchkGsmZDPBBgZARBo37Gz7zBs27adC/Yj5RMueDt27OSbA2EG8G//9m/yr//6rz5joEnAMu1H0CBXGJAVYLKIj3/84x4EnTp18mXYJ+VpWvTt29cF8yjffPBZR5s2MslBeO&#10;DgQTJ85AiZv3ChD/ZigEAzgUFHk6dM9QDYsGFdTlMhhIHClEANYVAICKEJdIWBltV+AzXLk2CgNyrVPc4gdt55JS3bf9oLGGSCJ+08MMjODNzyWNDur19FToZBoKx9FWgm5Hs/frnCI3ovOr832l8wILCoRWkOkAkAAqYEKsFNhyABDQR4TTD/wz/8g88CAAHNAVJ/1pMd/PM//7MvQ3agQKCpQHArJPr37+87GQEA2wAS5tkfmceIESN806GV2+/HP/kJ6d23j8yeO1tmzYmeqaiZTCE3JhiQIeB8MNA+g7rvWkw6V2PL8lSU6R8XdsqJmazat7D2b59BAiT2x68iZ7+HsGz2+/UfVM6HmR8Ged9P7Hj+vdTjQ48rCQabTlnvTuiN/gQLzcnGs//rggFBxfBgLu1xxQAg4EMPO9wHPp171Po0CQjuj33sY/KP//iPfh1NBIJYAxwoUIZ1WJcDCpoITBUO/fr185Chc5H9k2UAF/bBdpQZPHiwP37zli2kbft2vg+hZuYM/8tNPEuQ958EAXXYgRjCAOd2InKvQvTZ5QOCQkGDXwGhZhnPl2AflFUYKAg0M+CeCI4BDOhA3LFjRz1hEMROLDb8OR4/l1PnXOY8TWW6qXOT5fU5LfdvMyGtYHkqcBryV5Gz30NYNve4kcL3WbowYFgwzQMyAszgoE988hBfc1ObAwJq/b//+7/3NT5pPSDQwCb1p1anLFmBmgDXfgUCniCnH4H+AWDAlQi9isB6pgBBYdDdNUtoNgweOkRatGrpgVA5uUqqJkdPeea+hkLNhf0BAxyHgLoQDHRKuSQY6KXFfc0MCsWGP//9+eeW+diKzucwYyhGBx4GPvgy9Mpax76C7fkA0kFGwAXrcn8VWf/xaP9xGGR9mFn7yg+DvO8n63922g8w0GZCCAK8bdtW10xY4yAwUka4oGEaBwGBpB2G7Tt0lI6dOsvRvoPvUw4KHX2tzLgAAvrv/u7vfApPYDNlXAF9CFwBoAlARkBgs05NWUz/ACbAMX0HvXv39jDwNb/LBgAHQMHAAGAwz7pBLkPoP6C/Bwnlx0+Y6DsIgUFSc0EBEXYgxmGQ21TIhUEcAvmcDwYs045FNcsVBjzhKMwMirs3ISl28py/XmF5zmE3787DiAlM3frwHC1CHwEMnNybzfxjuevSqVLsnyn0q8iZdS41ckSMwyCaT5Vx24bH9EHuludu45T0fmIwmDhgsjSp+mLqVf1VPxhscTBYLcNGAoNxOTDQ5gH3GET9BJ18UJMB0D9A+s48QU+GQHBrU4FlBCuXF6nhte2vtT+1Ox2LmCAHAGxP5gAwgADbARM9FusoS6ciAGJbjs/+2GbQwEF+uyPca4ZFM2R50OCh/n8g+Pl/4llCPhgwXwwMCmUHoeMwINh1eRwG7C8JBmQGDQEDf87Fz9mgAiILqNRKzZ+fLg6y4FG36gkDU5KaNDnLpdkXuNry07Jr1wuppcWrEAw4wXCxMCBwuOFImwdkBZr604GnqTpBCSRI17lkSBkCnwClzQ8QfJC65Zru04+g2QFBTsBTq9OcoKZnOzIDsgqCn+10f2QNbEd5OinZL/0P9DkAkKhJ0sL/LBvvn9ulFQjqHBgkdCAWAwOmOB78aoVAXTAIXQgG+6UDMVZR+nXpZVF2XN9k1WDQAAIG6r2BgsKAEyi6NyEDAzIgrFDYunWzrF6zyt+bMMKl1HEYaFZAZ2E00rCNr/kJRoKPeUx6T3ADBi73MQ8gtJYn0AlisgUCmOClhsf0GwAHgptanqCnnIIA6AADjsn+CHhgQSclMAJMAIEp74lMgiyEext439zpyHDpvn0HZMFAgRDBoLvvYwACGzaszQJCdr8BzzfIAFVBoNkBjsMgboJ/48bou9DXAALrfBwG117LaNL6wOCjl8GgARTCYG+gUBgG3LCUcRYMYpkBKTU3H7VrH4GArOAwF3xaixN4pPqYgCfICU7AQCbAazoSAQPLdDARTQgCn+2ZEvxkFUyx1vgEP0ENENg/mQaBzvbsE4BwxQIIABHgw/aAgm3atGkrRzVtJl26dvMm4OksbGwwwHXBIOwzMBiUkZJgoC4GCvmaCafUnuxOwOhBHWoPg9URDLhrMQ6Drt16ORjQPKiQY9q2kU9+ioFFHdOBTxCSDfBa7ztgGbCgyRAGPMHMtgQr85QDJBrANC9YRnmgQX8DQGCfBD6pP4HPPGUVHuyD/THlPTHPtl27VkjTZk2lTbu20rGii3To3MU3FwBAsTBQIIQw0MxKm1r1hUIIAy3LstAsY73BoMyVBIG4C0GhoWBAkHTo6IKoY4UfX8C9AE2bN5P2rhYnzaeGJvAIXAKVQMcELwFN0A4cONCXpdamLMBgqpDAZAWY7ShLs4GgZl/sl21pNlCGJgllyE5YDkQ0+2Ab5nk/wIg+Ct//0NI1Lbp0djDo7AcX6ZWFxggDLcO+DAamxODP54997BwZPDj7ykMIg6iZsNmPMwhhwGi6aERdraxavVKGjUzBYHgEA20iAIOOnbr6zsNPHXqIv4ZPIBLE1OKk75gmAKYmp1YmYKnFCXB6/dmGIAcElCFIAQPbUB5zOVAvQ9JHQE1PBkBfgwKEY7EMCGhmQlYCGJhqGeChHY0ArG2nDtLJ7Z9HrxH8uTDo4WGwYcN6/1QjnNtUwHx+Ub9LCAWFQRwIOASB+uST+S6iYNcyCgOm7FPXM/KQPgO+S/oM/ud//sdgUC5KCvokkx3Mnn2T/PjHv0ttGakYGGhNVwgGPXr2kvYdOkvnLt39swoOOexQDwNSc4KQgCfYNStQGGBeE5xkD6wHCABEOwCp5ZkCBAJXTa3OlD4Hgln7CjgOx9TmBhmCNi2ADq81G2E5mYF/Lw4yR7mmQmvXxOnsYECGw9UFxh8cHDDYkgOD4i4tfvRqEl2K4BJG6pKFd+wSh7/GWb/RTNmK9p89aCJSdI0/fkmlCMWu9Scr6XJNXAUGHRWppMAPnQ8CqqRmgofBKZl0Nw0Dlx2sXLUydTVhrIcBP3rKlNS5fcdOvr3drEVzH3QEGTWy1sQaeFrLU6MDAgLRp+ipGpwpVxkoo2MQ6FPgtuQhQ4bI8OHD/T0GzJMZsG86D/VyJM0AQEIzgIAnA6AM65gHEJj3gdkHJuMAJECmc2eynC5+JCWwwwCBcQjaTAAA69atyYJBNhBI78PsKrvJkAyF6BKuml9JAgbcn8A2ScDgO+MYTHkQ67XX7vIwKH7Q0UevAAaZAIgPaPCjn1zA7P0oO4VNPMii66GNCwb1VxIAcF0QUO0zDFyQ0OtOLdqhk2trV3SWw4443AchgU9wUUNTaxOgBBrWtr/W/BqYQITABQ70IbCeJgEAwAAAEGBuT+bmI8BBcBPsBDQBz/YchynQYT96GRGwkE0AIfaNOTZTyrdwMAMG+tg0/T8bGwx0m5KFQXagpYY6FizDy2jUX9YdfAyEiMZH+m1L9xbmoc71h4AqfwdicTAY6YKE3y3wg4xcAHXo0snDABBoLa2X9gg0gpXAIzgJYAKflJ1sQDMHymEgQfAT7DoyEUBws5E+vIR11OqMM+A4BDzHZUwCU5oGHBsocVyFEcfi+IBDxz9oOZZxaZQxEwQ+zz0EBABBOxAZdKQwUCAkwQBnAyEbBnEwKATorMUMAd+woTAM2K//ftIwiJoJpZUZcOKn5jM3FznVGYAKEZRaFtsmupEiODbrC72PYF3OvQl1wiBQ1r7imUER78cvz0AvAsKGekNAtc8wcEFC56EfW+Bg0K5TRzn08MN8UGugk/oTmAQgy0j9yQBYhhkQRBufm5ZYDgTYniYEtTgZgA4q4jZlBYIOP2YdWQOAIaCZMrCIoKapoKBRk6EQ9GQrHIfjkT2wL+17iGDQzQMBABwoGCgE8sEgUy6TTZQYDLT2C4LMiQBIAyBd0zvlDUCnVG2a3i4IsFK8hfnj/z5cmnQ6s94QUCkMuBxVFww46UIYDB1G5yFPOh7kgqfCBY5Lt10gEoQEGM82JPgJQJoKzAMG7dmn1qZG5koAQcugIKbacaiBjgECYAAITOlkJHAVEMCBKcch3aefgv0DHu2HIMgJetazT91OmxujR&#10;7ssx4GGMQcVFV391QRgwANaQhhwyZFmwvqgmRA2FfLBIHLUXOBzDp93kO3sZWEzIak8gGE907ADUS8tHrzNhLRYHkAiDKK9hIECJZNlFBF8bi5nXX1hkDpupGB5A8Age9/1V0PAoE9fMgMXPAxFbt9ODjv0MJ/mAwWCkdpa+wtIzQlMwMA8wUuwkkEABspTjqyA9j3NA0zQ02TQoCeoafMT1GQINBdYx3HYjiYBtT/gIGsg0MePHy9jx46ViRMn+umECRPSrydNmuRfc6w2bY5x+44uLQID7sJsOBhEGUIEA5wEg2wbDBJO8nSAEBAFbxsO9xnOR2Uz2Uewzu8z8z44Vvr4vJdgXU4z4SC+hZnfWuQZiNddp5cWIxhsSuhA5EReuXJFfhi42pRBRgQ2MNDUnyk1NU0CmgHaNmeqqXsICAKZYA9hoHclkhGQMQAHgAEMCHjW0Y/AfoAE5XlN53NNTY1MmTLFT6uqqmTatGl+uU6rq6tl8uTJHhgct3lzRj92li6uORDCgMuodCACAx6EAgDWrl2dbiokdSKGUNDPEIcwCB0PdLxhA/sCIplMItyGy4pJMNB7Ew56GGT1EaiCQPAB6bOFzG3DLNOgywRzbP8ueNKBmbAunYWEAeeUOZ57X+7DD9cnvZfsfTOfKuO2DY/p36dbnruNU9L7icFgX29hbggY9Os/2MOAwCG9p6YHBtTypP7U+PQL0ARgMJFmAJTTZgRw0LY8+yCV16AHCkwJfGBAhkDwc8mRoNflQAHYENDsh/4Ayk6dOlVmzJghc+bMkenTp8vcuXP9lNcAgXVkBWQc7JN98HxG+gb2FwwyUNh7GOjrkoBBamraB+kVhL29hVlhkD3oKBsCelJz8q5YeVIAA8YZjPbBon0GBCKZAUHFlP4Agp/efaa04zGvta9Ae/5pVpAt0N8AAAACEAAKZAe8ptbnNTBgGccDCICAPgTgwrZkIoCHrIR9jho1SmbNmiUzZ86UBQsW+On8+fN9VoC5MsGVDpooHgadXGbggr6Tayr0HzTY/64kYyq0mVBJM6GeMFAARB2JkRUCSc7AgKco0UzLHdqM6YdggBPrL7pohzsPdvkm30HVgZiamvZB8cuK9YVCLgy2uBOPUYeZkYfhSZ0NgxG+tuTSItfkuzgYEFDU+MCAqQa/Bn0IBgIWAPDwUqZcAdCMQTMCAEDQky1g9g8QMJ19ZAQsZ0rQa7ZAc4PABgbab8H+aA7MmzfPNxmAA80E+gw4HtvSyXjUUUf6KyOdgUG37jJgyFAZPmq0B19Wn8F+hEEmC9ji98t+om0yTQvKYP2uWH/RRRcaDMpVIQz2BgoNAQMChHQaGBC81PI0E5gSiAQ9Qal3CgICmgSsp4lAU4IsQpsQlNXsgKAHAGQA9BEw5TXHYUrHoTYZyAoIao7B/tiWLIJygAEgME9HI00H+hHoQORqAk0KLUOnJsOROzsQ4IFuvf6uZEPAIASCBnbcxcCA5kHURKgbBvyAyovL18i3qubIC/OPs19hLkUlwUBdDBRyxxlEP7xKWqonc3hSKwyGOxgMGTbcA8E/JNQFCGP7u7lUnVqZGpZAp8YnE2Ce4Gc5gUrgspyBQpo9UBZgaB8CHYkAgSBlnmYAEFAQ6FSbCIACiLB/IARwgASZhZZh3xybsnpZkayCcgBMr3Z06+7+HweCLm46ePgIGeYyAwWfwgAAAAMFAnBQIACDEAj6+akVBlq7qzXQ1SEMMhCJyioMWK773LHjgiwY0Gfwp5/+TJ4cMF4eat3d/4jKQ/yQSs/h9ivMpaYkCMRdCAq5MCDtzAUBJzbLk2BAkDCG3w/fdW1sfqTEDx5yAUxNS01NgGkTgMAnA+A1QKAsywAFtTKvqdUJbKYEK0FP7R2aAKaJACjo/MPAhO0BARkGxwUmBD/7IYtgnvcBcBQulAEGurx7j55S4UDQo3dfGTZylHcIA/oMPjoY0HcABPTBM4Vh8L0tZ8qedn08CEI/NawqdRZ89DIYNICSgj+f67qFOYJBdG8CVxMUApgTnNdhM2GIA4HCoF+/AS4z6OYCpZeHwiGuduauRbIBAp3AJAug74BlNB0IVJYBAYKXWpkgZzlBrtkF8wQxwUztDiAIaJoALCN7AAg0I5iSGbAdxyQL0IyEYAcq2vzgmGQjQAXwcGzKRk2UHv5qAg84oXnA/4j13oTKqsk5MFAghE2FEAoKAoJXHYdB6AgIm/0+2YfCINweh4C58MLz5ZprrpHzzz8/DYMn+o/NAQF+pNMA+xXmUlJS0CeZ7KCuW5jrC4PBrnlAEwEgRP0GLq2uiILoiKOOlDZt2/igJOAAggZomC0QpAQgAUk5anVqal0HIAAGQa1NBW0uAAQCnLJ0QnL5UpsP7J9t2Sf7Z56sg31S8xPwvA+WU5blZAcAgu352Xf+DzpHgQFDrxsDDMLgj79OggHPM3h+3nG+aZALg/6ps+Cjl8GgAZQU+KHzQUBFB2LSCMSNm6ITOrSHwYqToqcjexgMS8OAQOnRs48HAsHCY894UEgHF8AacBp8BDyBR5ATmNTkBC3Lwhqcsvy2AgChDK8JbJoXbIvZTmFDGcz29BMAEJojbMdyXmOgQNCzL6a8H2CkTYQePbq7DKe7ayr0St+xyP/HNBcGq3JgoE0FrCBQa+CqM+3+yPTXMM1Aodbvj/1E5TPBr9Z954MBv8L8WPdhWSB4rGKQvLnrhtRZ8NHLYNAASgIArgsCqoaAAR4+YqQMGDjEBUpP/3ThCpfCH+6CjdqbACfoCEjmCW4CkCCm2UAgEqBMSfWZkrJrwBL0bAsE6BPA+uvM7IPMAJBoGQKefSgQKMM6yrEv4KB9E6wHUvzQC8fyzQ/XjOjqAr6Pa/qETYSDFQZcTaCz8NGuQ9IZwdtfvjV1BqBgUFw4wK2g4qNn900GgwbQ3kJAFV5aLAoGrpnAk47oQNRmgppalB8joTORTrajWx/ja1oCnODTqwcEHZcRCULmCVJSc0BB+s44AZoEGuzsI8wgCHimbEtgpwPabU+AMw9wOC6XGtmOMkzZRgHBcTUrOOJIriK0dtlAH9/v0bVbTx/4hWCwdu0aWbNm72GQL+XXeUyAa9NDy8at+6Zs1IGYC4PCyh75Go7kzS+DQaPT3kJA1ZAwIGDoOyC95iYfgubII6MHjmgwMrBIa2gClppYswCAQFntCyCoNTugwxHr4CXmtWnA/tgH21IW8PCatF+vNGhTgf1xbCCAFTSHHHqEH1LdrXtvn9307NXX/T+ZB742RhiE+8UNBQM/DD68FyAcFu+Xp+7xSS3z4IgNk0/fj5NavpMh+6xPvc48jyM6rsGgAbS3EFDVp5nAiX3SihNzOhBDEzAMT+YZB31clsCVAAKRHnyClEAkxSfQCUaCFpPma03OVQLWadagNTi1OdtrnwHBznbarAAiZBYa5GzHcTp37uybDJpdhPtl28MP53mIZAV9PQzIDKKsIAMCsh6cBQMX/GtWr9wnIORzEgzCfYTW/SsMrr766jQMXnzxxSIeiJqbGaRZEAvyzLpYZlAIBi7os/anAHHSLMRg0AjU0DAgcLiph/kBAwf5S3/UxgSqZgjU6NTuXBUgcIEEANDRggQ0mQGBruMR2EZHJ4ZTtqc80CGzIPC1P4DlHJugZx0Dj/Q1gAIEzAMHmgUKgt59Brj/g+ZBJjM4GGFQ/NORs/sMwqRAb6QLHQVyPWAQLM95ncpCDAaNQCEMwrsWFQbxk7ouGNBM0EeLE0Q8GKRX774u6Fq6IOvrg50amva99t4TlNpXoKMECVBqf+0z4DW1OmY5U4KedQS6XnIMswPAw3ptcrCOJgOv27bh8WdRpsAPxHZ3IAAG9BkMHsLVkUxfQeikZkIxMNgbIGCAwD7ZT7g91v3qMSgbdSBmYFDvzCBfUOfIYFByamgYqMeNnyhjx03wYOBaPR2KTZu18DcX0RQgrafGp3anXU9A+448F/RkBAQx/QPaL8C8QgCIKCy0CUIzAAMUgp7llCHwgQsZAcfkWGQPrY/m9xoPlbbt2kt3Ojy702nYyzVxGFcw1gV+mcLAKQsABG/Yn5BWAgySnuthMDh4VJ8OxI0b&#10;18uJJ51QJwzIDgiciZOqvAECGQI/bKo1uTYHCFSCmuVAQDv69PZjghgDAIUANT7lyAwACM8z0Ccd8UAToKAZCPvmGFivVrRuHQ10atasuXSjs7NHb6lwIOjjmgfDhjPACGeaBgqCTDOB5xlMljUOAKtTzYR8QMgHhaTAT7LCgP3o9urs7yYXBvQZvPTSS/WGgQazNheymwrZ0Mg0G4hrLRM818NgcPBof8AAa3OhavJUbzIFAomgpEYmgAlaOg2BAADQ4Kc25zX9CkzDDkMAAAjYDwFNcOsDUnmOITceMU//ABkCQKDJwLaAg30DEx7gSkZQ0bOPc1/p2bu/8LCW/QmD6DOsX4ZwYGDw0ctg0AhUDAz0pK4PDDBA4NeIqqfPkGnVNT6QBgwY6NrlPeWII4+STp27+HsOCFDt2NMrBpjApzZnSuBjhQHlmRLwZAUAARjwnEPMcw/ZN7dVA5tj2vAbDq0dCLjXoZO//EmnYUX3ntKjdz8HAgKfgFdHIAhhgPkf/I1KwGDNGv/bk2vWrPaOwyAMVrUG8t7AINxHuE8FhcKADsTzzjvPNxOAwQcffJD6thuvDAaNQNpnUBcMOLk3uvn6wAATPGQFM2fNkZoZs2TylGm+c7H/gMHSouXR0tLV3GQItON9x55r1wMFYEBGAAB0qp2CQIAan3Jsw1UIAp+nFQGEkSNH+icbkSHQXGjbtp00bdZSmjVv6QDU1TcNCGjcq3ef9HDqEAChC8MAENB3EN2n0NAwwOw3hEHcuTDYKeeeazAw1VN1wYATbV9ggBUI8+YvlPkLFnko8JrgqugaPaoMCNARqHcgEuxcAcCaEQADzRJYrtkEZWkCABRMByVXFmhCsK82Dgatj2krDJNmdCRTAprRknWBQJ0Dg8oDA4OkzCBug4GpQZQfBhkIKBA48U448fg6YUCAxZcRRDQZ5s5bIEuXHSsLFi6WKVOrXUo/wbfz6TsACqT01Obdunbzy2gmaJNAswJAQDldzpRyPJmZR7UzZdxBt25d/dWFnj0ZO9DPX+IEBpiBURrgScGv5n/BzIcwmORgsHr1Glm1apUHAq6rz0BBEAdCISgoDPLBBXOMXBicm4aB9RmYilL9YVB3ZpAEA0wgEVA0GY497gTvefPmOyhMlQkTXKYwfITLDHq4pgMpfRT8CgdqfPoHCHIgwTxTXndwU368pbVrMvBgFW4/9g8y6d/fu2+//v65BPyiMhDgvdQFgSTnwmC1g8FKNwUIxV9NODAwyGQGBgNTUaoLBpgTUU/GYmBQyHQqYi45Llm6XFasXO32ucI3H+hkpC0+bsJ4t+8h0qVrhbRqHT1/gOYAWUCzVHag/Qc+S3BNAH4OvnuP3v5OQ4KeoGV8A2ZemwNYYaW1Pk4K/iRnwWDNKgeDFQ4EACH/SER1PIjjQEgyQMgHg3Df7IuyIQzsaoKpXjrQMNDgI0gJLq40kCGsXrNOVq5a4wCxVGbOniXTplfLqDGjZfjIER4Oo8eOceWHue0GyYCBroZ3Hjh4kFsW3Q+hIx7ZJ/dFMGVZOPiJ9eF7oUzpwuBcg4GpfgIGyZcWIwjoiZiBQd19BsWaDAEoEKRkBcuWHyer17pjueOsWrNa5i2YL7PmzpGaWTNl2owamVI9TSqnTpFJU6pkQlWljK+c5DxRxk900wmTZMLESpdZTPHjGrhqwfyYsePT2QAOj58U7HU5HwzyjTXAcRAQuGpeJ0EgNPvUzx8rAHT/uk5hsHPnTjnnnHOsmWCqnz5KGKgVCswT2DNnzZJlx7omxCoXYOvXyep1LmtYu0ZOdO3zY088UZYed6wsXr5MFi1bJguXLpGFi5fIggWLZc7c+TK9ZqYPVDKBePCHrm9GoDYY7B8ZDD5Cffjhh36qzYTcuxYzMFBzAp5wQgSDYaPH+CBOCrT6OKyxFQoMXWaegKa2nzptusyaPddfiaBvgUuUBD7LaGaQCYwdP8E3C9gX28bfmx4n7qSAz2fKZ2BQ5SBAB2LmakI0AGlVFgw0cLEGcwgDtQZ+/HUSDPR7Yao2GJj2Wo0FBoVM4AEHPGjQUP86n5MAEDoJAvnKxSGgZh3H2h8w0HW63mBgOuDK20zYmElDMzBYK8efcJyHwfD9BAMNagIvaf3eWiGQtE6tZeIOgcB7C2GwcuXKAAi5MMBxGCQ5DgV9HcIgBAsOj8M2cRhYB6KpXmqsMEhaty/WwE5apw4BELqxwSD+vTDPNgYDU71E8wDrENVcGGz2MDg5gIGeeCEMDkQz4UA6CQJJDpsJBD4diOHlRQI3DFIN4LpgkM8AQQGjIND9p71+tSu71sOA7/CLX/xiFgx4BqJ+741VBoMDKD0Z1PsKA//zavUIooZwPIAb0knHS3IEg56NDAarXFmDgalIEfxxo1deeSXWgRjBIGwmZGCwzsNg+KhRMnTUaOk3YKDv+c9nvTKgnYDpzsBUp18+a1OhWMeDOnwdLi/ksCmQzzpoCRhEdy3SgXiShwABixUG+rmFMMBJAY+1WZC0XPcbfhdZTsNgUxoGZ599tu9AfOON6DEiBgNTWvlgwM+r7dixw8Pg4osvlu3bt8mWbTyMM/uxWpzI/DIzj0ofMHiwtGrTVpq1aCUtW7XOcatWx0iro7lPoJ1323YdpF37jn6+Tdv2cnTrNnndGh/TNtG6v7TdvjD7bJuaZtwhy+3adYzs3gfmqUuF3KFjZ+nYsYub4s7pad9+A/0Ix6opU31ghgDAcQgkBXd9vGnTBr9/hQHZWRYIsMLA35twoYfB6aefLnfccYf89a9/9d9z+J03RhkMDqBCCKipKf785z/72uOqq66URQvmyeKF82TB/Nkyf+5M51myYN7sjOfPkTmzZkiXLtxN2NLfdszzCFq14v6Bo6Uldxg667TVMQ4Kx7RJ2ZUl4N2y5txroG55tHeztFtl7GATzbeM3AK3kObOLdx88+bcwtzCL2/qyjb1U2e3rGnz5pGbRWY7b8qH69PlmslR6qZNs3xkanoEt1A7c2PUpKoqWemyghNPPFZWrDheVq48wZkmQ+bGJToVMw8+4RZnF7hrCebQXBUgC4u8Yb2DhrcDifPGkzf4fbGtlsnePvKGDYBmox+GfMUVV/gnHfH8Q0QHIuY7b6wyGBxA6cmgJ0b8BLnhhhukpqZGZs+eLTNmzPDzOp0+fbrMmjVLpk6d6ud5kMjo0aP9vQOdu3WViu7dIvfoJl2wm2d5525Mu3t36d4j5Wi9Wten3T322u+DfXWVTt0qpGNFFxk3frwMGTLE383Icwu6pMp26trNuzPLulZkuYL36ZzvtV+m8+4YoTunzDr+5+49e0jPXj2levo0mTptikyZMlmmTZsq1dXVztNT02iezwvrfLQ+s5zPN/qMI9c4z6h29lO33pWZPHmyO8YUd4xpztVph/viu+I7Oumkk+S2227zfUGI/gIcfteNUQaDAyhOBj0x4kY3fOVmGVc5VabNmCNTambL5OqZUlU9IzWd6aeTptb4+ZHjJsnQUeNkyKjRMnjkyMijXHvaTYeOHCXD3HKGK2MuQWKWYZ2nHB4ywm07fERBDxo23Hvg0GF+ynMRhrEu1WcwyC1nXdpDhsrAwZEHMa92rwcMHuKmQ6LlzAfrWZf2oCHSf9Bg52h+gJtn+3ETJvnjjXD/w/wFC2XW7Dkya47LmObN86Miczxvvh85yTzTuHPKO8+eM0/muumsWXP87d4MseaBMIy4DE05zDYLFy3xd4GuWr1OHn/8Cf+9Evw0E+LfdWOUweAAihOBEwP/5S9/yTK6+557Zc78RbL02BNkyfLjZfGy42SRO7kWLsHLZMHipTJ/0VKZt3Cx8xKZy70ArvzseS4g5uIFMmPOfO/KqdNl1LiJUjXN1Wqu5lJXudps3KTJMt5Bp2pajS+nZVhXOXWaVDJVu9eT8JRqmejsp5Or3fZTZILbx8TJ02RC1VT3mn1WeY+rrJSxk6qcJ8uYiVUyekJlokeNn5T2SPdew/kRYyfkeDg3PI0Z7yDoIOfmR4+vlJFj3XYOjHi0n/I6Mq912Rh3PP//pv5n3jPL9XOYMn2mt98n27v3oe9xdMrsI+6x7v8bO3Fy6jO&#10;d4sstXHqcvPnW274J+N577/nvV7933FhlMDhA4sTgpODkwO+++643/QWY+XfeeUd+8YtfeP/85z+Xn/3sZ/LTn/5U3nrrLXnzzTflJz/5iff//u//etPP8KMfvSE//OEb8vrrP5Yf/OCH8v3XnN302eeel9POOMtBY5Ecf9IKWb12vZx6+hly3oUXyvqNp7hlK+Wzl10un/mP/5Jrd39Jvv2d78iz335evvmt5+SZb31Lnn72WXnqm9+Up575pjz59DPy+FPOTz7t/dgTT8kjjz8pjzz2hOx59HF52PmhRx6VBx/eI9946GF54MEH5b5vPCj3PvB1uee+B+Sue++TO+/5mtyV8p0Oerff/VW57S7nO++WW++4S26+7Q656dbb5Su33CY33nyrz5K+9JWbZPeNN8n1N9wo133py7LLvc+rr9stO3ddJzuvvU6+eM21ctXOa+XKL+6SK6+6Rr7gfMVVO+XzX7hKLrviSrns8i/I55wvu/xKOevc82Wyg95cl0ksO/Y4WX9yrXz2c1dI7ZatUjNztox12cZoB5hzzrtAdnzmUjn/oovdZ3WR97kX7JBzzr/Qrzv7vPPlrHPOkzPPPlfOOOscOd2Zz3m795lSu/VUf8x3383+nrF+941VBoMDJGCgwa/TP/3pT/KHP/xB/vjHP6anCoc///lP/jVl1LwOy/7xj0z/4F//4fe/d9PIQIUa6PXXX5eFC1wq7FLoZcuWCZ1kdG7tvmG33HzrLXLTLTf76csvvyy/d9v/7re/zfh30fS3eZxV7ne/C6aR/+//1P9Xp3m/hfz732P+t9T/Glg/k3z+kzOfJ5/LtxzguFLDFYJd1+x27/MdueqqK/wVHHr/L7vsMl8OaL/7bvQ9vZdyFNCpZe+FwR0t92VS3y3Lw/cXfoe8l8Yqg8EBkl41CE8MThZOcsxJT2BoMCUF4G9+8xv51a9+Jb/85S9zsoe33347bTIJ1j/rTv6pLsWn86umZqocf/xy2bDhZLnm2l1yy223yp133ymv/uA1edttT5bx4x//OOMf/Sj7dR3+kSsfulCZpHWY95DPdZXRbAmH2ZNmU/oZcXVh9uxZcuqp2+TOO++UCy44z/f8P/30074MnxufKZ8t1kyNz5zPHv/617/238Vvnf3UfTcRACOwhaALIaZgaKwyGBwghZmB1lohBLBCgBNMA58TUZsKnNwExA9/+ENf6//gBz+Q1157Tb7//e/nmAC56667ZNSo0Q4GVbJoyWypmTFF1q5d57OBc847VyZWTpJTNtfK1772Nb8Ng5/oAWfcw6uvvuqX6VTN8UKH65LWh66rDP9PPtdVhs8jyXxWmM+ODGj58uWydOkiOeHEZfL5y//LgeBy+fKXv+x7/7/pmkU0x+JwwQCFdXwPGLAADRzCIg2KFCCizCYCgsHA5KWZASeEZgQAIKz1OZG0ZuLE4yQmGBnO+h3Xpv/2t7/ta3tOWmqyp556yvvJJ59M+4knnpDHHnvMr//c5z7nYTBhwgSZM2+aTKoc4zODa6+/XjZv2SIrVq107d3TZdeuXXL77bfL3XffLffdd5889NBD8sgjj8jjjz/uzT51/3rMfOa4+fzMM8948/7rY/5n9be+9a1EP/fcc4lmne4D6HEJcfKUSlm0eI5s3bZJLrnkM/KFL3xBFi1aJP/5n/+Z/p95n2z7/PPPC7+izHgBvgcMVDDwVHACKuADrAEHwCCz4DsFCiEQGqsMBgdIYTNBswKFgWYBnDxAgFSaE4wTkJORE5mTk2Ak0B999FHvPXv2eD/88MM+gL/xjW/IAw88IPfee6/PCm688Ua5Ztc1/pd9lixdIhdeeIG/B+LSSy/1qfHu3bt9GcY3fOUrX5Fbb73Vb3fPPff4wLn//vtzzP6L8de//vUGM/9X3A8++KD/n/nfk8x6tuU9f/WrX/X/15e+9CU/1Pukk06Q+fPnyim13Edwnvz3f/+38AtIfA6Yz4Ft2JZj8JkrCBVqIaiADt8TwOY7I7siWyGjIKsDCHzPZAkAobHKYHAApVcTaC4oFDhBqDlIMzlxOIE4kTihOLE4wTjRtIbjRNSaVmtjajJqck5cApFgpqYn0DnRN23a5H/67MQTT/RDZC+//HK59tpr/Yl/0003+bKc/GQFBBCBpMEWt64rxklB3FDOBwnMOj4HgEa2w//HPQI33gjwbnGBf5WHwbnnnu2yp8/KlVde6W8So7lAOYYQ8xkCA/YHcIFvmCnxufNdKAxeeOEFn0GQLZAlkCHQlOB7BfaaHVhmYPIKRx3qmAMFhGYN1BwKCG17hs0I7cQKOxGBiLZ1SVW1acHJCgxWrFjh28rc909GwIlMkwPYcPLSjgdANEu0ky+fw/Z42FbXNr1a+weSTGodGvAVa95vaE3ZQytEASj/K6AkqAlw7iScNm2KVFR0liFDBvksgQwJcAARypIFAF4+I63tsTYTtKnA8Xn//E/8z3x+fAdAgO+F74zvEgDw/eqVhsYqg8EBFKPRME0GpiEcQudbB0B0H/EyrNNpaI4Vite6LhwIo2Cqj6NLa/U3QbG/nQRXYErgPvLIQ66mp3n1oKvNX0gHLs02TeUJYLZnP6HDZfH1HJf/Tz8fPlM+5/h31FhlMDiACgM5bj1h9tYa4OpiyqhDICS9zmfWhyd+WF5f74sL7UePU5fD/5/POUkAUtcX+32EZevrxiqDwQFU0omBORnjTipXyEknbJKTAqaQ49uF+6preVIQJ1n3EXd8n0kutE18Xe76yOGyYp30HYQu9D02VhkMDqDynSC6PHS8TH2cb/ukk7pUXeh/Dj+TYhz/bkInlcfx9eE2jVUGg49A4YnRUE466XSZed8cfqZ764NBBoMyUPyEDE/SuhwPiDBIQte1fl+t+9+fjivf8lKVwcBkMnkZDEwmk5fBwGQyeRkMTCaTl8HAZDJ5GQxMJpOXwcBkMnkZDEwmk5fBwGQyeRkMTOWpv70n739np7z3lUp5/5nPyIfvvZNaUb4yGJjKTw4E790wSt67or28e9nR8u7n28p7uwbIh79/M1WgPGUwMJWd3n9qhwNBhwgEKf/Z+S+3TE+VKE8ZDExlJ5oGIQjSdhlCOctgYCo7/fX+FYkweO/qnqkS5SmDgans9OFvX5f3dvbIBsFVFfK3V76cKlGeMhiYylIfvPWUB8C7V3X2zYO/PndZak35ymBgKk29sVMqmzSRJk0qZecb7vWeWmlSuydaF6jcryCEMhiYSlBvyM7KWiH039hZG8EgWGZKlsHAVILaI7WVO134GwzqI4OBqSS1p7aJ0CpIwyBPM8GUkcHAVKIiE6DPIOVUpmDKL4OByWTyMhiYTCYvg4GpBLVHaptkOgvpP6CpYF0GhWUwMJWgMlcT6Dis9D2IdjWhLhkMTCWpN3ZWpjoPUwBgEJJ1IhaUwcBkMnkZDEwmk5fBwFSSyjQTQlufQSEZDEwlqMzVhD21qRuVWGaXEwrKYGAqQYX3JlSmLina1YS6ZDAwlaT03oQoS0g1EywzKCiDgclk8jIYmEwmL4OBqSRlVxPqL4OBqQQVDEc2FS2DgakktafWsoD6ymBgKiEFVw4SbYAoJIOByWTyMhiYSkvpR6Q7W79BvWQwMJWQGGWow4+d7CGo9ZLBwFRCil9FcHCoteygWBkMTCUkg8G+yGBgKiHZ1YR9kcHAZDJ5GQxMJpOXwcBUUgrvSeBCgj4mPf1rzKa8MhiYSkj0GWi/AJcZm6Qek47inYumuAwGphJSdsBnnnKE7ElHdclgYCopZZ55GJP9bkKdMhiYTCYvg4GphJQaZ2BDkPdKBgNT6Yl7EuwKQr1lMDCVsKIrCh4M1l9QpwwGpvKQv7XZriYUksHAVNJKDzqyzKBOGQxMpad0n4H+kIqpGBkMTCUku5qwLzIYmEwmJ5H/ByXWe6rYdaR1AAAAAElFTkSuQmCC\"></TD></TR>
  <TR>
    <TH>Identifier:</TH>
    <TD colspan=\"3\">CoSES_Models.HeatGenerator.DigitalTwins.WolfCHA10_GC</TD></TR>
  <TR>
    <TH>Version:</TH>
    <TD colspan=\"3\">1.0</TD></TR>
  <TR>
    <TH>File:</TH>
    <TD colspan=\"3\">WolfCHA10_GC.mo</TD></TR>
  <TR>
    <TH>Connectors:</TH>
    <TD>Electrical Low-Voltage AC Three-Phase Connector</TD>
    <TD>lV3Phase</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal Volume Flow Input Connector</TD>
    <TD>Return</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal Volume Flow Output Connector</TD>
    <TD>Flow</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Environment Conditions Connector</TD>
    <TD>environmentConditions</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Modulation of the heat pump</TD>
    <TD>HPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump on or off</TD>
    <TD>HPOn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump mode: true - heating; false - cooling</TD>
    <TD>HPMode</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Modulation of the auxiliary heater</TD>
    <TD>HPAuxModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Parameters:</TH>
    <TD>On / off button of the heat pump (if switched off, no power during     
              idling, but doesn't react to commands from the control system)</TD>
    <TD>HPButton</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum power of the auxiliary heater</TD>
    <TD>PAuxMax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal heat output at A2/W35 (not maximum heat output)</TD>
    <TD>PHeatNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal COP at A2/W35</TD>
    <TD>COPNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal EER at A35/W18</TD>
    <TD>EERNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Wolf Heatpump CHA</TD>
    <TD>wolfCHA1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Adapter to Green City connectors</TD>
    <TD>greenCityConnector1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Results:</TH>
    <TD>Flow temperature</TD>
    <TD>TFlow</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature</TD>
    <TD>TReturn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Output temperature of the heat pump</TD>
    <TD>THPout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Ambient temperature</TD>
    <TD>TAmb</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of HP</TD>
    <TD>QHeat_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of the auxiliary heating system</TD>
    <TD>QHeat_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cooling output of the heat pump</TD>
    <TD>PCooling</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric consumption of the heat pump</TD>
    <TD>PEl_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric consumption of the auxiliary heater</TD>
    <TD>PEl_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Coefficiency of Performance</TD>
    <TD>COP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy Efficiency Ratio (Cooling)</TD>
    <TD>EER</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated electric energy of the HP</TD>
    <TD>EEl_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated electric energy of the auxiliary heater</TD>
    <TD>EEl_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated heat energy of the HP</TD>
    <TD>EHeat_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated heat energy of the auxiliary heater</TD>
    <TD>EHeat_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cumulated cooling energy of the HP</TD>
    <TD>ECooling</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Modulation of the heat pump</TD>
    <TD>HPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump on or off</TD>
    <TD>HPOn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump mode: true - heating; false - cooling</TD>
    <TD>HPMode</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Modulation of the auxiliary heater</TD>
    <TD>HPAuxModulation</TD>
    <TD>&nbsp;</TD></TR></TBODY></TABLE></DIV>
<H2>Description:</H2>
<P><FONT size=\"2\">The model is not completely validated with measurements   
yet.</FONT></P>
<P><FONT size=\"2\">The model represents the CHA 10&nbsp;heat pump&nbsp;from Wolf  
and is based  on the modelica model NeoTower2.mo</FONT></P>
<P><FONT size=\"2\">The heat generator can be switched off&nbsp;completely by   
deactivating it via the parameter \"HPButton\". Otherwise,&nbsp;the heat pump   
runs  in idling mode.</FONT></P>
<P><FONT size=\"2\"><BR></FONT></P><FONT size=\"2\">
<P><FONT color=\"#465e70\" size=\"2\">Input/Control signals:</FONT></P>
<UL>
  <LI><FONT color=\"#465e70\" size=\"2\">HPOn - Boolean value to switch       
  the&nbsp;heat pump  on or off</FONT></LI>
  <LI>HPmode - Boolean value to switch between heating (true) and cooling        
   (false) mode</LI>
  <LI><FONT color=\"#465e70\" size=\"2\">HPModulation - Value of the modulation, if  
           it is below ModulationMin, it will be overwritten by that     
  value</FONT></LI>
  <LI>AUXModulation - Value of the modulation of the auxiliary heater - the aux. 
          heater can be&nbsp;controlled in&nbsp;4 stages: 0%,&nbsp;33%, 67%,     
  100%</LI></UL>
<P>The heat pump is devided into four sections and based on measurements from   
the CoSES laboratory:</P>
<UL>
  <LI>Start-up process: The     behavior is provided by a time   dependent       
  look-up     table.</LI>
  <LI>De-Icing process (only at heating): If the ambient temperature is low, the 
        air humidity can freeze on the heat exchanger of the heat pump. 
  Therefore,     the   heat pump goes into deicing if these conditions are true 
  and uses hot     water   from the house to defrost the heat exchangers. The    
   behavior is     provided by   a time   dependent look-up     table.</LI>
  <LI>Steady-state process:&nbsp; The steady-state     efficiency depends on the 
        return temperature and power   modulation and is     defined by a       
  two-dimensional look-up table. Load changes   during steady-state     are      
   modeled with a rising or falling       flank.</LI>
  <LI>Shut-down process: Similar to the     start-up process, the shut-down      
   process is provided by   a time dependent     look-up  table.</LI></UL></FONT> 
<P><BR></P></BODY></HTML>
",
			revisions="<html>
<ul>
<li>September 8, 2017 by Bram van der Heijde<br/>First implementation</li>
</ul>
</html>"),
		experiment(
			StopTime=2290,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.001,
			__esi_MinInterval="9.999999999999999e-10",
			__esi_AbsTolerance="1e-6"));
end TES_BU_Validation;
