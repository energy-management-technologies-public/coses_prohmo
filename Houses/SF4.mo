﻿// CP: 65001
// SimulationX Version: 4.2.3.69223
within CoSES_ProHMo.Houses;
model SF4 "House 4 with a StirlingCHP and an electric heater"
	GreenCity.Interfaces.Electrical.LV3Phase lV3Phase1 "Electrical Low-Voltage AC Three-Phase Connector" annotation(Placement(
		transformation(extent={{145,-315},{165,-295}}),
		iconTransformation(extent={{236.7,90},{256.7,110}})));
	Modelica.Blocks.Interfaces.RealInput UnixTime "UnixTime" annotation(Placement(
		transformation(extent={{410,-30},{370,10}}),
		iconTransformation(extent={{266.7,130},{226.7,170}})));
	GreenCity.Interfaces.Environment.EnvironmentConditions environmentConditions1 "Environment Conditions Connector" annotation(Placement(
		transformation(extent={{370,25},{390,45}}),
		iconTransformation(extent={{236.7,190},{256.7,210}})));
	Modelica.Blocks.Interfaces.BooleanInput StandardCBcontrol "If true, standard control will be used to control the condensing boiler" annotation(
		Placement(
			transformation(extent={{-140,-105},{-100,-65}}),
			iconTransformation(
				origin={-150,250},
				extent={{-20,-20},{20,20}},
				rotation=-90)),
		Dialog(
			group="Condensing Boiler",
			tab="Standard Control",
			visible=false));
	GreenCity.Utilities.Electrical.Grid grid2(
		OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
		useA=true,
		useB=true,
		useC=true,
		useD=true,
		useE=true,
		useF=true) "Electrical power grid for connection of maximum six 3-phase AC components" annotation(Placement(transformation(extent={{65,-205},{105,-165}})));
	GreenCity.Utilities.Electrical.Grid grid1(
		OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
		useA=true,
		useB=true,
		useC=true,
		useD=true,
		useE=true,
		useF=true) annotation(Placement(transformation(extent={{135,-275},{175,-235}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap1 annotation(Placement(transformation(extent={{160,-190},{170,-180}})));
	GreenCity.GreenBuilding.HeatingSystem.HeatingUnitFlowTemperature heatingUnitFlowTemperature1(qvMaxPump(displayUnit="l/min")=0.00025) annotation(Placement(transformation(extent={{180,-95},{215,-60}})));
	CoSES_ProHMo.Consumer.DHW_demand dHW_demand1(
		WeeklyData=WeeklyData,
		File=File,
		Table=Table,
		DHWfactor=DHWfactor,
		THotWaterSet=318.15) annotation(Placement(transformation(extent={{300,-135},{320,-115}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap5 annotation(Placement(transformation(extent={{130,-160},{140,-150}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap6 annotation(Placement(transformation(extent={{135,-175},{145,-165}})));
	CoSES_ProHMo.Generator.DigitalTwins wolfCGB20 annotation(Placement(transformation(extent={{-25,-110},{20,-65}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow defineVolumeFlow1 annotation(Placement(transformation(
		origin={125,15},
		extent={{-10,-10},{10,10}},
		rotation=-90)));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow extractVolumeFlow1 annotation(Placement(transformation(
		origin={95,15},
		extent={{10,-10},{-10,10}},
		rotation=-90)));
	GreenCity.Utilities.Thermal.MeasureThermal measureThermal1 annotation(Placement(transformation(extent={{220,-115},{230,-125}})));
	Modelica.Blocks.Sources.RealExpression TColdWater(y(quantity="Basics.Temp")=288.15) annotation(Placement(transformation(extent={{355,-160},{335,-140}})));
	CoSES_ProHMo.Consumer.SimpleHeatedBuilding simpleHeatedBuilding1(
		redeclare replaceable parameter GreenCity.Utilities.BuildingData.ParameterSelectionSCB.AgeOfBuilding.EnEV2007 buildingAge,
		nFloors=nFloors,
		nAp=nApartments,
		nPeople=nPeople,
		ALH=ALH,
		outline=true,
		livingTZoneInit(displayUnit="K")=TLiving_Init,
		UseStandardHeatNorm=UseStandardHeatNorm,
		QHeatNormLivingArea=QHeatNormLivingArea,
		n=n,
		TFlowHeatNorm(displayUnit="K")=TFlowHeatNorm,
		TReturnHeatNorm(displayUnit="K")=TReturnHeatNorm,
		TRef(displayUnit="K")=TRef,
		qvMaxLivingZone(displayUnit="m³/s")=qvMaxLivingZone,
		roofTZoneInit(displayUnit="K")=TRoof_Init,
		cellarTZoneInit(displayUnit="K")=TCellar_Init,
		UseIndividualPresence=UseIndividualPresence,
		PresenceFile=PresenceFile,
		Presence_WeeklyRepetition=WeeklyData,
		UseIndividualElecConsumption=UseIndividualElecConsumption,
		ElConsumptionFile=ElConsumptionFile,
		ElConsumptionTable=ElConsumptionTable,
		ElConsumptionFactor=ElFactor,
		ElConsumption_YearlyRepetition=true,
		ActivateNightTimeReduction=ActivateNightTimeReduction,
		Tnight(displayUnit="K")=Tnight,
		NightTimeReductionStart(displayUnit="s")=NightTimeReductionStart,
		NightTimeReductionEnd(displayUnit="s")=NightTimeReductionEnd,
		VariableTemperatureProfile=VariableTemperatureProfile,
		TMin(displayUnit="K")=TMin,
		ceilingInsul=0) "Heated 3-zone-building with changeable heating system" annotation(Placement(transformation(extent={{255,-100},{295,-60}})));
	parameter Real TStartCB(quantity="Basics.Temp")=328.15 "Minimum Temperature, at the top, at which the CB turns on" annotation(Dialog(
		group="Condensing Boiler",
		tab="Standard Control"));
	parameter Real TStopCB(quantity="Basics.Temp")=343.15 "Temperature at the bottom, at which the CB stops" annotation(Dialog(
		group="Condensing Boiler",
		tab="Standard Control"));
	parameter Real SOCminCB(quantity="Basics.RelMagnitude")=0.5 "SOC, at which the CB turns on (SOCmin < SOCmin and T<TStart)" annotation(Dialog(
		group="Condensing Boiler",
		tab="Standard Control"));
	parameter Real SOCmaxCB(quantity="Basics.RelMagnitude")=0.9500000000000001 "SOC, at which the CB turns off" annotation(Dialog(
		group="Condensing Boiler",
		tab="Standard Control"));
	protected
		Boolean CBon "Condensing boiler is switched on" annotation(
			HideResult=false,
			Dialog(
				group="Condensing Boiler",
				tab="Standard Control"));
	public
		Real CBinStandardControl "Standard control value of the CB modulation" annotation(Dialog(
			group="Condensing Boiler",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardControlEH "Standard Control for electric heater" annotation(
			Placement(
				transformation(extent={{-140,-165},{-100,-125}}),
				iconTransformation(
					origin={150,250},
					extent={{-20,-20},{20,20}},
					rotation=270)),
			Dialog(
				group="Electric Heater",
				tab="Standard Control",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardControlExcessPV "If true, only excess PV power is used for the electric heater" annotation(
			Placement(
				transformation(extent={{-120,30},{-80,70}}),
				iconTransformation(
					origin={0,250},
					extent={{-20,-20},{20,20}},
					rotation=-90)),
			Dialog(
				group="Electric Heater",
				tab="Standard Control",
				visible=false));
		parameter Real deltaTEHon(quantity="Thermics.TempDiff")=5 "Required temperature difference between storage temperature and max. storage temperature to switch on EH" annotation(Dialog(
			group="Electric Heater",
			tab="Standard Control"));
		parameter Real PGridEHon(quantity="Basics.Power")=500 "Required feed-in power to switch on EH" annotation(Dialog(
			group="Electric Heater",
			tab="Standard Control"));
		Boolean EHon "Electric heater is switched on" annotation(Dialog(
			group="Electric Heater",
			tab="Standard Control",
			visible=false));
		Real EHinStandardControl(quantity="Basics.Power") "Standard control value of the EH modulation" annotation(Dialog(
			group="Electric Heater",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.RealInput CBIn_TSet(quantity="Basics.Temp") if CBControlMode "Condensing boiler input - set temperature (power is constant)" annotation(
			Placement(
				transformation(extent={{-140,-40},{-100,0}}),
				iconTransformation(extent={{-270,-220},{-230,-180}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput ElHeaterIn_P(quantity="Basics.Power") if not CBControlMode "Electric heater power" annotation(
			Placement(
				transformation(extent={{-140,-195},{-100,-155}}),
				iconTransformation(extent={{-270,-170},{-230,-130}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput TDH_HEXout(quantity="Basics.Temp") "Outlet temperature of district heating heat exchanger" annotation(
			Placement(
				transformation(extent={{-140,70},{-100,110}}),
				iconTransformation(extent={{266.7,-170},{226.7,-130}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput DTH_HEXin(quantity="Basics.Temp") "Inlet temperature of district heating heat exchanger" annotation(
			Placement(
				transformation(extent={{-125,30},{-105,50}}),
				iconTransformation(extent={{236.7,-60},{256.7,-40}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput qv_HEX(quantity="Thermics.VolumeFlow") "Volume flow to heat exchanger" annotation(
			Placement(
				transformation(extent={{-125,55},{-105,75}}),
				iconTransformation(
					origin={246.7,0},
					extent={{10,-10},{-10,10}},
					rotation=-180)),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput qvDHpump(quantity="Thermics.VolumeFlow") "Reference volume flow of district heating pump - positive: feed in - negative: extraction" annotation(
			Placement(
				transformation(extent={{-140,-10},{-100,30}}),
				iconTransformation(extent={{266.7,-220},{226.7,-180}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput CBIn_P(quantity="Basics.Power") "Condensing boiler input - set pwer (DeltaT is constant)" annotation(
			Placement(
				transformation(extent={{-140,-75},{-100,-35}}),
				iconTransformation(extent={{-270,130},{-230,170}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		parameter Boolean CBControlMode=false "If true, flow temperature controlled, else power output is controlled" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
		parameter Real CBDeltaT(quantity="Thermics.TempDiff")=20 "Temperature difference between flow and return temperature" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
		parameter Real CBPmax(quantity="Basics.Power")=24000 if not CBControlMode "Maximum power of CB" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
		parameter Real CBTmax(quantity="Basics.Temp")=356.15 if CBControlMode "Maximum temperature of CB" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
		parameter Real CBPmin(quantity="Basics.Power")=2000 if not CBControlMode "Minimum power of CB" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
		parameter Real CBTmin(quantity="Basics.Temp")=293.15 if CBControlMode "Minimum temperature of CB" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
		parameter Real PElMax(quantity="Basics.Power")=6000 "Maximum power of the electric heater" annotation(Dialog(
			group="Electric Heater",
			tab="Heat Generator Parameters"));
		parameter Real Tmax_TS(quantity="Basics.Temp")=363.15 "Maximum temperature within the thermal storage" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real T0_TS(quantity="Basics.Temp")=303.15 "Temperature level to calculate the stored energy (e.g. return temperature of consumption)" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Boolean TSLinearProfile=true "If true, the temperature profile at simulation begin within the storage is linear, if false the profile is defined by a temperature vector" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTupInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=353.15 if TSLinearProfile "Temperature of upmost heat storage layer at simulation begin" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTlowInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=313.14999999999998 if TSLinearProfile "Temperature of lowmost heat storage layer at simulation begin" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTLayerVector[20](quantity="Basics.Temp")={313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15} if not TSLinearProfile "Vector of temperature profile of the layers at simulation begin, element 1 is at lowest layer" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Integer nPeople(quantity="Basics.Unitless")=6 "Number of people living in the building" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Integer nFloors=2 "nFloors" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Integer nApartments=2 "nApartments" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Real ALH(
			quantity="Geometry.Area",
			displayUnit="m²")=300 "Heated (living) area (e.g. 50m² per person)" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Boolean UseStandardHeatNorm=false "If true, use standard area-specific heating power, else define it manually" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real QHeatNormLivingArea(quantity="Thermics.HeatFlowSurf")=20 if not UseStandardHeatNorm "Area-specific heating power - modern radiators: 14 - 15 W/m²; space heating: 15 W/m²" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real n=1.3 "Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TFlowHeatNorm(quantity="Basics.Temp")=333.15 "Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 - 45°C" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TReturnHeatNorm(quantity="Basics.Temp")=318.15 "Normal return temperature - radiator: 45 - 65°C; floor heating: 28 - 35°C" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TRef(quantity="Basics.Temp")=294.15 "Reference indoor temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real qvMaxLivingZone(quantity="Thermics.VolumeFlow")=0.00025 "Maximumg flow rate in Living Zone" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TLiving_Init(
			quantity="Basics.Temp",
			displayUnit="°C")=294.15 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TRoof_Init(quantity="Basics.Temp")=274.65 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TCellar_Init(quantity="Basics.Temp")=280.15 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Boolean UseUnixTimeIndividual=false "If true, use unix time for individual presence and electric consumption" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean UseIndividualPresence=true "If the presence is used, individual presence data has to be provided, else standart presence is used" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String PresenceFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF4_WholeYear.txt" if UseIndividualPresence "File with presence timeseries (presence in %; 0% - no one is at home; 100% - everyone is at home)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean UseIndividualElecConsumption=true "If individual electricity consumption is used, individual consumption data ha to be provided, else standart load profiles are used" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String ElConsumptionFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF4_WholeYear.txt" if UseIndividualElecConsumption "File with electric consumption time series (consumption in kW)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String ElConsumptionTable="Pel" "Table with electric consumption time series (consumption in W)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Real YearlyElecConsumption_kWh=7000 if UseIndividualElecConsumption "YearlyElecConsumption_kWh" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Real ElFactor=YearlyElecConsumption_kWh/5149 if UseIndividualElecConsumption "ElFactor" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean ActivateNightTimeReduction=false "If true, night time reduction is activated, else temperature is constant" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real NightTimeReductionStart(
			quantity="Basics.Time",
			displayUnit="h")=82800 if ActivateNightTimeReduction "NightTimeReductionStart" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real NightTimeReductionEnd(
			quantity="Basics.Time",
			displayUnit="h")=25200 if ActivateNightTimeReduction "NightTimeReductionEnd" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real Tnight(quantity="Basics.Temp")=291.15 if ActivateNightTimeReduction "Temperature at night" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Boolean VariableTemperatureProfile=false "If true, presence will be used to define the temperature (if less people are at home, less rooms are heated and the average temperature will decrease)" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real TMin(quantity="Basics.Temp")=292.15 if VariableTemperatureProfile "Minimum temperature, when noone is at home (TRefSet = TMin + (TRef - TMin) * Presence(t))" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Boolean UseUnixTimeDHW=false "If true, use unix time for DHW consumption" annotation(
			HideResult=false,
			Dialog(
				group="DHW consumption",
				tab="Consumption Parameters",
				visible=false));
		parameter Boolean WeeklyData=true "If true: DHW consumption data repeats weekly" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter String File=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF4_WholeYear.txt" "DHW Data File" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter String Table="V_DHW" "DHW Table Name" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real V_DHWperDay_l=300 "V_DHWperDay_l" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real DHWfactor=V_DHWperDay_l/300 "Factor, with which the DHW consumption gets multiplied" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real alphaPV(
			quantity="Geometry.Angle",
			displayUnit="°")=0.6108652381980153 "Inclination angle of the PV system" annotation(Dialog(
			group="Parameters",
			tab="PV System"));
		parameter Real betaPV(
			quantity="Geometry.Angle",
			displayUnit="°")=3.141592653589793 "Orientation angle of the PV system" annotation(Dialog(
			group="Parameters",
			tab="PV System"));
		parameter Real PVPeak(quantity="Basics.Power")=10000 "Installed peak power of the PV system" annotation(Dialog(
			group="Parameters",
			tab="PV System"));
		Real PV_P(quantity="Basics.Power") "PV power" annotation(Dialog(
			group="Results",
			tab="PV System",
			visible=false));
		Real PV_E(
			quantity="Basics.Energy",
			displayUnit="kWh") "Produced energy of the PV system" annotation(Dialog(
			group="Results",
			tab="PV System",
			visible=false));
	protected
		Real CBIn "Input Signal of Condensing Boiler" annotation(
			HideResult=false,
			Dialog(tab="Results CB"));
	public
		Real CB_S_TM_VL(quantity="Basics.Temp") "Condensing boiler flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results CB",
			visible=false));
		Real CB_S_TM_RL(quantity="Basics.Temp") "Condensing boiler return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results CB",
			visible=false));
		Real CB_S_FW_HC(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow condensing boiler" annotation(Dialog(
			group="Volume Flow",
			tab="Results CB",
			visible=false));
		Real CB_S_FG(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Gas flow condensing boiler" annotation(Dialog(
			group="Volume Flow",
			tab="Results CB",
			visible=false));
		Real CB_VFuel(
			quantity="Basics.Volume",
			displayUnit="l") "Demanded fuel volume" annotation(Dialog(
			group="Volume Flow",
			tab="Results CB",
			visible=false));
		Real CB_P_heat_is(quantity="Basics.Power") "Heat output power of condensing boiler" annotation(Dialog(
			group="Power",
			tab="Results CB",
			visible=false));
		Real CB_P_gas_is(quantity="Basics.Power") "Gas power of condensing boiler" annotation(Dialog(
			group="Power",
			tab="Results CB",
			visible=false));
		Real CB_E_heat_produced(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of condensing boiler" annotation(Dialog(
			group="Energy",
			tab="Results CB",
			visible=false));
		Real CB_E_gas_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Gas input of condensing boiler" annotation(Dialog(
			group="Energy",
			tab="Results CB",
			visible=false));
		Real CB_Efficiency(quantity="Basics.RelMagnitude") "Heating efficiency" annotation(Dialog(
			group="Efficiency",
			tab="Results CB",
			visible=false));
		Real EH_P_heat_is(quantity="Basics.Power") "Heat output power of the electric heater" annotation(Dialog(
			group="Power",
			tab="Results EH",
			visible=false));
		Real EH_E_heat_produced(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of the electric heater" annotation(Dialog(
			group="Energy",
			tab="Results EH",
			visible=false));
		Real TS_S_TM_HC_VL(quantity="Basics.Temp") "Flow temperature consumption side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_RL(quantity="Basics.Temp") "Return temperature consumption side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_PS_VL(quantity="Basics.Temp") "Flow temperature producer side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_PS_RL(quantity="Basics.Temp") "Return temperature producer side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_HW_VL(quantity="Basics.Temp") "Flow temperature to fresh water station" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_HW_RL(quantity="Basics.Temp") "Return temperature to fresh water station" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_1(quantity="Basics.Temp") "Thermal storage temperature 1" annotation(Dialog(
			group="Storage Temperatures",
			__esi_groupCollapsed=true,
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_2(quantity="Basics.Temp") "Thermal storage temperature 2" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_3(quantity="Basics.Temp") "Thermal storage temperature 3" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_4(quantity="Basics.Temp") "Thermal storage temperature 4" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_5(quantity="Basics.Temp") "Thermal storage temperature 5" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_6(quantity="Basics.Temp") "Thermal storage temperature 6" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_7(quantity="Basics.Temp") "Thermal storage temperature 7" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_8(quantity="Basics.Temp") "Thermal storage temperature 8" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_9(quantity="Basics.Temp") "Thermal storage temperature 9" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_10(quantity="Basics.Temp") "Thermal storage temperature 10" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_E_Storage_BT(
			quantity="Basics.Energy",
			displayUnit="kWh") "Energy in thermal storage" annotation(Dialog(
			group="Energy",
			tab="Results TS",
			visible=false));
		Real TS_SOC_BT(quantity="Basics.RelMagnitude") "State of charge of the thermal storage" annotation(Dialog(
			group="State of charge",
			tab="Results TS",
			visible=false));
		Real HS_S_TM_VL_bM(quantity="Basics.Temp") "Heat Sink flow temperature before mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_VL_aM(quantity="Basics.Temp") "Heat Sink flow temperature after mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_RL(quantity="Basics.Temp") "Heat sink return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_HW_VL(quantity="Basics.Temp") "Heat sink flow temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_HW_RL(quantity="Basics.Temp") "Heat sink return temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_Room(quantity="Basics.Temp") "Temperature in the house" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_FW_HC_aM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow after mixing unit" annotation(Dialog(
			group="Volume Flow",
			tab="Results HS",
			visible=false));
		Real HS_S_FW_HC_bM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow before mixing unit" annotation(Dialog(
			group="Volume Flow",
			tab="Results HS",
			visible=false));
		Real HS_S_FW_HW_VL(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow domestic hot water consumption" annotation(Dialog(
			group="Volume Flow",
			tab="Results HS",
			visible=false));
		Real HS_P_DemHeatHC_is(quantity="Basics.Power") "Heating power" annotation(Dialog(
			group="Power",
			tab="Results HS",
			visible=false));
		Real HS_P_DemHeatHW_is(quantity="Basics.Power") "Domestic hot water power" annotation(Dialog(
			group="Power",
			tab="Results HS",
			visible=false));
		Real HS_E_DemHeatHC_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heating Energy" annotation(Dialog(
			group="Energy",
			tab="Results HS",
			visible=false));
		Real HS_E_DemHeatHW_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Domestic hot water energy" annotation(Dialog(
			group="Energy",
			tab="Results HS",
			visible=false));
		GreenCity.Local.Photovoltaic photovoltaic1(
			alphaModule(displayUnit="rad")=alphaPV,
			betaModule(displayUnit="rad")=betaPV,
			PPeak(displayUnit="Nm/s")=PVPeak) annotation(Placement(transformation(extent={{320,-250},{280,-210}})));
		GreenCity.Local.Controller.PV2ACInverter pV2ACInverter1 annotation(Placement(transformation(extent={{270,-210},{230,-250}})));
		Modelica.Blocks.Sources.RealExpression CBControlIn(y=CBIn) annotation(Placement(transformation(extent={{-90,-110},{-70,-90}})));
		Modelica.Blocks.Logical.Switch switch4 annotation(Placement(transformation(extent={{-55,-95},{-35,-75}})));
		Modelica.Blocks.Sources.RealExpression CBin_StandardControl(y=CBinStandardControl) annotation(Placement(transformation(extent={{-90,-80},{-70,-60}})));
		CoSES_ProHMo.Storage.HeatStorageCombined BSH1000(
			VStorage=0.86,
			dStorage=0.79,
			QlossRate=7,
			LinearProfile=TSLinearProfile,
			TupInit(displayUnit="K")=TSTupInit,
			TlowInit(displayUnit="K")=TSTlowInit,
			UseElHeater=true,
			ElHeaterPower(displayUnit="Nm/s")=PElMax,
			iUp=6,
			iLow=6,
			DeltaTHeater=5,
			TMax(displayUnit="K")=Tmax_TS,
			alphaMedStatic=1,
			use1=true,
			iFlow1=8,
			use2=true,
			iFlow2=8,
			use3=false,
			use4=true,
			iFlow4=8,
			use5=true,
			HE5=false,
			iFlow5=8,
			AHeatExchanger5=7.1,
			VHeatExchanger5=0.06,
			use6=true,
			HE6=true,
			AHeatExchanger6=7.1,
			VHeatExchanger6=0.06) annotation(Placement(transformation(extent={{85,-123},{120,-50}})));
		CoSES_ProHMo.Distribution.HydraulicSwitch_TS_DH hydraulicSwitch_TS_DH1 annotation(Placement(transformation(extent={{65,-30},{135,-15}})));
		Modelica.Blocks.Logical.Switch switch1 annotation(Placement(transformation(extent={{-55,-155},{-35,-135}})));
		Modelica.Blocks.Sources.RealExpression EHin_StandardControl(y=EHinStandardControl) annotation(Placement(transformation(extent={{-90,-140},{-70,-120}})));
		CoSES_ProHMo.Generator.DigitalTwins.WolfCGB20_GC wolfCGB20_GC1 annotation(Placement(transformation(extent={{-5,-110},{40,-65}})));
	equation
		// Input Conversion
		// CB
		if CBControlMode then
			CBIn = min(0, 2 + 8*((CBIn_TSet-CBTmin)/(CBTmax-CBTmin)));
		else
			CBIn = min(max(0, 2 + 8 * ((CBIn_P - CBPmin)/(CBPmax - CBPmin))), 10);
		end if;
		
		// Standard Control
		// Condensing boiler
		when ((TS_SOC_BT < SOCminCB) and (TS_S_TM_BT_8 < TStartCB) and StandardCBcontrol) then
			CBon = true;
		elsewhen ((TS_SOC_BT > SOCmaxCB) or (TS_S_TM_BT_3 > TStopCB) or (not StandardCBcontrol)) then
			CBon = false;
		end when;
		if CBon then
			CBinStandardControl = 10;
		else
			CBinStandardControl = 0;
		end if;
		
		// Electric heater
		when (((Tmax_TS - TS_S_TM_BT_5) > deltaTEHon) and StandardControlEH) then
			EHon = true;
		elsewhen ((Tmax_TS < TS_S_TM_BT_5) or (not StandardControlEH)) then
			EHon = false;
		end when;
		if EHon and StandardControlExcessPV then
			EHinStandardControl = min(max(0, -grid2.P), PElMax);
		elseif EHon then
			EHinStandardControl = PElMax;
		else
			EHinStandardControl = 0;
		end if;
		
		
		// Results
		// Condensing Boiler
		CB_S_TM_VL = wolfCGB20_GC1.TFlow;
		CB_S_TM_RL = wolfCGB20_GC1.TReturn;
		CB_S_FW_HC = wolfCGB20_GC1.qv;
		CB_S_FG = wolfCGB20_GC1.qvFuel;
		CB_VFuel = wolfCGB20_GC1.VFuel;
		CB_P_heat_is = wolfCGB20_GC1.QHeat;
		CB_P_gas_is = wolfCGB20_GC1.PFuel;
		CB_E_heat_produced = wolfCGB20_GC1.EHeat;
		CB_E_gas_consumed = wolfCGB20_GC1.EFuel;
		CB_Efficiency = wolfCGB20_GC1.Efficiency;
		
		// Electric heater
		EH_P_heat_is = BSH1000.PStorage[7];
		EH_E_heat_produced = BSH1000.EHeat[7];
		
		//Thermal Storage
		TS_S_TM_HC_HW_VL = BSH1000.FlowOut6.T;
		TS_S_TM_HC_HW_RL = BSH1000.ReturnIn6.T;
		TS_S_TM_BT_1 = BSH1000.TStorage[1];
		TS_S_TM_BT_2 = BSH1000.TStorage[2];
		TS_S_TM_BT_3 = BSH1000.TStorage[3];
		TS_S_TM_BT_4 = BSH1000.TStorage[4];
		TS_S_TM_BT_5 = BSH1000.TStorage[5];
		TS_S_TM_BT_6 = BSH1000.TStorage[6];
		TS_S_TM_BT_7 = BSH1000.TStorage[7];
		TS_S_TM_BT_8 = BSH1000.TStorage[8];
		TS_S_TM_BT_9 = BSH1000.TStorage[9];
		TS_S_TM_BT_10 = BSH1000.TStorage[10];
		TS_E_Storage_BT = BSH1000.cpMed*BSH1000.rhoMed*BSH1000.VStorage*((BSH1000.TLayer[1]+BSH1000.TLayer[2]+BSH1000.TLayer[3]+BSH1000.TLayer[4]+BSH1000.TLayer[5]+BSH1000.TLayer[6]+BSH1000.TLayer[7]+BSH1000.TLayer[8]+BSH1000.TLayer[9]+BSH1000.TLayer[10])/10-T0_TS);
		TS_SOC_BT = TS_E_Storage_BT/(BSH1000.cpMed*BSH1000.rhoMed*BSH1000.VStorage*(Tmax_TS - T0_TS));
		
		// Heat Sink (Heating)
		HS_S_TM_VL_bM = heatingUnitFlowTemperature1.FlowSupply.T;
		HS_S_TM_VL_aM = heatingUnitFlowTemperature1.FlowSink.T;
		HS_S_TM_RL = heatingUnitFlowTemperature1.ReturnSupply.T;
		HS_S_FW_HC_aM = heatingUnitFlowTemperature1.FlowSink.qv;
		HS_S_FW_HC_bM = heatingUnitFlowTemperature1.FlowSupply.qv;
		HS_P_DemHeatHC_is = simpleHeatedBuilding1.QHeat;
		HS_E_DemHeatHC_consumed = simpleHeatedBuilding1.EHeat;
		
		// Heat Sink (Domestic Hot Water)
		HS_S_TM_HW_VL = dHW_demand1.FlowHotWater.T;
		HS_S_TM_HW_RL = dHW_demand1.ReturnHotWater.T;
		HS_S_FW_HW_VL = dHW_demand1.ReturnHotWater.qv;
		HS_P_DemHeatHW_is = dHW_demand1.Q_DHW;
		HS_E_DemHeatHW_consumed = dHW_demand1.E_DHW;
		
		// PV
		PV_P = -grid2.PGrid[3];
		PV_E = -grid2.EGridfeed[3];
	initial equation
		// enter your equations here
		
		// Condensing Boiler
		if ((TS_SOC_BT < SOCminCB) and (TS_S_TM_BT_8 < TStartCB) and StandardCBcontrol) then
			CBon = true;
		else
			CBon = false;
		end if;
		
		// Electric heater
		if (((Tmax_TS - TS_S_TM_BT_5) > deltaTEHon) and StandardControlEH) then
			EHon = true;
		else
			EHon = false;
		end if;
	equation
		connect(environmentConditions1,dHW_demand1.EnvironmentConditions) annotation(Line(
			points={{380,35},{375,35},{324.7,35},{324.7,-120},{319.7,-120}},
			color={192,192,192},
			thickness=0.015625));
		connect(measureThermal1.TMedium,dHW_demand1.TPipe) annotation(
			Line(
				points={{225,-115},{225,-105},{315,-105},{315,-115}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(extractVolumeFlow1.qvMedium,defineVolumeFlow1.qvMedium) annotation(Line(
			points={{100,25},{100,30},{120,30},{120,25}},
			color={0,0,127},
			thickness=0.0625));
		connect(extractVolumeFlow1.TMedium,DTH_HEXin) annotation(Line(
			points={{90,25},{90,30},{90,40},{-110,40},{-115,40}},
			color={0,0,127},
			thickness=0.0625));
		connect(TDH_HEXout,defineVolumeFlow1.TMedium) annotation(Line(
			points={{-120,90},{-115,90},{130,90},{130,30},{130,25}},
			color={0,0,127},
			thickness=0.0625));
		connect(extractVolumeFlow1.qvMedium,qv_HEX) annotation(Line(
			points={{100,25},{100,30},{100,65},{-110,65},{-115,65}},
			color={0,0,127},
			thickness=0.0625));
		connect(TColdWater.y,dHW_demand1.TColdWater) annotation(Line(
			points={{334,-150},{329,-150},{315,-150},{315,-139.7},{315,-134.7}},
			color={0,0,127},
			thickness=0.0625));
		connect(heatingUnitFlowTemperature1.FlowSink,simpleHeatedBuilding1.PipeIn) annotation(Line(
			points={{215,-68.7},{220,-68.7},{250,-68.7},{250,-70},{255,-70}},
			color={190,30,45}));
		connect(heatingUnitFlowTemperature1.ReturnSink,simpleHeatedBuilding1.PipeOut) annotation(Line(
			points={{215,-86.3},{220,-86.3},{250,-86.3},{250,-85},{255,-85}},
			color={190,30,45},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.qv_Ref,heatingUnitFlowTemperature1.qvRef) annotation(Line(
			points={{260,-60},{260,-55},{206.3,-55},{206.3,-60}},
			color={0,0,127},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.T_Ref,heatingUnitFlowTemperature1.TRef) annotation(
			Line(
				points={{265,-60},{265,-50},{193,-50},{193,-60}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(simpleHeatedBuilding1.EnvironmentConditions,environmentConditions1) annotation(Line(
			points={{280,-60},{280,-55},{280,35},{375,35},{380,35}},
			color={192,192,192},
			thickness=0.0625));
		connect(photovoltaic1.DC,pV2ACInverter1.DCPV) annotation(Line(
			points={{280,-230},{275,-230},{270,-230}},
			color={247,148,29},
			thickness=0.0625));
		connect(pV2ACInverter1.MPP,photovoltaic1.MPP) annotation(Line(
			points={{250,-250},{250,-255},{300,-255},{300,-250}},
			color={0,0,127},
			thickness=0.0625));
		connect(environmentConditions1,photovoltaic1.EnvironmentConditions) annotation(Line(
			points={{380,35},{375,35},{325,35},{325,-230},{320,-230}},
			color={192,192,192},
			thickness=0.0625));
		connect(CBin_StandardControl.y,switch4.u1) annotation(Line(
			points={{-69,-70},{-64,-70},{-62,-70},{-62,-77},{-57,-77}},
			color={0,0,127},
			thickness=0.0625));
		connect(CBControlIn.y,switch4.u3) annotation(Line(
			points={{-69,-100},{-64,-100},{-62,-100},{-62,-93},{-57,-93}},
			color={0,0,127},
			thickness=0.0625));
		connect(measureThermal1.PipeOut,dHW_demand1.FlowHotWater) annotation(Line(
			points={{230,-120},{235,-120},{295,-120},{300,-120}},
			color={190,30,45}));
		connect(extractVolumeFlow1.Pipe,hydraulicSwitch_TS_DH1.HeatExchangerIn) annotation(Line(
			points={{95,5},{95,0},{95,-10},{95,-15}},
			color={190,30,45},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH1.HeatExchangerOut,defineVolumeFlow1.Pipe) annotation(Line(
			points={{105,-15},{105,-10},{105,0},{125,0},{125,5}},
			color={190,30,45},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH1.FeedInPumpOut,phaseTap5.Grid1) annotation(Line(
			points={{125,-15},{125,-10},{145,-10},{145,-155},{140,-155}},
			color={247,148,29},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH1.ExtractionPumpOut,phaseTap6.Grid1) annotation(
			Line(
				points={{120,-15},{120,-5},{155,-5},{155,-170},{145,-170}},
				color={247,148,29},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(phaseTap1.Grid1,heatingUnitFlowTemperature1.Grid1) annotation(Line(
			points={{170,-185},{175,-185},{184.3,-185},{184.3,-100},{184.3,-95}},
			color={247,148,29},
			thickness=0.015625));
		connect(grid1.LVMastGrid,lV3Phase1) annotation(
			Line(
				points={{155,-275},{155,-280},{155,-305}},
				color={247,148,29},
				thickness=0.015625),
			__esi_AutoRoute=false);
		connect(grid1.LVGridA,grid2.LVMastGrid) annotation(Line(
			points={{135,-240},{130,-240},{85,-240},{85,-210},{85,-205}},
			color={247,148,29},
			thickness=0.015625));
		connect(grid2.LVGridC,pV2ACInverter1.LVGrid3) annotation(Line(
			points={{65,-200},{60,-200},{60,-230},{225,-230},{230,-230}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid2.LVGridB,phaseTap5.Grid3) annotation(Line(
			points={{65,-185},{60,-185},{60,-155},{125,-155},{130,-155}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid2.LVGridD,phaseTap6.Grid3) annotation(Line(
			points={{105,-170},{110,-170},{130,-170},{135,-170}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid2.LVGridE,phaseTap1.Grid3) annotation(Line(
			points={{105,-185},{110,-185},{155,-185},{160,-185}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid2.LVGridF,simpleHeatedBuilding1.Grid3) annotation(Line(
			points={{105,-200},{110,-200},{285,-200},{285,-105},{285,-100}},
			color={247,148,29},
			thickness=0.0625));
		connect(ElHeaterIn_P,switch1.u3) annotation(Line(
			points={{-120,-175},{-115,-175},{-62,-175},{-62,-153},{-57,-153}},
			color={0,0,127},
			thickness=0.0625));
		connect(EHin_StandardControl.y,switch1.u1) annotation(Line(
			points={{-69,-130},{-64,-130},{-62,-130},{-62,-137},{-57,-137}},
			color={0,0,127},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH1.qvDistrictHeating,qvDHpump) annotation(Line(
			points={{85,-15},{85,-10},{85,10},{-115,10},{-120,10}},
			color={0,0,127},
			thickness=0.0625));
		connect(BSH1000.Grid3,grid1.LVGridB) annotation(Line(
			points={{115,-122.7},{115,-127.7},{115,-255},{130,-255},{135,-255}},
			color={247,148,29},
			thickness=0.0625));
		connect(switch1.y,BSH1000.Pel) annotation(Line(
			points={{-34,-145},{-29,-145},{110,-145},{110,-127.7},{110,-122.7}},
			color={0,0,127},
			thickness=0.0625));
		connect(BSH1000.FlowOut6,measureThermal1.PipeIn) annotation(Line(
			points={{120,-104.7},{125,-104.7},{215,-104.7},{215,-120},{220,-120}},
			color={190,30,45}));
		connect(dHW_demand1.ReturnHotWater,BSH1000.ReturnIn6) annotation(Line(
			points={{300,-130},{295,-130},{125,-130},{125,-114.7},{120,-114.7}},
			color={190,30,45}));
		connect(BSH1000.ReturnIn5,heatingUnitFlowTemperature1.ReturnSupply) annotation(Line(
			points={{120,-89.7},{125,-89.7},{175,-89.7},{175,-86.3},{180,-86.3}},
			color={190,30,45},
			thickness=0.0625));
		connect(heatingUnitFlowTemperature1.FlowSupply,BSH1000.FlowOut5) annotation(Line(
			points={{180,-68.7},{175,-68.7},{125,-68.7},{125,-80},{120,-80}},
			color={190,30,45},
			thickness=0.0625));
		connect(BSH1000.ReturnOut1,hydraulicSwitch_TS_DH1.StorageChargeReturn) annotation(Line(
			points={{85,-65},{80,-65},{70,-65},{70,-34.7},{70,-29.7}},
			color={190,30,45}));
		connect(BSH1000.FlowIn1,hydraulicSwitch_TS_DH1.StorageChargeFlow) annotation(Line(
			points={{85,-55},{80,-55},{75,-55},{75,-34.7},{75,-29.7}},
			color={190,30,45},
			thickness=0.0625));
		connect(BSH1000.ReturnIn4,hydraulicSwitch_TS_DH1.StorageDischargeReturn) annotation(Line(
			points={{120,-65},{125,-65},{130,-65},{130,-34.7},{130,-29.7}},
			color={190,30,45},
			thickness=0.0625));
		connect(BSH1000.FlowOut4,hydraulicSwitch_TS_DH1.StorageDischargeFlow) annotation(Line(
			points={{120,-55},{125,-55},{125,-34.7},{125,-29.7}},
			color={190,30,45}));
		connect(StandardCBcontrol,switch4.u2) annotation(Line(
			points={{-120,-85},{-115,-85},{-62,-85},{-57,-85}},
			color={255,0,255},
			thickness=0.0625));
		connect(StandardControlEH,switch1.u2) annotation(Line(
			points={{-120,-145},{-115,-145},{-62,-145},{-57,-145}},
			color={255,0,255},
			thickness=0.0625));
		connect(wolfCGB20_GC1.ControlIn,switch4.y) annotation(Line(
			points={{-5,-85},{-10,-85},{-29,-85},{-34,-85}},
			color={0,0,127},
			thickness=0.0625));
		connect(grid2.LVGridA,wolfCGB20_GC1.lV3Phase) annotation(Line(
			points={{65,-170},{60,-170},{30,-170},{30,-114.7},{30,-109.7}},
			color={247,148,29},
			thickness=0.0625));
		connect(wolfCGB20_GC1.Return,BSH1000.ReturnOut2) annotation(Line(
			points={{39.7,-90},{44.7,-90},{80,-90},{80,-89.7},{85,-89.7}},
			color={190,30,45},
			thickness=0.0625));
		connect(wolfCGB20_GC1.Flow,BSH1000.FlowIn2) annotation(Line(
			points={{39.7,-80},{44.7,-80},{80,-80},{85,-80}},
			color={190,30,45}));
		connect(environmentConditions1,wolfCGB20_GC1.environmentConditions) annotation(Line(
			points={{380,35},{375,35},{30,35},{30,-60},{30,-65}},
			color={192,192,192},
			thickness=0.0625));
		connect(UnixTime,simpleHeatedBuilding1.UnixTime) annotation(Line(
			points={{390,-10},{385,-10},{285,-10},{285,-55},{285,-60}},
			color={0,0,127},
			thickness=0.0625));
		connect(dHW_demand1.UnixTime,UnixTime) annotation(
			Line(
				points={{319.6666564941406,-130},{324.7,-130},{345,-130},{345,-10},{390,-10}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
	annotation(
		__esi_WolfCRK1(
			ReturnST(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowST(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ToFlow(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
		__esi_WolfSPU2_800(
			FromFlow1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FlowIn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
		__esi_wolfCGB14(
			Return(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			Flow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			measureThermal1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			condensingBoiler_DZ1(
				ReturnCB(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FlowCB(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				ToFlow(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromReturn(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))))),
		__esi_distributionValve1(
			PipeIn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeOutRegulated(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeOutRemain(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FromPipe(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToPipe2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToPipe1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
		__esi_mergingValve1(
			PipeOut(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeIn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeIn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FromPipe1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromPipe2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToPipe(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
		__esi_neoTower2(
			Return(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			Flow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			cHP_DZ1(
				ReturnCHP(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FlowCHP(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromReturn(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToFlow(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))))),
		__esi_hydraulicSwitch2HG1(
			HeatExchangerIn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorSupply2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorReturn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatExchangerOut(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorSupply1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorReturn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageChargeReturn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageChargeFlow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageDischargeFlow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageDischargeReturn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			DemandFlow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			DemandReturn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageChargeOut(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FeedInPump(
				PumpIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PumpOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				ToPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			FeedInValve(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			DemandValve(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal3(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			DHFeedIn_Extract(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve3(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve4(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureMixed(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			HeatGeneratorValve1(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve5(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureHGSupply1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureDHSupply(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			DistrictHeatingValve1(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			StorageChargeIn(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeIn(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeOut(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageChargeIn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageChargeOut1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeOut1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeIn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			measureDemandSupply(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureDemandReturn(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			ExtractionPump1(
				PumpIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PumpOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				ToPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureHGSupply2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve6(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			HeatGeneratorValve2(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			viewinfo[0](
				fMin=1,
				fMax=100,
				nf=100,
				kindSweep=1,
				expDataFormat=0,
				expNumberFormat="%.7lg",
				expMatrixNames={
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																								"A","B","C","D","E"},
				typename="AnaLinSysInfo")),
		__esi_WolfSEM1_500(
			FromFlow1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FlowIn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
		__esi_hydraulicSwitch_TS_DH1(viewinfo[0](
			fMin=1,
			fMax=100,
			nf=100,
			kindSweep=1,
			expDataFormat=0,
			expNumberFormat="%.7lg",
			expMatrixNames={
							"A","B","C","D","E"},
			typename="AnaLinSysInfo")),
		Icon(
			coordinateSystem(extent={{-250,-250},{250,250}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAALMAAACYCAIAAAAgHU9RAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAScQAAEnEB89x6jgAAFNtJREFUeF7tnQlUVNUfx9mGXVYBERJTwQwTJE1FEA3ZRBERQiVRWlRc
SOtPiopphIm4oKYoEkassQmyiCzDUhaRYajn1FE74vGoiaZZirhE/y/cCyoM4yjD8Gbmfs6cOcN7
d4bHu5/3u9/75s2g8B+DIQhmxn+NjY1//fUX/YHRjryb8c0333h4ePj7+5eVldFFjDbk14ympqb9
+/e/8sorCgoKPB5vzJgxWVlZLS0tdLXcI6dmXLlyJTQ0dPDgwdDCy8vLxcVFSUnJ2tp6x44dt2/f
po3kG3k0o76+3sfHx8DAQEtLKygoqKSkJC8vz9vbW1lZ2dzcfMmSJQ0NDbSpHCN3ZsADe3t7NTU1
MzOz9evXV1VV1dTU/PDDD1geHBxsaGioq6vr6elZV1dHnyCvyJcZBw4cGDVqFGrD8OHD9+3bd/z4
cTjxbRt4AEsiIyMHDhyoqqoKe3JycujT5BJ5MQPz0nXr1iFYIE9MmTIlNTUVKsAMogXh+++/x31s
bKytra2KigpiR0xMzKNHj+hLyBlyYcbZs2cXLVpkbGyM/g4MDCwoKMAI0kkLwnfffQc/MJXFgILS
ApPCw8OvXr1KX0iekH0z0N/Ozs46Ojr6+vphYWF8Ph99DwOICl3BKpQT2AOH1NXVjYyM5s2bd/r0
afpycoMsm9HS0pKenj569GjkBgSLrVu3ok6QIUM4pHJUVFSsWbMGsUNTU9PJyQkL6evKBzJrRnNz
c1RU1LBhwzAoODg4JCYmdg0WwiGlZfv27XgRHo9nZ2eXnJwsP6fCZNOMP/74Y+nSpSYmJggW06dP
z8/PhxZCRpDuIDUmKSkJUxUYNmTIkIiIiKamJvprZBoZNOPEiROwoV+/ftra2vCjvLz8xbQg4Il4
Otzy8/PDqATbVqxYIQ+nwmTNjKysLJR9FH8zM7PNmzeja0UJFsIhciC6rly5ErbBOS8vrx9qauiv
lFFkx4yHDx/GxcUhaaLs29jYJCQkPG+wEA4xLDIy0sLCgrwDl5ubS3+3LCIjZly9ehUzUtQJJSUl
V1fXjIyMmpqaFx5BugOe4WXj4+PfeOMN/KIRI0bg8f0H9+lGyBayYMbvv/8eEBCAOq+mprZgwYKS
kpIff/yRdqa4gW21tbWZmZnOzs6KioqIHatXr75+/TrdFBlC6s3AcTxp0iQcwaampuvWrSOHNe3G
3oHIUVpaGhgYCB3V1dXnzp177tw5ukGyghSb0dLSkpqaipKuoKAwbNiwnTt3/vTTT8gWtAN7GZQl
JI+PP/5YX18fyWby5MkVFRV0y2QCqTQDTty5cwdTD9QJlPQJEyZAEUxWxZg3RQFmoD7t2LHD0tIS
m2FtbZ2Wlvbvv//SrZRypM8M7Prz588vX77cwMBARUVl1qxZRUVFqBZiz5uiABfxq5OSkjCikVNh
MTExN/78UwZOlUqZGZia4kj18vLC6K6npxccHFxeXt57eVMUYCTkyMnJmTFjBiIwBpeQkBC4K+3F
Q2rMwFHY3Nycm5s7duxYHJ0o4BEREaSe0y7qUyAHHF2yZAk5Je/r61tXVweP6dZLIdJhBo6/mzdv
xsXFIW/yeDwEiwMHDpAMSHuGA5Dt2bRpEznbNnHixMLCwnvNzfRvkDakwIxHjx41NDSsXbvW2NhY
Q0PDxcUFeRPHqITzpijADGzYnj177OzsVFVVraysDn116Pbff0vjyMJ1M+7fv3/y5MkFCxbACSMj
Izw4evQopiF9kjdFARuGzcMkxdXVVVNT09zcHKPehQsXpG5k4bQZd+/eLS4uRpFAshs6dOjq1aur
qqpqa2tpJ3AVkkkxYwoICMAECkn5vffe+/nnn2E5/cOkAY6agfJ769atlJQUW1tbaIH76OhoDOQS
O5HVc7C1fD5/1apVmMoiG7m7uyOiStG1HVw0A4UXweLzzz+3sLDQ0tJydnY+dOgQSgUHg4VwiMpR
UVHkisPx48dnZmXd+usvqTjbwTkzUHLr6+tDQkIMDQ1Riv38/DBTRXHmbLAQDsmksbGxjo6OsHzw
4ME7du68dOkS9z+swC0zECwqKytnz56NEQQ7MTg4GDkDe5buZukETiNkZGRkkI9M6urq/i809MyZ
MxyPHVwxAwX25s2b2H0ODg7QwsbGZuPGjditKMhk/0o78LukpOTdd981MzND8fD390ea5nLs4IQZ
KK2XL1/eu3cvJiCY6aHw7tu3j5w4ovtVJqipqamurg4LC7O2tob9kyZNys/Pv/3339yMHX1vBorq
qVOnsL+MjY319fWnT5+OKQnKr5QGC+EgkMKP3bt3T5gwQUNDA8k0Pj7+ypUrHDwV1sdm3Lt3D/tr
zpw52tra5ubmQUFBhYWF0h4shEMyKWZbrq6uenp6AwcO3LBhw9mzZ7l2KqzPzEAJvX37dkFBgZub
G0rryJEj16xZg6FXZoKFEEgmzcvLCwwMROzAAIqsjT+cU5m0b8xA8UQJjYuLs7OzQ1G1t7ePiYlB
mZWiE1k9B5WDz+eHhoZaWVmpqqp6enoWFRVxJ5P2gRnIm+fOnQsPDx80aBDKqbu7e0JCguzlTVHA
wQAiIyPHjBmDwtn6AcmUlBs3bnAhk0raDBRMSIDiiZmbiYnJ/Pnzc3NzufwOWW9DYgfKp4uLC4YV
HC1bt269cOFCn2dSiZqBvImAOWPGDBRPTFBDQkJKS0tlO2+KAo4K7IScnBwfH5/+/fsbGRktX768
rq6ub8+TSsgMciIrMTERZRNa4P6zzz7DHkEtpbtH7qmtrT169OjSpUvJO3CzZ8+urKzswwmLJMxA
YUR5jI6OtrCwUFdXx2wNxRM7Qg6DhXCwT6qrqyMiIjBTU1FRmTJlSmZm5t27d+l+lCy9bga0QGFE
sDA2NjYwMEDBzM7ORvGU22AhHMzOkMN27drl4OBArgrD42vXrtG9KUF61wxogZI4a9Ys/JEvv/zy
smXLiouLuX/pTd9y/PhxRPKUlBRvb29kUiSPtWvX/vrrr3SfSopeNKO5uTk9PR0lUVlZefTo0QgW
KJVMCxFBWUVaX7hwIWptv3795syZgypL96xE6C0zrl+/vn379uHDh2O8dHR0RLBAnZSrE1k9B/Gc
fFfYiBEjFBUVnZ2di4qKHjx4QHdxL9MrZqD0rVy5EmVQS0sLwaJPPlooG0AO5HREDXt7ewUFhbFj
x8bGxkrmf26I3wwMGX5+fmpqaoaGhggWMv8OWW8DM5BJcXRhToeZ3aBBg8LCwiTwdVDiNAOT76ys
LAQLJSUljCPITbCEnbHoOeRUWG5uLmIH5neIHYGBgUhsdL/3DmIzo7Gxcc+ePTY2NhgRJ06ciMeQ
nQULMQI5SktLcbyZm5vzeLxp06YVFBTQvd8LiMeMixcvrlq1CoUOg8jMmTNR+sgASf8mhpjAkYb6
sWXLFsz1EO3HjRuXkJDwzz//0G4QK2IwAy4HBATo6emh0KHcIT9jXGQnsnoJBHn4ER8f7+bmptz2
Txc2b958+fJl2hnio6dmYPAjl94MHjwYhQ7lDlrQP4LRO0AOlOTs7GxfX19dXd2BAwcuWrTozJkz
tEvExIub0dTUdPDgwTFjxpCvQMTMqqqqigULyYCSDDlQnkNCQhA7kEl9fHyw/2nfiIMXNAPB4pNP
PrGyskJB8/DwSEpKQqpgwULCQA4+nx8ZGWlpaYkJLWaFaWlp4np79kXMOHHiRFBQ0IABA6Dq22+/
nZOTg01kwaJPwNFYXV29d+9ee3t7FO9Ro0Zt27bt1q1btKt6wHObUVFRgfkSnMBMJDQ0tLi4mARm
uqUMiYPYAVJSUjCgaGhoIPCtXr26599C+Rxm3Lt3Lzk52dHREYXL2toac6fKyko2gnABHJnoiCNH
jixdutTQ0LB///7+/v4o5LTnXghRzWhsbIyIiHj11VdVVVWdnJyQPbFBTAvuQOQoKytbt24dYoeW
lpa7u/uxY8de+HpSkcxAacK8yNTU1MDAYNmyZYjE2AhUMLpRDM5AOiU1NRVaoLSPGzcuLi7uxa4K
e7YZmAthAMO8GZFz586dDQ0NmDqzYMFNkEYR+y5duoRD96233kLlGDp0aHh4+JUrV2h3iowwM1CI
MjMzHRwckGtsbGwQMm7cuPHo0aPTp09jC+i2MLgE+gVlAx7cv38fBzCiKLkYfeHChadOnaL9Khrd
mnHz5k1UiNdeew3BwtPTE/PmO3futLS0QBdmBmchZpCT5TiGocgXX3yBWaS2tjbpRNK5oiDYjIsX
L2JGilfEWLV48eJffvml4yOXzAwu86QZAEfyrVu3MjIyEDjQlfb29mlpac2ifUWpADNOnjyJOQ+C
BSLn+vXrET+fPK3GzOAyncwgNDU1YfnMmTN5PB5mlxgK/vzzT7quezqbUVpaOnXqVPhF/uHPtWvX
Ok17mBlcRqAZ4MGDB3V1de+8846Ojo6JickHH3xw4cIFuq4bHpuB8SIxMdHW1hbBAnOeiooKuNb1
o7fMDC7TnRkAhR82fPrpp+TroDBzgSt0nSCoGY2NjXgOgoWmpmZQUFB9fX13b8wwM7iMEDMA+u72
7dtJSUnk66CcnJwKCwu762hqRmrbPxcyNzfftGlTQ0ODkM/aMjO4jHAzCBgcqqqq3NzcVFRU5s2b
99tvv9EVT0PNwGQ3JiamqKjo72d9oRgzg8uIYgbAkX/mzJno6Ojk5OTuPqNAzUB/o6qI8rF8ZgaX
EdEMgOMfsRSd3l0heJxARYSZwWVEN+OZMDNkCmYGQzDMDIZgmBkMwTAzGIJhZgiG+crMYAiGmSE2
7N4IcXI/IfxGm0oDzAyx8cy+Z2aICjODyzAzxMYQq0AiB7nFf5mHhU8uwY20lAqYGb0CJNi8JZ48
iIuLIwulC2ZGr/CkGfv37ycLpQtmhniora1dturHjhuEmGDvRR44TfZ4chW5rVixgj6TqzAzxEDs
gfxWA57/Rp/PSZgZPaWkpAR9/M6S5/4MJp7F5/PpD9yDmdFT0MGTPV7k6Gc1o1tkwAz07gt3MHki
eQVy49R37DMzhHEkv/jJniMLx0xM67qwK89sQ5bjHiVnTiAfD95fzqHvMmRmCONw7nek/75KPkse
FBYW4kF5eXnbemGgGbnRn7tAVuF+6gz6gJlBkSIztu/6gTxY/mHrRLRtpTDQhtz85nZ7MoO8Du6Z
GZ2RBzPoz4LoaMPM6IzUmdFxI2u7o6PZc/1paM/MoEiRGdjIjv4mS4QgYrNOoD0zgyJFZnTwzNGE
OPHkbcq0SrpOKGjJzKDIas0gzAs6Jbxxx6tVVFSQH5kZFFnNGYRntux4NZZAOyN1ZuCBiHMTgGZb
onbQHwSBBuXl5bhnZnRGhs3weetz4c0yMzPRALse98yMzkijGeQc6DO3mTTujq+++goNxjlswmM8
6LgxMyjcN0Pg+yZPLulY2InKym7nI0lJSXjW6LEfkx9PnTp16Ovj5EaWcARmRo8QKEdMTAx91IW8
vDwhPnEKZkZPQTeLeH0GucZHKrQAzIyeIuI1XXx+6xmRqdOl5n9UMzPEgIjXgbp6SdP/qGZmiIea
mppOV4d3unH5kk+BMDMYgmFmCIb5ysxgCIaZwRAMM4MhGGZGZ4qK0nZtCg6aN9HefjRl2jTPRWui
E4tKW6+jeJrcLXPHv2FH2/l8dpgufkxVWUHCmhm0wbg3PYN35bct5+cnb1vhTpcLYvLk6cEfbfs6
+1hbc8nDzHiCovStq3wnjbUcZKqv209ZWVmBoq6urmdsPnSEtXvg5oSC8iffEskIHauppkTbjfwg
nS5+TOWxw7sWWNIG6iZDPDfltC0vyz6w1tuCLhcEj6ehb2Q+dJz7u1v3k6dIFmZGO0UHw+Y4Wxlq
qXYI0RV1HdPBb354IK+kQ47eM4PCU9e19gzemizx0sHMaKPo4No5Ti9rq1MrBrzqOGdVVEzsl61s
37bWe4KpjhpZpaBmYO4fmXOUnrcSjxnaFjYzVu1s+21t7In6ZJ6jrSFdq6Jl5/PRXknXDWZGK+lb
3p5s2aHFxPnhe9IKjlVUkg2rqCg9kv3FJ37mJtqkgYL22BX7C0rb6oZ4zNCxsl+4/Ujb8jaqKsoK
kqOXerTLoTbKe+XurhmmV2FmoGDEfTTzdRMe6YQBjkGfHSqo6LxNlWVfr3Q216eFQ9U9PKeo7QOM
vWNGK1XpW993a3/iwEnzNx6S7IDCzPi2KPZDz9HGtHetvdfszeziRSuVx45kZXyTTsg7VlnV1qj3
zPg2Y9viaa/QBiYTAzZ8eZSukAzMDIgxw7ZdDNOpwdtTnufYfMoMno7RgM6YmBgZaNN69FxmFCds
CHAwpQ2GeyyJ/kay+4mZkfqpv+OQ9r619V8Xd/h5tucpM56JqGYUZ38d5u86REOVvrKly6KoVJE+
zyQ+mBnJG30dBpMOUFCwmxt+MJeuEAnxmKGozFPX1tXvQE9XR1ON1/6y/cf6rt6fX9X2NMnBzBCj
GSMWf1nUmfzspC1zh9IG3ZkhhP6WUz7cnMSn8yQJwszI373c/TV92g8vuS2PSS2ha0ThKTPEeqbL
0HCom+//Yg4dLm+fPksWZsa3SRt9J7QXDcXX527qrmjkbJo+bGA/VULrtLX1ZJd4zNCxHD8/Kqv8
Kfh8fgUmQH22c5gZrWp4P1bjJY8PYtIFVY2cjZ4W/dVpM+c1Wb19PqOvYWaAgl3LXEca0F5SeMl9
1a60kqc2qro6e+O0IcbtXihODcsgYjAzREF6zYAbu5e6jDJUUqQ99dLr05ZFpeYcRUUHGV8sdhmk
r0HXKSmPWLgnt5jOIZkZIiDNZoD83cFTbfqrKCm269EVJWUVVXXXtWlHyuhzmBkiIeVmgPyvwxdM
HGaqqa7GU1FWfKyIkpKSiqqahtZrbgv35RU9dfkOM0MEpN+MNvLzEzd/6Of5prmZmR7FxsZm1pIN
B/JLun5kJGvdFFNjfdpu/McZdPFjKkty9y62ow1Mhtn4bSZTn/LDX26cO5Iuf+l1lyX0Wi/OwMxg
CIaZwRAMM4MhGGYGQzCcMIPJwUGYGQzB9LEZ58+fP9nGLwyOgU6pr6+/fv067a0e8NxmgIcPHz5g
cBgcvbSresCLmMGQB5gZDMEwMxiCYWYwBMPMYAiGmcEQDDODIRhmBkMwzAyGYJgZDEH899//AbBb
4jbjvZz2AAAAAElFTkSuQmCC",
								extent={{-220.3,-245},{226.5,106.7}}),
							Text(
								textString="SF4",
								fontName="TUM Neue Helvetica 55 Regular",
								textStyle={
									TextStyle.Bold},
								extent={{-252.5,269.8},{253.7,97.3}}),
							Text(
								textString="DZ",
								fontSize=14,
								textStyle={
									TextStyle.Bold},
								lineColor={0,128,255},
								extent={{164.7,-196.8},{274.7,-260.2}}),
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAJYAAACWCAYAAAA8AXHiAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAScQAAEnEB89x6jgAAAG5JREFUeF7twTEBAAAAwqD1T20LLyAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAABuamA1AAEQKBC2AAAAAElFTkSuQmCC",
								extent={{-250,-250},{250,250}})}),
		Documentation(info="<HTML><HEAD>
<META http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"> 
<TITLE>House 1 with ST, CHP and CB</TITLE> 
<STYLE type=\"text/css\">
table>tbody>tr:hover,th{background-color:#edf1f3}html{font-size:90%}body,table{font-size:1rem}body{font-family:'Open Sans',Arial,sans-serif;color:#465e70}h1,h2{margin:1em 0 .6em;font-weight:600}p{margin-top:.3em;margin-bottom:.2em}table{border-collapse:collapse;margin:1em 0;width:100%;}tr{border:1px solid #beccd3}td,th{padding:.3em 1em; border-width: 1px; border-style: solid; border-color: rgb(190, 204, 211);}td{word-break:break-word}tr:nth-child(even){background-color:#fafafa}th{font-family:'Open Sans Semibold',Arial,sans-serif;font-weight:700;text-align:left;word-break:keep-all}.lib-wrap{width:70%}@media screen and (max-width:800px){.lib-wrap{width:100%}}td p{margin:.2em}
</STYLE>
    
<META name=\"GENERATOR\" content=\"MSHTML 11.00.10570.1001\"></HEAD> 
<BODY>
<H1>House 1 with ST, CHP and CB</H1>
<HR>

<P>This model is a digital twin of the House 1 in the CoSES laboratory (<A href=\"https://doi.org/10.1109/PESGM41954.2020.9281442\">https://doi.org/10.1109/PESGM41954.2020.9281442</A>)</P>
<P><BR></P>
<P>The house consists of the following components and default conditions:</P>
<UL>
  <LI>Combined Heat and Power unit (CHP): neoTower 2.0, nominal electric and 
  thermal power: 2 kW_el / 5       kW_th</LI>
  <LI>Condensing Boiler: Wolf CGB14, nominal thermal power: 14 kW_th</LI>
  <LI>Solar Thermal emulator of variable size, up to 9 kW_th</LI>
  <LI>Thermal Storage: Wolf SPU-2 800,&nbsp;content: 750 l&nbsp;</LI>
  <LI>Domestic Hot Water (DHW)&nbsp;preparation: Wolf SEM-1 500,&nbsp;DHW 
  storage, connected to the thermal storage and solar thermal unit, content: 500 
        l</LI>
  <LI>Consumption - House parameters:<BR>Construction year: 1995 to       
  2002<BR>Additional insulation: roof + floor<BR>Number of floors: 2<BR>Number   
      of apartements: 1<BR>Number of inhabitants: 6<BR>Living area:   
  300m²<BR>Heating     system: radiators (supply / return temperatur: 60 / 45   
  °C)</LI></UL>
<P>Four boolean inputs can activate a predefined standard control for simple  
 simulation results or during a lead time of the simulation. The standard  
control  of each element is as follows:</P>
<UL>
  <LI>CHP:<BR>Switched on at 100 % modulation, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; 
      T_Storage,top &lt; T_Start,CHP and SOC_Storage &lt; SOCminCHP<BR>Switched  
   off,   when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; T_Storage,bottom &gt; T_Stop,CHP or  
     SOC_Storage &gt; SOCmaxCHP<BR><BR></LI>
  <LI>Condensing Boiler:<BR>The condensing boiler should only operate, if the    
   CHP cannot provide the required heat.<BR>Switched on at 100 % modulation,     
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; T_Storage,top &lt; T_Start,CB and     
  SOC_Storage &lt; SOCminCB<BR>Switched off, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;   
    T_Storage,bottom &gt; T_Stop,CB or SOC_Storage &gt; SOCmaxCB<BR><BR></LI>
  <LI>Solar Thermal pump:<BR>The control is&nbsp;based on Green City's   
  'SolarController'<BR>Solar thermal pump switched on,   
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_DHWStorage,in) &gt;   
  deltaTonST<BR>Solar thermal pump switched off,   
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_DHWStorage,in)&nbsp;&lt;   
  deltaToffST<BR>The pump is controlled with the following   
  conditions:<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &lt;   
  deltaTFlowReturnLow, the reference volume flow is at  minimum   
  level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &gt;   
  deltaTFlowReturnMax, the reference volume flow is at  maximum   
  level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; Between the two temperature  boundaries the 
    reference volume flow is linearly  interpolated.<BR><BR></LI>
  <LI>Domestic Hot Water pump:<BR>If the solar thermal yield isn't enough to   
  heat the domestic hot water storage, the DHW pump can charge it from the   
  thermal storage.<BR>Switched on at qvMaxDHW, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; 
      T_DHWStorage,top &lt; T_onTS_DHW and T_Storage,top &gt;   
  T_offTS_DHW<BR>Switched off,   when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;   
  T_DHWStorage,top &gt; ToffTS_DHW or T_Storage,top &lt; TDHWStorage,top</LI></UL>
<P><BR></P>
<DIV class=\"lib-wrap\">
<TABLE class=\"type\">
  <TBODY>
  <TR>
    <TH>Symbol:</TH>
    <TD colspan=\"3\"><IMG width=\"330\" height=\"254\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUoAAAD+CAYAAABcBUzSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAEgtSURBVHhe7Z0HeBVV2scjJXQVxIIgShPWgqwVy4oFEXftoigq6q4FO0jsjSaIiCC9hE6AUJXQAoTk9pZCtezqp67rYncVEBHh/533zMzNzNy5JeHW5P0/z/skd+bMOXNn5vzu+542WWClpT7P7Y2cYvVDLVFxThaysnRW2y4AK23FoExXfZ6L3rUIFPTD0Dv3c/WTIqttLFYqxKBMUxEkDN6VajUWHMU5DEpW2opByUobcejNSlcxKFksFiuKGJRpKsvOHIvwtKbI8vsmUOGaNjTjkJ+lF4My7VSMHIuKq1mNjUZrWecVK7PEoExT1TYPK1z57Nmx0kEMSlatF3cisaKJQZm2sg7B2cOKr8iTNV9Tq22s2i0GZZoq2aG3XqnwsCy/bzI6r3j8JisGMSjTVZ/nIicFlTX5HlbqO6849GZFE4MyTUVwMlRe1Wqqh0VlMJ9Y6SoGJStEtcnDYkCzYhGDkpUmSlHnFY/fZMUgBmUaK206VZKgVJZruMaqJRzQrIwSgzJNRRU4FW2FopDUeFgp6rxisWIRgzJdlcJOlVR4WOzZsdJZDMo0Fg9bSbwI0CGXtQYvPsKqnhiUrFqqWrr4CKtaYlCmqyy8GvIwE12BU+lh1abOK1ZmiUGZpko+sFLrYdH3NX83q20sVirEoExXUe9z71xUYuJz5PZOkUeZDKWo8yrcDwQDmqUXgzKdJeBRmyovh96sdBWDkmVSLfOwePwmKwYxKNNM5OEQmFLl5aRT6J2szivzjwIZh94svRiUaSpjGJoj/LwkKUUeliWgk9TbzmJFE4MyI1QZDicaHCnzsFLUecVixSIGZVpLgUXSYJVqpajzKhWdSKzMEoMyzWT06HqDI8/Eiq63GchW21i1WwzKtJO+1zmJbZM6JdvD0peXdGcuZeM3WZkkBmVayzRUx9CGlxgl3cOicDtIR2pqSL4XzaE3K5oYlBmhyrbKhHs6SfawKG8Dl0T5zClWuolBmaYyejnJ9bKS6WGlHJQWPwz0/RnWLL0YlGkmAgfBqbZUVO37hrNEe9AhoCZZwJNVu8WgZBlV2zwsHr/JikEMyjQTe1gpkPh+ybzGrMwTgzKtVYycJPR0G5RkDyvVPwwsVixiUKa1kgdKgyeZMg8rBT8MLFYMYlCmtVIEypQpmaAUZWk/BvTF9T8ODGuWSQzKtBaDMlEK9aArZ0HRPg75WXoxKNNMVEmDno2FJaoCp6pco1L1w2Aqt6Z3XrGqLAYlS8oIjuSpdv8wsDJFDEqWVKpAyWJlghiUaSnjOpTJ8HAYlCxWeDEo005Kb6wZWjJUrIkki9AemNAZQakql5WRYlCmmyJU4GR7fdriGIn0ZiN+pwR2qqSqXFZmikGZZkolOPQeK0FSKyuhgI70nRIJrFSVy8pIMSjTTGnhYYlyDGF+QsGhtMeG5E/nkNAV3lNVLisTxaBMM0nPTteJY7bEAUtpG1XK0YMiOavpaGF+0JI0njJV5bIySwxKFovFiiIGZZopVR4ll2u0xHnurEwUgzJdRcudmeLd4pwktJ1xuckpl5VRYlCmqcjjCWkXTGiniiIuVygJ5bIySwzKdBX1vpo6FpIyEJrLTU65rIwSgzKdRZVY33aWrNrL5ao7WCxFDEoWi8WKIgZlmsqy7SwJ4nJZrFAxKNNVFr2xSRGXy2KFiEGZpiJPx9BuployeoG5XB5HyTKKQclisVhRxKBksVisKGJQpq30i1QkMyTkcpNTLiuTxKBMU1Hbmaysus6Gz3NzkOj6y+Ump1xWZolBmaaiCqzUW+HxaDNHinOUSp1Acbn0b+LLZWWWGJTpKvJw1IpLlVkJCXsn3tPhcpNTLiujxKBksVisKGJQppkqvRprS1RIyOUajUNvll4MynSVrnNBE68LmQClqlxWRolBmaYij8dUf5PSycDlCnFnDsskBmW6SlRWXheS16NkpYcYlOksqsT6trNk1V4uV93BYiliULJYLFYUMShZLBYrihiUaatUzUHmcpNTLiuTxKBMU1n2xiZBXC6LFSoGZbrq81zkpMKr4XJZrBAxKNNU5OmYw8FkhIRcbnLKZWWWGJQsFosVRQxKFovFiiIGZdqKe59rdrmsTBKDMk1FbWeysuoWbeCVxuOvVJXLyiwxKNNUVIGVeis8Hm0ucnGOUqkTKC6X/k18uazMEoMyXUUejlpxqTIrISGvNB53papcVkaJQclisVhRxKBksVisKGJQpqkq2850SmqbnU5cLquWi0GZdrIerqJZSKWOm7hcvSWuXFYmikGZprL0dJIgLpfFChWDksVisaKIQZkBone4UDiY7HYzLpfFUsSgTDNRKKh/ZwtVXq3iJjJM5HKTUy4rM8WgTDMZKmlxjvFFVwnsjeVyk1MuKzPFoEw76Xtj9S/i/xy5vZPVC8zlJq5cViaKQZlMnSWMrnisRunjIS43ssWrXFaNFT0mrGTpIWH1hJkrqpVROkofD3G54S2e5bJqrOhRYSVLu4UdLcxcWa2M0lH6eIjLDW/xLJdVY0WPCiuZisXbSYSXw+WGWiLKZdVI0ePCSqZi8XYS4eVwuaGWiHJZNVL0uLCSrUjeTiK9HC43OeWyapzokWElW5G8nUR6OVxucspl1TjRI8NKhay8nWR4OVxucspl1SjRY8NKhay8nWR4OVxucspl1SjRY8NKlfTeTnW9HJp+F5xhYm0hs0yilht5rcbq2rrL9xz5962O4nGdWbVaDMpUSu/tVMnLqS7I1JdmRS03MaC8a+yXVf++9PIvc17a2xJjVbWvM4uliEGZapF3U1f9G4O0pcCOyMjFjFhuYkApF5qo4ve19JirCkpSVctlsXRiUKZa5N2crP6NorhAUrWTrszDH2HLTSAoq/B9tQUqQvKqDiirVC6LZRSDMkNEy4KFAONILewSOQkEZRUU9jtXB5Qs1hGIQZkJsmqn01s44EXt6An3ov8IoEwSpCJ6zwxKVpLFoMwARfImY1k3MRJ0rL281IEypuYFBiUryWJQpr3CtNORxUJJqaqCL1mgjPDdIhmDkpVkMSjTXvEAZQRPjUHJYkUVgzLtlYowmEF5pDp06BC+/PJLlJaW4tNPP8WBAwfUPaxMFIMy7RUNJvr3vcRLDMojEUHR7XbDbrejpKREmsvlwr59+9QUrEwTgzIDFPv4yXhBk0F5JNq5cydsNlsQkpp5vV41BSvTxKDMBMUwn9vaqgvOVIT7Fgr3vdMclE6nMwSSZA6Hg0PwDBWDMkMUu1cZweLRS14dq0Knk0EZCkpql7QCJQGUlZmKAkqLCmN46Gm/yWuhwdHqg0yV25hcPPiGh9zieFYYVTNMtbKooGFQHol+/vln2SZphuTXX3+tpmBlmmIApR5kamUNPviRQUkDpfUDmhWvSJdel5YVm+LiWaoWnl8MyiMVwdLv90tIEjS/++47dU+8lClOBjEjdAaYeRJFdR+REBmYor9G5vOo2vXLUh46cyb0mTKxyoy2aWkt9utP1HDSSp654gJpFyUIUjVdcfDi6c7FBFPzMbkaOCjTYMUKPd4y74wVXUv1ex+hWT+gdF+t01fLaiEoWZrMbKHbKu5h3MhokokXlQo9j6pIBSXlr/P+qDD5RaypWxlSh6lQwRPVgEv/qnnSw69epOIc9cRpnzguWD6l0fIwfXEDKMUxhvNQ8w35LuHyrhE6UqiF3t8jz9Nk6n2psmoyKPXfTbs+6rNe+aNO98ZcwbU6paub6nHSadCujeHa6e6xZRmV26M6HqQqnbuaTliw3mrnaKWqnrfcpW0TPMjV569dI4vzCO5TFaXcICjlger/VLDy/U2ZqTKC0rTfdCG0tIY85X7dsSEXL/w+AyiD25ULod0z+aW1D5HyrpGi71f5UMRiwR+RoCLkYbiWCZbh4U3ROSRCYeqI3C6+n3Y/aDv9H/LDLxOb6og4Lvj8y8+Vz7gEib4+WJRhzEO9/+oxIeVX4dzFDgPoDXmZVe3zrjyGthuYFtxnPA/DvhjK1YGSCqGMRIY5VgVp0m+z2E8Z6x9kFVpB71E74WKRTn8y+mP0+Zr2BS+0YTuDMpzkTZc3OoIFL5wmukYW6cgM1zLBqqGgtLonoc+0UPA5FvdD3U7HKrcrhjoSVPi0wTJirE9VP3cjoELPrVLVOe+QY2h7MJ3ueNN56PfFUq4BlFRI75wc4b5qW/QFkZQLWJmpeb+Q+QtRGnGC+m3yxMS24I2gY8QF1z7T/uB5yX16yFrdmPA3NmLetUjyl1b3cBss5HrQfbVIR5bMa0f3MdXnkACFVkxV5rqje44jOjGm42Kp+CFlxFifqn7ulJcOULRdn06n6px3yDGG/HXHm89Dty+Wco2glAnMmRkf0uDFk9KfiCrzF5InqIerEKWx6HAJto+Y8qys5AKu4oGReRnKCX9jrfK+oNd8zJlToexPa4Vef82MNzYWKdfIKi/z9Y5ULoMyDpLPv/maCxmeaSH9cyz+D+vEmI8z5U8gMNeHkDIM26PUpyqduxlQ6nMYzFxIpFWSVuO86a/umISE3iI/EyhTJPNFiKcs8s7Keh1Nm76Bli1Hpzkw4wussF5lSF4MykRLVsbgd9JVWP13C8JGfhD3JEJFN18T/bXT7wtXhmF7BFAKVfXctfSVP+4qLLU8dHlX+bzlv5V55Ygfk8rjdNdIyHgexn3Ryq21oNQs3YEZKWTWP1/RZXo49RaSEYOSxdKrVk5h1IMy3YFp/PU2m97DiKxI+YQCl0HJYunFoDRZ+gEzArSCpgshzCKP2vIYzayOZVCyWHoxKMNYWgEzHDDiYNbhO4OSxdKLQRnF6tUbiu7dZ6pHplAJgGX4nnMGJYulF4MygpFX2adPPj777Cf1yBQrahgdu0XuCGJQslh6MSgtLO0AaVLkDp4oFlNXOYOSxdKLQamzdAeklWKCZpXBwqBksfRiUGYoIFksVvJUq0HJgGSxWLGoVoKSAclisaqiWglKFovFqooYlCwWixVFDMoq6PDhw2xslsaq2WJQsmLWL7/8gpkzZ2LZsmX4448/1K0sBmXNF4OSFZP+/e9/4/7770edOnXQtGlTvPrqqxKcLAZlbRCDkhVVbrcbV155JerVq4fbb78d9BqP+vXr47777sOnn36qpqq9sgTlH7/h4NZc/LakNw66R+Dwvm/VHaxMFIOSFVH5+flo164dmjRpgkGDBsHn86G4uBh9+/ZFdnY2Lr30UtjtdjV17VQIKAUkf8vrgd+mdsD+Sadg/0Rhs7ri8I+fqAlYmSYGJctSBw4cwJgxY9CsWTO0atUK48aNQ2lpKbxeL/x+v/Qyn3vuOQnQU089FYsWLVKPrH0yg/Kge5SAZEcFkjr7bf6lagpWpolByQrRt99+i8cff1y2R5577rlYsGABKioqJBzJe3Q4HEFgjh07Fm3btsUxxxyDt956C/v371dzMer3vftwcN+v6qeaJTMoKdw2Q1KCUniYh3/7WU3FyiQxKFkGbd26FX/7299w1FFH4frrr8f69etRXl4Op9MpAak3AmdZWRny8vIkUKnd8tFHH8V//vMfNTdF3+38CO/1fxDvP/gwvv/wY3VrzZEZlL9veMQalNM6qSlYmaaEgdK4qg292yXMy61iWvaLlQxt2LABZ555Jho0aIABAwbA5XIhEAiEAFJvBFAKydesWYMbbrgBdevWxTXXXINt27bJPA/u2Ye1jzyNV8W9HprVEPOu6IUvSpxyX02RGZTUFvlb7jlGSM7ogoPb5qgpWJmmxIAyylsVCaJVfy81K1GiMZHTp09Hy5YtpY0cOVICkjpurOBoZeRZUlhOgKXe8a5du6Jw40Zg/+/YMvglDD2qIYZnNcIwYVP/1A07FizC4YM1YyxmSGeO0KHdZfhtzvlBT/KPXfo2XFrGTnlXEb1lM8RXoOXl5EbjcneVdca8DJ763qNEvs1Uk1z6LsI7mmqoYgal3kOsfHeu+aXi9FlcRLqYVQGleoOLg2Xo8jTd/OCx6vZc7XWu9GAF1y8MPd4ybxZ++OEHvPDCCzJs7tSpkxxQTu2RHo/HEoiRjMBKNmzYMAncE088EbPmzMHer7+F67WReLNpS+FVNhDAbIy3W54C5xtvybbLTJcVKCOrEpSVUKxUcY7FM0rPcRBQuuOFZN2kPEx1Jb5SI0JR9w3vw64lig2UhptEN1JcMPWGGKBH6dSbrryP2hpKlqAUEAtu04PWdPMNoBTHKMWpv7Bq2SHnFC7vWq4PP/wQN998s/wBueqqq/Dee+9JSFq1R8Zq1G5JeeTm5qJLly5o3LgxXnr1VXz7n//io/mLMKFdZwzJypawHNXoOKx7YiB+/uJL9YwyU0cESsP/JPHZ6vk01APTMdo+9W+lU6Avgz4rVln39M1huvyCDocwE8RDz7d2KCZQhoDNAJvKG0vpjNdVvUHBtIosQWlIo7sZpn0GUAa3Kzc8WLb+VzpS3rVY1P540UUXycrQv39/2Gw2GT5bwa+qRnkTLFetWiUHqlPved9+/fDvr77CN24fZl/wFwnLYQKWQ0UonnfdTfivr1w9s8zTkYGSHtfKZ9dcN+izBFaEZ1imoQzoWRdpteMpX0M9I8k0yrEh9ZBkqi+hTQO1s/7EAZT0kTxHAasc/c2slPlih+QXCWamfcFjDdsZlFXRwoULcdppp8mpiC+++CIoXKahPlbQq66RV0rgpcHp9957r+xF7yGgWbZtG375+BMsu7Gv7NwZltVEQnN61wvxr9Xr1TPMLB0pKCufV3qOwzQN0XMcPIaOVz0+PUTNz7quHkiYBo/R1S3x2Vw3K9MpZoQpgzK86ILrLo4Mq003pDe1XVjeYSW9+WaEgFJ3w+TN0t/8YAivALHKoAyXdy3T3r17ZUcNDSJv3749Jk6cKD0/GhOpjY+Mt1GnEOX/7LPPysHpBOgVq9/Hr19/h6Knn8fIRs1Vz7IB3jmpHUonz8ChA7+rZ5wZOmJQap8Nz7RZeoiGgZX5eK0e6OuDxbGyPqvbQupmiBiUEaVcTMUqO3M00cXT/RJKsFamr7xJiixBKfILdsxY3kgyEdoL77XKoDTlfUGv+Zgzp0LZX0tEYxsffPBB6dldcMEFWLp0qRzCox9Enigjj5XGZ9LsnpNOOkl6smPE/7/88BMqJk7D2BPbClg2lB7myIbNsenZl2QHUKboyEGpPOM0hz4spOg5Dh5TNVAa6pusm6HHBp0ZQzlWYlDGLrrYBlAeocw3OJ6yyFt7X07LlqNrBTAJUr169ZI/FLfccgsKCwuPuNOmqkZApoHrNMuHBqfTeMunBw/Gd99/j09WrsbULn+W7ZXkXVJHz4rb78X3uz5Sv0F6Kx6gVABmCrvltkonoXJf1UCppFfz0fVay+hK2647zrA9pBwGZeyqAaDUv2CspgLz0KFDskPlrLPOQsOGDeWsGepooQHiVjBLtGmdPOvWrZOzfwiWNwlwb9u5E1/7SpHX83oBySbSqN1y9iVX4rMtNvXbpK+qDkpWpql6oMxw6UFZU4H522+/yTbI448/Xi5qMWLEiCoPIk+UUSdPSUkJHnroIQnwbn/+MzbbSvDz/32BdX9/DCOzm6uwrI8J7c/AjvlLcOj3g+o3Sz8xKGu+GJQmqwnA/Prrr/H888/LzhOaITNr1iwZ9lKnihW4UmEEbeppHz58uByY3uaUNpizcCH2fPc93CNG4+3mbWQYPiSrAd5q3hr2IaPw6/c/qt8wvcSgrPliUIaxTAXmThHG9unTR64VefXVV2PFihUy3KU2QitgpdII3ATwqVOnysHp1Bs/fNRIfLv7G+zIXYDJHc5Se8QbYXjdpigQ3ubPn/9b/abpIwZlzReDMorVqzcU3bvPVI9Mb23cuBEXX3yxnA3zwAMPoKioKOzKP+li1G5JnU30Hp4ePXrIULz//ffjgw8/whebSzD/il4iDBcAVYcQ5V13I/7j9qnfOD3EoKz5YlBGMPIq+/TJx2ef/aQemZ6i9sh58+ahQ4cOsk3ypZdeknO1tU4bu0PvTdphc/iE0QDzxA4LitUI5AR06o3v168fGjVqJAene8T5//jxv7Cq770YWedY6V2+nlUfU7uejw/yV+LwoUPqFUitGJQ1XwxKC8sUQJJokd2hQ4eiRYsWOP300zFhwoTgSuQKiOxwO9YHYWl3eFDmWIAKx0wVlgqsShwBuU/7nAqj8yYPc/DgwWjevDnOOvtsrFi9Gj/83+coynkZY5qdLDzLprKT551W7eAbPzktFtVgUNZ8MSh1lkmAJP3zn/+UITZ12lx22WVyjGJ5xVa43Arw7A4XnA4b/mPvhwr7jCAM/2u7RdiN4nOZTGMT2z+0DxdpcsXn1LVl0sB3v8+HgN8vV0und/WQh/zOu+/im692o3ziDLzbtrPsEafB6aMat0Dh0zn45T//Va9IasSgrPliUGYgIEkEFhpETpCkNyOuXbtWhK8VcDtLBHCcEogEQbKfHV3xte16bHHsEN7lRvzqaIMfHd3hcmxGsWM7djrG4oD9OHxgf0OG5RJaKQCm0+mA2+WEjzp5Kiowd+5cnHfeeWjWtCkef+opfPZ/n+HTgnXIvfAyjBCeJa1tScBcektffF2xXb0yyReDsuarVoMyEwFJL/1avnw5zhZhKb2n5umnn5aQke2RTi989pX4l/05eES4rXiQXux23oADzpYosy9EqW0RDvmz8Xvp0fDblglbib2BdjjsrYty+2x5DHmYbscm4Y1at2E6RXjs8YiyhPenGYX6brfwYAXsgmmdIp23Mk1U87rh97hlmyWF4NRuSUu/3XjjjbKT59bbbsOOjz7C7kAZll7fB2/UOVrt5MnGrAv+gn+uXo/DfyR/MWAGZc1XrQRlJgKS9OOPP2LUqFE4+eSTZVhKC1wQYLSVf8h7dDic+Mp+O761XQW3fYPwGLfiM8cAYFsW/uc7B9/ZeuDwh3WBnVn40tYPP3ouAj7JwsFAY3htayQoP7EPkkZ5KaaBzwm3AKS7ZAveX7lMenyzZ8/GnNkLsWjJUhQUboLD7YVHAtMJ2+b1yM+bj1mzRJo5cyLYbJlP/ipRvt0Fl4CtXZRHeWgrED388MPyh+HCiy7Cmo0b8f0/P8WGxwZidNMTZChOM3nGte4I/5QZ+OO3A+oVS44YlDVftRKUmagvvvgCAwcOlGMNaVEL/SBy/aIWNuFBuh2F+Nl+jgixW+MTxyD829Yfh3fWAXaJ2+0TtlVY+VHSi8QO8f//ZeGAtyV22UbjW0dP/Gpvg3LHbJlXEJICmNSTviF/Ol4acD+u6N4VzVu0wNFHHy3sZHTo1BW97uiHF9+cgoIiu4C3F5vmjUKPrm3QTKYhOwbNmx+H445TrPmxx6jbFbu8Xw7yN3rhdRuHM9EPAX3P119/Ha1bt8Ypp5yCGbNn4cfdXyMw9l2MP7mDDMNfysrCohtvx28//k+9askRg7Lmi0GZASJQ3CbCTlp1h96MSOEoQdI8iJzaFSnU3uLYjjLbAhzyNcTh7UfhoKexAsUKcbuFZ0ne5OFdRyn/lwsT2w/56uNQWQP59yPbEOGJbhN5Kh4q5e3xBbB27pu49aJToCyWcBTqZDeUIXHD7GzUURdRaNSiNa4fMARrbB4UzR+JS/90AuqL/ZSuQXZ99VjVjqonX2TWUBj9vfiOQVhSGApK+iEgz5maF6ZNmxZsdnjp9dfwzTff4OOlKzGp01mY2PFMfF5kI3KpVy45YlDWfDEo01j00q+CggK52g6BgeZGb9myRQLDOIhc+d/rWIOAfSl89vfgs63EN46/SihKD1J4joc/FV7k58I+U43+/z9hOwQ0y0SaD7Lwtau3CH8DskecOnZkp47LizLXOjx381kK4Oo0QbcrH8LrYydh0mRhY97CU/2vQcvGKgCPaY8nxi+Ha8sGzJ05Ge+MG4+Jk6di7Ijn0fvPzZU02S1w/m2PY9y74viJwisc/y7mLFqBzSVOYzunzrTXTNDg9J49e0r43t3/Xnz48cf4YvMWfL4xNWvaMChrvhiUaap9+/Zh8uTJOPXUU+Uajq+88ooMfWWnjQkgyjAgO76w/x37bKdiv6sN9nj/hF/dp+Kwp64IuwUkNShaGe2jsNybhf/Zu4lw/Rl8an8aX9lvw6eOp+DxB+BcORE9O9VRIHdcFwyctB47d+3E9h07sHP7drg3rcTrj9+CXtf0RM/rb8Uz7+TB5fEjIM63vKwMFdt2wrNxCR7ueZKSR6NW+OuzU+Ar24atFeWyLdLv84SFpGZau+WmTZvk4HSahXR5jx7YLH5AUiUGZc0XgzINpS1qQV4khZkETAJk5SDyUCNYehzr8In9Gex1d5LeIT4UgCwTECRPkjxIK0hqJtJIr3K7MBGSH3bVw/9s52CX4y24fCIMXzkBPTsepUCuQXN0u7EfXh07EfOWrcC6wkIR7jvh84uw2SM8UBEq0zt49OdH0Cx6fy4euPJEFZQnodfT40SI75FDgvRpYzHNq6brRGMt23fogIV5eTh4MPmrDDEoa74YlGkmWnX8rrvukm129GZEGgpk1R5pZRQmUy+3y74ZX7r7yfZGAl9USJJRGgrBS7Pwi+9MfGQfIvOUs3ecXvi96/HGA1fiuHoCcgQ6YQ1bnoyOZ5yJP19+OW579FGMHD8J+SvWwS6ALoGpO7d4g5KM2m5pFaLx48fL6ZvUQUSrEf30U3JHM5hBOWBAAcaN82D//vRdGo5VNTEo00jU/ti9e3fZaUMv5NqwYYMMM43tkeGNoEbtiuRZfunqh8OObKXjJlLYrZkAJXmgsGfhP/a+wit0yvxKHKUSwC7qXV83H68/ei3OPeM41KtbCUxpdeui2bEn4E/n9cJjI9/F++J4t4s6g5RzSwQoqZOHmiPohyRPeJN07ej95NSW+8knn6hXNfEyg/K008bhzjuXyb8MzJohBmUaiMJFmn7YqVMnnHDCCXJRC4KjVXuktRFonCh3zJFjI393NFdC6K0CktsEBGMBJRl5lCLsPvhBY+xzt8N/7bfiA/twOdyIYOkWUPI412PJ3DdFyDsAf+97O3qcexZOOqkpGuo8zQatz8QDb86Bw13Z5pgIUJIRLLXB6WvWrJHvKSdvnF6TS154MmQFShqju3v3HgwcuJ6BWQPEoEyxfvjhBwwZMgTHHnusDB8pjCRA0nAYKzBYmTLQ3I7P7APwY8lF+M7RAz+4L8XvnmNxuEzAkiAYS/hdLkBJPeQCsof89bCnpBO+t12GgCMfTk+5OK8ylJVXyM4Zv98N++aNWLl4IWbMeAtDBt2Fi9qcgPoSlnXQ8Yr7MH+DGz51qE+iQKk3um7UNvrUU0/J8abUvvv+++/LV2IkUuFAqYmASUv1dekykWGZoWJQplC0qEX//v1luEiLWixatEiG2tVdZNfpKBG2RXp/pSWL8ZvnRDlO8vAHUcJvgih1/viy8IPjEuwqGY1t9qlymJHTaYNXWH7u2xj4TA4GP/sixsxcDlfAD484T5qiGCgNIODZgtyXHkSn+opXeeIZPTBmqRN+jxJ+JwOUZNRuSUaLatDAdJrFNHbsWPz888/qVY+/IoFy1aoP0a3bVOHpLkZFxW65jZV5SjgojW90y1JfKWvxJjfDS8B0b42TpqWt7hvgwuWXOlHbGoWHBElakXz9+vUyfKQwUj/TpipGniUthkGDxWm+t5yN8y9xi/0ClLtUIBIw6a/+/w+EJ0mD0T/Mwveuy4VX5kORY5cyjtLpQ4XfhglPX4d64todVacuTul+O2av9aJceHAE9rLyMlSUubBw9AB0VkHZ5rybMa3ABV+SQUmmtVvS7CVaVIPWt3zsscfw738nZnV0K1BSqM2ArDlKKCjl+7i192sbFAsoK/dL2AZfu1ldUFrll3zRIPLFixfjjDPOkNP2Bg0aJMEYe3tkJLPLgeLbHZNw0NNEtjceKG2BP1yNlaE/2qycDwUcqeOGBqMTIEuFUcj9sbBdWfjSdbcCSRpsLvL1+gJYv2AYLj5BHR5U92hcdN3DGDlmKmbOzBU2EW+9+iiuObeVEnrXPQG9n3wbm50CgmpHVDJBSUY/OARxmsV03XXXyVdj3HTTTdi+Pf6rDJlBmRxAxlKHxHUOmpI29NmndKbX5LJCJGpGghTxFbSx3GTd/uA+3XZ1W3HQYzXlZ1C4/JIrCv9Gjx4th7G0bdsWY8aMieubEQlupfbF+Ml+Lr5zXIGPba9gR/FY/Oo9RXqWf7gb4lBJA2XaIsGR5n3TQHMByH2utvjOfiUO+psCrqPwpeNekSct1yYgJoDn8xdh5tCH0a3d8er1PgoNGjWT87ibNWuCRtnqYPQGx+D83k9gzppieFVvkkwB5Rz0/0sLJV2943D1k+8kDJRkWocYvRLjkUcekaMJzj//fOm9x1NmUCZHVaxDQX2O3N5aZCdSCWcmRT5DRilhoKRfrt5hf6boJlJlM1mYm2zpUdJDIY7RyqAbHrk8q/ySpy+//FJWVpp2R4ta0Ko55PFQmGhVyatj2qDzUsdiuBxF2OL4QC6ddtDbDPt87bDDMRb/53hC9oQfLG+MH5yXyB5u/DMLP3rOh9dWAL9tuRxD+YX9AXjtBUGv0ulyi3MtQd70d/D0fXfh4j93QRMBHjnXu2EjtG59Ki6/ug8efXEUFqwslD3kTt25uTwinF89HwP+2lF40s1w9IkdcdOzExMKSs2ozZLafWl2E81yopWXpkyZIl+hEQ9VB5TyGVSf+5wc7T35BDG9d0efrWBHsgBhTKAU0tKlyGHIRKUQlNFuchSAmm9ysXjYwsIvXH7JEb08i9ZU1MK/1atXS0+HwkOrin0kRrCk8Y/UVlniKBdgfBJ7bZ1QZp8nwLkTAfsyHHLXw/883QQY1+IL5/0y/P6ftxtc9iJxTIX0TJUhR8a2UlqH0uvxwlW8GSvy8zB9xgz5BsVp06Zi7tz5eG91Iey0zJrwJPWQlMcK785evAkrF83CtKmTMWXGLCx5f4P0WKNNW4yH0awmAibNcjrzzDPlrCcCJ406OFJVGZT07Oqef9lEpT6ThnpD6WJ9pkOe7cjPvCyTQ+6YlTBQSnCFBZIOeJpCQGn1a6jbXmVQWuWXWFEFWrlypVzUgsK+AQMGyOErFG5bVeZ4G4HyK3sffOR4Tf5PAKQVzvfZT8XXtuvEtlI5sPwnx/nY6+ygjpckD5cgGc7LE2ATwLRauJd6wV2RBseLfW7hWfp8fvnKB1q30jJdgoy8SvqBojbiK664Qs4Tv+eee/Dxxx+rd6x6qiooQ5wIQ10Rz6r6P6ULH/hUtw4pYlBWTYkDpbhd1BZigJd4IJSP1b3Juu1pDkoaRE7eS6tWrdCmTRs5tY7CbPJqrCpxIoy8S79jmQzDlR5xMjd2227CV7Y+sne8WHiQO+1v41t7L7kqugJK6/xqgmntluvWrQsuqkGjD0pKStQ7V3XFF5T0kQAm6k+O7vkOUXXrkJBWnrkOscIqgaAkqbDU3P8gyKp7k3XbjwCUvXrNx5w5Feqn+IvejPjCCy/IXu2zzjpLrqFIXmQ82yNjM3o1rVfCUdtGYfl2+yTsso8SHmWZ2KaE2ARJGocZ3pOsOUawpPtB3j21D1LnWseOHeU41uoMTq9y6E3Pqu551IfeUmJ/b3FeORHdverWIWM7aOS2fZamBIMyPaW9L6dly9FxB+bOnTvlNDp66Re1R1LoTZUyEe2R1TWCp7J6udYGSb3b1RvknskmmwvEj9fEiRNxzjnnSO//7bffrvLg9CqDUkgJfRWr7MzRRJCLFhbHAsrKMrQwOxSMsZTFqmGgND8c1r+W2svF4g3MjRs3yh5tguRrr70mZ97QQrNWlZQtPYy8S2qjpL/aWy1pdMLnIvSNVdUBpUGm0JuVfqq1HqXZjgSYv//+uxzuQ2MjqT1y+vTp8m2Je/bsOaKZNmyJN2qb3LFjhwy5P/30U/kSMxryRNCk9uRYZAZllZdZiwjK2H78WYkVg9JkVQUmvRmRXnpFizDQdLnCwkJ1j7LgBYMyvY1ASWuA0g8bidayHDFiBJo3b46uXbvKTp9oMoOSpjDyMms1SwzKMBYLMMkDoeEl5IHQS79ofrFeDMr0Nw2U+sHnNGKB1rds3769bLekCCHS4HQrUPIyazVLDMooVq/eULlEllnUQUPDSmh4Cb1GlmbemMWgTH+zAqUmanO+8MILZbvls88+i++//17dY1Q4UGriZdYyXwzKCEZeZZ8++YaHnirUihUr0LlzZzkdjqbC7d+/X91rFIMy/S0SKEkffvgh+vbtK34w6+GOO+7ARx99pO6pVCRQ8jJrNUMMSguzAqQmej0DLbDbrVs3rF27Vt1qLQZl+ls0UJKoHZqmO1L0QLDcvdsIPCtQ8jJrNUsMSp1FAqQmWshi0qRJ2LVrl7olvBiU6W+xgJJEIxtoQPrMmTPlfdXLDMrIgDRNwtBMTpYI7eE2TqKg/ZHGTlop2jHmMpW0NHsotGxtvKVFnjVcDMoYAVkdMSjT32IFZSSZQRmrQqYyhgDIPA04GvSsFO2YcNBTytaKpoHqBm7WMtVqUCYKkJoYlOlv6Q1KEm2L4MklDJRCWrqQMtRj1O2xrQmb2aqVoEw0IDUxKJNjLqf2Goyqz1NPf1CKrUFvjvbrw2TVooIy0jGm/aa8lKmW5imOOlCKY7TvEDo9suaoVoIyWWJQJscIkFu9m+Fx2cT/1mnCWeaB0rQ/xNszK9ox1mVqigpKfdkRF6bJbDEoEygGZXKsxOHDd8XDsdW+CjZn1VZoyozQW9sWDXpWinaMVZmqtKmVIWWoxzAoWfEQgzIZZkexqwK71zyJHUVzBShpVSSrdNaWCZ05lWmSCcpIy7ExKFlxVLxAabfTWwXdKCt1o7zMjjJh9NdspYHaB2TyIAOO9dhT0Bc/r38YDmfV2irTD5S69kJhRu4kCpT6MhU4hrY3UjpTpxKDkhUPxQuU9AqD9etWYtToVXjh1VK8+JovxJ592Ye33/WItMl5B026WJF7O/658S0gvzP2rLpRbqOOHXO6cJZKULIyRwzKBCpeoAwEnJgzdzO6X2nHC69sxPgJq/HW2AKMfLMAY8TfseNXY+jwtbj77z5RXu0BZbHDi9Lildi78m84nN8Ne967OdgDbpXeymoOKM2eYc3tgU6FGJQJVLxAWVbmxKQpG3D2hVswaowHeYvsWLHKBputGMuW2zB/oQPjJzlw7z/8EpK1AZQEQ5/Pg19W34XDi8/BocXnYZ8AZanfDZs9M0JvVuaIQZlAxQuUPp8XixcvwaCcPBF6e3Hfw15ce5MP8xa48NfbvLjnHyUY/LxLQJTaMmsHKLe4KvCVbYIIuS/E74vOxx+LuuGX5b3lPg69WfEWgzKBihcoqWe3vNyDnTuc+OdHm4UH6cSV1zkwf4Edt9xpQ+6s9/HxhyWoKC8WUCWw0kv/7fB4CBpW+WWuUUcNQXJX4UQcWHgO/lh4Nn7POw+HFpyJH5der6ZhULLiKwZlAhUPUJJ36PHYRbjtwvRZbizIc+HlIT5c2MOF14a5cNVfnXjupY2Yt9CL3NluzJztwsxZLsye68KatS64hYdplW8mGgGw2L0NuzZMxO/zz8bBeWfgwPxz8Pu8rvgj71xsLVqQUcODWJmjlIKShkfoG5+VkQWmRumo472iydzIXZ08qqcjBSVBsqzMgWEj3SLUDuC5V7wY/IIXOS958dzL4u+L6t+XfBgs/teMtt3/iB+PD3LD7yuW4Ximh+T0lkgaIvWv9aNxYM6ZODinMw7MPQu/C/tjVnt8W/A4XG53lbxJMgYlKxalDJRyapTlmCsjECvHcx0JKCuPk3BO0livWEDp9SqhMv2lz5Te53MKE+F2mR0jRntwxXWlWLjYhe3b7CgtVWzrVgd2bLeLcFz8X6GModT20faly1144hkCpfKubhqL6XYrwNSXnwlml7NtnPjivcH4I7c9fp/ZAQdyu6jWGX/MaIcd6ydji3tryLHRjEHJikWpAWXEQbJ6sOlnB+i2q8fHtmqJCbBRB+jGT5FASd4dwWvFKhcWirB6+UryhFzYvHkdlq8owsr3PHh9hFdCksLucuFZasfSEKDCwmJxrB35y1xYXeA0hNgE3vkiRH/+FZ8Ixf24qW8prr4+gGGjSkW5LricR9pmmkxzynD6gwLhSU5tgwPTTsVv0zsE7Y8pJ+GrJfdImOq9yZIY31POoGTFopSAMnRGgl4ENl2oHPT+TKAU+7Q8QmcR6GUEZbp4lNTRsm69EzfeEcBDj/tw290BFJf4MH5CHnr0moP+D/px38PCk5SQrDye4Lp9mxPDR25Gt0vc6HlDuYChV3iOlWk0UL48RIThIly/+Ooy9L3PjwuvKMPifBf8fuO5pLMRJL229dg35XT8NvFk/DrpNIMdfLcFPs8fgC3eHSJ95TXY6lwrwBl9mBCDkhWL0hSUVmAzgTLmqVMm8CbJmyRFA+Wq9514+AkfAn67CJN9KNrskF7f8y8Xori4SB7rF9DTH0cQLFhjw613edG1+1b8fUBAhN5GIFCaeQtdsq1y4HNeXH97ADNmuXHeXzIQlC4vtm3Ow6/vdsT+cafg1/ECkDr7fcxx+CLvIQHK7TK9Bsf/FY3AR45lUT1LBiUrFqUm9NZWJVE/GmUEZSUUjwSUuvySqFhA+fggH8pK7Xh0oA9LBMRyXvBi8jS/bIO06nwhz/Gdd93odnEprr3JhmXLlbZHfRpq76SQ/q77/ehzt1/CeNJUN86/PDNB+cF7b+PXtzpg/5sCjqPbG2z/m22xd0wXVKybjSL/ThS7/LB5y/DzlMvx7fx7YPNtE/mEXn/NGJSsWJSizhzzEvdCAnbKRyPY4uNRpi8oHxOApND6nn8E0PXicun1kfdHnTJWx2wucuCaG0tFGF2ON0fPx64dm4QHqXTWmNNv2eIU5pAe65Tpbpx7WWaCcuu6ufj1jc7YP0yAcngHaftG0N/22De8Iw683gpfTb0DO9bPw07PZvxr+RvYO+J0/DL+cpTbC+XCGVZ5kzEoWbEoRaAkqbDUQuIg6Ahsuu1BIMYPlL16zcecORXqp8QpFlA++rRPgit3jhtj3vEIYPoxcYonBJTKeEoHXhvuFZAsxcVXlYqwOiCHBhFYqZfcnJ7ypYHnO7ZlNig9RWux9/U/4ddX2mHvqx2EdRTWCftePg37XlE+//pCaxwYdiYOTr0Rv714sth3qkyzY+nbKPGUW+ZNlgpQ0its6Rn86Sfr1xyz0k8pBGXqpL0vp2XL0QkFZqweJQ3tGTqSpiL60eumUkydYe1REvxowPmCRQ7k5X+I3rd8gOv7+LC6wCXz09J5PE6sXbcJDzyyFnf0L8PSpYWYnuvK2DZKT9E6/O+1C/FrjvAgnz8de4X9Nvg0/N+sZ/Hj0EvV7Z2x71kB0GfaYt9zp+PXwadi36grsWvLKpREGISeTFASINu1G4djjhmJ7OxhCX8VCSt+qkGgNHmiwsJ1GGkvF0s0MKsSet/3iB833xmQYXU4UJJRR80HO2144y0/Luvpxdq1JSK0pk4MY5qly2nWjh2X9nSj3wM+TJ6WmW2UZMX+CuxY8i72P9YGewd1wb4n2+Hn4b1QWrwRn8x6EfufaIu9AztjzxPC4xR/Kc3epzth/6DO+HjxWBR7U+tR6gGpf+4YlJmjWutRmi0RwIy1M4dA+Tj1ehc5MWSEF+9O9KCiwi47aaw6dLZtdcj1J598pgQety0kTWnAgfxlNEPHiYHPOnDjHaWYNlMJvRctyTxQuos2YOvqBdj3VDfseeR07H+gNf41aTA2b/0YgY2rsefZi7FnQBf8OqoP9jwqgCn+3/dgO3zzYk94NhbA5k6NR2kFSP3zxqDMHDEoTRZPYMYCygcG+LF5s0P+LSx04NWhLgwTYXjRFgfWb3CGzKah4UIL8px4bbhbLtKr7/GmDh2fV4SrNofwIgMiDQ0yL8PFV5XhhtsDuKRnKZYuyyxQ2l1u+AuW4rMxj+Prl27Cnnvb4df7O+LTGa9ji/A07V4/Pp30HPbd3VZ4mXcKOF6PPfd1Euna48fnemF74SoRvocfIpQIUEYCpGYMyswSgzKMxQOYkUBJgNu4icZR+uWAc/pLoJsmwu57HvTLcZUUir861CuHD2mwpFk5K1asxXvvrRMhtshbzU+DJP3f7wG/XMR3yxbhlXoCwvssxR33+jFJhN+0wIaVl5q+5oTN48e2hZPxv7vPxp4+HfDrjSfjk7eFR7n9I3iLi/DNK/dg381t8fmr92NX7hjsubU9fr7rDOzr2xk/P/IX+NesEHn4LPKOLyhjAaRmDMrMEoMyitWrNxTdu89Uj6yaIoGSjIDlUcNr+kvbCIg0HZHGS9L0xKv+Vip7uimcVtLYUFy8SY6VpLSaUbskHX+XgCSB0iHAqaWhv2TkxWYWJBWzi2voFPbBjDH46cYu+P7u7vj4refgLVgJt9+Pb157EHsvb4my5Quxbda72NuzNX6+4XT82OtUfPjUbXAWbYLdbT1EKF6g7Nt3KerXH2b5DFkZgzKzxKCMYPQw9+mTX+0HOhooyQhcmum30V96iVjeEht6Xu/BuAnFcIlQu6SEQmvlr2bkTW4ucqHf3wPod7+ApDhWP1zInH9GmtOJ4tIKbJ03Fd/ccik+eekJeNcXoCRQBs+a97F9+ljY/AHsnDoWey47FT/dcw0+7tER7mnjYS+nxTKs70G8QLl79x489NBqHH30SPnjavU86Y1BmVliUFrYkQJSUyygjGYBvw0TpxSj980O4V360fOG0hC7+vpS/PUWF/7xqBd2m+JdWg1Az3wTPxACeqXTJ+GTay9GIG8e7D4/7B4vigPl0rvcJUJx/8rl2HbDFfA/8QAcbpeErHV+8Q29SbECk0GZWWJQ6ixegNQUD1A6RSX3+UqQO3s9HnnKiady/MJ8Bnv8GT/GvLNBpC2R4bVVPjXHnLCVlcM1JxfuhfPhEJDU77d7fHAWFMD76gtwlNjE/vCzcsgS0ZlDCg/Mp4UZh7FJC84+s9quKXTyRMjkixBFO8ZcppI2dPEYSlf5nu+qyfiO8EwUg1JYvAGpKR6g1Mzndcql1igcNxsNL/L7Y3/7YjzOJ9Vm9/pUCBq/C7Vnule/L39gHO7oS60lCpSawgGTnjnnm1ebxvqaoWae6psoUJr2Sylla0XTCl0GblZJDMqMVKIBqSkSKN3C8wmUlqO8XLXSUvi9HkvYuUTaMjVdacAnX8lqTOOU+ZWa0lCPuFsARdtuaaUBeEV46jTklx5GK5r7/KXix0A917IylPp98LgsQmmnG/7SMiWdAF95RYXxeworKysVPzjiGuuOSzQoNZmB2ajR8BhASaJtFmuyakoYKIW0dFZl0LRhkxcqZUorPVOdxxp+1bD0Vq0EZaIBqckKlC63AKSosMWFq7Bw3mRMnDQREycImzUbiws2wxuoQMCreULkJdpQuDof0ydOwLvvTsTMuUuwoVgJyYNpHDasf28JZoi8xo+fiNx5+SgUadwusX0VbZ+ECVTGRKNNEDZ5zjwUFAmPVMDVnS4dPk4XfIFScT5b8N7SOZg+TZzvhAmYMHUqcpesxGaHH2X6HwyR3lG8DgtnTsWEdyeEfE/NZs7Mxcp1m+Ap9QcXOk4WKDVpwGzS5A0ExvaMAZTEJM2bo/2V0AlaVFBGOsa035SXfBOBOeQmGOrO0xCmW4Cyd24xe5Ss8DKC0gmPT1TuTasw+rUncNM1F6B964aoV78u6tath/rHt0KXS/+K+wYNxbxVtCKQSCuO9bqKMOO1e9G2UUM0aNAYnS++C9PXOuD1qKt5C0/K69qIyS/ejQ7NGiE7uzHO7NEfMwpEqO4vwsRn70L7Zo3RsGEjNGrYANn16wmPRrG69eujaZu2uORv/fDqhGUocngFfMJ3fCTDnG4PfK4SLJ45Gg/3vwEXnn0Sjm4mzrWuuE5NmuL4P52Ha+98BCMm56HYqXiX5HG7CibgutNboWGDhmjUuBEaZNcPfk/FstHy+BNwXo/rMXjUNGxweuUPQ7JBqZcCEQOWYgClab+Vt2dQtGOsy9RkBcqI582gZFVVelC6PV44NyzEUzddhhYNtF/wemjcuInwLgTg6qrb6jRE5+63Y/ziTcK79MHnLsLUF+5Ac/UXv805t2LKGgd8OlD6XIWYMLgPTlDTnHbBHZgiQFkhQDnu6VtwvLo9K6suGjRqLMpsLMpsgoYCmsr2LDQ9+VI8O2UFHAJUoaF9cswpQm2vaxNmDnsMF5zWInhu9Rs0kufbuGHd4LbGJ56Jf7wyA5tcXnh9AbhWj0OP1k2D++tmN0RjOkZ+V/FDUf+o4L4GJ10ovusqeEs9sKc9KPXbokHPStGOsSpTFYXXlM4SfmHOm0HJqqqCoBSeT8D+Hl648wI0UStrs/YX4cGXxmBh3mIsXjIHk954CJe1OQ515P566NLjYeQVe1HuLcK0l+4Mwu7Uc/tgqgUoJz57B05WYduh+52YqoJy/KDb0Kqesv2UP9+AoVPmYdGiRcgTNmPcG7j5gi5oeJR63HWDsEqE4R53CrxK+h5eBxaPexLntTpank9WoxNxye2PYdyUOeIaLcK8mcPwYO/zcRztE9bgmHPx3NTlcNHQoILxuKrtscpxWS3R57lxWJSfj4ULFyJv8SLMGvc0rujUTN3fAn977B1sKSuF05Z8UNIMHlpmbfuEa6KAUulQqUyTTFAaO2DIswyeBx2vO4ZgaAi9gx6odv4MSlYEKaCkDgkvVk3KwVnNlQresEU3DJq8Ep6AH34RYlOYHQh48P74l3Gemibr6A54dEIBdpaVGEB52nl3YOYGAdBSP7xexZuiEHvqC3eidRRQdrrsXszeQCsTBUSZfpSVifOa9jzOO6ah3N/k5Kvx9vKN8HjcSe/cIY/bsWERHr22C+rRdz2qGS65+yUs20yzlJRr5POLEHtdPp7s0Vm5RsL+dNcQFLnL4F3zrg6UrfDYxDXYunMnKsqog2cbduzYjOF3dEe23N8UPf8+ChtLS+GyJw+U+imOtMyadWeO8r00M/Y0JwqU+jIVoBnAKEXpdLDTd+aYylfCdSWvnBzF+5QwFduMeWaOGJQJlASlR8DQvg6v978IdeXDk41z7hyCLV7qba703JxuL9y2jZgzaRSGDBmKISPfxtxVm1Dm22IA5Snn3IR38jegaFMhNmzYgA2FG1FUuBJvPXULTooCyo6X3I0ZBTYVhE74K0qxedmbuKJlE7m/YcsLMWThBgGtJIPS6YRXlLly0mCc21o9l049MXJugQCkt3IkgNMlvF0X1ubPwBvDxTUaOgRvzcyH3eU3eZQn4r4R87BxyxYUimu0cfMmrM4bhhvPPgZ1j6qLY9pegOcmiNBb/DglI/S2mgNOHYo84DxzxKBMoAiUHn8Axaunoc+ZzZVKnH0s7hy+GAGfxVAgAQKvSB8ICPP7BRTc8LiMoXf9Bk1xfKs2OKVNG7RR7ZQ2J+P4Y5sqnljWUegYBpSdL/87Ftm24oMPdmGn8La2l7mQ+/o/0LFpfbm/WfsbxXGbk+5ROkXY7XFvxJhH/4qTspVz7XjtQ5i/xg6/1sQQNGUolLxGwvxet/wxcq3Wg7Iujm55EtqccopyjU5pg1YtletTr/E5+Mcr80W4HoDbYU9oZ06kRTLiD8pQbzRTvbd0FIMygSJQ+kSIvCF/JK5o3lh5gJu0xBMT1oaM59ODgIb+yOE/BE4TKKNbeFC2bHc++j70JAYOHIhBg57BQ3ffis7Hqe2BWfVx4QMjUGhzw201TjGB5nR5RJmr8HKfv+BY9Xucf9tALCn0hm0v1a4RXcNQUEaweiej23VPYsZ7m+D1uWFLAChjWUWIPcrMEoMygQqCcukoXHmcElJGBiUNIaK2wzJlcLQIO71OIyiPb9cdD704HCPfGIHhw4cLewMjR7yCv994MZrXUdKEC73DWp2GIiz/B6Yu3wJ3DLNZ4m0aKF+5/fJg7/75fcKDkoYQBUrpGpXJwfWeEFAei6v7P4M33nxTXqMRb4zCkJefwFVdWqr766HrrYOxcos41mmLGyhjAaRmDMrMEoMygSJQUii95f3puO1MFZT1j8XtwxajLCT0FiGly471q/KRO2MGZkyfjSUr1sPlLcZ0HSjbXXAX5mwpx87tW5UZJxXbsHOrAzNfvjtqZ06DJi3QtkMndO58Ok4/XbEzr+iJx4aMxbJCmyg/nJebWFNC781469Fr0UoNvdv1ehDzC+zw0aIW+rTC2y3ZvBaL5szCDHGd5szOx0aXD+4CU2fOpDXYtmsXKuQ12oqKijKsHPcYTm+hDImq1/pyDJ+3XniVLmyPAyh5mbWaLQZlAkWgdHsD8BWtwjM3thcVhCpxNs667WUU+coMnTkuTynKHavw5HUd1XTH429PT4F/mxMzdaA8kuFBHS+5BzPpWOGpul0uOXTJRe2g1Hsu8ksFJMmc1MTgcWPRqH+gY0sFZA3aXYERc6gzx1f5g+Jyo6zUi7yR/YNAbdHtPiwp8cG3doIOlCfhobeXydWElO8kfoS8frjXT8X1nY6TaerUPwfPTFwGZ8ATF1DGumqQZgzKzBKDMoEiULpEmOj3u7B4zKM4tZFSubOb/gmPjVsO/1YRPpaWolSEkFvLvcgf8zg6HK0MjM5ufQ5emW/DzkCxIfQ+IlBeeg9y15pAKcxqfnmyjYYH2Qpmod9fTlPHkjbCubflYGmRSzZDlIrrRB60a61I072d/D5kPZ58F3ZvBTyGXu9WcmhVxY4dwetbsXUrCnNfwDknZStpsrti8MTlIaD83CkqxZAItkomM+jwd0BvbX/OHmSdtxpZDUT4XYeXWaspYlAmUBKUAkR2pxelztV4/e7L0Fit4NknnYG+Tw/H3LnzsWDBQox9uT/Oaq5W4qzGuPje11EovMxST/wGnBuHB4XCKqUmvEqfz4llE3JwwcnqwPB6x6Drdfdh5LhpWLBwEWZNHo47L+2ojoXMQpNOvTF+2UZ4A2VwmQac3/rMGMzLyxPXdw7mzl+AWVNHoe9FbVUIZ+GYP9+Oae8Xi9DeYWijjApK1XqLdJoMoNQsCjCrBsrQHu0jX36NVRUxKBOoICjtdtkzG7Ctwsv9eqJFQ9NDr7cGzfCXmwZhyRYChwiJXUWY+uIdaKHub9MtzBTGnD44UU1z2oW6KYwDb8EJ6sybdt3vwvR0BSWZ04OAz4ZF7zyHSzuow6nCWJsuV+H1aSvgCHjh8SpTGK9orc28iWzZJ1+MgeOWwVXmCxlHqQelHoak3KmV+8hyPlK2hxsehO+BK3XArGNaZq1qoNSDUJnxcmTLr7GqIgZlAqUHJYGAFm8IODZi2rhXcffNl+H00xqhfrayaEP9Vq1x1tV98MKb07DJ7Ybf55ZellwU49V70Ca7PurWbYjThbc4XYCSlkZT4KIuivFCP7Rrko06dRrgjMvvFUB0otxXhAnP3onTmortWdk4o8d9yCVQivzTEpRk4vsEvC6sXjINOY/fhb+c1xrHHiuuES2KcfTROOHsS3DHgJex4P0N8JX64BLH0HV1FkzAtZ1ORP169dGgYQM0yM5Gdv36yKa/coGMujjhhPbo1fdxDFuwCsUCrm5xrHkcZSRQkopF6B2EpQAngSgcKA1gnWNsw2xYv1MQ3Dk56pxqCUD9VD/6TAC0AKHcpqWNAkr1/2J1doxhkQsTUIPzuNXtudosG4JycDZO6PGWedcgMSgTKDMoyWgGTml5BVy2jXh/1SLkifBQ2vIVWLPZgUBphdLJo77Kwemwo2hDAfLzForwMw9LV6zGZhtt1+BCYwnt2Lx+NfIX5YkwPg/LVhbINC6nHZu07fOV7UXy2OSOk6yyURguwmm/CMU3rluB/HxxfRYuxMIlS7B8bSEc3jLheXorrwGNObVtwqqlS5S53do1NdhCLFv2HrbYvfDTMmtqu2xVQUleoj7MzhWfrUBpDuE1iMlOn7ueQP2sy7BC/E+SU/5UWBkWnCAISa/RCpTKcYpTSftVoOlNAyDlIz4H89UWu6D/I4FSHGPIX/VgQ84xXN41SAzKBMoKlJpRJ49Pm4WjzsTxhQmJXQKu/gB1aAQEPGgptHBpAoY0tHCvcXvqVgaqjtGbFz1ef+U1Eub3UkeUBejJ+6brKb6nPr3e/LTor2mR4iqDUkjvKVL4HQJKsU3br6XRywAakgEuAkrq/5SuElTRQBndo6wsUZc+EiiD25VQX+UkFVwZ9kfKuwaJQZlARQIlW3pYIkCZo+4Ll0dkUNJHCl8FnHK0bVbw0W9jUCZaDMoEikBJ0+yoMhIs2dLPiouLsXXr1riB0qoNM0QEGh1M9KG3lNjfOycHOUGYmuGjgKsStjGAMhhG08fextA72K6oy7cqoAyXdw0SgzKB+v777yUobTabpTfDlnqLZxuluV2S9oVT5VJkAjLBzhxNBD59pwh9rkwvjzFQMQZQiv+DHTOmtJXnUrksWpVAGSHvmiIGZQK1f/9+7N69G1999RX++9//sqWh0b357rvvcOjQIXnPooHS4DGqg88lKE0ADeeNWorAYwBlnGWAXpyVyLzTSAxKFkunSKA0j6PUPCcCpWFfuJA7nBiUaS8GJYulkzl8Dmf68PKzGI8hixSOs9JXDEoWS6dooDQP9SFtWWmd1soYlJkpBiWLxWJFEYOSxWKxoohByWKxWFHEoGSxWKyIAv4fQBkl4L8F+oEAAAAASUVORK5CYII=\"></TD></TR>
  <TR>
    <TH>Identifier:</TH>
    <TD colspan=\"3\">CoSES_Models.Houses.SF1</TD></TR>
  <TR>
    <TH>Version:</TH>
    <TD colspan=\"3\">1.0</TD></TR>
  <TR>
    <TH>File:</TH>
    <TD colspan=\"3\">SF1.mo</TD></TR>
  <TR>
    <TH>Connectors:</TH>
    <TD>Electrical Low-Voltage AC Three-Phase Connector</TD>
    <TD>lV3Phase1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Environment Conditions Connector</TD>
    <TD>environmentConditions1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the condensing       
                boiler</TD>
    <TD>StandardCBcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the CHP</TD>
    <TD>StandardCHPcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                     pump</TD>
    <TD>StandardSTcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the DHW pump</TD>
    <TD>StandardDHWcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                      supply and return is constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP input - electric power</TD>
    <TD>CHPIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to district heating heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                      negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of domestic hot water pump to charge the DHW     
                    storage</TD>
    <TD>qvDHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Parameters:</TH>
    <TD>Heated 3-zone-building with changeable heating system</TD>
    <TD>simpleHeatedBuilding1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature controller wiring for heating system</TD>
    <TD>heatingUnitFlowTemperature1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow generation and electrical power calculation</TD>
    <TD>pump1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW_demand</TD>
    <TD>dHW_demand1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                components</TD>
    <TD>grid1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat storage with variable temperature profile</TD>
    <TD>WolfSPU2_800</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar Thermal</TD>
    <TD>WolfCRK12</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow generation and electrical power calculation</TD>
    <TD>pump2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Specifies a Thermal Volume Flow Connector</TD>
    <TD>defineVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Extracts the characteristics of a Thermal Volume Flow Connector</TD>
    <TD>extractVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                components</TD>
    <TD>grid2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat storage with variable temperature profile</TD>
    <TD>WolfSEM1_500</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Controllable valve for volume flow distribution</TD>
    <TD>distributionValve1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Valve for volume flow merging</TD>
    <TD>mergingValve1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Hydraulic Switch with two heat generators</TD>
    <TD>hydraulicSwitch2HG1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBControlIn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CHPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Boolean expression</TD>
    <TD>CHP_EVU</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>TColdWater</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Green city of the CHP model based on measurements in the laboratory</TD>
    <TD>neoTower2_GC1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>VolumeFlow_DHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature difference between flow and return temperature</TD>
    <TD>CBDeltaT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature of CB</TD>
    <TD>CBTmax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature of CB</TD>
    <TD>CBTmin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum electrical power of CHP</TD>
    <TD>CHPmax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum electrical power of CHP</TD>
    <TD>CHPmin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, solar thermal collector is CPC collector, else, solar thermal 
                        collector is a flat plate collector</TD>
    <TD>CPC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inclination angle of solar thermal collector</TD>
    <TD>alphaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Orientation angle of solar thermal collector</TD>
    <TD>betaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in series</TD>
    <TD>nSeries</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in parallel</TD>
    <TD>nParallel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Effective surface area of solar thermal collector</TD>
    <TD>AModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Absorber volume</TD>
    <TD>VAbsorber</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature within the thermal storage</TD>
    <TD>Tmax_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature level to calculate the stored energy (e.g. return          
               temperature of consumption)</TD>
    <TD>T0_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, the temperature profile at simulation begin within the        
                 storage is linear, if false the profile is defined by a 
      temperature                 vector</TD>
    <TD>TSLinearProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of upmost heat storage layer at simulation begin</TD>
    <TD>TSTupInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of lowmost heat storage layer at simulation begin</TD>
    <TD>TSTlowInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Vector of temperature profile of the layers at simulation begin,       
                  element 1 is at lowest layer</TD>
    <TD>TSTLayerVector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature within the thermal storage</TD>
    <TD>Tmax_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature level to calculate the stored energy (e.g. return          
               temperature of DHW</TD>
    <TD>T0_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, the temperature profile at simulation begin within the        
                 storage is linear, if false the profile is defined by a 
      temperature                 vector</TD>
    <TD>DHWLinearProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of upmost heat storage layer at simulation begin</TD>
    <TD>DHWTupInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of lowmost heat storage layer at simulation begin</TD>
    <TD>DHWTlowInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Vector of temperature profile of the layers at simulation begin,       
                  element 1 is at lowest layer</TD>
    <TD>DHWTLayerVector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of people living in the building</TD>
    <TD>nPeople</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nFloors</TD>
    <TD>nFloors</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nApartments</TD>
    <TD>nApartments</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heated (living) area (e.g. 50m² per person)</TD>
    <TD>ALH</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, use standard area-specific heating power, else define it      
                   manually</TD>
    <TD>UseStandardHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Area-specific heating power - modern radiators: 14 - 15 W/m²; space    
                     heating: 15 W/m²</TD>
    <TD>QHeatNormLivingArea</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1</TD>
    <TD>n</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 -     
                     45°C</TD>
    <TD>TFlowHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal return temperature - radiator: 45 - 65°C; floor heating: 28 -   
                         35°C</TD>
    <TD>TReturnHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference indoor temperature</TD>
    <TD>TRef</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximumg flow rate in Living Zone</TD>
    <TD>qvMaxLivingZone</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TLiving_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TRoof_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TCellar_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If the presence is used, individual presence data has to be provided,  
                       else standart presence is used</TD>
    <TD>UseIndividualPresence</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with presence timeseries (presence in %; 0% - no one is at home;  
                       100% - everyone is at home)</TD>
    <TD>PresenceFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If individual electricity consumption is used, individual consumption  
                       data ha to be provided, else standart load profiles are 
      used</TD>
    <TD>UseIndividualElecConsumption</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with electric consumption time series (consumption in kW)</TD>
    <TD>ElConsumptionFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Table with electric consumption time series (consumption in W)</TD>
    <TD>ElConsumptionTable</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>ElFactor</TD>
    <TD>ElFactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, night time reduction is activated, else temperature is        
                 constant</TD>
    <TD>ActivateNightTimeReduction</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionStart</TD>
    <TD>NightTimeReductionStart</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionEnd</TD>
    <TD>NightTimeReductionEnd</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at night</TD>
    <TD>Tnight</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, presence will be used to define the temperature (if less      
                   people are at home, less rooms are heated and the average     
        temperature       will       decrease)</TD>
    <TD>VariableTemperatureProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature, when noone is at home (TRefSet = TMin + (TRef -   
                      TMin) * Presence(t))</TD>
    <TD>TMin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true: DHW consumption data repeats weekly</TD>
    <TD>WeeklyData</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Data File</TD>
    <TD>File</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Table Name</TD>
    <TD>Table</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>V_DHWperDay_l</TD>
    <TD>V_DHWperDay_l</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Factor, with which the DHW consumption gets multiplied</TD>
    <TD>DHWfactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CB turns on (SOCmin &lt; SOCmin and T<tstart)< 
      td=\"\">         </tstart)<>             
    <TD>SOCminCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CB turns off</TD>
    <TD>SOCmaxCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the CB turns on (T<tstart 
      td=\"\" and=\"\" socmin=\"\" <=\"\" socmin)<=\"\">         </tstart>             
    <TD>TStartCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the CB stops</TD>
    <TD>TStopCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CHP turns on (SOCmin &lt; SOCmin and T<tstart)< td=\"\"> 
                                </tstart)<>             
    <TD>SOCminCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CHP turns off</TD>
    <TD>SOCmaxCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the CHP turns on (T<tstart 
      td=\"\" and=\"\" socmin=\"\" <=\"\" socmin)<=\"\">         </tstart>             
    <TD>TStartCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the CHP stops</TD>
    <TD>TStopCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature difference between collector and storage         
                temperature</TD>
    <TD>deltaTonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-off-temperature difference between collector and storage        
                 temperature</TD>
    <TD>deltaToffST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature difference between flow and return, qv=qvMin</TD>
    <TD>deltaTFlowReturnLowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature difference between flow and return, qv=qvMax</TD>
    <TD>deltaTFlowReturnUpST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum volume flow of circulation pump</TD>
    <TD>qvMinST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum volume flow of circulation pump</TD>
    <TD>qvMaxST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature of DHW storage charging</TD>
    <TD>TonTS_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-off temperature of DHW storage charging</TD>
    <TD>ToffTS_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature, if the DHW temperature &lt; DHW consumption     
                    temperater + deltaTonDHW, the storage will be charged</TD>
    <TD>deltaTonDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum volume flow of circulation pump</TD>
    <TD>qvMaxDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>qvSTpump_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CHPin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>WolfCGB14 validated with CoSES mearusements</TD>
    <TD>wolfCGB14_GC1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Results:</TH>
    <TD>If true, standard control will be used to control the condensing       
                boiler</TD>
    <TD>StandardCBcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the CB modulation</TD>
    <TD>CBinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the CHP</TD>
    <TD>StandardCHPcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the CHP modulation</TD>
    <TD>CHPinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                     pump</TD>
    <TD>StandardSTcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on/off of solar thermal pump</TD>
    <TD>CPonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvRefST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal collector temperature</TD>
    <TD>TCollectorST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of solar thermal heat storage connection</TD>
    <TD>TStorageSTConnection</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature solar thermal</TD>
    <TD>TFlowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature solar thermal</TD>
    <TD>TReturnST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the DHW pump</TD>
    <TD>StandardDHWcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on/off of DHW pump</TD>
    <TD>CPonDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control flow rate of the DHW pump</TD>
    <TD>qvRefDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler flow temperature</TD>
    <TD>CB_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler return temperature</TD>
    <TD>CB_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow condensing boiler</TD>
    <TD>CB_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas flow condensing boiler</TD>
    <TD>CB_S_FG</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Demanded fuel volume</TD>
    <TD>CB_VFuel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of condensing boiler</TD>
    <TD>CB_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas power of condensing boiler</TD>
    <TD>CB_P_gas_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of condensing boiler</TD>
    <TD>CB_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas input of condensing boiler</TD>
    <TD>CB_E_gas_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating efficiency</TD>
    <TD>CB_Efficiency</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP flow temperature</TD>
    <TD>CHP_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP return temperature</TD>
    <TD>CHP_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow CHP</TD>
    <TD>CHP_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas flow CHP</TD>
    <TD>CHP_S_FG</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Demanded fuel volume</TD>
    <TD>CHP_VFuel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of CHP</TD>
    <TD>CHP_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric power output of CHP</TD>
    <TD>CHP_P_el_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas power of CHP</TD>
    <TD>CHP_P_gas_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of CHP</TD>
    <TD>CHP_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electricity output of CHP</TD>
    <TD>CHP_E_el_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas input of CHP</TD>
    <TD>CHP_E_gas_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal efficiency</TD>
    <TD>CHP_Efficiency_th</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric efficiency</TD>
    <TD>CHP_Efficiency_el</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Overall efficiency</TD>
    <TD>CHP_Efficiency_total</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal flow temperature</TD>
    <TD>ST_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal return temperature</TD>
    <TD>ST_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>ST_S_TM_Collector</TD>
    <TD>ST_S_TM_Collector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow solar thermal</TD>
    <TD>ST_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of solar thermal</TD>
    <TD>ST_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of solar thermal</TD>
    <TD>ST_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Power transfered to DHW storage</TD>
    <TD>TS_P_heat_toHWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature consumption side</TD>
    <TD>TS_S_TM_HC_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature consumption side</TD>
    <TD>TS_S_TM_HC_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature producer side</TD>
    <TD>TS_S_TM_PS_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature producer side</TD>
    <TD>TS_S_TM_PS_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature to DHW storage</TD>
    <TD>TS_S_TM_HC_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature to DHW storage</TD>
    <TD>TS_S_TM_HC_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 1</TD>
    <TD>TS_S_TM_HWS_1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 2</TD>
    <TD>TS_S_TM_HWS_2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 3</TD>
    <TD>TS_S_TM_HWS_3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 1</TD>
    <TD>TS_S_TM_BT_1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 2</TD>
    <TD>TS_S_TM_BT_2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 3</TD>
    <TD>TS_S_TM_BT_3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 4</TD>
    <TD>TS_S_TM_BT_4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 5</TD>
    <TD>TS_S_TM_BT_5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 6</TD>
    <TD>TS_S_TM_BT_6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 7</TD>
    <TD>TS_S_TM_BT_7</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 8</TD>
    <TD>TS_S_TM_BT_8</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 9</TD>
    <TD>TS_S_TM_BT_9</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 10</TD>
    <TD>TS_S_TM_BT_10</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to DHW storage</TD>
    <TD>TS_S_FW_HC_HW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy in thermal storage</TD>
    <TD>TS_E_Storage_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy in DHW storage</TD>
    <TD>TS_E_Storage_HWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Total energy transfered to DHW storage</TD>
    <TD>TS_E_heat_toHWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>State of charge of the thermal storage</TD>
    <TD>TS_SOC_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>State of charge of the DHW storage</TD>
    <TD>TS_SOC_HWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature before mixing unit</TD>
    <TD>HS_S_TM_VL_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature after mixing unit</TD>
    <TD>HS_S_TM_VL_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature</TD>
    <TD>HS_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink flow temperature hot water</TD>
    <TD>HS_S_TM_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature hot water</TD>
    <TD>HS_S_TM_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature in the house</TD>
    <TD>HS_S_TM_Room</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow after mixing unit</TD>
    <TD>HS_S_FW_HC_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow before mixing unit</TD>
    <TD>HS_S_FW_HC_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow domestic hot water consumption</TD>
    <TD>HS_S_FW_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating power</TD>
    <TD>HS_P_DemHeatHC_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water power</TD>
    <TD>HS_P_DemHeatHW_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating Energy</TD>
    <TD>HS_E_DemHeatHC_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water energy</TD>
    <TD>HS_E_DemHeatHW_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - set temperature (power is constant)</TD>
    <TD>CBIn_TSet</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                      supply and return is constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP input - electric power</TD>
    <TD>CHPIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to district heating heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                      negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of domestic hot water pump to charge the DHW     
                    storage</TD>
    <TD>qvDHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXin</TD>
    <TD>&nbsp;</TD></TR></TBODY></TABLE></DIV>
<H2>Description:</H2>
<P>&nbsp;</P></BODY></HTML>
"),
		experiment(
			StopTime=1,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.001,
			__esi_SolverOptions(
				solver="CVODE",
				typename="ExternalCVODEOptionData"),
			__esi_MinInterval="9.999999999999999e-10",
			__esi_AbsTolerance="1e-6"));
end SF4;
