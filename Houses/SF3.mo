﻿// CP: 65001
// SimulationX Version: 4.2.3.69223
within CoSES_ProHMo.Houses;
model SF3 "House 3 with ST and GSHP"
	GreenCity.Interfaces.Electrical.LV3Phase lV3Phase1 "Electrical Low-Voltage AC Three-Phase Connector" annotation(Placement(
		transformation(extent={{240,-500},{260,-480}}),
		iconTransformation(extent={{236.7,90},{256.7,110}})));
	GreenCity.Interfaces.Environment.EnvironmentConditions environmentConditions1 "Environment Conditions Connector" annotation(Placement(
		transformation(extent={{445,-105},{465,-85}}),
		iconTransformation(extent={{236.7,190},{256.7,210}})));
	GreenCity.Utilities.Thermal.Pump pump2(qvMax(displayUnit="l/min")=0.00025) "Volume flow generation and electrical power calculation" annotation(Placement(transformation(
		origin={75,-315},
		extent={{0,0},{10,10}},
		rotation=-180)));
	GreenCity.GreenBuilding.HeatingSystem.HeatingUnitFlowTemperature heatingUnitFlowTemperature1(qvMaxPump(displayUnit="l/min")=0.00025) annotation(Placement(transformation(extent={{255,-230},{290,-195}})));
	Consumer.DHW_demand dHW_demand1(
		WeeklyData=WeeklyData,
		File=File,
		Table=Table,
		DHWfactor=DHWfactor,
		THotWaterSet=323.15) annotation(Placement(transformation(extent={{375,-270},{395,-250}})));
	GreenCity.Utilities.Electrical.Grid grid1(
		OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
		useA=true,
		useB=true,
		useC=true,
		useD=true,
		useE=true,
		useF=true) annotation(Placement(transformation(extent={{155,-395},{195,-355}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap1 annotation(Placement(transformation(extent={{235,-380},{245,-370}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap5 annotation(Placement(transformation(extent={{200,-350},{210,-340}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap6 annotation(Placement(transformation(extent={{205,-365},{215,-355}})));
	GreenCity.Local.SolarThermal WolfCRK12(
		CPC=CPC,
		alphaModule(displayUnit="rad")=alphaModule,
		betaModule(displayUnit="rad")=betaModule,
		nSeries=nSeries,
		nParallel=nParallel,
		AModule=AModule,
		VAbsorber(displayUnit="m³")=VAbsorber,
		etaOptical=0.642,
		a1=0.885,
		a2=0.001,
		CCollector=8416) annotation(Placement(transformation(extent={{-35,-350},{5,-310}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow defineVolumeFlow1 annotation(Placement(transformation(
		origin={210,-115},
		extent={{-10,-10},{10,10}},
		rotation=-90)));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow extractVolumeFlow1 annotation(Placement(transformation(
		origin={180,-115},
		extent={{10,-10},{-10,10}},
		rotation=-90)));
	GreenCity.Utilities.Thermal.MeasureThermal measureThermal1 annotation(Placement(transformation(extent={{295,-250},{305,-260}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap7 annotation(Placement(transformation(extent={{140,-380},{130,-370}})));
	Storage.HeatStorageCombined5Inlets WolfBSP_W_SL_1000(
		VStorage=0.9,
		dStorage=0.79,
		QlossRate=7,
		LinearProfile=TSLinearProfile,
		TupInit(displayUnit="K")=TSTupInit,
		TlowInit(displayUnit="K")=TSTlowInit,
		TLayerVector(displayUnit="K")=TSTLayerVector,
		TMax(displayUnit="K")=Tmax_TS,
		alphaMedStatic=1,
		use1=true,
		iFlow1=8,
		iReturn1=2,
		use2=true,
		iFlow2=7,
		iReturn2=6,
		use3=true,
		iFlow3=4,
		iReturn3=2,
		use4=true,
		HE4=true,
		iFlow4=9,
		iReturn4=7,
		AHeatExchanger4=1.9,
		VHeatExchanger4=0.011,
		use5=true,
		HE5=true,
		iFlow5=5,
		AHeatExchanger5=3,
		VHeatExchanger5=0.0198,
		use6=true,
		iFlow6=8,
		iReturn6=2,
		use7=true,
		iFlow7=7,
		use8=true) annotation(Placement(transformation(extent={{165,-253},{200,-180}})));
	Distribution.HydraulicSwitch_TS_DH hydraulicSwitch_TS_DH1 annotation(Placement(transformation(extent={{150,-160},{220,-145}})));
	Distribution.SwitchPort switchPort1 annotation(Placement(transformation(extent={{100,-345},{120,-315}})));
	Distribution.SwitchPort switchPort2 annotation(Placement(transformation(extent={{100,-225},{120,-195}})));
	Generator.DigitalTwins.WolfBWS1_10_GC wolfBWS1_10(
		horizontalCollector=horizontalCollector,
		numberHE=numberHE,
		HoleDepth=HoleDepth,
		rHole=rHole,
		groundCollector1(horizontalCollector=false)) annotation(Placement(transformation(extent={{20,-230},{65,-185}})));
	parameter Real TStartHPhigh(quantity="Basics.Temp")=328.15 "Minimum Temperature, at the top, at which the heat pump turns on" annotation(Dialog(
		group="Heat Pump",
		tab="Standard Control"));
	parameter Real TStopHPhigh(quantity="Basics.Temp")=333.15 "Temperature at the top, at which the heat pump stops" annotation(Dialog(
		group="Heat Pump",
		tab="Standard Control"));
	parameter Real TStartHPlow(quantity="Basics.Temp")=316.15 "Minimum Temperature, at the bottom, at which the heat pump turns on" annotation(Dialog(
		group="Heat Pump",
		tab="Standard Control"));
	parameter Real TStopHPlow(quantity="Basics.Temp")=318.15 "Temperature at the bottom, at which the heat pump stops" annotation(Dialog(
		group="Heat Pump",
		tab="Standard Control"));
	parameter Real TStartAUXhigh(quantity="Basics.Temp")=323.15 "Minimum Temperature, at the top, at which the auxiliary heater turns on" annotation(Dialog(
		group="Heat Pump",
		tab="Standard Control"));
	parameter Real TStopAUXhigh(quantity="Basics.Temp")=328.15 "Minimum Temperature, at the bottom, at which the auxiliary heater turns on" annotation(Dialog(
		group="Heat Pump",
		tab="Standard Control"));
	parameter Real TStartAUXlow(quantity="Basics.Temp")=308.15 "Minimum Temperature, at the top, at which the auxiliary heater stops" annotation(Dialog(
		group="Heat Pump",
		tab="Standard Control"));
	parameter Real TStopAUXlow(quantity="Basics.Temp")=313.15 "Minimum Temperature, at the bottom, at which the auxiliary heater stops" annotation(Dialog(
		group="Heat Pump",
		tab="Standard Control"));
	protected
		Boolean HPon "Heat pump is switched on" annotation(
			HideResult=false,
			Dialog(
				group="Heat Pump",
				tab="Standard Control"));
		Boolean AUXon "Auxiliary heater is switched on" annotation(
			HideResult=false,
			Dialog(
				group="Heat Pump",
				tab="Standard Control"));
	public
		Boolean HPinStandardControl(quantity="Basics.Real") "Standard control value of the heat pump modulation" annotation(Dialog(
			group="Heat Pump",
			tab="Standard Control",
			visible=false));
		Real AUXinStandardControl(quantity="Basics.RelMagnitude") "Standard control value of the auxiliary heater modulation" annotation(Dialog(
			group="Heat Pump",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardHPcontrol "If true, standard control will be used to control the heat pump" annotation(
			Placement(
				transformation(
					origin={-125,-100},
					extent={{-20,-20},{20,20}}),
				iconTransformation(
					origin={-50,250},
					extent={{-20,-20},{20,20}},
					rotation=270)),
			Dialog(
				group="Heat Pump",
				tab="Standard Control",
				visible=false));
		Modelica.Blocks.Sources.RealExpression TColdWater(y(quantity="Basics.Temp")=293.15) annotation(Placement(transformation(extent={{435,-295},{415,-275}})));
		Consumer.SimpleHeatedBuilding simpleHeatedBuilding1(
			redeclare replaceable parameter GreenCity.Utilities.BuildingData.ParameterSelectionSCB.AgeOfBuilding.EnEV2016 buildingAge,
			nFloors=nFloors,
			nAp=nApartments,
			nPeople=nPeople,
			ALH=ALH,
			outline=true,
			livingTZoneInit(displayUnit="K")=TLiving_Init,
			UseStandardHeatNorm=UseStandardHeatNorm,
			QHeatNormLivingArea=QHeatNormLivingArea,
			n=n,
			TFlowHeatNorm(displayUnit="K")=TFlowHeatNorm,
			TReturnHeatNorm(displayUnit="K")=TReturnHeatNorm,
			TRef(displayUnit="K")=TRef,
			qvMaxLivingZone(displayUnit="m³/s")=qvMaxLivingZone,
			roofTZoneInit(displayUnit="K")=TRoof_Init,
			cellarTZoneInit(displayUnit="K")=TCellar_Init,
			UseIndividualPresence=UseIndividualPresence,
			PresenceFile=PresenceFile,
			Presence_WeeklyRepetition=true,
			UseIndividualElecConsumption=UseIndividualElecConsumption,
			ElConsumptionFile=ElConsumptionFile,
			ElConsumptionTable=ElConsumptionTable,
			ElConsumptionFactor=ElFactor,
			ElConsumption_YearlyRepetition=true,
			UseReferenceInputSignal=true,
			ActivateNightTimeReduction=false,
			Tnight(displayUnit="K")=Tnight,
			NightTimeReductionStart(displayUnit="s")=NightTimeReductionStart,
			NightTimeReductionEnd(displayUnit="s")=NightTimeReductionEnd,
			VariableTemperatureProfile=false,
			TMin(displayUnit="K")=TMin) "Heated 3-zone-building with changeable heating system" annotation(Placement(transformation(extent={{340,-235},{380,-195}})));
		Modelica.Blocks.Logical.Switch switch2 annotation(Placement(transformation(extent={{-90,-315},{-70,-295}})));
		Modelica.Blocks.Sources.RealExpression qvSTpump_StandardControl(y=qvRefST) annotation(Placement(transformation(extent={{-125,-295},{-105,-275}})));
		Modelica.Blocks.Logical.LogicalSwitch logicalSwitch1 annotation(Placement(transformation(extent={{-90,-245},{-70,-225}})));
		Modelica.Blocks.Sources.BooleanExpression SwitchSignal_StandardControl(y=SwitchSTTSPort_StandardControl) "Set output signal to a time varying Boolean expression" annotation(Placement(transformation(extent={{-125,-220},{-105,-200}})));
		Modelica.Blocks.Logical.LogicalSwitch logicalSwitch2 annotation(Placement(transformation(extent={{-90,-35},{-70,-15}})));
		Modelica.Blocks.Sources.BooleanExpression SwitchSignal_StandardControl1(y=SwitchHPTSPort_StandardControl) "Set output signal to a time varying Boolean expression" annotation(Placement(transformation(extent={{-125,-10},{-105,10}})));
		Modelica.Blocks.Logical.Switch switch5 annotation(Placement(transformation(extent={{-90,-175},{-70,-155}})));
		Modelica.Blocks.Sources.BooleanExpression booleanExpression3(y=StandardHPcontrol) annotation(Placement(transformation(extent={{-125,-175},{-105,-155}})));
		Modelica.Blocks.Sources.RealExpression AUXin_StandardControl(y=AUXinStandardControl) annotation(Placement(transformation(extent={{-125,-160},{-105,-140}})));
		Modelica.Blocks.Logical.LogicalSwitch logicalSwitch3 annotation(Placement(transformation(extent={{-90,-110},{-70,-90}})));
		Modelica.Blocks.Sources.BooleanExpression SwitchSignal_StandardControl2(y=HPinStandardControl) "Set output signal to a time varying Boolean expression" annotation(Placement(transformation(extent={{-125,-85},{-105,-65}})));
		GreenCity.Local.Photovoltaic photovoltaic1(
			alphaModule(displayUnit="rad")=alphaPV,
			betaModule(displayUnit="rad")=betaPV,
			PPeak(displayUnit="Nm/s")=PVPeak) annotation(Placement(transformation(extent={{395,-440},{355,-400}})));
		GreenCity.Local.Controller.PV2ACInverter pV2ACInverter1 annotation(Placement(transformation(extent={{345,-400},{305,-440}})));
		GreenCity.Utilities.Electrical.Grid grid2(
			OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
			useA=true,
			useB=true,
			useC=true,
			useD=true,
			useE=true,
			useF=true) annotation(Placement(transformation(extent={{230,-455},{270,-415}})));
		Modelica.Blocks.Interfaces.BooleanInput StandardTRefControl "If true, standard control for reference temperature is active" annotation(Placement(
			transformation(extent={{430,-270},{470,-230}}),
			iconTransformation(
				origin={-200,250},
				extent={{-20,-20},{20,20}},
				rotation=-90)));
		parameter Real deltaTLowDHW(quantity="Thermics.TempDiff")=2 "Required temperature difference between upper layer and DHW set temperature" annotation(Dialog(
			group="Heat Pump 3-way valve",
			tab="Standard Control"));
		Boolean SwitchHPTSPort_StandardControl "Switches between the inlet of the HP into the TS (true: upper storage connection)" annotation(Dialog(
			group="Heat Pump 3-way valve",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardControlHPSwitch "Standard Control for the 3-way valve to switch the connection to the thermal storage" annotation(
			Placement(
				transformation(extent={{-145,-45},{-105,-5}}),
				iconTransformation(
					origin={-150,250},
					extent={{-20,20},{20,-20}},
					rotation=-90)),
			Dialog(
				group="Heat Pump 3-way valve",
				tab="Standard Control",
				visible=false));
		parameter Real deltaTonST(quantity="Thermics.TempDiff")=3 "Switch-on temperature difference between collector and storage temperature" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control"));
		parameter Real deltaToffST(quantity="Thermics.TempDiff")=2 "Switch-off-temperature difference between collector and storage temperature" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control"));
		parameter Real deltaTFlowReturnLowST(quantity="Thermics.TempDiff")=3 "Minimum temperature difference between flow and return, qv=qvMin" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control"));
		parameter Real deltaTFlowReturnUpST(quantity="Thermics.TempDiff")=7 "Maximum temperature difference between flow and return, qv=qvMax" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control"));
		parameter Real qvMinST(
			quantity="Thermics.VolumeFlow",
			displayUnit="m³/h")=0 "Minimum volume flow of circulation pump" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control"));
		parameter Real qvMaxST(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min")=0.0003333333333333334 "Maximum volume flow of circulation pump" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control"));
		Boolean CPonST "Switch-on/off of solar thermal pump" annotation(Dialog(
			group="Solar Thermal",
			__esi_groupCollapsed=true,
			tab="Standard Control",
			visible=false));
		Real qvRefST(quantity="Thermics.VolumeFlow") "Reference volume flow of solar thermal pump" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control",
			visible=false));
		Real TCollectorST(quantity="Basics.Temp") "Solar thermal collector temperature" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control",
			visible=false));
		Real TStorageSTConnection(quantity="Basics.Temp") "Temperature of solar thermal heat storage connection" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control",
			visible=false));
		Real TFlowST(quantity="Basics.Temp") "Flow temperature solar thermal" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control",
			visible=false));
		Real TReturnST(quantity="Basics.Temp") "Return temperature solar thermal" annotation(Dialog(
			group="Solar Thermal",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardSTcontrol "If true, standard control will be used to control the solar thermal pump" annotation(
			Placement(
				transformation(
					origin={-125,-305},
					extent={{20,20},{-20,-20}},
					rotation=180),
				iconTransformation(
					origin={150,250},
					extent={{-20,-20},{20,20}},
					rotation=270)),
			Dialog(
				group="Solar Thermal",
				tab="Standard Control",
				visible=false));
		Boolean SwitchSTTSPort_StandardControl "Switches between the inlet of the HP into the TS (true: upper storage connection)" annotation(Dialog(
			group="Solar Thermal 3-way valve",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardControlSTSwitch "Standard Control for the 3-way valve to switch the connection to the thermal storage" annotation(
			Placement(
				transformation(extent={{-145,-255},{-105,-215}}),
				iconTransformation(
					origin={50,250},
					extent={{-20,20},{20,-20}},
					rotation=-90)),
			Dialog(
				group="Solar Thermal 3-way valve",
				tab="Standard Control",
				visible=false));
		parameter Boolean CPC=true "If true, solar thermal collector is CPC collector, else, solar thermal collector is a flat plate collector" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Real alphaModule(
			quantity="Geometry.Angle",
			displayUnit="°")=0.6108652381980153 "Inclination angle of solar thermal collector" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Real betaModule(
			quantity="Geometry.Angle",
			displayUnit="°")=3.1415926535897931 "Orientation angle of solar thermal collector" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Integer nSeries=1 "Number of solar thermal collectors in series" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Integer nParallel=1 "Number of solar thermal collectors in parallel" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Real AModule(
			quantity="Geometry.Area",
			displayUnit="m²")=1.312 "Effective surface area of solar thermal collector" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Real VAbsorber(quantity="Geometry.Volume")=1.7 "Absorber volume" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Boolean horizontalCollector=true "if true: horizontal collector, else downhole heat exchanger" annotation(Dialog(
			group="Heat Pump",
			tab="Heat Generator Parameters"));
		parameter Integer numberHE=1 "Number of independent heat exchangers" annotation(Dialog(
			group="Heat Pump",
			tab="Heat Generator Parameters"));
		parameter Real HoleDepth(
			quantity="Basics.Length",
			displayUnit="m")=6 "Depth of hole, for horizontal collectors: laying depth of heat collectors" annotation(Dialog(
			group="Heat Pump",
			tab="Heat Generator Parameters"));
		parameter Real rHole(
			quantity="Basics.Length",
			displayUnit="m")=0.3 "Radius of hole, for horizontal collectors: equivalent radius of the flat collector field" annotation(Dialog(
			group="Heat Pump",
			tab="Heat Generator Parameters"));
	protected
		parameter Real PmaxAUX(quantity="Basics.Power")=6000 "Maximum power of auxiliary heater" annotation(Dialog(
			group="Heat Pump",
			tab="Heat Generator Parameters"));
	public
		parameter Real Tmax_TS(quantity="Basics.Temp")=363.15 "Maximum temperature within the thermal storage" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real T0_TS(quantity="Basics.Temp")=303.15 "Temperature level to calculate the stored energy (e.g. return temperature of consumption)" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Boolean TSLinearProfile=true "If true, the temperature profile at simulation begin within the storage is linear, if false the profile is defined by a temperature vector" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTupInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=338.15 if TSLinearProfile "Temperature of upmost heat storage layer at simulation begin" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTlowInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=313.14999999999998 if TSLinearProfile "Temperature of lowmost heat storage layer at simulation begin" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTLayerVector[20](quantity="Basics.Temp")={313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15} if not TSLinearProfile "Vector of temperature profile of the layers at simulation begin, element 1 is at lowest layer" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Integer nPeople(quantity="Basics.Unitless")=8 "Number of people living in the building" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Integer nFloors=2 "nFloors" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Integer nApartments=3 "nApartments" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Real ALH(
			quantity="Geometry.Area",
			displayUnit="m²")=400 "Heated (living) area (e.g. 50m² per person)" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Boolean UseStandardHeatNorm=false "If true, use standard area-specific heating power, else define it manually" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real QHeatNormLivingArea(quantity="Thermics.HeatFlowSurf")=13 if not UseStandardHeatNorm "Area-specific heating power - modern radiators: 14 - 15 W/m²; space heating: 15 W/m²" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real n=1.3 "Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TFlowHeatNorm(quantity="Basics.Temp")=313.15 "Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 - 45°C" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TReturnHeatNorm(quantity="Basics.Temp")=303.15 "Normal return temperature - radiator: 45 - 65°C; floor heating: 28 - 35°C" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TRef(quantity="Basics.Temp")=294.15 "Reference indoor temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real qvMaxLivingZone(quantity="Thermics.VolumeFlow")=0.00025 "Maximumg flow rate in Living Zone" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TLiving_Init(
			quantity="Basics.Temp",
			displayUnit="°C")=294.15 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TRoof_Init(quantity="Basics.Temp")=274.65 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TCellar_Init(quantity="Basics.Temp")=280.15 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Boolean UseUnixTimeIndividual=false "If true, use unix time for individual presence and electric consumption" annotation(
			HideResult=false,
			Dialog(
				group="Additional Yields/Losses",
				tab="Consumption Parameters",
				visible=false));
		parameter Boolean UseIndividualPresence=true "If the presence is used, individual presence data has to be provided, else standart presence is used" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String PresenceFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF3_WholeYear.txt" if UseIndividualPresence "File with presence timeseries (presence in %; 0% - no one is at home; 100% - everyone is at home)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean UseIndividualElecConsumption=true "If individual electricity consumption is used, individual consumption data ha to be provided, else standart load profiles are used" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String ElConsumptionFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF3_WholeYear.txt" if UseIndividualElecConsumption "File with electric consumption time series (consumption in kW)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String ElConsumptionTable="Pel" if UseIndividualElecConsumption "Table with electric consumption time series (consumption in W)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Real YearlyElecConsumption_kWh=9500 if UseIndividualElecConsumption "YearlyElecConsumption_kWh" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Real ElFactor=YearlyElecConsumption_kWh/6041 if UseIndividualElecConsumption "ElFactor" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean ActivateNightTimeReduction=false "If true, night time reduction is activated, else temperature is constant" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real NightTimeReductionStart(
			quantity="Basics.Time",
			displayUnit="h")=82800 if ActivateNightTimeReduction "NightTimeReductionStart" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real NightTimeReductionEnd(
			quantity="Basics.Time",
			displayUnit="h")=25200 if ActivateNightTimeReduction "NightTimeReductionEnd" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real Tnight(quantity="Basics.Temp")=291.15 if ActivateNightTimeReduction "Temperature at night" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Boolean VariableTemperatureProfile=false "If true, presence will be used to define the temperature (if less people are at home, less rooms are heated and the average temperature will decrease)" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real TMin(quantity="Basics.Temp")=292.15 if VariableTemperatureProfile "Minimum temperature, when noone is at home (TRefSet = TMin + (TRef - TMin) * Presence(t))" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Boolean UseUnixTimeDHW=false "If true, use unix time for DHW consumption" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Boolean WeeklyData=true "If true: DHW consumption data repeats weekly" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter String File=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF3_WholeYear.txt" "DHW Data File" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter String Table="V_DHW" "DHW Table Name" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real V_DHWperDay_l=400 "V_DHWperDay_l" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real DHWfactor=V_DHWperDay_l/400 "Factor, with which the DHW consumption gets multiplied" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real alphaPV(
			quantity="Geometry.Angle",
			displayUnit="°")=0.6108652381980153 "Inclination angle of the PV system" annotation(Dialog(
			group="Parameters",
			tab="PV System"));
		parameter Real betaPV(
			quantity="Geometry.Angle",
			displayUnit="°")=3.141592653589793 "Orientation angle of the PV system" annotation(Dialog(
			group="Parameters",
			tab="PV System"));
		parameter Real PVPeak(quantity="Basics.Power")=10000 "Installed peak power of the PV system" annotation(Dialog(
			group="Parameters",
			tab="PV System"));
		Real PV_P(quantity="Basics.Power") "PV power" annotation(Dialog(
			group="Results",
			tab="PV System",
			visible=false));
		Real PV_E(
			quantity="Basics.Energy",
			displayUnit="kWh") "Produced energy of the PV system" annotation(Dialog(
			group="Results",
			tab="PV System",
			visible=false));
		Real HP_Signal(quantity="Basics.RelMagnitude") "Input Signal of Heat Pump" annotation(
			HideResult=false,
			Dialog(tab="Results HP"));
		Real HP_S_TM_VL(quantity="Basics.Temp") "Heat pump flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HP",
			visible=false));
		Real HP_S_TM_RL(quantity="Basics.Temp") "Heat pump return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HP",
			visible=false));
		Real HP_S_TM_BC_VL(quantity="Basics.Temp") "Heat pump brine flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HP",
			visible=false));
		Real HP_S_TM_BC_RL(quantity="Basics.Temp") "Heat pump brine return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HP",
			visible=false));
		Real HP_S_FW_HC(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow heat pump " annotation(Dialog(
			group="Volume Flow",
			tab="Results HP",
			visible=false));
		Real HP_S_FW_BC(quantity="Thermics.VolumeFlow") "Brine volume flow heat pump " annotation(Dialog(
			group="Volume Flow",
			tab="Results HP",
			visible=false));
		Real HP_P_heat_is(quantity="Basics.Power") "Heat output power of heat pump " annotation(Dialog(
			group="Power",
			tab="Results HP",
			visible=false));
		Real HP_P_elec_is(quantity="Basics.Power") "Electricity demand of heat pump" annotation(Dialog(
			group="Power",
			tab="Results HP",
			visible=false));
		Real HP_E_heat_produced(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of heat pump" annotation(Dialog(
			group="Energy",
			tab="Results HP",
			visible=false));
		Real HP_E_elec_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Consumed electric energy of heat pump" annotation(Dialog(
			group="Energy",
			tab="Results HP",
			visible=false));
		Real HP_COP(quantity="Basics.Unitless") "Coefficiency of performance" annotation(Dialog(
			group="Efficiency",
			tab="Results HP",
			visible=false));
		Real ST_S_TM_VL(quantity="Basics.Temp") "Solar thermal flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results ST",
			visible=false));
		Real ST_S_TM_RL(quantity="Basics.Temp") "Solar thermal return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results ST",
			visible=false));
		Real ST_S_TM_Collector(quantity="Basics.Temp") "ST_S_TM_Collector" annotation(Dialog(
			group="Temperatures",
			tab="Results ST",
			visible=false));
		Real ST_S_FW_HC(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow solar thermal" annotation(Dialog(
			group="Volume Flow",
			tab="Results ST",
			visible=false));
		Real ST_P_heat_is(quantity="Basics.Power") "Heat output power of solar thermal" annotation(Dialog(
			group="Power",
			tab="Results ST",
			visible=false));
		Real ST_E_heat_produced(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of solar thermal" annotation(Dialog(
			group="Energy",
			tab="Results ST",
			visible=false));
		Real TS_P_heat_toFWS(quantity="Basics.Power") "Power transfered to fresh water station" annotation(Dialog(
			group="Power",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_VL(quantity="Basics.Temp") "Flow temperature consumption side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_RL(quantity="Basics.Temp") "Return temperature consumption side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_PS_VL(quantity="Basics.Temp") "Flow temperature producer side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_PS_RL(quantity="Basics.Temp") "Return temperature producer side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_HW_VL(quantity="Basics.Temp") "Flow temperature to fresh water station" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_HW_RL(quantity="Basics.Temp") "Return temperature to fresh water station" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_1(quantity="Basics.Temp") "Thermal storage temperature 1" annotation(Dialog(
			group="Storage Temperatures",
			__esi_groupCollapsed=true,
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_2(quantity="Basics.Temp") "Thermal storage temperature 2" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_3(quantity="Basics.Temp") "Thermal storage temperature 3" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_4(quantity="Basics.Temp") "Thermal storage temperature 4" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_5(quantity="Basics.Temp") "Thermal storage temperature 5" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_6(quantity="Basics.Temp") "Thermal storage temperature 6" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_7(quantity="Basics.Temp") "Thermal storage temperature 7" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_8(quantity="Basics.Temp") "Thermal storage temperature 8" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_9(quantity="Basics.Temp") "Thermal storage temperature 9" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_10(quantity="Basics.Temp") "Thermal storage temperature 10" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_FW_HC_HW(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow to fresh water station" annotation(Dialog(
			group="Volume Flow",
			tab="Results TS",
			visible=false));
		Real TS_E_Storage_BT(
			quantity="Basics.Energy",
			displayUnit="kWh") "Energy in thermal storage" annotation(Dialog(
			group="Energy",
			tab="Results TS",
			visible=false));
		Real TS_E_heat_toFWS(
			quantity="Basics.Energy",
			displayUnit="kWh") "Total energy transfered to fresh water station" annotation(Dialog(
			group="Energy",
			tab="Results TS",
			visible=false));
		Real TS_SOC_BT(quantity="Basics.RelMagnitude") "State of charge of the thermal storage" annotation(Dialog(
			group="State of charge",
			tab="Results TS",
			visible=false));
		Real TRefSet(quantity="Basics.Temp") "Reference room temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_VL_bM(quantity="Basics.Temp") "Heat Sink flow temperature before mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_VL_aM(quantity="Basics.Temp") "Heat Sink flow temperature after mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_RL(quantity="Basics.Temp") "Heat sink return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_HW_VL(quantity="Basics.Temp") "Heat sink flow temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_HW_RL(quantity="Basics.Temp") "Heat sink return temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_Room(quantity="Basics.Temp") "Temperature in the house" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_FW_HC_aM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow after mixing unit" annotation(Dialog(
			group="Volume Flow",
			tab="Results HS",
			visible=false));
		Real HS_S_FW_HC_bM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow before mixing unit" annotation(Dialog(
			group="Volume Flow",
			tab="Results HS",
			visible=false));
		Real HS_S_FW_HW_VL(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow domestic hot water consumption" annotation(Dialog(
			group="Volume Flow",
			tab="Results HS",
			visible=false));
		Real HS_P_DemHeatHC_is(quantity="Basics.Power") "Heating power" annotation(Dialog(
			group="Power",
			tab="Results HS",
			visible=false));
		Real HS_P_DemHeatHW_is(quantity="Basics.Power") "Domestic hot water power" annotation(Dialog(
			group="Power",
			tab="Results HS",
			visible=false));
		Real HS_E_DemHeatHC_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heating Energy" annotation(Dialog(
			group="Energy",
			tab="Results HS",
			visible=false));
		Real HS_E_DemHeatHW_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Domestic hot water energy" annotation(Dialog(
			group="Energy",
			tab="Results HS",
			visible=false));
		Modelica.Blocks.Interfaces.RealInput UnixTime "UnixTime" annotation(
			Placement(
				transformation(
					origin={460,-130},
					extent={{-20,-20},{20,20}},
					rotation=180),
				iconTransformation(
					origin={250,150},
					extent={{-20,-20},{20,20}},
					rotation=180)),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput T_ref_in_heat(quantity="Basics.Temp") "Reference input temperature for heating" annotation(
			Placement(
				transformation(extent={{480,-235},{440,-195}}),
				iconTransformation(extent={{-270,-220},{-230,-180}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput TDH_HEXout(quantity="Basics.Temp") "Outlet temperature of district heating heat exchanger" annotation(
			Placement(
				transformation(extent={{-145,70},{-105,110}}),
				iconTransformation(extent={{266.7,-170},{226.7,-130}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput DTH_HEXin(quantity="Basics.Temp") "Inlet temperature of district heating heat exchanger" annotation(
			Placement(
				transformation(extent={{-125,35},{-105,55}}),
				iconTransformation(extent={{236.7,-60},{256.7,-40}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput qv_HEX(quantity="Thermics.VolumeFlow") "Volume flow to heat exchanger" annotation(
			Placement(
				transformation(extent={{-125,55},{-105,75}}),
				iconTransformation(
					origin={246.7,0},
					extent={{10,-10},{-10,10}},
					rotation=-180)),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput qvSTpump(quantity="Thermics.VolumeFlow") "Reference volume flow of solar thermal pump" annotation(
			Placement(
				transformation(extent={{-145,-355},{-105,-315}}),
				iconTransformation(extent={{-270,130},{-230,170}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput qvDHpump(quantity="Thermics.VolumeFlow") "Reference volume flow of district heating pump - positive: feed in - negative: extraction" annotation(
			Placement(
				transformation(extent={{-145,0},{-105,40}}),
				iconTransformation(extent={{266.7,-220},{226.7,-180}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput SwitchHPTSPort "Switches between the inlet of the HP into the TS (true: upper storage connection)" annotation(
			Placement(
				transformation(extent={{-145,-75},{-105,-35}}),
				iconTransformation(extent={{-270,-120},{-230,-80}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput HPAUXIn(quantity="Basics.Power") "Air source heat pump input - heat power (temperature difference between supply and return is constant)" annotation(
			Placement(
				transformation(extent={{-145,-205},{-105,-165}}),
				iconTransformation(extent={{-270,-70},{-230,-30}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput SwitchSTTSPort "Switches between the inlet of the HP into the TS (true: upper storage connection)" annotation(
			Placement(
				transformation(extent={{-145,-285},{-105,-245}}),
				iconTransformation(extent={{-270,180},{-230,220}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput HPOn "Turns the heat pump on" annotation(
			Placement(
				transformation(extent={{-145,-150},{-105,-110}}),
				iconTransformation(extent={{-270,-20},{-230,20}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Sources.RealExpression T_ref_set(y(
			quantity="Basics.Temp",
			displayUnit="K")=TRefSet) annotation(Placement(transformation(extent={{425,-225},{405,-205}})));
	initial equation
		// enter your equations here
		// Solar thermal controller
		if (((TCollectorST - TStorageSTConnection) > deltaTonST) and StandardSTcontrol) then
			CPonST = true;
		else	
			CPonST = false;
		end if;
		
		// Solar Thermal 3way valve
		if (((TCollectorST - TS_S_TM_BT_9) > deltaTonST) and StandardControlSTSwitch) then 
			SwitchSTTSPort_StandardControl = true;
		else
			SwitchSTTSPort_StandardControl = false;
		end if;
		
		// Heat pump 3way valve
		if (TS_S_TM_BT_8 < TStartHPhigh) and StandardControlHPSwitch then 
			SwitchHPTSPort_StandardControl = true;
		else
			SwitchHPTSPort_StandardControl = false;
		end if;
		
		// Heat pump
		if ((SwitchHPTSPort_StandardControl or (TS_S_TM_BT_3 < TStartHPlow)) and StandardHPcontrol) then
			HPon = true;
		else
			HPon = false;
		end if;
		if (HPon and ((TS_S_TM_BT_8 < TStartAUXhigh) or (TS_S_TM_BT_3 < TStartAUXlow))) then
			AUXon = true;
		else
			AUXon = false;
		end if;
		
		// Temperature setpoint
		if not StandardTRefControl then
			TRefSet = T_ref_in_heat;
		elseif ActivateNightTimeReduction then
			if (((environmentConditions1.HourOfDay * 3600) < NightTimeReductionStart) and ((environmentConditions1.HourOfDay * 3600) > NightTimeReductionEnd)) then
				if VariableTemperatureProfile then
					TRefSet = TMin + (TRef - TMin) * simpleHeatedBuilding1.presenceTable.y[1];
				else
					TRefSet = TRef;
				end if;
			else
				TRefSet = Tnight;
			end if;
		else
			if VariableTemperatureProfile then
				TRefSet = TMin + (TRef - TMin) * simpleHeatedBuilding1.presenceTable.y[1];
			else
				TRefSet = TRef;
			end if;
		end if;
	equation
		// Results
		// Heat Pump
		HP_S_TM_VL = wolfBWS1_10.TFlow;
		HP_S_TM_RL = wolfBWS1_10.TReturn;
		HP_S_FW_HC = wolfBWS1_10.wolfBWS1.VWater;
		HP_P_heat_is = wolfBWS1_10.QHeat_HP + wolfBWS1_10.QHeat_Aux;
		HP_P_elec_is = wolfBWS1_10.PEl_HP + wolfBWS1_10.PEl_Aux;
		HP_S_TM_BC_VL = wolfBWS1_10.TSourceIn;
		HP_S_TM_BC_RL = wolfBWS1_10.TSouceOut;
		HP_S_FW_BC = wolfBWS1_10.wolfBWS1.VSource;
		HP_E_heat_produced = wolfBWS1_10.EHeat_HP + wolfBWS1_10.EHeat_Aux;
		HP_E_elec_consumed = wolfBWS1_10.EEl_HP + wolfBWS1_10.EEl_Aux;
		HP_COP = wolfBWS1_10.COP;
		
		// Solar Thermal Collectors
		ST_S_TM_VL = WolfCRK12.FlowST.T;
		ST_S_TM_RL = WolfCRK12.ReturnST.T;
		ST_S_TM_Collector = WolfCRK12.TCollector;
		ST_S_FW_HC = WolfCRK12.FlowST.qv;
		ST_E_heat_produced = WolfCRK12.EST;
		ST_P_heat_is = WolfCRK12.QSolarThermal;
		
		// Hot Water Storage
		TS_P_heat_toFWS = WolfBSP_W_SL_1000.PStorage[8];
		TS_S_FW_HC_HW = dHW_demand1.qv_Set;
		TS_E_heat_toFWS = WolfBSP_W_SL_1000.EHeat[8];
		
		//Thermal Storage
		TS_S_TM_HC_VL = WolfBSP_W_SL_1000.FlowOut7.T;
		TS_S_TM_HC_RL = WolfBSP_W_SL_1000.ReturnIn7.T;
		TS_S_TM_PS_VL = switchPort2.In.T;
		TS_S_TM_PS_RL = switchPort2.Out.T;
		TS_S_TM_HC_HW_VL = dHW_demand1.FlowHotWater.T;
		TS_S_TM_HC_HW_RL = dHW_demand1.ReturnHotWater.T;
		TS_S_TM_BT_1 = WolfBSP_W_SL_1000.TStorage[1];
		TS_S_TM_BT_2 = WolfBSP_W_SL_1000.TStorage[2];
		TS_S_TM_BT_3 = WolfBSP_W_SL_1000.TStorage[3];
		TS_S_TM_BT_4 = WolfBSP_W_SL_1000.TStorage[4];
		TS_S_TM_BT_5 = WolfBSP_W_SL_1000.TStorage[5];
		TS_S_TM_BT_6 = WolfBSP_W_SL_1000.TStorage[6];
		TS_S_TM_BT_7 = WolfBSP_W_SL_1000.TStorage[7];
		TS_S_TM_BT_8 = WolfBSP_W_SL_1000.TStorage[8];
		TS_S_TM_BT_9 = WolfBSP_W_SL_1000.TStorage[9];
		TS_S_TM_BT_10 = WolfBSP_W_SL_1000.TStorage[10];
		TS_E_Storage_BT = WolfBSP_W_SL_1000.cpMed*WolfBSP_W_SL_1000.rhoMed*WolfBSP_W_SL_1000.VStorage*((WolfBSP_W_SL_1000.TLayer[1]+WolfBSP_W_SL_1000.TLayer[2]+WolfBSP_W_SL_1000.TLayer[3]+WolfBSP_W_SL_1000.TLayer[4]+WolfBSP_W_SL_1000.TLayer[5]+WolfBSP_W_SL_1000.TLayer[6]+WolfBSP_W_SL_1000.TLayer[7]+WolfBSP_W_SL_1000.TLayer[8]+WolfBSP_W_SL_1000.TLayer[9]+WolfBSP_W_SL_1000.TLayer[10])/10-T0_TS);
		TS_SOC_BT = TS_E_Storage_BT/(WolfBSP_W_SL_1000.cpMed*WolfBSP_W_SL_1000.rhoMed*WolfBSP_W_SL_1000.VStorage*(Tmax_TS - T0_TS));
		
		// Heat Sink (Heating)
		HS_S_TM_VL_bM = heatingUnitFlowTemperature1.FlowSupply.T;
		HS_S_TM_VL_aM = heatingUnitFlowTemperature1.FlowSink.T;
		HS_S_TM_RL = heatingUnitFlowTemperature1.ReturnSupply.T;
		HS_S_TM_Room = simpleHeatedBuilding1.TZone[2];
		HS_S_FW_HC_aM = heatingUnitFlowTemperature1.FlowSink.qv;
		HS_S_FW_HC_bM = heatingUnitFlowTemperature1.FlowSupply.qv;
		HS_P_DemHeatHC_is = simpleHeatedBuilding1.QHeat;
		HS_E_DemHeatHC_consumed = simpleHeatedBuilding1.EHeat;
		
		// Heat Sink (Domestic Hot Water)
		HS_S_TM_HW_VL = dHW_demand1.FlowHotWater.T;
		HS_S_TM_HW_RL = dHW_demand1.ReturnHotWater.T;
		HS_S_FW_HW_VL = dHW_demand1.qv_DHW;
		HS_P_DemHeatHW_is = dHW_demand1.Q_DHW;
		HS_E_DemHeatHW_consumed = dHW_demand1.E_DHW;
		
		
		// PV
		PV_P = -grid2.PGrid[4];
		PV_E = -grid2.EGridfeed[4];
	equation
		// Controller
		// Solar thermal controller (copied from Green City's 'SolarController')
		TCollectorST = WolfCRK12.TCollector;
		TStorageSTConnection = TS_S_TM_BT_5;
		TFlowST = WolfCRK12.FlowST.T;
		TReturnST = WolfCRK12.ReturnST.T;
		when (((TCollectorST - TStorageSTConnection) > deltaTonST) and StandardSTcontrol) then
			CPonST = true;
		elsewhen (((TCollectorST - TStorageSTConnection) < deltaToffST) or not StandardSTcontrol) then		
			CPonST = false;
		end when;
		if CPonST then
			qvRefST = max(min((qvMinST + (qvMaxST - qvMinST) * ((((TFlowST - TReturnST) - deltaTFlowReturnLowST) / (deltaTFlowReturnUpST - deltaTFlowReturnLowST)))),qvMaxST),qvMinST);
		else
			qvRefST = 0;
		end if;
		
		// Solar Thermal 3way valve
		when (((TCollectorST - TS_S_TM_BT_9) > deltaTonST) and StandardControlSTSwitch) then 
			SwitchSTTSPort_StandardControl = true;
		elsewhen (((TCollectorST - TS_S_TM_BT_9) < deltaToffST) or not StandardControlSTSwitch) then
			SwitchSTTSPort_StandardControl = false;
		end when;
		
		// Heat pump 3way valve
		when (TS_S_TM_BT_8 < TStartHPhigh) and StandardControlHPSwitch then 
			SwitchHPTSPort_StandardControl = true;
		elsewhen (TS_S_TM_BT_7 > TStopHPhigh) or not StandardControlHPSwitch then
			SwitchHPTSPort_StandardControl = false;
		end when;
		
		// Heat pump
		when ((SwitchHPTSPort_StandardControl or (TS_S_TM_BT_3 < TStartHPlow)) and StandardHPcontrol) then
			HPon = true;
		elsewhen (((not SwitchHPTSPort_StandardControl) and (TS_S_TM_BT_2 > TStopHPlow)) or not StandardHPcontrol) then
			HPon = false;
		end when;
		when (HPon and ((TS_S_TM_BT_8 < TStartAUXhigh) or (TS_S_TM_BT_3 < TStartAUXlow))) then
			AUXon = true;
		elsewhen ((((TS_S_TM_BT_7 > TStopAUXhigh) or (not SwitchHPTSPort_StandardControl)) and (TS_S_TM_BT_2 > TStopAUXlow)) or not HPon) then
			AUXon = false;
		end when;
		if HPon and AUXon then
			HPinStandardControl = true;
			AUXinStandardControl = 1;
		elseif HPon and not AUXon then
			HPinStandardControl =  true;
			AUXinStandardControl = 0;
		else
			HPinStandardControl = false;
			AUXinStandardControl = 0;
		end if;
	equation
		connect(environmentConditions1,WolfCRK12.EnvironmentConditions) annotation(Line(
			points={{455,-95},{450,-95},{-40,-95},{-40,-330},{-35,-330}},
			color={192,192,192},
			thickness=0.015625));
		connect(phaseTap1.Grid1,heatingUnitFlowTemperature1.Grid1) annotation(Line(
			points={{245,-375},{250,-375},{259.3,-375},{259.3,-235},{259.3,-230}},
			color={247,148,29},
			thickness=0.015625));
		connect(environmentConditions1,dHW_demand1.EnvironmentConditions) annotation(Line(
			points={{455,-95},{450,-95},{399.7,-95},{399.7,-255},{394.7,-255}},
			color={192,192,192},
			thickness=0.015625));
		connect(grid1.LVGridE,phaseTap1.Grid3) annotation(Line(
			points={{195,-375},{200,-375},{230,-375},{235,-375}},
			color={247,148,29},
			thickness=0.015625));
		connect(pump2.PumpIn,WolfCRK12.FlowST) annotation(Line(
			points={{65,-320},{60,-320},{10,-320},{5,-320}},
			color={190,30,45},
			thickness=0.015625));
		connect(measureThermal1.TMedium,dHW_demand1.TPipe) annotation(
			Line(
				points={{300,-250},{300,-245},{390,-245},{390,-250}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(extractVolumeFlow1.qvMedium,defineVolumeFlow1.qvMedium) annotation(Line(
			points={{185,-105},{185,-100},{205,-100},{205,-105}},
			color={0,0,127},
			thickness=0.0625));
		connect(extractVolumeFlow1.TMedium,DTH_HEXin) annotation(Line(
			points={{175,-105},{175,-100},{175,45},{-110,45},{-115,45}},
			color={0,0,127},
			thickness=0.0625));
		connect(TDH_HEXout,defineVolumeFlow1.TMedium) annotation(Line(
			points={{-125,90},{-120,90},{215,90},{215,-100},{215,-105}},
			color={0,0,127},
			thickness=0.0625));
		connect(extractVolumeFlow1.qvMedium,qv_HEX) annotation(Line(
			points={{185,-105},{185,-100},{185,65},{-110,65},{-115,65}},
			color={0,0,127},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH1.qvDistrictHeating,qvDHpump) annotation(Line(
			points={{170,-145},{170,-140},{170,20},{-120,20},{-125,20}},
			color={0,0,127},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH1.HeatExchangerOut,defineVolumeFlow1.Pipe) annotation(Line(
			points={{190,-145},{190,-140},{190,-130},{210,-130},{210,-125}},
			color={190,30,45},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH1.HeatExchangerIn,extractVolumeFlow1.Pipe) annotation(Line(
			points={{180,-145},{180,-140},{180,-130},{180,-125}},
			color={190,30,45}));
		connect(hydraulicSwitch_TS_DH1.FeedInPumpOut,phaseTap5.Grid1) annotation(Line(
			points={{210,-145},{210,-140},{215,-140},{215,-345},{210,-345}},
			color={247,148,29},
			thickness=0.0625));
		connect(phaseTap6.Grid1,hydraulicSwitch_TS_DH1.ExtractionPumpOut) annotation(
			Line(
				points={{215,-360},{220,-360},{220,-135},{205,-135},{205,-145}},
				color={247,148,29},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(WolfBSP_W_SL_1000.ReturnIn7,heatingUnitFlowTemperature1.ReturnSupply) annotation(Line(
			points={{200,-219.7},{205,-219.7},{250,-219.7},{250,-221.3},{255,-221.3}},
			color={190,30,45},
			thickness=0.0625));
		connect(WolfBSP_W_SL_1000.FlowOut7,heatingUnitFlowTemperature1.FlowSupply) annotation(Line(
			points={{200,-210},{205,-210},{250,-210},{250,-203.7},{255,-203.7}},
			color={190,30,45}));
		connect(hydraulicSwitch_TS_DH1.StorageDischargeReturn,WolfBSP_W_SL_1000.ReturnIn6) annotation(Line(
			points={{215,-159.7},{215,-164.7},{215,-195},{205,-195},{200,-195}},
			color={190,30,45}));
		connect(hydraulicSwitch_TS_DH1.StorageDischargeFlow,WolfBSP_W_SL_1000.FlowOut6) annotation(Line(
			points={{210,-159.7},{210,-164.7},{210,-185},{205,-185},{200,-185}},
			color={190,30,45},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH1.StorageChargeReturn,WolfBSP_W_SL_1000.ReturnOut1) annotation(Line(
			points={{155,-159.7},{155,-164.7},{155,-190},{160,-190},{165,-190}},
			color={190,30,45},
			thickness=0.0625));
		connect(WolfBSP_W_SL_1000.FlowIn1,hydraulicSwitch_TS_DH1.StorageChargeFlow) annotation(Line(
			points={{165,-185},{160,-185},{160,-164.7},{160,-159.7}},
			color={190,30,45},
			thickness=0.0625));
		connect(TColdWater.y,dHW_demand1.TColdWater) annotation(Line(
			points={{414,-285},{409,-285},{390,-285},{390,-274.7},{390,-269.7}},
			color={0,0,127},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.PipeIn,heatingUnitFlowTemperature1.FlowSink) annotation(Line(
			points={{340,-205},{335,-205},{295,-205},{295,-203.7},{290,-203.7}},
			color={190,30,45},
			thickness=0.0625));
		connect(heatingUnitFlowTemperature1.ReturnSink,simpleHeatedBuilding1.PipeOut) annotation(Line(
			points={{290,-221.3},{295,-221.3},{335,-221.3},{335,-220},{340,-220}},
			color={190,30,45},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.qv_Ref,heatingUnitFlowTemperature1.qvRef) annotation(Line(
			points={{345,-195},{345,-190},{281.3,-190},{281.3,-195}},
			color={0,0,127},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.T_Ref,heatingUnitFlowTemperature1.TRef) annotation(
			Line(
				points={{350,-195},{350,-185},{268,-185},{268,-195}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(grid1.LVGridF,simpleHeatedBuilding1.Grid3) annotation(Line(
			points={{195,-390},{200,-390},{370,-390},{370,-240},{370,-235}},
			color={247,148,29},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.EnvironmentConditions,environmentConditions1) annotation(Line(
			points={{365,-195},{365,-190},{365,-95},{450,-95},{455,-95}},
			color={192,192,192},
			thickness=0.0625));
		connect(qvSTpump_StandardControl.y,switch2.u1) annotation(Line(
			points={{-104,-285},{-99,-285},{-97,-285},{-97,-297},{-92,-297}},
			color={0,0,127},
			thickness=0.0625));
		connect(switch2.y,pump2.qvRef) annotation(Line(
			points={{-69,-305},{-64,-305},{70,-305},{70,-310},{70,-315}},
			color={0,0,127},
			thickness=0.0625));
		connect(phaseTap7.Grid1,pump2.Grid1) annotation(Line(
			points={{130,-375},{125,-375},{72.3,-375},{72.3,-330},{72.3,-325}},
			color={247,148,29},
			thickness=0.015625));
		connect(qvSTpump,switch2.u3) annotation(Line(
			points={{-125,-335},{-120,-335},{-97,-335},{-97,-313},{-92,-313}},
			color={0,0,127},
			thickness=0.0625));
		connect(SwitchSignal_StandardControl.y,logicalSwitch1.u1) annotation(Line(
			points={{-104,-210},{-99,-210},{-97,-210},{-97,-227},{-92,-227}},
			color={255,0,255},
			thickness=0.0625));
		connect(SwitchSTTSPort,logicalSwitch1.u3) annotation(Line(
			points={{-125,-265},{-120,-265},{-97,-265},{-97,-243},{-92,-243}},
			color={255,0,255},
			thickness=0.0625));
		connect(logicalSwitch1.y,switchPort1.SwitchPort) annotation(Line(
			points={{-69,-235},{-64,-235},{110,-235},{110,-310},{110,-315}},
			color={255,0,255},
			thickness=0.0625));
		connect(SwitchSignal_StandardControl1.y,logicalSwitch2.u1) annotation(Line(
			points={{-104,0},{-99,0},{-97,0},{-97,-17},{-92,-17}},
			color={255,0,255},
			thickness=0.0625));
		connect(logicalSwitch2.y,switchPort2.SwitchPort) annotation(Line(
			points={{-69,-25},{-64,-25},{110,-25},{110,-190},{110,-195}},
			color={255,0,255},
			thickness=0.0625));
		connect(SwitchHPTSPort,logicalSwitch2.u3) annotation(Line(
			points={{-125,-55},{-120,-55},{-97,-55},{-97,-33},{-92,-33}},
			color={255,0,255},
			thickness=0.0625));
		connect(AUXin_StandardControl.y,switch5.u1) annotation(Line(
			points={{-104,-150},{-99,-150},{-97,-150},{-97,-157},{-92,-157}},
			color={0,0,127},
			thickness=0.0625));
		connect(booleanExpression3.y,switch5.u2) annotation(Line(
			points={{-104,-165},{-99,-165},{-97,-165},{-92,-165}},
			color={255,0,255},
			thickness=0.0625));
		connect(HPAUXIn,switch5.u3) annotation(Line(
			points={{-125,-185},{-120,-185},{-97,-185},{-97,-173},{-92,-173}},
			color={0,0,127},
			thickness=0.0625));
		connect(wolfBWS1_10.HPAuxModulation,switch5.y) annotation(Line(
			points={{20,-210},{15,-210},{-64,-210},{-64,-165},{-69,-165}},
			color={0,0,127},
			thickness=0.0625));
		connect(SwitchSignal_StandardControl2.y,logicalSwitch3.u1) annotation(Line(
			points={{-104,-75},{-99,-75},{-97,-75},{-97,-92},{-92,-92}},
			color={255,0,255},
			thickness=0.0625));
		connect(logicalSwitch3.y,wolfBWS1_10.HPOn) annotation(Line(
			points={{-69,-100},{-64,-100},{15,-100},{15,-200},{20,-200}},
			color={255,0,255},
			thickness=0.0625));
		connect(wolfBWS1_10.environmentConditions,environmentConditions1) annotation(Line(
			points={{55,-185},{55,-180},{55,-95},{450,-95},{455,-95}},
			color={192,192,192},
			thickness=0.0625));
		connect(measureThermal1.PipeOut,dHW_demand1.FlowHotWater) annotation(Line(
			points={{305,-255},{310,-255},{370,-255},{375,-255}},
			color={190,30,45}));
		connect(WolfBSP_W_SL_1000.FlowOut8,measureThermal1.PipeIn) annotation(Line(
			points={{200,-234.7},{205,-234.7},{290,-234.7},{290,-255},{295,-255}},
			color={190,30,45}));
		connect(dHW_demand1.ReturnHotWater,WolfBSP_W_SL_1000.ReturnIn8) annotation(Line(
			points={{375,-265},{370,-265},{205,-265},{205,-244.7},{200,-244.7}},
			color={190,30,45}));
		connect(phaseTap6.Grid3,grid1.LVGridD) annotation(Line(
			points={{205,-360},{200,-360},{195,-360}},
			color={247,148,29},
			thickness=0.0625));
		connect(phaseTap5.Grid3,grid1.LVGridA) annotation(Line(
			points={{200,-345},{195,-345},{150,-345},{150,-360},{155,-360}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid1.LVGridB,phaseTap7.Grid3) annotation(Line(
			points={{155,-375},{150,-375},{145,-375},{140,-375}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid1.LVGridC,wolfBWS1_10.lV3Phase) annotation(Line(
			points={{155,-390},{150,-390},{55,-390},{55,-234.7},{55,-229.7}},
			color={247,148,29},
			thickness=0.0625));
		connect(HPOn,logicalSwitch3.u3) annotation(Line(
			points={{-125,-130},{-120,-130},{-97,-130},{-97,-108},{-92,-108}},
			color={255,0,255},
			thickness=0.0625));
		connect(pV2ACInverter1.MPP,photovoltaic1.MPP) annotation(Line(
			points={{325,-440},{325,-445},{375,-445},{375,-440}},
			color={0,0,127},
			thickness=0.0625));
		connect(photovoltaic1.DC,pV2ACInverter1.DCPV) annotation(Line(
			points={{355,-420},{350,-420},{345,-420}},
			color={247,148,29},
			thickness=0.0625));
		connect(environmentConditions1,photovoltaic1.EnvironmentConditions) annotation(Line(
			points={{455,-95},{450,-95},{400,-95},{400,-420},{395,-420}},
			color={192,192,192},
			thickness=0.0625));
		connect(grid2.LVGridD,pV2ACInverter1.LVGrid3) annotation(Line(
			points={{270,-420},{275,-420},{300,-420},{305,-420}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid2.LVMastGrid,lV3Phase1) annotation(
			Line(
				points={{250,-455},{250,-460},{250,-490}},
				color={247,148,29},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(grid2.LVGridA,grid1.LVMastGrid) annotation(Line(
			points={{230,-420},{225,-420},{175,-420},{175,-400},{175,-395}},
			color={247,148,29},
			thickness=0.0625));
		connect(logicalSwitch3.u2,StandardHPcontrol) annotation(Line(
			points={{-92,-100},{-97,-100},{-120,-100},{-125,-100}},
			color={255,0,255},
			thickness=0.0625));
		connect(logicalSwitch2.u2,StandardControlHPSwitch) annotation(Line(
			points={{-92,-25},{-97,-25},{-120,-25},{-125,-25}},
			color={255,0,255},
			thickness=0.0625));
		connect(switch2.u2,StandardSTcontrol) annotation(Line(
			points={{-92,-305},{-97,-305},{-120,-305},{-125,-305}},
			color={255,0,255},
			thickness=0.0625));
		connect(StandardControlSTSwitch,logicalSwitch1.u2) annotation(Line(
			points={{-125,-235},{-120,-235},{-97,-235},{-92,-235}},
			color={255,0,255},
			thickness=0.0625));
		connect(UnixTime,simpleHeatedBuilding1.UnixTime) annotation(Line(
			points={{460,-130},{455,-130},{370,-130},{370,-190},{370,-195}},
			color={0,0,127},
			thickness=0.0625));
		connect(dHW_demand1.UnixTime,UnixTime) annotation(
			Line(
				points={{394.6666870117188,-265},{399.7,-265},{425,-265},{425,-130},{460,-130}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(switchPort2.Out,wolfBWS1_10.Return) annotation(Line(
			points={{100,-220},{95,-220},{69.7,-220},{69.7,-210},{64.7,-210}},
			color={190,30,45}));
		connect(wolfBWS1_10.Flow,switchPort2.In) annotation(Line(
			points={{64.7,-200},{69.7,-200},{95,-200},{100,-200}},
			color={190,30,45}));
		connect(switchPort2.Out1,WolfBSP_W_SL_1000.FlowIn2) annotation(Line(
			points={{120,-200},{125,-200},{160,-200},{165,-200}},
			color={190,30,45}));
		connect(WolfBSP_W_SL_1000.ReturnOut2,switchPort2.In1) annotation(Line(
			points={{165,-205},{160,-205},{124.7,-205},{119.7,-205}},
			color={190,30,45}));
		connect(switchPort2.Out2,WolfBSP_W_SL_1000.FlowIn3) annotation(Line(
			points={{119.7,-215},{124.7,-215},{160,-215},{165,-215}},
			color={190,30,45}));
		connect(WolfBSP_W_SL_1000.ReturnOut3,switchPort2.In2) annotation(Line(
			points={{165,-219.7},{160,-219.7},{124.7,-219.7},{124.7,-220},{119.7,-220}},
			color={190,30,45}));
		connect(WolfBSP_W_SL_1000.FlowIn4,switchPort1.Out1) annotation(Line(
			points={{165,-229.7},{160,-229.7},{125,-229.7},{125,-320},{120,-320}},
			color={190,30,45},
			thickness=0.0625));
		connect(WolfBSP_W_SL_1000.ReturnOut4,switchPort1.In1) annotation(
			Line(
				points={{165,-234.6666564941406},{160,-234.7},{130,-234.7},{130,-325},{119.6666564941406,-325}},
				color={190,30,45}),
			__esi_AutoRoute=false);
		connect(switchPort1.In2,WolfBSP_W_SL_1000.ReturnOut5) annotation(Line(
			points={{119.7,-340},{124.7,-340},{160,-340},{160,-249.7},{165,-249.7}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort1.Out2,WolfBSP_W_SL_1000.FlowIn5) annotation(
			Line(
				points={{119.6666564941406,-335},{124.7,-335},{155,-335},{155,-244.7},{165,-244.6666564941406}},
				color={190,30,45}),
			__esi_AutoRoute=false);
		connect(switchPort1.In,pump2.PumpOut) annotation(Line(
			points={{100,-320},{95,-320},{80,-320},{75,-320}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort1.Out,WolfCRK12.ReturnST) annotation(Line(
			points={{100,-340},{95,-340},{10,-340},{5,-340}},
			color={190,30,45}));
		connect(simpleHeatedBuilding1.T_ref_in_heat,T_ref_set.y) annotation(Line(
			points={{379.7,-215},{384.7,-215},{399,-215},{404,-215}},
			color={0,0,127},
			thickness=0.0625));
	annotation(
		__esi_WolfCRK1(
			ReturnST(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowST(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ToFlow(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
		__esi_WolfSPU2_800(
			FromFlow1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FlowIn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
		__esi_wolfCGB14(
			Return(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			Flow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			measureThermal1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			condensingBoiler_DZ1(
				ReturnCB(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FlowCB(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				ToFlow(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromReturn(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))))),
		__esi_distributionValve1(
			PipeIn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeOutRegulated(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeOutRemain(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FromPipe(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToPipe2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToPipe1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
		__esi_mergingValve1(
			PipeOut(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeIn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeIn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FromPipe1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromPipe2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToPipe(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
		__esi_neoTower2(
			Return(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			Flow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			cHP_DZ1(
				ReturnCHP(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FlowCHP(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromReturn(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToFlow(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))))),
		__esi_hydraulicSwitch2HG1(
			HeatExchangerIn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorSupply2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorReturn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatExchangerOut(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorSupply1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorReturn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageChargeReturn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageChargeFlow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageDischargeFlow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageDischargeReturn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			DemandFlow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			DemandReturn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageChargeOut(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FeedInPump(
				PumpIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PumpOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				ToPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			FeedInValve(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			DemandValve(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal3(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			DHFeedIn_Extract(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve3(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve4(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureMixed(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			HeatGeneratorValve1(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve5(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureHGSupply1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureDHSupply(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			DistrictHeatingValve1(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			StorageChargeIn(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeIn(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeOut(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageChargeIn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageChargeOut1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeOut1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeIn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			measureDemandSupply(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureDemandReturn(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			ExtractionPump1(
				PumpIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PumpOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				ToPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureHGSupply2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve6(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			HeatGeneratorValve2(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			viewinfo[0](
				fMin=1,
				fMax=100,
				nf=100,
				kindSweep=1,
				expDataFormat=0,
				expNumberFormat="%.7lg",
				expMatrixNames={
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																							"A","B","C","D","E"},
				typename="AnaLinSysInfo")),
		__esi_WolfSEM1_500(
			FromFlow1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FlowIn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
		__esi_hydraulicSwitch_TS_DH1(viewinfo[0](
			fMin=1,
			fMax=100,
			nf=100,
			kindSweep=1,
			expDataFormat=0,
			expNumberFormat="%.7lg",
			expMatrixNames={
							"A","B","C","D","E"},
			typename="AnaLinSysInfo")),
		Icon(
			coordinateSystem(extent={{-250,-250},{250,250}}),
			graphics={
							Text(
								textString="SF3",
								fontName="TUM Neue Helvetica 55 Regular",
								textStyle={
									TextStyle.Bold},
								extent={{-252.5,269.8},{253.7,97.3}}),
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAALQAAACWCAIAAAD4yzVYAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAScQAAEnEB89x6jgAAIj9JREFUeF7tnQdYE3cfxy8smSJLoUjRAlq3YrXqK1WrdQAO6t5Kba2r
Vvsq4gCULSJDmQKyh+y9IQPFXbRaQcVatfriqFTFBYT3m9xBIxAEBRLxPs89PknuEsP9P/f7f393
CRB1NDRCoOWoq6ioqKyspO7QCPCxyxEZGTl58mQzM7Ps7GzqIZp6Pl45qqqqvLy8DAwMCIKQkpIa
NmxYdHQ0l8ulVtN8tHLcvXt3y5Ytn376KcxA2ZgxYwaDwejfv7+TkxM9xTTwMcpRUlIyc+ZMFRUV
BQWFNWvW5Ofnp6enz5s3T1JSUktLy9zc/I8//qA2/bj56OTIysoaPXp0t27devfubW1tzeFwTp48
WVxcDEU2bdqkpqampKQ0ZcqUs2fPUk/4iPm45PD29h40aBAqxOeff+7v73+CD/wAuFFUVOTs7Kyt
rS0tLT1q1Khjx45RT/tY+VjkQJKwsLBAyJCQkEB7goFHtTh+/DhpBgn8wCMBAQGGhoYQqF+/fi4u
LjU1NdRLfHx8FHKUlZUhSairq6MrWb16dWZmJqYS1AlKCgHwIKRJSEiYPXs2NMLUs2PHDqRX6oXq
efXPk9LE1PxdNreLTlIPdUW6vhxsNnvixImKiopIoHv27GEymeQMQunQBNIPCASfZGVlVVVVkVUv
XLhAvVxdXfWLl6VxyYf6DrJT7nFk7PjLUXE1r15T67oWXVkOLpcbGRk5dOhQZAiEDFdXV17EqA8Z
LQA/sBmsgkyIIFBk7NixLBaLfNmHl8uiTefvZcg5EqoO0iqe+gOLnT1ePXlGru1KdFk5Xr586ejo
2LdvX6QHIyOj8PBwTCWNQkbLYGMo4uHhgfCB+WjIkCHBwcEQ7nH5zeQVP9gQ0o6EihOh4SCh4qqp
m/OzReWNP6n/u6vQNeW4d+/e2rVrNTQ0MKizZs3KyMgQFjJahpxioqOjx48fD8l0dHSsrKyePX36
8MrVzB+3OEj1sCOUnOEHQ8VJSSPWbMlfxV2qAe6Ccpw5c2b69OkKCgrIGRs3biwsLMQAv4MZDZAR
ZNGiRZiekGqh3Y3y8n9u3mJZ2btoaO8j5FE/MMXYS/cImTTtj1xmXVc5Bd/V5IiNjR02bBgKBrKC
k5MTOTVQg/yukPUDmWPbtm0QDtpBPjz4qvLJpbAYr/5D9xGyToQ6FjvJ7l5DRvwaGPrqaVeIIF1H
jurqal9fX319fbSgw4cPDwkJOXXqVJtCRstAMryas7Nznz59MMVAwbi4uNrqmj8LOSHjv7ElFBwI
VZQQOwmlA710mLvsnt6toN7ZB0sXkePu3bvbt2/X1NRkMBg4rOPj42HG+0wlzQI5Tp8+jVg6ZswY
/EcQ0cfX9+XLFxUll+LnLbcl5O15EbWnPdHDXkY5ZdW6v6/eoN7fh0lXkOP69esIBPLy8jIyMqtX
r87Pz8cQUuPZ3kA4ZJrExMSpU6cSBKGqqrp169b79+8/uf1X3i+7HeXUEVHhB6qILUMxfLLp7aJT
1Lv8APmw5UBjiQFDK4Hj+JNPPtmzZw+KPxoTciA7CPgB+QoKCszNzZWUlGDkvHnzSktLX/7z5Nzh
I+46+vURVQ03fAaNLI1Nrv0wz5J9wHIgZISFhQ0YMAAho3///u7u7hgzJEdqDDsYTFv4v3bu3Eme
lTcyMsrJyXn9/EVpXIqf4Zf7JOQhBxSBH65auiddPJ8/eASXqbf+gfBByoGCUVlZaWdnp6WlhYFB
5YiMjDx79mw7xs/WgCoFRTw8PCAoeaU3NDT09evXt4tORk4zs5dSduBHEFtC0bl7z9yfLR9fv8Gt
raV+hg+BD0+OmpqasrKy9evXY75HSZ87d25GRgZyQLvHz9YAHfFfR0REfP3119LS0n379nVxcamo
qLh/uTT9h037VbTsGcqoH/aEMhSJM1t67+yvtdXV1E8i9nxgcrx69YrNZpuamsrJyampqW3YsAHx
E4cvNVaigIwgSUlJZmZmCMUqKirr1q1DBHlyr4Jt7Xjwk772Ejw/MMvsJWSD/jPpWmrW62dVH8QU
88HIgank+fPn6FG/+OIL8kIappVOiJ+thIyoGzduRC7G25szZw6Uff7P0wuB4T5DRtpLK5MRZC8h
59l34IWg8BePHov/FPNhyFFbW/vgwQMfHx/M7t26dUPI8Pf3x95//7Of7Qj5fqDswIEDkYTGjBmD
clJVVXUjpzB08gxHWVXyLJktoeTS4xMUlcobf9aK9yeJPgA50JWUl5dbWFj07NmTPHUdFRWFmb6T
42drgBx4Y97e3qNHj5aVldXX1/fz9/v78eP/lfyWsHC1s1IvBwnehVx7orudVPfk5T/87/zF2tfi
2+WKuxwvXrxAG7J8+XJM55qamuTnuPCISOJnayDPksXExBgbGysqKmpra1tZWV27du1B6fW8rTvd
dD6zl+rhSGigkUFEjZw+5xb7+Ouq5+IZQcRajmfPnqWlpU2ePBlHoYGBgaWlJYvF6rizn+0FGVEh
8cqVKzU0NJSVlXED2ajyXsWZQ77ewwwdZDC/qDvxI6rf8C8vRcY+f/BIDCOImMqBkPHw4cOQkJBh
w4ahMRk5cqSrqysm9U47x/X+4N0WFhZu27YNWqPlnjJlCnR5VlVVlpgWbDTFUVadjCD7CDk37c+O
Oxz85+ZtrphFEHGUgwwZSHaffvqpkpLS1KlTg4ODcSy2HDKOc/KxUHfEA1QL2HzgwIFRo0ah+OHf
yKioR48e/ZHPjpm1YL+ylgODfyGXUEQtyd2688HlK2J1FkTs5Hj58uX58+c3bNigpqamrq6+cOHC
5ORkmNEoZJziJJeybag7fG6xV9zjzKLu1CNyXciIit5q0qRJEF1XV9fJ2fnmzZv3fy/LXLfloNZn
DpL8zxryIoh87Lylt4tOVr94Se0LUSNeciBk5OXlzZkzB/2qnp7exo0bs7OzsXOpPS1ACduXy5SC
ItR9DucFWwsLdYfPeXbQa5YydUd0HC8qQoKOi4tbsGABdO/evfvmn3/+taSk8u7/OHudD/Uf7CCt
6kio8y/UyQZ+YYR552XlP+IQUcVFDi6Xi3obGRk5btw4VGBDQ0NbW1vsWczc5C5uxBlObB2TQLUg
78KSWrZsLUumoVRgg2qO8mu2iOUoPnH8BH82hOK5ublr167FXIkuZu7cuTgM/nnw8GJwRMA4I0QQ
R14EUUf9cNfWP+cb+OT2XZFHVLGQo6am5tatWx4eHp999pmCgsLEiRN9fX0bneOCB4JzRDEnE3JU
s7uTD95gr685K1tdrHiJfYBc+4Kjxb3AeMYx4G8uMk4Vnzh5gopKiCDk1x2QsnEA4DBISEh4XFlZ
nlMQaTxnv5Im74PKvLNkCi6q2vnbrB5cLhVtRBW9HAgZJSUl27dv79mzp6qq6qxZs1A/zp071yhk
3GYvfcr+HKNO3edwIEftZel/OIPhByYUbikDNlRwpuPuU87n3N8Z3N8Yjzhjqa35MxHKCXWns2io
HCTIp1DEy8vLyMhIXl4elnh7e9++c+fuuZK0VesP9NRxkOhBniXDjYRFK+8Un65+/pzaU52OiOV4
/vw5k8mcP38+Ki3q7Zo1a9LT04Wdyahi677k9GoY4BqWAvcao+aCLKSpPqHEvcHAUlOk8A97CB7k
3S1h3OXMITf+k726hiUnVI6cnKSoQE9PT7RIfBwcHDwPe3mFxMal5VGb8CjMSYnwoTZpBZ4BEam5
1FMbID9rGBoaamxsjINBS0trx44dV65ceXT9ZuHOvZ56A/gRldfoIoIETZh65/gpUc0vIpOD/ExG
UlLSN998g/g5dOjQXbt2sVgsYSEDYGhrWdJclnQlx/Ai2xN5gnuVwb3O4J5j1J0n6k4RdSeJunME
7vIehBznJf9krb7K3om6gjJzhbOPeqE3yYl0t/npp8XTRmpraxMUklJSOn309MbN/vZ7C0ffsDjK
kPRItx/GUpu0ggHGaw8eI5/5BiiKiKipqanm5uY4JFBCvvvuO0jz7O/Kc96B/l+M5UdUNV790NS5
ciyptlo0k4to5Kitrb1z546Pj8/w4cPl5OTGjx+Po5Y8K0DtPyGQ2YJ7iYFSgfFGbag7ztfiV4J7
mYGpBDfqThN4kHuRUXO6G5cpWXNaDpb8zfmSeok3SfS32ji1NzWYzSEtpzR4yuLdvrE8P9pJDhJE
1IKCgp07dw4YMEBGRmbatGlo2nlnyZLSI6bNcuim4qCowtxlx+tcRIQI5Kiuri4rK7O0tNTR0VFR
UTExMQkODm4UP4WBPIEQitmEW87gOXGK4N3mTyhvLNcYdWfr15YzatgKgk3vv2SHrxspQQ4kQ0JV
d9DEiaj1YPr0aUZjh+h3J1cR0ko6/1l1OBVFJjnMeRO5CcnX44b3VaW2IhR79hnxFbWCz3cW+0Px
LOHgeADOzs5jxoxBREUECToadP/+/b9On49dsDhj7ean9/4nwp62s+V49eoVJPj+++/RlWC6XbVq
FQ6Xli+kYQYRXCrY01A8MPaIn42daLT8xkAVqS2RanpyjCT70CIdCQZvXBkSGsNWWXumpZErWKzC
hBhP68VDP+GPOkHIaxmY2aeQKwVB4Vk4gtqG6PufBfvCqRWtBnsDESQwMHDGjBkIXr1790ZYuX79
+sPS61UVD7i1ojzb0alyvHjxAirgkEIVNTAw2LJlC1r/Zs9xNYAjvorVlzeDsKUxlZBL7SmZujNE
YxWaW+BQ7UkZZA70tA3LE/ag8+wgvHjgGt16NyT7rAkk/8cGCrJTvSzMKJaYb2huhnh/OQAODOwE
xK+FCxeiZVNXV//hhx9gDEosteNERCfJQZ7jwvFhaGgIM0aPHu3o6IidgqJK7aEW+Z3tgAGuPkO1
JHVsgoqcb10w+3AIXlvL72y5HEnEWERU8mX9VmqRcsAOqeFmW2ztPIPj4rOyyLWtoV3kIIEN2dnZ
mzdvJi/UoaXHkfNapJ/26Aw5ED9v3LiB7hAhAzMr6mdAQAD2RWtChiA3OBurT3bnXmHUnWhV2SAX
tDAwo/ai1Gt2jwYtSNK9lxhQdsAPhsQn2kMmzJq9YsWGTZt+trX1DA5OyM6mNhVCO8oBsE/wLw4b
hA/y6w4RERFPnz6l9mOn0+FywAxECoQMFEw1NbUFCxYkJCSgirYQMlrgGnt7bZEsr19tIoGwBV1u
bbH0Y/ZIwRNo9UQ6mI3S16AG918kJaV79x46YcLsZSt/2mJ3OCxBmCPtKwdAv4Zs7uXlNXHiRHT4
+vr6+/fvv3fvHkovtUM7kY6VA2agW0OFRJ3U09P76aefUDnJ46OtoE9BH4ujn3tagte4NpFA6IJ2
9yzxiqNWwvalXkuQdF/Hbd+aTTbQaKoID6lu8rojvpn3o7N3AvWEN2h3OQD5dYfo6Oj58+cjtuOI
+u9//3vp0qXO96MD5UD8DAsL++qrryQlJUeOHIlqyWazWzjHJQwc8dACHSzvDAda00u8HqSxAcIX
Xl9zkcEtY1QfV0RwucNe3MSSjLRIJwsLc/PV8+ZOmTKqn766OjXcFFKyykO/NveOpzYXoCPkIIEf
mZmZqLjo6cgLdUwmk9qznUVHyYFm3cnJCdlKWlp6woQJiKIomG89x9WUs+zwWqYMuhUsGN3qYkXu
r/yM2UQCYQvvhFgZL5lyL/OsqmMx8FL/Y82g/gMB2Oz0tMhI3/1OlpbfrzH/dvZXemrUsBOErJre
1K2hTaJqx8kBkNYhxO7duwcPHsxgMHCYpaSkdGZE7RA5Ll++TH5aB8qjNqJC4jh4/w+L44i/xD5Q
zVLiBVJ0H008aGbhp9fqk4pcpuQTzqA/WavxChCOekUezUYfNouZnBhk97PpBD1q5KU09Iy2h1Gr
G+hQOQAOJ8T2w4cPwwz4MWLECA8Pj8ePH1M7uoNpfzkKCwvNzMwQptCvI2SQ31Wkftb35jw7qLpI
kXcCFPXgrd0s+tgTRE2x3G32Mur5gkTYL160EAEZLPr5MHX6SxBWfmrQvtn1U4xqnyEb/Kg1DXS0
HAByYC4mP86OXk9bW/uXX35B90ft7o6kPeWorq7Gz4AGTEJCYsCAAVZWVvjZWnkmo5VUcgyROXgD
j+SBHrXZc+fkAoFOEbxzG9cYiLHNtCpB3+tKS5LDKqExwqKZYc1PibI2USY3ke6pP8kyklrRQCfI
AcizZKmpqWvWrCHr8aJFi1BUqP3eYbSbHBUVFW5ubg2zo7e3N5R/h5DRAmXs3dWnlHhCXGBgpqg9
IcOrHyX112AbFsSLC/zSghzKX1V7Ueoh5yvqVRrIDv1xpDR1hpQhofefDTaeYQLnzxNj/ex+WjSM
OoEur2Ewa1dMk+vvnSMHCbq8/Px8a2trHR0dKSmpKVOmJCYmUnu/Y2gfOW7evLlp0ybyHNe3336L
+oGC8f4hQxDeh0ZZUjVF8jUshQrO9HvsOTWnFDD2vIv1LAIRlXe9HksR/+453qRTU6xQy5KpOSOL
gFJ9TvEvznzqtepJ8F1sWH8SjCGh2mfwl5MmmfAxNp7+1fjh+lrUpTc51T4mm4M75NpKm8DBhr3q
6ur6xRdfkD2gr69vx50lawc54AGmbWVlZVQ81L2srCzMke92jksYvI/9sXqjCyWviQBES97ZjlIG
IsgDzuTqIgVeRL3MqDmuwPtIGL+EVJ9Q+p3tcI31yzOOAZclyWVJNPEj0W/PxqkjWrpkT0jLqQya
umFPSLOXVztZDgA5oEhISAgUxvStp6dnY2Nz584dajDalfeVIy4ubvLkyTIyMn379t2zZ09eXt47
nMl4Bx5xxqI8VHF0SV14w/87o+as7C32CjxI+gE5LrI9ye2Pc/LRp5SzNpF3BciJ8D9otWnjoqmG
Ah/24f1hrz599MaON1v9i52zT+gbnwcToPPlAPADB2RSUtLixYtxTGpqaq5aterixYvUkLQf7y7H
8+fPUdPQXGE/jho1Cu0Wm81u35DRAq/ZylVs3YaYiVGvPSdTw+F9EBAPkn4IyvEWcnISIwPc3d1t
67G3tz906HBwSKwwLUjy0uOCD1FPsT3ofTT+Lddi2gvymmV2djY6F8zmCgoKpqamBQUF1Ni0E+8o
B0LGrl279PX1MfPhbUVGRiJ+tm/IaAEYUMNUEGxAUD9qObK1rG7kXaxCh1JbJNtaOT5M4EdhYeH+
/fv79+9P/maK0NDQdrzQ/y5yIDYvX768Z8+eSkpKKGiob3iX7RsyWgYTRNNTFzADkwt1h/+Bwjom
0bXlAOSVbX9/fyMjI2lp6YEDBzo6Ov7999/UUL0fbZYD3dTUqVPRauvq6lpaWubk5OD9daYZwqjk
GAp+EQFUsfo2f7Gta4GCjSFAh4i2QE5ODrPMli1bysrKqAF7D9ogx4sXLxCSx44diwo2ePBgFxcX
FotFmisOoDG5yV5L3eEjOO90bXBwYiDS09M3b96srq6uqqpqZmYGaaiRe1daK8f9+/etra0xt6Ex
mTRp0tGjR/GeOi1k0LQG+IFMis4Ww4QSgi4SutS+x3deWiXH1atXzc3Ne/XqBSXhJkIy3gdthhiC
QUEViYuLQ5eAAm9oaIgu8tmzd/wTDm+XA3l41qxZ3bt3Rz/t6el569atS5cuiUPIoGkKeTbh9u3b
aBqWLFkiLy/fp08fCwuLv/76ixrOttCSHKhI0dHRCBmysrLDhw/HbcTgmpqa3377DW+Cejs04gTG
BcXj7t27r1+/Li0t3b17t4aGBuo9RPn111+pcW01QuV49OgRIueAAQPQIM2cORP/6/Pnz7lcLoyh
5RBbSDnIOoHDuKKi4siRIygeKCHffPMNWktycFtJ83LcvHkT7ZA2/w8jrl+/HvNIwweQaDnEGUE5
AA7mJ0+eJCcnjxs3Dp3EqFGjQkND0XWSa99KM3KcP39+7ty5ioqKWlpa6FBu3Lgh+FeZaTnEmUZy
kLx8+fLkyZMYUykpqX79+u3fv//hw4fUuhZpLAc6EXSqCLoDBw4MDg7GqzTqhWg5xJlm5QDV1dUY
tbVr16KxUFdXX7duXXl5ObVOOP/K8erVq6CgoCFDhsAvY2Nj/E8wrunH4Wk5xBlhcgCUf3QxTk5O
vXv3RlqYM2fOmTNnqHVCoORAcsEMgpBB/q4IhAzBqUQQWg5xpgU5AA71Z8+excTEkH+lG0EEcaSF
C3WUHBEREf3794dT9vb2d+7caTSVCELLIc60LAcJbCguLjYxMZGUlJw/f/7ly5epFU2g5MB4e3p6
otWpqqpq+ZtVtBziTGvkABjEsrIyNze38PDwyspK6tEmUHKQJzBa1oKElkOcaaUcgBxxQN1vjn8D
aSuh5RBnWi9Ha6Dl6FLQcrQzhSx2fgEzPTs/M7ew2R+rIDs79Oef8+u/0yLO0HK0A7n5zJD4LKeg
lB2H49e7HFvtGGNmHTXXKiw1u4DaQoCgBQtcPvmkILfJV5rED1qOd6eQyQpLyNrhlbDAJtJ4V6TJ
vhTTfakmNomm+1Jm2qVO3RmVmNH4ryxEWlgkEcQRDY0wJ6fMhGZ/SYcYQcvxjgTGZa51iZ1mGWWy
Lxk2wAljq7gZ5LIn1sQmYeqO8MSMN76JEOPoGN2t2ymCyCSIUCjy6ae+RkYRlpZZ4jrF0HK0meTM
vK2e8dMsI0xt04ytE0gbGi1N5Ujy8wvu0eMkQTAJgkMQJ/jLcYKIgyWamsErV2bExVGbig20HG0j
IilnmcOxmbZpJtBiTzNakEsjObISEvx1dYsJgs2Xg1xY/AWK4PFcggjQ1AzZtInJZJJPEQdoOdpA
QGzmtzbRpnZpzVYL3mIVZ2wdj3Jiui/5mx0RDXLkpacHjB6NgoGlQY6GBYoUEQSmmxQoMnmy+GQR
Wo7WEpOSM9cmxtQ2tVkhYAOvnOxNnr47BjPO9F3RU7YFJ2f+G0gzEhKC+vRBnYAKjeRoWFBCsDZQ
Xz8jKop6mkih5WgVGTkFy+1iMPyCWhhbxZvsTYIQuL1wX+Qax+hfPOPdw9KOxGSgs41Pz2Oy3vi5
4lxdw6WkEDswucADLCgkjVxBCsESOGiQONQPWo5WYXskCfGzYTZBY4IOBZljnk3UHp8k/+iMjNxm
Tmk0JeTHH9MIIkxK6pCqaqiMDKIGVEA5EZxuMMXAniOTJol8t9ByvJ2svMIFe6MwcZBmmNgkYVlm
H+0SnJKZW0ht1DoKCwv9Ro8+oKGRFBkZ7+kZtGqV37BhIQRRwC8kDXEVtxMIImrHDuppIoKW4+0E
xmZN2xnNa0/4BcN0T7SNX1J2Xtu0aCAjPNx98uSG0+d5ubmxhw8HGBuHyspCDhQSyEF2Mb4GBiwW
i9xMJNByvB3HoFRj3jmueNO9yYvsYo/GteF33TdLQVYWq6DxNBTv5eU/dmw6v2yQxSNYRiYnubk/
7NJZ0HK8nZ3eiQgcprYpS+yi3/wrbe0M6kTojz9Gy8ujrUXyyMASHEytEwW0HG9nq0fcbMec2dYx
EUk51EMdyTF7+yglpdP8ySXN3596VBTQcrydXzxijW2S9ge3+De02pUoG5s4aWn4Qcsh7nJsdoud
tzu4gNmpbzJw5kwkj3RaDjGXY4tH4vL9yZ181SM7Ofmwqmqqjw91XxTQcrwdC5/0xf6/uYd16oV1
NpNpraub7O1N3RcFtBxvZ5tX8nfR95a75cWndtLvfgSIpfsJItWvyW/P70RoOd6OpU/KipDylSE3
fvDMSs7ojI/3YU7xNDAIhRx05hBzOeyPpi3x+2150FX4YX4wMyLxfU+CtUxedrbX2LHB/E+L0XKI
uxxHjmXOcz++/OjVZQFXVgSXL3TNcz6ampvXqittbSUHOfTLL4MIAssRtLKBjf8+bWdCy/F2MnLy
FzpnQIulRy7z/bi+0OvcmoPphyPSsnL+PWGanFWw1z+x0WX6NhHj4OCmp3eUIAIIIpAg3AgiOzqa
WicKaDlahZV/6tKAK8sCS+EHluVBZUghc91PLHNJs/FPDo7LRKMbn5ZjuifG6Wg69Zy2kOzv72Nq
6spgYDaBGf4EAUVc1NULRfoNBlqOVpGenb/CNWNl6B9LA34n/cANTDTLg68tDSyd55K/wiVtjUs8
bix0Y4cltDaUZMbERNvbHzIycpGTQ6kgpxKYQVYOv7lzqe1EBC1HazkSm7nAnb0ypPxfP/iKLAu8
suLoNXiDGQe5dWXojRUH0tOy3rg+x2ax8jIyclJSshISUsPCMH0ELl/uOWHCgV69DvE9QJ0gtSAX
1A93Tc0Mkc4pgJajDfhEww8Oz4PAK//6Ub9ADtKVZQGl//VKE/yhUgMDD+joHFBVdZKRce7WzVVC
wo9vAEoFzBDUgjJDQiLaxoZ6suig5Wgb3pHpS11zkElXYEIRLCECy4qjVxd6n3c8msaqD6fHXF29
CSKEXyGwwAlMHI2cwIJHYMZhNLHr15NPFC20HG0mMS1nk2faXLfjyKRkf9tIjqVHfl8VfmuRc3pW
fbsb5+7uyS8SjWwQXLAWZhyUkQnbvJl8lsih5XhHvCLTfnDLmOvKWRZUiiCC/oXXy/BryYqQG4s9
T/hGZ7Lqf64W5EC1QBWBFphoXPv2jT1wgHyKOEDL8e7kFxQGxqRvPZS82CVznkfxYr+LS/wv8c6C
HDrt+uZf+GskB4QgnSCnGGiBOBK0enWumH1plpajHUjOyPONSrfyiV9wkLnkyO97/BqPcaybmxc/
c8AP2AAnUCp8CcKlW7eD/foFb9iQFhFBbSpO0HK0G6mZeTP3Jm3wSENFoR6qJ97d3YUgPAjCSUJi
v4KC24AB3iYmgd99l+TnV5Df+Nc0iA+0HO1GYkb+Zre49JxmrrkUZmcn+/kl+vmlBAWlhYfnZXXs
pbv2gpaj3UD87Ao/hgC0HDRCoeWgEQotB41QaDnaQqr72ukTRw8nMbU8mp7X9KusCbZzqA0MR49d
7JzEf5CZmxpgYUo93hzjxk0xX2frH53B31xMoOVoCwk2M/poyBIk/VYfTs5p+nWF6J8GURtIyCp+
uf0Y/0FmVrz7CgPq8eaQlJRRVtHqM3zCvJ0HYvhPEQNoOdpCx8lBISmj8Nn4+TuPiEcBoeVoC+0j
Rzc1na/WuQY14OPu/ONMI01qraSswYRlTiL+JAcJLUdbaB855Hrpm+4T+KVObGZhVqzv7mXjKT+k
Pxu/0E4c7KDlaAsdIwcPdpK/9eJh1AZqQ6du9BKDmYWWoy10nByc5IC9ywypDVSGTFnn2Xnf6RcK
LUdbeEMOKQUVjZ69mqCuJE1t0BY5cmM8t5roUhvofDlrV4gof98TBS1HW3hDjrfSWjny0uIcf5w3
QKGbJLle+4uZlkFNS1LnQ8vRFtpHDoaEpIx89x4NKHfvLi8rI0GtVe7/1XcuieLgBi1Hm3hDDv2l
LpGJaZmNCVz7ObWBMDlaQFl7+MrtR/I79/fECIWWoy20TyBtDiUlrQkz1jn4xeYXMsUgbZDQcrSF
9pFDtpeesdWxgkYUMpkNH0gWE2g52kL7yNFsKyuO0HK0BVqO94CWg5ZDKLQctBxCoeWg5RBKV5cj
0X7uUH1NFZKR63xTcpvKcWzbGGoDNc3ek3fH8x9kZiceXmtIPa7Zb8QCR/ITYuINLQeNUGg5aIRC
y0EjFFoOGqHQctAIRVzkoP0QQzAoRUVFtBw0zUDKcefOHWq03o93kaO8vLykpOQCjfiBcbl48eKD
Bw+o0Xo/2iwHqK6ufk0jxuAApobq/XgXOWg+Emg5aIRCy0EjFFoOGqHQctAIhZaDRii0HDRCoeWg
EUJd3f8B7chaQ/qC6bMAAAAASUVORK5CYII=",
								extent={{-225,-251.5},{225,123.5}}),
							Text(
								textString="DZ",
								fontSize=14,
								textStyle={
									TextStyle.Bold},
								lineColor={0,128,255},
								extent={{164.7,-196.8},{274.7,-260.2}})}),
		Documentation(info="<HTML><HEAD>
<META http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"> 
<TITLE>House 1 with ST, CHP and CB</TITLE> 
<STYLE type=\"text/css\">
table>tbody>tr:hover,th{background-color:#edf1f3}html{font-size:90%}body,table{font-size:1rem}body{font-family:'Open Sans',Arial,sans-serif;color:#465e70}h1,h2{margin:1em 0 .6em;font-weight:600}p{margin-top:.3em;margin-bottom:.2em}table{border-collapse:collapse;margin:1em 0;width:100%;}tr{border:1px solid #beccd3}td,th{padding:.3em 1em; border-width: 1px; border-style: solid; border-color: rgb(190, 204, 211);}td{word-break:break-word}tr:nth-child(even){background-color:#fafafa}th{font-family:'Open Sans Semibold',Arial,sans-serif;font-weight:700;text-align:left;word-break:keep-all}.lib-wrap{width:70%}@media screen and (max-width:800px){.lib-wrap{width:100%}}td p{margin:.2em}
</STYLE>
    
<META name=\"GENERATOR\" content=\"MSHTML 11.00.10570.1001\"></HEAD> 
<BODY>
<H1>House 1 with ST, CHP and CB</H1>
<HR>

<P>This model is a digital twin of the House 1 in the CoSES laboratory (<A href=\"https://doi.org/10.1109/PESGM41954.2020.9281442\">https://doi.org/10.1109/PESGM41954.2020.9281442</A>)</P>
<P><BR></P>
<P>The house consists of the following components and default conditions:</P>
<UL>
  <LI>Combined Heat and Power unit (CHP): neoTower 2.0, nominal electric and 
  thermal power: 2 kW_el / 5       kW_th</LI>
  <LI>Condensing Boiler: Wolf CGB14, nominal thermal power: 14 kW_th</LI>
  <LI>Solar Thermal emulator of variable size, up to 9 kW_th</LI>
  <LI>Thermal Storage: Wolf SPU-2 800,&nbsp;content: 750 l&nbsp;</LI>
  <LI>Domestic Hot Water (DHW)&nbsp;preparation: Wolf SEM-1 500,&nbsp;DHW 
  storage, connected to the thermal storage and solar thermal unit, content: 500 
        l</LI>
  <LI>Consumption - House parameters:<BR>Construction year: 1995 to       
  2002<BR>Additional insulation: roof + floor<BR>Number of floors: 2<BR>Number   
      of apartements: 1<BR>Number of inhabitants: 6<BR>Living area:   
  300m²<BR>Heating     system: radiators (supply / return temperatur: 60 / 45   
  °C)</LI></UL>
<P>Four boolean inputs can activate a predefined standard control for simple  
 simulation results or during a lead time of the simulation. The standard  
control  of each element is as follows:</P>
<UL>
  <LI>CHP:<BR>Switched on at 100 % modulation, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; 
      T_Storage,top &lt; T_Start,CHP and SOC_Storage &lt; SOCminCHP<BR>Switched  
   off,   when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; T_Storage,bottom &gt; T_Stop,CHP or  
     SOC_Storage &gt; SOCmaxCHP<BR><BR></LI>
  <LI>Condensing Boiler:<BR>The condensing boiler should only operate, if the    
   CHP cannot provide the required heat.<BR>Switched on at 100 % modulation,     
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; T_Storage,top &lt; T_Start,CB and     
  SOC_Storage &lt; SOCminCB<BR>Switched off, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;   
    T_Storage,bottom &gt; T_Stop,CB or SOC_Storage &gt; SOCmaxCB<BR><BR></LI>
  <LI>Solar Thermal pump:<BR>The control is&nbsp;based on Green City's   
  'SolarController'<BR>Solar thermal pump switched on,   
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_DHWStorage,in) &gt;   
  deltaTonST<BR>Solar thermal pump switched off,   
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_DHWStorage,in)&nbsp;&lt;   
  deltaToffST<BR>The pump is controlled with the following   
  conditions:<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &lt;   
  deltaTFlowReturnLow, the reference volume flow is at  minimum   
  level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &gt;   
  deltaTFlowReturnMax, the reference volume flow is at  maximum   
  level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; Between the two temperature  boundaries the 
    reference volume flow is linearly  interpolated.<BR><BR></LI>
  <LI>Domestic Hot Water pump:<BR>If the solar thermal yield isn't enough to   
  heat the domestic hot water storage, the DHW pump can charge it from the   
  thermal storage.<BR>Switched on at qvMaxDHW, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; 
      T_DHWStorage,top &lt; T_onTS_DHW and T_Storage,top &gt;   
  T_offTS_DHW<BR>Switched off,   when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;   
  T_DHWStorage,top &gt; ToffTS_DHW or T_Storage,top &lt; TDHWStorage,top</LI></UL>
<P><BR></P>
<DIV class=\"lib-wrap\">
<TABLE class=\"type\">
  <TBODY>
  <TR>
    <TH>Symbol:</TH>
    <TD colspan=\"3\"><IMG width=\"330\" height=\"254\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUoAAAD+CAYAAABcBUzSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAEgtSURBVHhe7Z0HeBVV2scjJXQVxIIgShPWgqwVy4oFEXftoigq6q4FO0jsjSaIiCC9hE6AUJXQAoTk9pZCtezqp67rYncVEBHh/533zMzNzNy5JeHW5P0/z/skd+bMOXNn5vzu+542WWClpT7P7Y2cYvVDLVFxThaysnRW2y4AK23FoExXfZ6L3rUIFPTD0Dv3c/WTIqttLFYqxKBMUxEkDN6VajUWHMU5DEpW2opByUobcejNSlcxKFksFiuKGJRpKsvOHIvwtKbI8vsmUOGaNjTjkJ+lF4My7VSMHIuKq1mNjUZrWecVK7PEoExT1TYPK1z57Nmx0kEMSlatF3cisaKJQZm2sg7B2cOKr8iTNV9Tq22s2i0GZZoq2aG3XqnwsCy/bzI6r3j8JisGMSjTVZ/nIicFlTX5HlbqO6849GZFE4MyTUVwMlRe1Wqqh0VlMJ9Y6SoGJStEtcnDYkCzYhGDkpUmSlHnFY/fZMUgBmUaK206VZKgVJZruMaqJRzQrIwSgzJNRRU4FW2FopDUeFgp6rxisWIRgzJdlcJOlVR4WOzZsdJZDMo0Fg9bSbwI0CGXtQYvPsKqnhiUrFqqWrr4CKtaYlCmqyy8GvIwE12BU+lh1abOK1ZmiUGZpko+sFLrYdH3NX83q20sVirEoExXUe9z71xUYuJz5PZOkUeZDKWo8yrcDwQDmqUXgzKdJeBRmyovh96sdBWDkmVSLfOwePwmKwYxKNNM5OEQmFLl5aRT6J2szivzjwIZh94svRiUaSpjGJoj/LwkKUUeliWgk9TbzmJFE4MyI1QZDicaHCnzsFLUecVixSIGZVpLgUXSYJVqpajzKhWdSKzMEoMyzWT06HqDI8/Eiq63GchW21i1WwzKtJO+1zmJbZM6JdvD0peXdGcuZeM3WZkkBmVayzRUx9CGlxgl3cOicDtIR2pqSL4XzaE3K5oYlBmhyrbKhHs6SfawKG8Dl0T5zClWuolBmaYyejnJ9bKS6WGlHJQWPwz0/RnWLL0YlGkmAgfBqbZUVO37hrNEe9AhoCZZwJNVu8WgZBlV2zwsHr/JikEMyjQTe1gpkPh+ybzGrMwTgzKtVYycJPR0G5RkDyvVPwwsVixiUKa1kgdKgyeZMg8rBT8MLFYMYlCmtVIEypQpmaAUZWk/BvTF9T8ODGuWSQzKtBaDMlEK9aArZ0HRPg75WXoxKNNMVEmDno2FJaoCp6pco1L1w2Aqt6Z3XrGqLAYlS8oIjuSpdv8wsDJFDEqWVKpAyWJlghiUaSnjOpTJ8HAYlCxWeDEo005Kb6wZWjJUrIkki9AemNAZQakql5WRYlCmmyJU4GR7fdriGIn0ZiN+pwR2qqSqXFZmikGZZkolOPQeK0FSKyuhgI70nRIJrFSVy8pIMSjTTGnhYYlyDGF+QsGhtMeG5E/nkNAV3lNVLisTxaBMM0nPTteJY7bEAUtpG1XK0YMiOavpaGF+0JI0njJV5bIySwxKFovFiiIGZZopVR4ll2u0xHnurEwUgzJdRcudmeLd4pwktJ1xuckpl5VRYlCmqcjjCWkXTGiniiIuVygJ5bIySwzKdBX1vpo6FpIyEJrLTU65rIwSgzKdRZVY33aWrNrL5ao7WCxFDEoWi8WKIgZlmsqy7SwJ4nJZrFAxKNNVFr2xSRGXy2KFiEGZpiJPx9BuployeoG5XB5HyTKKQclisVhRxKBksVisKGJQpq30i1QkMyTkcpNTLiuTxKBMU1Hbmaysus6Gz3NzkOj6y+Ump1xWZolBmaaiCqzUW+HxaDNHinOUSp1Acbn0b+LLZWWWGJTpKvJw1IpLlVkJCXsn3tPhcpNTLiujxKBksVisKGJQppkqvRprS1RIyOUajUNvll4MynSVrnNBE68LmQClqlxWRolBmaYij8dUf5PSycDlCnFnDsskBmW6SlRWXheS16NkpYcYlOksqsT6trNk1V4uV93BYiliULJYLFYUMShZLBYrihiUaatUzUHmcpNTLiuTxKBMU1n2xiZBXC6LFSoGZbrq81zkpMKr4XJZrBAxKNNU5OmYw8FkhIRcbnLKZWWWGJQsFosVRQxKFovFiiIGZdqKe59rdrmsTBKDMk1FbWeysuoWbeCVxuOvVJXLyiwxKNNUVIGVeis8Hm0ucnGOUqkTKC6X/k18uazMEoMyXUUejlpxqTIrISGvNB53papcVkaJQclisVhRxKBksVisKGJQpqkq2850SmqbnU5cLquWi0GZdrIerqJZSKWOm7hcvSWuXFYmikGZprL0dJIgLpfFChWDksVisaKIQZkBone4UDiY7HYzLpfFUsSgTDNRKKh/ZwtVXq3iJjJM5HKTUy4rM8WgTDMZKmlxjvFFVwnsjeVyk1MuKzPFoEw76Xtj9S/i/xy5vZPVC8zlJq5cViaKQZlMnSWMrnisRunjIS43ssWrXFaNFT0mrGTpIWH1hJkrqpVROkofD3G54S2e5bJqrOhRYSVLu4UdLcxcWa2M0lH6eIjLDW/xLJdVY0WPCiuZisXbSYSXw+WGWiLKZdVI0ePCSqZi8XYS4eVwuaGWiHJZNVL0uLCSrUjeTiK9HC43OeWyapzokWElW5G8nUR6OVxucspl1TjRI8NKhay8nWR4OVxucspl1SjRY8NKhay8nWR4OVxucspl1SjRY8NKlfTeTnW9HJp+F5xhYm0hs0yilht5rcbq2rrL9xz5962O4nGdWbVaDMpUSu/tVMnLqS7I1JdmRS03MaC8a+yXVf++9PIvc17a2xJjVbWvM4uliEGZapF3U1f9G4O0pcCOyMjFjFhuYkApF5qo4ve19JirCkpSVctlsXRiUKZa5N2crP6NorhAUrWTrszDH2HLTSAoq/B9tQUqQvKqDiirVC6LZRSDMkNEy4KFAONILewSOQkEZRUU9jtXB5Qs1hGIQZkJsmqn01s44EXt6An3ov8IoEwSpCJ6zwxKVpLFoMwARfImY1k3MRJ0rL281IEypuYFBiUryWJQpr3CtNORxUJJqaqCL1mgjPDdIhmDkpVkMSjTXvEAZQRPjUHJYkUVgzLtlYowmEF5pDp06BC+/PJLlJaW4tNPP8WBAwfUPaxMFIMy7RUNJvr3vcRLDMojEUHR7XbDbrejpKREmsvlwr59+9QUrEwTgzIDFPv4yXhBk0F5JNq5cydsNlsQkpp5vV41BSvTxKDMBMUwn9vaqgvOVIT7Fgr3vdMclE6nMwSSZA6Hg0PwDBWDMkMUu1cZweLRS14dq0Knk0EZCkpql7QCJQGUlZmKAkqLCmN46Gm/yWuhwdHqg0yV25hcPPiGh9zieFYYVTNMtbKooGFQHol+/vln2SZphuTXX3+tpmBlmmIApR5kamUNPviRQUkDpfUDmhWvSJdel5YVm+LiWaoWnl8MyiMVwdLv90tIEjS/++47dU+8lClOBjEjdAaYeRJFdR+REBmYor9G5vOo2vXLUh46cyb0mTKxyoy2aWkt9utP1HDSSp654gJpFyUIUjVdcfDi6c7FBFPzMbkaOCjTYMUKPd4y74wVXUv1ex+hWT+gdF+t01fLaiEoWZrMbKHbKu5h3MhokokXlQo9j6pIBSXlr/P+qDD5RaypWxlSh6lQwRPVgEv/qnnSw69epOIc9cRpnzguWD6l0fIwfXEDKMUxhvNQ8w35LuHyrhE6UqiF3t8jz9Nk6n2psmoyKPXfTbs+6rNe+aNO98ZcwbU6paub6nHSadCujeHa6e6xZRmV26M6HqQqnbuaTliw3mrnaKWqnrfcpW0TPMjV569dI4vzCO5TFaXcICjlger/VLDy/U2ZqTKC0rTfdCG0tIY85X7dsSEXL/w+AyiD25ULod0z+aW1D5HyrpGi71f5UMRiwR+RoCLkYbiWCZbh4U3ROSRCYeqI3C6+n3Y/aDv9H/LDLxOb6og4Lvj8y8+Vz7gEib4+WJRhzEO9/+oxIeVX4dzFDgPoDXmZVe3zrjyGthuYFtxnPA/DvhjK1YGSCqGMRIY5VgVp0m+z2E8Z6x9kFVpB71E74WKRTn8y+mP0+Zr2BS+0YTuDMpzkTZc3OoIFL5wmukYW6cgM1zLBqqGgtLonoc+0UPA5FvdD3U7HKrcrhjoSVPi0wTJirE9VP3cjoELPrVLVOe+QY2h7MJ3ueNN56PfFUq4BlFRI75wc4b5qW/QFkZQLWJmpeb+Q+QtRGnGC+m3yxMS24I2gY8QF1z7T/uB5yX16yFrdmPA3NmLetUjyl1b3cBss5HrQfbVIR5bMa0f3MdXnkACFVkxV5rqje44jOjGm42Kp+CFlxFifqn7ulJcOULRdn06n6px3yDGG/HXHm89Dty+Wco2glAnMmRkf0uDFk9KfiCrzF5InqIerEKWx6HAJto+Y8qys5AKu4oGReRnKCX9jrfK+oNd8zJlToexPa4Vef82MNzYWKdfIKi/z9Y5ULoMyDpLPv/maCxmeaSH9cyz+D+vEmI8z5U8gMNeHkDIM26PUpyqduxlQ6nMYzFxIpFWSVuO86a/umISE3iI/EyhTJPNFiKcs8s7Keh1Nm76Bli1Hpzkw4wussF5lSF4MykRLVsbgd9JVWP13C8JGfhD3JEJFN18T/bXT7wtXhmF7BFAKVfXctfSVP+4qLLU8dHlX+bzlv5V55Ygfk8rjdNdIyHgexn3Ryq21oNQs3YEZKWTWP1/RZXo49RaSEYOSxdKrVk5h1IMy3YFp/PU2m97DiKxI+YQCl0HJYunFoDRZ+gEzArSCpgshzCKP2vIYzayOZVCyWHoxKMNYWgEzHDDiYNbhO4OSxdKLQRnF6tUbiu7dZ6pHplAJgGX4nnMGJYulF4MygpFX2adPPj777Cf1yBQrahgdu0XuCGJQslh6MSgtLO0AaVLkDp4oFlNXOYOSxdKLQamzdAeklWKCZpXBwqBksfRiUGYoIFksVvJUq0HJgGSxWLGoVoKSAclisaqiWglKFovFqooYlCwWixVFDMoq6PDhw2xslsaq2WJQsmLWL7/8gpkzZ2LZsmX4448/1K0sBmXNF4OSFZP+/e9/4/7770edOnXQtGlTvPrqqxKcLAZlbRCDkhVVbrcbV155JerVq4fbb78d9BqP+vXr47777sOnn36qpqq9sgTlH7/h4NZc/LakNw66R+Dwvm/VHaxMFIOSFVH5+flo164dmjRpgkGDBsHn86G4uBh9+/ZFdnY2Lr30UtjtdjV17VQIKAUkf8vrgd+mdsD+Sadg/0Rhs7ri8I+fqAlYmSYGJctSBw4cwJgxY9CsWTO0atUK48aNQ2lpKbxeL/x+v/Qyn3vuOQnQU089FYsWLVKPrH0yg/Kge5SAZEcFkjr7bf6lagpWpolByQrRt99+i8cff1y2R5577rlYsGABKioqJBzJe3Q4HEFgjh07Fm3btsUxxxyDt956C/v371dzMer3vftwcN+v6qeaJTMoKdw2Q1KCUniYh3/7WU3FyiQxKFkGbd26FX/7299w1FFH4frrr8f69etRXl4Op9MpAak3AmdZWRny8vIkUKnd8tFHH8V//vMfNTdF3+38CO/1fxDvP/gwvv/wY3VrzZEZlL9veMQalNM6qSlYmaaEgdK4qg292yXMy61iWvaLlQxt2LABZ555Jho0aIABAwbA5XIhEAiEAFJvBFAKydesWYMbbrgBdevWxTXXXINt27bJPA/u2Ye1jzyNV8W9HprVEPOu6IUvSpxyX02RGZTUFvlb7jlGSM7ogoPb5qgpWJmmxIAyylsVCaJVfy81K1GiMZHTp09Hy5YtpY0cOVICkjpurOBoZeRZUlhOgKXe8a5du6Jw40Zg/+/YMvglDD2qIYZnNcIwYVP/1A07FizC4YM1YyxmSGeO0KHdZfhtzvlBT/KPXfo2XFrGTnlXEb1lM8RXoOXl5EbjcneVdca8DJ763qNEvs1Uk1z6LsI7mmqoYgal3kOsfHeu+aXi9FlcRLqYVQGleoOLg2Xo8jTd/OCx6vZc7XWu9GAF1y8MPd4ybxZ++OEHvPDCCzJs7tSpkxxQTu2RHo/HEoiRjMBKNmzYMAncE088EbPmzMHer7+F67WReLNpS+FVNhDAbIy3W54C5xtvybbLTJcVKCOrEpSVUKxUcY7FM0rPcRBQuuOFZN2kPEx1Jb5SI0JR9w3vw64lig2UhptEN1JcMPWGGKBH6dSbrryP2hpKlqAUEAtu04PWdPMNoBTHKMWpv7Bq2SHnFC7vWq4PP/wQN998s/wBueqqq/Dee+9JSFq1R8Zq1G5JeeTm5qJLly5o3LgxXnr1VXz7n//io/mLMKFdZwzJypawHNXoOKx7YiB+/uJL9YwyU0cESsP/JPHZ6vk01APTMdo+9W+lU6Avgz4rVln39M1huvyCDocwE8RDz7d2KCZQhoDNAJvKG0vpjNdVvUHBtIosQWlIo7sZpn0GUAa3Kzc8WLb+VzpS3rVY1P540UUXycrQv39/2Gw2GT5bwa+qRnkTLFetWiUHqlPved9+/fDvr77CN24fZl/wFwnLYQKWQ0UonnfdTfivr1w9s8zTkYGSHtfKZ9dcN+izBFaEZ1imoQzoWRdpteMpX0M9I8k0yrEh9ZBkqi+hTQO1s/7EAZT0kTxHAasc/c2slPlih+QXCWamfcFjDdsZlFXRwoULcdppp8mpiC+++CIoXKahPlbQq66RV0rgpcHp9957r+xF7yGgWbZtG375+BMsu7Gv7NwZltVEQnN61wvxr9Xr1TPMLB0pKCufV3qOwzQN0XMcPIaOVz0+PUTNz7quHkiYBo/R1S3x2Vw3K9MpZoQpgzK86ILrLo4Mq003pDe1XVjeYSW9+WaEgFJ3w+TN0t/8YAivALHKoAyXdy3T3r17ZUcNDSJv3749Jk6cKD0/GhOpjY+Mt1GnEOX/7LPPysHpBOgVq9/Hr19/h6Knn8fIRs1Vz7IB3jmpHUonz8ChA7+rZ5wZOmJQap8Nz7RZeoiGgZX5eK0e6OuDxbGyPqvbQupmiBiUEaVcTMUqO3M00cXT/RJKsFamr7xJiixBKfILdsxY3kgyEdoL77XKoDTlfUGv+Zgzp0LZX0tEYxsffPBB6dldcMEFWLp0qRzCox9Enigjj5XGZ9LsnpNOOkl6smPE/7/88BMqJk7D2BPbClg2lB7myIbNsenZl2QHUKboyEGpPOM0hz4spOg5Dh5TNVAa6pusm6HHBp0ZQzlWYlDGLrrYBlAeocw3OJ6yyFt7X07LlqNrBTAJUr169ZI/FLfccgsKCwuPuNOmqkZApoHrNMuHBqfTeMunBw/Gd99/j09WrsbULn+W7ZXkXVJHz4rb78X3uz5Sv0F6Kx6gVABmCrvltkonoXJf1UCppFfz0fVay+hK2647zrA9pBwGZeyqAaDUv2CspgLz0KFDskPlrLPOQsOGDeWsGepooQHiVjBLtGmdPOvWrZOzfwiWNwlwb9u5E1/7SpHX83oBySbSqN1y9iVX4rMtNvXbpK+qDkpWpql6oMxw6UFZU4H522+/yTbI448/Xi5qMWLEiCoPIk+UUSdPSUkJHnroIQnwbn/+MzbbSvDz/32BdX9/DCOzm6uwrI8J7c/AjvlLcOj3g+o3Sz8xKGu+GJQmqwnA/Prrr/H888/LzhOaITNr1iwZ9lKnihW4UmEEbeppHz58uByY3uaUNpizcCH2fPc93CNG4+3mbWQYPiSrAd5q3hr2IaPw6/c/qt8wvcSgrPliUIaxTAXmThHG9unTR64VefXVV2PFihUy3KU2QitgpdII3ATwqVOnysHp1Bs/fNRIfLv7G+zIXYDJHc5Se8QbYXjdpigQ3ubPn/9b/abpIwZlzReDMorVqzcU3bvPVI9Mb23cuBEXX3yxnA3zwAMPoKioKOzKP+li1G5JnU30Hp4ePXrIULz//ffjgw8/whebSzD/il4iDBcAVYcQ5V13I/7j9qnfOD3EoKz5YlBGMPIq+/TJx2ef/aQemZ6i9sh58+ahQ4cOsk3ypZdeknO1tU4bu0PvTdphc/iE0QDzxA4LitUI5AR06o3v168fGjVqJAene8T5//jxv7Cq770YWedY6V2+nlUfU7uejw/yV+LwoUPqFUitGJQ1XwxKC8sUQJJokd2hQ4eiRYsWOP300zFhwoTgSuQKiOxwO9YHYWl3eFDmWIAKx0wVlgqsShwBuU/7nAqj8yYPc/DgwWjevDnOOvtsrFi9Gj/83+coynkZY5qdLDzLprKT551W7eAbPzktFtVgUNZ8MSh1lkmAJP3zn/+UITZ12lx22WVyjGJ5xVa43Arw7A4XnA4b/mPvhwr7jCAM/2u7RdiN4nOZTGMT2z+0DxdpcsXn1LVl0sB3v8+HgN8vV0und/WQh/zOu+/im692o3ziDLzbtrPsEafB6aMat0Dh0zn45T//Va9IasSgrPliUGYgIEkEFhpETpCkNyOuXbtWhK8VcDtLBHCcEogEQbKfHV3xte16bHHsEN7lRvzqaIMfHd3hcmxGsWM7djrG4oD9OHxgf0OG5RJaKQCm0+mA2+WEjzp5Kiowd+5cnHfeeWjWtCkef+opfPZ/n+HTgnXIvfAyjBCeJa1tScBcektffF2xXb0yyReDsuarVoMyEwFJL/1avnw5zhZhKb2n5umnn5aQke2RTi989pX4l/05eES4rXiQXux23oADzpYosy9EqW0RDvmz8Xvp0fDblglbib2BdjjsrYty+2x5DHmYbscm4Y1at2E6RXjs8YiyhPenGYX6brfwYAXsgmmdIp23Mk1U87rh97hlmyWF4NRuSUu/3XjjjbKT59bbbsOOjz7C7kAZll7fB2/UOVrt5MnGrAv+gn+uXo/DfyR/MWAGZc1XrQRlJgKS9OOPP2LUqFE4+eSTZVhKC1wQYLSVf8h7dDic+Mp+O761XQW3fYPwGLfiM8cAYFsW/uc7B9/ZeuDwh3WBnVn40tYPP3ouAj7JwsFAY3htayQoP7EPkkZ5KaaBzwm3AKS7ZAveX7lMenyzZ8/GnNkLsWjJUhQUboLD7YVHAtMJ2+b1yM+bj1mzRJo5cyLYbJlP/ipRvt0Fl4CtXZRHeWgrED388MPyh+HCiy7Cmo0b8f0/P8WGxwZidNMTZChOM3nGte4I/5QZ+OO3A+oVS44YlDVftRKUmagvvvgCAwcOlGMNaVEL/SBy/aIWNuFBuh2F+Nl+jgixW+MTxyD829Yfh3fWAXaJ2+0TtlVY+VHSi8QO8f//ZeGAtyV22UbjW0dP/Gpvg3LHbJlXEJICmNSTviF/Ol4acD+u6N4VzVu0wNFHHy3sZHTo1BW97uiHF9+cgoIiu4C3F5vmjUKPrm3QTKYhOwbNmx+H445TrPmxx6jbFbu8Xw7yN3rhdRuHM9EPAX3P119/Ha1bt8Ypp5yCGbNn4cfdXyMw9l2MP7mDDMNfysrCohtvx28//k+9askRg7Lmi0GZASJQ3CbCTlp1h96MSOEoQdI8iJzaFSnU3uLYjjLbAhzyNcTh7UfhoKexAsUKcbuFZ0ne5OFdRyn/lwsT2w/56uNQWQP59yPbEOGJbhN5Kh4q5e3xBbB27pu49aJToCyWcBTqZDeUIXHD7GzUURdRaNSiNa4fMARrbB4UzR+JS/90AuqL/ZSuQXZ99VjVjqonX2TWUBj9vfiOQVhSGApK+iEgz5maF6ZNmxZsdnjp9dfwzTff4OOlKzGp01mY2PFMfF5kI3KpVy45YlDWfDEo01j00q+CggK52g6BgeZGb9myRQLDOIhc+d/rWIOAfSl89vfgs63EN46/SihKD1J4joc/FV7k58I+U43+/z9hOwQ0y0SaD7Lwtau3CH8DskecOnZkp47LizLXOjx381kK4Oo0QbcrH8LrYydh0mRhY97CU/2vQcvGKgCPaY8nxi+Ha8sGzJ05Ge+MG4+Jk6di7Ijn0fvPzZU02S1w/m2PY9y74viJwisc/y7mLFqBzSVOYzunzrTXTNDg9J49e0r43t3/Xnz48cf4YvMWfL4xNWvaMChrvhiUaap9+/Zh8uTJOPXUU+Uajq+88ooMfWWnjQkgyjAgO76w/x37bKdiv6sN9nj/hF/dp+Kwp64IuwUkNShaGe2jsNybhf/Zu4lw/Rl8an8aX9lvw6eOp+DxB+BcORE9O9VRIHdcFwyctB47d+3E9h07sHP7drg3rcTrj9+CXtf0RM/rb8Uz7+TB5fEjIM63vKwMFdt2wrNxCR7ueZKSR6NW+OuzU+Ar24atFeWyLdLv84SFpGZau+WmTZvk4HSahXR5jx7YLH5AUiUGZc0XgzINpS1qQV4khZkETAJk5SDyUCNYehzr8In9Gex1d5LeIT4UgCwTECRPkjxIK0hqJtJIr3K7MBGSH3bVw/9s52CX4y24fCIMXzkBPTsepUCuQXN0u7EfXh07EfOWrcC6wkIR7jvh84uw2SM8UBEq0zt49OdH0Cx6fy4euPJEFZQnodfT40SI75FDgvRpYzHNq6brRGMt23fogIV5eTh4MPmrDDEoa74YlGkmWnX8rrvukm129GZEGgpk1R5pZRQmUy+3y74ZX7r7yfZGAl9USJJRGgrBS7Pwi+9MfGQfIvOUs3ecXvi96/HGA1fiuHoCcgQ6YQ1bnoyOZ5yJP19+OW579FGMHD8J+SvWwS6ALoGpO7d4g5KM2m5pFaLx48fL6ZvUQUSrEf30U3JHM5hBOWBAAcaN82D//vRdGo5VNTEo00jU/ti9e3fZaUMv5NqwYYMMM43tkeGNoEbtiuRZfunqh8OObKXjJlLYrZkAJXmgsGfhP/a+wit0yvxKHKUSwC7qXV83H68/ei3OPeM41KtbCUxpdeui2bEn4E/n9cJjI9/F++J4t4s6g5RzSwQoqZOHmiPohyRPeJN07ej95NSW+8knn6hXNfEyg/K008bhzjuXyb8MzJohBmUaiMJFmn7YqVMnnHDCCXJRC4KjVXuktRFonCh3zJFjI393NFdC6K0CktsEBGMBJRl5lCLsPvhBY+xzt8N/7bfiA/twOdyIYOkWUPI412PJ3DdFyDsAf+97O3qcexZOOqkpGuo8zQatz8QDb86Bw13Z5pgIUJIRLLXB6WvWrJHvKSdvnF6TS154MmQFShqju3v3HgwcuJ6BWQPEoEyxfvjhBwwZMgTHHnusDB8pjCRA0nAYKzBYmTLQ3I7P7APwY8lF+M7RAz+4L8XvnmNxuEzAkiAYS/hdLkBJPeQCsof89bCnpBO+t12GgCMfTk+5OK8ylJVXyM4Zv98N++aNWLl4IWbMeAtDBt2Fi9qcgPoSlnXQ8Yr7MH+DGz51qE+iQKk3um7UNvrUU0/J8abUvvv+++/LV2IkUuFAqYmASUv1dekykWGZoWJQplC0qEX//v1luEiLWixatEiG2tVdZNfpKBG2RXp/pSWL8ZvnRDlO8vAHUcJvgih1/viy8IPjEuwqGY1t9qlymJHTaYNXWH7u2xj4TA4GP/sixsxcDlfAD484T5qiGCgNIODZgtyXHkSn+opXeeIZPTBmqRN+jxJ+JwOUZNRuSUaLatDAdJrFNHbsWPz888/qVY+/IoFy1aoP0a3bVOHpLkZFxW65jZV5SjgojW90y1JfKWvxJjfDS8B0b42TpqWt7hvgwuWXOlHbGoWHBElakXz9+vUyfKQwUj/TpipGniUthkGDxWm+t5yN8y9xi/0ClLtUIBIw6a/+/w+EJ0mD0T/Mwveuy4VX5kORY5cyjtLpQ4XfhglPX4d64todVacuTul+O2av9aJceHAE9rLyMlSUubBw9AB0VkHZ5rybMa3ABV+SQUmmtVvS7CVaVIPWt3zsscfw738nZnV0K1BSqM2ArDlKKCjl+7i192sbFAsoK/dL2AZfu1ldUFrll3zRIPLFixfjjDPOkNP2Bg0aJMEYe3tkJLPLgeLbHZNw0NNEtjceKG2BP1yNlaE/2qycDwUcqeOGBqMTIEuFUcj9sbBdWfjSdbcCSRpsLvL1+gJYv2AYLj5BHR5U92hcdN3DGDlmKmbOzBU2EW+9+iiuObeVEnrXPQG9n3wbm50CgmpHVDJBSUY/OARxmsV03XXXyVdj3HTTTdi+Pf6rDJlBmRxAxlKHxHUOmpI29NmndKbX5LJCJGpGghTxFbSx3GTd/uA+3XZ1W3HQYzXlZ1C4/JIrCv9Gjx4th7G0bdsWY8aMieubEQlupfbF+Ml+Lr5zXIGPba9gR/FY/Oo9RXqWf7gb4lBJA2XaIsGR5n3TQHMByH2utvjOfiUO+psCrqPwpeNekSct1yYgJoDn8xdh5tCH0a3d8er1PgoNGjWT87ibNWuCRtnqYPQGx+D83k9gzppieFVvkkwB5Rz0/0sLJV2943D1k+8kDJRkWocYvRLjkUcekaMJzj//fOm9x1NmUCZHVaxDQX2O3N5aZCdSCWcmRT5DRilhoKRfrt5hf6boJlJlM1mYm2zpUdJDIY7RyqAbHrk8q/ySpy+//FJWVpp2R4ta0Ko55PFQmGhVyatj2qDzUsdiuBxF2OL4QC6ddtDbDPt87bDDMRb/53hC9oQfLG+MH5yXyB5u/DMLP3rOh9dWAL9tuRxD+YX9AXjtBUGv0ulyi3MtQd70d/D0fXfh4j93QRMBHjnXu2EjtG59Ki6/ug8efXEUFqwslD3kTt25uTwinF89HwP+2lF40s1w9IkdcdOzExMKSs2ozZLafWl2E81yopWXpkyZIl+hEQ9VB5TyGVSf+5wc7T35BDG9d0efrWBHsgBhTKAU0tKlyGHIRKUQlNFuchSAmm9ysXjYwsIvXH7JEb08i9ZU1MK/1atXS0+HwkOrin0kRrCk8Y/UVlniKBdgfBJ7bZ1QZp8nwLkTAfsyHHLXw/883QQY1+IL5/0y/P6ftxtc9iJxTIX0TJUhR8a2UlqH0uvxwlW8GSvy8zB9xgz5BsVp06Zi7tz5eG91Iey0zJrwJPWQlMcK785evAkrF83CtKmTMWXGLCx5f4P0WKNNW4yH0awmAibNcjrzzDPlrCcCJ406OFJVGZT07Oqef9lEpT6ThnpD6WJ9pkOe7cjPvCyTQ+6YlTBQSnCFBZIOeJpCQGn1a6jbXmVQWuWXWFEFWrlypVzUgsK+AQMGyOErFG5bVeZ4G4HyK3sffOR4Tf5PAKQVzvfZT8XXtuvEtlI5sPwnx/nY6+ygjpckD5cgGc7LE2ATwLRauJd6wV2RBseLfW7hWfp8fvnKB1q30jJdgoy8SvqBojbiK664Qs4Tv+eee/Dxxx+rd6x6qiooQ5wIQ10Rz6r6P6ULH/hUtw4pYlBWTYkDpbhd1BZigJd4IJSP1b3Juu1pDkoaRE7eS6tWrdCmTRs5tY7CbPJqrCpxIoy8S79jmQzDlR5xMjd2227CV7Y+sne8WHiQO+1v41t7L7kqugJK6/xqgmntluvWrQsuqkGjD0pKStQ7V3XFF5T0kQAm6k+O7vkOUXXrkJBWnrkOscIqgaAkqbDU3P8gyKp7k3XbjwCUvXrNx5w5Feqn+IvejPjCCy/IXu2zzjpLrqFIXmQ82yNjM3o1rVfCUdtGYfl2+yTsso8SHmWZ2KaE2ARJGocZ3pOsOUawpPtB3j21D1LnWseOHeU41uoMTq9y6E3Pqu551IfeUmJ/b3FeORHdverWIWM7aOS2fZamBIMyPaW9L6dly9FxB+bOnTvlNDp66Re1R1LoTZUyEe2R1TWCp7J6udYGSb3b1RvknskmmwvEj9fEiRNxzjnnSO//7bffrvLg9CqDUkgJfRWr7MzRRJCLFhbHAsrKMrQwOxSMsZTFqmGgND8c1r+W2svF4g3MjRs3yh5tguRrr70mZ97QQrNWlZQtPYy8S2qjpL/aWy1pdMLnIvSNVdUBpUGm0JuVfqq1HqXZjgSYv//+uxzuQ2MjqT1y+vTp8m2Je/bsOaKZNmyJN2qb3LFjhwy5P/30U/kSMxryRNCk9uRYZAZllZdZiwjK2H78WYkVg9JkVQUmvRmRXnpFizDQdLnCwkJ1j7LgBYMyvY1ASWuA0g8bidayHDFiBJo3b46uXbvKTp9oMoOSpjDyMms1SwzKMBYLMMkDoeEl5IHQS79ofrFeDMr0Nw2U+sHnNGKB1rds3769bLekCCHS4HQrUPIyazVLDMooVq/eULlEllnUQUPDSmh4Cb1GlmbemMWgTH+zAqUmanO+8MILZbvls88+i++//17dY1Q4UGriZdYyXwzKCEZeZZ8++YaHnirUihUr0LlzZzkdjqbC7d+/X91rFIMy/S0SKEkffvgh+vbtK34w6+GOO+7ARx99pO6pVCRQ8jJrNUMMSguzAqQmej0DLbDbrVs3rF27Vt1qLQZl+ls0UJKoHZqmO1L0QLDcvdsIPCtQ8jJrNUsMSp1FAqQmWshi0qRJ2LVrl7olvBiU6W+xgJJEIxtoQPrMmTPlfdXLDMrIgDRNwtBMTpYI7eE2TqKg/ZHGTlop2jHmMpW0NHsotGxtvKVFnjVcDMoYAVkdMSjT32IFZSSZQRmrQqYyhgDIPA04GvSsFO2YcNBTytaKpoHqBm7WMtVqUCYKkJoYlOlv6Q1KEm2L4MklDJRCWrqQMtRj1O2xrQmb2aqVoEw0IDUxKJNjLqf2Goyqz1NPf1CKrUFvjvbrw2TVooIy0jGm/aa8lKmW5imOOlCKY7TvEDo9suaoVoIyWWJQJscIkFu9m+Fx2cT/1mnCWeaB0rQ/xNszK9ox1mVqigpKfdkRF6bJbDEoEygGZXKsxOHDd8XDsdW+CjZn1VZoyozQW9sWDXpWinaMVZmqtKmVIWWoxzAoWfEQgzIZZkexqwK71zyJHUVzBShpVSSrdNaWCZ05lWmSCcpIy7ExKFlxVLxAabfTWwXdKCt1o7zMjjJh9NdspYHaB2TyIAOO9dhT0Bc/r38YDmfV2irTD5S69kJhRu4kCpT6MhU4hrY3UjpTpxKDkhUPxQuU9AqD9etWYtToVXjh1VK8+JovxJ592Ye33/WItMl5B026WJF7O/658S0gvzP2rLpRbqOOHXO6cJZKULIyRwzKBCpeoAwEnJgzdzO6X2nHC69sxPgJq/HW2AKMfLMAY8TfseNXY+jwtbj77z5RXu0BZbHDi9Lildi78m84nN8Ne967OdgDbpXeymoOKM2eYc3tgU6FGJQJVLxAWVbmxKQpG3D2hVswaowHeYvsWLHKBputGMuW2zB/oQPjJzlw7z/8EpK1AZQEQ5/Pg19W34XDi8/BocXnYZ8AZanfDZs9M0JvVuaIQZlAxQuUPp8XixcvwaCcPBF6e3Hfw15ce5MP8xa48NfbvLjnHyUY/LxLQJTaMmsHKLe4KvCVbYIIuS/E74vOxx+LuuGX5b3lPg69WfEWgzKBihcoqWe3vNyDnTuc+OdHm4UH6cSV1zkwf4Edt9xpQ+6s9/HxhyWoKC8WUCWw0kv/7fB4CBpW+WWuUUcNQXJX4UQcWHgO/lh4Nn7POw+HFpyJH5der6ZhULLiKwZlAhUPUJJ36PHYRbjtwvRZbizIc+HlIT5c2MOF14a5cNVfnXjupY2Yt9CL3NluzJztwsxZLsye68KatS64hYdplW8mGgGw2L0NuzZMxO/zz8bBeWfgwPxz8Pu8rvgj71xsLVqQUcODWJmjlIKShkfoG5+VkQWmRumo472iydzIXZ08qqcjBSVBsqzMgWEj3SLUDuC5V7wY/IIXOS958dzL4u+L6t+XfBgs/teMtt3/iB+PD3LD7yuW4Ximh+T0lkgaIvWv9aNxYM6ZODinMw7MPQu/C/tjVnt8W/A4XG53lbxJMgYlKxalDJRyapTlmCsjECvHcx0JKCuPk3BO0livWEDp9SqhMv2lz5Te53MKE+F2mR0jRntwxXWlWLjYhe3b7CgtVWzrVgd2bLeLcFz8X6GModT20faly1144hkCpfKubhqL6XYrwNSXnwlml7NtnPjivcH4I7c9fp/ZAQdyu6jWGX/MaIcd6ydji3tryLHRjEHJikWpAWXEQbJ6sOlnB+i2q8fHtmqJCbBRB+jGT5FASd4dwWvFKhcWirB6+UryhFzYvHkdlq8owsr3PHh9hFdCksLucuFZasfSEKDCwmJxrB35y1xYXeA0hNgE3vkiRH/+FZ8Ixf24qW8prr4+gGGjSkW5LricR9pmmkxzynD6gwLhSU5tgwPTTsVv0zsE7Y8pJ+GrJfdImOq9yZIY31POoGTFopSAMnRGgl4ENl2oHPT+TKAU+7Q8QmcR6GUEZbp4lNTRsm69EzfeEcBDj/tw290BFJf4MH5CHnr0moP+D/px38PCk5SQrDye4Lp9mxPDR25Gt0vc6HlDuYChV3iOlWk0UL48RIThIly/+Ooy9L3PjwuvKMPifBf8fuO5pLMRJL229dg35XT8NvFk/DrpNIMdfLcFPs8fgC3eHSJ95TXY6lwrwBl9mBCDkhWL0hSUVmAzgTLmqVMm8CbJmyRFA+Wq9514+AkfAn67CJN9KNrskF7f8y8Xori4SB7rF9DTH0cQLFhjw613edG1+1b8fUBAhN5GIFCaeQtdsq1y4HNeXH97ADNmuXHeXzIQlC4vtm3Ow6/vdsT+cafg1/ECkDr7fcxx+CLvIQHK7TK9Bsf/FY3AR45lUT1LBiUrFqUm9NZWJVE/GmUEZSUUjwSUuvySqFhA+fggH8pK7Xh0oA9LBMRyXvBi8jS/bIO06nwhz/Gdd93odnEprr3JhmXLlbZHfRpq76SQ/q77/ehzt1/CeNJUN86/PDNB+cF7b+PXtzpg/5sCjqPbG2z/m22xd0wXVKybjSL/ThS7/LB5y/DzlMvx7fx7YPNtE/mEXn/NGJSsWJSizhzzEvdCAnbKRyPY4uNRpi8oHxOApND6nn8E0PXicun1kfdHnTJWx2wucuCaG0tFGF2ON0fPx64dm4QHqXTWmNNv2eIU5pAe65Tpbpx7WWaCcuu6ufj1jc7YP0yAcngHaftG0N/22De8Iw683gpfTb0DO9bPw07PZvxr+RvYO+J0/DL+cpTbC+XCGVZ5kzEoWbEoRaAkqbDUQuIg6Ahsuu1BIMYPlL16zcecORXqp8QpFlA++rRPgit3jhtj3vEIYPoxcYonBJTKeEoHXhvuFZAsxcVXlYqwOiCHBhFYqZfcnJ7ypYHnO7ZlNig9RWux9/U/4ddX2mHvqx2EdRTWCftePg37XlE+//pCaxwYdiYOTr0Rv714sth3qkyzY+nbKPGUW+ZNlgpQ0its6Rn86Sfr1xyz0k8pBGXqpL0vp2XL0QkFZqweJQ3tGTqSpiL60eumUkydYe1REvxowPmCRQ7k5X+I3rd8gOv7+LC6wCXz09J5PE6sXbcJDzyyFnf0L8PSpYWYnuvK2DZKT9E6/O+1C/FrjvAgnz8de4X9Nvg0/N+sZ/Hj0EvV7Z2x71kB0GfaYt9zp+PXwadi36grsWvLKpREGISeTFASINu1G4djjhmJ7OxhCX8VCSt+qkGgNHmiwsJ1GGkvF0s0MKsSet/3iB833xmQYXU4UJJRR80HO2144y0/Luvpxdq1JSK0pk4MY5qly2nWjh2X9nSj3wM+TJ6WmW2UZMX+CuxY8i72P9YGewd1wb4n2+Hn4b1QWrwRn8x6EfufaIu9AztjzxPC4xR/Kc3epzth/6DO+HjxWBR7U+tR6gGpf+4YlJmjWutRmi0RwIy1M4dA+Tj1ehc5MWSEF+9O9KCiwi47aaw6dLZtdcj1J598pgQety0kTWnAgfxlNEPHiYHPOnDjHaWYNlMJvRctyTxQuos2YOvqBdj3VDfseeR07H+gNf41aTA2b/0YgY2rsefZi7FnQBf8OqoP9jwqgCn+3/dgO3zzYk94NhbA5k6NR2kFSP3zxqDMHDEoTRZPYMYCygcG+LF5s0P+LSx04NWhLgwTYXjRFgfWb3CGzKah4UIL8px4bbhbLtKr7/GmDh2fV4SrNofwIgMiDQ0yL8PFV5XhhtsDuKRnKZYuyyxQ2l1u+AuW4rMxj+Prl27Cnnvb4df7O+LTGa9ji/A07V4/Pp30HPbd3VZ4mXcKOF6PPfd1Euna48fnemF74SoRvocfIpQIUEYCpGYMyswSgzKMxQOYkUBJgNu4icZR+uWAc/pLoJsmwu57HvTLcZUUir861CuHD2mwpFk5K1asxXvvrRMhtshbzU+DJP3f7wG/XMR3yxbhlXoCwvssxR33+jFJhN+0wIaVl5q+5oTN48e2hZPxv7vPxp4+HfDrjSfjk7eFR7n9I3iLi/DNK/dg381t8fmr92NX7hjsubU9fr7rDOzr2xk/P/IX+NesEHn4LPKOLyhjAaRmDMrMEoMyitWrNxTdu89Uj6yaIoGSjIDlUcNr+kvbCIg0HZHGS9L0xKv+Vip7uimcVtLYUFy8SY6VpLSaUbskHX+XgCSB0iHAqaWhv2TkxWYWJBWzi2voFPbBjDH46cYu+P7u7vj4refgLVgJt9+Pb157EHsvb4my5Quxbda72NuzNX6+4XT82OtUfPjUbXAWbYLdbT1EKF6g7Nt3KerXH2b5DFkZgzKzxKCMYPQw9+mTX+0HOhooyQhcmum30V96iVjeEht6Xu/BuAnFcIlQu6SEQmvlr2bkTW4ucqHf3wPod7+ApDhWP1zInH9GmtOJ4tIKbJ03Fd/ccik+eekJeNcXoCRQBs+a97F9+ljY/AHsnDoWey47FT/dcw0+7tER7mnjYS+nxTKs70G8QLl79x489NBqHH30SPnjavU86Y1BmVliUFrYkQJSUyygjGYBvw0TpxSj980O4V360fOG0hC7+vpS/PUWF/7xqBd2m+JdWg1Az3wTPxACeqXTJ+GTay9GIG8e7D4/7B4vigPl0rvcJUJx/8rl2HbDFfA/8QAcbpeErHV+8Q29SbECk0GZWWJQ6ixegNQUD1A6RSX3+UqQO3s9HnnKiady/MJ8Bnv8GT/GvLNBpC2R4bVVPjXHnLCVlcM1JxfuhfPhEJDU77d7fHAWFMD76gtwlNjE/vCzcsgS0ZlDCg/Mp4UZh7FJC84+s9quKXTyRMjkixBFO8ZcppI2dPEYSlf5nu+qyfiO8EwUg1JYvAGpKR6g1Mzndcql1igcNxsNL/L7Y3/7YjzOJ9Vm9/pUCBq/C7Vnule/L39gHO7oS60lCpSawgGTnjnnm1ebxvqaoWae6psoUJr2Sylla0XTCl0GblZJDMqMVKIBqSkSKN3C8wmUlqO8XLXSUvi9HkvYuUTaMjVdacAnX8lqTOOU+ZWa0lCPuFsARdtuaaUBeEV46jTklx5GK5r7/KXix0A917IylPp98LgsQmmnG/7SMiWdAF95RYXxeworKysVPzjiGuuOSzQoNZmB2ajR8BhASaJtFmuyakoYKIW0dFZl0LRhkxcqZUorPVOdxxp+1bD0Vq0EZaIBqckKlC63AKSosMWFq7Bw3mRMnDQREycImzUbiws2wxuoQMCreULkJdpQuDof0ydOwLvvTsTMuUuwoVgJyYNpHDasf28JZoi8xo+fiNx5+SgUadwusX0VbZ+ECVTGRKNNEDZ5zjwUFAmPVMDVnS4dPk4XfIFScT5b8N7SOZg+TZzvhAmYMHUqcpesxGaHH2X6HwyR3lG8DgtnTsWEdyeEfE/NZs7Mxcp1m+Ap9QcXOk4WKDVpwGzS5A0ExvaMAZTEJM2bo/2V0AlaVFBGOsa035SXfBOBOeQmGOrO0xCmW4Cyd24xe5Ss8DKC0gmPT1TuTasw+rUncNM1F6B964aoV78u6tath/rHt0KXS/+K+wYNxbxVtCKQSCuO9bqKMOO1e9G2UUM0aNAYnS++C9PXOuD1qKt5C0/K69qIyS/ejQ7NGiE7uzHO7NEfMwpEqO4vwsRn70L7Zo3RsGEjNGrYANn16wmPRrG69eujaZu2uORv/fDqhGUocngFfMJ3fCTDnG4PfK4SLJ45Gg/3vwEXnn0Sjm4mzrWuuE5NmuL4P52Ha+98BCMm56HYqXiX5HG7CibgutNboWGDhmjUuBEaZNcPfk/FstHy+BNwXo/rMXjUNGxweuUPQ7JBqZcCEQOWYgClab+Vt2dQtGOsy9RkBcqI582gZFVVelC6PV44NyzEUzddhhYNtF/wemjcuInwLgTg6qrb6jRE5+63Y/ziTcK79MHnLsLUF+5Ac/UXv805t2LKGgd8OlD6XIWYMLgPTlDTnHbBHZgiQFkhQDnu6VtwvLo9K6suGjRqLMpsLMpsgoYCmsr2LDQ9+VI8O2UFHAJUoaF9cswpQm2vaxNmDnsMF5zWInhu9Rs0kufbuGHd4LbGJ56Jf7wyA5tcXnh9AbhWj0OP1k2D++tmN0RjOkZ+V/FDUf+o4L4GJ10ovusqeEs9sKc9KPXbokHPStGOsSpTFYXXlM4SfmHOm0HJqqqCoBSeT8D+Hl648wI0UStrs/YX4cGXxmBh3mIsXjIHk954CJe1OQ515P566NLjYeQVe1HuLcK0l+4Mwu7Uc/tgqgUoJz57B05WYduh+52YqoJy/KDb0Kqesv2UP9+AoVPmYdGiRcgTNmPcG7j5gi5oeJR63HWDsEqE4R53CrxK+h5eBxaPexLntTpank9WoxNxye2PYdyUOeIaLcK8mcPwYO/zcRztE9bgmHPx3NTlcNHQoILxuKrtscpxWS3R57lxWJSfj4ULFyJv8SLMGvc0rujUTN3fAn977B1sKSuF05Z8UNIMHlpmbfuEa6KAUulQqUyTTFAaO2DIswyeBx2vO4ZgaAi9gx6odv4MSlYEKaCkDgkvVk3KwVnNlQresEU3DJq8Ep6AH34RYlOYHQh48P74l3Gemibr6A54dEIBdpaVGEB52nl3YOYGAdBSP7xexZuiEHvqC3eidRRQdrrsXszeQCsTBUSZfpSVifOa9jzOO6ah3N/k5Kvx9vKN8HjcSe/cIY/bsWERHr22C+rRdz2qGS65+yUs20yzlJRr5POLEHtdPp7s0Vm5RsL+dNcQFLnL4F3zrg6UrfDYxDXYunMnKsqog2cbduzYjOF3dEe23N8UPf8+ChtLS+GyJw+U+imOtMyadWeO8r00M/Y0JwqU+jIVoBnAKEXpdLDTd+aYylfCdSWvnBzF+5QwFduMeWaOGJQJlASlR8DQvg6v978IdeXDk41z7hyCLV7qba703JxuL9y2jZgzaRSGDBmKISPfxtxVm1Dm22IA5Snn3IR38jegaFMhNmzYgA2FG1FUuBJvPXULTooCyo6X3I0ZBTYVhE74K0qxedmbuKJlE7m/YcsLMWThBgGtJIPS6YRXlLly0mCc21o9l049MXJugQCkt3IkgNMlvF0X1ubPwBvDxTUaOgRvzcyH3eU3eZQn4r4R87BxyxYUimu0cfMmrM4bhhvPPgZ1j6qLY9pegOcmiNBb/DglI/S2mgNOHYo84DxzxKBMoAiUHn8Axaunoc+ZzZVKnH0s7hy+GAGfxVAgAQKvSB8ICPP7BRTc8LiMoXf9Bk1xfKs2OKVNG7RR7ZQ2J+P4Y5sqnljWUegYBpSdL/87Ftm24oMPdmGn8La2l7mQ+/o/0LFpfbm/WfsbxXGbk+5ROkXY7XFvxJhH/4qTspVz7XjtQ5i/xg6/1sQQNGUolLxGwvxet/wxcq3Wg7Iujm55EtqccopyjU5pg1YtletTr/E5+Mcr80W4HoDbYU9oZ06kRTLiD8pQbzRTvbd0FIMygSJQ+kSIvCF/JK5o3lh5gJu0xBMT1oaM59ODgIb+yOE/BE4TKKNbeFC2bHc++j70JAYOHIhBg57BQ3ffis7Hqe2BWfVx4QMjUGhzw201TjGB5nR5RJmr8HKfv+BY9Xucf9tALCn0hm0v1a4RXcNQUEaweiej23VPYsZ7m+D1uWFLAChjWUWIPcrMEoMygQqCcukoXHmcElJGBiUNIaK2wzJlcLQIO71OIyiPb9cdD704HCPfGIHhw4cLewMjR7yCv994MZrXUdKEC73DWp2GIiz/B6Yu3wJ3DLNZ4m0aKF+5/fJg7/75fcKDkoYQBUrpGpXJwfWeEFAei6v7P4M33nxTXqMRb4zCkJefwFVdWqr766HrrYOxcos41mmLGyhjAaRmDMrMEoMygSJQUii95f3puO1MFZT1j8XtwxajLCT0FiGly471q/KRO2MGZkyfjSUr1sPlLcZ0HSjbXXAX5mwpx87tW5UZJxXbsHOrAzNfvjtqZ06DJi3QtkMndO58Ok4/XbEzr+iJx4aMxbJCmyg/nJebWFNC781469Fr0UoNvdv1ehDzC+zw0aIW+rTC2y3ZvBaL5szCDHGd5szOx0aXD+4CU2fOpDXYtmsXKuQ12oqKijKsHPcYTm+hDImq1/pyDJ+3XniVLmyPAyh5mbWaLQZlAkWgdHsD8BWtwjM3thcVhCpxNs667WUU+coMnTkuTynKHavw5HUd1XTH429PT4F/mxMzdaA8kuFBHS+5BzPpWOGpul0uOXTJRe2g1Hsu8ksFJMmc1MTgcWPRqH+gY0sFZA3aXYERc6gzx1f5g+Jyo6zUi7yR/YNAbdHtPiwp8cG3doIOlCfhobeXydWElO8kfoS8frjXT8X1nY6TaerUPwfPTFwGZ8ATF1DGumqQZgzKzBKDMoEiULpEmOj3u7B4zKM4tZFSubOb/gmPjVsO/1YRPpaWolSEkFvLvcgf8zg6HK0MjM5ufQ5emW/DzkCxIfQ+IlBeeg9y15pAKcxqfnmyjYYH2Qpmod9fTlPHkjbCubflYGmRSzZDlIrrRB60a61I072d/D5kPZ58F3ZvBTyGXu9WcmhVxY4dwetbsXUrCnNfwDknZStpsrti8MTlIaD83CkqxZAItkomM+jwd0BvbX/OHmSdtxpZDUT4XYeXWaspYlAmUBKUAkR2pxelztV4/e7L0Fit4NknnYG+Tw/H3LnzsWDBQox9uT/Oaq5W4qzGuPje11EovMxST/wGnBuHB4XCKqUmvEqfz4llE3JwwcnqwPB6x6Drdfdh5LhpWLBwEWZNHo47L+2ojoXMQpNOvTF+2UZ4A2VwmQac3/rMGMzLyxPXdw7mzl+AWVNHoe9FbVUIZ+GYP9+Oae8Xi9DeYWijjApK1XqLdJoMoNQsCjCrBsrQHu0jX36NVRUxKBOoICjtdtkzG7Ctwsv9eqJFQ9NDr7cGzfCXmwZhyRYChwiJXUWY+uIdaKHub9MtzBTGnD44UU1z2oW6KYwDb8EJ6sybdt3vwvR0BSWZ04OAz4ZF7zyHSzuow6nCWJsuV+H1aSvgCHjh8SpTGK9orc28iWzZJ1+MgeOWwVXmCxlHqQelHoak3KmV+8hyPlK2hxsehO+BK3XArGNaZq1qoNSDUJnxcmTLr7GqIgZlAqUHJYGAFm8IODZi2rhXcffNl+H00xqhfrayaEP9Vq1x1tV98MKb07DJ7Ybf55ZellwU49V70Ca7PurWbYjThbc4XYCSlkZT4KIuivFCP7Rrko06dRrgjMvvFUB0otxXhAnP3onTmortWdk4o8d9yCVQivzTEpRk4vsEvC6sXjINOY/fhb+c1xrHHiuuES2KcfTROOHsS3DHgJex4P0N8JX64BLH0HV1FkzAtZ1ORP169dGgYQM0yM5Gdv36yKa/coGMujjhhPbo1fdxDFuwCsUCrm5xrHkcZSRQkopF6B2EpQAngSgcKA1gnWNsw2xYv1MQ3Dk56pxqCUD9VD/6TAC0AKHcpqWNAkr1/2J1doxhkQsTUIPzuNXtudosG4JycDZO6PGWedcgMSgTKDMoyWgGTml5BVy2jXh/1SLkifBQ2vIVWLPZgUBphdLJo77Kwemwo2hDAfLzForwMw9LV6zGZhtt1+BCYwnt2Lx+NfIX5YkwPg/LVhbINC6nHZu07fOV7UXy2OSOk6yyURguwmm/CMU3rluB/HxxfRYuxMIlS7B8bSEc3jLheXorrwGNObVtwqqlS5S53do1NdhCLFv2HrbYvfDTMmtqu2xVQUleoj7MzhWfrUBpDuE1iMlOn7ueQP2sy7BC/E+SU/5UWBkWnCAISa/RCpTKcYpTSftVoOlNAyDlIz4H89UWu6D/I4FSHGPIX/VgQ84xXN41SAzKBMoKlJpRJ49Pm4WjzsTxhQmJXQKu/gB1aAQEPGgptHBpAoY0tHCvcXvqVgaqjtGbFz1ef+U1Eub3UkeUBejJ+6brKb6nPr3e/LTor2mR4iqDUkjvKVL4HQJKsU3br6XRywAakgEuAkrq/5SuElTRQBndo6wsUZc+EiiD25VQX+UkFVwZ9kfKuwaJQZlARQIlW3pYIkCZo+4Ll0dkUNJHCl8FnHK0bVbw0W9jUCZaDMoEikBJ0+yoMhIs2dLPiouLsXXr1riB0qoNM0QEGh1M9KG3lNjfOycHOUGYmuGjgKsStjGAMhhG08fextA72K6oy7cqoAyXdw0SgzKB+v777yUobTabpTfDlnqLZxuluV2S9oVT5VJkAjLBzhxNBD59pwh9rkwvjzFQMQZQiv+DHTOmtJXnUrksWpVAGSHvmiIGZQK1f/9+7N69G1999RX++9//sqWh0b357rvvcOjQIXnPooHS4DGqg88lKE0ADeeNWorAYwBlnGWAXpyVyLzTSAxKFkunSKA0j6PUPCcCpWFfuJA7nBiUaS8GJYulkzl8Dmf68PKzGI8hixSOs9JXDEoWS6dooDQP9SFtWWmd1soYlJkpBiWLxWJFEYOSxWKxoohByWKxWFHEoGSxWKyIAv4fQBkl4L8F+oEAAAAASUVORK5CYII=\"></TD></TR>
  <TR>
    <TH>Identifier:</TH>
    <TD colspan=\"3\">CoSES_Models.Houses.SF1</TD></TR>
  <TR>
    <TH>Version:</TH>
    <TD colspan=\"3\">1.0</TD></TR>
  <TR>
    <TH>File:</TH>
    <TD colspan=\"3\">SF1.mo</TD></TR>
  <TR>
    <TH>Connectors:</TH>
    <TD>Electrical Low-Voltage AC Three-Phase Connector</TD>
    <TD>lV3Phase1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Environment Conditions Connector</TD>
    <TD>environmentConditions1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the condensing       
                boiler</TD>
    <TD>StandardCBcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the CHP</TD>
    <TD>StandardCHPcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                     pump</TD>
    <TD>StandardSTcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the DHW pump</TD>
    <TD>StandardDHWcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                      supply and return is constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP input - electric power</TD>
    <TD>CHPIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to district heating heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                      negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of domestic hot water pump to charge the DHW     
                    storage</TD>
    <TD>qvDHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Parameters:</TH>
    <TD>Heated 3-zone-building with changeable heating system</TD>
    <TD>simpleHeatedBuilding1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature controller wiring for heating system</TD>
    <TD>heatingUnitFlowTemperature1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow generation and electrical power calculation</TD>
    <TD>pump1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW_demand</TD>
    <TD>dHW_demand1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                components</TD>
    <TD>grid1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat storage with variable temperature profile</TD>
    <TD>WolfSPU2_800</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar Thermal</TD>
    <TD>WolfCRK12</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow generation and electrical power calculation</TD>
    <TD>pump2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Specifies a Thermal Volume Flow Connector</TD>
    <TD>defineVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Extracts the characteristics of a Thermal Volume Flow Connector</TD>
    <TD>extractVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                components</TD>
    <TD>grid2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat storage with variable temperature profile</TD>
    <TD>WolfSEM1_500</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Controllable valve for volume flow distribution</TD>
    <TD>distributionValve1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Valve for volume flow merging</TD>
    <TD>mergingValve1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Hydraulic Switch with two heat generators</TD>
    <TD>hydraulicSwitch2HG1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBControlIn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CHPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Boolean expression</TD>
    <TD>CHP_EVU</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>TColdWater</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Green city of the CHP model based on measurements in the laboratory</TD>
    <TD>neoTower2_GC1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>VolumeFlow_DHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature difference between flow and return temperature</TD>
    <TD>CBDeltaT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature of CB</TD>
    <TD>CBTmax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature of CB</TD>
    <TD>CBTmin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum electrical power of CHP</TD>
    <TD>CHPmax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum electrical power of CHP</TD>
    <TD>CHPmin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, solar thermal collector is CPC collector, else, solar thermal 
                        collector is a flat plate collector</TD>
    <TD>CPC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inclination angle of solar thermal collector</TD>
    <TD>alphaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Orientation angle of solar thermal collector</TD>
    <TD>betaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in series</TD>
    <TD>nSeries</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in parallel</TD>
    <TD>nParallel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Effective surface area of solar thermal collector</TD>
    <TD>AModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Absorber volume</TD>
    <TD>VAbsorber</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature within the thermal storage</TD>
    <TD>Tmax_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature level to calculate the stored energy (e.g. return          
               temperature of consumption)</TD>
    <TD>T0_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, the temperature profile at simulation begin within the        
                 storage is linear, if false the profile is defined by a 
      temperature                 vector</TD>
    <TD>TSLinearProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of upmost heat storage layer at simulation begin</TD>
    <TD>TSTupInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of lowmost heat storage layer at simulation begin</TD>
    <TD>TSTlowInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Vector of temperature profile of the layers at simulation begin,       
                  element 1 is at lowest layer</TD>
    <TD>TSTLayerVector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature within the thermal storage</TD>
    <TD>Tmax_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature level to calculate the stored energy (e.g. return          
               temperature of DHW</TD>
    <TD>T0_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, the temperature profile at simulation begin within the        
                 storage is linear, if false the profile is defined by a 
      temperature                 vector</TD>
    <TD>DHWLinearProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of upmost heat storage layer at simulation begin</TD>
    <TD>DHWTupInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of lowmost heat storage layer at simulation begin</TD>
    <TD>DHWTlowInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Vector of temperature profile of the layers at simulation begin,       
                  element 1 is at lowest layer</TD>
    <TD>DHWTLayerVector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of people living in the building</TD>
    <TD>nPeople</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nFloors</TD>
    <TD>nFloors</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nApartments</TD>
    <TD>nApartments</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heated (living) area (e.g. 50m² per person)</TD>
    <TD>ALH</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, use standard area-specific heating power, else define it      
                   manually</TD>
    <TD>UseStandardHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Area-specific heating power - modern radiators: 14 - 15 W/m²; space    
                     heating: 15 W/m²</TD>
    <TD>QHeatNormLivingArea</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1</TD>
    <TD>n</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 -     
                     45°C</TD>
    <TD>TFlowHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal return temperature - radiator: 45 - 65°C; floor heating: 28 -   
                         35°C</TD>
    <TD>TReturnHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference indoor temperature</TD>
    <TD>TRef</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximumg flow rate in Living Zone</TD>
    <TD>qvMaxLivingZone</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TLiving_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TRoof_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TCellar_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If the presence is used, individual presence data has to be provided,  
                       else standart presence is used</TD>
    <TD>UseIndividualPresence</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with presence timeseries (presence in %; 0% - no one is at home;  
                       100% - everyone is at home)</TD>
    <TD>PresenceFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If individual electricity consumption is used, individual consumption  
                       data ha to be provided, else standart load profiles are 
      used</TD>
    <TD>UseIndividualElecConsumption</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with electric consumption time series (consumption in kW)</TD>
    <TD>ElConsumptionFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Table with electric consumption time series (consumption in W)</TD>
    <TD>ElConsumptionTable</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>ElFactor</TD>
    <TD>ElFactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, night time reduction is activated, else temperature is        
                 constant</TD>
    <TD>ActivateNightTimeReduction</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionStart</TD>
    <TD>NightTimeReductionStart</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionEnd</TD>
    <TD>NightTimeReductionEnd</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at night</TD>
    <TD>Tnight</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, presence will be used to define the temperature (if less      
                   people are at home, less rooms are heated and the average     
        temperature       will       decrease)</TD>
    <TD>VariableTemperatureProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature, when noone is at home (TRefSet = TMin + (TRef -   
                      TMin) * Presence(t))</TD>
    <TD>TMin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true: DHW consumption data repeats weekly</TD>
    <TD>WeeklyData</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Data File</TD>
    <TD>File</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Table Name</TD>
    <TD>Table</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>V_DHWperDay_l</TD>
    <TD>V_DHWperDay_l</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Factor, with which the DHW consumption gets multiplied</TD>
    <TD>DHWfactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CB turns on (SOCmin &lt; SOCmin and T<tstart)< 
      td=\"\">         </tstart)<>             
    <TD>SOCminCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CB turns off</TD>
    <TD>SOCmaxCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the CB turns on (T<tstart 
      td=\"\" and=\"\" socmin=\"\" <=\"\" socmin)<=\"\">         </tstart>             
    <TD>TStartCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the CB stops</TD>
    <TD>TStopCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CHP turns on (SOCmin &lt; SOCmin and T<tstart)< td=\"\"> 
                                </tstart)<>             
    <TD>SOCminCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CHP turns off</TD>
    <TD>SOCmaxCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the CHP turns on (T<tstart 
      td=\"\" and=\"\" socmin=\"\" <=\"\" socmin)<=\"\">         </tstart>             
    <TD>TStartCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the CHP stops</TD>
    <TD>TStopCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature difference between collector and storage         
                temperature</TD>
    <TD>deltaTonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-off-temperature difference between collector and storage        
                 temperature</TD>
    <TD>deltaToffST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature difference between flow and return, qv=qvMin</TD>
    <TD>deltaTFlowReturnLowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature difference between flow and return, qv=qvMax</TD>
    <TD>deltaTFlowReturnUpST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum volume flow of circulation pump</TD>
    <TD>qvMinST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum volume flow of circulation pump</TD>
    <TD>qvMaxST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature of DHW storage charging</TD>
    <TD>TonTS_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-off temperature of DHW storage charging</TD>
    <TD>ToffTS_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature, if the DHW temperature &lt; DHW consumption     
                    temperater + deltaTonDHW, the storage will be charged</TD>
    <TD>deltaTonDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum volume flow of circulation pump</TD>
    <TD>qvMaxDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>qvSTpump_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CHPin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>WolfCGB14 validated with CoSES mearusements</TD>
    <TD>wolfCGB14_GC1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Results:</TH>
    <TD>If true, standard control will be used to control the condensing       
                boiler</TD>
    <TD>StandardCBcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the CB modulation</TD>
    <TD>CBinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the CHP</TD>
    <TD>StandardCHPcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the CHP modulation</TD>
    <TD>CHPinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                     pump</TD>
    <TD>StandardSTcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on/off of solar thermal pump</TD>
    <TD>CPonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvRefST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal collector temperature</TD>
    <TD>TCollectorST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of solar thermal heat storage connection</TD>
    <TD>TStorageSTConnection</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature solar thermal</TD>
    <TD>TFlowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature solar thermal</TD>
    <TD>TReturnST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the DHW pump</TD>
    <TD>StandardDHWcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on/off of DHW pump</TD>
    <TD>CPonDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control flow rate of the DHW pump</TD>
    <TD>qvRefDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler flow temperature</TD>
    <TD>CB_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler return temperature</TD>
    <TD>CB_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow condensing boiler</TD>
    <TD>CB_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas flow condensing boiler</TD>
    <TD>CB_S_FG</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Demanded fuel volume</TD>
    <TD>CB_VFuel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of condensing boiler</TD>
    <TD>CB_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas power of condensing boiler</TD>
    <TD>CB_P_gas_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of condensing boiler</TD>
    <TD>CB_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas input of condensing boiler</TD>
    <TD>CB_E_gas_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating efficiency</TD>
    <TD>CB_Efficiency</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP flow temperature</TD>
    <TD>CHP_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP return temperature</TD>
    <TD>CHP_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow CHP</TD>
    <TD>CHP_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas flow CHP</TD>
    <TD>CHP_S_FG</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Demanded fuel volume</TD>
    <TD>CHP_VFuel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of CHP</TD>
    <TD>CHP_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric power output of CHP</TD>
    <TD>CHP_P_el_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas power of CHP</TD>
    <TD>CHP_P_gas_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of CHP</TD>
    <TD>CHP_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electricity output of CHP</TD>
    <TD>CHP_E_el_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas input of CHP</TD>
    <TD>CHP_E_gas_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal efficiency</TD>
    <TD>CHP_Efficiency_th</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric efficiency</TD>
    <TD>CHP_Efficiency_el</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Overall efficiency</TD>
    <TD>CHP_Efficiency_total</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal flow temperature</TD>
    <TD>ST_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal return temperature</TD>
    <TD>ST_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>ST_S_TM_Collector</TD>
    <TD>ST_S_TM_Collector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow solar thermal</TD>
    <TD>ST_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of solar thermal</TD>
    <TD>ST_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of solar thermal</TD>
    <TD>ST_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Power transfered to DHW storage</TD>
    <TD>TS_P_heat_toHWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature consumption side</TD>
    <TD>TS_S_TM_HC_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature consumption side</TD>
    <TD>TS_S_TM_HC_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature producer side</TD>
    <TD>TS_S_TM_PS_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature producer side</TD>
    <TD>TS_S_TM_PS_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature to DHW storage</TD>
    <TD>TS_S_TM_HC_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature to DHW storage</TD>
    <TD>TS_S_TM_HC_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 1</TD>
    <TD>TS_S_TM_HWS_1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 2</TD>
    <TD>TS_S_TM_HWS_2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 3</TD>
    <TD>TS_S_TM_HWS_3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 1</TD>
    <TD>TS_S_TM_BT_1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 2</TD>
    <TD>TS_S_TM_BT_2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 3</TD>
    <TD>TS_S_TM_BT_3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 4</TD>
    <TD>TS_S_TM_BT_4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 5</TD>
    <TD>TS_S_TM_BT_5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 6</TD>
    <TD>TS_S_TM_BT_6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 7</TD>
    <TD>TS_S_TM_BT_7</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 8</TD>
    <TD>TS_S_TM_BT_8</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 9</TD>
    <TD>TS_S_TM_BT_9</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 10</TD>
    <TD>TS_S_TM_BT_10</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to DHW storage</TD>
    <TD>TS_S_FW_HC_HW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy in thermal storage</TD>
    <TD>TS_E_Storage_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy in DHW storage</TD>
    <TD>TS_E_Storage_HWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Total energy transfered to DHW storage</TD>
    <TD>TS_E_heat_toHWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>State of charge of the thermal storage</TD>
    <TD>TS_SOC_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>State of charge of the DHW storage</TD>
    <TD>TS_SOC_HWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature before mixing unit</TD>
    <TD>HS_S_TM_VL_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature after mixing unit</TD>
    <TD>HS_S_TM_VL_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature</TD>
    <TD>HS_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink flow temperature hot water</TD>
    <TD>HS_S_TM_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature hot water</TD>
    <TD>HS_S_TM_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature in the house</TD>
    <TD>HS_S_TM_Room</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow after mixing unit</TD>
    <TD>HS_S_FW_HC_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow before mixing unit</TD>
    <TD>HS_S_FW_HC_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow domestic hot water consumption</TD>
    <TD>HS_S_FW_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating power</TD>
    <TD>HS_P_DemHeatHC_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water power</TD>
    <TD>HS_P_DemHeatHW_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating Energy</TD>
    <TD>HS_E_DemHeatHC_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water energy</TD>
    <TD>HS_E_DemHeatHW_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - set temperature (power is constant)</TD>
    <TD>CBIn_TSet</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                      supply and return is constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP input - electric power</TD>
    <TD>CHPIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to district heating heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                      negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of domestic hot water pump to charge the DHW     
                    storage</TD>
    <TD>qvDHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXin</TD>
    <TD>&nbsp;</TD></TR></TBODY></TABLE></DIV>
<H2>Description:</H2>
<P>&nbsp;</P></BODY></HTML>
"),
		experiment(
			StopTime=1,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.001,
			__esi_SolverOptions(
				solver="CVODE",
				typename="ExternalCVODEOptionData"),
			__esi_MinInterval="9.999999999999999e-10",
			__esi_AbsTolerance="1e-6"));
end SF3;
