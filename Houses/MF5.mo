﻿// CP: 65001
// SimulationX Version: 4.2.3.69223
within CoSES_ProHMo.Houses;
model MF5 "House 5 with CHP and CB"
	GreenCity.Interfaces.Electrical.LV3Phase lV3Phase1 "Electrical Low-Voltage AC Three-Phase Connector" annotation(Placement(
		transformation(extent={{230,-385},{250,-365}}),
		iconTransformation(extent={{236.7,90},{256.7,110}})));
	Modelica.Blocks.Interfaces.RealInput UnixTime "UnixTime" annotation(Placement(
		transformation(extent={{510,-35},{470,5}}),
		iconTransformation(extent={{266.7,130},{226.7,170}})));
	GreenCity.Interfaces.Environment.EnvironmentConditions environmentConditions1 "Environment Conditions Connector" annotation(Placement(
		transformation(extent={{485,20},{505,40}}),
		iconTransformation(extent={{236.7,190},{256.7,210}})));
	Modelica.Blocks.Interfaces.BooleanInput StandardCBcontrol "If true, standard control will be used to control the condensing boiler" annotation(
		Placement(
			transformation(extent={{-95,-35},{-55,5}}),
			iconTransformation(
				origin={150,250},
				extent={{-20,-20},{20,20}},
				rotation=-90)),
		Dialog(
			group="Condensing Boiler",
			tab="Standard Control",
			visible=false));
	CoSES_ProHMo.Consumer.SimpleHeatedBuilding simpleHeatedBuilding1(
		redeclare replaceable parameter GreenCity.Utilities.BuildingData.ParameterSelectionSCB.AgeOfBuilding.To2001 buildingAge,
		nFloors=nFloors,
		nAp=nApartments,
		nPeople=nPeople,
		ALH=ALH,
		outline=true,
		livingTZoneInit(displayUnit="K")=TLiving_Init,
		UseStandardHeatNorm=UseStandardHeatNorm,
		QHeatNormLivingArea=QHeatNormLivingArea,
		n=n,
		TFlowHeatNorm(displayUnit="K")=TFlowHeatNorm,
		TReturnHeatNorm(displayUnit="K")=TReturnHeatNorm,
		TRef(displayUnit="K")=TRef,
		qvMaxLivingZone(displayUnit="m³/s")=qvMaxLivingZone,
		roofTZoneInit(displayUnit="K")=TRoof_Init,
		cellarTZoneInit(displayUnit="K")=TCellar_Init,
		UseIndividualPresence=UseIndividualPresence,
		PresenceFile=PresenceFile,
		Presence_WeeklyRepetition=true,
		UseIndividualElecConsumption=UseIndividualElecConsumption,
		ElConsumptionFile=ElConsumptionFile,
		ElConsumptionTable=ElConsumptionTable,
		ElConsumptionFactor=ElFactor,
		ElConsumption_YearlyRepetition=true,
		ActivateNightTimeReduction=ActivateNightTimeReduction,
		Tnight(displayUnit="K")=Tnight,
		NightTimeReductionStart(displayUnit="s")=NightTimeReductionStart,
		NightTimeReductionEnd(displayUnit="s")=NightTimeReductionEnd,
		VariableTemperatureProfile=VariableTemperatureProfile,
		TMin(displayUnit="K")=TMin,
		ceilingInsul=0,
		wallInsul=0,
		ceilingInsA=0.8,
		wallInsA=0.6,
		floorInsA=0.8) "Heated 3-zone-building with changeable heating system" annotation(Placement(transformation(extent={{375,-90},{415,-50}})));
	GreenCity.GreenBuilding.HeatingSystem.HeatingUnitFlowTemperature heatingUnitFlowTemperature1(qvMaxPump(displayUnit="l/min")=0.0005) annotation(Placement(transformation(extent={{295,-85},{330,-50}})));
	CoSES_ProHMo.Consumer.DHW_demand dHW_demand1(
		WeeklyData=WeeklyData,
		File=File,
		Table=Table,
		DHWfactor=DHWfactor) annotation(Placement(transformation(extent={{415,-150},{435,-130}})));
	GreenCity.Utilities.Electrical.Grid grid1(
		OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
		useA=true,
		useB=true,
		useC=true,
		useD=true,
		useE=true,
		useF=true) annotation(Placement(transformation(extent={{165,-280},{205,-240}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap1 annotation(Placement(transformation(extent={{275,-265},{285,-255}})));
	CoSES_ProHMo.Storage.HeatStorageCombined WolfSPU2_2000(
		VStorage=1.95,
		dStorage=1.1,
		QlossRate=7,
		LinearProfile=TSLinearProfile,
		TupInit(displayUnit="K")=TSTupInit,
		TlowInit(displayUnit="K")=TSTlowInit,
		TLayerVector(displayUnit="K")=TSTLayerVector,
		TMax(displayUnit="K")=Tmax_TS,
		alphaMedStatic=1,
		use1=true,
		iFlow1=9,
		iReturn1=2,
		use2=false,
		HE2=true,
		iFlow2=5,
		AHeatExchanger2=25,
		VHeatExchanger2=0.0165,
		use4=true,
		iFlow4=9,
		iReturn4=2,
		use5=true,
		iFlow5=9,
		iReturn5=2) "Heat storage with variable temperature profile" annotation(Placement(transformation(extent={{165,-179},{200,-105}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow defineVolumeFlow1 annotation(Placement(transformation(
		origin={210,-10},
		extent={{-10,-10},{10,10}},
		rotation=-90)));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow extractVolumeFlow1 annotation(Placement(transformation(
		origin={180,-10},
		extent={{10,-10},{-10,10}},
		rotation=-90)));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap5 annotation(Placement(transformation(extent={{210,-210},{220,-200}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap6 annotation(Placement(transformation(extent={{215,-225},{225,-215}})));
	CoSES_ProHMo.Distribution.HydraulicSwitch2HG hydraulicSwitch2HG1 annotation(Placement(transformation(extent={{150,-75},{220,-60}})));
	Modelica.Blocks.Sources.RealExpression CBControlIn(y=CBIn) annotation(Placement(transformation(extent={{-40,-40},{-20,-20}})));
	Modelica.Blocks.Sources.RealExpression CHPModulation(y=CHPin) annotation(Placement(transformation(extent={{-40,-110},{-20,-90}})));
	Modelica.Blocks.Sources.BooleanExpression CHP_EVU(y=(CHPEVU and not StandardCHPcontrol) or CHPon) "Set output signal to a time varying Boolean expression" annotation(Placement(transformation(extent={{-40,-65},{-20,-45}})));
	Modelica.Blocks.Sources.RealExpression TColdWater(y(quantity="Basics.Temp")=288.15) annotation(Placement(transformation(extent={{470,-165},{450,-145}})));
	parameter Real SOCminCB(quantity="Basics.RelMagnitude")=0.5 "SOC, at which the CB turns on (SOCmin < SOCmin and T<TStart)" annotation(Dialog(
		group="Condensing Boiler",
		tab="Standard Control"));
	parameter Real SOCmaxCB(quantity="Basics.RelMagnitude")=0.65 "SOC, at which the CB turns off" annotation(Dialog(
		group="Condensing Boiler",
		tab="Standard Control"));
	parameter Real TStartCB(quantity="Basics.Temp")=333.15 "Minimum Temperature, at the top, at which the CB turns on (T<TStart and SOCmin < SOCmin)" annotation(Dialog(
		group="Condensing Boiler",
		tab="Standard Control"));
	parameter Real TStopCB(quantity="Basics.Temp")=343.15 "Temperature at the bottom, at which the CB stops" annotation(Dialog(
		group="Condensing Boiler",
		tab="Standard Control"));
	protected
		Boolean CBon "Condensing boiler is switched on" annotation(
			HideResult=false,
			Dialog(
				group="Condensing Boiler",
				tab="Standard Control"));
	public
		Real CBinStandardControl "Standard control value of the CB modulation" annotation(Dialog(
			group="Condensing Boiler",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.BooleanInput StandardCHPcontrol "If true, standard control will be used to control the CHP" annotation(
			Placement(
				transformation(extent={{-95,-105},{-55,-65}}),
				iconTransformation(
					origin={50,250},
					extent={{-20,-20},{20,20}},
					rotation=-90)),
			Dialog(
				group="CHP",
				tab="Standard Control",
				visible=false));
		parameter Real SOCminCHP(quantity="Basics.RelMagnitude")=0.6 "SOC, at which the CHP turns on (SOCmin < SOCmin and T<TStart)" annotation(Dialog(
			group="CHP",
			tab="Standard Control"));
		parameter Real SOCmaxCHP(quantity="Basics.RelMagnitude")=0.9500000000000001 "SOC, at which the CHP turns off" annotation(Dialog(
			group="CHP",
			tab="Standard Control"));
		parameter Real TStartCHP(quantity="Basics.Temp")=338.15 "Minimum Temperature, at the top, at which the CHP turns on (T<TStart and SOCmin < SOCmin)" annotation(Dialog(
			group="CHP",
			tab="Standard Control"));
		parameter Real TStopCHP(quantity="Basics.Temp")=343.15 "Temperature at the bottom, at which the CHP stops" annotation(Dialog(
			group="CHP",
			tab="Standard Control"));
	protected
		Boolean CHPon "CHP is switched on" annotation(
			HideResult=false,
			Dialog(
				group="CHP",
				tab="Standard Control"));
	public
		Real CHPinStandardControl(quantity="Basics.RelMagnitude") "Standard control value of the CHP modulation" annotation(Dialog(
			group="CHP",
			tab="Standard Control",
			visible=false));
		Boolean CHPEVU "CHPEVU" annotation(Dialog(
			group="CHP",
			tab="Standard Control",
			visible=false));
		Modelica.Blocks.Interfaces.RealInput CBIn_TSet(quantity="Basics.Temp") if CBControlMode "Condensing boiler input - set temperature (power is constant)" annotation(
			Placement(
				transformation(extent={{-140,-40},{-100,0}}),
				iconTransformation(extent={{-270,-220},{-230,-180}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput CBIn_P(quantity="Basics.Power") if not CBControlMode "Condensing boiler input - heat power (temperature difference between supply and return is constant)" annotation(
			Placement(
				transformation(extent={{-140,-70},{-100,-30}}),
				iconTransformation(extent={{-270,-170},{-230,-130}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput CHPIn_P(quantity="Basics.Power") "CHP input - electric power" annotation(
			Placement(
				transformation(extent={{-140,-105},{-100,-65}}),
				iconTransformation(extent={{-270,-70},{-230,-30}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput TDH_HEXout(quantity="Basics.Temp") "Outlet temperature of district heating heat exchanger" annotation(
			Placement(
				transformation(extent={{-140,70},{-100,110}}),
				iconTransformation(extent={{266.7,-170},{226.7,-130}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput qv_HEX(quantity="Thermics.VolumeFlow") "Volume flow to district heating heat exchanger" annotation(
			Placement(
				transformation(extent={{-120,55},{-100,75}}),
				iconTransformation(extent={{236.7,-10},{256.7,10}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput qvDHpump(quantity="Thermics.VolumeFlow") "Reference volume flow of district heating pump - positive: feed in - negative: extraction" annotation(
			Placement(
				transformation(extent={{-140,-5},{-100,35}}),
				iconTransformation(extent={{266.7,-220},{226.7,-180}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput TDH_HEXin(quantity="Basics.Temp") "Inlet temperature of district heating heat exchanger" annotation(
			Placement(
				transformation(extent={{-120,30},{-100,50}}),
				iconTransformation(extent={{236.7,-60},{256.7,-40}})),
			Dialog(
				tab="Input Signals",
				visible=false));
	protected
		parameter Boolean CBControlMode=false "If true, flow temperature controlled, else power output is controlled" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
	public
		parameter Real CBDeltaT(quantity="Thermics.TempDiff")=20 "Temperature difference between flow and return temperature" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
	protected
		parameter Real CBPmax(quantity="Basics.Power")=50000 if not CBControlMode "Maximum power of CB" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
	public
		parameter Real CBTmax(quantity="Basics.Temp")=356.15 if CBControlMode "Maximum temperature of CB" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
	protected
		parameter Real CBPmin(quantity="Basics.Power")=10000 if not CBControlMode "Minimum power of CB" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
	public
		parameter Real CBTmin(quantity="Basics.Temp")=293.15 if CBControlMode "Minimum temperature of CB" annotation(Dialog(
			group="Condensing Boiler",
			tab="Heat Generator Parameters"));
		parameter Real CHPmax(quantity="Basics.Power")=5000 "Maximum electrical power of CHP" annotation(Dialog(
			group="CHP",
			tab="Heat Generator Parameters"));
		parameter Real CHPmin(quantity="Basics.Power")=2900 "Minimum electrical power of CHP" annotation(Dialog(
			group="CHP",
			tab="Heat Generator Parameters"));
		parameter Boolean CPC=true "If true, solar thermal collector is CPC collector, else, solar thermal collector is a flat plate collector" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Real alphaModule(
			quantity="Geometry.Angle",
			displayUnit="°")=0.6108652381980153 "Inclination angle of solar thermal collector" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Real betaModule(
			quantity="Geometry.Angle",
			displayUnit="°")=3.1415926535897931 "Orientation angle of solar thermal collector" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Integer nSeries=1 "Number of solar thermal collectors in series" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Integer nParallel=1 "Number of solar thermal collectors in parallel" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Real AModule(
			quantity="Geometry.Area",
			displayUnit="m²")=1.312 "Effective surface area of solar thermal collector" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Real VAbsorber(quantity="Geometry.Volume")=1.7 "Absorber volume" annotation(Dialog(
			group="Solar Thermal Collector",
			tab="Heat Generator Parameters"));
		parameter Real Tmax_TS(quantity="Basics.Temp")=353.15 "Maximum temperature within the thermal storage" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real T0_TS(quantity="Basics.Temp")=313.15 "Temperature level to calculate the stored energy (e.g. return temperature of consumption)" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Boolean TSLinearProfile=true "If true, the temperature profile at simulation begin within the storage is linear, if false the profile is defined by a temperature vector" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTupInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=353.15 if TSLinearProfile "Temperature of upmost heat storage layer at simulation begin" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTlowInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=313.14999999999998 if TSLinearProfile "Temperature of lowmost heat storage layer at simulation begin" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Real TSTLayerVector[20](quantity="Basics.Temp")={313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15} if not TSLinearProfile "Vector of temperature profile of the layers at simulation begin, element 1 is at lowest layer" annotation(Dialog(
			group="Thermal Storage",
			tab="Storage Parameters"));
		parameter Integer nPeople(quantity="Basics.Unitless")=6 "Number of people living in the building" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Integer nFloors=2 "nFloors" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Integer nApartments=1 "nApartments" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Real ALH(
			quantity="Geometry.Area",
			displayUnit="m²")=300 "Heated (living) area (e.g. 50m² per person)" annotation(Dialog(
			group="Size",
			tab="Consumption Parameters"));
		parameter Boolean UseStandardHeatNorm=false "If true, use standard area-specific heating power, else define it manually" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real QHeatNormLivingArea(quantity="Thermics.HeatFlowSurf")=22 if not UseStandardHeatNorm "Area-specific heating power - modern radiators: 14 - 15 W/m²; space heating: 15 W/m²" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real n=1.3 "Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TFlowHeatNorm(quantity="Basics.Temp")=333.15 "Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 - 45°C" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TReturnHeatNorm(quantity="Basics.Temp")=318.15 "Normal return temperature - radiator: 45 - 65°C; floor heating: 28 - 35°C" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TRef(quantity="Basics.Temp")=294.15 "Reference indoor temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real qvMaxLivingZone(quantity="Thermics.VolumeFlow")=0.00025 "Maximumg flow rate in Living Zone" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TLiving_Init(quantity="Basics.Temp")=294.15 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TRoof_Init(quantity="Basics.Temp")=274.65 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Real TCellar_Init(quantity="Basics.Temp")=280.15 "Initial Temperature" annotation(Dialog(
			group="Heating System",
			tab="Consumption Parameters"));
		parameter Boolean UseUnixTimeIndividual=false "If true, use unix time for individual presence and electric consumption" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean UseIndividualPresence=true "If the presence is used, individual presence data has to be provided, else standart presence is used" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String PresenceFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\MF5_WholeYear.txt" if UseIndividualPresence "File with presence timeseries (presence in %; 0% - no one is at home; 100% - everyone is at home)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean UseIndividualElecConsumption=true "If individual electricity consumption is used, individual consumption data ha to be provided, else standart load profiles are used" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String ElConsumptionFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\MF5_WholeYear.txt" if UseIndividualElecConsumption "File with electric consumption time series (consumption in kW)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter String ElConsumptionTable="Pel" if UseIndividualElecConsumption "Table with electric consumption time series (consumption in W)" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Real YearlyElecConsumption_kWh=6500 if UseIndividualElecConsumption "YearlyElecConsumption_kWh" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Real ElFactor=YearlyElecConsumption_kWh/5175 if UseIndividualElecConsumption "ElFactor" annotation(Dialog(
			group="Additional Yields/Losses",
			tab="Consumption Parameters"));
		parameter Boolean ActivateNightTimeReduction=false "If true, night time reduction is activated, else temperature is constant" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real NightTimeReductionStart(
			quantity="Basics.Time",
			displayUnit="h")=82800 if ActivateNightTimeReduction "NightTimeReductionStart" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real NightTimeReductionEnd(
			quantity="Basics.Time",
			displayUnit="h")=25200 if ActivateNightTimeReduction "NightTimeReductionEnd" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real Tnight(quantity="Basics.Temp")=291.15 if ActivateNightTimeReduction "Temperature at night" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Boolean VariableTemperatureProfile=false "If true, presence will be used to define the temperature (if less people are at home, less rooms are heated and the average temperature will decrease)" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Real TMin(quantity="Basics.Temp")=292.15 if VariableTemperatureProfile "Minimum temperature, when noone is at home (TRefSet = TMin + (TRef - TMin) * Presence(t))" annotation(Dialog(
			group="Temperature Control",
			tab="Consumption Parameters"));
		parameter Boolean UseUnixTimeDHW=false "If true, use unix time for DHW consumption" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Boolean WeeklyData=false "If true: DHW consumption data repeats weekly" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter String File=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\MF5_WholeYear.txt" "DHW Data File" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter String Table="V_DHW" "DHW Table Name" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real V_DHWperDay_l=nPeople*50 "V_DHWperDay_l" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
		parameter Real DHWfactor=V_DHWperDay_l/300 "Factor, with which the DHW consumption gets multiplied" annotation(Dialog(
			group="DHW consumption",
			tab="Consumption Parameters"));
	protected
		Real CBIn "Input Signal of Condensing Boiler" annotation(
			HideResult=false,
			Dialog(tab="Results CB"));
	public
		Real CB_S_TM_VL(quantity="Basics.Temp") "Condensing boiler flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results CB",
			visible=false));
		Real CB_S_TM_RL(quantity="Basics.Temp") "Condensing boiler return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results CB",
			visible=false));
		Real CB_S_FW_HC(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow condensing boiler" annotation(Dialog(
			group="Volume Flow",
			tab="Results CB",
			visible=false));
		Real CB_S_FG(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Gas flow condensing boiler" annotation(Dialog(
			group="Volume Flow",
			tab="Results CB",
			visible=false));
		Real CB_VFuel(
			quantity="Basics.Volume",
			displayUnit="l") "Demanded fuel volume" annotation(Dialog(
			group="Volume Flow",
			tab="Results CB",
			visible=false));
		Real CB_P_heat_is(quantity="Basics.Power") "Heat output power of condensing boiler" annotation(Dialog(
			group="Power",
			tab="Results CB",
			visible=false));
		Real CB_P_gas_is(quantity="Basics.Power") "Gas power of condensing boiler" annotation(Dialog(
			group="Power",
			tab="Results CB",
			visible=false));
		Real CB_E_heat_produced(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of condensing boiler" annotation(Dialog(
			group="Energy",
			tab="Results CB",
			visible=false));
		Real CB_E_gas_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Gas input of condensing boiler" annotation(Dialog(
			group="Energy",
			tab="Results CB",
			visible=false));
		Real CB_Efficiency(quantity="Basics.RelMagnitude") "Heating efficiency" annotation(Dialog(
			group="Efficiency",
			tab="Results CB",
			visible=false));
	protected
		Real CHPin "Input Signal of CHP" annotation(
			HideResult=false,
			Dialog(tab="Results CHP"));
	public
		Real CHP_S_TM_VL(quantity="Basics.Temp") "CHP flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results CHP",
			visible=false));
		Real CHP_S_TM_RL(quantity="Basics.Temp") "CHP return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results CHP",
			visible=false));
		Real CHP_S_FW_HC(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow CHP" annotation(Dialog(
			group="Volume Flow",
			tab="Results CHP",
			visible=false));
		Real CHP_S_FG(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Gas flow CHP" annotation(Dialog(
			group="Volume Flow",
			tab="Results CHP",
			visible=false));
		Real CHP_VFuel(
			quantity="Basics.Volume",
			displayUnit="l") "Demanded fuel volume" annotation(Dialog(
			group="Volume Flow",
			tab="Results CHP",
			visible=false));
		Real CHP_P_heat_is(quantity="Basics.Power") "Heat output power of CHP" annotation(Dialog(
			group="Power",
			tab="Results CHP",
			visible=false));
		Real CHP_P_el_is(quantity="Basics.Power") "Electric power output of CHP" annotation(Dialog(
			group="Power",
			tab="Results CHP",
			visible=false));
		Real CHP_P_gas_is(quantity="Basics.Power") "Gas power of CHP" annotation(Dialog(
			group="Power",
			tab="Results CHP",
			visible=false));
		Real CHP_E_heat_produced(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of CHP" annotation(Dialog(
			group="Energy",
			tab="Results CHP",
			visible=false));
		Real CHP_E_el_produced(
			quantity="Basics.Energy",
			displayUnit="kWh") "Electricity output of CHP" annotation(Dialog(
			group="Energy",
			tab="Results CHP",
			visible=false));
		Real CHP_E_gas_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Gas input of CHP" annotation(Dialog(
			group="Energy",
			tab="Results CHP",
			visible=false));
		Real CHP_Efficiency_th(quantity="Basics.RelMagnitude") "Thermal efficiency" annotation(Dialog(
			group="Efficiency",
			tab="Results CHP",
			visible=false));
		Real CHP_Efficiency_el(quantity="Basics.RelMagnitude") "Electric efficiency" annotation(Dialog(
			group="Efficiency",
			tab="Results CHP",
			visible=false));
		Real CHP_Efficiency_total(quantity="Basics.RelMagnitude") "Overall efficiency" annotation(Dialog(
			group="Efficiency",
			tab="Results CHP",
			visible=false));
		Real TS_S_TM_HC_VL(quantity="Basics.Temp") "Flow temperature consumption side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_RL(quantity="Basics.Temp") "Return temperature consumption side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_PS_VL(quantity="Basics.Temp") "Flow temperature producer side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_PS_RL(quantity="Basics.Temp") "Return temperature producer side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_1(quantity="Basics.Temp") "Thermal storage temperature 1" annotation(Dialog(
			group="Storage Temperatures",
			__esi_groupCollapsed=true,
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_2(quantity="Basics.Temp") "Thermal storage temperature 2" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_3(quantity="Basics.Temp") "Thermal storage temperature 3" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_4(quantity="Basics.Temp") "Thermal storage temperature 4" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_5(quantity="Basics.Temp") "Thermal storage temperature 5" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_6(quantity="Basics.Temp") "Thermal storage temperature 6" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_7(quantity="Basics.Temp") "Thermal storage temperature 7" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_8(quantity="Basics.Temp") "Thermal storage temperature 8" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_9(quantity="Basics.Temp") "Thermal storage temperature 9" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_10(quantity="Basics.Temp") "Thermal storage temperature 10" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_E_Storage_BT(
			quantity="Basics.Energy",
			displayUnit="kWh") "Energy in thermal storage" annotation(Dialog(
			group="Energy",
			tab="Results TS",
			visible=false));
		Real TS_SOC_BT(quantity="Basics.RelMagnitude") "State of charge of the thermal storage" annotation(Dialog(
			group="State of charge",
			tab="Results TS",
			visible=false));
		Real HS_S_TM_VL_bM(quantity="Basics.Temp") "Heat Sink flow temperature before mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_VL_aM(quantity="Basics.Temp") "Heat Sink flow temperature after mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_RL(quantity="Basics.Temp") "Heat sink return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_HW_VL(quantity="Basics.Temp") "Heat sink flow temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_HW_RL(quantity="Basics.Temp") "Heat sink return temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_TM_Room(quantity="Basics.Temp") "Temperature in the house" annotation(Dialog(
			group="Temperatures",
			tab="Results HS",
			visible=false));
		Real HS_S_FW_HC_aM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow after mixing unit" annotation(Dialog(
			group="Volume Flow",
			tab="Results HS",
			visible=false));
		Real HS_S_FW_HC_bM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow before mixing unit" annotation(Dialog(
			group="Volume Flow",
			tab="Results HS",
			visible=false));
		Real HS_S_FW_HW_VL(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow domestic hot water consumption" annotation(Dialog(
			group="Volume Flow",
			tab="Results HS",
			visible=false));
		Real HS_P_DemHeatHC_is(quantity="Basics.Power") "Heating power" annotation(Dialog(
			group="Power",
			tab="Results HS",
			visible=false));
		Real HS_P_DemHeatHW_is(quantity="Basics.Power") "Domestic hot water power" annotation(Dialog(
			group="Power",
			tab="Results HS",
			visible=false));
		Real HS_E_DemHeatHC_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heating Energy" annotation(Dialog(
			group="Energy",
			tab="Results HS",
			visible=false));
		Real HS_E_DemHeatHW_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Domestic hot water energy" annotation(Dialog(
			group="Energy",
			tab="Results HS",
			visible=false));
		Modelica.Blocks.Logical.Switch switch3 annotation(Placement(transformation(extent={{-5,-95},{15,-75}})));
		Modelica.Blocks.Sources.RealExpression CHPin_StandardControl(y=CHPinStandardControl) annotation(Placement(transformation(extent={{-40,-80},{-20,-60}})));
		Modelica.Blocks.Logical.Switch switch4 annotation(Placement(transformation(extent={{-5,-25},{15,-5}})));
		Modelica.Blocks.Sources.RealExpression CBin_StandardControl(y=CBinStandardControl) annotation(Placement(transformation(extent={{-40,-10},{-20,10}})));
		CoSES_ProHMo.Generator.DigitalTwins.NeoTower5_GC neoTower5_GC1 "Green city of the CHP model based on measurements in the laboratory" annotation(Placement(transformation(extent={{35,-110},{80,-65}})));
		CoSES_ProHMo.Generator.DigitalTwins.WolfCGB50_GC wolfCGB50_GC1 "WolfCGB50 validated with CoSES mearusements" annotation(Placement(transformation(extent={{90,-40},{135,5}})));
		GreenCity.Utilities.Thermal.MeasureThermal measureThermal1 annotation(Placement(transformation(extent={{340,-130},{350,-140}})));
		GreenCity.Local.Photovoltaic photovoltaic1 annotation(Placement(transformation(extent={{435,-330},{395,-290}})));
		GreenCity.Local.Controller.PV2ACInverter pV2ACInverter1 annotation(Placement(transformation(extent={{385,-290},{345,-330}})));
		GreenCity.Utilities.Electrical.Grid grid2(
			OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
			useA=true,
			useD=true) annotation(Placement(transformation(extent={{220,-345},{260,-305}})));
	equation
		// Input Conversion
		// CB
		if CBControlMode then
			CBIn = min(max(0, 10*((CBIn_TSet-CBTmin)/(CBTmax-CBTmin))), 10);
		else
			CBIn = min(max(0, 2 + 8 * ((CBIn_P - CBPmin)/(CBPmax - CBPmin))), 10);
		end if;
		
		//CHP
		if CHPIn_P > 0 then
			CHPin = min(max(CHPmin / CHPmax, (CHPmin / CHPmax + (CHPIn_P - CHPmin) / CHPmax)), 1);
			CHPEVU = true;
		else
			CHPin = 0;
			CHPEVU = false;
		end if;
		
		// Controller
		// CHP
		when ((TS_SOC_BT < SOCminCHP) and (WolfSPU2_2000.TStorage[8] < TStartCHP) and StandardCHPcontrol) then
			CHPon = true;
		elsewhen ((TS_SOC_BT > SOCmaxCHP) or (WolfSPU2_2000.TStorage[3] > TStopCHP) or not StandardCHPcontrol) then
			CHPon = false;
		end when;
		if CHPon then
			CHPinStandardControl = 1;
		else
			CHPinStandardControl = 0;
		end if;
		
		// Condensing boiler
		when ((TS_SOC_BT < SOCminCB) and (TS_S_TM_BT_8 < TStartCB) and StandardCBcontrol) then
			CBon = true;
		elsewhen ((TS_SOC_BT > SOCmaxCB) or (TS_S_TM_BT_3 > TStopCB) or (not StandardCBcontrol)) then
			CBon = false;
		end when;
		if CBon then
			CBinStandardControl = 10;
		else
			CBinStandardControl = 0;
		end if;
		
		// Results
		// Condensing Boiler
		CB_S_TM_VL = wolfCGB50_GC1.TFlow;
		CB_S_TM_RL = wolfCGB50_GC1.TReturn;
		CB_S_FW_HC = wolfCGB50_GC1.qv;
		CB_S_FG = wolfCGB50_GC1.qvFuel;
		CB_VFuel = wolfCGB50_GC1.VFuel;
		CB_P_heat_is = wolfCGB50_GC1.QHeat;
		CB_P_gas_is = wolfCGB50_GC1.PFuel;
		CB_E_heat_produced = wolfCGB50_GC1.EHeat;
		CB_E_gas_consumed = wolfCGB50_GC1.EFuel;
		CB_Efficiency = wolfCGB50_GC1.Efficiency;
		
		// CHP
		CHP_S_TM_VL = neoTower5_GC1.TFlow;
		CHP_S_TM_RL = neoTower5_GC1.TReturn;
		CHP_S_FW_HC = neoTower5_GC1.qv;
		CHP_S_FG = neoTower5_GC1.qvFuel;
		CHP_VFuel = neoTower5_GC1.VFuel;
		CHP_P_heat_is = neoTower5_GC1.QHeat;
		CHP_P_el_is = neoTower5_GC1.PGEN;
		CHP_P_gas_is = neoTower5_GC1.PFuel;
		CHP_E_heat_produced = neoTower5_GC1.EHeat;
		CHP_E_el_produced = neoTower5_GC1.EGEN;
		CHP_E_gas_consumed = neoTower5_GC1.EFuel;
		CHP_Efficiency_th = neoTower5_GC1.HeatEfficiency;
		CHP_Efficiency_el = neoTower5_GC1.ElEfficiency;
		CHP_Efficiency_total = CHP_Efficiency_th + CHP_Efficiency_el;
		
		//Thermal Storage
		TS_S_TM_HC_VL = hydraulicSwitch2HG1.TSupplyHC;
		TS_S_TM_HC_RL = hydraulicSwitch2HG1.TReturnHC;
		TS_S_TM_PS_VL = (hydraulicSwitch2HG1.TSupplyHG1+hydraulicSwitch2HG1.TSupplyHG2)/2;
		TS_S_TM_PS_RL = (hydraulicSwitch2HG1.TReturnHG1+hydraulicSwitch2HG1.TReturnHG2)/2;
		TS_S_TM_BT_1 = WolfSPU2_2000.TStorage[1];
		TS_S_TM_BT_2 = WolfSPU2_2000.TStorage[2];
		TS_S_TM_BT_3 = WolfSPU2_2000.TStorage[3];
		TS_S_TM_BT_4 = WolfSPU2_2000.TStorage[4];
		TS_S_TM_BT_5 = WolfSPU2_2000.TStorage[5];
		TS_S_TM_BT_6 = WolfSPU2_2000.TStorage[6];
		TS_S_TM_BT_7 = WolfSPU2_2000.TStorage[7];
		TS_S_TM_BT_8 = WolfSPU2_2000.TStorage[8];
		TS_S_TM_BT_9 = WolfSPU2_2000.TStorage[9];
		TS_S_TM_BT_10 = WolfSPU2_2000.TStorage[10];
		TS_E_Storage_BT = WolfSPU2_2000.cpMed*WolfSPU2_2000.rhoMed*WolfSPU2_2000.VStorage*((WolfSPU2_2000.TLayer[1]+WolfSPU2_2000.TLayer[2]+WolfSPU2_2000.TLayer[3]+WolfSPU2_2000.TLayer[4]+WolfSPU2_2000.TLayer[5]+WolfSPU2_2000.TLayer[6]+WolfSPU2_2000.TLayer[7]+WolfSPU2_2000.TLayer[8]+WolfSPU2_2000.TLayer[9]+WolfSPU2_2000.TLayer[10])/10-T0_TS);
		TS_SOC_BT = TS_E_Storage_BT/(WolfSPU2_2000.cpMed*WolfSPU2_2000.rhoMed*WolfSPU2_2000.VStorage*(Tmax_TS - T0_TS));
		
		// Heat Sink (Heating)
		HS_S_TM_VL_bM = heatingUnitFlowTemperature1.FlowSupply.T;
		HS_S_TM_VL_aM = heatingUnitFlowTemperature1.FlowSink.T;
		HS_S_TM_RL = heatingUnitFlowTemperature1.ReturnSupply.T;
		HS_S_TM_Room = simpleHeatedBuilding1.TZone[2];
		HS_S_FW_HC_aM = heatingUnitFlowTemperature1.FlowSink.qv;
		HS_S_FW_HC_bM = heatingUnitFlowTemperature1.FlowSupply.qv;
		HS_P_DemHeatHC_is = simpleHeatedBuilding1.QHeat;
		HS_E_DemHeatHC_consumed = simpleHeatedBuilding1.EHeat;
		HS_S_TM_Room = simpleHeatedBuilding1.TZone[2];
		
		// Heat Sink (Domestic Hot Water)
		HS_S_TM_HW_VL = dHW_demand1.FlowHotWater.T;
		HS_S_TM_HW_RL = dHW_demand1.ReturnHotWater.T;
		HS_S_FW_HW_VL = dHW_demand1.qv_DHW;
		HS_P_DemHeatHW_is = dHW_demand1.Q_DHW;
		HS_E_DemHeatHW_consumed = dHW_demand1.E_DHW;
	initial equation
		// enter your equations here
		
		// CHP
		if ((TS_SOC_BT < SOCminCHP) and (WolfSPU2_2000.TStorage[8] < TStartCHP) and StandardCHPcontrol) then
			CHPon = true;
		else
			CHPon = false;
		end if;
		
		// CB
		if ((TS_SOC_BT < SOCminCB) and (TS_S_TM_BT_8 < TStartCB) and StandardCBcontrol) then
			CBon = true;
		else
			CBon = false;
		end if;
	equation
		connect(phaseTap1.Grid1,heatingUnitFlowTemperature1.Grid1) annotation(Line(
			points={{285,-260},{290,-260},{299.3,-260},{299.3,-90},{299.3,-85}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid1.LVGridE,phaseTap1.Grid3) annotation(Line(
			points={{205,-260},{210,-260},{270,-260},{275,-260}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid1.LVGridF,simpleHeatedBuilding1.Grid3) annotation(Line(
			points={{205,-275},{210,-275},{405,-275},{405,-95},{405,-90}},
			color={247,148,29},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.T_Ref,heatingUnitFlowTemperature1.TRef) annotation(
			Line(
				points={{385,-50},{385,-40},{308,-40},{308,-50}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(simpleHeatedBuilding1.qv_Ref,heatingUnitFlowTemperature1.qvRef) annotation(Line(
			points={{380,-50},{380,-45},{321.3,-45},{321.3,-50}},
			color={0,0,127},
			thickness=0.0625));
		connect(heatingUnitFlowTemperature1.ReturnSink,simpleHeatedBuilding1.PipeOut) annotation(Line(
			points={{330,-76.3},{335,-76.3},{370,-76.3},{370,-75},{375,-75}},
			color={190,30,45},
			thickness=0.0625));
		connect(heatingUnitFlowTemperature1.FlowSink,simpleHeatedBuilding1.PipeIn) annotation(Line(
			points={{330,-58.7},{335,-58.7},{370,-58.7},{370,-60},{375,-60}},
			color={190,30,45}));
		connect(simpleHeatedBuilding1.EnvironmentConditions,environmentConditions1) annotation(Line(
			points={{400,-50},{400,-45},{400,30},{490,30},{495,30}},
			color={192,192,192},
			thickness=0.0625));
		connect(environmentConditions1,dHW_demand1.EnvironmentConditions) annotation(Line(
			points={{495,30},{490,30},{439.7,30},{439.7,-135},{434.7,-135}},
			color={192,192,192},
			thickness=0.0625));
		connect(hydraulicSwitch2HG1.HeatExchangerIn,extractVolumeFlow1.Pipe) annotation(Line(
			points={{180,-60},{180,-55},{180,-25},{180,-20}},
			color={190,30,45}));
		connect(hydraulicSwitch2HG1.HeatExchangerOut,defineVolumeFlow1.Pipe) annotation(Line(
			points={{190,-60},{190,-55},{190,-25},{210,-25},{210,-20}},
			color={190,30,45},
			thickness=0.0625));
		connect(hydraulicSwitch2HG1.qvDistrictHeating,qvDHpump) annotation(Line(
			points={{170,-60},{170,-55},{170,15},{-115,15},{-120,15}},
			color={0,0,127},
			thickness=0.0625));
		connect(phaseTap5.Grid1,hydraulicSwitch2HG1.FeedInPumpOut) annotation(Line(
			points={{220,-205},{225,-205},{225,-55},{210,-55},{210,-60}},
			color={247,148,29},
			thickness=0.0625));
		connect(hydraulicSwitch2HG1.ExtractionPumpOut,phaseTap6.Grid1) annotation(
			Line(
				points={{205,-60},{205,-50},{230,-50},{230,-220},{225,-220}},
				color={247,148,29},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(TDH_HEXout,defineVolumeFlow1.TMedium) annotation(Line(
			points={{-120,90},{-115,90},{215,90},{215,5},{215,0}},
			color={0,0,127},
			thickness=0.0625));
		connect(extractVolumeFlow1.qvMedium,defineVolumeFlow1.qvMedium) annotation(Line(
			points={{185,0},{185,5},{205,5},{205,0}},
			color={0,0,127},
			thickness=0.0625));
		connect(extractVolumeFlow1.TMedium,TDH_HEXin) annotation(Line(
			points={{175,0},{175,5},{175,40},{-105,40},{-110,40}},
			color={0,0,127},
			thickness=0.0625));
		connect(qv_HEX,extractVolumeFlow1.qvMedium) annotation(Line(
			points={{-110,65},{-105,65},{185,65},{185,5},{185,0}},
			color={0,0,127},
			thickness=0.0625));
		connect(TColdWater.y,dHW_demand1.TColdWater) annotation(Line(
			points={{449,-155},{444,-155},{430,-155},{430,-154.7},{430,-149.7}},
			color={0,0,127},
			thickness=0.0625));
		connect(CHPModulation.y,switch3.u3) annotation(Line(
			points={{-19,-100},{-14,-100},{-12,-100},{-12,-93},{-7,-93}},
			color={0,0,127},
			thickness=0.0625));
		connect(CBControlIn.y,switch4.u3) annotation(Line(
			points={{-19,-30},{-14,-30},{-12,-30},{-12,-23},{-7,-23}},
			color={0,0,127},
			thickness=0.0625));
		connect(CBin_StandardControl.y,switch4.u1) annotation(Line(
			points={{-19,0},{-14,0},{-12,0},{-12,-7},{-7,-7}},
			color={0,0,127},
			thickness=0.0625));
		connect(CHPin_StandardControl.y,switch3.u1) annotation(Line(
			points={{-19,-70},{-14,-70},{-12,-70},{-12,-77},{-7,-77}},
			color={0,0,127},
			thickness=0.0625));
		connect(switch3.u2,StandardCHPcontrol) annotation(Line(
			points={{-7,-85},{-12,-85},{-70,-85},{-75,-85}},
			color={255,0,255},
			thickness=0.0625));
		connect(switch4.u2,StandardCBcontrol) annotation(Line(
			points={{-7,-15},{-12,-15},{-70,-15},{-75,-15}},
			color={255,0,255},
			thickness=0.0625));
		connect(hydraulicSwitch2HG1.DemandFlow,heatingUnitFlowTemperature1.FlowSupply) annotation(Line(
			points={{219.7,-65},{224.7,-65},{290,-65},{290,-58.7},{295,-58.7}},
			color={190,30,45}));
		connect(hydraulicSwitch2HG1.DemandReturn,heatingUnitFlowTemperature1.ReturnSupply) annotation(Line(
			points={{219.7,-70},{224.7,-70},{290,-70},{290,-76.3},{295,-76.3}},
			color={190,30,45},
			thickness=0.0625));
		connect(CHP_EVU.y,neoTower5_GC1.CHPOn) annotation(Line(
			points={{-19,-55},{-14,-55},{30,-55},{30,-70},{35,-70}},
			color={255,0,255},
			thickness=0.0625));
		connect(switch3.y,neoTower5_GC1.CHPModulation) annotation(Line(
			points={{16,-85},{21,-85},{30,-85},{30,-80},{35,-80}},
			color={0,0,127},
			thickness=0.0625));
		connect(neoTower5_GC1.Return,hydraulicSwitch2HG1.HeatGeneratorReturn1) annotation(Line(
			points={{79.7,-90},{84.7,-90},{145,-90},{145,-70},{150,-70}},
			color={190,30,45},
			thickness=0.0625));
		connect(hydraulicSwitch2HG1.HeatGeneratorSupply1,neoTower5_GC1.Flow) annotation(Line(
			points={{150,-65},{145,-65},{84.7,-65},{84.7,-80},{79.7,-80}},
			color={190,30,45},
			thickness=0.0625));
		connect(neoTower5_GC1.environmentConditions,environmentConditions1) annotation(Line(
			points={{70,-65},{70,-60},{70,30},{490,30},{495,30}},
			color={192,192,192},
			thickness=0.0625));
		connect(wolfCGB50_GC1.Return,hydraulicSwitch2HG1.HeatGeneratorReturn2) annotation(Line(
			points={{134.7,-20},{139.7,-20},{155,-20},{155,-55},{155,-60}},
			color={190,30,45},
			thickness=0.0625));
		connect(hydraulicSwitch2HG1.HeatGeneratorSupply2,wolfCGB50_GC1.Flow) annotation(Line(
			points={{160,-60},{160,-55},{160,-10},{139.7,-10},{134.7,-10}},
			color={190,30,45},
			thickness=0.0625));
		connect(switch4.y,wolfCGB50_GC1.ControlIn) annotation(Line(
			points={{16,-15},{21,-15},{85,-15},{90,-15}},
			color={0,0,127},
			thickness=0.0625));
		connect(wolfCGB50_GC1.lV3Phase,grid1.LVGridB) annotation(Line(
			points={{125,-39.7},{125,-44.7},{125,-260},{160,-260},{165,-260}},
			color={247,148,29},
			thickness=0.0625));
		connect(wolfCGB50_GC1.environmentConditions,environmentConditions1) annotation(Line(
			points={{125,5},{125,10},{125,30},{490,30},{495,30}},
			color={192,192,192},
			thickness=0.0625));
		connect(measureThermal1.TMedium,dHW_demand1.TPipe) annotation(Line(
			points={{345,-130},{345,-125},{430,-125},{430,-130}},
			color={0,0,127},
			thickness=0.0625));
		connect(measureThermal1.PipeOut,dHW_demand1.FlowHotWater) annotation(Line(
			points={{350,-135},{355,-135},{410,-135},{415,-135}},
			color={190,30,45}));
		connect(grid1.LVGridD,phaseTap6.Grid3) annotation(Line(
			points={{205,-245},{210,-245},{210,-220},{215,-220}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid1.LVGridA,phaseTap5.Grid3) annotation(Line(
			points={{165,-245},{160,-245},{160,-205},{205,-205},{210,-205}},
			color={247,148,29},
			thickness=0.0625));
		connect(neoTower5_GC1.lV3Phase,grid1.LVGridC) annotation(Line(
			points={{70,-109.7},{70,-114.7},{70,-275},{160,-275},{165,-275}},
			color={247,148,29},
			thickness=0.0625));
		connect(dHW_demand1.ReturnHotWater,WolfSPU2_2000.ReturnIn5) annotation(Line(
			points={{415,-145},{410,-145},{205,-145},{205,-145.3},{200,-145.3}},
			color={190,30,45}));
		connect(WolfSPU2_2000.FlowOut5,measureThermal1.PipeIn) annotation(Line(
			points={{200,-135.3},{205,-135.3},{335,-135.3},{335,-135},{340,-135}},
			color={190,30,45}));
		connect(hydraulicSwitch2HG1.StorageDischargeReturn,WolfSPU2_2000.ReturnIn4) annotation(Line(
			points={{215,-74.7},{215,-79.7},{215,-120},{205,-120},{200,-120}},
			color={190,30,45}));
		connect(WolfSPU2_2000.FlowOut4,hydraulicSwitch2HG1.StorageDischargeFlow) annotation(Line(
			points={{200,-110},{205,-110},{210,-110},{210,-79.7},{210,-74.7}},
			color={190,30,45}));
		connect(hydraulicSwitch2HG1.StorageChargeReturn,WolfSPU2_2000.ReturnOut1) annotation(Line(
			points={{155,-74.7},{155,-79.7},{155,-120},{160,-120},{165,-120}},
			color={190,30,45},
			thickness=0.0625));
		connect(WolfSPU2_2000.FlowIn1,hydraulicSwitch2HG1.StorageChargeFlow) annotation(Line(
			points={{165,-110},{160,-110},{160,-79.7},{160,-74.7}},
			color={190,30,45},
			thickness=0.0625));
		connect(pV2ACInverter1.MPP,photovoltaic1.MPP) annotation(Line(
			points={{365,-330},{365,-335},{415,-335},{415,-330}},
			color={0,0,127},
			thickness=0.0625));
		connect(photovoltaic1.DC,pV2ACInverter1.DCPV) annotation(Line(
			points={{395,-310},{390,-310},{385,-310}},
			color={247,148,29},
			thickness=0.0625));
		connect(environmentConditions1,photovoltaic1.EnvironmentConditions) annotation(Line(
			points={{495,30},{490,30},{440,30},{440,-310},{435,-310}},
			color={192,192,192},
			thickness=0.0625));
		connect(grid2.LVMastGrid,lV3Phase1) annotation(Line(
			points={{240,-345},{240,-350},{245,-350},{245,-375},{240,-375}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid2.LVGridD,pV2ACInverter1.LVGrid3) annotation(Line(
			points={{260,-310},{265,-310},{340,-310},{345,-310}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid2.LVGridA,grid1.LVMastGrid) annotation(Line(
			points={{220,-310},{215,-310},{185,-310},{185,-285},{185,-280}},
			color={247,148,29},
			thickness=0.0625));
		connect(UnixTime,simpleHeatedBuilding1.UnixTime) annotation(Line(
			points={{490,-15},{485,-15},{405,-15},{405,-45},{405,-50}},
			color={0,0,127},
			thickness=0.0625));
		connect(dHW_demand1.UnixTime,UnixTime) annotation(
			Line(
				points={{434.6666870117188,-145},{439.7,-145},{445,-145},{445,-15},{490,-15}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
	annotation(
		__esi_neoTower2(
			Return(viewinfo[0](
				tabGroupAlignment=589834,
				typename="ModelInfo")),
			Flow(viewinfo[0](
				tabGroupAlignment=589834,
				typename="ModelInfo")),
			cHP_DZ1(
				ReturnCHP(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				FlowCHP(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				FromReturn(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
				ToFlow(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
			measureThermal1(
				PipeOut(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				PipeIn(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				FromPipe(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
				ToPipe(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
			measureThermal2(
				PipeOut(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				PipeIn(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
				FromPipe(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
				ToPipe(Pipe(viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))))),
		__esi_hydraulicSwitch2HG1(viewinfo[0](
			fMin=1,
			fMax=100,
			nf=100,
			kindSweep=1,
			expDataFormat=0,
			expNumberFormat="%.7lg",
			expMatrixNames={
							"A","B","C","D","E"},
			typename="AnaLinSysInfo")),
		Icon(
			coordinateSystem(extent={{-250,-250},{250,250}}),
			graphics={
							Text(
								textString="MF5",
								fontName="TUM Neue Helvetica 55 Regular",
								textStyle={
									TextStyle.Bold},
								extent={{-252.5,269.8},{253.7,97.3}}),
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAALEAAACSCAYAAAAKEStdAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAScQAAEnEB89x6jgAAHMJJREFUeF7tnQlYVdX6xkEUUDTAeR5QEdBSTEtxyNR/Dlk55JQ3JTU1
MUtvNjgUTqUmmTmHqDkPgAOIosggms12bby3eu70NF0rTcUBrfe/38Xeh81hH9gHGc6wvuf5PRz2
Wmtz9ubda31rrW+t7QFp0pzcpIjLyH766SdcvHhR/U1aWZoUcRnY1q1bcf/992PIkCFITU1Vj0or
K5MiLkXLycnBypUrERQUBA8PD1SpUgXh4eHYs2cP/vzzTzWXtNI2KeJSsu+//x7Tpk1Do0aN4Onp
iaFDh6J///6oVKkSQkJCsGzZMvz+++9qbmmlaVLEpWCffPKJEKy/vz/8/PwwZcoUpKenIzk5GcOG
DUPlypXRsGFDTJw4Ef/85z/VUtJKy6SIb9MOHz4sXAZvb280adIECxcuxOnTp3HmzBlBWlqaqKFr
1aqFGjVq4IEHHsDHH3+slpZWGiZFfBv21ltvoU2bNsJlCA0NxaZNm4Rw3333XWRnZwu0z0uWLBG1
McV+zz33ID4+Xj2LtNs1KeIS2IULFzBz5kzh/1LArF0TEhLw3nvviVpYE7AGj506dQqxsbGi1qZ7
QfG/8cYbuHXrlnpWaSU1KWI77auvvsLYsWNRs2ZNIUb6uceOHRMCplCtBazBNNbSrIEfeughIX66
H7Nnz8YPP/ygnl1aSUyK2A7LyMhAt27dROctMDAQ0dHROHnypHAZihKwhibklJQUREZGwtfXV/jK
I0eOxLlz59S/Is1ekyI2YRzj3bZtG8LCwkTtS/+X48GsffX+rxkoZJbJysrCnDlzhJ9MMXfv3l2k
S7PfpIiLsWvXrokRh6ZNmwoX4L777sPu3buLdR+Kg0ImK1asQOvWrcXD0b59e/GwSLPPpIiLMPqq
48ePF00+RTZ48GAxjXy7AtbQhuJ27NiBiIgIeHl5oVmzZliwYIH6DaSZMSliG/bBBx+gb9++qFat
GqpXr45nnnlG+L8UXWkIWA8fCvrJI0aMEFPVderUwdSpU9VvIq04kyI2MLoLbdu2FTVj48aNsXz5
ciFee/1fs2gdvszMTMyYMUN0HMmDgwap30haUSZFrLPc3FysWbMGLVq0EPEPHTt2xPbt20WtXNq1
rxF8SOhivPrqq8Kt4EPE73DgwAH1G0ozMili1RjAwwmMunXrCgEPHDhQiKe8BKxBEfNvcvaPM3v8
LsHBwYjdGKt+U2nWJkWs2DfffINHH31UDHVxWpgTGBwTppiMhFbW8KH58MMPxSxgnz59RFgnO5cv
vPCC+o2l6c2tRczxX/qhXbt2FcNn9H85gUH/lJ0tI4GVF5qQGUA0btw4ETzk4+OD0aNHq99emmZu
K2L6v1u2bLEE8DDmd9WqVUI4ZdWBKwnvv/++cDGef/551K5dWwz19erVCydOnFCvRJrbifiPP/4Q
ATzz589HvXr1hCi0CYyPPvpICMZITBUJHyq2DDExMeJhY4ePoyc7d+5Ur8q9za1EzIgxBvBMnjxZ
xD6weebY7NGjR0UNXJ4dOHvhw8XvyBk9PnQcT27ZsqWY8XN3cxsR37hxQ/i/HHWoWrWqaJqffvpp
sQKjov1fs2h+cmJiIh5++GExEcNouunTp+Pvf/+7eqXuZy4vYnberl69in379okxV9ZgDOB57bXX
LM20kWAcGa3D99RTT6FBgwbimoYPHy6Ou6O5tIjp//7vf//D6tWrRQeO7kPPnj2xceNG0WFypA6c
vWgdPvr2fCjp2zMSLikpSb169zGXFfHNmzfF+O+sWbNELALjH+hKcPk8ayxH7MDZC4cCeS1cJtWp
UyfxkHJiZNPmTepdcA9zSREzfJITFWPGjBH+L5vcCRMmOEUHzl54LRxV4UhFv379RMwFV4wwEu67
775T74hrm8uJ+PLlyzh06JDYgYczcKyZGHzOCDQ2wUZCcHa0Dh8j4fjgstMaEBAgHlx38JNdRsT0
f8+fP4/NmzfjzjvvFDUwm1gOQVG8bHqNBOBK8Do52sJIuFatWokOH/fDOH78uHqXXNNcQsT0f7/9
9lvRyWFTyila/vPeeecd4Va4gv9rFq3DunTpUjEaQz+5S5cu2LtvH3777Tf1jrmWOb2Ir1+/LnxC
DjcxSIZNKeML6FKUdwSao0AR041Yt26dGI1hp7Z58+Zixu/f//mPeudcx5xaxPR/2VQ+8sgjosZh
E8qBfy4h4j/R6B/sLpxWHl7uNLR3716xlRYfcG6z9dxzz7ncymqnFDEnMH755RcRsM4INHbg7r77
bixatEj8A121A2eWM++exrun81ogPszcF2PixIkiSo+1MqfaGWrqKuZ0Imb8w3+UJpEdNq7A4JBS
7969sWHDBos/aP1PdTfeP/OuELL2O2clOTrDjVratWsnWq0ePXoIl8sVdup0KhHT/z179qxoErkC
g00kVyDv2rVLNJ3u6P8akVcTF+zMajHSnBjhymqO3nBLrbdjY/G9k+9A5DQiZvwDh4+4AoNNIteg
TZo0SYyNVtQKDGeDrRTvFeOoOTHCsWRu3jJ33jz84x//UO+085nDi5j+L+N/9+/fL5bQ0//lJiNz
584Vu+i4u/9rL2yt2GodPHhQrBihn8xamXsqs6Z2RnNoEdP//e9//ytWIHfo0EGEHtKX4woM3nB3
mMAoK9jh4+oQrhjhrCbXFjK2hPstc9remcxhRcwJjK+//losjuQEBoPYBw0aJCYwZAeudGBFQLhF
QOfOnYWQ6Sdv275NjP44izmkiBnAzlk2Dgtx9IEBPE888YToTXNiQ3bgSg/NT+beydxnma0d953j
jJ+zvJrB4UTMDhz3exgwYICoGdjUcT8IBoG7+wRGWaEFELHfwYkRznoSbqXF95E4+kbgDiNibQKD
Aets0jiWee+994rXBLBWZrNn9A+QlB6skRmuGhUVJV5jxgAivgWKEyOOLGSHEDEj0Bj7unjxYuH/
srfMTkZcXJy4sdL/LT94vzkxwtlPToxwZTW3COBiAraSjmgVLmI+4WzK6P9qExjcOZ2LIXlc+r/l
D0d92HnmKBBHg1gj063jLCnDXR3NKlTEFDCHefgOC/q/DODhFqoM4JHjvxULXThtxQhf78vxeVYw
L774otj2wJGswkTMsUjuocBQQS5yZAA7VyC78goMZ4StIWdFOTrEllILIGKao1iFiJgrkNlh4zb/
bKroc9H/ZTMmJzAcD3aquWfHSy+9JHYg4k6d3OiQEyPcDqyirdxF/MUXX4jeL5smPtXcL4FbSPGJ
d6cVGM4GhcwONgOI+AYpCplbz65duxYXL15U/7sVY+UqYg7VMOqMw2dcRk//98iRI0LARjdO4lhQ
xHT1GDXIACKOIjEQi37yv/71L/W/XP5WLiLmFDJrW/Z0uQMlN/t4+eWXxY2R47/OhTYxwgkp+sna
ihG+oJLDcxVhZS7in3/+Wbz+lWOObILYkWMTxKda+r/OCwXLpWEMtGckHEeXOMtaETsQlamI2cTw
DfO8SLoQnP3hoDlrX+n/Oj9aJcROurbPHWdZ2Unn+sfysjITMYXKoZg77rhDNDkTJ04UU5r0qeQE
huvAyoitKoVLP5kzfNz3jrOvfA9KeViZiJgv4eYQDJsYroObN2+eCOCR47+uCYXMWpn/d668oY/c
qFEjsfLm888/V1VRdlaqIubc+vr160UADycwGKPKHSm1lxga3QCJa8DWla0vJ0a4bQJdSLbCdCG5
AqcsrdRETP+Xe55x6phNCgPYOWXJpkb6v+4BhczKiqEEdCc4mcVhOO6Lx2E5jlKVhZWKiNlTffzx
x8W0JLeQioyMFEMwfDKl/+t+sOJi68tlZdwXhG7lXXfdJXYg4nrJ0rbbFjGfOjr02gpkTk1ysw4+
kVLA7gtbX8KXrzOAiDUy+0dcbsZ980rTSixiBvBwvRv3MODwGceBX3/9deH/8Ek0ujCJe8FKjFrg
sjL9XnkMtWUnv7SsRCJmAM8rr7wihlLYVNDn4Wtc+cWl/yvRowlZmxihn8x1k5wYYYvNFT23a3aL
mJtsjB8/XrwDTntzD8d/ZQdOUhSaNvRxF5wY4QLVnJwcVV0lM7tEzAAevnqKQyf169cXEU3cF41j
gdL/lRQFO3rs6HMfEQqa48mskfkuPsbR/Pjjj6rK7DdTIuYaOAbw0P9lhD83MuETxU2buTrjs88+
E1/S6MtLJIT6YGtNsXJLBlZ8fCmQtrKarTt1VBIrVsS//vorli9fjrCwMDE3zqVE7LxxYoP+DAUu
RSwpDk3EP6ibF7Ly42e25lwczNEtzi0w+N5eK1LEnMDgng+cfWENzB4mBauP5pcilpjBWsQ0VoIM
qGdQGAPsqTG29vydO6CaNZsi5qYZ9Fs4ecEdeOi3cFm99f4DUsQSMxiJWDMO1zKdrTxbe7b6rKHN
vmPEUMQc+uDG1Rz/5Qn5RiIu1aZgrU2KWGKGokRMY+vOipOzvRw44OwvvQAzK0YKiJgONwXLKUI+
ERzL4x/nk2JrPE+KWGKG4kRMYytP0UZHR4t9k7kv3KhRo/Dpp5+qOYzNImKuwGBh+r8szBf5UZzF
BW1IEUvMYEbENOrp0qVLYjaYXgAn07gannuRWLuymllEzDluLsemiLmFEcfzjNwHa5MilpjBrIg1
o3vBkQpurM6oSK7hs7WbvUXEFOLKlSuF4q9cuWJ6OlCKWGIGe0VMo7Y4nsyYHI5Y2HpJjkXEFC2r
azO1r96kiCVmKImIaWZ0WaBjVxKTIpaYoaQiNmNSxJJyQYpYUiGcOW18vCRIEUucHiliSYXx59kV
hsftRYpYUmH8kdjL8Li9SBFLKoybCfcZHrcXKWJJhfDpe2m4mXgfrqZNM0y3ByliSYVwM6G7Qg/c
UjBKtwcpYoVnn30W9/X/yDS8YUbnkZjjVPZJ3IrvJrgR39Uwjz1IESsMG/OhECfX+hUH8xmdQ2Ke
3L1dcHNfhOCWglEee5AiVtBEbJRmjRTx7ZFzYDBu7emKmxYi8NW529vRVIpYIbzTKCFOPROmfiDS
rI8T6/ISc5z/SunM7b4XNxRyNfbcY5jXHqSIVbhsSoNCHfGXE+I4P0+K2lsg3bqsxBw3d3bCrZ2d
kav8zKejYV57kCI2wFrEUc8kFsojsY/cHeEF2X43bm7viGvKT6P89iBFrHAi/STS07MtULjDRh+z
fJ40dU+BdD3l/YA5I7nb2uP61g64sbW9jnDcUI4b5bcXtxYxl6hQpKWB0fndnTOnMpG7+U7kvmNE
W1w/NsWwnL24tYhLS4A8R3JysmGau/Jdxtu4sTkU1zeF4MamsELkKhiVKwluK+LSrEFL6zyuwpfJ
S3FtYxvciLXN1bjOhmVLgluKuDQFTLRzPf3cp5Zzk9TjZwvldQeubWiF6xta2uSGglG5kuJSItYL
iGjHBw+JtJlmiz4D95nOr6Xr82tY53V1rq5pXizX1rYwLFtSXE7E23eeRkbGKYuAuLcxP0+bHiPS
CC/Yuqw1mggJX35jlEdD+1tafuvP7sJ7753BtZUtcO0tRahFcPWtZoblSW72QsPjReFyIrb+fPRo
vqDNcv+ALIsIzZTV8ujzmy3rSnx5fAuuvqGItDhibIv4etJU/Ja9xjDNFlLEBmgCNFtOn18ro//s
LnyxfxmuLg0yxaWYdobnyFnWTElvbphmC5cWsR59vqLQl+ErpYzymMHev+sKfHE4FlcXtTRJEM4e
2Vqg/NdpO9S0Vvjmo8wCaUUhRWxFScoYURrncEauzm8lyIluaeHK/Nbi59XoIFyObmOVFoxb55Jx
fXkX5LwSlHcsOhg/vznI8PxGuKyINexxJzThGWGUvyhKWs7ZuTKndR6zW1nImd0al+e0UVA+zwku
lHaFx5Qy4rM43ho/xTxseH4jZE1sg/c/vGC67LBRGyx5ExISxDGzZV2NnOdDkDNLEeTzSo2rcuX5
YFyd1QZnTmbg0gtKui7NFr9k7TA8vxFSxDbQyvHVvEbpevR/h+iPWed1B67MUIQ7U8eMYFxbOVqk
/bh2SsE0I5TyOQv6FDqvLVxaxPxZktEJwjKDhpqLh2BebtzMn9rf0n92Ny5HheBKVKgF/q5PvxKl
CNWSFopL0wrmv6Sk/zD/kQJlikKK2ADeELNl+BJA5v3b3/4mfmrl9J/dhc93vIlTWZm4GDMelyaF
5DO5YLDPL2/PsqTlpsYp6bq8In9ogfzF4fIipjtgr5jsyc+8Wn7tsx7r/K7O75Gh+HzTEvHz0hN5
XJ7zUKF8WtqlZ7rj38un5v+ucHG8wrrnCpWxhcuJ2EhA1sf1aUZs3LjR8Lg1eefKn5L+8ssvMejR
Dy3o87oTvz/WDhf/ogjysTDlc1tcGl3Qnfhu1TwlTTmucP7JnmqZtrigHiOXH1OEPHt4gXK2cCkR
m6UoIR84cMDwuDVmHgZ35ctta3FxiCLCoYqIFS4MLege5Kx7WaSRr2JfF8fyfs8rI34OCcPX0wrX
4Ea4pYjJ7YhQCrh43j1+FBcebINfh4TjtPL7hUEFhfzbwDYiXfv9gvL77wp5x0Pwaf+2OKXLXxRu
K2K+BIdCnDAlxTDdFlLA9nGhZzAu9G5rmKbn4n3BuNizDXLmPY1Pe4Xh87EDDPMZ4bYiJqvXpVlE
aQ9G55LY5qt7Q/DDI7a3cf33F5/ho8R48flsRKhCfg1tBrcWMeEMW0Tvo4ZitWbOy+ZnkSQF+fDR
BxUXo/g9Oz4Jt0/AxO1FLHEcPl4YbXi8OKSIywH5EJYtUsQSp0eKWOL0OL2IDxzYiMXTh2PY0HCE
h3dAhw4K4eHoOOxRjHjmNWw8YFwuO+MI4tfMxEDmF3RFv2EzsPawQV5yfA9WzY5EH0v+Xhg9fSm2
p6rpR7dg4ZTh6GlJL4IBA/D4C0uw5fAxZFj/HScjJWU3Vs5/Ck+M7oaIiILXOPDJF7HsnRQczzAu
m31gCUZ16YyO+ntjg06dOmH4qDGYFbMZh6zO47wiVsT77IjOaBvaAg1r+cGvmgc8PHRUU47VaoQW
YR1x76iZiDtoVT49CdsXj0SQpYwfmrYfgddsif7oO1j0RC80sOSvg3uGPY8NKWp68hpMH9ABNS3p
ReDrizvqNkJQSF+MWbAPh47r/o6zoIh32cxH0bNzKzRtEAj/Gl7w8ip4jb4BddEoKBRh/cbi1U3J
SMu0OsfeWehczQeV9PfGBp6envCrXgN1GrfBPX3G441D+edxThHzCQ5vgzp+lZSLM75oC8rFe1Wv
i9BOj2GpXsgVKWILPqhRryeiViXgqK3ayhFJicXs0X0QrFQe3nrh2sK3Buo3640ZGw7imF7Idog4
H09Urt4EncbGWGpk5xOxIuCR4S3g76UIWLuwoO4YMX0x4uLi8ti4GAujuqOF7sIrVa6FNvfMQJx2
njIXcV10GTEDi9eq30klZt409GhSD1Ut+XwQ+PAc7Dx0vPDfdESEgHuhRXVfeGnXUD8MPUbNwNI3
16nXGYOYOYMR0eAO+Oius2bjkVickIJ07VyFRNwSo5cUvF9xcbHYsHo2JnTR8ih43oFG7Z5CrHoe
pxNxbFRb1GcNrF1Q0HAs2rYPSanpljzZ2ek4cSQeWxdEoZuWz6MSfANbYsgSNcCnzEXcCA9ELceO
Y7pzKGSmHUXiupno26IufLW8fv0xd/tBpOnyOSq7lzyOXsF6AXfD4/NWYVdyKjIytf9TBjLSDiFh
TTRGNK6H6lpej+roPG09ko6rK5kLibgdntld8O+Rk1lHsO/tJ9HFks8XdZs/hEVqy+pcIj7wKh4K
CoSPxYUIwsjXE3Asy8rdUMlMPYiY4UFqXsWv8quFlhPX5aVXkIgFmWsx6a7mCLDkDcG4FXtxxNpn
dDSUWvi5RzqhfhXte9dH9ycWYlNyBk4a5c9Mw9YZfdEk0EfN7wHv/nORkKIu+TIp4uzsEzgcPwd9
Lfmqo0GrSKxS75dTiXj/ggfROMAnvxbu+Rzij9i4gUQpdyIlIf/tR3v2Iv5wWl5aRYo4ewOmtm+B
QEveVhizfA9SHNwvTln/VwwKr5svuraD8cLqfcg4aZyfZB47hPh9e/L/BwdTkalVOqZEnIX01Dgs
HBZoccG8Axqi98xtOKHmcSIR78f8gc0Q4OOpXrAHWo1fh9QTWboLtoNCIlY6DN7VEFCnPurXN6Bu
bQTo/cDbEfHB1zAstBH8LHm7Y8bmRByzzudQpGDdXx9GeN1K6nf2QIM+UxCz/ahBXpMUEnEV1Kht
fe/roV7dmvD3zUuv5n8vRj4bi2TNJVFwIhGvwwTFj/TTjUb837yDtscgi6OQiO2lZCI+uHk5RnVs
g5qVvfJblK7TEJd41HaL4hDsxMJRPRFUSbs+D7QfMRsbEo1dOVMUEnFxeMOnejf0fXId9uvO40Qi
Xo3IpnVQTXdR/aOTCo89mqXMRVwJ3lWr446AQAQG5uNfww8++pEVj+YYumArkk7chhjKhe2YP7yH
bsTHA+Gj5uJtW+6XGewWsSc8PavA278p2g1bZBGyFLHlfH5orPh4L29PQUqKAfFrMWdMd9Sz5C9O
xGZojojJsdh1OMvBa2FSHiIOxeS4wvc+6eA+rPhrL7TS8nl6olr9YAxdtF+cx4lEvA+z728Af2/t
gj0QNnkjjqWXlk9cjpMdzZqhy+jpiNm1H0cyFAEX0TFyHJLx1tMDcGfN/Oto0i8KK3bexnv9THXs
yEmkp8Rj+ZhWlr/tUbUOWj+8QNTGTiTik1g1rglq6qaXPQfMx5G0InZPzErHkY1TcKe3N7wFAWjU
cgLWMq3MRdwQfSe/hi3JJ8S2AQVIT0dGprOIN59t84cjorl2fcr97zga0bHFLKxNXIBBrRuihuV/
0B/ztAkP0yJWyExF4puP59fGHv5oEjoZ65U0JxKx8mVXjUPDmtXUi1DwDMOUTcdsDvFkpR9D7KTQ
fP+zWi00Gbcqr+kucxEXNcTmpGybjyERzdXrU/Bsgv7TV2BXEdeYGD0Izev45pfp82L+MKedIt6/
chxaW/IqIg6Z5Hwizs6OV1yKeqhhGWxXUJ7GLWkGY8VZGUiNm4wQy0V7iICgyNVquhRxCVBcimn9
0E7nUng06YdnV+40HB48mRCNB4N0M5MKfV/ag5QTah47RMyJq5Xjgi3ncWIRK8TPwf31/OFtuRiF
0AF4avFWpLOZzshA+onj2LxgQAEBe3j7o0GfOUjQziNFXDKSVyHqgbtQS+lcWe5tk7sxMGopdiYe
Efef7F0zBQ80zZ+g4MiCV8g4rNp/FJnauQqJuC2mbc8rryctNQlrX+yjq4UVV8a/MdpOWS/O43wi
JoqQe9ULgI9uzLIoKlWujnpNI7FGfw4p4pKT/Bam/l971K5sIopQwdOrMrx9HsBLuw4VjA8pJGJz
eHoFomnYFFEL8zzOKWISvxpje9aFfw1f+FTxKhjLSryUY1V8UD2gNnpF6mpgDSni2yNpG+ZFdkOr
BlXh610Zlb04hqtdu0KlSkrl4Q2fqn64s18k1h44XHgBgF0iVmpyryrw8a+DxgOmYEFi/nmcV8Qq
8fGr8MKY7ujaNQD+/v55BAQgsGsEevzlJayONy6XnZ6Mncsi0V4r498QbSPGYrl18LxG6jYsfWog
gi35g3D/mLmIO6KmH16PWcO6obklPQSPzHgTu11VxCpJSe/g1ZnD8WDvxmjUSLt2hbvuQvvBkzFv
QxJS043Liha1QR0EWu6ZbWrXro9efcZj3rpEJFqdx+lFLJFIEUucHiliidMjRSxxepxGxFLIEls4
vIjPnTsnRSwpkqysLMcW8bfffouzZ89KJDb55JNPRGV3/vx5VTmlZ7ctYtrNmzeRm5srkRQLK73S
tlIRsTRpFWlSxNKc3qSIpTm9SRFLc3ID/h/8ZagvdxaFgwAAAABJRU5ErkJggg==",
								extent={{-216.5,-232.6},{230.2,116.9}}),
							Text(
								textString="DZ",
								fontSize=14,
								textStyle={
									TextStyle.Bold},
								lineColor={0,128,255},
								extent={{164.7,-196.8},{274.7,-260.2}})}),
		Documentation(info="<HTML><HEAD>
<META http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"> 
<TITLE>House 1 with ST, CHP and CB</TITLE> 
<STYLE type=\"text/css\">
table>tbody>tr:hover,th{background-color:#edf1f3}html{font-size:90%}body,table{font-size:1rem}body{font-family:'Open Sans',Arial,sans-serif;color:#465e70}h1,h2{margin:1em 0 .6em;font-weight:600}p{margin-top:.3em;margin-bottom:.2em}table{border-collapse:collapse;margin:1em 0;width:100%;}tr{border:1px solid #beccd3}td,th{padding:.3em 1em; border-width: 1px; border-style: solid; border-color: rgb(190, 204, 211);}td{word-break:break-word}tr:nth-child(even){background-color:#fafafa}th{font-family:'Open Sans Semibold',Arial,sans-serif;font-weight:700;text-align:left;word-break:keep-all}.lib-wrap{width:70%}@media screen and (max-width:800px){.lib-wrap{width:100%}}td p{margin:.2em}
</STYLE>
    
<META name=\"GENERATOR\" content=\"MSHTML 11.00.10570.1001\"></HEAD> 
<BODY>
<H1>House 1 with ST, CHP and CB</H1>
<HR>

<P>This model is a digital twin of the House 1 in the CoSES laboratory (<A href=\"https://doi.org/10.1109/PESGM41954.2020.9281442\">https://doi.org/10.1109/PESGM41954.2020.9281442</A>)</P>
<P><BR></P>
<P>The house consists of the following components and default conditions:</P>
<UL>
  <LI>Combined Heat and Power unit (CHP): neoTower 2.0, nominal electric and 
  thermal power: 2 kW_el / 5       kW_th</LI>
  <LI>Condensing Boiler: Wolf CGB14, nominal thermal power: 14 kW_th</LI>
  <LI>Solar Thermal emulator of variable size, up to 9 kW_th</LI>
  <LI>Thermal Storage: Wolf SPU-2 800,&nbsp;content: 750 l&nbsp;</LI>
  <LI>Domestic Hot Water (DHW)&nbsp;preparation: Wolf SEM-1 500,&nbsp;DHW 
  storage, connected to the thermal storage and solar thermal unit, content: 500 
        l</LI>
  <LI>Consumption - House parameters:<BR>Construction year: 1995 to       
  2002<BR>Additional insulation: roof + floor<BR>Number of floors: 2<BR>Number   
      of apartements: 1<BR>Number of inhabitants: 6<BR>Living area:   
  300m²<BR>Heating     system: radiators (supply / return temperatur: 60 / 45   
  °C)</LI></UL>
<P>Four boolean inputs can activate a predefined standard control for simple  
 simulation results or during a lead time of the simulation. The standard  
control  of each element is as follows:</P>
<UL>
  <LI>CHP:<BR>Switched on at 100 % modulation, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; 
      T_Storage,top &lt; T_Start,CHP and SOC_Storage &lt; SOCminCHP<BR>Switched  
   off,   when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; T_Storage,bottom &gt; T_Stop,CHP or  
     SOC_Storage &gt; SOCmaxCHP<BR><BR></LI>
  <LI>Condensing Boiler:<BR>The condensing boiler should only operate, if the    
   CHP cannot provide the required heat.<BR>Switched on at 100 % modulation,     
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; T_Storage,top &lt; T_Start,CB and     
  SOC_Storage &lt; SOCminCB<BR>Switched off, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;   
    T_Storage,bottom &gt; T_Stop,CB or SOC_Storage &gt; SOCmaxCB<BR><BR></LI>
  <LI>Solar Thermal pump:<BR>The control is&nbsp;based on Green City's   
  'SolarController'<BR>Solar thermal pump switched on,   
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_DHWStorage,in) &gt;   
  deltaTonST<BR>Solar thermal pump switched off,   
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_DHWStorage,in)&nbsp;&lt;   
  deltaToffST<BR>The pump is controlled with the following   
  conditions:<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &lt;   
  deltaTFlowReturnLow, the reference volume flow is at  minimum   
  level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &gt;   
  deltaTFlowReturnMax, the reference volume flow is at  maximum   
  level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; Between the two temperature  boundaries the 
    reference volume flow is linearly  interpolated.<BR><BR></LI>
  <LI>Domestic Hot Water pump:<BR>If the solar thermal yield isn't enough to   
  heat the domestic hot water storage, the DHW pump can charge it from the   
  thermal storage.<BR>Switched on at qvMaxDHW, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; 
      T_DHWStorage,top &lt; T_onTS_DHW and T_Storage,top &gt;   
  T_offTS_DHW<BR>Switched off,   when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;   
  T_DHWStorage,top &gt; ToffTS_DHW or T_Storage,top &lt; TDHWStorage,top</LI></UL>
<P><BR></P>
<DIV class=\"lib-wrap\">
<TABLE class=\"type\">
  <TBODY>
  <TR>
    <TH>Symbol:</TH>
    <TD colspan=\"3\"><IMG width=\"330\" height=\"254\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUoAAAD+CAYAAABcBUzSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAEgtSURBVHhe7Z0HeBVV2scjJXQVxIIgShPWgqwVy4oFEXftoigq6q4FO0jsjSaIiCC9hE6AUJXQAoTk9pZCtezqp67rYncVEBHh/533zMzNzNy5JeHW5P0/z/skd+bMOXNn5vzu+542WWClpT7P7Y2cYvVDLVFxThaysnRW2y4AK23FoExXfZ6L3rUIFPTD0Dv3c/WTIqttLFYqxKBMUxEkDN6VajUWHMU5DEpW2opByUobcejNSlcxKFksFiuKGJRpKsvOHIvwtKbI8vsmUOGaNjTjkJ+lF4My7VSMHIuKq1mNjUZrWecVK7PEoExT1TYPK1z57Nmx0kEMSlatF3cisaKJQZm2sg7B2cOKr8iTNV9Tq22s2i0GZZoq2aG3XqnwsCy/bzI6r3j8JisGMSjTVZ/nIicFlTX5HlbqO6849GZFE4MyTUVwMlRe1Wqqh0VlMJ9Y6SoGJStEtcnDYkCzYhGDkpUmSlHnFY/fZMUgBmUaK206VZKgVJZruMaqJRzQrIwSgzJNRRU4FW2FopDUeFgp6rxisWIRgzJdlcJOlVR4WOzZsdJZDMo0Fg9bSbwI0CGXtQYvPsKqnhiUrFqqWrr4CKtaYlCmqyy8GvIwE12BU+lh1abOK1ZmiUGZpko+sFLrYdH3NX83q20sVirEoExXUe9z71xUYuJz5PZOkUeZDKWo8yrcDwQDmqUXgzKdJeBRmyovh96sdBWDkmVSLfOwePwmKwYxKNNM5OEQmFLl5aRT6J2szivzjwIZh94svRiUaSpjGJoj/LwkKUUeliWgk9TbzmJFE4MyI1QZDicaHCnzsFLUecVixSIGZVpLgUXSYJVqpajzKhWdSKzMEoMyzWT06HqDI8/Eiq63GchW21i1WwzKtJO+1zmJbZM6JdvD0peXdGcuZeM3WZkkBmVayzRUx9CGlxgl3cOicDtIR2pqSL4XzaE3K5oYlBmhyrbKhHs6SfawKG8Dl0T5zClWuolBmaYyejnJ9bKS6WGlHJQWPwz0/RnWLL0YlGkmAgfBqbZUVO37hrNEe9AhoCZZwJNVu8WgZBlV2zwsHr/JikEMyjQTe1gpkPh+ybzGrMwTgzKtVYycJPR0G5RkDyvVPwwsVixiUKa1kgdKgyeZMg8rBT8MLFYMYlCmtVIEypQpmaAUZWk/BvTF9T8ODGuWSQzKtBaDMlEK9aArZ0HRPg75WXoxKNNMVEmDno2FJaoCp6pco1L1w2Aqt6Z3XrGqLAYlS8oIjuSpdv8wsDJFDEqWVKpAyWJlghiUaSnjOpTJ8HAYlCxWeDEo005Kb6wZWjJUrIkki9AemNAZQakql5WRYlCmmyJU4GR7fdriGIn0ZiN+pwR2qqSqXFZmikGZZkolOPQeK0FSKyuhgI70nRIJrFSVy8pIMSjTTGnhYYlyDGF+QsGhtMeG5E/nkNAV3lNVLisTxaBMM0nPTteJY7bEAUtpG1XK0YMiOavpaGF+0JI0njJV5bIySwxKFovFiiIGZZopVR4ll2u0xHnurEwUgzJdRcudmeLd4pwktJ1xuckpl5VRYlCmqcjjCWkXTGiniiIuVygJ5bIySwzKdBX1vpo6FpIyEJrLTU65rIwSgzKdRZVY33aWrNrL5ao7WCxFDEoWi8WKIgZlmsqy7SwJ4nJZrFAxKNNVFr2xSRGXy2KFiEGZpiJPx9BuployeoG5XB5HyTKKQclisVhRxKBksVisKGJQpq30i1QkMyTkcpNTLiuTxKBMU1Hbmaysus6Gz3NzkOj6y+Ump1xWZolBmaaiCqzUW+HxaDNHinOUSp1Acbn0b+LLZWWWGJTpKvJw1IpLlVkJCXsn3tPhcpNTLiujxKBksVisKGJQppkqvRprS1RIyOUajUNvll4MynSVrnNBE68LmQClqlxWRolBmaYij8dUf5PSycDlCnFnDsskBmW6SlRWXheS16NkpYcYlOksqsT6trNk1V4uV93BYiliULJYLFYUMShZLBYrihiUaatUzUHmcpNTLiuTxKBMU1n2xiZBXC6LFSoGZbrq81zkpMKr4XJZrBAxKNNU5OmYw8FkhIRcbnLKZWWWGJQsFosVRQxKFovFiiIGZdqKe59rdrmsTBKDMk1FbWeysuoWbeCVxuOvVJXLyiwxKNNUVIGVeis8Hm0ucnGOUqkTKC6X/k18uazMEoMyXUUejlpxqTIrISGvNB53papcVkaJQclisVhRxKBksVisKGJQpqkq2850SmqbnU5cLquWi0GZdrIerqJZSKWOm7hcvSWuXFYmikGZprL0dJIgLpfFChWDksVisaKIQZkBone4UDiY7HYzLpfFUsSgTDNRKKh/ZwtVXq3iJjJM5HKTUy4rM8WgTDMZKmlxjvFFVwnsjeVyk1MuKzPFoEw76Xtj9S/i/xy5vZPVC8zlJq5cViaKQZlMnSWMrnisRunjIS43ssWrXFaNFT0mrGTpIWH1hJkrqpVROkofD3G54S2e5bJqrOhRYSVLu4UdLcxcWa2M0lH6eIjLDW/xLJdVY0WPCiuZisXbSYSXw+WGWiLKZdVI0ePCSqZi8XYS4eVwuaGWiHJZNVL0uLCSrUjeTiK9HC43OeWyapzokWElW5G8nUR6OVxucspl1TjRI8NKhay8nWR4OVxucspl1SjRY8NKhay8nWR4OVxucspl1SjRY8NKlfTeTnW9HJp+F5xhYm0hs0yilht5rcbq2rrL9xz5962O4nGdWbVaDMpUSu/tVMnLqS7I1JdmRS03MaC8a+yXVf++9PIvc17a2xJjVbWvM4uliEGZapF3U1f9G4O0pcCOyMjFjFhuYkApF5qo4ve19JirCkpSVctlsXRiUKZa5N2crP6NorhAUrWTrszDH2HLTSAoq/B9tQUqQvKqDiirVC6LZRSDMkNEy4KFAONILewSOQkEZRUU9jtXB5Qs1hGIQZkJsmqn01s44EXt6An3ov8IoEwSpCJ6zwxKVpLFoMwARfImY1k3MRJ0rL281IEypuYFBiUryWJQpr3CtNORxUJJqaqCL1mgjPDdIhmDkpVkMSjTXvEAZQRPjUHJYkUVgzLtlYowmEF5pDp06BC+/PJLlJaW4tNPP8WBAwfUPaxMFIMy7RUNJvr3vcRLDMojEUHR7XbDbrejpKREmsvlwr59+9QUrEwTgzIDFPv4yXhBk0F5JNq5cydsNlsQkpp5vV41BSvTxKDMBMUwn9vaqgvOVIT7Fgr3vdMclE6nMwSSZA6Hg0PwDBWDMkMUu1cZweLRS14dq0Knk0EZCkpql7QCJQGUlZmKAkqLCmN46Gm/yWuhwdHqg0yV25hcPPiGh9zieFYYVTNMtbKooGFQHol+/vln2SZphuTXX3+tpmBlmmIApR5kamUNPviRQUkDpfUDmhWvSJdel5YVm+LiWaoWnl8MyiMVwdLv90tIEjS/++47dU+8lClOBjEjdAaYeRJFdR+REBmYor9G5vOo2vXLUh46cyb0mTKxyoy2aWkt9utP1HDSSp654gJpFyUIUjVdcfDi6c7FBFPzMbkaOCjTYMUKPd4y74wVXUv1ex+hWT+gdF+t01fLaiEoWZrMbKHbKu5h3MhokokXlQo9j6pIBSXlr/P+qDD5RaypWxlSh6lQwRPVgEv/qnnSw69epOIc9cRpnzguWD6l0fIwfXEDKMUxhvNQ8w35LuHyrhE6UqiF3t8jz9Nk6n2psmoyKPXfTbs+6rNe+aNO98ZcwbU6paub6nHSadCujeHa6e6xZRmV26M6HqQqnbuaTliw3mrnaKWqnrfcpW0TPMjV569dI4vzCO5TFaXcICjlger/VLDy/U2ZqTKC0rTfdCG0tIY85X7dsSEXL/w+AyiD25ULod0z+aW1D5HyrpGi71f5UMRiwR+RoCLkYbiWCZbh4U3ROSRCYeqI3C6+n3Y/aDv9H/LDLxOb6og4Lvj8y8+Vz7gEib4+WJRhzEO9/+oxIeVX4dzFDgPoDXmZVe3zrjyGthuYFtxnPA/DvhjK1YGSCqGMRIY5VgVp0m+z2E8Z6x9kFVpB71E74WKRTn8y+mP0+Zr2BS+0YTuDMpzkTZc3OoIFL5wmukYW6cgM1zLBqqGgtLonoc+0UPA5FvdD3U7HKrcrhjoSVPi0wTJirE9VP3cjoELPrVLVOe+QY2h7MJ3ueNN56PfFUq4BlFRI75wc4b5qW/QFkZQLWJmpeb+Q+QtRGnGC+m3yxMS24I2gY8QF1z7T/uB5yX16yFrdmPA3NmLetUjyl1b3cBss5HrQfbVIR5bMa0f3MdXnkACFVkxV5rqje44jOjGm42Kp+CFlxFifqn7ulJcOULRdn06n6px3yDGG/HXHm89Dty+Wco2glAnMmRkf0uDFk9KfiCrzF5InqIerEKWx6HAJto+Y8qys5AKu4oGReRnKCX9jrfK+oNd8zJlToexPa4Vef82MNzYWKdfIKi/z9Y5ULoMyDpLPv/maCxmeaSH9cyz+D+vEmI8z5U8gMNeHkDIM26PUpyqduxlQ6nMYzFxIpFWSVuO86a/umISE3iI/EyhTJPNFiKcs8s7Keh1Nm76Bli1Hpzkw4wussF5lSF4MykRLVsbgd9JVWP13C8JGfhD3JEJFN18T/bXT7wtXhmF7BFAKVfXctfSVP+4qLLU8dHlX+bzlv5V55Ygfk8rjdNdIyHgexn3Ryq21oNQs3YEZKWTWP1/RZXo49RaSEYOSxdKrVk5h1IMy3YFp/PU2m97DiKxI+YQCl0HJYunFoDRZ+gEzArSCpgshzCKP2vIYzayOZVCyWHoxKMNYWgEzHDDiYNbhO4OSxdKLQRnF6tUbiu7dZ6pHplAJgGX4nnMGJYulF4MygpFX2adPPj777Cf1yBQrahgdu0XuCGJQslh6MSgtLO0AaVLkDp4oFlNXOYOSxdKLQamzdAeklWKCZpXBwqBksfRiUGYoIFksVvJUq0HJgGSxWLGoVoKSAclisaqiWglKFovFqooYlCwWixVFDMoq6PDhw2xslsaq2WJQsmLWL7/8gpkzZ2LZsmX4448/1K0sBmXNF4OSFZP+/e9/4/7770edOnXQtGlTvPrqqxKcLAZlbRCDkhVVbrcbV155JerVq4fbb78d9BqP+vXr47777sOnn36qpqq9sgTlH7/h4NZc/LakNw66R+Dwvm/VHaxMFIOSFVH5+flo164dmjRpgkGDBsHn86G4uBh9+/ZFdnY2Lr30UtjtdjV17VQIKAUkf8vrgd+mdsD+Sadg/0Rhs7ri8I+fqAlYmSYGJctSBw4cwJgxY9CsWTO0atUK48aNQ2lpKbxeL/x+v/Qyn3vuOQnQU089FYsWLVKPrH0yg/Kge5SAZEcFkjr7bf6lagpWpolByQrRt99+i8cff1y2R5577rlYsGABKioqJBzJe3Q4HEFgjh07Fm3btsUxxxyDt956C/v371dzMer3vftwcN+v6qeaJTMoKdw2Q1KCUniYh3/7WU3FyiQxKFkGbd26FX/7299w1FFH4frrr8f69etRXl4Op9MpAak3AmdZWRny8vIkUKnd8tFHH8V//vMfNTdF3+38CO/1fxDvP/gwvv/wY3VrzZEZlL9veMQalNM6qSlYmaaEgdK4qg292yXMy61iWvaLlQxt2LABZ555Jho0aIABAwbA5XIhEAiEAFJvBFAKydesWYMbbrgBdevWxTXXXINt27bJPA/u2Ye1jzyNV8W9HprVEPOu6IUvSpxyX02RGZTUFvlb7jlGSM7ogoPb5qgpWJmmxIAyylsVCaJVfy81K1GiMZHTp09Hy5YtpY0cOVICkjpurOBoZeRZUlhOgKXe8a5du6Jw40Zg/+/YMvglDD2qIYZnNcIwYVP/1A07FizC4YM1YyxmSGeO0KHdZfhtzvlBT/KPXfo2XFrGTnlXEb1lM8RXoOXl5EbjcneVdca8DJ763qNEvs1Uk1z6LsI7mmqoYgal3kOsfHeu+aXi9FlcRLqYVQGleoOLg2Xo8jTd/OCx6vZc7XWu9GAF1y8MPd4ybxZ++OEHvPDCCzJs7tSpkxxQTu2RHo/HEoiRjMBKNmzYMAncE088EbPmzMHer7+F67WReLNpS+FVNhDAbIy3W54C5xtvybbLTJcVKCOrEpSVUKxUcY7FM0rPcRBQuuOFZN2kPEx1Jb5SI0JR9w3vw64lig2UhptEN1JcMPWGGKBH6dSbrryP2hpKlqAUEAtu04PWdPMNoBTHKMWpv7Bq2SHnFC7vWq4PP/wQN998s/wBueqqq/Dee+9JSFq1R8Zq1G5JeeTm5qJLly5o3LgxXnr1VXz7n//io/mLMKFdZwzJypawHNXoOKx7YiB+/uJL9YwyU0cESsP/JPHZ6vk01APTMdo+9W+lU6Avgz4rVln39M1huvyCDocwE8RDz7d2KCZQhoDNAJvKG0vpjNdVvUHBtIosQWlIo7sZpn0GUAa3Kzc8WLb+VzpS3rVY1P540UUXycrQv39/2Gw2GT5bwa+qRnkTLFetWiUHqlPved9+/fDvr77CN24fZl/wFwnLYQKWQ0UonnfdTfivr1w9s8zTkYGSHtfKZ9dcN+izBFaEZ1imoQzoWRdpteMpX0M9I8k0yrEh9ZBkqi+hTQO1s/7EAZT0kTxHAasc/c2slPlih+QXCWamfcFjDdsZlFXRwoULcdppp8mpiC+++CIoXKahPlbQq66RV0rgpcHp9957r+xF7yGgWbZtG375+BMsu7Gv7NwZltVEQnN61wvxr9Xr1TPMLB0pKCufV3qOwzQN0XMcPIaOVz0+PUTNz7quHkiYBo/R1S3x2Vw3K9MpZoQpgzK86ILrLo4Mq003pDe1XVjeYSW9+WaEgFJ3w+TN0t/8YAivALHKoAyXdy3T3r17ZUcNDSJv3749Jk6cKD0/GhOpjY+Mt1GnEOX/7LPPysHpBOgVq9/Hr19/h6Knn8fIRs1Vz7IB3jmpHUonz8ChA7+rZ5wZOmJQap8Nz7RZeoiGgZX5eK0e6OuDxbGyPqvbQupmiBiUEaVcTMUqO3M00cXT/RJKsFamr7xJiixBKfILdsxY3kgyEdoL77XKoDTlfUGv+Zgzp0LZX0tEYxsffPBB6dldcMEFWLp0qRzCox9Enigjj5XGZ9LsnpNOOkl6smPE/7/88BMqJk7D2BPbClg2lB7myIbNsenZl2QHUKboyEGpPOM0hz4spOg5Dh5TNVAa6pusm6HHBp0ZQzlWYlDGLrrYBlAeocw3OJ6yyFt7X07LlqNrBTAJUr169ZI/FLfccgsKCwuPuNOmqkZApoHrNMuHBqfTeMunBw/Gd99/j09WrsbULn+W7ZXkXVJHz4rb78X3uz5Sv0F6Kx6gVABmCrvltkonoXJf1UCppFfz0fVay+hK2647zrA9pBwGZeyqAaDUv2CspgLz0KFDskPlrLPOQsOGDeWsGepooQHiVjBLtGmdPOvWrZOzfwiWNwlwb9u5E1/7SpHX83oBySbSqN1y9iVX4rMtNvXbpK+qDkpWpql6oMxw6UFZU4H522+/yTbI448/Xi5qMWLEiCoPIk+UUSdPSUkJHnroIQnwbn/+MzbbSvDz/32BdX9/DCOzm6uwrI8J7c/AjvlLcOj3g+o3Sz8xKGu+GJQmqwnA/Prrr/H888/LzhOaITNr1iwZ9lKnihW4UmEEbeppHz58uByY3uaUNpizcCH2fPc93CNG4+3mbWQYPiSrAd5q3hr2IaPw6/c/qt8wvcSgrPliUIaxTAXmThHG9unTR64VefXVV2PFihUy3KU2QitgpdII3ATwqVOnysHp1Bs/fNRIfLv7G+zIXYDJHc5Se8QbYXjdpigQ3ubPn/9b/abpIwZlzReDMorVqzcU3bvPVI9Mb23cuBEXX3yxnA3zwAMPoKioKOzKP+li1G5JnU30Hp4ePXrIULz//ffjgw8/whebSzD/il4iDBcAVYcQ5V13I/7j9qnfOD3EoKz5YlBGMPIq+/TJx2ef/aQemZ6i9sh58+ahQ4cOsk3ypZdeknO1tU4bu0PvTdphc/iE0QDzxA4LitUI5AR06o3v168fGjVqJAene8T5//jxv7Cq770YWedY6V2+nlUfU7uejw/yV+LwoUPqFUitGJQ1XwxKC8sUQJJokd2hQ4eiRYsWOP300zFhwoTgSuQKiOxwO9YHYWl3eFDmWIAKx0wVlgqsShwBuU/7nAqj8yYPc/DgwWjevDnOOvtsrFi9Gj/83+coynkZY5qdLDzLprKT551W7eAbPzktFtVgUNZ8MSh1lkmAJP3zn/+UITZ12lx22WVyjGJ5xVa43Arw7A4XnA4b/mPvhwr7jCAM/2u7RdiN4nOZTGMT2z+0DxdpcsXn1LVl0sB3v8+HgN8vV0und/WQh/zOu+/im692o3ziDLzbtrPsEafB6aMat0Dh0zn45T//Va9IasSgrPliUGYgIEkEFhpETpCkNyOuXbtWhK8VcDtLBHCcEogEQbKfHV3xte16bHHsEN7lRvzqaIMfHd3hcmxGsWM7djrG4oD9OHxgf0OG5RJaKQCm0+mA2+WEjzp5Kiowd+5cnHfeeWjWtCkef+opfPZ/n+HTgnXIvfAyjBCeJa1tScBcektffF2xXb0yyReDsuarVoMyEwFJL/1avnw5zhZhKb2n5umnn5aQke2RTi989pX4l/05eES4rXiQXux23oADzpYosy9EqW0RDvmz8Xvp0fDblglbib2BdjjsrYty+2x5DHmYbscm4Y1at2E6RXjs8YiyhPenGYX6brfwYAXsgmmdIp23Mk1U87rh97hlmyWF4NRuSUu/3XjjjbKT59bbbsOOjz7C7kAZll7fB2/UOVrt5MnGrAv+gn+uXo/DfyR/MWAGZc1XrQRlJgKS9OOPP2LUqFE4+eSTZVhKC1wQYLSVf8h7dDic+Mp+O761XQW3fYPwGLfiM8cAYFsW/uc7B9/ZeuDwh3WBnVn40tYPP3ouAj7JwsFAY3htayQoP7EPkkZ5KaaBzwm3AKS7ZAveX7lMenyzZ8/GnNkLsWjJUhQUboLD7YVHAtMJ2+b1yM+bj1mzRJo5cyLYbJlP/ipRvt0Fl4CtXZRHeWgrED388MPyh+HCiy7Cmo0b8f0/P8WGxwZidNMTZChOM3nGte4I/5QZ+OO3A+oVS44YlDVftRKUmagvvvgCAwcOlGMNaVEL/SBy/aIWNuFBuh2F+Nl+jgixW+MTxyD829Yfh3fWAXaJ2+0TtlVY+VHSi8QO8f//ZeGAtyV22UbjW0dP/Gpvg3LHbJlXEJICmNSTviF/Ol4acD+u6N4VzVu0wNFHHy3sZHTo1BW97uiHF9+cgoIiu4C3F5vmjUKPrm3QTKYhOwbNmx+H445TrPmxx6jbFbu8Xw7yN3rhdRuHM9EPAX3P119/Ha1bt8Ypp5yCGbNn4cfdXyMw9l2MP7mDDMNfysrCohtvx28//k+9askRg7Lmi0GZASJQ3CbCTlp1h96MSOEoQdI8iJzaFSnU3uLYjjLbAhzyNcTh7UfhoKexAsUKcbuFZ0ne5OFdRyn/lwsT2w/56uNQWQP59yPbEOGJbhN5Kh4q5e3xBbB27pu49aJToCyWcBTqZDeUIXHD7GzUURdRaNSiNa4fMARrbB4UzR+JS/90AuqL/ZSuQXZ99VjVjqonX2TWUBj9vfiOQVhSGApK+iEgz5maF6ZNmxZsdnjp9dfwzTff4OOlKzGp01mY2PFMfF5kI3KpVy45YlDWfDEo01j00q+CggK52g6BgeZGb9myRQLDOIhc+d/rWIOAfSl89vfgs63EN46/SihKD1J4joc/FV7k58I+U43+/z9hOwQ0y0SaD7Lwtau3CH8DskecOnZkp47LizLXOjx381kK4Oo0QbcrH8LrYydh0mRhY97CU/2vQcvGKgCPaY8nxi+Ha8sGzJ05Ge+MG4+Jk6di7Ijn0fvPzZU02S1w/m2PY9y74viJwisc/y7mLFqBzSVOYzunzrTXTNDg9J49e0r43t3/Xnz48cf4YvMWfL4xNWvaMChrvhiUaap9+/Zh8uTJOPXUU+Uajq+88ooMfWWnjQkgyjAgO76w/x37bKdiv6sN9nj/hF/dp+Kwp64IuwUkNShaGe2jsNybhf/Zu4lw/Rl8an8aX9lvw6eOp+DxB+BcORE9O9VRIHdcFwyctB47d+3E9h07sHP7drg3rcTrj9+CXtf0RM/rb8Uz7+TB5fEjIM63vKwMFdt2wrNxCR7ueZKSR6NW+OuzU+Ar24atFeWyLdLv84SFpGZau+WmTZvk4HSahXR5jx7YLH5AUiUGZc0XgzINpS1qQV4khZkETAJk5SDyUCNYehzr8In9Gex1d5LeIT4UgCwTECRPkjxIK0hqJtJIr3K7MBGSH3bVw/9s52CX4y24fCIMXzkBPTsepUCuQXN0u7EfXh07EfOWrcC6wkIR7jvh84uw2SM8UBEq0zt49OdH0Cx6fy4euPJEFZQnodfT40SI75FDgvRpYzHNq6brRGMt23fogIV5eTh4MPmrDDEoa74YlGkmWnX8rrvukm129GZEGgpk1R5pZRQmUy+3y74ZX7r7yfZGAl9USJJRGgrBS7Pwi+9MfGQfIvOUs3ecXvi96/HGA1fiuHoCcgQ6YQ1bnoyOZ5yJP19+OW579FGMHD8J+SvWwS6ALoGpO7d4g5KM2m5pFaLx48fL6ZvUQUSrEf30U3JHM5hBOWBAAcaN82D//vRdGo5VNTEo00jU/ti9e3fZaUMv5NqwYYMMM43tkeGNoEbtiuRZfunqh8OObKXjJlLYrZkAJXmgsGfhP/a+wit0yvxKHKUSwC7qXV83H68/ei3OPeM41KtbCUxpdeui2bEn4E/n9cJjI9/F++J4t4s6g5RzSwQoqZOHmiPohyRPeJN07ej95NSW+8knn6hXNfEyg/K008bhzjuXyb8MzJohBmUaiMJFmn7YqVMnnHDCCXJRC4KjVXuktRFonCh3zJFjI393NFdC6K0CktsEBGMBJRl5lCLsPvhBY+xzt8N/7bfiA/twOdyIYOkWUPI412PJ3DdFyDsAf+97O3qcexZOOqkpGuo8zQatz8QDb86Bw13Z5pgIUJIRLLXB6WvWrJHvKSdvnF6TS154MmQFShqju3v3HgwcuJ6BWQPEoEyxfvjhBwwZMgTHHnusDB8pjCRA0nAYKzBYmTLQ3I7P7APwY8lF+M7RAz+4L8XvnmNxuEzAkiAYS/hdLkBJPeQCsof89bCnpBO+t12GgCMfTk+5OK8ylJVXyM4Zv98N++aNWLl4IWbMeAtDBt2Fi9qcgPoSlnXQ8Yr7MH+DGz51qE+iQKk3um7UNvrUU0/J8abUvvv+++/LV2IkUuFAqYmASUv1dekykWGZoWJQplC0qEX//v1luEiLWixatEiG2tVdZNfpKBG2RXp/pSWL8ZvnRDlO8vAHUcJvgih1/viy8IPjEuwqGY1t9qlymJHTaYNXWH7u2xj4TA4GP/sixsxcDlfAD484T5qiGCgNIODZgtyXHkSn+opXeeIZPTBmqRN+jxJ+JwOUZNRuSUaLatDAdJrFNHbsWPz888/qVY+/IoFy1aoP0a3bVOHpLkZFxW65jZV5SjgojW90y1JfKWvxJjfDS8B0b42TpqWt7hvgwuWXOlHbGoWHBElakXz9+vUyfKQwUj/TpipGniUthkGDxWm+t5yN8y9xi/0ClLtUIBIw6a/+/w+EJ0mD0T/Mwveuy4VX5kORY5cyjtLpQ4XfhglPX4d64todVacuTul+O2av9aJceHAE9rLyMlSUubBw9AB0VkHZ5rybMa3ABV+SQUmmtVvS7CVaVIPWt3zsscfw738nZnV0K1BSqM2ArDlKKCjl+7i192sbFAsoK/dL2AZfu1ldUFrll3zRIPLFixfjjDPOkNP2Bg0aJMEYe3tkJLPLgeLbHZNw0NNEtjceKG2BP1yNlaE/2qycDwUcqeOGBqMTIEuFUcj9sbBdWfjSdbcCSRpsLvL1+gJYv2AYLj5BHR5U92hcdN3DGDlmKmbOzBU2EW+9+iiuObeVEnrXPQG9n3wbm50CgmpHVDJBSUY/OARxmsV03XXXyVdj3HTTTdi+Pf6rDJlBmRxAxlKHxHUOmpI29NmndKbX5LJCJGpGghTxFbSx3GTd/uA+3XZ1W3HQYzXlZ1C4/JIrCv9Gjx4th7G0bdsWY8aMieubEQlupfbF+Ml+Lr5zXIGPba9gR/FY/Oo9RXqWf7gb4lBJA2XaIsGR5n3TQHMByH2utvjOfiUO+psCrqPwpeNekSct1yYgJoDn8xdh5tCH0a3d8er1PgoNGjWT87ibNWuCRtnqYPQGx+D83k9gzppieFVvkkwB5Rz0/0sLJV2943D1k+8kDJRkWocYvRLjkUcekaMJzj//fOm9x1NmUCZHVaxDQX2O3N5aZCdSCWcmRT5DRilhoKRfrt5hf6boJlJlM1mYm2zpUdJDIY7RyqAbHrk8q/ySpy+//FJWVpp2R4ta0Ko55PFQmGhVyatj2qDzUsdiuBxF2OL4QC6ddtDbDPt87bDDMRb/53hC9oQfLG+MH5yXyB5u/DMLP3rOh9dWAL9tuRxD+YX9AXjtBUGv0ulyi3MtQd70d/D0fXfh4j93QRMBHjnXu2EjtG59Ki6/ug8efXEUFqwslD3kTt25uTwinF89HwP+2lF40s1w9IkdcdOzExMKSs2ozZLafWl2E81yopWXpkyZIl+hEQ9VB5TyGVSf+5wc7T35BDG9d0efrWBHsgBhTKAU0tKlyGHIRKUQlNFuchSAmm9ysXjYwsIvXH7JEb08i9ZU1MK/1atXS0+HwkOrin0kRrCk8Y/UVlniKBdgfBJ7bZ1QZp8nwLkTAfsyHHLXw/883QQY1+IL5/0y/P6ftxtc9iJxTIX0TJUhR8a2UlqH0uvxwlW8GSvy8zB9xgz5BsVp06Zi7tz5eG91Iey0zJrwJPWQlMcK785evAkrF83CtKmTMWXGLCx5f4P0WKNNW4yH0awmAibNcjrzzDPlrCcCJ406OFJVGZT07Oqef9lEpT6ThnpD6WJ9pkOe7cjPvCyTQ+6YlTBQSnCFBZIOeJpCQGn1a6jbXmVQWuWXWFEFWrlypVzUgsK+AQMGyOErFG5bVeZ4G4HyK3sffOR4Tf5PAKQVzvfZT8XXtuvEtlI5sPwnx/nY6+ygjpckD5cgGc7LE2ATwLRauJd6wV2RBseLfW7hWfp8fvnKB1q30jJdgoy8SvqBojbiK664Qs4Tv+eee/Dxxx+rd6x6qiooQ5wIQ10Rz6r6P6ULH/hUtw4pYlBWTYkDpbhd1BZigJd4IJSP1b3Juu1pDkoaRE7eS6tWrdCmTRs5tY7CbPJqrCpxIoy8S79jmQzDlR5xMjd2227CV7Y+sne8WHiQO+1v41t7L7kqugJK6/xqgmntluvWrQsuqkGjD0pKStQ7V3XFF5T0kQAm6k+O7vkOUXXrkJBWnrkOscIqgaAkqbDU3P8gyKp7k3XbjwCUvXrNx5w5Feqn+IvejPjCCy/IXu2zzjpLrqFIXmQ82yNjM3o1rVfCUdtGYfl2+yTsso8SHmWZ2KaE2ARJGocZ3pOsOUawpPtB3j21D1LnWseOHeU41uoMTq9y6E3Pqu551IfeUmJ/b3FeORHdverWIWM7aOS2fZamBIMyPaW9L6dly9FxB+bOnTvlNDp66Re1R1LoTZUyEe2R1TWCp7J6udYGSb3b1RvknskmmwvEj9fEiRNxzjnnSO//7bffrvLg9CqDUkgJfRWr7MzRRJCLFhbHAsrKMrQwOxSMsZTFqmGgND8c1r+W2svF4g3MjRs3yh5tguRrr70mZ97QQrNWlZQtPYy8S2qjpL/aWy1pdMLnIvSNVdUBpUGm0JuVfqq1HqXZjgSYv//+uxzuQ2MjqT1y+vTp8m2Je/bsOaKZNmyJN2qb3LFjhwy5P/30U/kSMxryRNCk9uRYZAZllZdZiwjK2H78WYkVg9JkVQUmvRmRXnpFizDQdLnCwkJ1j7LgBYMyvY1ASWuA0g8bidayHDFiBJo3b46uXbvKTp9oMoOSpjDyMms1SwzKMBYLMMkDoeEl5IHQS79ofrFeDMr0Nw2U+sHnNGKB1rds3769bLekCCHS4HQrUPIyazVLDMooVq/eULlEllnUQUPDSmh4Cb1GlmbemMWgTH+zAqUmanO+8MILZbvls88+i++//17dY1Q4UGriZdYyXwzKCEZeZZ8++YaHnirUihUr0LlzZzkdjqbC7d+/X91rFIMy/S0SKEkffvgh+vbtK34w6+GOO+7ARx99pO6pVCRQ8jJrNUMMSguzAqQmej0DLbDbrVs3rF27Vt1qLQZl+ls0UJKoHZqmO1L0QLDcvdsIPCtQ8jJrNUsMSp1FAqQmWshi0qRJ2LVrl7olvBiU6W+xgJJEIxtoQPrMmTPlfdXLDMrIgDRNwtBMTpYI7eE2TqKg/ZHGTlop2jHmMpW0NHsotGxtvKVFnjVcDMoYAVkdMSjT32IFZSSZQRmrQqYyhgDIPA04GvSsFO2YcNBTytaKpoHqBm7WMtVqUCYKkJoYlOlv6Q1KEm2L4MklDJRCWrqQMtRj1O2xrQmb2aqVoEw0IDUxKJNjLqf2Goyqz1NPf1CKrUFvjvbrw2TVooIy0jGm/aa8lKmW5imOOlCKY7TvEDo9suaoVoIyWWJQJscIkFu9m+Fx2cT/1mnCWeaB0rQ/xNszK9ox1mVqigpKfdkRF6bJbDEoEygGZXKsxOHDd8XDsdW+CjZn1VZoyozQW9sWDXpWinaMVZmqtKmVIWWoxzAoWfEQgzIZZkexqwK71zyJHUVzBShpVSSrdNaWCZ05lWmSCcpIy7ExKFlxVLxAabfTWwXdKCt1o7zMjjJh9NdspYHaB2TyIAOO9dhT0Bc/r38YDmfV2irTD5S69kJhRu4kCpT6MhU4hrY3UjpTpxKDkhUPxQuU9AqD9etWYtToVXjh1VK8+JovxJ592Ye33/WItMl5B026WJF7O/658S0gvzP2rLpRbqOOHXO6cJZKULIyRwzKBCpeoAwEnJgzdzO6X2nHC69sxPgJq/HW2AKMfLMAY8TfseNXY+jwtbj77z5RXu0BZbHDi9Lildi78m84nN8Ne967OdgDbpXeymoOKM2eYc3tgU6FGJQJVLxAWVbmxKQpG3D2hVswaowHeYvsWLHKBputGMuW2zB/oQPjJzlw7z/8EpK1AZQEQ5/Pg19W34XDi8/BocXnYZ8AZanfDZs9M0JvVuaIQZlAxQuUPp8XixcvwaCcPBF6e3Hfw15ce5MP8xa48NfbvLjnHyUY/LxLQJTaMmsHKLe4KvCVbYIIuS/E74vOxx+LuuGX5b3lPg69WfEWgzKBihcoqWe3vNyDnTuc+OdHm4UH6cSV1zkwf4Edt9xpQ+6s9/HxhyWoKC8WUCWw0kv/7fB4CBpW+WWuUUcNQXJX4UQcWHgO/lh4Nn7POw+HFpyJH5der6ZhULLiKwZlAhUPUJJ36PHYRbjtwvRZbizIc+HlIT5c2MOF14a5cNVfnXjupY2Yt9CL3NluzJztwsxZLsye68KatS64hYdplW8mGgGw2L0NuzZMxO/zz8bBeWfgwPxz8Pu8rvgj71xsLVqQUcODWJmjlIKShkfoG5+VkQWmRumo472iydzIXZ08qqcjBSVBsqzMgWEj3SLUDuC5V7wY/IIXOS958dzL4u+L6t+XfBgs/teMtt3/iB+PD3LD7yuW4Ximh+T0lkgaIvWv9aNxYM6ZODinMw7MPQu/C/tjVnt8W/A4XG53lbxJMgYlKxalDJRyapTlmCsjECvHcx0JKCuPk3BO0livWEDp9SqhMv2lz5Te53MKE+F2mR0jRntwxXWlWLjYhe3b7CgtVWzrVgd2bLeLcFz8X6GModT20faly1144hkCpfKubhqL6XYrwNSXnwlml7NtnPjivcH4I7c9fp/ZAQdyu6jWGX/MaIcd6ydji3tryLHRjEHJikWpAWXEQbJ6sOlnB+i2q8fHtmqJCbBRB+jGT5FASd4dwWvFKhcWirB6+UryhFzYvHkdlq8owsr3PHh9hFdCksLucuFZasfSEKDCwmJxrB35y1xYXeA0hNgE3vkiRH/+FZ8Ixf24qW8prr4+gGGjSkW5LricR9pmmkxzynD6gwLhSU5tgwPTTsVv0zsE7Y8pJ+GrJfdImOq9yZIY31POoGTFopSAMnRGgl4ENl2oHPT+TKAU+7Q8QmcR6GUEZbp4lNTRsm69EzfeEcBDj/tw290BFJf4MH5CHnr0moP+D/px38PCk5SQrDye4Lp9mxPDR25Gt0vc6HlDuYChV3iOlWk0UL48RIThIly/+Ooy9L3PjwuvKMPifBf8fuO5pLMRJL229dg35XT8NvFk/DrpNIMdfLcFPs8fgC3eHSJ95TXY6lwrwBl9mBCDkhWL0hSUVmAzgTLmqVMm8CbJmyRFA+Wq9514+AkfAn67CJN9KNrskF7f8y8Xori4SB7rF9DTH0cQLFhjw613edG1+1b8fUBAhN5GIFCaeQtdsq1y4HNeXH97ADNmuXHeXzIQlC4vtm3Ow6/vdsT+cafg1/ECkDr7fcxx+CLvIQHK7TK9Bsf/FY3AR45lUT1LBiUrFqUm9NZWJVE/GmUEZSUUjwSUuvySqFhA+fggH8pK7Xh0oA9LBMRyXvBi8jS/bIO06nwhz/Gdd93odnEprr3JhmXLlbZHfRpq76SQ/q77/ehzt1/CeNJUN86/PDNB+cF7b+PXtzpg/5sCjqPbG2z/m22xd0wXVKybjSL/ThS7/LB5y/DzlMvx7fx7YPNtE/mEXn/NGJSsWJSizhzzEvdCAnbKRyPY4uNRpi8oHxOApND6nn8E0PXicun1kfdHnTJWx2wucuCaG0tFGF2ON0fPx64dm4QHqXTWmNNv2eIU5pAe65Tpbpx7WWaCcuu6ufj1jc7YP0yAcngHaftG0N/22De8Iw683gpfTb0DO9bPw07PZvxr+RvYO+J0/DL+cpTbC+XCGVZ5kzEoWbEoRaAkqbDUQuIg6Ahsuu1BIMYPlL16zcecORXqp8QpFlA++rRPgit3jhtj3vEIYPoxcYonBJTKeEoHXhvuFZAsxcVXlYqwOiCHBhFYqZfcnJ7ypYHnO7ZlNig9RWux9/U/4ddX2mHvqx2EdRTWCftePg37XlE+//pCaxwYdiYOTr0Rv714sth3qkyzY+nbKPGUW+ZNlgpQ0its6Rn86Sfr1xyz0k8pBGXqpL0vp2XL0QkFZqweJQ3tGTqSpiL60eumUkydYe1REvxowPmCRQ7k5X+I3rd8gOv7+LC6wCXz09J5PE6sXbcJDzyyFnf0L8PSpYWYnuvK2DZKT9E6/O+1C/FrjvAgnz8de4X9Nvg0/N+sZ/Hj0EvV7Z2x71kB0GfaYt9zp+PXwadi36grsWvLKpREGISeTFASINu1G4djjhmJ7OxhCX8VCSt+qkGgNHmiwsJ1GGkvF0s0MKsSet/3iB833xmQYXU4UJJRR80HO2144y0/Luvpxdq1JSK0pk4MY5qly2nWjh2X9nSj3wM+TJ6WmW2UZMX+CuxY8i72P9YGewd1wb4n2+Hn4b1QWrwRn8x6EfufaIu9AztjzxPC4xR/Kc3epzth/6DO+HjxWBR7U+tR6gGpf+4YlJmjWutRmi0RwIy1M4dA+Tj1ehc5MWSEF+9O9KCiwi47aaw6dLZtdcj1J598pgQety0kTWnAgfxlNEPHiYHPOnDjHaWYNlMJvRctyTxQuos2YOvqBdj3VDfseeR07H+gNf41aTA2b/0YgY2rsefZi7FnQBf8OqoP9jwqgCn+3/dgO3zzYk94NhbA5k6NR2kFSP3zxqDMHDEoTRZPYMYCygcG+LF5s0P+LSx04NWhLgwTYXjRFgfWb3CGzKah4UIL8px4bbhbLtKr7/GmDh2fV4SrNofwIgMiDQ0yL8PFV5XhhtsDuKRnKZYuyyxQ2l1u+AuW4rMxj+Prl27Cnnvb4df7O+LTGa9ji/A07V4/Pp30HPbd3VZ4mXcKOF6PPfd1Euna48fnemF74SoRvocfIpQIUEYCpGYMyswSgzKMxQOYkUBJgNu4icZR+uWAc/pLoJsmwu57HvTLcZUUir861CuHD2mwpFk5K1asxXvvrRMhtshbzU+DJP3f7wG/XMR3yxbhlXoCwvssxR33+jFJhN+0wIaVl5q+5oTN48e2hZPxv7vPxp4+HfDrjSfjk7eFR7n9I3iLi/DNK/dg381t8fmr92NX7hjsubU9fr7rDOzr2xk/P/IX+NesEHn4LPKOLyhjAaRmDMrMEoMyitWrNxTdu89Uj6yaIoGSjIDlUcNr+kvbCIg0HZHGS9L0xKv+Vip7uimcVtLYUFy8SY6VpLSaUbskHX+XgCSB0iHAqaWhv2TkxWYWJBWzi2voFPbBjDH46cYu+P7u7vj4refgLVgJt9+Pb157EHsvb4my5Quxbda72NuzNX6+4XT82OtUfPjUbXAWbYLdbT1EKF6g7Nt3KerXH2b5DFkZgzKzxKCMYPQw9+mTX+0HOhooyQhcmum30V96iVjeEht6Xu/BuAnFcIlQu6SEQmvlr2bkTW4ucqHf3wPod7+ApDhWP1zInH9GmtOJ4tIKbJ03Fd/ccik+eekJeNcXoCRQBs+a97F9+ljY/AHsnDoWey47FT/dcw0+7tER7mnjYS+nxTKs70G8QLl79x489NBqHH30SPnjavU86Y1BmVliUFrYkQJSUyygjGYBvw0TpxSj980O4V360fOG0hC7+vpS/PUWF/7xqBd2m+JdWg1Az3wTPxACeqXTJ+GTay9GIG8e7D4/7B4vigPl0rvcJUJx/8rl2HbDFfA/8QAcbpeErHV+8Q29SbECk0GZWWJQ6ixegNQUD1A6RSX3+UqQO3s9HnnKiady/MJ8Bnv8GT/GvLNBpC2R4bVVPjXHnLCVlcM1JxfuhfPhEJDU77d7fHAWFMD76gtwlNjE/vCzcsgS0ZlDCg/Mp4UZh7FJC84+s9quKXTyRMjkixBFO8ZcppI2dPEYSlf5nu+qyfiO8EwUg1JYvAGpKR6g1Mzndcql1igcNxsNL/L7Y3/7YjzOJ9Vm9/pUCBq/C7Vnule/L39gHO7oS60lCpSawgGTnjnnm1ebxvqaoWae6psoUJr2Sylla0XTCl0GblZJDMqMVKIBqSkSKN3C8wmUlqO8XLXSUvi9HkvYuUTaMjVdacAnX8lqTOOU+ZWa0lCPuFsARdtuaaUBeEV46jTklx5GK5r7/KXix0A917IylPp98LgsQmmnG/7SMiWdAF95RYXxeworKysVPzjiGuuOSzQoNZmB2ajR8BhASaJtFmuyakoYKIW0dFZl0LRhkxcqZUorPVOdxxp+1bD0Vq0EZaIBqckKlC63AKSosMWFq7Bw3mRMnDQREycImzUbiws2wxuoQMCreULkJdpQuDof0ydOwLvvTsTMuUuwoVgJyYNpHDasf28JZoi8xo+fiNx5+SgUadwusX0VbZ+ECVTGRKNNEDZ5zjwUFAmPVMDVnS4dPk4XfIFScT5b8N7SOZg+TZzvhAmYMHUqcpesxGaHH2X6HwyR3lG8DgtnTsWEdyeEfE/NZs7Mxcp1m+Ap9QcXOk4WKDVpwGzS5A0ExvaMAZTEJM2bo/2V0AlaVFBGOsa035SXfBOBOeQmGOrO0xCmW4Cyd24xe5Ss8DKC0gmPT1TuTasw+rUncNM1F6B964aoV78u6tath/rHt0KXS/+K+wYNxbxVtCKQSCuO9bqKMOO1e9G2UUM0aNAYnS++C9PXOuD1qKt5C0/K69qIyS/ejQ7NGiE7uzHO7NEfMwpEqO4vwsRn70L7Zo3RsGEjNGrYANn16wmPRrG69eujaZu2uORv/fDqhGUocngFfMJ3fCTDnG4PfK4SLJ45Gg/3vwEXnn0Sjm4mzrWuuE5NmuL4P52Ha+98BCMm56HYqXiX5HG7CibgutNboWGDhmjUuBEaZNcPfk/FstHy+BNwXo/rMXjUNGxweuUPQ7JBqZcCEQOWYgClab+Vt2dQtGOsy9RkBcqI582gZFVVelC6PV44NyzEUzddhhYNtF/wemjcuInwLgTg6qrb6jRE5+63Y/ziTcK79MHnLsLUF+5Ac/UXv805t2LKGgd8OlD6XIWYMLgPTlDTnHbBHZgiQFkhQDnu6VtwvLo9K6suGjRqLMpsLMpsgoYCmsr2LDQ9+VI8O2UFHAJUoaF9cswpQm2vaxNmDnsMF5zWInhu9Rs0kufbuGHd4LbGJ56Jf7wyA5tcXnh9AbhWj0OP1k2D++tmN0RjOkZ+V/FDUf+o4L4GJ10ovusqeEs9sKc9KPXbokHPStGOsSpTFYXXlM4SfmHOm0HJqqqCoBSeT8D+Hl648wI0UStrs/YX4cGXxmBh3mIsXjIHk954CJe1OQ515P566NLjYeQVe1HuLcK0l+4Mwu7Uc/tgqgUoJz57B05WYduh+52YqoJy/KDb0Kqesv2UP9+AoVPmYdGiRcgTNmPcG7j5gi5oeJR63HWDsEqE4R53CrxK+h5eBxaPexLntTpank9WoxNxye2PYdyUOeIaLcK8mcPwYO/zcRztE9bgmHPx3NTlcNHQoILxuKrtscpxWS3R57lxWJSfj4ULFyJv8SLMGvc0rujUTN3fAn977B1sKSuF05Z8UNIMHlpmbfuEa6KAUulQqUyTTFAaO2DIswyeBx2vO4ZgaAi9gx6odv4MSlYEKaCkDgkvVk3KwVnNlQresEU3DJq8Ep6AH34RYlOYHQh48P74l3Gemibr6A54dEIBdpaVGEB52nl3YOYGAdBSP7xexZuiEHvqC3eidRRQdrrsXszeQCsTBUSZfpSVifOa9jzOO6ah3N/k5Kvx9vKN8HjcSe/cIY/bsWERHr22C+rRdz2qGS65+yUs20yzlJRr5POLEHtdPp7s0Vm5RsL+dNcQFLnL4F3zrg6UrfDYxDXYunMnKsqog2cbduzYjOF3dEe23N8UPf8+ChtLS+GyJw+U+imOtMyadWeO8r00M/Y0JwqU+jIVoBnAKEXpdLDTd+aYylfCdSWvnBzF+5QwFduMeWaOGJQJlASlR8DQvg6v978IdeXDk41z7hyCLV7qba703JxuL9y2jZgzaRSGDBmKISPfxtxVm1Dm22IA5Snn3IR38jegaFMhNmzYgA2FG1FUuBJvPXULTooCyo6X3I0ZBTYVhE74K0qxedmbuKJlE7m/YcsLMWThBgGtJIPS6YRXlLly0mCc21o9l049MXJugQCkt3IkgNMlvF0X1ubPwBvDxTUaOgRvzcyH3eU3eZQn4r4R87BxyxYUimu0cfMmrM4bhhvPPgZ1j6qLY9pegOcmiNBb/DglI/S2mgNOHYo84DxzxKBMoAiUHn8Axaunoc+ZzZVKnH0s7hy+GAGfxVAgAQKvSB8ICPP7BRTc8LiMoXf9Bk1xfKs2OKVNG7RR7ZQ2J+P4Y5sqnljWUegYBpSdL/87Ftm24oMPdmGn8La2l7mQ+/o/0LFpfbm/WfsbxXGbk+5ROkXY7XFvxJhH/4qTspVz7XjtQ5i/xg6/1sQQNGUolLxGwvxet/wxcq3Wg7Iujm55EtqccopyjU5pg1YtletTr/E5+Mcr80W4HoDbYU9oZ06kRTLiD8pQbzRTvbd0FIMygSJQ+kSIvCF/JK5o3lh5gJu0xBMT1oaM59ODgIb+yOE/BE4TKKNbeFC2bHc++j70JAYOHIhBg57BQ3ffis7Hqe2BWfVx4QMjUGhzw201TjGB5nR5RJmr8HKfv+BY9Xucf9tALCn0hm0v1a4RXcNQUEaweiej23VPYsZ7m+D1uWFLAChjWUWIPcrMEoMygQqCcukoXHmcElJGBiUNIaK2wzJlcLQIO71OIyiPb9cdD704HCPfGIHhw4cLewMjR7yCv994MZrXUdKEC73DWp2GIiz/B6Yu3wJ3DLNZ4m0aKF+5/fJg7/75fcKDkoYQBUrpGpXJwfWeEFAei6v7P4M33nxTXqMRb4zCkJefwFVdWqr766HrrYOxcos41mmLGyhjAaRmDMrMEoMygSJQUii95f3puO1MFZT1j8XtwxajLCT0FiGly471q/KRO2MGZkyfjSUr1sPlLcZ0HSjbXXAX5mwpx87tW5UZJxXbsHOrAzNfvjtqZ06DJi3QtkMndO58Ok4/XbEzr+iJx4aMxbJCmyg/nJebWFNC781469Fr0UoNvdv1ehDzC+zw0aIW+rTC2y3ZvBaL5szCDHGd5szOx0aXD+4CU2fOpDXYtmsXKuQ12oqKijKsHPcYTm+hDImq1/pyDJ+3XniVLmyPAyh5mbWaLQZlAkWgdHsD8BWtwjM3thcVhCpxNs667WUU+coMnTkuTynKHavw5HUd1XTH429PT4F/mxMzdaA8kuFBHS+5BzPpWOGpul0uOXTJRe2g1Hsu8ksFJMmc1MTgcWPRqH+gY0sFZA3aXYERc6gzx1f5g+Jyo6zUi7yR/YNAbdHtPiwp8cG3doIOlCfhobeXydWElO8kfoS8frjXT8X1nY6TaerUPwfPTFwGZ8ATF1DGumqQZgzKzBKDMoEiULpEmOj3u7B4zKM4tZFSubOb/gmPjVsO/1YRPpaWolSEkFvLvcgf8zg6HK0MjM5ufQ5emW/DzkCxIfQ+IlBeeg9y15pAKcxqfnmyjYYH2Qpmod9fTlPHkjbCubflYGmRSzZDlIrrRB60a61I072d/D5kPZ58F3ZvBTyGXu9WcmhVxY4dwetbsXUrCnNfwDknZStpsrti8MTlIaD83CkqxZAItkomM+jwd0BvbX/OHmSdtxpZDUT4XYeXWaspYlAmUBKUAkR2pxelztV4/e7L0Fit4NknnYG+Tw/H3LnzsWDBQox9uT/Oaq5W4qzGuPje11EovMxST/wGnBuHB4XCKqUmvEqfz4llE3JwwcnqwPB6x6Drdfdh5LhpWLBwEWZNHo47L+2ojoXMQpNOvTF+2UZ4A2VwmQac3/rMGMzLyxPXdw7mzl+AWVNHoe9FbVUIZ+GYP9+Oae8Xi9DeYWijjApK1XqLdJoMoNQsCjCrBsrQHu0jX36NVRUxKBOoICjtdtkzG7Ctwsv9eqJFQ9NDr7cGzfCXmwZhyRYChwiJXUWY+uIdaKHub9MtzBTGnD44UU1z2oW6KYwDb8EJ6sybdt3vwvR0BSWZ04OAz4ZF7zyHSzuow6nCWJsuV+H1aSvgCHjh8SpTGK9orc28iWzZJ1+MgeOWwVXmCxlHqQelHoak3KmV+8hyPlK2hxsehO+BK3XArGNaZq1qoNSDUJnxcmTLr7GqIgZlAqUHJYGAFm8IODZi2rhXcffNl+H00xqhfrayaEP9Vq1x1tV98MKb07DJ7Ybf55ZellwU49V70Ca7PurWbYjThbc4XYCSlkZT4KIuivFCP7Rrko06dRrgjMvvFUB0otxXhAnP3onTmortWdk4o8d9yCVQivzTEpRk4vsEvC6sXjINOY/fhb+c1xrHHiuuES2KcfTROOHsS3DHgJex4P0N8JX64BLH0HV1FkzAtZ1ORP169dGgYQM0yM5Gdv36yKa/coGMujjhhPbo1fdxDFuwCsUCrm5xrHkcZSRQkopF6B2EpQAngSgcKA1gnWNsw2xYv1MQ3Dk56pxqCUD9VD/6TAC0AKHcpqWNAkr1/2J1doxhkQsTUIPzuNXtudosG4JycDZO6PGWedcgMSgTKDMoyWgGTml5BVy2jXh/1SLkifBQ2vIVWLPZgUBphdLJo77Kwemwo2hDAfLzForwMw9LV6zGZhtt1+BCYwnt2Lx+NfIX5YkwPg/LVhbINC6nHZu07fOV7UXy2OSOk6yyURguwmm/CMU3rluB/HxxfRYuxMIlS7B8bSEc3jLheXorrwGNObVtwqqlS5S53do1NdhCLFv2HrbYvfDTMmtqu2xVQUleoj7MzhWfrUBpDuE1iMlOn7ueQP2sy7BC/E+SU/5UWBkWnCAISa/RCpTKcYpTSftVoOlNAyDlIz4H89UWu6D/I4FSHGPIX/VgQ84xXN41SAzKBMoKlJpRJ49Pm4WjzsTxhQmJXQKu/gB1aAQEPGgptHBpAoY0tHCvcXvqVgaqjtGbFz1ef+U1Eub3UkeUBejJ+6brKb6nPr3e/LTor2mR4iqDUkjvKVL4HQJKsU3br6XRywAakgEuAkrq/5SuElTRQBndo6wsUZc+EiiD25VQX+UkFVwZ9kfKuwaJQZlARQIlW3pYIkCZo+4Ll0dkUNJHCl8FnHK0bVbw0W9jUCZaDMoEikBJ0+yoMhIs2dLPiouLsXXr1riB0qoNM0QEGh1M9KG3lNjfOycHOUGYmuGjgKsStjGAMhhG08fextA72K6oy7cqoAyXdw0SgzKB+v777yUobTabpTfDlnqLZxuluV2S9oVT5VJkAjLBzhxNBD59pwh9rkwvjzFQMQZQiv+DHTOmtJXnUrksWpVAGSHvmiIGZQK1f/9+7N69G1999RX++9//sqWh0b357rvvcOjQIXnPooHS4DGqg88lKE0ADeeNWorAYwBlnGWAXpyVyLzTSAxKFkunSKA0j6PUPCcCpWFfuJA7nBiUaS8GJYulkzl8Dmf68PKzGI8hixSOs9JXDEoWS6dooDQP9SFtWWmd1soYlJkpBiWLxWJFEYOSxWKxoohByWKxWFHEoGSxWKyIAv4fQBkl4L8F+oEAAAAASUVORK5CYII=\"></TD></TR>
  <TR>
    <TH>Identifier:</TH>
    <TD colspan=\"3\">CoSES_Models.Houses.SF1</TD></TR>
  <TR>
    <TH>Version:</TH>
    <TD colspan=\"3\">1.0</TD></TR>
  <TR>
    <TH>File:</TH>
    <TD colspan=\"3\">SF1.mo</TD></TR>
  <TR>
    <TH>Connectors:</TH>
    <TD>Electrical Low-Voltage AC Three-Phase Connector</TD>
    <TD>lV3Phase1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Environment Conditions Connector</TD>
    <TD>environmentConditions1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the condensing       
                boiler</TD>
    <TD>StandardCBcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the CHP</TD>
    <TD>StandardCHPcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                     pump</TD>
    <TD>StandardSTcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the DHW pump</TD>
    <TD>StandardDHWcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                      supply and return is constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP input - electric power</TD>
    <TD>CHPIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to district heating heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                      negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of domestic hot water pump to charge the DHW     
                    storage</TD>
    <TD>qvDHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Parameters:</TH>
    <TD>Heated 3-zone-building with changeable heating system</TD>
    <TD>simpleHeatedBuilding1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature controller wiring for heating system</TD>
    <TD>heatingUnitFlowTemperature1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow generation and electrical power calculation</TD>
    <TD>pump1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW_demand</TD>
    <TD>dHW_demand1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                components</TD>
    <TD>grid1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat storage with variable temperature profile</TD>
    <TD>WolfSPU2_800</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar Thermal</TD>
    <TD>WolfCRK12</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow generation and electrical power calculation</TD>
    <TD>pump2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Specifies a Thermal Volume Flow Connector</TD>
    <TD>defineVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Extracts the characteristics of a Thermal Volume Flow Connector</TD>
    <TD>extractVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                components</TD>
    <TD>grid2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat storage with variable temperature profile</TD>
    <TD>WolfSEM1_500</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Controllable valve for volume flow distribution</TD>
    <TD>distributionValve1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Valve for volume flow merging</TD>
    <TD>mergingValve1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Hydraulic Switch with two heat generators</TD>
    <TD>hydraulicSwitch2HG1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBControlIn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CHPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Boolean expression</TD>
    <TD>CHP_EVU</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>TColdWater</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Green city of the CHP model based on measurements in the laboratory</TD>
    <TD>neoTower2_GC1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>VolumeFlow_DHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature difference between flow and return temperature</TD>
    <TD>CBDeltaT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature of CB</TD>
    <TD>CBTmax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature of CB</TD>
    <TD>CBTmin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum electrical power of CHP</TD>
    <TD>CHPmax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum electrical power of CHP</TD>
    <TD>CHPmin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, solar thermal collector is CPC collector, else, solar thermal 
                        collector is a flat plate collector</TD>
    <TD>CPC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inclination angle of solar thermal collector</TD>
    <TD>alphaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Orientation angle of solar thermal collector</TD>
    <TD>betaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in series</TD>
    <TD>nSeries</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in parallel</TD>
    <TD>nParallel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Effective surface area of solar thermal collector</TD>
    <TD>AModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Absorber volume</TD>
    <TD>VAbsorber</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature within the thermal storage</TD>
    <TD>Tmax_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature level to calculate the stored energy (e.g. return          
               temperature of consumption)</TD>
    <TD>T0_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, the temperature profile at simulation begin within the        
                 storage is linear, if false the profile is defined by a 
      temperature                 vector</TD>
    <TD>TSLinearProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of upmost heat storage layer at simulation begin</TD>
    <TD>TSTupInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of lowmost heat storage layer at simulation begin</TD>
    <TD>TSTlowInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Vector of temperature profile of the layers at simulation begin,       
                  element 1 is at lowest layer</TD>
    <TD>TSTLayerVector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature within the thermal storage</TD>
    <TD>Tmax_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature level to calculate the stored energy (e.g. return          
               temperature of DHW</TD>
    <TD>T0_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, the temperature profile at simulation begin within the        
                 storage is linear, if false the profile is defined by a 
      temperature                 vector</TD>
    <TD>DHWLinearProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of upmost heat storage layer at simulation begin</TD>
    <TD>DHWTupInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of lowmost heat storage layer at simulation begin</TD>
    <TD>DHWTlowInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Vector of temperature profile of the layers at simulation begin,       
                  element 1 is at lowest layer</TD>
    <TD>DHWTLayerVector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of people living in the building</TD>
    <TD>nPeople</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nFloors</TD>
    <TD>nFloors</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nApartments</TD>
    <TD>nApartments</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heated (living) area (e.g. 50m² per person)</TD>
    <TD>ALH</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, use standard area-specific heating power, else define it      
                   manually</TD>
    <TD>UseStandardHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Area-specific heating power - modern radiators: 14 - 15 W/m²; space    
                     heating: 15 W/m²</TD>
    <TD>QHeatNormLivingArea</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1</TD>
    <TD>n</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 -     
                     45°C</TD>
    <TD>TFlowHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal return temperature - radiator: 45 - 65°C; floor heating: 28 -   
                         35°C</TD>
    <TD>TReturnHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference indoor temperature</TD>
    <TD>TRef</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximumg flow rate in Living Zone</TD>
    <TD>qvMaxLivingZone</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TLiving_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TRoof_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TCellar_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If the presence is used, individual presence data has to be provided,  
                       else standart presence is used</TD>
    <TD>UseIndividualPresence</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with presence timeseries (presence in %; 0% - no one is at home;  
                       100% - everyone is at home)</TD>
    <TD>PresenceFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If individual electricity consumption is used, individual consumption  
                       data ha to be provided, else standart load profiles are 
      used</TD>
    <TD>UseIndividualElecConsumption</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with electric consumption time series (consumption in kW)</TD>
    <TD>ElConsumptionFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Table with electric consumption time series (consumption in W)</TD>
    <TD>ElConsumptionTable</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>ElFactor</TD>
    <TD>ElFactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, night time reduction is activated, else temperature is        
                 constant</TD>
    <TD>ActivateNightTimeReduction</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionStart</TD>
    <TD>NightTimeReductionStart</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionEnd</TD>
    <TD>NightTimeReductionEnd</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at night</TD>
    <TD>Tnight</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, presence will be used to define the temperature (if less      
                   people are at home, less rooms are heated and the average     
        temperature       will       decrease)</TD>
    <TD>VariableTemperatureProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature, when noone is at home (TRefSet = TMin + (TRef -   
                      TMin) * Presence(t))</TD>
    <TD>TMin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true: DHW consumption data repeats weekly</TD>
    <TD>WeeklyData</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Data File</TD>
    <TD>File</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Table Name</TD>
    <TD>Table</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>V_DHWperDay_l</TD>
    <TD>V_DHWperDay_l</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Factor, with which the DHW consumption gets multiplied</TD>
    <TD>DHWfactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CB turns on (SOCmin &lt; SOCmin and T<tstart)< 
      td=\"\">         </tstart)<>             
    <TD>SOCminCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CB turns off</TD>
    <TD>SOCmaxCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the CB turns on (T<tstart 
      td=\"\" and=\"\" socmin=\"\" <=\"\" socmin)<=\"\">         </tstart>             
    <TD>TStartCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the CB stops</TD>
    <TD>TStopCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CHP turns on (SOCmin &lt; SOCmin and T<tstart)< td=\"\"> 
                                </tstart)<>             
    <TD>SOCminCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CHP turns off</TD>
    <TD>SOCmaxCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the CHP turns on (T<tstart 
      td=\"\" and=\"\" socmin=\"\" <=\"\" socmin)<=\"\">         </tstart>             
    <TD>TStartCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the CHP stops</TD>
    <TD>TStopCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature difference between collector and storage         
                temperature</TD>
    <TD>deltaTonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-off-temperature difference between collector and storage        
                 temperature</TD>
    <TD>deltaToffST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature difference between flow and return, qv=qvMin</TD>
    <TD>deltaTFlowReturnLowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature difference between flow and return, qv=qvMax</TD>
    <TD>deltaTFlowReturnUpST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum volume flow of circulation pump</TD>
    <TD>qvMinST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum volume flow of circulation pump</TD>
    <TD>qvMaxST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature of DHW storage charging</TD>
    <TD>TonTS_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-off temperature of DHW storage charging</TD>
    <TD>ToffTS_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature, if the DHW temperature &lt; DHW consumption     
                    temperater + deltaTonDHW, the storage will be charged</TD>
    <TD>deltaTonDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum volume flow of circulation pump</TD>
    <TD>qvMaxDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>qvSTpump_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CHPin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>WolfCGB14 validated with CoSES mearusements</TD>
    <TD>wolfCGB14_GC1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Results:</TH>
    <TD>If true, standard control will be used to control the condensing       
                boiler</TD>
    <TD>StandardCBcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the CB modulation</TD>
    <TD>CBinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the CHP</TD>
    <TD>StandardCHPcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the CHP modulation</TD>
    <TD>CHPinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                     pump</TD>
    <TD>StandardSTcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on/off of solar thermal pump</TD>
    <TD>CPonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvRefST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal collector temperature</TD>
    <TD>TCollectorST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of solar thermal heat storage connection</TD>
    <TD>TStorageSTConnection</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature solar thermal</TD>
    <TD>TFlowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature solar thermal</TD>
    <TD>TReturnST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the DHW pump</TD>
    <TD>StandardDHWcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on/off of DHW pump</TD>
    <TD>CPonDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control flow rate of the DHW pump</TD>
    <TD>qvRefDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler flow temperature</TD>
    <TD>CB_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler return temperature</TD>
    <TD>CB_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow condensing boiler</TD>
    <TD>CB_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas flow condensing boiler</TD>
    <TD>CB_S_FG</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Demanded fuel volume</TD>
    <TD>CB_VFuel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of condensing boiler</TD>
    <TD>CB_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas power of condensing boiler</TD>
    <TD>CB_P_gas_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of condensing boiler</TD>
    <TD>CB_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas input of condensing boiler</TD>
    <TD>CB_E_gas_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating efficiency</TD>
    <TD>CB_Efficiency</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP flow temperature</TD>
    <TD>CHP_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP return temperature</TD>
    <TD>CHP_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow CHP</TD>
    <TD>CHP_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas flow CHP</TD>
    <TD>CHP_S_FG</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Demanded fuel volume</TD>
    <TD>CHP_VFuel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of CHP</TD>
    <TD>CHP_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric power output of CHP</TD>
    <TD>CHP_P_el_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas power of CHP</TD>
    <TD>CHP_P_gas_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of CHP</TD>
    <TD>CHP_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electricity output of CHP</TD>
    <TD>CHP_E_el_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas input of CHP</TD>
    <TD>CHP_E_gas_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal efficiency</TD>
    <TD>CHP_Efficiency_th</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric efficiency</TD>
    <TD>CHP_Efficiency_el</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Overall efficiency</TD>
    <TD>CHP_Efficiency_total</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal flow temperature</TD>
    <TD>ST_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal return temperature</TD>
    <TD>ST_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>ST_S_TM_Collector</TD>
    <TD>ST_S_TM_Collector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow solar thermal</TD>
    <TD>ST_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of solar thermal</TD>
    <TD>ST_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of solar thermal</TD>
    <TD>ST_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Power transfered to DHW storage</TD>
    <TD>TS_P_heat_toHWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature consumption side</TD>
    <TD>TS_S_TM_HC_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature consumption side</TD>
    <TD>TS_S_TM_HC_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature producer side</TD>
    <TD>TS_S_TM_PS_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature producer side</TD>
    <TD>TS_S_TM_PS_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature to DHW storage</TD>
    <TD>TS_S_TM_HC_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature to DHW storage</TD>
    <TD>TS_S_TM_HC_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 1</TD>
    <TD>TS_S_TM_HWS_1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 2</TD>
    <TD>TS_S_TM_HWS_2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 3</TD>
    <TD>TS_S_TM_HWS_3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 1</TD>
    <TD>TS_S_TM_BT_1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 2</TD>
    <TD>TS_S_TM_BT_2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 3</TD>
    <TD>TS_S_TM_BT_3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 4</TD>
    <TD>TS_S_TM_BT_4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 5</TD>
    <TD>TS_S_TM_BT_5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 6</TD>
    <TD>TS_S_TM_BT_6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 7</TD>
    <TD>TS_S_TM_BT_7</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 8</TD>
    <TD>TS_S_TM_BT_8</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 9</TD>
    <TD>TS_S_TM_BT_9</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 10</TD>
    <TD>TS_S_TM_BT_10</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to DHW storage</TD>
    <TD>TS_S_FW_HC_HW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy in thermal storage</TD>
    <TD>TS_E_Storage_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy in DHW storage</TD>
    <TD>TS_E_Storage_HWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Total energy transfered to DHW storage</TD>
    <TD>TS_E_heat_toHWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>State of charge of the thermal storage</TD>
    <TD>TS_SOC_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>State of charge of the DHW storage</TD>
    <TD>TS_SOC_HWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature before mixing unit</TD>
    <TD>HS_S_TM_VL_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature after mixing unit</TD>
    <TD>HS_S_TM_VL_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature</TD>
    <TD>HS_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink flow temperature hot water</TD>
    <TD>HS_S_TM_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature hot water</TD>
    <TD>HS_S_TM_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature in the house</TD>
    <TD>HS_S_TM_Room</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow after mixing unit</TD>
    <TD>HS_S_FW_HC_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow before mixing unit</TD>
    <TD>HS_S_FW_HC_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow domestic hot water consumption</TD>
    <TD>HS_S_FW_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating power</TD>
    <TD>HS_P_DemHeatHC_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water power</TD>
    <TD>HS_P_DemHeatHW_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating Energy</TD>
    <TD>HS_E_DemHeatHC_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water energy</TD>
    <TD>HS_E_DemHeatHW_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - set temperature (power is constant)</TD>
    <TD>CBIn_TSet</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                      supply and return is constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP input - electric power</TD>
    <TD>CHPIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to district heating heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                      negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of domestic hot water pump to charge the DHW     
                    storage</TD>
    <TD>qvDHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXin</TD>
    <TD>&nbsp;</TD></TR></TBODY></TABLE></DIV>
<H2>Description:</H2>
<P>&nbsp;</P></BODY></HTML>
"),
		experiment(
			StopTime=1,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.001,
			__esi_SolverOptions(
				solver="CVODE",
				typename="ExternalCVODEOptionData"),
			__esi_MinInterval="9.999999999999999e-10",
			__esi_AbsTolerance="1e-6"));
end MF5;
