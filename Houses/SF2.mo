﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo.Houses;
model SF2 "House 2 with ST, CB and ASHP"
	GreenCity.Interfaces.Electrical.LV3Phase lV3Phase1 "Electrical Low-Voltage AC Three-Phase Connector" annotation(Placement(
		transformation(extent={{240,-475},{260,-455}}),
		iconTransformation(extent={{236.7,90},{256.7,110}})));
	GreenCity.Interfaces.Environment.EnvironmentConditions environmentConditions1 "Environment Conditions Connector" annotation(Placement(
		transformation(extent={{480,20},{500,40}}),
		iconTransformation(extent={{236.7,190},{256.7,210}})));
	GreenCity.Utilities.Thermal.Pump pump2(
		qvMax(displayUnit="l/min")=0.00025,
		PPump(tableID "External table object")) "Volume flow generation and electrical power calculation" annotation(Placement(transformation(
		origin={110,-120},
		extent={{0,0},{10,10}},
		rotation=-180)));
	GreenCity.GreenBuilding.HeatingSystem.HeatingUnitFlowTemperature heatingUnitFlowTemperature1(
		CirculationPump(PPump(tableID "External table object")),
		qvMaxPump(displayUnit="l/min")=0.00025) annotation(Placement(transformation(extent={{280,-120},{315,-85}})));
	Consumer.DHW_demand dHW_demand1(
		Load(tableID "External table object"),
		WeeklyData=WeeklyData,
		File=File,
		Table=Table,
		DHWfactor=DHWfactor,
		THotWaterSet=323.15) annotation(Placement(transformation(extent={{390,-205},{410,-185}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap2 annotation(Placement(transformation(extent={{255,-325},{265,-315}})));
	GreenCity.Utilities.Electrical.Grid grid1(
		OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
		useA=true,
		useB=true,
		useC=true,
		useD=true,
		useE=true,
		useF=true) annotation(Placement(transformation(extent={{195,-375},{235,-335}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap1 annotation(Placement(transformation(extent={{265,-345},{275,-335}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap5 annotation(Placement(transformation(extent={{240,-295},{250,-285}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap6 annotation(Placement(transformation(extent={{245,-310},{255,-300}})));
	Generator.DigitalTwins wolfCGB20 annotation(Placement(transformation(extent={{50,-100},{95,-55}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow defineVolumeFlow1 annotation(Placement(transformation(
		origin={235,15},
		extent={{-10,-10},{10,10}},
		rotation=-90)));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow extractVolumeFlow1 annotation(Placement(transformation(
		origin={205,15},
		extent={{10,-10},{-10,10}},
		rotation=-90)));
	GreenCity.Utilities.Thermal.MeasureThermal measureThermal1 annotation(Placement(transformation(extent={{355,-185},{365,-195}})));
	GreenCity.Utilities.Electrical.Grid grid2(
		OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
		useA=true,
		useB=true,
		useC=true,
		useD=true,
		useE=true,
		useF=true) annotation(Placement(transformation(extent={{160,-325},{200,-285}})));
	Storage.HeatStorageCombined5Inlets WolfBSP800(
		VStorage(displayUnit="m³")=VStorage,
		dStorage=0.79,
		QlossRate=7,
		LinearProfile=TSLinearProfile,
		TupInit(displayUnit="K")=TSTupInit,
		TlowInit(displayUnit="K")=TSTlowInit,
		TLayerVector(displayUnit="K")=TSTLayerVector,
		TMax(displayUnit="K")=Tmax_TS,
		alphaMedStatic=1,
		use1=true,
		iFlow1=9,
		iReturn1=2,
		use2=true,
		HE2=true,
		iFlow2=5,
		AHeatExchanger2=2.5,
		VHeatExchanger2=0.0165,
		use4=true,
		iFlow4=10,
		iReturn4=6,
		use5=true,
		iFlow5=4,
		iReturn5=1,
		use6=true,
		iFlow6=9,
		iReturn6=2,
		use7=true,
		iFlow7=4,
		use8=true) annotation(Placement(transformation(extent={{190,-208},{225,-135}})));
	Distribution.SwitchPort switchPort1 annotation(Placement(transformation(extent={{135,-210},{155,-180}})));
	Generator.DigitalTwins.WolfCHA10_GC wolfCHA1(
		HPButton=UseHP,
		PAuxMax(displayUnit="Nm/s")=PAuxMax,
		PHeatNom(displayUnit="Nm/s")=PHeatNom,
		PCoolingNom(displayUnit="Nm/s")=PCoolingNom,
		COPNom=COPNom,
		EERNom=EERNom,
		wolfCHA1(
			PElHeatFactor_table(tableID "External table object"),
			PHeatFactor_table(tableID "External table object"),
			PElHeatModulationFactor_table(tableID "External table object"),
			PHeatModulationFactor_table(tableID "External table object"),
			PElCoolingFactor_table(tableID "External table object"),
			PCoolingFactor_table(tableID "External table object"),
			PElCoolingModulationFactor_table(tableID "External table object"),
			PCoolingModulationFactor_table(tableID "External table object"),
			StartUpHeat_table(tableID "External table object"),
			CoolDownHeat_table(tableID "External table object"),
			DeIcing_table(tableID "External table object"),
			StartUpCooling_table(tableID "External table object"),
			CoolDownCooling_table(tableID "External table object"))) annotation(Placement(transformation(extent={{10,-280},{55,-235}})));
	GreenCity.Utilities.Electrical.PhaseTap phaseTap7 annotation(Placement(transformation(extent={{145,-310},{135,-300}})));
	Modelica.Blocks.Sources.RealExpression TColdWater(y(quantity="Basics.Temp")=288.15) annotation(Placement(transformation(extent={{465,-230},{445,-210}})));
	Modelica.Blocks.Logical.Switch switch2 annotation(Placement(transformation(extent={{-55,-110},{-35,-90}})));
	Modelica.Blocks.Sources.RealExpression qvSTpump_StandardControl(y=qvRefST) annotation(Placement(transformation(extent={{-120,-90},{-100,-70}})));
	Modelica.Blocks.Logical.LogicalSwitch logicalSwitch1 annotation(Placement(transformation(extent={{10,-180},{30,-160}})));
	Modelica.Blocks.Sources.BooleanExpression SwitchSignal_StandardControl(y=HP3WVinStandardControl) "Set output signal to a time varying Boolean expression" annotation(Placement(transformation(extent={{-35,-160},{-15,-140}})));
	GreenCity.Local.Controller.PV2ACInverter pV2ACInverter1 annotation(Placement(transformation(extent={{380,-365},{340,-405}})));
	Distribution.HydraulicSwitch_TS_DH_HG hydraulicSwitch_TS_DH_HG1(
		FeedInPump(PPump(tableID "External table object")),
		ExtractionPump1(PPump(tableID "External table object"))) annotation(Placement(transformation(extent={{175,-30},{245,-15}})));
	Modelica.Blocks.Logical.Switch switch4 annotation(Placement(transformation(extent={{55,-35},{75,-15}})));
	Modelica.Blocks.Sources.RealExpression CBin_StandardControl(y=CBinStandardControl) annotation(Placement(transformation(extent={{20,-20},{40,0}})));
	Modelica.Blocks.Logical.Switch switch3 annotation(Placement(transformation(extent={{-50,-295},{-30,-275}})));
	Modelica.Blocks.Sources.RealExpression HPin_StandardControl(y=HPinStandardControl) annotation(Placement(transformation(extent={{-90,-275},{-70,-255}})));
	Modelica.Blocks.Logical.Switch switch5 annotation(Placement(transformation(extent={{-55,-360},{-35,-340}})));
	Modelica.Blocks.Sources.RealExpression AUXin_StandardControl(y=AUXinStandardControl) annotation(Placement(transformation(extent={{-90,-350},{-70,-330}})));
	Generator.DigitalTwins.WolfCGB20_GC wolfCGB20_GC1(
		CBButton=UseCB,
		DeltaT=CBDeltaT,
		wolfCGB1(
			EffHeat_table(tableID "External table object"),
			StartUp_table(tableID "External table object"),
			CoolDown_table(tableID "External table object"),
			PHeat_table(tableID "External table object"),
			DeltaT_table(tableID "External table object"))) "WolfCGB20 validated with CoSES mearusements" annotation(Placement(transformation(extent={{90,-50},{135,-5}})));
	parameter Real dt_ems(quantity="Basics.Time")=900 "Timestep size EMS" annotation(Dialog(tab="Control"));
	parameter Boolean MinimumControlEMS=true "If true, inputs can be overwritten, if the energy management system fails to keep the temperatures above the boundries of the standard control." annotation(Dialog(tab="Control"));
	Modelica.Blocks.Interfaces.BooleanInput StandardTRefControl(start=true) "If true, standard control for room temperature" annotation(
		Placement(
			transformation(extent={{460,-230},{500,-190}}),
			iconTransformation(
				origin={-200,250},
				extent={{-20,-20},{20,20}},
				rotation=-90)),
		Dialog(
			tab="Control",
			visible=false));
	Modelica.Blocks.Math.Gain gain1(k=10) annotation(Placement(transformation(extent={{-15,-60},{5,-40}})));
	Modelica.Blocks.Sources.BooleanExpression Overwrite_CB(y=OverwriteCB) annotation(Placement(transformation(extent={{-15,-35},{5,-15}})));
	Modelica.Blocks.Sources.BooleanExpression Overwrite_HP3WV(y=OverwriteHP3WV) annotation(Placement(transformation(extent={{-35,-180},{-15,-160}})));
	Modelica.Blocks.Logical.GreaterThreshold greaterThreshold1 annotation(Placement(transformation(extent={{-15,-240},{-5,-230}})));
	Modelica.Blocks.Sources.BooleanExpression Overwrite_HP(y=OverwriteHP) annotation(Placement(transformation(extent={{-90,-295},{-70,-275}})));
	Consumer.SimpleHeatingCoolingBuilding simpleHeatedBuilding1(
		cellar(
			CoolLoadFactorPerson(tableID "External table object"),
			CoolLoadFactorLigth(tableID "External table object"),
			CoolLoadFactorMachine(tableID "External table object"),
			AppliedLoadFactorLight(tableID "External table object"),
			AppliedLoadFactorMachine(tableID "External table object"),
			PelDIN(tableID "External table object"),
			NumberPersonDIN(tableID "External table object"),
			NumberPerson(tableID "External table object"),
			ElectricalPower(tableID "External table object"),
			ReactivePower(tableID "External table object"),
			BaseLoad(tableID "External table object"),
			NormLoad(tableID "External table object"),
			MachineLoad(tableID "External table object"),
			LightLoad(tableID "External table object"),
			InnerLoad(tableID "External table object")),
		livingZone(
			CoolLoadFactorPerson(tableID "External table object"),
			CoolLoadFactorLight(tableID "External table object"),
			CoolLoadFactorMachine(tableID "External table object"),
			AppliedLoadFactorLight(tableID "External table object"),
			AppliedLoadFactorMachine(tableID "External table object"),
			PelDIN(tableID "External table object"),
			NumberPersonDIN(tableID "External table object"),
			NumberPerson(tableID "External table object"),
			ElectricalPower(tableID "External table object"),
			ReactivePower(tableID "External table object"),
			BaseLoad(tableID "External table object"),
			NormLoad(tableID "External table object"),
			MachineLoad(tableID "External table object"),
			LightLoad(tableID "External table object"),
			InnerLoad(tableID "External table object")),
		roof(
			CoolLoadFactorPerson(tableID "External table object"),
			CoolLoadFactorLigth(tableID "External table object"),
			CoolLoadFactorMachine(tableID "External table object"),
			AppliedLoadFactorLight(tableID "External table object"),
			AppliedLoadFactorMachine(tableID "External table object"),
			PelDIN(tableID "External table object"),
			NumberPersonDIN(tableID "External table object"),
			NumberPerson(tableID "External table object"),
			ElectricalPower(tableID "External table object"),
			ReactivePower(tableID "External table object"),
			BaseLoad(tableID "External table object"),
			NormLoad(tableID "External table object"),
			MachineLoad(tableID "External table object"),
			LightLoad(tableID "External table object"),
			InnerLoad(tableID "External table object")),
		PelTable(tableID "External table object"),
		presenceTable(tableID "External table object"),
		redeclare replaceable parameter GreenCity.Utilities.BuildingData.ParameterSelectionSCB.AgeOfBuilding.EnEV2007 buildingAge,
		nFloors=nFloors,
		nAp=nApartments,
		nPeople=nPeople,
		ALH=ALH,
		livingTZoneInit(displayUnit="K")=TLiving_Init,
		UseStandardHeatNorm=UseStandardHeatNorm,
		QHeatNormLivingArea=QHeatNormLivingArea,
		n=n,
		TFlowHeatNorm(displayUnit="K")=TFlowHeatNorm,
		TReturnHeatNorm(displayUnit="K")=TReturnHeatNorm,
		qvMaxLivingZone(displayUnit="m³/s")=qvMaxLivingZone,
		ActivateCooling=ActivateCooling,
		TRefCooling(displayUnit="K")=TRefCooling,
		roofTZoneInit(displayUnit="K")=TRoof_Init,
		cellarTZoneInit(displayUnit="K")=TCellar_Init,
		UseIndividualPresence=UseIndividualPresence,
		PresenceFile=PresenceFile,
		Presence_WeeklyRepetition=true,
		UseIndividualElecConsumption=UseIndividualElecConsumption,
		ElConsumptionFile=ElConsumptionFile,
		ElConsumptionTable=ElConsumptionTable,
		ElConsumptionFactor=ElFactor,
		ElConsumption_YearlyRepetition=true,
		UseReferenceInputSignal=true,
		ActivateNightTimeReduction=false,
		Tnight(displayUnit="K")=Tnight,
		NightTimeReductionStart(displayUnit="s")=NightTimeReductionStart,
		NightTimeReductionEnd(displayUnit="s")=NightTimeReductionEnd,
		VariableTemperatureProfile=false,
		TMin(displayUnit="K")=TMin,
		roofInsul(displayUnit="m")=0.10 + AdditionalInsulation,
		ceilingInsul(displayUnit="m")=0 + AdditionalInsulation,
		wallInsul(displayUnit="m")=0.10 + AdditionalInsulation,
		floorInsul(displayUnit="m")=0.10 + AdditionalInsulation) annotation(Placement(transformation(extent={{385,-125},{425,-85}})));
	Modelica.Blocks.Sources.BooleanExpression Overwrite_HPMode(y=HPMode_SC) annotation(Placement(transformation(extent={{-65,-235},{-45,-215}})));
	Distribution.SwitchPort switchPort2 annotation(Placement(transformation(extent={{85,-265},{105,-235}})));
	Distribution.SwitchPort switchPort3 annotation(Placement(transformation(extent={{365,-90},{345,-120}})));
	Generator.DigitalTwins.SolarThermal_GC WolfCRK12(
		UseST=UseST,
		CPC=CPC,
		alphaModule(displayUnit="rad")=alphaModule,
		betaModule(displayUnit="rad")=betaModule,
		nSeries=nSeries,
		nParallel=nParallel,
		AModule=AModule,
		VAbsorber(displayUnit="m³")=VAbsorber) annotation(Placement(transformation(extent={{50,-155},{90,-115}})));
	Generator.DigitalTwins.Photovoltaic_GC photovoltaic_GC1(
		UsePV=UsePV,
		alphaModule(displayUnit="rad")=alphaPV,
		betaModule(displayUnit="rad")=betaPV,
		PPeak(displayUnit="Nm/s")=PVPeak) annotation(Placement(transformation(extent={{430,-405},{390,-365}})));
	Storage.StationaryBattery_GC stationaryBattery_GC1(
		RsCalc(tableID "External table object"),
		R1Calc(tableID "External table object"),
		R2Calc(tableID "External table object"),
		P1Calc(tableID "External table object"),
		P2Calc(tableID "External table object"),
		T1Calc(tableID "External table object"),
		T2Table(tableID "External table object"),
		VocCalc(tableID "External table object"),
		QRealMin(tableID "External table object"),
		QRealMax(tableID "External table object"),
		UseBat=UseBat,
		EMaxNominal(displayUnit="J")=EMaxNominal,
		SOCInit(displayUnit="-")=SOCInit) annotation(Placement(transformation(extent={{65,-440},{105,-400}})));
	GreenCity.StorageSystems.Controller.Batt2ACInverter batt2ACInverter1(
		PMax(displayUnit="Nm/s")=PMax,
		CosPhi=0.9) annotation(Placement(transformation(extent={{125,-435},{155,-405}})));
	GreenCity.Utilities.Electrical.Grid grid3(
		OutputType=GreenCity.Utilities.Electrical.Grid.OutputEnum.MasterGrid,
		useA=true,
		useB=true,
		useC=true,
		useD=true,
		useE=true,
		useF=true) annotation(Placement(transformation(extent={{230,-440},{270,-400}})));
	Modelica.Blocks.Logical.Switch switch1 annotation(Placement(transformation(extent={{25,-405},{45,-385}})));
	Modelica.Blocks.Sources.RealExpression Battery_StandardControl(y=BatPChargeSC) annotation(Placement(transformation(extent={{-40,-385},{-20,-365}})));
	Boolean OverwriteCB "Overwrite Input Signal" annotation(Dialog(
		group="Condensing Boiler",
		tab="Control",
		visible=false));
	Boolean MinControlCB "Minimum Control overwrites input signal from the energy management system" annotation(Dialog(
		group="Condensing Boiler",
		tab="Control",
		visible=false));
	parameter Real TStartCB(quantity="Basics.Temp")=328.15 "Minimum Temperature, at the top, at which the CB turns on" annotation(Dialog(
		group="Condensing Boiler",
		tab="Control"));
	parameter Real TStopCB(quantity="Basics.Temp")=333.15 "Temperature at the bottom, at which the CB stops" annotation(Dialog(
		group="Condensing Boiler",
		tab="Control"));
	Real CBinStandardControl "Standard control value of the CB modulation" annotation(Dialog(
		group="Condensing Boiler",
		tab="Control",
		visible=false));
	Modelica.Blocks.Interfaces.BooleanInput StandardControlCB "If true, standard control will be used to control the condensing boiler" annotation(
		Placement(
			transformation(extent={{-135,-40},{-95,0}}),
			iconTransformation(
				origin={-100,250},
				extent={{-20,-20},{20,20}},
				rotation=-90)),
		Dialog(
			group="Condensing Boiler",
			tab="Control",
			visible=false));
	Boolean OverwriteHP "Overwrite Input Signal" annotation(Dialog(
		group="Heat Pump",
		tab="Control",
		visible=false));
	Boolean MinControlHP "Minimum Control overwrites input signal from the energy management system" annotation(Dialog(
		group="Heat Pump",
		tab="Control",
		visible=false));
	parameter Real TStartHPhigh(quantity="Basics.Temp")=328.15 "Minimum Temperature, at the top, at which the heat pump turns on" annotation(Dialog(
		group="Heat Pump",
		tab="Control"));
	parameter Real TStopHPhigh(quantity="Basics.Temp")=333.15 "Temperature at the top, at which the heat pump stops" annotation(Dialog(
		group="Heat Pump",
		tab="Control"));
	parameter Real TStartHPlow(quantity="Basics.Temp")=313.15 "Minimum Temperature, at the bottom, at which the heat pump turns on" annotation(Dialog(
		group="Heat Pump",
		tab="Control"));
	parameter Real TStopHPlow(quantity="Basics.Temp")=318.15 "Temperature at the bottom, at which the heat pump stops" annotation(Dialog(
		group="Heat Pump",
		tab="Control"));
	parameter Real TStartAUXhigh(quantity="Basics.Temp")=323.15 "Minimum Temperature, at the top, at which the auxiliary heater turns on" annotation(Dialog(
		group="Heat Pump",
		tab="Control"));
	parameter Real TStopAUXhigh(quantity="Basics.Temp")=328.15 "Minimum Temperature, at the bottom, at which the auxiliary heater turns on" annotation(Dialog(
		group="Heat Pump",
		tab="Control"));
	parameter Real TStartAUXlow(quantity="Basics.Temp")=308.15 "Minimum Temperature, at the top, at which the auxiliary heater stops" annotation(Dialog(
		group="Heat Pump",
		tab="Control"));
	parameter Real TStopAUXlow(quantity="Basics.Temp")=313.15 "Minimum Temperature, at the bottom, at which the auxiliary heater stops" annotation(Dialog(
		group="Heat Pump",
		tab="Control"));
	Real HPinStandardControl(quantity="Basics.RelMagnitude") "Standard control value of the heat pump modulation" annotation(Dialog(
		group="Heat Pump",
		tab="Control",
		visible=false));
	Real AUXinStandardControl(quantity="Basics.RelMagnitude") "Standard control value of the auxiliary heater modulation" annotation(Dialog(
		group="Heat Pump",
		tab="Control",
		visible=false));
	Modelica.Blocks.Interfaces.BooleanInput StandardControlHP "If true, standard control will be used to control the heat pump" annotation(
		Placement(
			transformation(extent={{-135,-305},{-95,-265}}),
			iconTransformation(
				origin={0,250},
				extent={{-20,-20},{20,20}},
				rotation=270)),
		Dialog(
			group="Heat Pump",
			tab="Control",
			visible=false));
	Boolean dhw "DHW is required" annotation(Dialog(
		group="Heat Pump",
		tab="Control",
		visible=false));
	Boolean heating "heating is required" annotation(Dialog(
		group="Heat Pump",
		tab="Control",
		visible=false));
	Boolean HPMode_SC "combined HP mode: true if heating, false if cooling" annotation(Dialog(
		group="Heat Pump",
		tab="Control"));
	Boolean OverwriteHP3WV "Overwrite Signal for heat pump 3 way vale" annotation(Dialog(
		group="Heat Pump 3-way valve",
		tab="Control",
		visible=false));
	Boolean MinControlHP3WV "Minimum Control overwrites input signal from the energy management system" annotation(Dialog(
		group="Heat Pump 3-way valve",
		tab="Control",
		visible=false));
	parameter Real deltaTLowDHW(quantity="Thermics.TempDiff")=2 "Required temperature difference between upper layer and DHW set temperature" annotation(Dialog(
		group="Heat Pump 3-way valve",
		tab="Control"));
	Boolean HP3WVinStandardControl "Switches between the inlet of the HP into the TS (true: upper storage connection)" annotation(Dialog(
		group="Heat Pump 3-way valve",
		tab="Control",
		visible=false));
	Modelica.Blocks.Interfaces.BooleanInput StandardControlHP3WV "Standard Control for the 3-way valve to switch the connection to the thermal storage" annotation(
		Placement(
			transformation(extent={{-135,-190},{-95,-150}}),
			iconTransformation(
				origin={-50,250},
				extent={{-20,-20},{20,20}},
				rotation=-90)),
		Dialog(
			group="Heat Pump 3-way valve",
			tab="Control",
			visible=false));
	parameter Real deltaTonST(quantity="Thermics.TempDiff")=3 "Switch-on temperature difference between collector and storage temperature" annotation(Dialog(
		group="Solar Thermal",
		__esi_groupCollapsed=true,
		tab="Control"));
	parameter Real deltaToffST(quantity="Thermics.TempDiff")=2 "Switch-off-temperature difference between collector and storage temperature" annotation(Dialog(
		group="Solar Thermal",
		tab="Control"));
	parameter Real deltaTFlowReturnLowST(quantity="Thermics.TempDiff")=3 "Minimum temperature difference between flow and return, qv=qvMin" annotation(Dialog(
		group="Solar Thermal",
		tab="Control"));
	parameter Real deltaTFlowReturnUpST(quantity="Thermics.TempDiff")=7 "Maximum temperature difference between flow and return, qv=qvMax" annotation(Dialog(
		group="Solar Thermal",
		tab="Control"));
	parameter Real qvMinST(
		quantity="Thermics.VolumeFlow",
		displayUnit="m³/h")=0 "Minimum volume flow of circulation pump" annotation(Dialog(
		group="Solar Thermal",
		tab="Control"));
	parameter Real qvMaxST(
		quantity="Thermics.VolumeFlow",
		displayUnit="l/min")=0.0003333333333333334 "Maximum volume flow of circulation pump" annotation(Dialog(
		group="Solar Thermal",
		tab="Control"));
	Boolean CPonST "Switch-on/off of solar thermal pump" annotation(Dialog(
		group="Solar Thermal",
		__esi_groupCollapsed=true,
		tab="Control",
		visible=false));
	Real qvRefST(quantity="Thermics.VolumeFlow") "Reference volume flow of solar thermal pump" annotation(Dialog(
		group="Solar Thermal",
		tab="Control",
		visible=false));
	Real TCollectorST(quantity="Basics.Temp") "Solar thermal collector temperature" annotation(Dialog(
		group="Solar Thermal",
		tab="Control",
		visible=false));
	Real TStorageSTConnection(quantity="Basics.Temp") "Temperature of solar thermal heat storage connection" annotation(Dialog(
		group="Solar Thermal",
		tab="Control",
		visible=false));
	Real TFlowST(quantity="Basics.Temp") "Flow temperature solar thermal" annotation(Dialog(
		group="Solar Thermal",
		tab="Control",
		visible=false));
	Real TReturnST(quantity="Basics.Temp") "Return temperature solar thermal" annotation(Dialog(
		group="Solar Thermal",
		tab="Control",
		visible=false));
	Modelica.Blocks.Interfaces.BooleanInput StandardControlST "If true, standard control will be used to control the solar thermal pump" annotation(
		Placement(
			transformation(extent={{-135,-120},{-95,-80}}),
			iconTransformation(
				origin={50,250},
				extent={{-20,-20},{20,20}},
				rotation=270)),
		Dialog(
			group="Solar Thermal",
			tab="Control",
			visible=false));
	parameter Boolean LowLevelControl_Battery=true "Use low level control for the battery, if true: P_grit,set =  BatPCharge, else P_bat = BatPCharge" annotation(Dialog(
		group="Battery",
		tab="Control"));
	Real BatPChargeSC(quantity="Basics.Power") "Charging or discharging power of the battery, calculated by the standard control" annotation(Dialog(
		group="Battery",
		tab="Control",
		visible=false));
	Modelica.Blocks.Interfaces.BooleanInput StandardControlBattery "If true, standard control will be used to charge and discharge the battery" annotation(
		Placement(
			transformation(extent={{-135,-415},{-95,-375}}),
			iconTransformation(
				origin={-150,250},
				extent={{-20,-20},{20,20}},
				rotation=-90)),
		Dialog(
			group="Battery",
			tab="Control",
			visible=false));
	Modelica.Blocks.Interfaces.RealInput BatPCharge(quantity="Basics.Power") "Charging or discharging power of the battery" annotation(
		Placement(
			transformation(extent={{-135,-445},{-95,-405}}),
			iconTransformation(extent={{-270,130},{-230,170}})),
		Dialog(
			group="Battery",
			tab="Control",
			visible=false));
	parameter Boolean UseCB=false "Use condensing boiler" annotation(Dialog(
		group="Condensing Boiler",
		tab="Heat Generator Parameters"));
	parameter Real CBDeltaT(quantity="Thermics.TempDiff")=20 "Temperature difference between flow and return temperature" annotation(Dialog(
		group="Condensing Boiler",
		tab="Heat Generator Parameters"));
	parameter Real PCB(quantity="Basics.Power")=14000 "Maximum Power of the condensing boiler" annotation(Dialog(
		group="Condensing Boiler",
		tab="Heat Generator Parameters"));
	parameter Boolean UseHP=true "Use heat pump" annotation(Dialog(
		group="Heat Pump",
		tab="Heat Generator Parameters"));
	parameter Real PAuxMax(quantity="Basics.Power")=9000 "Maximum power of the auxiliary heater" annotation(Dialog(
		group="Heat Pump",
		tab="Heat Generator Parameters"));
	parameter Real PHeatNom(quantity="Basics.Power")=5750 "Nominal heat output at A2/W35 (not maximum heat output)" annotation(Dialog(
		group="Heat Pump",
		tab="Heat Generator Parameters"));
	parameter Real PCoolingNom(quantity="Basics.Power")=6010 "Nominal cooling power at A35/W18 (not maximum cooling output)" annotation(Dialog(
		group="Heat Pump",
		tab="Heat Generator Parameters"));
	parameter Real COPNom=4.65 "Nominal COP at A2/W35" annotation(Dialog(
		group="Heat Pump",
		tab="Heat Generator Parameters"));
	parameter Real EERNom=5.92 "Nominal EER at A35/W18" annotation(Dialog(
		group="Heat Pump",
		tab="Heat Generator Parameters"));
	parameter Boolean UseST=false "Use solar thermal unit" annotation(Dialog(
		group="Solar Thermal Collector",
		tab="Heat Generator Parameters"));
	parameter Boolean CPC=true "If true, solar thermal collector is CPC collector, else, solar thermal collector is a flat plate collector" annotation(Dialog(
		group="Solar Thermal Collector",
		tab="Heat Generator Parameters"));
	parameter Real alphaModule(
		quantity="Geometry.Angle",
		displayUnit="°")=0.6108652381980153 "Inclination angle of solar thermal collector" annotation(Dialog(
		group="Solar Thermal Collector",
		tab="Heat Generator Parameters"));
	parameter Real betaModule(
		quantity="Geometry.Angle",
		displayUnit="°")=3.1415926535897931 "Orientation angle of solar thermal collector" annotation(Dialog(
		group="Solar Thermal Collector",
		tab="Heat Generator Parameters"));
	parameter Integer nSeries=1 "Number of solar thermal collectors in series" annotation(Dialog(
		group="Solar Thermal Collector",
		tab="Heat Generator Parameters"));
	parameter Integer nParallel=1 "Number of solar thermal collectors in parallel" annotation(Dialog(
		group="Solar Thermal Collector",
		tab="Heat Generator Parameters"));
	parameter Real AModule(
		quantity="Geometry.Area",
		displayUnit="m²")=1.312 "Effective surface area of solar thermal collector" annotation(Dialog(
		group="Solar Thermal Collector",
		tab="Heat Generator Parameters"));
	parameter Real VAbsorber(
		quantity="Geometry.Volume",
		displayUnit="l")=0.0017 "Absorber volume" annotation(Dialog(
		group="Solar Thermal Collector",
		tab="Heat Generator Parameters"));
	parameter Boolean UsePV=true "Use PV unit" annotation(Dialog(
		group="Parameters",
		tab="PV System"));
	parameter Real alphaPV(
		quantity="Geometry.Angle",
		displayUnit="°")=0.6108652381980153 "Inclination angle of the PV system" annotation(Dialog(
		group="Parameters",
		tab="PV System"));
	parameter Real betaPV(
		quantity="Geometry.Angle",
		displayUnit="°")=3.141592653589793 "Orientation angle of the PV system" annotation(Dialog(
		group="Parameters",
		tab="PV System"));
	parameter Real PVPeak(quantity="Basics.Power")=20000 "Installed peak power of the PV system" annotation(Dialog(
		group="Parameters",
		tab="PV System"));
	Real PV_P(quantity="Basics.Power") "PV power" annotation(Dialog(
		group="Results",
		tab="PV System",
		visible=false));
	Real PV_E(
		quantity="Basics.Energy",
		displayUnit="kWh") "Produced energy of the PV system" annotation(Dialog(
		group="Results",
		tab="PV System",
		visible=false));
	parameter Real VStorage(
		quantity="Basics.Volume",
		displayUnit="l")=0.785 "Volume of the thermal storage" annotation(Dialog(
		group="Thermal Storage",
		tab="Storage Parameters"));
	parameter Real Tmax_TS(quantity="Basics.Temp")=363.15 "Maximum temperature within the thermal storage" annotation(Dialog(
		group="Thermal Storage",
		tab="Storage Parameters"));
	parameter Real T0_TS(quantity="Basics.Temp")=303.15 "Temperature level to calculate the stored energy (e.g. return temperature of consumption)" annotation(Dialog(
		group="Thermal Storage",
		tab="Storage Parameters"));
	parameter Boolean TSLinearProfile=true "If true, the temperature profile at simulation begin within the storage is linear, if false the profile is defined by a temperature vector" annotation(Dialog(
		group="Thermal Storage",
		tab="Storage Parameters"));
	parameter Real TSTupInit(
		quantity="Thermics.Temp",
		displayUnit="°C")=338.15 if TSLinearProfile "Temperature of upmost heat storage layer at simulation begin" annotation(Dialog(
		group="Thermal Storage",
		tab="Storage Parameters"));
	parameter Real TSTlowInit(
		quantity="Thermics.Temp",
		displayUnit="°C")=313.14999999999998 if TSLinearProfile "Temperature of lowmost heat storage layer at simulation begin" annotation(Dialog(
		group="Thermal Storage",
		tab="Storage Parameters"));
	parameter Real TSTLayerVector[20](quantity="Basics.Temp")={313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15,313.15} if not TSLinearProfile "Vector of temperature profile of the layers at simulation begin, element 1 is at lowest layer" annotation(Dialog(
		group="Thermal Storage",
		tab="Storage Parameters"));
	parameter Boolean UseBat=true "Use battery" annotation(Dialog(
		group="Battery",
		tab="Storage Parameters"));
	parameter Real SOCInit(quantity="Basics.RelMagnitude")=0.5 "Initial state of charge" annotation(Dialog(
		group="Battery",
		tab="Storage Parameters"));
	parameter Real PMax(quantity="Basics.Power")=20000 "Maximum charge/discharge power" annotation(Dialog(
		group="Battery",
		tab="Storage Parameters"));
	parameter Real EMaxNominal(
		quantity="Basics.Energy",
		displayUnit="kWh")=36000000 "Nominal maximum energy content" annotation(Dialog(
		group="Battery",
		tab="Storage Parameters"));
	Real SOCBat(quantity="Basics.RelMagnitude") "State of charge of the electric storage" annotation(Dialog(
		group="Battery",
		tab="Storage Parameters",
		visible=false));
	Real PBattery(quantity="Basics.Power") "Battery input/output power" annotation(Dialog(
		group="Battery",
		tab="Storage Parameters",
		visible=false));
	Real EBatteryCharge(
		quantity="Basics.Energy",
		displayUnit="kWh") "Electrical energy for battery charging" annotation(Dialog(
		group="Battery",
		tab="Storage Parameters",
		visible=false));
	Real EBatteryDischarge(
		quantity="Basics.Energy",
		displayUnit="kWh") "Electrical energy for battery discharging" annotation(Dialog(
		group="Battery",
		tab="Storage Parameters",
		visible=false));
	Real Bat_cylces "Total number of battery cycles" annotation(Dialog(
		group="Battery",
		tab="Storage Parameters",
		visible=false));
	parameter Integer nPeople(quantity="Basics.Unitless")=6 "Number of people living in the building" annotation(Dialog(
		group="Size",
		tab="Consumption Parameters"));
	parameter Integer nFloors=2 "nFloors" annotation(Dialog(
		group="Size",
		tab="Consumption Parameters"));
	parameter Integer nApartments=2 "nApartments" annotation(Dialog(
		group="Size",
		tab="Consumption Parameters"));
	parameter Real ALH(
		quantity="Geometry.Area",
		displayUnit="m²")=300 "Heated (living) area (e.g. 50m² per person)" annotation(Dialog(
		group="Size",
		tab="Consumption Parameters"));
	parameter Real AdditionalInsulation(quantity="Basics.Length")=0 "Additional Insulation (cm)" annotation(Dialog(
		group="Size",
		tab="Consumption Parameters"));
	parameter Boolean UseStandardHeatNorm=false "If true, use standard area-specific heating power, else define it manually" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Real QHeatNormLivingArea(quantity="Thermics.HeatFlowSurf")=16 if not UseStandardHeatNorm "Area-specific heating power - modern radiators: 14 - 15 W/m²; space heating: 15 W/m²" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Real n=1.1 "Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Real TFlowHeatNorm(quantity="Basics.Temp")=313.15 "Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 - 45°C" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Real TReturnHeatNorm(quantity="Basics.Temp")=303.15 "Normal return temperature - radiator: 45 - 65°C; floor heating: 28 - 35°C" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Real TRef(quantity="Basics.Temp")=294.15 "Reference indoor temperature" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Real qvMaxLivingZone(quantity="Thermics.VolumeFlow")=0.00025 "Maximumg flow rate in Living Zone" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Real TLiving_Init(
		quantity="Basics.Temp",
		displayUnit="°C")=294.15 "Initial Temperature" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Real TRoof_Init(quantity="Basics.Temp")=274.65 "Initial Temperature" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Real TCellar_Init(quantity="Basics.Temp")=280.15 "Initial Temperature" annotation(Dialog(
		group="Heating System",
		tab="Consumption Parameters"));
	parameter Boolean ActivateCooling=false "If true, cooling system is activated" annotation(Dialog(
		group="Cooling",
		tab="Consumption Parameters"));
	parameter Real TRefCooling(quantity="Basics.Temp")=297.15 "Reference indoor temperature for cooling" annotation(Dialog(
		group="Cooling",
		tab="Consumption Parameters"));
	parameter Boolean UseUnixTimeIndividual=false "If true, use unix time for individual presence and electric consumption" annotation(Dialog(
		group="Additional Yields/Losses",
		tab="Consumption Parameters"));
	parameter Boolean UseIndividualPresence=true "If the presence is used, individual presence data has to be provided, else standart presence is used" annotation(Dialog(
		group="Additional Yields/Losses",
		tab="Consumption Parameters"));
	parameter String PresenceFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF2_WholeYear.txt" if UseIndividualPresence "File with presence timeseries (presence in %; 0% - no one is at home; 100% - everyone is at home)" annotation(Dialog(
		group="Additional Yields/Losses",
		tab="Consumption Parameters"));
	parameter Boolean UseIndividualElecConsumption=true "If individual electricity consumption is used, individual consumption data ha to be provided, else standart load profiles are used" annotation(Dialog(
		group="Additional Yields/Losses",
		tab="Consumption Parameters"));
	parameter String ElConsumptionFile=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF2_WholeYear.txt" if UseIndividualElecConsumption "File with electric consumption time series (consumption in kW)" annotation(Dialog(
		group="Additional Yields/Losses",
		tab="Consumption Parameters"));
	parameter String ElConsumptionTable="Pel" if UseIndividualElecConsumption "Table with electric consumption time series (consumption in W)" annotation(Dialog(
		group="Additional Yields/Losses",
		tab="Consumption Parameters"));
	parameter Real YearlyElecConsumption_kWh=7000 if UseIndividualElecConsumption "YearlyElecConsumption_kWh" annotation(Dialog(
		group="Additional Yields/Losses",
		tab="Consumption Parameters"));
	parameter Real ElFactor=YearlyElecConsumption_kWh/5010 if UseIndividualElecConsumption "ElFactor" annotation(Dialog(
		group="Additional Yields/Losses",
		tab="Consumption Parameters"));
	parameter Boolean ActivateNightTimeReduction=false "If true, night time reduction is activated, else temperature is constant" annotation(Dialog(
		group="Temperature Control",
		tab="Consumption Parameters"));
	parameter Real NightTimeReductionStart(
		quantity="Basics.Time",
		displayUnit="h")=82800 if ActivateNightTimeReduction "NightTimeReductionStart" annotation(Dialog(
		group="Temperature Control",
		tab="Consumption Parameters"));
	parameter Real NightTimeReductionEnd(
		quantity="Basics.Time",
		displayUnit="h")=25200 if ActivateNightTimeReduction "NightTimeReductionEnd" annotation(Dialog(
		group="Temperature Control",
		tab="Consumption Parameters"));
	parameter Real Tnight(quantity="Basics.Temp")=291.15 if ActivateNightTimeReduction "Temperature at night" annotation(Dialog(
		group="Temperature Control",
		tab="Consumption Parameters"));
	parameter Boolean VariableTemperatureProfile=false "If true, presence will be used to define the temperature (if less people are at home, less rooms are heated and the average temperature will decrease)" annotation(Dialog(
		group="Temperature Control",
		tab="Consumption Parameters"));
	parameter Real TMin(quantity="Basics.Temp")=292.15 if VariableTemperatureProfile "Minimum temperature, when noone is at home (TRefSet = TMin + (TRef - TMin) * Presence(t))" annotation(Dialog(
		group="Temperature Control",
		tab="Consumption Parameters"));
	parameter Boolean UseUnixTimeDHW=false "If true, use unix time for DHW consumption" annotation(Dialog(
		group="DHW consumption",
		tab="Consumption Parameters"));
	parameter Boolean WeeklyData=false "If true: DHW consumption data repeats weekly" annotation(Dialog(
		group="DHW consumption",
		tab="Consumption Parameters"));
	parameter String File=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\SF2_WholeYear.txt" "DHW Data File" annotation(Dialog(
		group="DHW consumption",
		tab="Consumption Parameters"));
	parameter String Table="V_DHW" "DHW Table Name" annotation(Dialog(
		group="DHW consumption",
		tab="Consumption Parameters"));
	parameter Real V_DHWperDay_l=300 "V_DHWperDay_l" annotation(Dialog(
		group="DHW consumption",
		tab="Consumption Parameters"));
	parameter Real DHWfactor=V_DHWperDay_l/300 "Factor, with which the DHW consumption gets multiplied" annotation(Dialog(
		group="DHW consumption",
		tab="Consumption Parameters"));
	protected
		Real CBIn "Input Signal of Condensing Boiler" annotation(
			HideResult=false,
			Dialog(tab="Results CB"));
	public
		Real CB_S_TM_VL(quantity="Basics.Temp") "Condensing boiler flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results CB",
			visible=false));
		Real CB_S_TM_RL(quantity="Basics.Temp") "Condensing boiler return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results CB",
			visible=false));
		Real CB_S_FW_HC(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow condensing boiler" annotation(Dialog(
			group="Volume Flow",
			tab="Results CB",
			visible=false));
		Real CB_S_FG(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Gas flow condensing boiler" annotation(Dialog(
			group="Volume Flow",
			tab="Results CB",
			visible=false));
		Real CB_VFuel(
			quantity="Basics.Volume",
			displayUnit="l") "Demanded fuel volume" annotation(Dialog(
			group="Volume Flow",
			tab="Results CB",
			visible=false));
		Real CB_P_heat_is(quantity="Basics.Power") "Heat output power of condensing boiler" annotation(Dialog(
			group="Power",
			tab="Results CB",
			visible=false));
		Real CB_P_gas_is(quantity="Basics.Power") "Gas power of condensing boiler" annotation(Dialog(
			group="Power",
			tab="Results CB",
			visible=false));
		Real CB_E_heat_produced(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of condensing boiler" annotation(Dialog(
			group="Energy",
			tab="Results CB",
			visible=false));
		Real CB_E_gas_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Gas input of condensing boiler" annotation(Dialog(
			group="Energy",
			tab="Results CB",
			visible=false));
		Real CB_Efficiency(quantity="Basics.RelMagnitude") "Heating efficiency" annotation(Dialog(
			group="Efficiency",
			tab="Results CB",
			visible=false));
		Real HP_S_TM_VL(quantity="Basics.Temp") "Heat pump flow temperature (output of the outdoor unit)" annotation(Dialog(
			group="Temperatures",
			tab="Results HP",
			visible=false));
		Real HP_S_TM_RL(quantity="Basics.Temp") "Heat pump return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HP",
			visible=false));
		Real HP_S_TM_HC_TL(quantity="Basics.Temp") "Heat pump flow temperature - Low temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HP",
			visible=false));
		Real HP_S_TM_HW_TH(quantity="Basics.Temp") "Heat pump flow temperature - High temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results HP",
			visible=false));
		Real HP_S_FW_HC(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow heat pump " annotation(Dialog(
			group="Volume Flow",
			tab="Results HP",
			visible=false));
		Real HP_P_heat_HP(quantity="Basics.Power") "Heat output power of the heat pump " annotation(Dialog(
			group="Power",
			tab="Results HP",
			visible=false));
		Real HP_P_heat_Aux(quantity="Basics.Power") "Heat output power of the auxiliary heater" annotation(Dialog(
			group="Power",
			tab="Results HP",
			visible=false));
		Real HP_P_heat_Tot(quantity="Basics.Power") "Total heat output power of the heat pump" annotation(Dialog(
			group="Power",
			tab="Results HP",
			visible=false));
		Real HP_P_cooling(quantity="Basics.Power") "Cooling output of the heat pump" annotation(Dialog(
			group="Power",
			tab="Results HP",
			visible=false));
		Real HP_P_elec_is(quantity="Basics.Power") "Total electricity demand of heat pump" annotation(Dialog(
			group="Power",
			tab="Results HP",
			visible=false));
		Real HP_E_heat_HP(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of the heat pump " annotation(Dialog(
			group="Energy",
			tab="Results HP",
			visible=false));
		Real HP_E_heat_Aux(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output power of the auxiliary heater" annotation(Dialog(
			group="Energy",
			tab="Results HP",
			visible=false));
		Real HP_E_heat_Tot(
			quantity="Basics.Energy",
			displayUnit="kWh") "Total heat output power of the heat pump" annotation(Dialog(
			group="Energy",
			tab="Results HP",
			visible=false));
		Real HP_E_cooling(quantity="Basics.Energy") "Cooling output of the heat pump" annotation(Dialog(
			group="Energy",
			tab="Results HP",
			visible=false));
		Real HP_E_elec_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Consumed electric energy of heat pump" annotation(Dialog(
			group="Energy",
			tab="Results HP",
			visible=false));
		Real HP_COP(quantity="Basics.Unitless") "Coefficiency of performance" annotation(Dialog(
			group="Efficiency",
			tab="Results HP",
			visible=false));
		Real HP_EER "Energy Efficiency Ratio (Cooling)" annotation(Dialog(
			group="Efficiency",
			tab="Results HP",
			visible=false));
		Real ST_S_TM_VL(quantity="Basics.Temp") "Solar thermal flow temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results ST",
			visible=false));
		Real ST_S_TM_RL(quantity="Basics.Temp") "Solar thermal return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Results ST",
			visible=false));
		Real ST_S_TM_Collector(quantity="Basics.Temp") "ST_S_TM_Collector" annotation(Dialog(
			group="Temperatures",
			tab="Results ST",
			visible=false));
		Real ST_S_FW_HC(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow solar thermal" annotation(Dialog(
			group="Volume Flow",
			tab="Results ST",
			visible=false));
		Real ST_P_heat_is(quantity="Basics.Power") "Heat output power of solar thermal" annotation(Dialog(
			group="Power",
			tab="Results ST",
			visible=false));
		Real ST_E_heat_produced(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of solar thermal" annotation(Dialog(
			group="Energy",
			tab="Results ST",
			visible=false));
		Real TS_S_TM_HC_VL(quantity="Basics.Temp") "Flow temperature consumption side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_RL(quantity="Basics.Temp") "Return temperature consumption side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_PS_VL(quantity="Basics.Temp") "Flow temperature producer side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_PS_RL(quantity="Basics.Temp") "Return temperature producer side" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_HW_VL(quantity="Basics.Temp") "Flow temperature to fresh water station" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_HC_HW_RL(quantity="Basics.Temp") "Return temperature to fresh water station" annotation(Dialog(
			group="Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_1(quantity="Basics.Temp") "Thermal storage temperature 1" annotation(Dialog(
			group="Storage Temperatures",
			__esi_groupCollapsed=true,
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_2(quantity="Basics.Temp") "Thermal storage temperature 2" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_3(quantity="Basics.Temp") "Thermal storage temperature 3" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_4(quantity="Basics.Temp") "Thermal storage temperature 4" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_5(quantity="Basics.Temp") "Thermal storage temperature 5" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_6(quantity="Basics.Temp") "Thermal storage temperature 6" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_7(quantity="Basics.Temp") "Thermal storage temperature 7" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_8(quantity="Basics.Temp") "Thermal storage temperature 8" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_9(quantity="Basics.Temp") "Thermal storage temperature 9" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_S_TM_BT_10(quantity="Basics.Temp") "Thermal storage temperature 10" annotation(Dialog(
			group="Storage Temperatures",
			tab="Results TS",
			visible=false));
		Real TS_E_Storage_BT(
			quantity="Basics.Energy",
			displayUnit="kWh") "Energy in thermal storage" annotation(Dialog(
			group="Energy",
			tab="Results TS",
			visible=false));
		Real TS_SOC_BT(quantity="Basics.RelMagnitude") "State of charge of the thermal storage" annotation(Dialog(
			group="State of charge",
			tab="Results TS",
			visible=false));
		Boolean HeatingSeason "If true, standard control is set to heating season (definition: heating was required within the last 24 hours)" annotation(Dialog(
			tab="Consumption Results",
			visible=false));
		Real time_heating_off(quantity="Basics.Time") "time since heating was used the last time" annotation(Dialog(
			tab="Consumption Results",
			visible=false));
		Real TRefSetCooling(quantity="Basics.Temp") "Reference cooling temperature" annotation(Dialog(
			group="Temperatures",
			tab="Consumption Results",
			visible=false));
		Real TRefSetHeating "Reference heating temperature" annotation(Dialog(
			group="Temperatures",
			tab="Consumption Results",
			visible=false));
		Real HS_S_TM_VL_bM(quantity="Basics.Temp") "Heat Sink flow temperature before mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Consumption Results",
			visible=false));
		Real HS_S_TM_VL_aM(quantity="Basics.Temp") "Heat Sink flow temperature after mixing unit" annotation(Dialog(
			group="Temperatures",
			tab="Consumption Results",
			visible=false));
		Real HS_S_TM_RL(quantity="Basics.Temp") "Heat sink return temperature" annotation(Dialog(
			group="Temperatures",
			tab="Consumption Results",
			visible=false));
		Real HS_S_TM_HW_VL(quantity="Basics.Temp") "Heat sink flow temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Consumption Results",
			visible=false));
		Real HS_S_TM_HW_RL(quantity="Basics.Temp") "Heat sink return temperature hot water" annotation(Dialog(
			group="Temperatures",
			tab="Consumption Results",
			visible=false));
		Real HS_S_TM_Room(quantity="Basics.Temp") "Temperature in the house" annotation(Dialog(
			group="Temperatures",
			tab="Consumption Results",
			visible=false));
		Real HS_S_FW_HC_aM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow after mixing unit" annotation(Dialog(
			group="Volume Flow",
			tab="Consumption Results",
			visible=false));
		Real HS_S_FW_HC_bM(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow before mixing unit" annotation(Dialog(
			group="Volume Flow",
			tab="Consumption Results",
			visible=false));
		Real HS_S_FW_HW_VL(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow domestic hot water consumption" annotation(Dialog(
			group="Volume Flow",
			tab="Consumption Results",
			visible=false));
		Real HS_P_DemHeatHC_is(quantity="Basics.Power") "Heating power" annotation(Dialog(
			group="Power",
			tab="Consumption Results",
			visible=false));
		Real HS_P_DemHeatHW_is(quantity="Basics.Power") "Domestic hot water power" annotation(Dialog(
			group="Power",
			tab="Consumption Results",
			visible=false));
		Real P_El_Grid(quantity="Basics.Power") "Electric power at the grid connection point" annotation(Dialog(
			group="Power",
			tab="Consumption Results",
			visible=false));
		Real HS_E_DemHeatHC_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heating Energy" annotation(Dialog(
			group="Energy",
			tab="Consumption Results",
			visible=false));
		Real HS_E_DemHeatHW_consumed(
			quantity="Basics.Energy",
			displayUnit="kWh") "Domestic hot water energy" annotation(Dialog(
			group="Energy",
			tab="Consumption Results",
			visible=false));
		Real E_El_Purchase(
			quantity="Basics.Energy",
			displayUnit="kWh") "Total purchased energy at the grid connection" annotation(Dialog(
			group="Energy",
			tab="Consumption Results",
			visible=false));
		Real E_El_Feedin(
			quantity="Basics.Energy",
			displayUnit="kWh") "Total feed-in energy at the grid connection" annotation(Dialog(
			group="Energy",
			tab="Consumption Results",
			visible=false));
		Modelica.Blocks.Interfaces.RealInput CBModulation(quantity="Basics.Power") if not CBControlMode "Condensing boiler modulation" annotation(
			Placement(
				transformation(extent={{-135,-70},{-95,-30}}),
				iconTransformation(extent={{-270,-170},{-230,-130}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput TDH_HEXout(quantity="Basics.Temp") "Outlet temperature of district heating heat exchanger" annotation(
			Placement(
				transformation(extent={{-135,70},{-95,110}}),
				iconTransformation(extent={{266.7,-170},{226.7,-130}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput DTH_HEXin(quantity="Basics.Temp") "Inlet temperature of district heating heat exchanger" annotation(
			Placement(
				transformation(extent={{-120,30},{-100,50}}),
				iconTransformation(extent={{236.7,-60},{256.7,-40}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealOutput qv_HEX(quantity="Thermics.VolumeFlow") "Volume flow to heat exchanger" annotation(
			Placement(
				transformation(extent={{-120,55},{-100,75}}),
				iconTransformation(
					origin={246.7,0},
					extent={{10,-10},{-10,10}},
					rotation=-180)),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput qvSTpump(quantity="Thermics.VolumeFlow") "Reference volume flow of solar thermal pump" annotation(
			Placement(
				transformation(extent={{-135,-145},{-95,-105}}),
				iconTransformation(extent={{-270,80},{-230,120}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput qvDHpump(quantity="Thermics.VolumeFlow") "Reference volume flow of district heating pump - positive: feed in - negative: extraction" annotation(
			Placement(
				transformation(extent={{-135,-10},{-95,30}}),
				iconTransformation(extent={{266.7,-220},{226.7,-180}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput HPModulation(quantity="Basics.Power") "Air source heat pump modulation in percent" annotation(
			Placement(
				transformation(extent={{-135,-335},{-95,-295}}),
				iconTransformation(extent={{-270,-120},{-230,-80}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput SwitchHP3WV "Switches between the inlet of the HP into the TS (true: upper storage connection)" annotation(
			Placement(
				transformation(extent={{-135,-215},{-95,-175}}),
				iconTransformation(extent={{-270,30},{-230,70}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput HPAuxModulation(quantity="Basics.Power") "Air source heat pump - modulation of the auxiliary heater (percent)" annotation(
			Placement(
				transformation(extent={{-135,-380},{-95,-340}}),
				iconTransformation(extent={{-270,-70},{-230,-30}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.BooleanInput HPMode "Heating mode (true) or cooling mode (false)" annotation(
			Placement(
				transformation(extent={{-135,-265},{-95,-225}}),
				iconTransformation(extent={{-270,-20},{-230,20}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput UnixTime "UnixTime" annotation(
			Placement(
				transformation(extent={{460,-60},{500,-20}}),
				iconTransformation(extent={{266.7,130},{226.7,170}})),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput T_ref_in_heat(quantity="Basics.Temp") "Reference input temperature for heating" annotation(
			Placement(
				transformation(extent={{480,-200},{520,-160}}),
				iconTransformation(
					origin={-150,-247},
					extent={{-20,-19.7},{20,20.3}},
					rotation=90)),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Interfaces.RealInput T_ref_in_cool(quantity="Basics.Temp") "Reference input temperature for cooling" annotation(
			Placement(
				transformation(extent={{480,-170},{520,-130}}),
				iconTransformation(
					origin={-100,-247},
					extent={{-20,-19.7},{20,20.3}},
					rotation=90)),
			Dialog(
				tab="Input Signals",
				visible=false));
		Modelica.Blocks.Sources.RealExpression T_ref_set_heating(y(
			quantity="Basics.Temp",
			displayUnit="K")=TRefSetHeating) annotation(Placement(transformation(extent={{500,-125},{480,-105}})));
		Modelica.Blocks.Sources.RealExpression T_ref_set_cooling(y(
			quantity="Basics.Temp",
			displayUnit="K")=TRefSetCooling) annotation(Placement(transformation(extent={{500,-105},{480,-85}})));
		Modelica.Blocks.Logical.Or or1 annotation(Placement(transformation(extent={{-50,-405},{-30,-385}})));
		Modelica.Blocks.Sources.BooleanExpression booleanExpression1(y=LowLevelControl_Battery) annotation(Placement(transformation(extent={{-90,-425},{-70,-405}})));
	equation
		// Results
		// Condensing Boiler
		CB_S_TM_VL = wolfCGB20_GC1.TFlow;
		CB_S_TM_RL = wolfCGB20_GC1.TReturn;
		CB_S_FW_HC = wolfCGB20_GC1.qv;
		CB_S_FG = wolfCGB20_GC1.qvFuel;
		CB_VFuel = wolfCGB20_GC1.VFuel;
		CB_P_heat_is = wolfCGB20_GC1.QHeat;
		CB_P_gas_is = wolfCGB20_GC1.PFuel;
		CB_E_heat_produced = wolfCGB20_GC1.EHeat;
		CB_E_gas_consumed = wolfCGB20_GC1.EFuel;
		CB_Efficiency = wolfCGB20_GC1.Efficiency;
		
		// Heat Pump
		HP_S_TM_VL = wolfCHA1.THPout;
		HP_S_TM_RL = wolfCHA1.TReturn;
		HP_S_TM_HC_TL = switchPort1.Out2.T;
		HP_S_TM_HW_TH = switchPort1.Out1.T;
		HP_S_FW_HC = wolfCHA1.wolfCHA1.VWater;
		HP_P_heat_HP = wolfCHA1.QHeat_HP;
		HP_P_heat_Aux = wolfCHA1.QHeat_Aux;
		HP_P_heat_Tot = wolfCHA1.QHeat_HP + wolfCHA1.QHeat_Aux;
		HP_P_cooling = wolfCHA1.PCooling;
		HP_P_elec_is = wolfCHA1.PEl_HP + wolfCHA1.PEl_Aux;
		HP_E_heat_HP = wolfCHA1.EHeat_HP;
		HP_E_heat_Aux = wolfCHA1.EHeat_Aux;
		HP_E_heat_Tot = wolfCHA1.EHeat_HP + wolfCHA1.EHeat_Aux;
		HP_E_cooling = wolfCHA1.ECooling;
		HP_E_elec_consumed = wolfCHA1.EEl_HP + wolfCHA1.EEl_Aux;
		HP_COP = wolfCHA1.COP;
		HP_EER = wolfCHA1.EER;
		
		// Solar Thermal Collectors
		ST_S_TM_VL = WolfCRK12.FlowST.T;
		ST_S_TM_RL = WolfCRK12.ReturnST.T;
		ST_S_TM_Collector = WolfCRK12.TCollector;
		ST_S_FW_HC = WolfCRK12.FlowST.qv;
		ST_E_heat_produced = WolfCRK12.EST;
		ST_P_heat_is = WolfCRK12.QSolarThermal;
		
		//Thermal Storage
		TS_S_TM_HC_VL = hydraulicSwitch_TS_DH_HG1.TSupplyDH;
		TS_S_TM_HC_RL = hydraulicSwitch_TS_DH_HG1.TReturnDH;
		TS_S_TM_PS_VL = hydraulicSwitch_TS_DH_HG1.TSupplyHG;
		TS_S_TM_PS_RL = hydraulicSwitch_TS_DH_HG1.TReturnHG;
		TS_S_TM_HC_HW_VL = WolfBSP800.FlowOut8.T;
		TS_S_TM_HC_HW_RL = WolfBSP800.ReturnIn8.T;
		TS_S_TM_BT_1 = WolfBSP800.TStorage[1];
		TS_S_TM_BT_2 = WolfBSP800.TStorage[2];
		TS_S_TM_BT_3 = WolfBSP800.TStorage[3];
		TS_S_TM_BT_4 = WolfBSP800.TStorage[4];
		TS_S_TM_BT_5 = WolfBSP800.TStorage[5];
		TS_S_TM_BT_6 = WolfBSP800.TStorage[6];
		TS_S_TM_BT_7 = WolfBSP800.TStorage[7];
		TS_S_TM_BT_8 = WolfBSP800.TStorage[8];
		TS_S_TM_BT_9 = WolfBSP800.TStorage[9];
		TS_S_TM_BT_10 = WolfBSP800.TStorage[10];
		TS_E_Storage_BT = WolfBSP800.cpMed*WolfBSP800.rhoMed*WolfBSP800.VStorage*((WolfBSP800.TLayer[1]+WolfBSP800.TLayer[2]+WolfBSP800.TLayer[3]+WolfBSP800.TLayer[4]+WolfBSP800.TLayer[5]+WolfBSP800.TLayer[6]+WolfBSP800.TLayer[7]+WolfBSP800.TLayer[8]+WolfBSP800.TLayer[9]+WolfBSP800.TLayer[10])/10-T0_TS);
		TS_SOC_BT = TS_E_Storage_BT/(WolfBSP800.cpMed*WolfBSP800.rhoMed*WolfBSP800.VStorage*(Tmax_TS - T0_TS));
		
		// Heat Sink (Heating)
		HS_S_TM_VL_bM = heatingUnitFlowTemperature1.FlowSupply.T;
		HS_S_TM_VL_aM = heatingUnitFlowTemperature1.FlowSink.T;
		HS_S_TM_RL = heatingUnitFlowTemperature1.ReturnSupply.T;
		HS_S_TM_Room = simpleHeatedBuilding1.TZone[2];
		HS_S_FW_HC_aM = - heatingUnitFlowTemperature1.FlowSupply.qv;
		HS_S_FW_HC_bM = heatingUnitFlowTemperature1.FlowSink.qv;
		HS_P_DemHeatHC_is = simpleHeatedBuilding1.QHeat;
		HS_E_DemHeatHC_consumed = simpleHeatedBuilding1.EHeat;
		
		// Heat Sink (Domestic Hot Water)
		HS_S_TM_HW_VL = dHW_demand1.FlowHotWater.T;
		HS_S_TM_HW_RL = dHW_demand1.ReturnHotWater.T;
		HS_S_FW_HW_VL = dHW_demand1.ReturnHotWater.qv;
		HS_P_DemHeatHW_is = dHW_demand1.Q_DHW;
		HS_E_DemHeatHW_consumed = dHW_demand1.E_DHW;
		
		// PV
		PV_P = -grid1.PGrid[6];
		PV_E = -grid1.EGridfeed[6];
		
		// Battery
		SOCBat = stationaryBattery_GC1.SOCAct;
		PBattery = stationaryBattery_GC1.PBattery;
		EBatteryCharge = stationaryBattery_GC1.EBattCharge;
		EBatteryDischarge = stationaryBattery_GC1.EBattDischarge;
		Bat_cylces = ceil(EBatteryDischarge / EMaxNominal);
		
		
		// Electricity
		P_El_Grid = grid3.P;
		E_El_Purchase = grid3.ESupply;
		E_El_Feedin = grid3.EFeed;
	equation
		// Control
		
		// check if heating is active
		when not simpleHeatedBuilding1.Heating then
			time_heating_off = time;
		end when;
		if (time - time_heating_off) > 86400 and not simpleHeatedBuilding1.Heating then
			HeatingSeason = false;
		else
			HeatingSeason = true;
		end if;
		
		
		// Overwrite CB Signal
		if StandardControlCB or (MinControlCB and MinimumControlEMS) then
			OverwriteCB = true;
		else
			OverwriteCB = false;
		end if;
		
		// Standard Control CB
		when ((TS_S_TM_BT_9 < TStartCB) or ((TS_S_TM_BT_5 < heatingUnitFlowTemperature1.TRef - 5) and HeatingSeason)) and UseCB then
			// T_TS9 < TStart
			// or (T_TS5 < TFlowRefHeating + 5) during heating season
			CBinStandardControl = 10;
			MinControlCB = true;
		elsewhen (TS_S_TM_BT_2 > TStopCB) or (TS_S_TM_BT_7 > TStopCB and not HeatingSeason) or (TS_S_TM_BT_9 > TStartCB and rem(UnixTime, dt_ems) < 0.1 and not StandardControlCB) then
			// T_TS2 > TStop
			// or (T_TS7 > TStop) if no heating season
			// or StartCondition is not valid (Minimum Control reset, when standard control is deactivated)
			CBinStandardControl = 0;
			MinControlCB = false;
		end when;
		
		// Overwrite HP 3way valve Signal
		if StandardControlHP3WV or (MinControlHP3WV and MinimumControlEMS) then
			OverwriteHP3WV = true;
			dhw = true;
		else
			OverwriteHP3WV = false;
			dhw = false;
		end if;
		
		// Standard Control HP 3way valve
		when (TS_S_TM_BT_9 < TStartHPhigh) and UseHP then 
			// T_TS9 < TStartHigh
			HP3WVinStandardControl = true;
			MinControlHP3WV = true;
		elsewhen (TS_S_TM_BT_7 > TStopHPhigh) or ((TS_S_TM_BT_9 > TStartHPhigh) and rem(UnixTime, dt_ems) < 0.1 and not StandardControlHP) then
			// T_TS7 > TStopHigh
			// or StartCondition is not valid (Minimum Control reset, when standard control is deactivated)
			HP3WVinStandardControl = false;
			MinControlHP3WV = false;
		end when;
		// Overwrite HP Signal
		if StandardControlHP or not UseHP or (MinControlHP and MinimumControlEMS) then
			OverwriteHP = true;
			heating = true;
		else
			OverwriteHP = false;
			heating = false;
		end if;
		
		// Standard Control HP
		when (HP3WVinStandardControl or ((((TS_S_TM_BT_4 < TStartHPlow) and (HPModulation < 0.3)) or (TS_S_TM_BT_4 < (TStartHPlow - 5))) and HeatingSeason)) and UseHP then
		//when (HP3WVinStandardControl or ((TS_S_TM_BT_4 < TStartHPlow) and HeatingSeason)) and UseHP then
			// T_TS9 < TStartHigh
			// T_TS4 < TStartLow during heating season
			HPinStandardControl = 1;
			MinControlHP = true;
		elsewhen (not HP3WVinStandardControl and ((TS_S_TM_BT_2 > TStopHPlow) or not HeatingSeason)) or (not HP3WVinStandardControl and ((TS_S_TM_BT_4 > TStartHPlow) or not HeatingSeason) and rem(UnixTime, dt_ems) < 0.1 and not StandardControlHP) then
			// T_TS2 > TStopHigh and // T_TS7 > TStopHighLow
			// T_TS7 > TStopHigh if no heating season
			// or StartCondition is not valid (Minimum Control reset, when standard control is deactivated)
			HPinStandardControl = 0;
			MinControlHP = false;
		end when;
		when ((TS_S_TM_BT_9 < TStartAUXhigh) or ((TS_S_TM_BT_4 < TStartAUXlow) and HeatingSeason)) and UseHP then
			// T_TS9 < TStartHigh
			// T_TS4 < TStartLow during heating season
			AUXinStandardControl = 1;
		elsewhen (not HP3WVinStandardControl and ((TS_S_TM_BT_2 > TStopAUXlow) or not HeatingSeason)) or (not HP3WVinStandardControl and (TS_S_TM_BT_4 > TStartAUXlow) and rem(UnixTime, dt_ems) < 0.1 and not StandardControlHP) then
			// T_TS2 > TStopHigh and // T_TS7 > TStopHighLow
			// T_TS7 > TStopHigh if no heating season
			// or StartCondition is not valid (Minimum Control reset, when standard control is deactivated)
			AUXinStandardControl = 0;
		end when;
		
		HPMode_SC = HPMode or heating or dhw;
		
		// Standard Control Battery
		if LowLevelControl_Battery and UseBat then
			// Power flow at the grid coupling point according to setpoint
			BatPChargeSC = BatPCharge - grid1.P;
		elseif StandardControlBattery and UseBat then
			// No power flow at the grid coupling point
			BatPChargeSC = -grid1.P;
		else
			BatPChargeSC = 0;
		end if;
		
		// Solar thermal controller (copied from Green City's 'SolarController')
		TCollectorST = WolfCRK12.TCollector;
		TStorageSTConnection = TS_S_TM_BT_5;
		TFlowST = WolfCRK12.FlowST.T;
		TReturnST = WolfCRK12.ReturnST.T;
		when (((TCollectorST - TStorageSTConnection) > deltaTonST) and StandardControlST) and UseST then
			CPonST = true;
		elsewhen (((TCollectorST - TStorageSTConnection) < deltaToffST) or not StandardControlST) then		
			CPonST = false;
		end when;
		if CPonST then
			qvRefST = max(min((qvMinST + (qvMaxST - qvMinST) * ((((TFlowST - TReturnST) - deltaTFlowReturnLowST) / (deltaTFlowReturnUpST - deltaTFlowReturnLowST)))),qvMaxST),qvMinST);
		else
			qvRefST = 0;
		end if;
		
		if StandardTRefControl then
			TRefSetCooling = TRefCooling;
			if ActivateNightTimeReduction then
				if (((environmentConditions1.HourOfDay * 3600) < NightTimeReductionStart) and ((environmentConditions1.HourOfDay * 3600) > NightTimeReductionEnd)) then
					if VariableTemperatureProfile then
						TRefSetHeating = TMin + (TRef - TMin) * simpleHeatedBuilding1.presenceTable.y[1];
					else
						TRefSetHeating = TRef;
					end if;
				else
					TRefSetHeating = Tnight;
				end if;
			else
				if VariableTemperatureProfile then
					TRefSetHeating = TMin + (TRef - TMin) * simpleHeatedBuilding1.presenceTable.y[1];
				else
					TRefSetHeating = TRef;
				end if;
			end if;
		else
			TRefSetHeating = T_ref_in_heat;
			TRefSetCooling = T_ref_in_cool;
		end if;
	initial equation
		// enter your equations here
		time_heating_off = 0;
		
		// Solar thermal controller (copied from Green City's 'SolarController')
		assert(deltaTonST > deltaToffST, "Switch-on-temperature difference lower than switch-off-temperature difference");
		assert((deltaTonST > 0) and (deltaToffST > 0), "Switching temperature differences must be greater than 0");
		assert(deltaTFlowReturnUpST > deltaTFlowReturnLowST, "Volume Flow Control Temperature Difference are out of limits");
		if (((TCollectorST - TStorageSTConnection) > deltaTonST) and StandardControlST) then
			CPonST = true;
		else
			CPonST = false;
		end if;
		
		// Heat pump 3way valve
		if (TS_S_TM_BT_9 < TStartHPhigh) and UseHP then 
			HP3WVinStandardControl = true;
			MinControlHP3WV = true;
		else
			HP3WVinStandardControl = false;
			MinControlHP3WV = false;
		end if;
		
		// Heat pump
		if ((TS_S_TM_BT_9 < TStartHPhigh) or (TS_S_TM_BT_4 < TStartHPlow)) and UseHP then
			HPinStandardControl = 1;
			MinControlHP = true;
		else
			HPinStandardControl = 0;
			MinControlHP = false;
		end if;
		if ((TS_S_TM_BT_9 < TStartAUXhigh) or (TS_S_TM_BT_4 < TStartAUXlow)) and UseHP then
			AUXinStandardControl = 1;
		else
			AUXinStandardControl = 0;
		end if;
		
		// Condensing boiler
		if ((TS_S_TM_BT_10 < TStartCB) or (TS_S_TM_BT_5 < heatingUnitFlowTemperature1.TRef - 5)) and UseCB then
			CBinStandardControl = 10;
			MinControlCB = true;
		else
			CBinStandardControl = 0;
			MinControlCB = true;
		end if;
	equation
		connect(phaseTap1.Grid1,heatingUnitFlowTemperature1.Grid1) annotation(Line(
			points={{275,-340},{280,-340},{284.3,-340},{284.3,-125},{284.3,-120}},
			color={247,148,29},
			thickness=0.015625));
		connect(grid1.LVGridA,grid2.LVMastGrid) annotation(Line(
			points={{195,-340},{190,-340},{180,-340},{180,-330},{180,-325}},
			color={247,148,29},
			thickness=0.0625));
		connect(extractVolumeFlow1.qvMedium,defineVolumeFlow1.qvMedium) annotation(Line(
			points={{210,25},{210,30},{230,30},{230,25}},
			color={0,0,127},
			thickness=0.0625));
		connect(extractVolumeFlow1.TMedium,DTH_HEXin) annotation(Line(
			points={{200,25},{200,30},{200,40},{-105,40},{-110,40}},
			color={0,0,127},
			thickness=0.0625));
		connect(TDH_HEXout,defineVolumeFlow1.TMedium) annotation(Line(
			points={{-115,90},{-110,90},{240,90},{240,30},{240,25}},
			color={0,0,127},
			thickness=0.0625));
		connect(WolfBSP800.FlowIn2,pump2.PumpOut) annotation(Line(
			points={{190,-155},{185,-155},{115,-155},{115,-125},{110,-125}},
			color={190,30,45},
			thickness=0.0625));
		connect(extractVolumeFlow1.qvMedium,qv_HEX) annotation(Line(
			points={{210,25},{210,30},{210,65},{-105,65},{-110,65}},
			color={0,0,127},
			thickness=0.0625));
		connect(phaseTap7.Grid1,pump2.Grid1) annotation(Line(
			points={{135,-305},{130,-305},{107.3,-305},{107.3,-135},{107.3,-130}},
			color={247,148,29},
			thickness=0.0625));
		connect(TColdWater.y,dHW_demand1.TColdWater) annotation(Line(
			points={{444,-220},{439,-220},{405,-220},{405,-209.7},{405,-204.7}},
			color={0,0,127},
			thickness=0.0625));
		connect(grid2.LVGridB,phaseTap7.Grid3) annotation(Line(
			points={{160,-305},{155,-305},{150,-305},{145,-305}},
			color={247,148,29},
			thickness=0.0625));
		connect(qvSTpump_StandardControl.y,switch2.u1) annotation(Line(
			points={{-99,-80},{-94,-80},{-62,-80},{-62,-92},{-57,-92}},
			color={0,0,127},
			thickness=0.0625));
		connect(qvSTpump,switch2.u3) annotation(Line(
			points={{-115,-125},{-110,-125},{-62,-125},{-62,-108},{-57,-108}},
			color={0,0,127},
			thickness=0.0625));
		connect(switch2.y,pump2.qvRef) annotation(Line(
			points={{-34,-100},{-29,-100},{105,-100},{105,-115},{105,-120}},
			color={0,0,127},
			thickness=0.0625));
		connect(wolfCHA1.environmentConditions,environmentConditions1) annotation(Line(
			points={{45,-235},{45,-230},{45,30},{485,30},{490,30}},
			color={192,192,192},
			thickness=0.0625));
		connect(grid1.LVGridB,wolfCHA1.lV3Phase) annotation(Line(
			points={{195,-355},{190,-355},{45,-355},{45,-284.7},{45,-279.7}},
			color={247,148,29},
			thickness=0.0625));
		connect(logicalSwitch1.y,switchPort1.SwitchPort) annotation(Line(
			points={{31,-170},{36,-170},{145,-170},{145,-175},{145,-180}},
			color={255,0,255},
			thickness=0.0625));
		connect(SwitchSignal_StandardControl.y,logicalSwitch1.u1) annotation(Line(
			points={{-14,-150},{-9,-150},{3,-150},{3,-162},{8,-162}},
			color={255,0,255},
			thickness=0.0625));
		connect(grid2.LVGridD,phaseTap5.Grid3) annotation(Line(
			points={{200,-290},{205,-290},{235,-290},{240,-290}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid2.LVGridE,phaseTap6.Grid3) annotation(Line(
			points={{200,-305},{205,-305},{240,-305},{245,-305}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid2.LVGridF,phaseTap2.Grid3) annotation(Line(
			points={{200,-320},{205,-320},{250,-320},{255,-320}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid1.LVGridD,phaseTap1.Grid3) annotation(Line(
			points={{235,-340},{240,-340},{260,-340},{265,-340}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid1.LVGridF,pV2ACInverter1.LVGrid3) annotation(Line(
			points={{235,-370},{240,-370},{335,-370},{335,-385},{340,-385}},
			color={247,148,29},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH_HG1.qvDistrictHeating,qvDHpump) annotation(Line(
			points={{195,-15},{195,-10},{195,10},{-110,10},{-115,10}},
			color={0,0,127},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH_HG1.StorageChargeFlow,WolfBSP800.FlowIn1) annotation(Line(
			points={{185,-29.7},{185,-34.7},{185,-140},{190,-140}},
			color={190,30,45}));
		connect(WolfBSP800.ReturnOut1,hydraulicSwitch_TS_DH_HG1.StorageChargeReturn) annotation(Line(
			points={{190,-145},{185,-145},{180,-145},{180,-34.7},{180,-29.7}},
			color={190,30,45}));
		connect(WolfBSP800.FlowOut6,hydraulicSwitch_TS_DH_HG1.StorageDischargeFlow) annotation(Line(
			points={{225,-140},{230,-140},{235,-140},{235,-34.7},{235,-29.7}},
			color={190,30,45}));
		connect(hydraulicSwitch_TS_DH_HG1.StorageDischargeReturn,WolfBSP800.ReturnIn6) annotation(Line(
			points={{240,-29.7},{240,-34.7},{240,-150},{230,-150},{225,-150}},
			color={190,30,45}));
		connect(hydraulicSwitch_TS_DH_HG1.HeatExchangerIn,extractVolumeFlow1.Pipe) annotation(Line(
			points={{205,-15},{205,-10},{205,0},{205,5}},
			color={190,30,45}));
		connect(hydraulicSwitch_TS_DH_HG1.HeatExchangerOut,defineVolumeFlow1.Pipe) annotation(Line(
			points={{215,-15},{215,-10},{215,0},{235,0},{235,5}},
			color={190,30,45},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH_HG1.FeedInPumpOut,phaseTap5.Grid1) annotation(Line(
			points={{235,-15},{235,-10},{255,-10},{255,-290},{250,-290}},
			color={247,148,29},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH_HG1.ExtractionPumpOut,phaseTap6.Grid1) annotation(
			Line(
				points={{230,-15},{230,-60},{260,-60},{260,-305},{255,-305}},
				color={247,148,29},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(WolfBSP800.ReturnIn7,heatingUnitFlowTemperature1.ReturnSupply) annotation(
			Line(
				points={{225,-174.6666564941406},{230,-174.7},{250,-174.7},{250,-111},{280,-111.3333282470703}},
				color={190,30,45},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(WolfBSP800.FlowOut7,heatingUnitFlowTemperature1.FlowSupply) annotation(
			Line(
				points={{225,-165},{230,-165},{245,-165},{245,-93.7},{280,-93.66667175292969}},
				color={190,30,45}),
			__esi_AutoRoute=false);
		connect(CBin_StandardControl.y,switch4.u1) annotation(Line(
			points={{41,-10},{46,-10},{48,-10},{48,-17},{53,-17}},
			color={0,0,127},
			thickness=0.0625));
		connect(HPModulation,switch3.u3) annotation(Line(
			points={{-115,-315},{-110,-315},{-57,-315},{-57,-293},{-52,-293}},
			color={0,0,127},
			thickness=0.0625));
		connect(wolfCHA1.HPModulation,switch3.y) annotation(Line(
			points={{10,-255},{5,-255},{-24,-255},{-24,-285},{-29,-285}},
			color={0,0,127},
			thickness=0.0625));
		connect(HPin_StandardControl.y,switch3.u1) annotation(Line(
			points={{-69,-265},{-64,-265},{-57,-265},{-57,-277},{-52,-277}},
			color={0,0,127},
			thickness=0.0625));
		connect(HPAuxModulation,switch5.u3) annotation(Line(
			points={{-115,-360},{-110,-360},{-62,-360},{-62,-358},{-57,-358}},
			color={0,0,127},
			thickness=0.0625));
		connect(switch5.y,wolfCHA1.HPAuxModulation) annotation(Line(
			points={{-34,-350},{-29,-350},{5,-350},{5,-260},{10,-260}},
			color={0,0,127},
			thickness=0.0625));
		connect(AUXin_StandardControl.y,switch5.u1) annotation(Line(
			points={{-69,-340},{-64,-340},{-62,-340},{-62,-342},{-57,-342}},
			color={0,0,127},
			thickness=0.0625));
		connect(measureThermal1.PipeOut,dHW_demand1.FlowHotWater) annotation(Line(
			points={{365,-190},{370,-190},{385,-190},{390,-190}},
			color={190,30,45}));
		connect(switch2.u2,StandardControlST) annotation(Line(
			points={{-57,-100},{-62,-100},{-110,-100},{-115,-100}},
			color={255,0,255},
			thickness=0.0625));
		connect(wolfCGB20_GC1.ControlIn,switch4.y) annotation(Line(
			points={{90,-25},{85,-25},{81,-25},{76,-25}},
			color={0,0,127},
			thickness=0.0625));
		connect(wolfCGB20_GC1.Return,hydraulicSwitch_TS_DH_HG1.HeatGeneratorReturn) annotation(Line(
			points={{134.7,-30},{139.7,-30},{170,-30},{170,-25},{175,-25}},
			color={190,30,45},
			thickness=0.0625));
		connect(hydraulicSwitch_TS_DH_HG1.HeatGeneratorSupply,wolfCGB20_GC1.Flow) annotation(Line(
			points={{175,-20},{170,-20},{139.7,-20},{134.7,-20}},
			color={190,30,45},
			thickness=0.0625));
		connect(grid2.LVGridA,wolfCGB20_GC1.lV3Phase) annotation(Line(
			points={{160,-290},{155,-290},{125,-290},{125,-54.7},{125,-49.7}},
			color={247,148,29},
			thickness=0.0625));
		connect(environmentConditions1,wolfCGB20_GC1.environmentConditions) annotation(Line(
			points={{490,30},{485,30},{125,30},{125,0},{125,-5}},
			color={192,192,192},
			thickness=0.0625));
		connect(CBModulation,gain1.u) annotation(Line(
			points={{-115,-50},{-110,-50},{-22,-50},{-17,-50}},
			color={0,0,127},
			thickness=0.0625));
		connect(gain1.y,switch4.u3) annotation(Line(
			points={{6,-50},{11,-50},{48,-50},{48,-33},{53,-33}},
			color={0,0,127},
			thickness=0.0625));
		connect(Overwrite_CB.y,switch4.u2) annotation(Line(
			points={{6,-25},{11,-25},{48,-25},{53,-25}},
			color={255,0,255},
			thickness=0.0625));
		connect(Overwrite_HP3WV.y,logicalSwitch1.u2) annotation(Line(
			points={{-14,-170},{-9,-170},{3,-170},{8,-170}},
			color={255,0,255},
			thickness=0.0625));
		connect(greaterThreshold1.y,wolfCHA1.HPOn) annotation(Line(
			points={{-4.7,-235},{0.3,-235},{5,-235},{5,-240},{10,-240}},
			color={255,0,255},
			thickness=0.0625));
		connect(greaterThreshold1.u,switch3.y) annotation(Line(
			points={{-16,-235},{-21,-235},{-24,-235},{-24,-285},{-29,-285}},
			color={0,0,127},
			thickness=0.0625));
		connect(Overwrite_HP.y,switch3.u2) annotation(Line(
			points={{-69,-285},{-64,-285},{-57,-285},{-52,-285}},
			color={255,0,255},
			thickness=0.0625));
		connect(switch5.u2,Overwrite_HP.y) annotation(Line(
			points={{-57,-350},{-62,-350},{-64,-350},{-64,-285},{-69,-285}},
			color={255,0,255},
			thickness=0.0625));
		connect(heatingUnitFlowTemperature1.qvRef,simpleHeatedBuilding1.qv_RefHeating) annotation(Line(
			points={{306.3,-85},{306.3,-80},{395,-80},{395,-85}},
			color={0,0,127},
			thickness=0.0625));
		connect(environmentConditions1,simpleHeatedBuilding1.EnvironmentConditions) annotation(Line(
			points={{490,30},{485,30},{410,30},{410,-80},{410,-85}},
			color={192,192,192},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.UnixTime,UnixTime) annotation(Line(
			points={{415,-85},{415,-80},{415,-40},{475,-40},{480,-40}},
			color={0,0,127},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.Grid3,grid1.LVGridE) annotation(Line(
			points={{415,-125},{415,-130},{415,-355},{240,-355},{235,-355}},
			color={247,148,29},
			thickness=0.0625));
		connect(switchPort2.In,wolfCHA1.Flow) annotation(Line(
			points={{85,-240},{80,-240},{59.7,-240},{59.7,-250},{54.7,-250}},
			color={190,30,45},
			thickness=0.0625));
		connect(wolfCHA1.Return,switchPort2.Out) annotation(Line(
			points={{54.7,-260},{59.7,-260},{80,-260},{85,-260}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort1.In,switchPort2.Out1) annotation(Line(
			points={{135,-185},{130,-185},{110,-185},{110,-240},{105,-240}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort2.In1,switchPort1.Out) annotation(Line(
			points={{104.7,-245},{109.7,-245},{130,-245},{130,-205},{135,-205}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort1.Out1,WolfBSP800.FlowIn4) annotation(Line(
			points={{155,-185},{160,-185},{185,-185},{185,-184.7},{190,-184.7}},
			color={190,30,45}));
		connect(WolfBSP800.ReturnOut4,switchPort1.In1) annotation(Line(
			points={{190,-189.7},{185,-189.7},{159.7,-189.7},{159.7,-190},{154.7,-190}},
			color={190,30,45}));
		connect(switchPort1.Out2,WolfBSP800.FlowIn5) annotation(Line(
			points={{154.7,-200},{159.7,-200},{185,-200},{185,-199.7},{190,-199.7}},
			color={190,30,45}));
		connect(WolfBSP800.ReturnOut5,switchPort1.In2) annotation(Line(
			points={{190,-204.7},{185,-204.7},{159.7,-204.7},{159.7,-205},{154.7,-205}},
			color={190,30,45}));
		connect(switchPort2.Out2,switchPort3.In1) annotation(
			Line(
				points={{104.6666717529297,-255},{109.7,-255},{335,-255},{335,-110},{345.3333435058594,-110}},
				color={190,30,45}),
			__esi_AutoRoute=false);
		connect(switchPort2.In2,switchPort3.Out1) annotation(Line(
			points={{104.7,-260},{109.7,-260},{340,-260},{340,-115},{345,-115}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort3.Out,simpleHeatedBuilding1.PipeIn) annotation(Line(
			points={{365,-95},{370,-95},{380,-95},{385,-95}},
			color={190,30,45}));
		connect(switchPort3.In,simpleHeatedBuilding1.PipeOut) annotation(Line(
			points={{365,-115},{370,-115},{380,-115},{380,-110},{385,-110}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort3.In2,heatingUnitFlowTemperature1.FlowSink) annotation(Line(
			points={{345.3,-95},{340.3,-95},{320,-95},{320,-93.7},{315,-93.7}},
			color={190,30,45},
			thickness=0.0625));
		connect(switchPort3.Out2,heatingUnitFlowTemperature1.ReturnSink) annotation(Line(
			points={{345.3,-100},{340.3,-100},{320,-100},{320,-111.3},{315,-111.3}},
			color={190,30,45}));
		connect(simpleHeatedBuilding1.T_Ref,heatingUnitFlowTemperature1.TRef) annotation(
			Line(
				points={{400,-85},{400,-75},{293,-75},{293,-85}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(simpleHeatedBuilding1.Cooling,switchPort3.SwitchPort) annotation(Line(
			points={{390,-124.7},{390,-129.7},{355,-129.7},{355,-125},{355,-120}},
			color={255,0,255},
			thickness=0.0625));
		connect(measureThermal1.TMedium,dHW_demand1.TPipe) annotation(Line(
			points={{360,-185},{360,-180},{405,-180},{405,-185}},
			color={0,0,127},
			thickness=0.0625));
		connect(dHW_demand1.EnvironmentConditions,environmentConditions1) annotation(
			Line(
				points={{409.6666870117188,-190},{414.7,-190},{435,-190},{435,30},{490,30}},
				color={192,192,192},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(WolfBSP800.FlowOut8,measureThermal1.PipeIn) annotation(Line(
			points={{225,-189.7},{230,-189.7},{350,-189.7},{350,-190},{355,-190}},
			color={190,30,45}));
		connect(dHW_demand1.ReturnHotWater,WolfBSP800.ReturnIn8) annotation(Line(
			points={{390,-200},{385,-200},{230,-200},{230,-199.7},{225,-199.7}},
			color={190,30,45}));
		connect(WolfCRK12.FlowST,pump2.PumpIn) annotation(Line(
			points={{90,-125},{95,-125},{100,-125}},
			color={190,30,45}));
		connect(WolfBSP800.ReturnOut2,WolfCRK12.ReturnST) annotation(Line(
			points={{190,-160},{185,-160},{95,-160},{95,-145},{90,-145}},
			color={190,30,45}));
		connect(environmentConditions1,WolfCRK12.EnvironmentConditions) annotation(Line(
			points={{490,30},{485,30},{45,30},{45,-135},{50,-135}},
			color={192,192,192},
			thickness=0.0625));
		connect(photovoltaic_GC1.DC,pV2ACInverter1.DCPV) annotation(Line(
			points={{390,-385},{385,-385},{380,-385}},
			color={247,148,29},
			thickness=0.0625));
		connect(pV2ACInverter1.MPP,photovoltaic_GC1.MPP) annotation(Line(
			points={{360,-405},{360,-410},{410,-410},{410,-405}},
			color={0,0,127},
			thickness=0.0625));
		connect(environmentConditions1,photovoltaic_GC1.EnvironmentConditions) annotation(Line(
			points={{490,30},{485,30},{435,30},{435,-385},{430,-385}},
			color={192,192,192},
			thickness=0.0625));
		connect(stationaryBattery_GC1.ControlBus,batt2ACInverter1.ControlBus) annotation(Line(
			points={{85,-440},{85,-445},{140,-445},{140,-440},{140,-435}},
			color={229,229,0},
			thickness=0.0625));
		connect(stationaryBattery_GC1.DC,batt2ACInverter1.DCBatt) annotation(Line(
			points={{105,-420},{110,-420},{120,-420},{125,-420}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid3.LVMastGrid,lV3Phase1) annotation(
			Line(
				points={{250,-440},{250,-445},{250,-465}},
				color={247,148,29},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(grid1.LVMastGrid,grid3.LVGridA) annotation(Line(
			points={{215,-375},{215,-380},{215,-405},{225,-405},{230,-405}},
			color={247,148,29},
			thickness=0.0625));
		connect(grid3.LVGridB,batt2ACInverter1.LVGrid3) annotation(Line(
			points={{230,-420},{225,-420},{160,-420},{155,-420}},
			color={247,148,29},
			thickness=0.0625));
		connect(BatPCharge,switch1.u3) annotation(Line(
			points={{-115,-425},{-110,-425},{18,-425},{18,-403},{23,-403}},
			color={0,0,127},
			thickness=0.0625));
		connect(switch1.y,batt2ACInverter1.PRef) annotation(Line(
			points={{46,-395},{51,-395},{140,-395},{140,-400},{140,-405}},
			color={0,0,127},
			thickness=0.0625));
		connect(Battery_StandardControl.y,switch1.u1) annotation(Line(
			points={{-19,-375},{-14,-375},{18,-375},{18,-387},{23,-387}},
			color={0,0,127},
			thickness=0.0625));
		connect(SwitchHP3WV,logicalSwitch1.u3) annotation(Line(
			points={{-115,-195},{-110,-195},{3,-195},{3,-178},{8,-178}},
			color={255,0,255},
			thickness=0.0625));
		connect(dHW_demand1.UnixTime,UnixTime) annotation(
			Line(
				points={{409.6666870117188,-200},{434.7,-200},{455,-200},{455,-40},{480,-40}},
				color={0,0,127},
				thickness=0.015625),
			__esi_AutoRoute=false);
		connect(T_ref_set_heating.y,simpleHeatedBuilding1.T_ref_in_heat) annotation(Line(
			points={{479,-115},{474,-115},{429.7,-115},{429.7,-105},{424.7,-105}},
			color={0,0,127},
			thickness=0.0625));
		connect(T_ref_set_cooling.y,simpleHeatedBuilding1.T_ref_in_cool) annotation(Line(
			points={{479,-95},{474,-95},{429.7,-95},{424.7,-95}},
			color={0,0,127},
			thickness=0.0625));
		connect(switchPort2.SwitchPort,Overwrite_HPMode.y) annotation(Line(
			points={{95,-235},{95,-230},{95,-225},{-39,-225},{-44,-225}},
			color={255,0,255},
			thickness=0.0625));
		connect(wolfCHA1.HPMode,Overwrite_HPMode.y) annotation(Line(
			points={{10,-245},{5,-245},{-39,-245},{-39,-225},{-44,-225}},
			color={255,0,255},
			thickness=0.0625));
		connect(StandardControlBattery,or1.u1) annotation(Line(
			points={{-115,-395},{-110,-395},{-57,-395},{-52,-395}},
			color={255,0,255},
			thickness=0.0625));
		connect(or1.y,switch1.u2) annotation(Line(
			points={{-29,-395},{-24,-395},{18,-395},{23,-395}},
			color={255,0,255},
			thickness=0.0625));
		connect(booleanExpression1.y,or1.u2) annotation(Line(
			points={{-69,-415},{-64,-415},{-57,-415},{-57,-403},{-52,-403}},
			color={255,0,255},
			thickness=0.0625));
	annotation(
		__esi_WolfCRK1(
			ReturnST(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowST(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ToFlow(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
		__esi_WolfSPU2_800(
			FromFlow1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FlowIn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
		__esi_wolfCGB14(
			Return(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			Flow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			measureThermal1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			condensingBoiler_DZ1(
				ReturnCB(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FlowCB(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				ToFlow(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromReturn(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))))),
		__esi_distributionValve1(
			PipeIn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeOutRegulated(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeOutRemain(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FromPipe(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToPipe2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToPipe1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
		__esi_mergingValve1(
			PipeOut(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeIn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			PipeIn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FromPipe1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromPipe2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToPipe(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")))),
		__esi_neoTower2(
			Return(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			Flow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			cHP_DZ1(
				ReturnCHP(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FlowCHP(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromReturn(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToFlow(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))))),
		__esi_hydraulicSwitch2HG1(
			HeatExchangerIn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorSupply2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorReturn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatExchangerOut(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorSupply1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			HeatGeneratorReturn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageChargeReturn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageChargeFlow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageDischargeFlow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageDischargeReturn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			DemandFlow(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			DemandReturn(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			StorageChargeOut(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FeedInPump(
				PumpIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PumpOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				ToPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			FeedInValve(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			DemandValve(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureThermal3(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			DHFeedIn_Extract(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve3(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve4(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureMixed(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			HeatGeneratorValve1(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve5(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureHGSupply1(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureDHSupply(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			DistrictHeatingValve1(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			StorageChargeIn(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeIn(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeOut(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageChargeIn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageChargeOut1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeOut1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			StorageDischargeIn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			measureDemandSupply(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureDemandReturn(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			ExtractionPump1(
				PumpIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PumpOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				ToPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPump(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			measureHGSupply2(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			mergingValve6(
				PipeOut(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn1(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeIn2(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				FromPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			HeatGeneratorValve2(
				PipeIn(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRegulated(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				PipeOutRemain(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")),
				FromPipe(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe2(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"))),
				ToPipe1(Pipe(
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo"),
					viewinfo[0](
						tabGroupAlignment=589834,
						typename="ModelInfo")))),
			viewinfo[0](
				fMin=1,
				fMax=100,
				nf=100,
				kindSweep=1,
				expDataFormat=0,
				expNumberFormat="%.7lg",
				expMatrixNames={
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																"A","B","C","D","E"},
				typename="AnaLinSysInfo")),
		__esi_WolfSEM1_500(
			FromFlow1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn1(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow2(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromFlow3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToReturn3(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow4(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn5(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			ToFlow6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FromReturn6(Pipe(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
			FlowIn1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut1(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut2(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowIn3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnOut3(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn4(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn5(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			FlowOut6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo")),
			ReturnIn6(
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"),
				viewinfo[0](
					tabGroupAlignment=589834,
					typename="ModelInfo"))),
		__esi_hydraulicSwitch_TS_DH_HG1(viewinfo[0](
			fMin=1,
			fMax=100,
			nf=100,
			kindSweep=1,
			expDataFormat=0,
			expNumberFormat="%.7lg",
			expMatrixNames={
							"A","B","C","D","E"},
			typename="AnaLinSysInfo")),
		Icon(
			coordinateSystem(extent={{-250,-250},{250,250}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAALMAAACTCAIAAABK2r+SAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAScQAAEnEB89x6jgAAKA9JREFUeF7tnQdYU1cbx2/YGwUEURRBQAFx74FUnICooGBVFJXaWnH7
URRFEAiiAiIoe8reMxB2Etwb6xattto6C8oO43uTe0BGQLAICeT/nIfnjnPDzT2/+57/e+4I1sgV
V6zEJaPxzZs3paWlaIarJg10MiIiIhYuXGhkZEQmk9EirpgauGRUVFR4eXmpqqpiGMbPzz9p0qSY
mJiGhga0esBrgJLx+vXrPXv2jBgxArBYtWrV0qVLeXh4xo4de+LEibKyMlRpYGsgknHr1q3ly5cP
HjxYVFTUwsIiLy8vPT3d2NiYl5d32LBhsOT58+eo6gDWgCMjKytr+vTpgoKCCgoKdnZ2NBrt8uXL
ly5dys3NtbS0lJaWFhcXX7x48fXr19EGA1UDi4xz585pampCbICOw8/P7yJTAAcInzh+/DiEDQEB
AaAnLi4ObTYgNVDIgLzUysoKjAX4CV1d3djYWIgTFy5cwLHABXAUFRX5+/uDGwV6xowZ4+rqWldX
hz5igGlAkPHo0aMtW7bIyMjw8fFt3rw5MzMTehCAABHRQrAQiImPjzc0NASGgKSDBw+CXUUf1KTq
0k8PE1PzD9v/eeEKWtTv1P/JoFAo8+fPFxMTA8t55MiRwsJCPDYgFtoJh4NEIgFDQkJC4DxMTEyK
i4vRxzU20quqHsYmeyppOkoOCpitfS86sa6mFq3rR+rPZDQ0NERGRmppafHz84OxcHNzY9iKJmPR
iQAOqAZI2djYDB8+HPiYM2cOlUrFP/bd7w+i9VfbE4SdMSki/2BPVc3LJz1rPpXja/uN+i0Z1dXV
RCJRSUkJHMO8efPCw8OhB2ljLDoXjtHp06dVVVWhGxo/fnxYWBjQ9u/T58lmFnYYvzM2+Dg2hMgz
2E1+VM6+g6XPX6D/3S/UP8n4+++/t23bNmTIEGhRcAzQNXRkLDoXkAQ9CwQeiBlAmKKiIiS6FZ8/
v7//iLRtN5FvkCMm7gJwEAYfF5eNN97w6soNtAecr35IxrVr15YuXSoqKgrewtLSsqCgAFr3G7Bo
Fm47TE1NIZsF2rZv3/6spAQiROFhh5Myw49hIhA5oGdxEhgUtmDZH3nUxn4xwt7fyIB0FMI+hArw
By4uLnDSQ4+AWvhbhXtSsK779+8H2oA5PT09+OTqf8vuhkZ5qWkdw4SPYzJQHPkkzk2Ycjs4vOYz
x9uO/kMGnU738fFRUVGBbHPixImhoaFXrlzplrHoXEAYIOLs7Dxq1CjoWSZNmpSYmFhPp/+RRwmZ
s9ABEyViUhA8HHnEXYeOpBwhlr9+g/aMM9VPyHj9+rWVldXQoUMJBAJ0JQkJCYDFf+lBWAo4g48N
Dg6eMWMG/CM1NTVfP9/q6qp/bhbHG60HOJwYnlTWiSAJPUvalh3/PnmG9o8D1R/IePr06dq1a0VE
RMAHbN68OS8v7+rVq6gxe1pAG/gYiBYLFy7EMExGRubAgQPv3r4te/lXzt5DzsIyjpgEwEHEBjsS
xCIWGf518SraS04Tx5MBrQWJA5zBw4YNs7W1BUMAaQjeit9JOBzAn7m5OdgOQUFBExOTR48eVZd+
un7G111hdJMnlYYJn3FTHyWm1dfS0e5yjjiYjLq6uvPnz48dOxawGDNmjIeHB4QKIAM14HcWdCvg
PKytrfFBd21t7dzcXHpl1YPYZJ+J044RRIAM4APgcJUfdcXtbOX7j40cdVsQR5LR0NBQVlbm6OgI
xgLM4Ny5cyMjI69fv96DfrMrAjIgPrm7uwOdYHs1NDQiIiLACL+kXYpYtMKJTxL6FOhZHDAxFwm5
3P02/5b80VBfj74D24vzyIBQAaH7119/lZKSAmNhbGxMIpEgvPe43+yKgEX41+Hh4To6OhA5lJWV
Xd3c3oJ+f5BuscNl0FBwowxPikkCHwnGZv/cvFNP54yLtxxGRm1tLZVKNTAwEBYWlpaW3rFjB/T3
ENhRQ/WFgEjoxZKSklasWAF7NXjwYNgrYPfT6zeUI45u8qOceAAOhu2wxwSD5y54Ssqurahk/9Ew
jiEDepCqqipIR6dOnQpnJxgLJycnPJ6jJupT4Z4UIpm8vDzsnpGRESypKvt82z/snOYkJ35J3HbY
Y8JnlDWKQ6KqPv7bUM/WdHAGGfX19e/evfP29oYeHXoQMBZ+fn64B0QtwwbC98fBwUFdXR3gmD17
dmpqamVlZQk5L/SHJURBKXwozAETPzloOM3epfT5ywY2vi2IA8gAT1dSUvLbb7/JysqKioouXbo0
KioKzshe9ptdEZABO+bl5TVt2jTIZlVVVf0DAv4t/ffvW8UJaza5iMkSeRiXZx0xCUc+idRNv7y5
/TvbJrTsTkZ1dTUkHWZmZtCFy8nJ4XdkwZI+8ZtdEewY7F50dDR+VQ+/D/lpScm7h09y9li7KYxy
4h3kjA2BtMUBE43SM/rzwmXIddkwoWVrMsrLyzMyMnR1dYWEhOD8O3jwIIVC+X7jmz0lfCgMCAag
ZWRkBg0aBEDDbpf+/ebq6XNntSYSBaBbkWF6UiG/STPvRSdUvv/Abgktm5IBxuLDhw9hYWHjx48H
LCZPnuzq6godea8NZP13wd7m5+fv379fRUWFn59/8eLF2dnZFRUVDxNTg+cscBaSwW0HwOGuMPqS
y+myl3+xFRzsSAZuLBwdHUeOHCkmJgbHNCQkBM65rxqLS7RMNMUewlE+efLklClTwHZMnz49Oibm
w8ePz3Mo0fqrT0gMJRJwTyoGUST3wOH3Dx7V09nFdrAdGWAsbt68uWPHDmmmTE1NU1JSAIv2xuI+
ldgSBZiuKxRtA8cFWh6a6iPhntTX1xe/S1lRURFAefHixdv7jzJ+3u02VAn3pERskAMmEr9mw1+X
rtZVV6Nj0adiLzLAWOTl5a1cuRLOMGVlZUtLSzKZDEcWHebWKqNqvaKZoBka7TH1UGMhdpvqg+aZ
KqOOf0hlPIjWh7rA9KRxcXGrV68G2yEpKblv3747d+6Uvn5DPersqaZB5JdyZtqOY5hQ4DTtxymk
6rJPfe5J2YWMhoYGMBaRkZGzZ88GYzFp0iQHBwc4rBCQ8ePbXu9oOnUUkeao8IE2q4HG84SyH58F
vaXpNlJ4ILSg+b7QpYsXLl5gRDvgG3zGTz/9NGLECAgeQAm4kE/vP9wJOu8/c46zoDRzKEzmGCZy
WkH1ll/I51d/963tYAsy6urq/vzzTw8PD4gTkOnp6Oj4+PjgA0f48QUVU8/cpAahGab+ov7YSCE8
o/4K08BHPUWg4S6hlDYZX/sH9Wf6dVH6BbE2UaSXdeXSxcsXkT26fPkylUq1sbHR0tKCoDh37tzk
5OTS0tKSrNyIpYYnxMB2MHoWyGZPSQ3P/83u/YPHfQhH35MBxuL27dtWVlaysrJSUlKGhoYQOW7c
uNHGWFynhtMpEsAHmqfRXlA2N97Eai9IAjHQZdRdEW0oIdRRRa/QUh7QjtEvSzQ8I9TRhK/R4vD6
QE/vxw9mzPhinPHbRzw9PQELYWHhiRMn+vj6/vXq1evrt1M3/nJqiAKRZxDA4YSJw0TSus2vrlxn
jHb0hfqYjMrKysLCQhMTEwiwkIlYWFhkZGR0NGLxhGrVQOF9RD2Mz5ZQdtbfEGh4QqgpkqmmDml4
SAAU6m/zlVInAi5ACcyC88ArA1gV1FGvaSvx2bbKzk6OCjpz5owTkrOzs+fZc+dC4xIyclEVhvLJ
qRHeqEoX5BkYldZyc6Ygw4IvGBoaumzZssGDB8vLyx+ysXn48OGHp8/zrY96KI8h8kLkYOS0YDuC
dZa8unStTyJHn5EBxgICKeQdixYtgtA6fvx4CLMUCqUTYwF6T9NuoPLWUGXAb4KlqLsqDM3f8Duh
4SpP43Ws8TLWeANruEpoeMDAouExoZYqCTHjb5phPYW/nKaKPqW1yJGn7XfvWrdkyvDhwzEkXj6+
EUoqKnNWGm+zdvENj0ftmxHh+tMsVKUL0tDf7h6Lb9lKEA4hKKampm7evBlsh4iIyLZt2yCclH8s
ve7l5zt5BpF/MNgOJ0yCOHTEw/jUhroBQ0Z9ff1ff/3l7e0NThOCKoRWMBkQZr86kAU9BfQXEB4Y
TBTy1l0RZABxEYNupaGY0HCf0HCHAHAwllzHYBZiBnQo9bf4gAzYFn1KCyX5HbVcrIBakpX4hcW1
Fq2z9WPC0UNk4AJPCibU2toav0wIISQtLa2isvJRYlr4In2iwCCi2GDKYacayFP6Qn1ABp1Of/To
0cGDB+F0gXCqp6cXEhLSxm92IkYAuM3HCAl3CY1FTCBgun0pZq69zZiG+rAV2r6lyOG/TCbgrUjg
kVLUnD9/KVNLliyaM3PcaAl8FSYgMXLu5rNp0Okkhx63xKvg+mHm+FFSqBYmLqs0aR5awdRW65Nh
sFXHgpMBBF0X/rIXxsMQYaHv3r17deVGjLFJxrbd5f+87av0tbfJqKmpgcAAyRvkINDFmpubQ4fS
+RUy6A7AeDYXMJv1VKGGR4TGC1jD09Y0tClPmXUeIVuKPq6FyJ6mCgQmGQRe2QnmR8+kp+MrKJT8
xGgPW1MteWaTY5joMDVjYiq+sqWSfA6bTER1MOW5ax3C0YouC84HsB0BAQFLliyBYwJnC4BSUlLy
/uGTijfvoc9FB67X1atkVFVVQecKQQKCp6qq6t69e3NycjoayGoWIzstxBj9wgVR+kVxKHUXRRou
YchMdF6gziWsjiICJuMNbSmkM5DWwvRzys/wyYFbRzaBwaf8U6uUGJSfleJltQJp3ZYdbiw6hv9O
Bgi/ApeUlLR69eohQ4bIyMj88ssvcLZAcEUHri/US2TgA1mBgYGTJ08GLCB4wpkBRwRiKTo8nQrC
xjuaDp0mDgGA0d63CQxj0ZKAjgvDk95ssiCXGITBR+EhxGejHE4G9CX8k4z2OTp5hsYnksn4P+2K
eoQMXBA5srKydu7cOXr0aH5+/pUrV+bl5fUhHL1BBvjNZ8+eAQqQlwoJCYHVguAJB6KLxqJZwEcV
TR7sZ+NVjJGPtIOAdfmdWf8pgX5V7BNNs+VwWfrZH1UQGgAHgXe4wnidFSs3bbLcvXuvo6NnaOhX
KelBMkBwTKhUKqS7+KO52traUVFR5eV984jsdycDsIDACFmZrKystLT0mjVrEhMTIXh2Yiw60SVa
JsDRSMMgI21LQEflMQHqQ7x5QrVCn/JFkU4rpqrIoJb9Il5eAQWFCTo6K83M9+xzOhue2BEgPUsG
CEwYmHEvLy/AAu9zXV1d//nnH3Q0e1HflwzAAhIzQ0ND+JIQJCFUQsCEMwMdhm4KYsYH2iz6JQno
Edo2f6cF6tdelfiLthZ9UEtleBMPrFyxQEWmPR8M8QmJjpq8xGT7Se8ktEEr9TgZIPxJBYgWxsbG
IiIicDpZWVndu3cPHdPe0nckA/xmeHj4/PnzeXl5p0yZQiQSIVR2PpDVkaALYFw/owlDpspo6YsY
PuLZpQK9D9R/Rqi7IVxLlQQfep9KbH11PiM90tnKarO5ufEqXd2pqqOlpVFbI/ELSU5YaOGdiKq3
0PcgAxfAQSKRtm7dKicnJyYmBrG2+XVQvaPvRQYk5S4uLmpqamCmAA4wFhAnQeh7d0dvqYvw3ASS
VUZick2o8RoGdrJV83dSmGNfjIknTOtKY4SQ+sL2A19UKgUQifR2cba23rp186rl85S/ECIko7J0
f1i7TuX7kQECe15YWGhjY6OhoUEgEHR0dNLT02tre+ltcd+FDAh9+Pt48cvN0dHRcAZ8863e0Ilc
pzKOOLRlMfXMK6pJXZFQ4+Uu5yZQ8xaBXiRWRxGFzgjyVfiQFliwtDtUSmFKYqDDbn1tZdTs/LKq
2r+dR6ub9V3JAAEc4NPxK3DwHyD0ggUp7ZXf3Oh5MgBzIyMjQUFByMt37dqFP1qIvmhPqJQ2mZFu
QNbKHN/8SoEgcR2jU8TaXMFnKMJp3Y9rTRlau27f2Qy0tIUoeakB9oZN/kNaaYKlL1rTrO9NBgjI
gC4YvxkdMjsFBQWwHb3wZvSeJAOS75iYGDDV+Hv/jxw5Al+siyMWXRQ0MJz9jCYvYQxhdTg0jhdY
C3VKCPQr4s1XaL8o0GIEHw/eprxyU6xZtGluSoStviReRUBOdcGhSLSiWb1ABggfCktNTd2yZQtE
YnFx8fXr18OBRcf9+6jHyHjz5s3p06e1tLSgR5w3b97Zs2cB9m8zFh0JT1kZg10PCPU3BOouCjde
xRqvsHKjDwiwnHFRDR8Ze0qg0yTbGoussG2T+JoGunhV5lnae4Z/GR4vSIzzddi1dsIwZgVMRFZt
hU1MDr72i3qHDFyQ0+Xm5h4+fHjEiBHg3pYsWZKSkoKO/ndQz5Dxxx9/QMcBewzhDroSCH1AdI8/
Q/aJpt5AY1w1raAqMm7OoAgyTOU9xsWRRgq6BM/4C3YVQsU9Jh+FGP2yGCOjuUeooCm2uX848Zzp
xKaRLgKPtJLWTF1dA6b09ZfNnztJVR5dUxORVtLfHfJdrpt0S3CmwVE9ceIEGA4+Pr5p06b5+/t/
/vwZNUOPqgfIgF4Q+mpJSUkIdJBlZWVlwZJvG8jqRGA8y6jjH1MP4a0LAYBxXe0Zof6mQDVNroo6
jGE77hDqr/HXXmXczcUo9wnlNFXIUcF4gv1soPBVUYe2hiPJx+bXhRObb8tgJX5hKc3Fvx4JZXnR
tJfJAAEZwEdwcPCyZct4eXlVVVUdHBxevXqFGqPn9F/JSEhIWLhwoYCAgJKSEhgLCHffNmLRXUF7
110SBQPxJ3X9BVoeutvvCQEIeErbh+BgkoE2YHoUSEzaWVFyuK/rkR07TBdNanHnDgZn5KhRo2fN
XWm+3+H4ubD0drdm4ep9MkAAB4TkxMREExMTOCHl5eXhhLx79y5qkh7St5NRWVnp4+MzefJkPKxB
ZkWlUnvWWHSil9SN0FM0+0p0h/BtAn4/H4KjNRmdiEomJ0X4u7u72zcJTsQzZ7yCQ+M6YgJXbkZc
0Bm0ib3rueCEblyN+y/CL0ZCeN67dy9kK2JiYitWrCgoKEBt0xP6RjJevHgBVkhFRQUCmr6+fkRE
BPjNHjcWnegjdeYL6mY0wxRA0FDEB90NPvs3zbDhCk8XyeBQARxAw/Hjx9XU1MDhQVYYHh7eU5dn
v4UMyKA2btwoJycH6dOmTZuSkpJgF3vcWHSucopKm1wDjyLNzxDA2jqqcP8mA4Rfr4bgPWfOHEhY
xo0bB/7048ePqKn+g7pNRl5eHuRL+K3e1tbWZDIZepBexoKliqlngIxrTc8QgJ5RfwXTimb6ryBU
Ax9RUVGrV68WFhZWVFQ8cODAkydPUIN9q7pBRlVVVVhYGP4MGc4mhULBmWUHgdUopUxAM01qO4bR
TwVnJjREenq6paWljIwMJIlACSxBLfdN6ioZb9++tbOzw+9y1tHRCQpiOPzeNBZcfVWAAkR0yBDB
doiIiCxatCgzM7P+W59V6RIZjx8/xi8HS0lJ7dq1Cywx7AQXCzYU3igxMTF6enoQ2qdOnert7V1R
UYEasjv6OhmFhYWQEUlISAwdOtTDw+Ply5e///47OxgLrtoLHzj4888/ISfA38WupKR06NCh9j8S
+FV1RgYEIqAPjAX+BCZ4HDC9dXV1d+/ehT1A+8IVOwnaBcIGcFBbW3v//v2DBw/itgNyyTt37qB2
7Zo6JOPDhw+nTp3S0NCAXGj58uVgNiEoNTQ0AC5cMthWOBn4YDmcw//88w/0JpCt4O9EBBeCN25X
xJqMP/74Y//+/QoKCtBXbd++vbi4uPlWIi4Z7KyWZIDgTC4rK0tISJg5c6agoOCMGTMiIiKqu/ZO
HxZk3Lx5E3IecXFxeXl5W1vbkpISoA+t45LB3mpDBq6qqipYuGrVKgj/kF26urpCh4DWday2ZGRn
Zy9YsAD4gn4kODj43bt3bdIeLhnsLJZkgOh0OvgMCwsLyCSGDBmyc+fOZ8++8ntNX8ioqakBFMaP
Hw9kLVu2DP4HsNb+uUouGeysjsgAQeB/8eKFo6Pj8OHDIaUwNja+caOz3xBFZLx588bOzg6MBeQ5
+CXdji7McMlgZ3VCBgjO80+fPkVGRo4bN05AQGDevHnp6ekdNTQiA4wJ9EBAhoODw8uXL9v0IC3F
JYOd1TkZuCCZKCoqwm/8+fHHHx88eIBWtBYiAxrbw8ODTCaXl5e370FaiksGO6srZICgEe/fvw9W
FCJCR88oIDKABuiHOgkVzeKSwc7qIhmgr7b4FwfaRXHJYGd1nYyviktGvxKXDK5Yi0sGV6zFJaOf
60LRNx5eLhn9XPTkH9BUN8WRZOTkFWblFiSTcmNSs7NyC9HS1go7cCCW2Jc/LcAmqk+cj6a6KY4h
IzuvMDQhy8E/ZY97nIVLzHqHqBVHIvWOxDkGsLhxN+HUKVchoWjmE/QDXDVJOk8f3EAz3REHkHE+
kXzEJ9HUPkrvUKSeXdJyxwx9+2QD+xR9u5QVx7OPtHvrVUpoaLicXAaGeRkYxLm7Z2e2ejR5QIme
vb42cU5d0T403x2xNRmxadl7PBKWHYpc7pAGNOjbJenZJiw7ErfsSDz+d7kTydanFRmkxMQgZeXL
GFaIYQBHGD+/r6Ki3+LFMW5uudnZqNKAUV38HCj0+HlovjtiUzIKCinQTRgejjJwSAcgltniKLQp
bcnISU8Pmj6dhmFQKBhWhGEXMAwoycewCAzzHzcu8tAhCoWCavd30ZMW0ONmM8tctKg7YkcyMnMK
drrFG9inGtgnN4UHlqUtGUFr1+Zg2EVmwGhZABRYCCUB+Jg6NdnPD23Qr0WPnUWPnYkXtKg7Yjsy
Usn5W47HGjik6R3FO452xTYeVkEg0bdLXuGSc7jFGxTjicRIAQG8K2lfIIoAHJcwLERMLHz/l99I
65eix8ygx06rjZkOhR47Ay3tjtiLjPwCyvZTccsd0pv8RGsa7JPBfhocS11qE6NvEwHF0D7Rwa+V
zwjbsSO1KWwACi3JwAuV2b9A8AjftAlt0+9Ej55ZGzWN3lRgGq3ojtiIDJiARAMyDr2jic1M6NnG
6x9LgZQEplfbRe5yi7P2Sjx9Pj08iRydkp1Ozs8vbBVyCgoK/LW1oflxt3GFaTVw29GSD1ieQCBE
7NqFNutHepjlRo+YRI+YUhsxqalMQeu6IzYiIz49d9nBCAgMCAtgwj5Zzy55lW3U/84kBMSQUrO6
9Fu6mcnJocrK6RjmMXy4/8iR4D3zmnxGSz4AjvMCAkne3mizfqF7ZM/a8InNhR4+gX5+Yt2lY2h1
d8RGZBw6mwiuE7ecEDb0j6Ua20U5BiQnkTp9Vw0rJbq6nuTjC7e2JpNI0U5OAevX+yooxDDjBxSc
D/gLniN41iy0DeerNnJ2bcj4mlCt6qZSEzqhJqztQ/1dFLuQQc4tWO8YDR4CsMDDxm73+MRWv1bY
PQX9/HO03ZefZM7KyIg8etRXSyuJGTygfwE4YCJp6FBUg8NVE6JVG6RZGzyuZakJ1iiiteptuy52
ISM2LUfvUDgj47BPMTgcfTyIxXsRuytKXtveJzcn57yVVdCQIUBGEdONFvQLMmoCxlYHqFcHalQF
qOOlOlC9Bv4GqKMa3Re7kBGakLXEJhZihoFtzNlIFu9n7kGlh4cHzZqVy+xNijifjGp/5Wq/0TV+
am1Kla/qywJPVKn7YhcyAuMywWwuO5rkxOoKWY8rJycnZOlSgOMah5NR7T+u2lu56pxSm8JY6K2E
Kn2T2IWMoLisZbYJFseje230GnqWgOnTOZ2MyrOKVV4jq7xGtCmVzL+o0jeJXcgIiM1cZHU+OD4L
363eUXJwcLyKCprhQL0vzqw+rciyVLmPqjw9EtVroZqUVu+37ETsQwbJkJjlG9tLr0dtVtTOnWiK
A/U6eE3lSaUOyqhyV8VnNwtQ1SZVnhpd6dWlPJZdyAiMzfjRt3jr6SxyTj6+Z72j8//7H5riQP3t
s6rKeXQnpZI4GlVlqgjIcFaqclZG852KbWJGdNrac7c2BDw46tfpj1z3qPJIJI8J3zgQxA66lRNf
4TC6k1J5bHSFkwaqTaOVH1OtZJTRD8khaFHHYhcyQuIzjd1o5uefrz13gxjUS3D4GhmFcbgDLbdV
qWhTjqoxJ5RbLFStPKpabqtafpSx8LOd2v1oJ7R9x2IXMmJTyWtO5pgFP94YWmLiee2wb1p27vft
VoK3bTuLYdEcTsZna9XytkWt3HZiy+UVsIT5t+IgYwJW3Y89hbbvWOxCRk5egYVb+qbQZ+v9720M
frLO/+4vHqTQeBK+ly2F1/8vKigoCNywAbAIxjCOjxkHxnxuXSpPG8PyT/vV2ixvLuX7xtReiMI3
70TsQgbIzi91vf/9DYEPAY4NgY82hjxdfapwn1fa+QRSfv4Xj23vl+wbw4KYLio1MPDs3LneTCz8
MSyUw8m4mp36edfYzzvV8VK+C7mKW7mpn3ahhZ93j2WUpjpQ/03U15+3YCMyEtOzTU6QmWHjPgOO
gPsAB4Cy6mTBNvc0x4AU6HGyc/P2eCStJSaTsrvd12RERgaYmblKSAATgRjmh//lZDI+OjB+geXT
L2M//TIGL5+3j8VXgUq3o4UVxwzKt6s114HlD7JiUKWOxUZkgFyCU9f63DYLfgJk4GVD4APcfKz1
vr3aJXudS9qPLunr/O/v9cooKGT9DFIbkZOSIu3szunpnRQX920KFYAFlDAM8+Tk3AT0znoZ/C2z
UP+8VQNK1UlzfDkufGFNitcnC018Gi9odadiLzLyCyl7vNI2BD00C3rcDAejBDB6mU2hJRBFYBZY
WXvupksw+nXDZuWkpKSfP58aFJTk4xNlbx+wbp3H9OmuCgpnMCyAyQT8xZmAArMe4EDt7dHGnKmy
jRplZpplG9VLN2owyrapaAVYkGRvfGGVn3Xdv29QhY0an8zUSzdpXPna867sRQYoO69gj1cGoLAx
5EvkaFHuQy8DE4wo4k4JTWg1mn561iw3IaETGOaKYe4Y5sNs/hBmr9EcJ/ACS6D4GBj0g4cMPpmo
l5polJmOLTPV+GT6JR6Umk/9ZKoJ5fEpK5gtM9EsZc4yJkw0itd/5bZhtiMDBHmKjW/6GvcLm84/
NwtiGFIWJeD+prBnZq7klnf3nFRWjsCwoKYCEaINEHiBVYAF+NC8/vJsUqnhmNIVYz8kh35cqfGQ
gr7U75H+pYZjoeCzpYbq+CyUZ8s18YWdiB3JwHXmfLrZqcx1vsUbQ0qg+wAU2sDBNB+3zoR/uZnj
lJpaaDsOWhZgBUKIF4b56uvn969H1t4v0vx34Rhoz4e/30WLaLSqvPTP4afx6dJF6h8Wq5UaTHy7
UO32Qk4mA5RBznUKTFt3grTG4/LG0GfgTAERPK1lGBG/e0f8SYWFX7qDTsjAfQaEClcpqbC9e9EG
/Ut3tq//MJv1j70VFRX9O3NMSXL8y3kat+Z+yV86EVuTgSszO9c9LM3yTBrktKtPXzI9e8Ms6BGU
vV7pBQWtriW2JAP6ESg4EBAnICs5ISTks2IFKerrgzycq6Ls7MdTOruv4Na0MX/HR6OZTsUBZDQr
Po3sF5X2s1uqWWiJhQeZlN32Ns9To0dHtfAZUACRU0JCrioqQVu2pDB/lmsg6E1QIJpqrZthIZc6
WNVenEQGrv1eKWtcMuPS2/3kPo12QkvLnZ/fRVj4hLT0aXV1H2PjkJ07kzw9C7s28sFVS3EeGcTA
VP8ORsdzUlIy4+OzEhOzU1Pzc1igw1XXxXlkcNU74pLBFWtxyeCKtbhkcMVa7EdGkpPRxHFqKriW
HI5rd12DkkcKOqCLKmhMnbfxJP7sUn5a+MmftdFyVpoyRcf8V2JQbK8+ufAdRCJFuR/9ab3RpIkT
0TdT0dbW3rD7mH9qVvt7E5KJa6aMH4PqsZCampre8hU7nPyS0QZI7EdG3G+zJIR5MVyalpHtychJ
PbdFDVUQlBm55GgCc3leUqCt0Si0nJV4eQXEJGTkx81be+xMPHMTjhMp2mX3yhnjhstKi4sK8/Lw
oG+G8fPzi0pKDx0xcq7xYe/E7Jbjf3EH50qJ8aF6LEQgEISEhCVlFMZOXkNsQcdAIgOJV0BktK75
sRCOCx0k/4OmOsoSQvxNR4eF+IUHDZnxs3ssqRmOr5HRJAKP0BC1VU7NbPRXMsQUJ6w86BfbrJBz
Jy0WzRiC1vIKahnsOB3H3IxDRPI/tHbeSGEBFCaGampvsPLwDWN+OV9f53W6o6SE8VUYv5iUgU14
ChrPaUWGygbXiOgY5kYMxUSFn9w7H63DCCIyo7Y0v2imv5IhPnq6GfIfTFELC7LjvQ4YT0dw8Gno
b3fjoC4l2sVcdwx/ExZzN9p6J5BzCyn4oaNQCsikkJPmqoqD8AqYoNYWt4RMZtxoRYbGr2H5rQ4o
NTc9Zn8TGwKDhi6ybXrt2UAhgyFqrOsOg7GogtzstTaBnNKhkPytjKbLoYMydJ75saCM1q8nA1EK
om2Wq8qhwMGr+7+IFOZtBp2TQSvMybBfgtYKSw/fcKZp9UAigxbnbrlcA1WQnWV6yP/bb0HvVZF8
f1s5TY6A77im4f4zsShYtBYlPzuTlJGOi5xHwQ9sZ2RQC/PCiKtE+Jnr+MVltPdHNvsTNieDR0BY
tL1EhJq6226RkRVsv1FbHlVQW2LhEs3q+LKfSD5WRlNlERjyOludQ7sT61qR0f54Cgsy1vEJiU43
3Xc+t+DLEWFzMr6qrpKRlRBmtVpXgY8HHWDVhVtdojgDDFqU80ZdFbTfmJbR/rP49+2iWpHRkfj4
habPM/Ns+cH9lQxI03l4+VqIF7L/pqOLDZlubOWbwSFg0KKIZgtU0J5j440OfAcyQASC4KDhC22a
P5vNydC0jGh3a0VhTurZ/zKeIaOqe+A4J93Y1YNkgM8A/4HWgCiFecH2S5tcOSM5WXgYZWxsT8a3
jnQRGEGjtWRlVfXXHjwX8X3fD9fzInkfWDlFBn2vYT9sPR7Wkc+gUgqbhQxopw6UIUoe2e8ndRRO
BSSH69rgaPRXMljmJpyqKOdNP6g2G41V//OKb33IkKgJ9svV5FHayrPACh/s+hoZjOtQvhZjUAVR
WcXNZ5mLuWRwgqKOm+mq8SI2IDtxCslsl7dSC2NtV6gOFcErMcczmKOgXSAj3XuzKqrAJYPDxEhc
p8k1s6G9xcE/Ja+gqcNgjEvkRBwxGNMUMHgFFlmF4WB8lYwCcrKHWRMYBDE5FQsf5nIuGRwikq/V
6lnDBfjQQI681g/mh31CYlKZCnH9abGyNAoXGL/IeHO3uAw0ZtWKDLXNngnJKfhGDCUnxXtYLWjm
gkdsmPp2HAwuGRwkku//jOeMFBHgbb743k58AsKSUnrWoUlfnr9rRUbHAscuIK2oud0XbcYlg7OU
EUXcoTdJVWawhIiQIM+X+zP4+PiExSSlZKcZbPVKSGt1+87XySAICIpKyo5QMtju0OJXhNiPjMRj
y8eqKCrg+uFgbHsycjMC9sxDFZS1pv/ogt9TkJ96/vjWmWj52Nl6OzzbvkWhnygjI9L1sLmJgYa6
Ovq2CjNmzDTdcdQ3hcU9XUkOKzXVRqF6LKSoqLRk2TY7n8RWvy3FjmRwxR5iCzK4cLChuGRwxVp9
TEZJScltrthSt27dKi4ufvfuHWqt/6BukwGi0+m1XLGx4OxFTfUf9C1kcDUQxCWDK9biksEVa3HJ
4Iq1uGRwxUqNjf8HRtCH+jww470AAAAASUVORK5CYII=",
								extent={{-225,-250.8},{225,118.8}}),
							Text(
								textString="SF2",
								fontName="TUM Neue Helvetica 55 Regular",
								textStyle={
									TextStyle.Bold},
								extent={{-252.5,269.8},{253.7,97.3}}),
							Text(
								textString="DZ",
								fontSize=14,
								textStyle={
									TextStyle.Bold},
								lineColor={0,128,255},
								extent={{164.7,-196.8},{274.7,-260.2}})}),
		Documentation(info="<HTML><HEAD>
<META http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"> 
<TITLE>House 1 with ST, CHP and CB</TITLE> 
<STYLE type=\"text/css\">
table>tbody>tr:hover,th{background-color:#edf1f3}html{font-size:90%}body,table{font-size:1rem}body{font-family:'Open Sans',Arial,sans-serif;color:#465e70}h1,h2{margin:1em 0 .6em;font-weight:600}p{margin-top:.3em;margin-bottom:.2em}table{border-collapse:collapse;margin:1em 0;width:100%;}tr{border:1px solid #beccd3}td,th{padding:.3em 1em; border-width: 1px; border-style: solid; border-color: rgb(190, 204, 211);}td{word-break:break-word}tr:nth-child(even){background-color:#fafafa}th{font-family:'Open Sans Semibold',Arial,sans-serif;font-weight:700;text-align:left;word-break:keep-all}.lib-wrap{width:70%}@media screen and (max-width:800px){.lib-wrap{width:100%}}td p{margin:.2em}
</STYLE>
    
<META name=\"GENERATOR\" content=\"MSHTML 11.00.10570.1001\"></HEAD> 
<BODY>
<H1>House 1 with ST, CHP and CB</H1>
<HR>

<P>This model is a digital twin of the House 1 in the CoSES laboratory (<A href=\"https://doi.org/10.1109/PESGM41954.2020.9281442\">https://doi.org/10.1109/PESGM41954.2020.9281442</A>)</P>
<P><BR></P>
<P>The house consists of the following components and default conditions:</P>
<UL>
  <LI>Combined Heat and Power unit (CHP): neoTower 2.0, nominal electric and 
  thermal power: 2 kW_el / 5       kW_th</LI>
  <LI>Condensing Boiler: Wolf CGB14, nominal thermal power: 14 kW_th</LI>
  <LI>Solar Thermal emulator of variable size, up to 9 kW_th</LI>
  <LI>Thermal Storage: Wolf SPU-2 800,&nbsp;content: 750 l&nbsp;</LI>
  <LI>Domestic Hot Water (DHW)&nbsp;preparation: Wolf SEM-1 500,&nbsp;DHW 
  storage, connected to the thermal storage and solar thermal unit, content: 500 
        l</LI>
  <LI>Consumption - House parameters:<BR>Construction year: 1995 to       
  2002<BR>Additional insulation: roof + floor<BR>Number of floors: 2<BR>Number   
      of apartements: 1<BR>Number of inhabitants: 6<BR>Living area:   
  300m²<BR>Heating     system: radiators (supply / return temperatur: 60 / 45   
  °C)</LI></UL>
<P>Four boolean inputs can activate a predefined standard control for simple  
 simulation results or during a lead time of the simulation. The standard  
control  of each element is as follows:</P>
<UL>
  <LI>CHP:<BR>Switched on at 100 % modulation, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; 
      T_Storage,top &lt; T_Start,CHP and SOC_Storage &lt; SOCminCHP<BR>Switched  
   off,   when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; T_Storage,bottom &gt; T_Stop,CHP or  
     SOC_Storage &gt; SOCmaxCHP<BR><BR></LI>
  <LI>Condensing Boiler:<BR>The condensing boiler should only operate, if the    
   CHP cannot provide the required heat.<BR>Switched on at 100 % modulation,     
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; T_Storage,top &lt; T_Start,CB and     
  SOC_Storage &lt; SOCminCB<BR>Switched off, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;   
    T_Storage,bottom &gt; T_Stop,CB or SOC_Storage &gt; SOCmaxCB<BR><BR></LI>
  <LI>Solar Thermal pump:<BR>The control is&nbsp;based on Green City's   
  'SolarController'<BR>Solar thermal pump switched on,   
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_DHWStorage,in) &gt;   
  deltaTonST<BR>Solar thermal pump switched off,   
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_DHWStorage,in)&nbsp;&lt;   
  deltaToffST<BR>The pump is controlled with the following   
  conditions:<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &lt;   
  deltaTFlowReturnLow, the reference volume flow is at  minimum   
  level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &gt;   
  deltaTFlowReturnMax, the reference volume flow is at  maximum   
  level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; Between the two temperature  boundaries the 
    reference volume flow is linearly  interpolated.<BR><BR></LI>
  <LI>Domestic Hot Water pump:<BR>If the solar thermal yield isn't enough to   
  heat the domestic hot water storage, the DHW pump can charge it from the   
  thermal storage.<BR>Switched on at qvMaxDHW, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; 
      T_DHWStorage,top &lt; T_onTS_DHW and T_Storage,top &gt;   
  T_offTS_DHW<BR>Switched off,   when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;   
  T_DHWStorage,top &gt; ToffTS_DHW or T_Storage,top &lt; TDHWStorage,top</LI></UL>
<P><BR></P>
<DIV class=\"lib-wrap\">
<TABLE class=\"type\">
  <TBODY>
  <TR>
    <TH>Symbol:</TH>
    <TD colspan=\"3\"><IMG width=\"330\" height=\"254\" src=\"data:image/png;base64,
iVBORw0KGgoAAAANSUhEUgAAAUoAAAD+CAYAAABcBUzSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAEgtSURBVHhe7Z0HeBVV2scjJXQVxIIgShPWgqwVy4oFEXftoigq6q4FO0jsjSaIiCC9hE6AUJXQAoTk9pZCtezqp67rYncVEBHh/533zMzNzNy5JeHW5P0/z/skd+bMOXNn5vzu+542WWClpT7P7Y2cYvVDLVFxThaysnRW2y4AK23FoExXfZ6L3rUIFPTD0Dv3c/WTIqttLFYqxKBMUxEkDN6VajUWHMU5DEpW2opByUobcejNSlcxKFksFiuKGJRpKsvOHIvwtKbI8vsmUOGaNjTjkJ+lF4My7VSMHIuKq1mNjUZrWecVK7PEoExT1TYPK1z57Nmx0kEMSlatF3cisaKJQZm2sg7B2cOKr8iTNV9Tq22s2i0GZZoq2aG3XqnwsCy/bzI6r3j8JisGMSjTVZ/nIicFlTX5HlbqO6849GZFE4MyTUVwMlRe1Wqqh0VlMJ9Y6SoGJStEtcnDYkCzYhGDkpUmSlHnFY/fZMUgBmUaK206VZKgVJZruMaqJRzQrIwSgzJNRRU4FW2FopDUeFgp6rxisWIRgzJdlcJOlVR4WOzZsdJZDMo0Fg9bSbwI0CGXtQYvPsKqnhiUrFqqWrr4CKtaYlCmqyy8GvIwE12BU+lh1abOK1ZmiUGZpko+sFLrYdH3NX83q20sVirEoExXUe9z71xUYuJz5PZOkUeZDKWo8yrcDwQDmqUXgzKdJeBRmyovh96sdBWDkmVSLfOwePwmKwYxKNNM5OEQmFLl5aRT6J2szivzjwIZh94svRiUaSpjGJoj/LwkKUUeliWgk9TbzmJFE4MyI1QZDicaHCnzsFLUecVixSIGZVpLgUXSYJVqpajzKhWdSKzMEoMyzWT06HqDI8/Eiq63GchW21i1WwzKtJO+1zmJbZM6JdvD0peXdGcuZeM3WZkkBmVayzRUx9CGlxgl3cOicDtIR2pqSL4XzaE3K5oYlBmhyrbKhHs6SfawKG8Dl0T5zClWuolBmaYyejnJ9bKS6WGlHJQWPwz0/RnWLL0YlGkmAgfBqbZUVO37hrNEe9AhoCZZwJNVu8WgZBlV2zwsHr/JikEMyjQTe1gpkPh+ybzGrMwTgzKtVYycJPR0G5RkDyvVPwwsVixiUKa1kgdKgyeZMg8rBT8MLFYMYlCmtVIEypQpmaAUZWk/BvTF9T8ODGuWSQzKtBaDMlEK9aArZ0HRPg75WXoxKNNMVEmDno2FJaoCp6pco1L1w2Aqt6Z3XrGqLAYlS8oIjuSpdv8wsDJFDEqWVKpAyWJlghiUaSnjOpTJ8HAYlCxWeDEo005Kb6wZWjJUrIkki9AemNAZQakql5WRYlCmmyJU4GR7fdriGIn0ZiN+pwR2qqSqXFZmikGZZkolOPQeK0FSKyuhgI70nRIJrFSVy8pIMSjTTGnhYYlyDGF+QsGhtMeG5E/nkNAV3lNVLisTxaBMM0nPTteJY7bEAUtpG1XK0YMiOavpaGF+0JI0njJV5bIySwxKFovFiiIGZZopVR4ll2u0xHnurEwUgzJdRcudmeLd4pwktJ1xuckpl5VRYlCmqcjjCWkXTGiniiIuVygJ5bIySwzKdBX1vpo6FpIyEJrLTU65rIwSgzKdRZVY33aWrNrL5ao7WCxFDEoWi8WKIgZlmsqy7SwJ4nJZrFAxKNNVFr2xSRGXy2KFiEGZpiJPx9BuployeoG5XB5HyTKKQclisVhRxKBksVisKGJQpq30i1QkMyTkcpNTLiuTxKBMU1Hbmaysus6Gz3NzkOj6y+Ump1xWZolBmaaiCqzUW+HxaDNHinOUSp1Acbn0b+LLZWWWGJTpKvJw1IpLlVkJCXsn3tPhcpNTLiujxKBksVisKGJQppkqvRprS1RIyOUajUNvll4MynSVrnNBE68LmQClqlxWRolBmaYij8dUf5PSycDlCnFnDsskBmW6SlRWXheS16NkpYcYlOksqsT6trNk1V4uV93BYiliULJYLFYUMShZLBYrihiUaatUzUHmcpNTLiuTxKBMU1n2xiZBXC6LFSoGZbrq81zkpMKr4XJZrBAxKNNU5OmYw8FkhIRcbnLKZWWWGJQsFosVRQxKFovFiiIGZdqKe59rdrmsTBKDMk1FbWeysuoWbeCVxuOvVJXLyiwxKNNUVIGVeis8Hm0ucnGOUqkTKC6X/k18uazMEoMyXUUejlpxqTIrISGvNB53papcVkaJQclisVhRxKBksVisKGJQpqkq2850SmqbnU5cLquWi0GZdrIerqJZSKWOm7hcvSWuXFYmikGZprL0dJIgLpfFChWDksVisaKIQZkBone4UDiY7HYzLpfFUsSgTDNRKKh/ZwtVXq3iJjJM5HKTUy4rM8WgTDMZKmlxjvFFVwnsjeVyk1MuKzPFoEw76Xtj9S/i/xy5vZPVC8zlJq5cViaKQZlMnSWMrnisRunjIS43ssWrXFaNFT0mrGTpIWH1hJkrqpVROkofD3G54S2e5bJqrOhRYSVLu4UdLcxcWa2M0lH6eIjLDW/xLJdVY0WPCiuZisXbSYSXw+WGWiLKZdVI0ePCSqZi8XYS4eVwuaGWiHJZNVL0uLCSrUjeTiK9HC43OeWyapzokWElW5G8nUR6OVxucspl1TjRI8NKhay8nWR4OVxucspl1SjRY8NKhay8nWR4OVxucspl1SjRY8NKlfTeTnW9HJp+F5xhYm0hs0yilht5rcbq2rrL9xz5962O4nGdWbVaDMpUSu/tVMnLqS7I1JdmRS03MaC8a+yXVf++9PIvc17a2xJjVbWvM4uliEGZapF3U1f9G4O0pcCOyMjFjFhuYkApF5qo4ve19JirCkpSVctlsXRiUKZa5N2crP6NorhAUrWTrszDH2HLTSAoq/B9tQUqQvKqDiirVC6LZRSDMkNEy4KFAONILewSOQkEZRUU9jtXB5Qs1hGIQZkJsmqn01s44EXt6An3ov8IoEwSpCJ6zwxKVpLFoMwARfImY1k3MRJ0rL281IEypuYFBiUryWJQpr3CtNORxUJJqaqCL1mgjPDdIhmDkpVkMSjTXvEAZQRPjUHJYkUVgzLtlYowmEF5pDp06BC+/PJLlJaW4tNPP8WBAwfUPaxMFIMy7RUNJvr3vcRLDMojEUHR7XbDbrejpKREmsvlwr59+9QUrEwTgzIDFPv4yXhBk0F5JNq5cydsNlsQkpp5vV41BSvTxKDMBMUwn9vaqgvOVIT7Fgr3vdMclE6nMwSSZA6Hg0PwDBWDMkMUu1cZweLRS14dq0Knk0EZCkpql7QCJQGUlZmKAkqLCmN46Gm/yWuhwdHqg0yV25hcPPiGh9zieFYYVTNMtbKooGFQHol+/vln2SZphuTXX3+tpmBlmmIApR5kamUNPviRQUkDpfUDmhWvSJdel5YVm+LiWaoWnl8MyiMVwdLv90tIEjS/++47dU+8lClOBjEjdAaYeRJFdR+REBmYor9G5vOo2vXLUh46cyb0mTKxyoy2aWkt9utP1HDSSp654gJpFyUIUjVdcfDi6c7FBFPzMbkaOCjTYMUKPd4y74wVXUv1ex+hWT+gdF+t01fLaiEoWZrMbKHbKu5h3MhokokXlQo9j6pIBSXlr/P+qDD5RaypWxlSh6lQwRPVgEv/qnnSw69epOIc9cRpnzguWD6l0fIwfXEDKMUxhvNQ8w35LuHyrhE6UqiF3t8jz9Nk6n2psmoyKPXfTbs+6rNe+aNO98ZcwbU6paub6nHSadCujeHa6e6xZRmV26M6HqQqnbuaTliw3mrnaKWqnrfcpW0TPMjV569dI4vzCO5TFaXcICjlger/VLDy/U2ZqTKC0rTfdCG0tIY85X7dsSEXL/w+AyiD25ULod0z+aW1D5HyrpGi71f5UMRiwR+RoCLkYbiWCZbh4U3ROSRCYeqI3C6+n3Y/aDv9H/LDLxOb6og4Lvj8y8+Vz7gEib4+WJRhzEO9/+oxIeVX4dzFDgPoDXmZVe3zrjyGthu
YFtxnPA/DvhjK1YGSCqGMRIY5VgVp0m+z2E8Z6x9kFVpB71E74WKRTn8y+mP0+Zr2BS+0YTuDMpzkTZc3OoIFL5wmukYW6cgM1zLBqqGgtLonoc+0UPA5FvdD3U7HKrcrhjoSVPi0wTJirE9VP3cjoELPrVLVOe+QY2h7MJ3ueNN56PfFUq4BlFRI75wc4b5qW/QFkZQLWJmpeb+Q+QtRGnGC+m3yxMS24I2gY8QF1z7T/uB5yX16yFrdmPA3NmLetUjyl1b3cBss5HrQfbVIR5bMa0f3MdXnkACFVkxV5rqje44jOjGm42Kp+CFlxFifqn7ulJcOULRdn06n6px3yDGG/HXHm89Dty+Wco2glAnMmRkf0uDFk9KfiCrzF5InqIerEKWx6HAJto+Y8qys5AKu4oGReRnKCX9jrfK+oNd8zJlToexPa4Vef82MNzYWKdfIKi/z9Y5ULoMyDpLPv/maCxmeaSH9cyz+D+vEmI8z5U8gMNeHkDIM26PUpyqduxlQ6nMYzFxIpFWSVuO86a/umISE3iI/EyhTJPNFiKcs8s7Keh1Nm76Bli1Hpzkw4wussF5lSF4MykRLVsbgd9JVWP13C8JGfhD3JEJFN18T/bXT7wtXhmF7BFAKVfXctfSVP+4qLLU8dHlX+bzlv5V55Ygfk8rjdNdIyHgexn3Ryq21oNQs3YEZKWTWP1/RZXo49RaSEYOSxdKrVk5h1IMy3YFp/PU2m97DiKxI+YQCl0HJYunFoDRZ+gEzArSCpgshzCKP2vIYzayOZVCyWHoxKMNYWgEzHDDiYNbhO4OSxdKLQRnF6tUbiu7dZ6pHplAJgGX4nnMGJYulF4MygpFX2adPPj777Cf1yBQrahgdu0XuCGJQslh6MSgtLO0AaVLkDp4oFlNXOYOSxdKLQamzdAeklWKCZpXBwqBksfRiUGYoIFksVvJUq0HJgGSxWLGoVoKSAclisaqiWglKFovFqooYlCwWixVFDMoq6PDhw2xslsaq2WJQsmLWL7/8gpkzZ2LZsmX4448/1K0sBmXNF4OSFZP+/e9/4/7770edOnXQtGlTvPrqqxKcLAZlbRCDkhVVbrcbV155JerVq4fbb78d9BqP+vXr47777sOnn36qpqq9sgTlH7/h4NZc/LakNw66R+Dwvm/VHaxMFIOSFVH5+flo164dmjRpgkGDBsHn86G4uBh9+/ZFdnY2Lr30UtjtdjV17VQIKAUkf8vrgd+mdsD+Sadg/0Rhs7ri8I+fqAlYmSYGJctSBw4cwJgxY9CsWTO0atUK48aNQ2lpKbxeL/x+v/Qyn3vuOQnQU089FYsWLVKPrH0yg/Kge5SAZEcFkjr7bf6lagpWpolByQrRt99+i8cff1y2R5577rlYsGABKioqJBzJe3Q4HEFgjh07Fm3btsUxxxyDt956C/v371dzMer3vftwcN+v6qeaJTMoKdw2Q1KCUniYh3/7WU3FyiQxKFkGbd26FX/7299w1FFH4frrr8f69etRXl4Op9MpAak3AmdZWRny8vIkUKnd8tFHH8V//vMfNTdF3+38CO/1fxDvP/gwvv/wY3VrzZEZlL9veMQalNM6qSlYmaaEgdK4qg292yXMy61iWvaLlQxt2LABZ555Jho0aIABAwbA5XIhEAiEAFJvBFAKydesWYMbbrgBdevWxTXXXINt27bJPA/u2Ye1jzyNV8W9HprVEPOu6IUvSpxyX02RGZTUFvlb7jlGSM7ogoPb5qgpWJmmxIAyylsVCaJVfy81K1GiMZHTp09Hy5YtpY0cOVICkjpurOBoZeRZUlhOgKXe8a5du6Jw40Zg/+/YMvglDD2qIYZnNcIwYVP/1A07FizC4YM1YyxmSGeO0KHdZfhtzvlBT/KPXfo2XFrGTnlXEb1lM8RXoOXl5EbjcneVdca8DJ763qNEvs1Uk1z6LsI7mmqoYgal3kOsfHeu+aXi9FlcRLqYVQGleoOLg2Xo8jTd/OCx6vZc7XWu9GAF1y8MPd4ybxZ++OEHvPDCCzJs7tSpkxxQTu2RHo/HEoiRjMBKNmzYMAncE088EbPmzMHer7+F67WReLNpS+FVNhDAbIy3W54C5xtvybbLTJcVKCOrEpSVUKxUcY7FM0rPcRBQuuOFZN2kPEx1Jb5SI0JR9w3vw64lig2UhptEN1JcMPWGGKBH6dSbrryP2hpKlqAUEAtu04PWdPMNoBTHKMWpv7Bq2SHnFC7vWq4PP/wQN998s/wBueqqq/Dee+9JSFq1R8Zq1G5JeeTm5qJLly5o3LgxXnr1VXz7n//io/mLMKFdZwzJypawHNXoOKx7YiB+/uJL9YwyU0cESsP/JPHZ6vk01APTMdo+9W+lU6Avgz4rVln39M1huvyCDocwE8RDz7d2KCZQhoDNAJvKG0vpjNdVvUHBtIosQWlIo7sZpn0GUAa3Kzc8WLb+VzpS3rVY1P540UUXycrQv39/2Gw2GT5bwa+qRnkTLFetWiUHqlPved9+/fDvr77CN24fZl/wFwnLYQKWQ0UonnfdTfivr1w9s8zTkYGSHtfKZ9dcN+izBFaEZ1imoQzoWRdpteMpX0M9I8k0yrEh9ZBkqi+hTQO1s/7EAZT0kTxHAasc/c2slPlih+QXCWamfcFjDdsZlFXRwoULcdppp8mpiC+++CIoXKahPlbQq66RV0rgpcHp9957r+xF7yGgWbZtG375+BMsu7Gv7NwZltVEQnN61wvxr9Xr1TPMLB0pKCufV3qOwzQN0XMcPIaOVz0+PUTNz7quHkiYBo/R1S3x2Vw3K9MpZoQpgzK86ILrLo4Mq003pDe1XVjeYSW9+WaEgFJ3w+TN0t/8YAivALHKoAyXdy3T3r17ZUcNDSJv3749Jk6cKD0/GhOpjY+Mt1GnEOX/7LPPysHpBOgVq9/Hr19/h6Knn8fIRs1Vz7IB3jmpHUonz8ChA7+rZ5wZOmJQap8Nz7RZeoiGgZX5eK0e6OuDxbGyPqvbQupmiBiUEaVcTMUqO3M00cXT/RJKsFamr7xJiixBKfILdsxY3kgyEdoL77XKoDTlfUGv+Zgzp0LZX0tEYxsffPBB6dldcMEFWLp0qRzCox9Enigjj5XGZ9LsnpNOOkl6smPE/7/88BMqJk7D2BPbClg2lB7myIbNsenZl2QHUKboyEGpPOM0hz4spOg5Dh5TNVAa6pusm6HHBp0ZQzlWYlDGLrrYBlAeocw3OJ6yyFt7X07LlqNrBTAJUr169ZI/FLfccgsKCwuPuNOmqkZApoHrNMuHBqfTeMunBw/Gd99/j09WrsbULn+W7ZXkXVJHz4rb78X3uz5Sv0F6Kx6gVABmCrvltkonoXJf1UCppFfz0fVay+hK2647zrA9pBwGZeyqAaDUv2CspgLz0KFDskPlrLPOQsOGDeWsGepooQHiVjBLtGmdPOvWrZOzfwiWNwlwb9u5E1/7SpHX83oBySbSqN1y9iVX4rMtNvXbpK+qDkpWpql6oMxw6UFZU4H522+/yTbI448/Xi5qMWLEiCoPIk+UUSdPSUkJHnroIQnwbn/+MzbbSvDz/32BdX9/DCOzm6uwrI8J7c/AjvlLcOj3g+o3Sz8xKGu+GJQmqwnA/Prrr/H888/LzhOaITNr1iwZ9lKnihW4UmEEbeppHz58uByY3uaUNpizcCH2fPc93CNG4+3mbWQYPiSrAd5q3hr2IaPw6/c/qt8wvcSgrPliUIaxTAXmThHG9unTR64VefXVV2PFihUy3KU2QitgpdII3ATwqVOnysHp1Bs/fNRIfLv7G+zIXYDJHc5Se8QbYXjdpigQ3ubPn/9b/abpIwZlzReDMorVqzcU3bvPVI9Mb23cuBEXX3yxnA3zwAMPoKioKOzKP+li1G5JnU30Hp4ePXrIULz//ffjgw8/whebSzD/il4iDBcAVYcQ5V13I/7j9qnfOD3EoKz5YlBGMPIq+/TJx2ef/aQemZ6i9sh58+ahQ4cOsk3ypZdeknO1tU4bu0PvTdphc/iE0QDzxA4LitUI5AR06o3v168fGjVqJAene8T5//jxv7Cq770YWedY6V2+nlUfU7uejw/yV+LwoU
PqFUitGJQ1XwxKC8sUQJJokd2hQ4eiRYsWOP300zFhwoTgSuQKiOxwO9YHYWl3eFDmWIAKx0wVlgqsShwBuU/7nAqj8yYPc/DgwWjevDnOOvtsrFi9Gj/83+coynkZY5qdLDzLprKT551W7eAbPzktFtVgUNZ8MSh1lkmAJP3zn/+UITZ12lx22WVyjGJ5xVa43Arw7A4XnA4b/mPvhwr7jCAM/2u7RdiN4nOZTGMT2z+0DxdpcsXn1LVl0sB3v8+HgN8vV0und/WQh/zOu+/im692o3ziDLzbtrPsEafB6aMat0Dh0zn45T//Va9IasSgrPliUGYgIEkEFhpETpCkNyOuXbtWhK8VcDtLBHCcEogEQbKfHV3xte16bHHsEN7lRvzqaIMfHd3hcmxGsWM7djrG4oD9OHxgf0OG5RJaKQCm0+mA2+WEjzp5Kiowd+5cnHfeeWjWtCkef+opfPZ/n+HTgnXIvfAyjBCeJa1tScBcektffF2xXb0yyReDsuarVoMyEwFJL/1avnw5zhZhKb2n5umnn5aQke2RTi989pX4l/05eES4rXiQXux23oADzpYosy9EqW0RDvmz8Xvp0fDblglbib2BdjjsrYty+2x5DHmYbscm4Y1at2E6RXjs8YiyhPenGYX6brfwYAXsgmmdIp23Mk1U87rh97hlmyWF4NRuSUu/3XjjjbKT59bbbsOOjz7C7kAZll7fB2/UOVrt5MnGrAv+gn+uXo/DfyR/MWAGZc1XrQRlJgKS9OOPP2LUqFE4+eSTZVhKC1wQYLSVf8h7dDic+Mp+O761XQW3fYPwGLfiM8cAYFsW/uc7B9/ZeuDwh3WBnVn40tYPP3ouAj7JwsFAY3htayQoP7EPkkZ5KaaBzwm3AKS7ZAveX7lMenyzZ8/GnNkLsWjJUhQUboLD7YVHAtMJ2+b1yM+bj1mzRJo5cyLYbJlP/ipRvt0Fl4CtXZRHeWgrED388MPyh+HCiy7Cmo0b8f0/P8WGxwZidNMTZChOM3nGte4I/5QZ+OO3A+oVS44YlDVftRKUmagvvvgCAwcOlGMNaVEL/SBy/aIWNuFBuh2F+Nl+jgixW+MTxyD829Yfh3fWAXaJ2+0TtlVY+VHSi8QO8f//ZeGAtyV22UbjW0dP/Gpvg3LHbJlXEJICmNSTviF/Ol4acD+u6N4VzVu0wNFHHy3sZHTo1BW97uiHF9+cgoIiu4C3F5vmjUKPrm3QTKYhOwbNmx+H445TrPmxx6jbFbu8Xw7yN3rhdRuHM9EPAX3P119/Ha1bt8Ypp5yCGbNn4cfdXyMw9l2MP7mDDMNfysrCohtvx28//k+9askRg7Lmi0GZASJQ3CbCTlp1h96MSOEoQdI8iJzaFSnU3uLYjjLbAhzyNcTh7UfhoKexAsUKcbuFZ0ne5OFdRyn/lwsT2w/56uNQWQP59yPbEOGJbhN5Kh4q5e3xBbB27pu49aJToCyWcBTqZDeUIXHD7GzUURdRaNSiNa4fMARrbB4UzR+JS/90AuqL/ZSuQXZ99VjVjqonX2TWUBj9vfiOQVhSGApK+iEgz5maF6ZNmxZsdnjp9dfwzTff4OOlKzGp01mY2PFMfF5kI3KpVy45YlDWfDEo01j00q+CggK52g6BgeZGb9myRQLDOIhc+d/rWIOAfSl89vfgs63EN46/SihKD1J4joc/FV7k58I+U43+/z9hOwQ0y0SaD7Lwtau3CH8DskecOnZkp47LizLXOjx381kK4Oo0QbcrH8LrYydh0mRhY97CU/2vQcvGKgCPaY8nxi+Ha8sGzJ05Ge+MG4+Jk6di7Ijn0fvPzZU02S1w/m2PY9y74viJwisc/y7mLFqBzSVOYzunzrTXTNDg9J49e0r43t3/Xnz48cf4YvMWfL4xNWvaMChrvhiUaap9+/Zh8uTJOPXUU+Uajq+88ooMfWWnjQkgyjAgO76w/x37bKdiv6sN9nj/hF/dp+Kwp64IuwUkNShaGe2jsNybhf/Zu4lw/Rl8an8aX9lvw6eOp+DxB+BcORE9O9VRIHdcFwyctB47d+3E9h07sHP7drg3rcTrj9+CXtf0RM/rb8Uz7+TB5fEjIM63vKwMFdt2wrNxCR7ueZKSR6NW+OuzU+Ar24atFeWyLdLv84SFpGZau+WmTZvk4HSahXR5jx7YLH5AUiUGZc0XgzINpS1qQV4khZkETAJk5SDyUCNYehzr8In9Gex1d5LeIT4UgCwTECRPkjxIK0hqJtJIr3K7MBGSH3bVw/9s52CX4y24fCIMXzkBPTsepUCuQXN0u7EfXh07EfOWrcC6wkIR7jvh84uw2SM8UBEq0zt49OdH0Cx6fy4euPJEFZQnodfT40SI75FDgvRpYzHNq6brRGMt23fogIV5eTh4MPmrDDEoa74YlGkmWnX8rrvukm129GZEGgpk1R5pZRQmUy+3y74ZX7r7yfZGAl9USJJRGgrBS7Pwi+9MfGQfIvOUs3ecXvi96/HGA1fiuHoCcgQ6YQ1bnoyOZ5yJP19+OW579FGMHD8J+SvWwS6ALoGpO7d4g5KM2m5pFaLx48fL6ZvUQUSrEf30U3JHM5hBOWBAAcaN82D//vRdGo5VNTEo00jU/ti9e3fZaUMv5NqwYYMMM43tkeGNoEbtiuRZfunqh8OObKXjJlLYrZkAJXmgsGfhP/a+wit0yvxKHKUSwC7qXV83H68/ei3OPeM41KtbCUxpdeui2bEn4E/n9cJjI9/F++J4t4s6g5RzSwQoqZOHmiPohyRPeJN07ej95NSW+8knn6hXNfEyg/K008bhzjuXyb8MzJohBmUaiMJFmn7YqVMnnHDCCXJRC4KjVXuktRFonCh3zJFjI393NFdC6K0CktsEBGMBJRl5lCLsPvhBY+xzt8N/7bfiA/twOdyIYOkWUPI412PJ3DdFyDsAf+97O3qcexZOOqkpGuo8zQatz8QDb86Bw13Z5pgIUJIRLLXB6WvWrJHvKSdvnF6TS154MmQFShqju3v3HgwcuJ6BWQPEoEyxfvjhBwwZMgTHHnusDB8pjCRA0nAYKzBYmTLQ3I7P7APwY8lF+M7RAz+4L8XvnmNxuEzAkiAYS/hdLkBJPeQCsof89bCnpBO+t12GgCMfTk+5OK8ylJVXyM4Zv98N++aNWLl4IWbMeAtDBt2Fi9qcgPoSlnXQ8Yr7MH+DGz51qE+iQKk3um7UNvrUU0/J8abUvvv+++/LV2IkUuFAqYmASUv1dekykWGZoWJQplC0qEX//v1luEiLWixatEiG2tVdZNfpKBG2RXp/pSWL8ZvnRDlO8vAHUcJvgih1/viy8IPjEuwqGY1t9qlymJHTaYNXWH7u2xj4TA4GP/sixsxcDlfAD484T5qiGCgNIODZgtyXHkSn+opXeeIZPTBmqRN+jxJ+JwOUZNRuSUaLatDAdJrFNHbsWPz888/qVY+/IoFy1aoP0a3bVOHpLkZFxW65jZV5SjgojW90y1JfKWvxJjfDS8B0b42TpqWt7hvgwuWXOlHbGoWHBElakXz9+vUyfKQwUj/TpipGniUthkGDxWm+t5yN8y9xi/0ClLtUIBIw6a/+/w+EJ0mD0T/Mwveuy4VX5kORY5cyjtLpQ4XfhglPX4d64todVacuTul+O2av9aJceHAE9rLyMlSUubBw9AB0VkHZ5rybMa3ABV+SQUmmtVvS7CVaVIPWt3zsscfw738nZnV0K1BSqM2ArDlKKCjl+7i192sbFAsoK/dL2AZfu1ldUFrll3zRIPLFixfjjDPOkNP2Bg0aJMEYe3tkJLPLgeLbHZNw0NNEtjceKG2BP1yNlaE/2qycDwUcqeOGBqMTIEuFUcj9sbBdWfjSdbcCSRpsLvL1+gJYv2AYLj5BHR5U92hcdN3DGDlmKmbOzBU2EW+9+iiuObeVEnrXPQG9n3wbm50CgmpHVDJBSUY/OARxmsV03XXXyVdj3HTTTdi+Pf6rDJlBmRxAxlKHxHUOmpI29NmndKbX5LJCJGpGghTxFbSx3GTd/uA+3XZ1W3HQYzXlZ1C4/JIrCv9Gjx4th7G0bdsWY8aMieubEQlupfbF+Ml+Lr5zXIGPb
a9gR/FY/Oo9RXqWf7gb4lBJA2XaIsGR5n3TQHMByH2utvjOfiUO+psCrqPwpeNekSct1yYgJoDn8xdh5tCH0a3d8er1PgoNGjWT87ibNWuCRtnqYPQGx+D83k9gzppieFVvkkwB5Rz0/0sLJV2943D1k+8kDJRkWocYvRLjkUcekaMJzj//fOm9x1NmUCZHVaxDQX2O3N5aZCdSCWcmRT5DRilhoKRfrt5hf6boJlJlM1mYm2zpUdJDIY7RyqAbHrk8q/ySpy+//FJWVpp2R4ta0Ko55PFQmGhVyatj2qDzUsdiuBxF2OL4QC6ddtDbDPt87bDDMRb/53hC9oQfLG+MH5yXyB5u/DMLP3rOh9dWAL9tuRxD+YX9AXjtBUGv0ulyi3MtQd70d/D0fXfh4j93QRMBHjnXu2EjtG59Ki6/ug8efXEUFqwslD3kTt25uTwinF89HwP+2lF40s1w9IkdcdOzExMKSs2ozZLafWl2E81yopWXpkyZIl+hEQ9VB5TyGVSf+5wc7T35BDG9d0efrWBHsgBhTKAU0tKlyGHIRKUQlNFuchSAmm9ysXjYwsIvXH7JEb08i9ZU1MK/1atXS0+HwkOrin0kRrCk8Y/UVlniKBdgfBJ7bZ1QZp8nwLkTAfsyHHLXw/883QQY1+IL5/0y/P6ftxtc9iJxTIX0TJUhR8a2UlqH0uvxwlW8GSvy8zB9xgz5BsVp06Zi7tz5eG91Iey0zJrwJPWQlMcK785evAkrF83CtKmTMWXGLCx5f4P0WKNNW4yH0awmAibNcjrzzDPlrCcCJ406OFJVGZT07Oqef9lEpT6ThnpD6WJ9pkOe7cjPvCyTQ+6YlTBQSnCFBZIOeJpCQGn1a6jbXmVQWuWXWFEFWrlypVzUgsK+AQMGyOErFG5bVeZ4G4HyK3sffOR4Tf5PAKQVzvfZT8XXtuvEtlI5sPwnx/nY6+ygjpckD5cgGc7LE2ATwLRauJd6wV2RBseLfW7hWfp8fvnKB1q30jJdgoy8SvqBojbiK664Qs4Tv+eee/Dxxx+rd6x6qiooQ5wIQ10Rz6r6P6ULH/hUtw4pYlBWTYkDpbhd1BZigJd4IJSP1b3Juu1pDkoaRE7eS6tWrdCmTRs5tY7CbPJqrCpxIoy8S79jmQzDlR5xMjd2227CV7Y+sne8WHiQO+1v41t7L7kqugJK6/xqgmntluvWrQsuqkGjD0pKStQ7V3XFF5T0kQAm6k+O7vkOUXXrkJBWnrkOscIqgaAkqbDU3P8gyKp7k3XbjwCUvXrNx5w5Feqn+IvejPjCCy/IXu2zzjpLrqFIXmQ82yNjM3o1rVfCUdtGYfl2+yTsso8SHmWZ2KaE2ARJGocZ3pOsOUawpPtB3j21D1LnWseOHeU41uoMTq9y6E3Pqu551IfeUmJ/b3FeORHdverWIWM7aOS2fZamBIMyPaW9L6dly9FxB+bOnTvlNDp66Re1R1LoTZUyEe2R1TWCp7J6udYGSb3b1RvknskmmwvEj9fEiRNxzjnnSO//7bffrvLg9CqDUkgJfRWr7MzRRJCLFhbHAsrKMrQwOxSMsZTFqmGgND8c1r+W2svF4g3MjRs3yh5tguRrr70mZ97QQrNWlZQtPYy8S2qjpL/aWy1pdMLnIvSNVdUBpUGm0JuVfqq1HqXZjgSYv//+uxzuQ2MjqT1y+vTp8m2Je/bsOaKZNmyJN2qb3LFjhwy5P/30U/kSMxryRNCk9uRYZAZllZdZiwjK2H78WYkVg9JkVQUmvRmRXnpFizDQdLnCwkJ1j7LgBYMyvY1ASWuA0g8bidayHDFiBJo3b46uXbvKTp9oMoOSpjDyMms1SwzKMBYLMMkDoeEl5IHQS79ofrFeDMr0Nw2U+sHnNGKB1rds3769bLekCCHS4HQrUPIyazVLDMooVq/eULlEllnUQUPDSmh4Cb1GlmbemMWgTH+zAqUmanO+8MILZbvls88+i++//17dY1Q4UGriZdYyXwzKCEZeZZ8++YaHnirUihUr0LlzZzkdjqbC7d+/X91rFIMy/S0SKEkffvgh+vbtK34w6+GOO+7ARx99pO6pVCRQ8jJrNUMMSguzAqQmej0DLbDbrVs3rF27Vt1qLQZl+ls0UJKoHZqmO1L0QLDcvdsIPCtQ8jJrNUsMSp1FAqQmWshi0qRJ2LVrl7olvBiU6W+xgJJEIxtoQPrMmTPlfdXLDMrIgDRNwtBMTpYI7eE2TqKg/ZHGTlop2jHmMpW0NHsotGxtvKVFnjVcDMoYAVkdMSjT32IFZSSZQRmrQqYyhgDIPA04GvSsFO2YcNBTytaKpoHqBm7WMtVqUCYKkJoYlOlv6Q1KEm2L4MklDJRCWrqQMtRj1O2xrQmb2aqVoEw0IDUxKJNjLqf2Goyqz1NPf1CKrUFvjvbrw2TVooIy0jGm/aa8lKmW5imOOlCKY7TvEDo9suaoVoIyWWJQJscIkFu9m+Fx2cT/1mnCWeaB0rQ/xNszK9ox1mVqigpKfdkRF6bJbDEoEygGZXKsxOHDd8XDsdW+CjZn1VZoyozQW9sWDXpWinaMVZmqtKmVIWWoxzAoWfEQgzIZZkexqwK71zyJHUVzBShpVSSrdNaWCZ05lWmSCcpIy7ExKFlxVLxAabfTWwXdKCt1o7zMjjJh9NdspYHaB2TyIAOO9dhT0Bc/r38YDmfV2irTD5S69kJhRu4kCpT6MhU4hrY3UjpTpxKDkhUPxQuU9AqD9etWYtToVXjh1VK8+JovxJ592Ye33/WItMl5B026WJF7O/658S0gvzP2rLpRbqOOHXO6cJZKULIyRwzKBCpeoAwEnJgzdzO6X2nHC69sxPgJq/HW2AKMfLMAY8TfseNXY+jwtbj77z5RXu0BZbHDi9Lildi78m84nN8Ne967OdgDbpXeymoOKM2eYc3tgU6FGJQJVLxAWVbmxKQpG3D2hVswaowHeYvsWLHKBputGMuW2zB/oQPjJzlw7z/8EpK1AZQEQ5/Pg19W34XDi8/BocXnYZ8AZanfDZs9M0JvVuaIQZlAxQuUPp8XixcvwaCcPBF6e3Hfw15ce5MP8xa48NfbvLjnHyUY/LxLQJTaMmsHKLe4KvCVbYIIuS/E74vOxx+LuuGX5b3lPg69WfEWgzKBihcoqWe3vNyDnTuc+OdHm4UH6cSV1zkwf4Edt9xpQ+6s9/HxhyWoKC8WUCWw0kv/7fB4CBpW+WWuUUcNQXJX4UQcWHgO/lh4Nn7POw+HFpyJH5der6ZhULLiKwZlAhUPUJJ36PHYRbjtwvRZbizIc+HlIT5c2MOF14a5cNVfnXjupY2Yt9CL3NluzJztwsxZLsye68KatS64hYdplW8mGgGw2L0NuzZMxO/zz8bBeWfgwPxz8Pu8rvgj71xsLVqQUcODWJmjlIKShkfoG5+VkQWmRumo472iydzIXZ08qqcjBSVBsqzMgWEj3SLUDuC5V7wY/IIXOS958dzL4u+L6t+XfBgs/teMtt3/iB+PD3LD7yuW4Ximh+T0lkgaIvWv9aNxYM6ZODinMw7MPQu/C/tjVnt8W/A4XG53lbxJMgYlKxalDJRyapTlmCsjECvHcx0JKCuPk3BO0livWEDp9SqhMv2lz5Te53MKE+F2mR0jRntwxXWlWLjYhe3b7CgtVWzrVgd2bLeLcFz8X6GModT20faly1144hkCpfKubhqL6XYrwNSXnwlml7NtnPjivcH4I7c9fp/ZAQdyu6jWGX/MaIcd6ydji3tryLHRjEHJikWpAWXEQbJ6sOlnB+i2q8fHtmqJCbBRB+jGT5FASd4dwWvFKhcWirB6+UryhFzYvHkdlq8owsr3PHh9hFdCksLucuFZasfSEKDCwmJxrB35y1xYXeA0hNgE3vkiRH/+FZ8Ixf24qW8prr4+gGGjSkW5LricR9pmmkxzynD6gwLhSU5tgwPTTsVv0zsE7Y8pJ+GrJfdImOq9yZIY31POoGTFopSAMnRGgl4ENl2oHPT+TKAU+7Q8QmcR6GUEZbp4lNTRsm69EzfeEcBDj/tw290BFJf4MH5CHnr0moP+D/px38PCk5SQrDye4Lp9mxPDR25G
t0vc6HlDuYChV3iOlWk0UL48RIThIly/+Ooy9L3PjwuvKMPifBf8fuO5pLMRJL229dg35XT8NvFk/DrpNIMdfLcFPs8fgC3eHSJ95TXY6lwrwBl9mBCDkhWL0hSUVmAzgTLmqVMm8CbJmyRFA+Wq9514+AkfAn67CJN9KNrskF7f8y8Xori4SB7rF9DTH0cQLFhjw613edG1+1b8fUBAhN5GIFCaeQtdsq1y4HNeXH97ADNmuXHeXzIQlC4vtm3Ow6/vdsT+cafg1/ECkDr7fcxx+CLvIQHK7TK9Bsf/FY3AR45lUT1LBiUrFqUm9NZWJVE/GmUEZSUUjwSUuvySqFhA+fggH8pK7Xh0oA9LBMRyXvBi8jS/bIO06nwhz/Gdd93odnEprr3JhmXLlbZHfRpq76SQ/q77/ehzt1/CeNJUN86/PDNB+cF7b+PXtzpg/5sCjqPbG2z/m22xd0wXVKybjSL/ThS7/LB5y/DzlMvx7fx7YPNtE/mEXn/NGJSsWJSizhzzEvdCAnbKRyPY4uNRpi8oHxOApND6nn8E0PXicun1kfdHnTJWx2wucuCaG0tFGF2ON0fPx64dm4QHqXTWmNNv2eIU5pAe65Tpbpx7WWaCcuu6ufj1jc7YP0yAcngHaftG0N/22De8Iw683gpfTb0DO9bPw07PZvxr+RvYO+J0/DL+cpTbC+XCGVZ5kzEoWbEoRaAkqbDUQuIg6Ahsuu1BIMYPlL16zcecORXqp8QpFlA++rRPgit3jhtj3vEIYPoxcYonBJTKeEoHXhvuFZAsxcVXlYqwOiCHBhFYqZfcnJ7ypYHnO7ZlNig9RWux9/U/4ddX2mHvqx2EdRTWCftePg37XlE+//pCaxwYdiYOTr0Rv714sth3qkyzY+nbKPGUW+ZNlgpQ0its6Rn86Sfr1xyz0k8pBGXqpL0vp2XL0QkFZqweJQ3tGTqSpiL60eumUkydYe1REvxowPmCRQ7k5X+I3rd8gOv7+LC6wCXz09J5PE6sXbcJDzyyFnf0L8PSpYWYnuvK2DZKT9E6/O+1C/FrjvAgnz8de4X9Nvg0/N+sZ/Hj0EvV7Z2x71kB0GfaYt9zp+PXwadi36grsWvLKpREGISeTFASINu1G4djjhmJ7OxhCX8VCSt+qkGgNHmiwsJ1GGkvF0s0MKsSet/3iB833xmQYXU4UJJRR80HO2144y0/Luvpxdq1JSK0pk4MY5qly2nWjh2X9nSj3wM+TJ6WmW2UZMX+CuxY8i72P9YGewd1wb4n2+Hn4b1QWrwRn8x6EfufaIu9AztjzxPC4xR/Kc3epzth/6DO+HjxWBR7U+tR6gGpf+4YlJmjWutRmi0RwIy1M4dA+Tj1ehc5MWSEF+9O9KCiwi47aaw6dLZtdcj1J598pgQety0kTWnAgfxlNEPHiYHPOnDjHaWYNlMJvRctyTxQuos2YOvqBdj3VDfseeR07H+gNf41aTA2b/0YgY2rsefZi7FnQBf8OqoP9jwqgCn+3/dgO3zzYk94NhbA5k6NR2kFSP3zxqDMHDEoTRZPYMYCygcG+LF5s0P+LSx04NWhLgwTYXjRFgfWb3CGzKah4UIL8px4bbhbLtKr7/GmDh2fV4SrNofwIgMiDQ0yL8PFV5XhhtsDuKRnKZYuyyxQ2l1u+AuW4rMxj+Prl27Cnnvb4df7O+LTGa9ji/A07V4/Pp30HPbd3VZ4mXcKOF6PPfd1Euna48fnemF74SoRvocfIpQIUEYCpGYMyswSgzKMxQOYkUBJgNu4icZR+uWAc/pLoJsmwu57HvTLcZUUir861CuHD2mwpFk5K1asxXvvrRMhtshbzU+DJP3f7wG/XMR3yxbhlXoCwvssxR33+jFJhN+0wIaVl5q+5oTN48e2hZPxv7vPxp4+HfDrjSfjk7eFR7n9I3iLi/DNK/dg381t8fmr92NX7hjsubU9fr7rDOzr2xk/P/IX+NesEHn4LPKOLyhjAaRmDMrMEoMyitWrNxTdu89Uj6yaIoGSjIDlUcNr+kvbCIg0HZHGS9L0xKv+Vip7uimcVtLYUFy8SY6VpLSaUbskHX+XgCSB0iHAqaWhv2TkxWYWJBWzi2voFPbBjDH46cYu+P7u7vj4refgLVgJt9+Pb157EHsvb4my5Quxbda72NuzNX6+4XT82OtUfPjUbXAWbYLdbT1EKF6g7Nt3KerXH2b5DFkZgzKzxKCMYPQw9+mTX+0HOhooyQhcmum30V96iVjeEht6Xu/BuAnFcIlQu6SEQmvlr2bkTW4ucqHf3wPod7+ApDhWP1zInH9GmtOJ4tIKbJ03Fd/ccik+eekJeNcXoCRQBs+a97F9+ljY/AHsnDoWey47FT/dcw0+7tER7mnjYS+nxTKs70G8QLl79x489NBqHH30SPnjavU86Y1BmVliUFrYkQJSUyygjGYBvw0TpxSj980O4V360fOG0hC7+vpS/PUWF/7xqBd2m+JdWg1Az3wTPxACeqXTJ+GTay9GIG8e7D4/7B4vigPl0rvcJUJx/8rl2HbDFfA/8QAcbpeErHV+8Q29SbECk0GZWWJQ6ixegNQUD1A6RSX3+UqQO3s9HnnKiady/MJ8Bnv8GT/GvLNBpC2R4bVVPjXHnLCVlcM1JxfuhfPhEJDU77d7fHAWFMD76gtwlNjE/vCzcsgS0ZlDCg/Mp4UZh7FJC84+s9quKXTyRMjkixBFO8ZcppI2dPEYSlf5nu+qyfiO8EwUg1JYvAGpKR6g1Mzndcql1igcNxsNL/L7Y3/7YjzOJ9Vm9/pUCBq/C7Vnule/L39gHO7oS60lCpSawgGTnjnnm1ebxvqaoWae6psoUJr2Sylla0XTCl0GblZJDMqMVKIBqSkSKN3C8wmUlqO8XLXSUvi9HkvYuUTaMjVdacAnX8lqTOOU+ZWa0lCPuFsARdtuaaUBeEV46jTklx5GK5r7/KXix0A917IylPp98LgsQmmnG/7SMiWdAF95RYXxeworKysVPzjiGuuOSzQoNZmB2ajR8BhASaJtFmuyakoYKIW0dFZl0LRhkxcqZUorPVOdxxp+1bD0Vq0EZaIBqckKlC63AKSosMWFq7Bw3mRMnDQREycImzUbiws2wxuoQMCreULkJdpQuDof0ydOwLvvTsTMuUuwoVgJyYNpHDasf28JZoi8xo+fiNx5+SgUadwusX0VbZ+ECVTGRKNNEDZ5zjwUFAmPVMDVnS4dPk4XfIFScT5b8N7SOZg+TZzvhAmYMHUqcpesxGaHH2X6HwyR3lG8DgtnTsWEdyeEfE/NZs7Mxcp1m+Ap9QcXOk4WKDVpwGzS5A0ExvaMAZTEJM2bo/2V0AlaVFBGOsa035SXfBOBOeQmGOrO0xCmW4Cyd24xe5Ss8DKC0gmPT1TuTasw+rUncNM1F6B964aoV78u6tath/rHt0KXS/+K+wYNxbxVtCKQSCuO9bqKMOO1e9G2UUM0aNAYnS++C9PXOuD1qKt5C0/K69qIyS/ejQ7NGiE7uzHO7NEfMwpEqO4vwsRn70L7Zo3RsGEjNGrYANn16wmPRrG69eujaZu2uORv/fDqhGUocngFfMJ3fCTDnG4PfK4SLJ45Gg/3vwEXnn0Sjm4mzrWuuE5NmuL4P52Ha+98BCMm56HYqXiX5HG7CibgutNboWGDhmjUuBEaZNcPfk/FstHy+BNwXo/rMXjUNGxweuUPQ7JBqZcCEQOWYgClab+Vt2dQtGOsy9RkBcqI582gZFVVelC6PV44NyzEUzddhhYNtF/wemjcuInwLgTg6qrb6jRE5+63Y/ziTcK79MHnLsLUF+5Ac/UXv805t2LKGgd8OlD6XIWYMLgPTlDTnHbBHZgiQFkhQDnu6VtwvLo9K6suGjRqLMpsLMpsgoYCmsr2LDQ9+VI8O2UFHAJUoaF9cswpQm2vaxNmDnsMF5zWInhu9Rs0kufbuGHd4LbGJ56Jf7wyA5tcXnh9AbhWj0OP1k2D++tmN0RjOkZ+V/FDUf+o4L4GJ10ovusqeEs9sKc9KPXbokHPStGOsSpTFYXXlM4SfmHOm0HJqqqCoBSeT8D+Hl648wI0UStrs/YX4cGXxmB
h3mIsXjIHk954CJe1OQ515P566NLjYeQVe1HuLcK0l+4Mwu7Uc/tgqgUoJz57B05WYduh+52YqoJy/KDb0Kqesv2UP9+AoVPmYdGiRcgTNmPcG7j5gi5oeJR63HWDsEqE4R53CrxK+h5eBxaPexLntTpank9WoxNxye2PYdyUOeIaLcK8mcPwYO/zcRztE9bgmHPx3NTlcNHQoILxuKrtscpxWS3R57lxWJSfj4ULFyJv8SLMGvc0rujUTN3fAn977B1sKSuF05Z8UNIMHlpmbfuEa6KAUulQqUyTTFAaO2DIswyeBx2vO4ZgaAi9gx6odv4MSlYEKaCkDgkvVk3KwVnNlQresEU3DJq8Ep6AH34RYlOYHQh48P74l3Gemibr6A54dEIBdpaVGEB52nl3YOYGAdBSP7xexZuiEHvqC3eidRRQdrrsXszeQCsTBUSZfpSVifOa9jzOO6ah3N/k5Kvx9vKN8HjcSe/cIY/bsWERHr22C+rRdz2qGS65+yUs20yzlJRr5POLEHtdPp7s0Vm5RsL+dNcQFLnL4F3zrg6UrfDYxDXYunMnKsqog2cbduzYjOF3dEe23N8UPf8+ChtLS+GyJw+U+imOtMyadWeO8r00M/Y0JwqU+jIVoBnAKEXpdLDTd+aYylfCdSWvnBzF+5QwFduMeWaOGJQJlASlR8DQvg6v978IdeXDk41z7hyCLV7qba703JxuL9y2jZgzaRSGDBmKISPfxtxVm1Dm22IA5Snn3IR38jegaFMhNmzYgA2FG1FUuBJvPXULTooCyo6X3I0ZBTYVhE74K0qxedmbuKJlE7m/YcsLMWThBgGtJIPS6YRXlLly0mCc21o9l049MXJugQCkt3IkgNMlvF0X1ubPwBvDxTUaOgRvzcyH3eU3eZQn4r4R87BxyxYUimu0cfMmrM4bhhvPPgZ1j6qLY9pegOcmiNBb/DglI/S2mgNOHYo84DxzxKBMoAiUHn8Axaunoc+ZzZVKnH0s7hy+GAGfxVAgAQKvSB8ICPP7BRTc8LiMoXf9Bk1xfKs2OKVNG7RR7ZQ2J+P4Y5sqnljWUegYBpSdL/87Ftm24oMPdmGn8La2l7mQ+/o/0LFpfbm/WfsbxXGbk+5ROkXY7XFvxJhH/4qTspVz7XjtQ5i/xg6/1sQQNGUolLxGwvxet/wxcq3Wg7Iujm55EtqccopyjU5pg1YtletTr/E5+Mcr80W4HoDbYU9oZ06kRTLiD8pQbzRTvbd0FIMygSJQ+kSIvCF/JK5o3lh5gJu0xBMT1oaM59ODgIb+yOE/BE4TKKNbeFC2bHc++j70JAYOHIhBg57BQ3ffis7Hqe2BWfVx4QMjUGhzw201TjGB5nR5RJmr8HKfv+BY9Xucf9tALCn0hm0v1a4RXcNQUEaweiej23VPYsZ7m+D1uWFLAChjWUWIPcrMEoMygQqCcukoXHmcElJGBiUNIaK2wzJlcLQIO71OIyiPb9cdD704HCPfGIHhw4cLewMjR7yCv994MZrXUdKEC73DWp2GIiz/B6Yu3wJ3DLNZ4m0aKF+5/fJg7/75fcKDkoYQBUrpGpXJwfWeEFAei6v7P4M33nxTXqMRb4zCkJefwFVdWqr766HrrYOxcos41mmLGyhjAaRmDMrMEoMygSJQUii95f3puO1MFZT1j8XtwxajLCT0FiGly471q/KRO2MGZkyfjSUr1sPlLcZ0HSjbXXAX5mwpx87tW5UZJxXbsHOrAzNfvjtqZ06DJi3QtkMndO58Ok4/XbEzr+iJx4aMxbJCmyg/nJebWFNC781469Fr0UoNvdv1ehDzC+zw0aIW+rTC2y3ZvBaL5szCDHGd5szOx0aXD+4CU2fOpDXYtmsXKuQ12oqKijKsHPcYTm+hDImq1/pyDJ+3XniVLmyPAyh5mbWaLQZlAkWgdHsD8BWtwjM3thcVhCpxNs667WUU+coMnTkuTynKHavw5HUd1XTH429PT4F/mxMzdaA8kuFBHS+5BzPpWOGpul0uOXTJRe2g1Hsu8ksFJMmc1MTgcWPRqH+gY0sFZA3aXYERc6gzx1f5g+Jyo6zUi7yR/YNAbdHtPiwp8cG3doIOlCfhobeXydWElO8kfoS8frjXT8X1nY6TaerUPwfPTFwGZ8ATF1DGumqQZgzKzBKDMoEiULpEmOj3u7B4zKM4tZFSubOb/gmPjVsO/1YRPpaWolSEkFvLvcgf8zg6HK0MjM5ufQ5emW/DzkCxIfQ+IlBeeg9y15pAKcxqfnmyjYYH2Qpmod9fTlPHkjbCubflYGmRSzZDlIrrRB60a61I072d/D5kPZ58F3ZvBTyGXu9WcmhVxY4dwetbsXUrCnNfwDknZStpsrti8MTlIaD83CkqxZAItkomM+jwd0BvbX/OHmSdtxpZDUT4XYeXWaspYlAmUBKUAkR2pxelztV4/e7L0Fit4NknnYG+Tw/H3LnzsWDBQox9uT/Oaq5W4qzGuPje11EovMxST/wGnBuHB4XCKqUmvEqfz4llE3JwwcnqwPB6x6Drdfdh5LhpWLBwEWZNHo47L+2ojoXMQpNOvTF+2UZ4A2VwmQac3/rMGMzLyxPXdw7mzl+AWVNHoe9FbVUIZ+GYP9+Oae8Xi9DeYWijjApK1XqLdJoMoNQsCjCrBsrQHu0jX36NVRUxKBOoICjtdtkzG7Ctwsv9eqJFQ9NDr7cGzfCXmwZhyRYChwiJXUWY+uIdaKHub9MtzBTGnD44UU1z2oW6KYwDb8EJ6sybdt3vwvR0BSWZ04OAz4ZF7zyHSzuow6nCWJsuV+H1aSvgCHjh8SpTGK9orc28iWzZJ1+MgeOWwVXmCxlHqQelHoak3KmV+8hyPlK2hxsehO+BK3XArGNaZq1qoNSDUJnxcmTLr7GqIgZlAqUHJYGAFm8IODZi2rhXcffNl+H00xqhfrayaEP9Vq1x1tV98MKb07DJ7Ybf55ZellwU49V70Ca7PurWbYjThbc4XYCSlkZT4KIuivFCP7Rrko06dRrgjMvvFUB0otxXhAnP3onTmortWdk4o8d9yCVQivzTEpRk4vsEvC6sXjINOY/fhb+c1xrHHiuuES2KcfTROOHsS3DHgJex4P0N8JX64BLH0HV1FkzAtZ1ORP169dGgYQM0yM5Gdv36yKa/coGMujjhhPbo1fdxDFuwCsUCrm5xrHkcZSRQkopF6B2EpQAngSgcKA1gnWNsw2xYv1MQ3Dk56pxqCUD9VD/6TAC0AKHcpqWNAkr1/2J1doxhkQsTUIPzuNXtudosG4JycDZO6PGWedcgMSgTKDMoyWgGTml5BVy2jXh/1SLkifBQ2vIVWLPZgUBphdLJo77Kwemwo2hDAfLzForwMw9LV6zGZhtt1+BCYwnt2Lx+NfIX5YkwPg/LVhbINC6nHZu07fOV7UXy2OSOk6yyURguwmm/CMU3rluB/HxxfRYuxMIlS7B8bSEc3jLheXorrwGNObVtwqqlS5S53do1NdhCLFv2HrbYvfDTMmtqu2xVQUleoj7MzhWfrUBpDuE1iMlOn7ueQP2sy7BC/E+SU/5UWBkWnCAISa/RCpTKcYpTSftVoOlNAyDlIz4H89UWu6D/I4FSHGPIX/VgQ84xXN41SAzKBMoKlJpRJ49Pm4WjzsTxhQmJXQKu/gB1aAQEPGgptHBpAoY0tHCvcXvqVgaqjtGbFz1ef+U1Eub3UkeUBejJ+6brKb6nPr3e/LTor2mR4iqDUkjvKVL4HQJKsU3br6XRywAakgEuAkrq/5SuElTRQBndo6wsUZc+EiiD25VQX+UkFVwZ9kfKuwaJQZlARQIlW3pYIkCZo+4Ll0dkUNJHCl8FnHK0bVbw0W9jUCZaDMoEikBJ0+yoMhIs2dLPiouLsXXr1riB0qoNM0QEGh1M9KG3lNjfOycHOUGYmuGjgKsStjGAMhhG08fextA72K6oy7cqoAyXdw0SgzKB+v777yUobTabpTfDlnqLZxuluV2S9oVT5VJkAjLBzhxNBD59pwh9rkwvjzFQMQZQiv+DHTOmtJXnUrksWpVAGSHvmiIGZQK1f/9+7N69G1999RX++9//sqWh0b357r
vvcOjQIXnPooHS4DGqg88lKE0ADeeNWorAYwBlnGWAXpyVyLzTSAxKFkunSKA0j6PUPCcCpWFfuJA7nBiUaS8GJYulkzl8Dmf68PKzGI8hixSOs9JXDEoWS6dooDQP9SFtWWmd1soYlJkpBiWLxWJFEYOSxWKxoohByWKxWFHEoGSxWKyIAv4fQBkl4L8F+oEAAAAASUVORK5CYII=\"></TD></TR>
  <TR>
    <TH>Identifier:</TH>
    <TD colspan=\"3\">CoSES_Models.Houses.SF1</TD></TR>
  <TR>
    <TH>Version:</TH>
    <TD colspan=\"3\">1.0</TD></TR>
  <TR>
    <TH>File:</TH>
    <TD colspan=\"3\">SF1.mo</TD></TR>
  <TR>
    <TH>Connectors:</TH>
    <TD>Electrical Low-Voltage AC Three-Phase Connector</TD>
    <TD>lV3Phase1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Environment Conditions Connector</TD>
    <TD>environmentConditions1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the condensing       
                boiler</TD>
    <TD>StandardCBcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the CHP</TD>
    <TD>StandardCHPcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                     pump</TD>
    <TD>StandardSTcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the DHW pump</TD>
    <TD>StandardDHWcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                      supply and return is constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP input - electric power</TD>
    <TD>CHPIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to district heating heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                      negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of domestic hot water pump to charge the DHW     
                    storage</TD>
    <TD>qvDHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Parameters:</TH>
    <TD>Heated 3-zone-building with changeable heating system</TD>
    <TD>simpleHeatedBuilding1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature controller wiring for heating system</TD>
    <TD>heatingUnitFlowTemperature1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow generation and electrical power calculation</TD>
    <TD>pump1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW_demand</TD>
    <TD>dHW_demand1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                components</TD>
    <TD>grid1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat storage with variable temperature profile</TD>
    <TD>WolfSPU2_800</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar Thermal</TD>
    <TD>WolfCRK12</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow generation and electrical power calculation</TD>
    <TD>pump2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Specifies a Thermal Volume Flow Connector</TD>
    <TD>defineVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Extracts the characteristics of a Thermal Volume Flow Connector</TD>
    <TD>extractVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                components</TD>
    <TD>grid2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat storage with variable temperature profile</TD>
    <TD>WolfSEM1_500</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Controllable valve for volume flow distribution</TD>
    <TD>distributionValve1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Valve for volume flow merging</TD>
    <TD>mergingValve1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Hydraulic Switch with two heat generators</TD>
    <TD>hydraulicSwitch2HG1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBControlIn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CHPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Boolean expression</TD>
    <TD>CHP_EVU</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>TColdWater</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Green city of the CHP model based on measurements in the laboratory</TD>
    <TD>neoTower2_GC1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>VolumeFlow_DHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature difference between flow and return temperature</TD>
    <TD>CBDeltaT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature of CB</TD>
    <TD>CBTmax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature of CB</TD>
    <TD>CBTmin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum electrical power of CHP</TD>
    <TD>CHPmax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum electrical power of CHP</TD>
    <TD>CHPmin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, solar thermal collector is CPC collector, else, solar thermal 
                        collector is a flat plate collector</TD>
    <TD>CPC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inclination angle of solar thermal collector</TD>
    <TD>alphaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Orientation angle of solar thermal collector</TD>
    <TD>betaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in series</TD>
    <TD>nSeries</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in parallel</TD>
    <TD>nParallel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Effective surface area of solar thermal collector</TD>
    <TD>AModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Absorber volume</TD>
    <TD>VAbsorber</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature within the thermal storage</TD>
    <TD>Tmax_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature level to calculate the stored energy (e.g. return          
               temperature of consumption)</TD>
    <TD>T0_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, the temperature profile at simulation begin within the        
                 storage is linear, if false the profile is defined by a 
      temperature                 vector</TD>
    <TD>TSLinearProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of upmost heat storage layer at simulation begin</TD>
    <TD>TSTupInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of lowmost heat storage layer at simulation begin</TD>
    <TD>TSTlowInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Vector of temperature profile of the layers at simulation begin,       
                  element 1 is at lowest layer</TD>
    <TD>TSTLayerVector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature within the thermal storage</TD>
    <TD>Tmax_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature level to calculate the stored energy (e.g. return          
               temperature of DHW</TD>
    <TD>T0_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, the temperature profile at simulation begin within the        
                 storage is linear, if false the profile is defined by a 
      temperature                 vector</TD>
    <TD>DHWLinearProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of upmost heat storage layer at simulation begin</TD>
    <TD>DHWTupInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of lowmost heat storage layer at simulation begin</TD>
    <TD>DHWTlowInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Vector of temperature profile of the layers at simulation begin,       
                  element 1 is at lowest layer</TD>
    <TD>DHWTLayerVector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of people living in the building</TD>
    <TD>nPeople</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nFloors</TD>
    <TD>nFloors</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nApartments</TD>
    <TD>nApartments</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heated (living) area (e.g. 50m² per person)</TD>
    <TD>ALH</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, use standard area-specific heating power, else define it      
                   manually</TD>
    <TD>UseStandardHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Area-specific heating power - modern radiators: 14 - 15 W/m²; space    
                     heating: 15 W/m²</TD>
    <TD>QHeatNormLivingArea</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1</TD>
    <TD>n</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 -     
                     45°C</TD>
    <TD>TFlowHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal return temperature - radiator: 45 - 65°C; floor heating: 28 -   
                         35°C</TD>
    <TD>TReturnHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference indoor temperature</TD>
    <TD>TRef</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximumg flow rate in Living Zone</TD>
    <TD>qvMaxLivingZone</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TLiving_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TRoof_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TCellar_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If the presence is used, individual presence data has to be provided,  
                       else standart presence is used</TD>
    <TD>UseIndividualPresence</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with presence timeseries (presence in %; 0% - no one is at home;  
                       100% - everyone is at home)</TD>
    <TD>PresenceFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If individual electricity consumption is used, individual consumption  
                       data ha to be provided, else standart load profiles are 
      used</TD>
    <TD>UseIndividualElecConsumption</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with electric consumption time series (consumption in kW)</TD>
    <TD>ElConsumptionFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Table with electric consumption time series (consumption in W)</TD>
    <TD>ElConsumptionTable</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>ElFactor</TD>
    <TD>ElFactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, night time reduction is activated, else temperature is        
                 constant</TD>
    <TD>ActivateNightTimeReduction</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionStart</TD>
    <TD>NightTimeReductionStart</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionEnd</TD>
    <TD>NightTimeReductionEnd</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at night</TD>
    <TD>Tnight</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, presence will be used to define the temperature (if less      
                   people are at home, less rooms are heated and the average     
        temperature       will       decrease)</TD>
    <TD>VariableTemperatureProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature, when noone is at home (TRefSet = TMin + (TRef -   
                      TMin) * Presence(t))</TD>
    <TD>TMin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true: DHW consumption data repeats weekly</TD>
    <TD>WeeklyData</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Data File</TD>
    <TD>File</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Table Name</TD>
    <TD>Table</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>V_DHWperDay_l</TD>
    <TD>V_DHWperDay_l</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Factor, with which the DHW consumption gets multiplied</TD>
    <TD>DHWfactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CB turns on (SOCmin &lt; SOCmin and T<tstart)< 
      td=\"\">         </tstart)<>             
    <TD>SOCminCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CB turns off</TD>
    <TD>SOCmaxCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the CB turns on (T<tstart 
      td=\"\" and=\"\" socmin=\"\" <=\"\" socmin)<=\"\">         </tstart>             
    <TD>TStartCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the CB stops</TD>
    <TD>TStopCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CHP turns on (SOCmin &lt; SOCmin and T<tstart)< td=\"\"> 
                                </tstart)<>             
    <TD>SOCminCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>SOC, at which the CHP turns off</TD>
    <TD>SOCmaxCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the CHP turns on (T<tstart 
      td=\"\" and=\"\" socmin=\"\" <=\"\" socmin)<=\"\">         </tstart>             
    <TD>TStartCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the CHP stops</TD>
    <TD>TStopCHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature difference between collector and storage         
                temperature</TD>
    <TD>deltaTonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-off-temperature difference between collector and storage        
                 temperature</TD>
    <TD>deltaToffST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature difference between flow and return, qv=qvMin</TD>
    <TD>deltaTFlowReturnLowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature difference between flow and return, qv=qvMax</TD>
    <TD>deltaTFlowReturnUpST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum volume flow of circulation pump</TD>
    <TD>qvMinST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum volume flow of circulation pump</TD>
    <TD>qvMaxST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature of DHW storage charging</TD>
    <TD>TonTS_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-off temperature of DHW storage charging</TD>
    <TD>ToffTS_DHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature, if the DHW temperature &lt; DHW consumption     
                    temperater + deltaTonDHW, the storage will be charged</TD>
    <TD>deltaTonDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum volume flow of circulation pump</TD>
    <TD>qvMaxDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>qvSTpump_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CHPin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>WolfCGB14 validated with CoSES mearusements</TD>
    <TD>wolfCGB14_GC1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Results:</TH>
    <TD>If true, standard control will be used to control the condensing       
                boiler</TD>
    <TD>StandardCBcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the CB modulation</TD>
    <TD>CBinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the CHP</TD>
    <TD>StandardCHPcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the CHP modulation</TD>
    <TD>CHPinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                     pump</TD>
    <TD>StandardSTcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on/off of solar thermal pump</TD>
    <TD>CPonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvRefST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal collector temperature</TD>
    <TD>TCollectorST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of solar thermal heat storage connection</TD>
    <TD>TStorageSTConnection</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature solar thermal</TD>
    <TD>TFlowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature solar thermal</TD>
    <TD>TReturnST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the DHW pump</TD>
    <TD>StandardDHWcontrol</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on/off of DHW pump</TD>
    <TD>CPonDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control flow rate of the DHW pump</TD>
    <TD>qvRefDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler flow temperature</TD>
    <TD>CB_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler return temperature</TD>
    <TD>CB_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow condensing boiler</TD>
    <TD>CB_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas flow condensing boiler</TD>
    <TD>CB_S_FG</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Demanded fuel volume</TD>
    <TD>CB_VFuel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of condensing boiler</TD>
    <TD>CB_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas power of condensing boiler</TD>
    <TD>CB_P_gas_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of condensing boiler</TD>
    <TD>CB_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas input of condensing boiler</TD>
    <TD>CB_E_gas_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating efficiency</TD>
    <TD>CB_Efficiency</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP flow temperature</TD>
    <TD>CHP_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP return temperature</TD>
    <TD>CHP_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow CHP</TD>
    <TD>CHP_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas flow CHP</TD>
    <TD>CHP_S_FG</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Demanded fuel volume</TD>
    <TD>CHP_VFuel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of CHP</TD>
    <TD>CHP_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric power output of CHP</TD>
    <TD>CHP_P_el_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas power of CHP</TD>
    <TD>CHP_P_gas_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of CHP</TD>
    <TD>CHP_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electricity output of CHP</TD>
    <TD>CHP_E_el_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas input of CHP</TD>
    <TD>CHP_E_gas_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal efficiency</TD>
    <TD>CHP_Efficiency_th</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electric efficiency</TD>
    <TD>CHP_Efficiency_el</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Overall efficiency</TD>
    <TD>CHP_Efficiency_total</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal flow temperature</TD>
    <TD>ST_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal return temperature</TD>
    <TD>ST_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>ST_S_TM_Collector</TD>
    <TD>ST_S_TM_Collector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow solar thermal</TD>
    <TD>ST_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of solar thermal</TD>
    <TD>ST_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of solar thermal</TD>
    <TD>ST_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Power transfered to DHW storage</TD>
    <TD>TS_P_heat_toHWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature consumption side</TD>
    <TD>TS_S_TM_HC_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature consumption side</TD>
    <TD>TS_S_TM_HC_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature producer side</TD>
    <TD>TS_S_TM_PS_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature producer side</TD>
    <TD>TS_S_TM_PS_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature to DHW storage</TD>
    <TD>TS_S_TM_HC_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature to DHW storage</TD>
    <TD>TS_S_TM_HC_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 1</TD>
    <TD>TS_S_TM_HWS_1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 2</TD>
    <TD>TS_S_TM_HWS_2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW storage temperature 3</TD>
    <TD>TS_S_TM_HWS_3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 1</TD>
    <TD>TS_S_TM_BT_1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 2</TD>
    <TD>TS_S_TM_BT_2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 3</TD>
    <TD>TS_S_TM_BT_3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 4</TD>
    <TD>TS_S_TM_BT_4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 5</TD>
    <TD>TS_S_TM_BT_5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 6</TD>
    <TD>TS_S_TM_BT_6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 7</TD>
    <TD>TS_S_TM_BT_7</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 8</TD>
    <TD>TS_S_TM_BT_8</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 9</TD>
    <TD>TS_S_TM_BT_9</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 10</TD>
    <TD>TS_S_TM_BT_10</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to DHW storage</TD>
    <TD>TS_S_FW_HC_HW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy in thermal storage</TD>
    <TD>TS_E_Storage_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy in DHW storage</TD>
    <TD>TS_E_Storage_HWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Total energy transfered to DHW storage</TD>
    <TD>TS_E_heat_toHWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>State of charge of the thermal storage</TD>
    <TD>TS_SOC_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>State of charge of the DHW storage</TD>
    <TD>TS_SOC_HWS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature before mixing unit</TD>
    <TD>HS_S_TM_VL_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature after mixing unit</TD>
    <TD>HS_S_TM_VL_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature</TD>
    <TD>HS_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink flow temperature hot water</TD>
    <TD>HS_S_TM_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature hot water</TD>
    <TD>HS_S_TM_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature in the house</TD>
    <TD>HS_S_TM_Room</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow after mixing unit</TD>
    <TD>HS_S_FW_HC_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow before mixing unit</TD>
    <TD>HS_S_FW_HC_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow domestic hot water consumption</TD>
    <TD>HS_S_FW_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating power</TD>
    <TD>HS_P_DemHeatHC_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water power</TD>
    <TD>HS_P_DemHeatHW_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating Energy</TD>
    <TD>HS_E_DemHeatHC_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water energy</TD>
    <TD>HS_E_DemHeatHW_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - set temperature (power is constant)</TD>
    <TD>CBIn_TSet</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                      supply and return is constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>CHP input - electric power</TD>
    <TD>CHPIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to district heating heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                      negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of domestic hot water pump to charge the DHW     
                    storage</TD>
    <TD>qvDHWpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXin</TD>
    <TD>&nbsp;</TD></TR></TBODY></TABLE></DIV>
<H2>Description:</H2>
<P>&nbsp;</P></BODY></HTML>
"),
		experiment(
			StopTime=1,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.001,
			__esi_Solver(
				bEffJac=false,
				bSparseMat=false,
				typename="CVODE"),
			__esi_SolverOptions(
				solver="CVODE",
				typename="ExternalCVODEOptionData"),
			__esi_MinInterval="9.999999999999999e-10",
			__esi_AbsTolerance="1e-6"));
end SF2;
