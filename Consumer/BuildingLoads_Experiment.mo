﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo.Consumer;
model BuildingLoads_Experiment "Heating, Cooling, DHW and Power Loads"
	import GreenCity.BuildingAndConsumers.*;
	import GreenCity.*;
	GreenCity.Interfaces.Thermal.VolumeFlowIn Flow_Heating "Flow Pipe" annotation(Placement(
		transformation(extent={{-105,50},{-85,70}}),
		iconTransformation(extent={{186.7,-60},{206.7,-40}})));
	GreenCity.Interfaces.Thermal.VolumeFlowOut Return_Heating "Return Pipe" annotation(Placement(
		transformation(extent={{-105,0},{-85,20}}),
		iconTransformation(extent={{186.7,-10},{206.7,10}})));
	GreenCity.Interfaces.Thermal.VolumeFlowIn Flow_Cooling "Flow Pipe" annotation(Placement(
		transformation(extent={{-105,-50},{-85,-30}}),
		iconTransformation(extent={{186.7,-160},{206.7,-140}})));
	GreenCity.Interfaces.Thermal.VolumeFlowOut Return_Cooling "Return Pipe" annotation(Placement(
		transformation(extent={{-105,-100},{-85,-80}}),
		iconTransformation(extent={{186.7,-110},{206.7,-90}})));
	parameter String File=CoSES_ProHMo.LibraryPath + "Data\\Consumption\\Simplified\\Test.txt" "File with consumption timeseries" annotation(Dialog(tab="Parameters"));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow defineVolumeHeating "Specifies a Thermal Volume Flow Connector" annotation(Placement(transformation(extent={{-35,20},{-55,0}})));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow extractVolumeHeating annotation(Placement(transformation(extent={{-55,70},{-35,50}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow defineVolumeCooling annotation(Placement(transformation(extent={{-35,-80},{-55,-100}})));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow extractVolumeCooling annotation(Placement(transformation(extent={{-55,-30},{-35,-50}})));
	Real T_RetHeating(quantity="Basics.Temp") "Set return temperature for heating" annotation(Dialog(
		group="Temperature",
		tab="Parameters"));
	Real T_RetCooling(quantity="Basics.Temp") "Set return temperature for cooling" annotation(Dialog(
		group="Temperature",
		tab="Parameters"));
	parameter Real T_RetDHW(quantity="Basics.Temp")=303.15 "Set return temperature for DHW" annotation(Dialog(
		group="Temperature",
		tab="Parameters"));
	parameter Real T_MinDHW(quantity="Basics.Temp")=323.15 "Minimum supply temperature DHW" annotation(Dialog(
		group="Temperature",
		tab="Parameters"));
	parameter Real TSet(quantity="Basics.Temp")=294.15 "Set temperature (internal value, no real temperature!)" annotation(Dialog(
		group="Temperature",
		tab="Parameters"));
	parameter Real deltaT(quantity="Thermodynamics.TempDiff")=0.01 "Temperature difference for control (internal value, no real temperature!)" annotation(Dialog(
		group="Temperature",
		tab="Parameters"));
	parameter Real qv_MaxHeating(quantity="Thermics.VolumeFlow")=0.00025 "Maximum volume flow heating" annotation(Dialog(
		group="Volume flow",
		tab="Parameters"));
	parameter Real qv_MaxCooling(quantity="Thermics.VolumeFlow")=0.00025 "Maximum volume flow cooling" annotation(Dialog(
		group="Volume flow",
		tab="Parameters"));
	parameter Real qv_MaxDHW(quantity="Thermics.VolumeFlow")=0.0005 "Maximum volume flow DHW" annotation(Dialog(
		group="Volume flow",
		tab="Parameters"));
	parameter Real cpMed(quantity="Thermics.SpecHeatCapacity")=4177 "Specific Heat Capacity of heating medium" annotation(Dialog(
		group="Medium",
		__esi_groupCollapsed=true,
		tab="Parameters"));
	parameter Real rhoMed(quantity="Basics.Density")=1000 "Density" annotation(Dialog(
		group="Medium",
		tab="Parameters"));
	parameter Real CHeating(quantity="Thermics.HeatCapacity")=400000000 "Mass heat capacity (internal value, no real temperature!)." annotation(Dialog(
		group="Medium",
		tab="Parameters"));
	parameter Real CosPhi=0.95 "CosPhi" annotation(Dialog(
		group="Electric consumption",
		tab="Parameters"));
	Modelica.Blocks.Interfaces.RealInput UnixTime(quantity="Basics.Time") "Unix time stamp" annotation(
		Placement(
			transformation(extent={{90,-75},{130,-35}}),
			iconTransformation(
				origin={50,200},
				extent={{-20,-20},{20,20}},
				rotation=-90)),
		Dialog(
			tab="Results",
			visible=false));
	Modelica.Blocks.Interfaces.BooleanOutput Cooling "True, if cooling demand" annotation(
		Placement(
			transformation(extent={{90,-10},{110,10}}),
			iconTransformation(
				origin={99.7,-196.7},
				extent={{-10,-9.699999999999999},{10,10.3}},
				rotation=-90)),
		Dialog(
			tab="Results",
			visible=false));
	Modelica.Blocks.Interfaces.BooleanOutput Heating "true, if heating demand" annotation(
		Placement(
			transformation(extent={{90,-30},{110,-10}}),
			iconTransformation(
				origin={50,-196.7},
				extent={{-10,-10},{10,10}},
				rotation=270)),
		Dialog(
			tab="Results",
			visible=false));
	Modelica.Blocks.Interfaces.RealOutput T_RefHeating "reference temperature heating" annotation(
		Placement(
			transformation(extent={{90,65},{110,85}}),
			iconTransformation(
				origin={0,-196.7},
				extent={{-10,10},{10,-10}},
				rotation=-90)),
		Dialog(
			group="Temperatures",
			tab="Results",
			visible=false));
	Real T_SupHeating(quantity="Basics.Temp") "Heating supply temperature" annotation(Dialog(
		group="Temperatures",
		tab="Results",
		visible=false));
	Real T_SupCooling(quantity="Basics.Temp") "Cooling supply temperature" annotation(Dialog(
		group="Temperatures",
		tab="Results",
		visible=false));
	Real T_SupDHW(quantity="Basics.Temp") "DHW supply temperature" annotation(Dialog(
		group="Temperatures",
		tab="Results",
		visible=false));
	Real THeating(quantity="Basics.Temp") "Heating temperature (internal value, no real temperature!)" annotation(Dialog(
		group="Temperatures",
		tab="Results",
		visible=false));
	Real TCooling(quantity="Basics.Temp") "CoolingTemperature" annotation(Dialog(
		group="Temperatures",
		tab="Results",
		visible=false));
	Modelica.Blocks.Interfaces.RealOutput qv_RefHeating(quantity="Thermics.VolumeFlow") "reference volume flow heating" annotation(
		Placement(
			transformation(extent={{90,15},{110,35}}),
			iconTransformation(
				origin={-50,-196.7},
				extent={{-10,10},{10,-10}},
				rotation=-90)),
		Dialog(
			group="Volume flow",
			tab="Results",
			visible=false));
	Modelica.Blocks.Interfaces.RealOutput qv_RefCooling(quantity="Thermics.VolumeFlow") "reference volume flow cooling" annotation(
		Placement(
			transformation(extent={{90,40},{110,60}}),
			iconTransformation(
				origin={-100,-196.7},
				extent={{-10,10},{10,-10}},
				rotation=-90)),
		Dialog(
			group="Volume flow",
			tab="Results",
			visible=false));
	Real qv_RefDHW(quantity="Thermics.VolumeFlow") "Set volume flow for DHW" annotation(Dialog(
		group="Volume flow",
		tab="Results",
		visible=false));
	Real qv_Heating(quantity="Thermics.VolumeFlow") "Actual volume flow for Heating" annotation(Dialog(
		group="Volume flow",
		tab="Results",
		visible=false));
	Real qv_Cooling(quantity="Thermics.VolumeFlow") "Actual volume flow for Cooling" annotation(Dialog(
		group="Volume flow",
		tab="Results",
		visible=false));
	Real qv_DHW(quantity="Thermics.VolumeFlow") "Actual volume flow for DHW" annotation(Dialog(
		group="Volume flow",
		tab="Results",
		visible=false));
	Real P_El(quantity="Basics.Power") "Electric power consumption" annotation(Dialog(
		group="Power",
		tab="Results",
		visible=false));
	Real Q_El(quantity="Basics.Power") "Reactive power" annotation(Dialog(
		group="Power",
		tab="Results",
		visible=false));
	Real P_SetHeating(quantity="Basics.Power") "Set heating power consumption" annotation(Dialog(
		group="Power",
		tab="Results",
		visible=false));
	Real P_Heating(quantity="Basics.Power") "Actual heating power consumption" annotation(Dialog(
		group="Power",
		tab="Results",
		visible=false));
	Real P_SetCooling(quantity="Basics.Power") "Cooling power consumption" annotation(Dialog(
		group="Power",
		tab="Results",
		visible=false));
	Real P_Cooling(quantity="Basics.Power") "Actual cooling power consumption" annotation(Dialog(
		group="Power",
		tab="Results",
		visible=false));
	Real P_SetDHW(quantity="Basics.Power") "DHW power consumption" annotation(Dialog(
		group="Power",
		tab="Results",
		visible=false));
	Real P_DHW(quantity="Basics.Power") "Actual DHW power consumption" annotation(Dialog(
		group="Power",
		tab="Results",
		visible=false));
	Real E_El(quantity="Basics.Energy") "Electric energy" annotation(Dialog(
		group="Energy",
		tab="Results",
		visible=false));
	Real E_SetHeating(quantity="Basics.Energy") "Set heating energy" annotation(Dialog(
		group="Energy",
		tab="Results",
		visible=false));
	Real E_Heating(quantity="Basics.Energy") "Actual heating energy" annotation(Dialog(
		group="Energy",
		tab="Results",
		visible=false));
	Real E_SetCooling(quantity="Basics.Energy") "Set cooling energy" annotation(Dialog(
		group="Energy",
		tab="Results",
		visible=false));
	Real E_Cooling(quantity="Basics.Energy") "Actual cooling energy" annotation(Dialog(
		group="Energy",
		tab="Results",
		visible=false));
	Real E_SetDHW(quantity="Basics.Energy") "Set DHW energy" annotation(Dialog(
		group="Energy",
		tab="Results",
		visible=false));
	Real E_DHW(quantity="Basics.Energy") "Actual DHW energy" annotation(Dialog(
		group="Energy",
		tab="Results",
		visible=false));
	Modelica.Blocks.Tables.CombiTable1Ds Load(
		tableOnFile=true,
		tableName="Consumption",
		fileName=File,
		columns={2,3,4}) annotation(Placement(transformation(extent={{55,-65},{35,-45}})));
	GreenCity.Interfaces.Thermal.VolumeFlowIn Flow_DHW "Flow Pipe" annotation(Placement(
		transformation(extent={{-105,-150},{-85,-130}}),
		iconTransformation(extent={{186.7,40},{206.7,60}})));
	GreenCity.Interfaces.Thermal.VolumeFlowOut Return_DHW "Return Pipe" annotation(Placement(
		transformation(extent={{-105,-200},{-85,-180}}),
		iconTransformation(extent={{186.7,90},{206.7,110}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow defineVolumeDHW annotation(Placement(transformation(extent={{-35,-180},{-55,-200}})));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow extractVolumeDHW annotation(Placement(transformation(extent={{-55,-130},{-35,-150}})));
	equation
		//Dictating if we are in cooling or heating. Setting 100W as the minimum since the minimum for to eradicate reading errors from the sensors
		
		if Load.y[2]>500 then
			Heating=true;
			Cooling=false;
		elseif Load.y[2]<-500 then 
			Heating=false;
			Cooling=true;	
		else
			Heating=false;
			Cooling=false;
		end if;
		
		// Heating
		//Set the references for power, flowrate and return temperature from text file 
		if Heating then
			P_SetHeating = max(0, Load.y[2]);  // set heating power in W
			qv_RefHeating = max(0, Load.y[1]/(1000*60));  // set heating power in l/min
		else
			P_SetHeating = 0; 
			qv_RefHeating =0;
		end if;
		
		T_RefHeating=max(0, Load.y[3]+273.15);
		
		//Setting the actual supply of flowrate and temperature coming from the model
		qv_Heating = extractVolumeHeating.qvMedium;  // actual volume flow
		T_SupHeating = extractVolumeHeating.TMedium;  // actual supply temperature
		
		//Then we know the heating power (from text file) and the actual temperature and flowrate from the model
		//We can thus calculate what the return temperature is. Need to make sure it's not zero so we don't divide by zero.
		T_RetHeating= T_SupHeating -P_SetHeating/(rhoMed * cpMed*max(1e-8,qv_Heating));
		
		defineVolumeHeating.TMedium = T_RetHeating; // Return temperature
		
		P_Heating = qv_Heating * rhoMed * cpMed * max(0, T_SupHeating - T_RetHeating);  // actual heating power
		
		der(E_SetHeating) = P_SetHeating;
		der(E_Heating) = P_Heating;
		
		//Cooling
		//Set the references for power and flowrate from text file
		if Cooling then
			P_SetCooling = min(0, Load.y[2]);  // set cooling power in W
			qv_RefCooling = max(0, Load.y[1]/(1000*60));  // set cooling flowrate in l/min
		else
			P_SetCooling = 0; 
			qv_RefCooling =0;
		end if;
		
		//Setting the actual supply of flowrate and temperature coming from the model
		qv_Cooling = extractVolumeCooling.qvMedium;  // actual volume flow
		T_SupCooling = extractVolumeCooling.TMedium;  // actual supply temperature
		
		//Then we know the heating power (from text file) and the actual temperature and flowrate from the model
		//We can thus calculate what the return temperature is. Need to make sure it's not zero so we don't divide by zero.
		T_RetCooling= T_SupCooling -P_SetCooling/(rhoMed * cpMed*max(1e-8,qv_Cooling));
		//T_RetCooling= T_SupCooling+10;
		
		defineVolumeCooling.TMedium = T_RetCooling; // Return temperature
		
		P_Cooling = qv_Cooling * rhoMed * cpMed * min(0, T_SupCooling - T_RetCooling);  // actual cooling power
		
		der(E_SetCooling) = P_SetCooling;
		der(E_Cooling) = P_Cooling;
		
		//DHW
		//Set to zero
		defineVolumeDHW.TMedium=273.15+50;
		T_SupDHW = extractVolumeDHW.TMedium;
		qv_DHW=extractVolumeDHW.qvMedium;
		
		P_SetDHW = 0;
		P_DHW = 0;
		
		qv_RefDHW=0;
		defineVolumeDHW.qvMedium = qv_RefDHW;
		
		der(E_SetDHW) = P_SetDHW;
		der(E_DHW) = P_DHW;
	initial equation
		E_El = 0;
		E_SetHeating = 0;
		E_Heating = 0;
		E_SetCooling = 0;
		E_Cooling = 0;
		E_SetDHW = 0;
		E_DHW = 0;
		
		THeating = TSet;
		TCooling = TSet;
	equation
		connect(extractVolumeHeating.Pipe,Flow_Heating) annotation(Line(
			points={{-55,60},{-60,60},{-90,60},{-95,60}},
			color={190,30,45},
			thickness=0.0625));
		connect(defineVolumeHeating.Pipe,Return_Heating) annotation(Line(
			points={{-55,10},{-60,10},{-90,10},{-95,10}},
			color={190,30,45}));
		connect(extractVolumeHeating.qvMedium,defineVolumeHeating.qvMedium) annotation(Line(
			points={{-35,55},{-30,55},{-30,15},{-35,15}},
			color={0,0,127},
			thickness=0.0625));
		connect(extractVolumeCooling.Pipe,Flow_Cooling) annotation(Line(
			points={{-55,-40},{-60,-40},{-90,-40},{-95,-40}},
			color={190,30,45},
			thickness=0.0625));
		connect(extractVolumeCooling.qvMedium,defineVolumeCooling.qvMedium) annotation(Line(
			points={{-35,-45},{-30,-45},{-30,-85},{-35,-85}},
			color={0,0,127},
			thickness=0.0625));
		connect(defineVolumeCooling.Pipe,Return_Cooling) annotation(Line(
			points={{-55,-90},{-60,-90},{-90,-90},{-95,-90}},
			color={190,30,45}));
		connect(Load.u,UnixTime) annotation(Line(
			points={{57,-55},{62,-55},{105,-55},{110,-55}},
			color={0,0,127},
			thickness=0.0625));
		connect(defineVolumeDHW.Pipe,Return_DHW) annotation(Line(
			points={{-55,-190},{-60,-190},{-90,-190},{-95,-190}},
			color={190,30,45}));
		connect(extractVolumeDHW.Pipe,Flow_DHW) annotation(Line(
			points={{-55,-140},{-60,-140},{-90,-140},{-95,-140}},
			color={190,30,45},
			thickness=0.0625));
	annotation(
		__esi_protocols(transient={
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																								Protocol(var=time),
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																								Protocol(var=statAnaRefVar)}),
		__esi_solverOptions(
			solver="CVODE",
			typename="ExternalCVODEOptionData"),
		statAnaRefVar(__esi_flags=2),
		__esi_solver(
			bEffJac=false,
			bSparseMat=false,
			typename="CVODE"),
		Icon(
			coordinateSystem(extent={{-200,-200},{200,200}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAADUQAAA1EBvbkFJwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABUpSURB
VHhe7Z0JdFRFuscDOHJG5j0dDcvoOBCCMAiyKItBJaLOU4dRFBcU57iguAsOwvjUQYVRfCAIjAsK
IiAMLqCAKBrAhKxANpJA9j1k66Q73el78LjyvfrX7e65t7u607dzO7kNt8/5nXTXV925Xf9bVV99
tXSU8pHUd8Rvvu0XMz+pb0xGYt/BbQwyiQD6DbZBs6TomGegoUtO9ePbvjGTWeYmnzebRBqN+/vG
XOWSVX4kRg+KZ3fBD4LMJpEI0xIVlouLKs0SzZp7yhHbkNC/f58o9LmiDKnDx9OhKVNNIgBoJdIQ
fTKrwdyhUhmKnltE7W12cjqdJhEAtCp87mWVhi7SouCBKRNxN5jiRh7QLPWP45Ticu86SpXAQJUX
fcDpwIkTJyIOSZI81w/tvPQ86SvwNX9RfenTiR9//JFOnjxJzdt3Ud2a9w2NZdcefq3ff/+95/qh
nbeepsAKIHDV0lWq8jAyx9d/aAqsBQice/NdvBzSR00iZ95ROvnLL4bCkZnj6WuLnpxvCqwFpcAn
yqt4E2hE2o8U8Gs0BdaIUmBRwRoJXKMpsEZCEfgX1mxOnz6dYmNjqba2VphHCz169PAwefJkYR6A
azQF1ohWgSsrK2nQhRfS6l59KOVX51C/3r3pzTffFOYNlllX9CDn6iiOyO4G12gKrBEtAi9evJhi
zuxNGUzYujPP5ZQx7uvZmyZOnEg///yz8H0d0e4S9/XbTIF1JxiBUaCXjBxJc3v9mipcwiqpZXx6
xn/ReeecQ1lZWcLPCIS79qKJFtnd4BrDJrDdbqeGhgahLZLpSOCdO3fSb/v0ob1nnO0jrDf5v/ot
jTmjN82ePVv4WSLmz5/vEbi4uFiYxw2uMWwCr1+/nmbOnCm0RTKBBJ42bRrF9TqTCphw1UzAYChn
vNLrLLpoyBAeVvT+TG9QayFu4zK5eYYDh7SysjKfvLjGsAk8adIkGjhwIK/JInuk4k9gpA8bNoz3
raEwevRoSktLU32miJtGywKfeUYPDp6jTxblxTWGReD6+no6++yzqWfPnrR161ZhnkglkMBLlixR
pWmhpKQkKIFblsvNs5Irh4j7YlxjWAR+4YUXKCoqinPTTTcJ80QqnRX4p59+otGjRtFI5oSNGDGC
MjMzeXqwArtFRRONCoTnfVifL8qLawyLwGie3QLHxMRQY2OjMF8kEozAGRkZlJqaqgJ9pTsffBM8
b2pqopSUFP48GIGXLl1KrSuiqDcbS+N1XFwcF7impsYnL8A16i5wbm4unXfeeR6B4QDgi4vyRiId
CQwh4WxhBOGmoKCAHA6HJ9/dd9/NazK6Mi0C9+rVS/UaZdu+yv9YGNeou8CzZs1SiYu/8fHxwryR
SDACz5gxQ2VraWnxCAwWLlxIr776Kg+EINKFtGCbaCVXxPagCTH+x8K4Rt0FRr/y4IMP8rt07Nix
tGDBAh6DLS0tFeaPNPQQWEQoAifMjeLOrMgGcI26C9za2sr/vvTSSzRlyhT+HEOlU2W45BG43xBV
YXalwMnJyfwv+l90Ad52N9AqLE4WUAp8KgEhi+aU0OHJ2arC7EqB4ZwNGjSIHKz/3bNnD61Zs0aY
79Ckb6lo3gpTYC1wgeeWCgVGn/rdd9/xqcH29nYPVVVVZLFYeEH7A46YliYatXfMhT3oov7++2Bc
Y+WSav757us3Be6AQAJfcMEFFB0dTWeddZYPGFnA5o9zzz2XDh48qPrMQHwyWx4Pt7W1Ce3AFDgE
/AkMMP2H4U8oaJ06jP6NPOEvsrkxBQ6BQAJ3Jd5jYhGmwCEAgffNLaaU+O4VeNOmTcJ0JYYUuH5P
gjDdKEDguQtKKP4G7RP1XU3S1Tm0YVmFcQTG3pnM/7lFaDMKkSQwrvHVZVXGEbhhbyIl9oultto6
od0IhENgjJ0/WLuW1r7zDt37x+H8+dYtW4R5tWA4gYueX0RJAy6i47u+EtqNQDhr8IkT39GsUaOF
tlAwnMAZcddR/sNzqPjl14R2I2AK7EKrwK2FxZR+6VXUsD+JsqfdJcxjBEyBXWgVuOLN9+jYM8+T
o6WFkoeMMewGc70EfiImlqxWK49RgzVjL1Px1MyZHluo66cNJXD2rfdQ/Vff8OeHr72Jmg9m+uQx
AnoJ/BQT2FvUQNhsNuHnBMIwAjuaLZRy0VhyuKYaj81/gSreXuuTzwh0l8BLFy0Sfk4gDCNw7fad
lDvzQc/rmn9/SnkPPanKYxT0EviRgTGs4H8Q2pS8PebSyBe44IlnqGrdRs9ra0kZd7iUeYxCOJ0s
EW9FvMDt7ZR2yeVkLatQpaeNiiNbZZUqzQgYUWB/D0MI3Jx+SHhqT959j/Km2zu9u+lKgXft2kV3
jB9Pt48bR3dcNk6YB/h7GELgkleWcbzTy1e9wyNb3undTVfX4JTkZLonLk5oc+PvYQiB8VlNaQd9
0ptS0ynzhuk+6d1NVwu8avRY3kSLbG78PbpdYPSxqRdPoHY2mPe2tdvaKDl2NDmsVh9bd6IU+AdL
q7DA9WRvQgLNfeQRoc2Nv0e3C1z1/iYqeHye0AZQg5tS0oS27gICPzmvmBfea8/n0I+OdmGhdyWi
h1P6qfsFPnLPQ1S7bYfQBtAHoy8W2boLCLz7+U288CKB/W/s6h6BHVabHL1qahbaAbxoeNMiW3cB
gVFrvnp4Kc24N9/QHHhC3gzXLQJjaU72LYFPALBV11DayMuFtu4ChQWRIwms1XZff5cJzOPN/3pX
aFOCiBYiWyJbx7STM38DOVuqBbbTky4TOH1cPLUeLRTalCAmjdi0yBaQir0kfTSOpLXnkTN1oTjP
aUiXCNySm0cZl18rtHmDWSXUdpFNhNRURM6v7iJp00XkLNhEzoY8ktZfQE67sYZb3YVuAr++aB4t
vne40Fa24k3+kwAimzeYF8b8sMimorWWnPsfJ+n935GUuZycDpvHJu24kaT89er8pym6Cbxi0VzK
Xfzf5PzyTnLa1OdlZf3lTr6CUpnmj3a7na/wwEoPkR01Uzq4hAk7gKTEp9n/qvfNU7qbnFsv9U0/
DdFNYDTR111zNUkpz5G0YRA5K/fzdHtDI6UMvZRHqrzf4w+s0cJaLR9b4SckbYwl6YtbWVNc4GtX
IG25hPXL+4S20wldBfb0wSVfsH7w91zsmq2fUt79j/nkDwRWWZa9vtrzWqo6QNInk7gTBWdKmdcv
OW+RtDt8sW37cUHLYUDCIzBoqSTnZ38iy5ILqXajtugU1knn3j2LnG4HauMQ2YHCMEiQX4i9lffP
zsajYnsnsFVVU9ELi4U2oxE+gRnt9jYqmfEHcq5jjlDxZypbINoqj1HlA+w9rJ91pjPnjIklytcR
UvKzcj8tsHWGms0fU8qwy3h0TmQ3EmEVuPFAKt97xJtY1nc69z0aePgCTzhrNWvez6faOReQNSdV
nC9YWCsiretPzrYmsT1EsFg/OeYSqvlom9BuJMIqcPGLr1Lp/70hv4ZnvfsO2bttyFfl48CB2jCY
pM9v4HbMOlVv0uFYRNbEOzNXiG2hgCVHIy+nyvc2dBh6NQJhFfjglddTc2a2Kk068p48xMn/QH5d
ncwcqCtI2noZOcvlddIAi/KOzX3W8zpUpJpUuQ9v952DDgVLVg4duuoGPpxLG31FUNG57iRsAiOe
nDZ6Er/jlfk4dZkkbR7BazNqrciBsuQc4QWpTAsV6ZM4TT5AIMrfeMsTtCn551IqfsnYp/qFTeCK
d9bRsaf/V5VHRZtFrsX++mR2Y2D8jHG00K6Fo5vJuf0asU0j2dP/6tm0zm/iSy7XNMbvasImcM4d
93Z6W2j2bawwvw5y3BuIdrscfKn1XQumBezE8I6y4RrrPtulymckwiIwCoBP7vsLNwZJySuvc0Q2
rUgZr5CUwMbWAluw4GbzdqywQgU3szLNSIRF4Loduylnxv0+ebTCC5TVEJFNM8yLl9b1I2drjdge
BAhuYOJEmYZdkVi0by0x5tmcYRH46Ny/U8W7nZ/NwfIetASiVZghsfdhcqa9JLYFwaH4P1Pz4Syf
9OKFrwjXehuBsAiM4YNed/ShyTdyj1pk00z9EXmuWDG1GCwIT2LJr2hUgM3sGDFg6ORt6250Fxh3
uF7DG4CxsHKjWmeRPr+enHnyGFwLCE/mPfSU/Bpj6ox/ktN63GPPnna3JqcSDlvd518IbXqiu8CI
XOk5NkQ0q+CxvwltIVGyi6SPxottAch/ZK4cWbNUkHPbFN6fSwfme+w1W7fJEySK9/iFtQJYmpQx
UZ+hWyB0Fzjz+lt5DFqUJxQQKQp2uY+Shm8PCNOBtGUkOSu/FdqEuMKT9kwmsGsaFELzOLelnOfB
jozUERPJWq7eNSmidOlKvgjiYPyNfMuOKI9e6Crw1Mnx/IeKde2LWOGmDh9PbXX/aQ47wlpcQgcG
jvC/OjP7XyR9ebvYJsCSnUPlDw0l6YM/qOajpeS/k3PfI57Xhc+9TKWvBY57o1nGAkScDcbPKfkb
u1kE+fRCV4HnjB1P+bPnCO2dAU2flv4NG8zR/MGbF9k9c8WYbxbZlbRUknX1cGp5fZi8Dkxps9XL
tdi1uqTlSD5f9uvP60dcHi2B22lsq6nlN284px11FfitIeGZQsPqjmDP0kLtRVNpq6jkhYnXonxS
8gKSkvzvk+KUfslXptTMG0X1X30tzgNna89/gh9Zf75dGH1DC5Q+/moeI1Cm5971QFj3Resm8KKF
L9KXA4aQvV7/H6jkZ2ndPENo8yb/0ac9wQg4fH5npJpL5cBHm2ArDUKbWFv2wUByFO/pYBEgS19/
ITnrDvPX1R9u9dl+g1g1+tyyN9RBElDz8XbVeSV6o5vAKx57gjbEXCy0dRYUbjBnabUWHOPBf4fF
wl8jUMIdH39jcsxPZ61SpzWVyOu/dk5lw6A6YXjSh6yVJO26mT/HtaLZxTYctx2jANx4nvwKuHMG
HyNM53XqJjCa6Ouuvlpo04NgztLCONV7dyI8Vn99sVSdIi+Yd09VsiEU95IPL/WkicKTPjhs8rSn
yzPHAW/u92B6EdtiA+175pG/MB0jpavA7khWODi24B8BC4HX3lFxPk0pzuQK2Bd/PJGcRds8TbL3
8MlfeNIbLLSXtsvfH4sC0i+bTPVffsOdLkTBvPMraUxKocPXyS2A3kSMwAgkBDpLK2/W43zYIbIF
qsXO/I0kvX0mSbtv81mwHyg86UO7Q16LXSJHpw7/6RYeRw/m5sDnY+gEL1xo7wQRIzCfXB8rPksL
ww7Egt0n53kTsC92tJGUt843nSGHJzUc0Fb0qbx2mzXv1Ru2aPKOMS2KNWwiW2eIGIGBv7O0sLAe
K0i805UErMV+8IQnBTZ/8Cb/2EfB1XoF1qJiShtzpeb3dURECSw6Swv9HWavOjq8pUOP2htW0PDI
MZ4W2v1R/g1Jm9logg21hPYAKD1vvYgogflZWl67FI/8dTZVvhvc7BDGxcHWYvfqSZGtQ7ZfS9IR
YxyuahiBG+x2ymlqob11jdTgZ7zrfZYWr72sWQs21OfxqFlzKLIrwfAGsWWRrUNq0mWP3AB7lLtV
4DbWDJa0WOlAfRMlHW+iklYrVdvaaB8TuUkwYeF9lhZi1DiayTtfIIKtxXz1pOs861CQvrgl6AX3
Nkc75Vta6ShDZO8M3SJwo93Ba2tCbQMdbrRQrdey01qbnYtsYfmU6SDzxtv4WVoIesCr1hqoD6YW
wxvv9KLB+lwexxaGQl1AWIiK75rLysOq19IkBV0msI3V1iJWWxOPN1Iyq62lrLba28V5QaW1jfaz
L97q9aX5WVor36acO++j6o3/Vtn8gdagkt0IuHFwM+Wt/5ByliznNxpuohb2P1pZYeOXQ5E/qPBk
EEhf3ytcA4bvdKQZ3VEDFTCBUTYeO1aLHN0ib7pTvCdUwi4wCjSzyUIJ7Mvksi/VqOG3GUpdN4Sy
AOBF49daMiZMCep3HtAN7GP/Gy0FONjQTGns9Tc79lBSVR0dYJ+P7gE3E64xi+XJZk5bGeuDRZ+n
BZwfwqcTrXKcGcLmsrKAn4Gai2vz5Gc1HUdR8JAnVoyU6rPWOiwCo6kpbGnlhYYCRG11eOUPlmL2
XvTR7sJAdAnXVLOZjTW98orIYgWKWuKdzsfFcxao0vA/yllN37tzD31TVUuZTGz4BO6aHRL7H6fW
1EW8CcYNlN/sJSxWhiBMiv1aX9zKd2Kq3t9JdBX4/iefokONzfwORRPULOhDQwECpTKR3TcJFpoH
s2qkhomD2ukQCORvXIyxKMKTNnaT4sZMZzUeTWkO+z51EFuRtyNaWLOfU19He0vz6FhDtbpLqskg
KeEBecoS22qbOvbsQ0FXgee/tpQqWm3CAu0saN5R2PjsYAIC7qYZwy+RHYhqsSg8CWcI3n4a+//c
IWLXcjyA2BZ2c2SzlgN5C9n7HKn/kNdkw16ewLfIYisNn7UK4ITpga4Cax0maQWFdog1m8HUIn9N
sxKRR91ReBL+AJpx3GxocjEaOO7yBeCsucfy6GM9NRa7KtAEb75YDmViM5xO21k7IqIEhrAHWReQ
zQpRZHcTqGn2RjUuZvm1hCdbWBdUzGpoMus+UFv3M38DY3m76P8iPq1lJadORJTAAKKhqcxjzaTI
HkzTrERZixEZw6Z1Ub6OwDArHF1TZ4k4gYGdgVqDZhCOD2oNavUBVmu/rq3XHBFy12KMr0MOTxqU
iBQYoKbCs0ZfiNqMfhFj7FBqkdujxj6ozoQnjUjECqw38KgP/H5458KTBsQU2AVqMZb9iGyRjCmw
gkg5nlALpsAGITExkcaPH0/Lly8ni2tdtx6YAhuIuLg46tmzJ8XGxtK0adNox44dnYuDM3QVeOjQ
obRy5UqTEJkwYQJFRUV56NWrF51//vk0Y8YMysoKYvmtAN0EXrNmDY0ZM8akE0RHR6sEBqjRw4YN
o4cfdsWyNaKbwCadZ+rUqR5R9WqmTYENQmFhIQ0YMIDi4+Np2bJlujlapsAGoby8nMrKQv29KP+Y
Ap/imAKf4gQnsOAn2U0iA2jnpefJqMR+g23KROxAD2YFo4mxgGb4bQmlltAWNThdlcjAPKkpcuQA
rQqffVGloYvUqKTomGcEBn4GFqq8ifHxqbluogfPi0ro378Pe9HoYzSJcGIboG0UHvv7xlzF2usf
xBlNIpDvk/oOvpKL635wkc2afAoQ2wAtXbKqH6jSrj45LbFvjNX3zSbGhGuVhj7X0yzzR1TU/wOA
lBvmG7VkMAAAAABJRU5ErkJggg==",
								extent={{-200,-200},{200,200}})}),
		experiment(
			StopTime=1,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.002,
			__esi_MinInterval="9.999999999999999e-10",
			__esi_MaxInterval="0.001",
			__esi_AbsTolerance="1e-6"));
end BuildingLoads_Experiment;
