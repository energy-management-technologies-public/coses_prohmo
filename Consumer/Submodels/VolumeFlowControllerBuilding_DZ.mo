﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo.Consumer.Submodels;
model VolumeFlowControllerBuilding_DZ "Volume flow controller for zone internal heating system"
	import GreenCity.GreenBuilding.Building.Control.*;
	import GreenCity.GreenBuilding.Building.*;
	import GreenCity.GreenBuilding.*;
	import GreenCity.*;
	input Modelica.Blocks.Interfaces.RealInput TRef(
		quantity="Thermics.Temp",
		displayUnit="°C") "Reference temperature" annotation(
		Placement(
			transformation(extent={{112,-129},{152,-89}}),
			iconTransformation(extent={{-170,80},{-130,120}})),
		Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
	input Modelica.Blocks.Interfaces.RealInput TAct(
		quantity="Thermics.Temp",
		displayUnit="°C") "Actual temperature" annotation(
		Placement(
			transformation(
				origin={246,-195},
				extent={{-20,-20},{20,20}},
				rotation=90),
			iconTransformation(
				origin={-150,-100},
				extent={{-20,-20},{20,20}})),
		Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
	input Modelica.Blocks.Interfaces.RealInput TFlow(
		quantity="Thermics.Temp",
		displayUnit="°C") "Flow temperature" annotation(
		Placement(
			transformation(
				origin={235,-55},
				extent={{-20,-20},{20,20}},
				rotation=-90),
			iconTransformation(
				origin={-50,-150},
				extent={{-20,20},{20,-20}},
				rotation=90)),
		Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
	input Modelica.Blocks.Interfaces.RealInput TReturn(
		quantity="Thermics.Temp",
		displayUnit="°C") "Return temperature" annotation(
		Placement(
			transformation(
				origin={330,-55},
				extent={{-20,-20},{20,20}},
				rotation=-90),
			iconTransformation(
				origin={50,-150},
				extent={{-20,-20},{20,20}},
				rotation=90)),
		Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
	output Modelica.Blocks.Interfaces.RealOutput qvRef(
		quantity="Thermics.VolumeFlow",
		displayUnit="m³/h") "Reference volume flow" annotation(
		Placement(
			transformation(extent={{547,-113},{567,-93}}),
			iconTransformation(extent={{140,-10},{160,10}})),
		Dialog(
			group="Volume Flow",
			tab="Results",
			visible=false));
	protected
		Boolean ON "Switch-ON/OFF for heat controller" annotation(Dialog(
			group="Volume Flow",
			tab="Results"));
	public
		parameter Boolean ContinousControl=true "If true, the control is continuous, depending on TAct, if false, the control is switched off, if TAct > TRef + deltaTup" annotation(Dialog(tab="Parameters"));
		parameter Real QHeatMax(
			quantity="Basics.Power",
			displayUnit="kW")=5000 "Maximum heating power" annotation(Dialog(
			group="Heat Power",
			tab="Parameters"));
		parameter Real deltaTup(
			quantity="Thermodynamics.TempDiff",
			displayUnit="K")=0.2 "Upper temperature difference (heat power = 0)" annotation(Dialog(
			group="Temperature Difference",
			tab="Parameters"));
		parameter Real deltaTlow(
			quantity="Thermodynamics.TempDiff",
			displayUnit="K")=-0.2 "Lower temperature difference (heat power = max)" annotation(Dialog(
			group="Temperature Difference",
			tab="Parameters"));
		parameter Real qvMax(
			quantity="Thermics.VolumeFlow",
			displayUnit="m³/h")=0.00055555555555555556 "Maximum volume flow for heating system" annotation(Dialog(
			group="Volume Flow",
			tab="Parameters"));
		parameter Real cpMed(
			quantity="Thermics.SpecHeatCapacity",
			displayUnit="kJ/(kg·K)")=4177 "Specific heat capacity of heating medium" annotation(Dialog(
			group="Medium",
			tab="Parameters"));
		parameter Real rhoMed(
			quantity="Thermics.Density",
			displayUnit="kg/m³")=1000 "Densitiy of heating medium" annotation(Dialog(
			group="Medium",
			tab="Parameters"));
	initial equation
		assert(deltaTup>deltaTlow,"Upper temperature limit must be higher than lower one");
	initial algorithm
		if (TAct>(TRef+deltaTup)) then
			ON:=false;
		else
			ON:=true;
		end if;
	algorithm
		if ContinousControl then
			when (TAct>(TRef+deltaTup)) then
				ON:=false;		
			elsewhen (TAct<TRef) then
				ON:=true;
			end when;
		else
			when (TAct>(TRef+deltaTup)) then
				ON:=false;		
			elsewhen (TAct<(TRef+deltaTlow)) then
				ON:=true;
			end when;
		end if;
	equation
		if ON then
			qvRef=min(min(max(((TRef+deltaTup-TAct)*qvMax/(deltaTup-deltaTlow)),0),qvMax), QHeatMax/(cpMed*rhoMed*max((TFlow-TReturn),0.1)));
		else
			qvRef=0;
		end if;
	annotation(
		viewinfo[0](
			minOrder=0.5,
			maxOrder=12,
			mode=0,
			minStep=0.01,
			maxStep=0.1,
			relTol=1e-05,
			oversampling=4,
			anaAlgorithm=0,
			bPerMinStates=true,
			bPerScaleRows=true,
			bPerScaleCols=true,
			typename="AnaStatInfo"),
		Icon(
			coordinateSystem(extent={{-150,-150},{150,150}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAG4AAABuCAYAAADGWyb7AAAABGdBTUEAALGPC/xhBQAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAjbSURBVHhe7ZxLiB1FFIbzVkyMUREXBhyU
gBIDg4IogmSny2xElxHBrSFLV1FxJRiELIQoQXDhA8xGUBAcRER0E10EN0KEZCG4iI+Fgou2v04f
c06lzkzd6rrTPffWBz8z0/fv6ur6u6qrH3e2VSqVSiWH1VZrrZqqSYgsyGRdMMRWrhpf64ZXe9p0
RTYusRWqpiOXmLk5fvx4s7a21ly6dKmpzAfaljamrWMZ9HK5wXz+/Pm+6MpmQZvHsmjlYoykXxkH
p+e5GCNdtzIOtH2YRysXY7x69WpfTGWz4ZwX5tHKxRjnDQfGiRMnmlOnTvVLKpowj1YuxjhPLly4
0KysrPy/raNHj9ZZa4DOopeLMc4Lehjl79+2vTm1c2/zws6bu78PHDhQZ7EKnUUvF2MsDT2KnkXZ
h7fvaj7ffVtzec+dnT7avb8Lks8YPuv5dSLB0ZPoUZRLD5PAtC7uuaN5aseezrO6utoNp8uMzqKX
izGWgJ4j1yQHt+/oelYsNC2GT6nD6dOn+5KWD2kDJRdjHArXIjIBoSfRo2JBxcQwynDKugyvW2no
5JTAefzcuXP9kjx0Fr1cjHEIegLy5q590XA2EkHrictWuCHAAUZdGer5OeRSR2fRy8UYc+Boo9Ks
T4/5dvft0VBm0Tu7bjUTlylDL2P/gfM6I04uOoteLsY4K1RaJiAnd94SDSFX9L7Hd+zuyp7yxIUe
Jr2M3pfTjoLkoORijKlQwWPHjnXrpE5AcsUBwXY4QKY4ceF8rIfHWdoxRHJQcjHGFIZMQHLFxIUD
hG1ywExp4kJw+iYCdcwdHSQHJRdjTIFzTrjeZmtKkxaC0/WhfpMMjmGBHkeFxxD1nFpwW2Ko1Cfj
MaCeNbjAmEINzhK2R2o7xtBZ9HIxxhRqcJalDY4QwnqIYgF5y0NyLx1mfcy01MFxnghhWU5wXCrI
Te4c2C4X/KmXHLo9mE3mbhckByUXY0whDI4dpCFTFIPlpYLTt99S9yeE7bIuF/wp03ragnW4TJJt
5yL1VnIxxhTC4GhEdpLKryev/FLBST30/pTQRnf8aQtuDnCH55kdN033XmUsuFjDh3jllwiO85ne
j9Ja731T2oJ7qtzh4dZfSlt4RLbtYowpzCM4jlIpV8Sy1ODw6/0oLXpyrC7AtpcyOHkIGROfhVBO
rBH1k4qS2ujJBPVcyuBmhXK8o58GlhvgSOo6i/T6G93Qxo+Pc5vcDOc5Iu0RO+g2Qrar5GKMKcgO
ClMKDmho6pO7PVlX72MMPiek8JEWT0tkkpJ6SSGw3UAuxpjC1IMTcp+c08s2uginN1EP/bphKIbP
Wb9EIzkouRhjClsluHnC/vMsMhaYiJ7IOXcWdBa9XIwxhRrctV6Z8qrGrPssOSi5GGMKNbhr32Wr
wc0I5YwdHBf8vNEWC0vEm2pMUGZBZ9HLxRhTqMFdm7ly/vLeH2VmyW0w3U4p6Cx6uRhjClSGoCRA
uROfIllniChn7OCAmSd1YcjUL0wxKaE3phzMIbqterkYYwrSeGNqCsEB9ZAnAkz/ua6jJ9JGOYT7
2crFGFPgGoYKj6lZL2znjW6TIYR5tHIxxsq4hHm0cjHGyriEebRyMcbKuIR5tHIxxsq4hHm0cjHG
yriEebRyMcbKuIR5tHIxxsq4hHm0cjHGyriEebRyMcbKuIR5tHIxxsq4hHm0cjHGyriEebRyMcah
cK9O7uKHbznx+px8FiLLN3preNEJ82jlYoxDofGlrPCGK485vO3I8pxHIYuEtIOSizEOpQY3DGkH
JRdjHEoNbhjSDkouxjiUGtwwpB2UXIxxKDW4YUg7KLkYYw68r09ISL9/wptQshzpLxzq5UiW49HL
lw1pByUXY8xB96TSWjYibeBijDnU4MoRaQMXY8xBguPlT85xQ7XeuXDRkf1WcjFGD85jNGhM8mVC
fi8B4Ul9wm2JUr5UvxWR/VZyMUYPPYHwRIOWQAfnaVEnLpF9dTFGj9rjrsNBk/Nt0xRkv5VcjDEH
GpJ1+VkCHdyU4CVc/TXjedwQl7KVXIwxh2UJLhzCZ/0mTgq6/F4uxphDDa4cuvxeLsaYw7IEtyWH
yjqrvM6WmpzU4DaHyL66GKMHRxgNGpMMH/MILtyWaF5HvEb/03DvfCZfbhQN/fqXLquXizHmsKjn
OK4VpR4odu2on4bwnxiGorfXy8UYc1jU4EBuLqDYf5wtPVmRspRcjDGHRQ5O96hw/8IeWeJbsrq8
Xi7G6EGlmBDEJA9I5xFcbHuoRCOlsN45TNezxDAJelu9XIzRg8YKvaHmEZwn6rNZ6OFS/48v/US/
1DWdlKfkYoweyxycHi75Heh5uj6lRgBdZi8XY/RY1qES9HAplwX0MFlWapgEKVPJxRhzILCwnFKa
Cnq4ZFKie2HJW19SppKLMeawDMGFQenLgJK9X8pUcjHGHLjDQHhI7xBDqCxH+qjVy5Esx6OXTwU9
XOr6lhwmQcpVcjHGoYTnJ43e4RBZPqWwNOFkRKRnmSWIbMPFGIeyqMEBvUvqiRgdSqPL7+VijENZ
5OD0TBLJpUFJdPm9XIxxKIscXA5//n25ufL7t0Ys85B2UHIxxqHU4Cw/XHm3OfPVvVGd//HZ7vN/
/v2jd9fgJsNvf12MhiZ6/pMHmrPfHOkCBGkHJRdjrJTnve+eiIaGnnzvSHPsg8Pd71///IrJopeL
MVbKQyBvfLnSPPX+Q82Rs6vNcx8/2Lz2xX1dWCc/PdQtkyDDPFq5GGOlPHq4JDCC2vvmo12Q8ncN
bqIwXDIkEtLBM4905zZ+Hnr74W64HBzcZryEs4z89OvH3bBIYAybhMRPgqPn8ferH95jsujlYozh
TLBSjtgkRcLj95feuttk0cvFGOdxN6ByDS6+w+CQ9LjHnt5nsujlcoO59I3TynWYYYbBvfzZ/c2L
r991Qw69oqy0ipm7nsewWc955fn+l9NdYJzTGB6dniZyiZmrpiOXtVaxFarGF9m4rLaKrVQ1vshm
XTDUnjcdkcWGoVUqlUolYNu2/wBPYLbyu8f1wQAAAABJRU5ErkJggg==",
								extent={{-150,150},{150,-150}})}),
		experiment(
			StopTime=1,
			StartTime=0,
			Interval=0.002,
			__esi_MaxInterval="0.002"));
end VolumeFlowControllerBuilding_DZ;
