﻿// CP: 65001
// SimulationX Version: 4.2.3.69223
within CoSES_ProHMo.Consumer.Submodels;
model CoolingSystem "Cooling System, can be used with SimpleHeatedBuilding"
	import GreenCity.GreenBuilding.AirConditioning.*;
	import GreenCity.GreenBuilding.*;
	import GreenCity.*;
	protected
		function CalcPower "CalcPower"
			input Real alphaHeat;
			input Real alphaCool;
			input Real kappa;
			input Real TRoom;
			input Real TWater;
			output Real QHeatCool;
			algorithm
				if (TWater>=TRoom) then
					QHeatCool:=((1/kappa)+(1/alphaHeat))^(-1)*(TWater-TRoom);
				else
					QHeatCool:=((1/kappa)+(1/alphaCool))^(-1)*(TWater-TRoom);
				end if;
		end CalcPower;
	public
		GreenCity.Interfaces.Thermal.VolumeFlowIn Flow "Flow pipe" annotation(Placement(
			transformation(extent={{40,10},{60,30}}),
			iconTransformation(extent={{-210,90},{-190,110}})));
		GreenCity.Interfaces.Thermal.VolumeFlowOut Return "Return pipe" annotation(Placement(
			transformation(extent={{40,-25},{60,-5}}),
			iconTransformation(extent={{-210,-110},{-190,-90}})));
	protected
		Real aCool(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=if useAsWallPanels then 1.6*abs(TCoolNorm-TZoneNorm)^0.3+5.1 else alphaCool*((QCoolingNorm/alphaCool)^(1/1.1))^0.1 "Cooling heat transmission factor" annotation(Dialog(
			group="Heat Transmission",
			tab="Results"));
		Real aHeat(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=if useAsWallPanels then 1.6*abs(THeatNorm-TZoneNorm)^0.3+5.1 else alphaHeat "Heating transmission factor" annotation(Dialog(
			group="Heat Transmission",
			tab="Results"));
		Real kappa(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=(((TZoneNorm-TCoolNorm)/QCoolingNorm)-(1/aCool))^(-1) "Heat transition coefficient" annotation(Dialog(
			group="Heat Transmission",
			tab="Results"));
	public
		input Modelica.Blocks.Interfaces.RealInput TZone(
			quantity="Thermics.Temp",
			displayUnit="°C") "Temperature of heated zone" annotation(
			Placement(
				transformation(
					origin={245,50},
					extent={{-20,20},{20,-20}},
					rotation=-180),
				iconTransformation(
					origin={0,-200},
					extent={{-20,-20},{20,20}},
					rotation=90)),
			Dialog(
				group="Temperature",
				tab="Results",
				visible=false));
		Real TFlow(
			quantity="Thermics.Temp",
			displayUnit="°C") "Flow temperature of heating system" annotation(Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
		Real TReturn(
			quantity="Thermics.Temp",
			displayUnit="°C") "Return temperature of heating system" annotation(Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
	protected
		Real QCoolingNorm(
			quantity="Basics.Power",
			displayUnit="kW")=if defineQCool then QCoolNorm else QHeatNorm/0.7*deltaTSubnormal/deltaTOverHeating "Calculated nominal cooling power" annotation(Dialog(
			group="Heating Power",
			tab="Results"));
	public
		output Modelica.Blocks.Interfaces.RealOutput QHeatCool(
			quantity="Basics.Power",
			displayUnit="kW") "Heat power of heating system" annotation(
			Placement(
				transformation(extent={{230,-15},{250,5}}),
				iconTransformation(extent={{186.7,40},{206.7,60}})),
			Dialog(
				group="Heating Power",
				tab="Results",
				visible=false));
		GreenCity.Utilities.Thermal.MeasureThermal measureThermal2 annotation(Placement(transformation(
			origin={120,-15},
			extent={{-10,-10},{10,10}},
			rotation=-180)));
		Modelica.Blocks.Sources.RealExpression realExpression1(y=TRefCooling) annotation(Placement(transformation(extent={{210,60},{190,80}})));
		Real ECool(
			quantity="Basics.Energy",
			displayUnit="kWh") "Cold output of heating system" annotation(Dialog(
			group="Energy",
			tab="Results",
			visible=false));
		output Modelica.Blocks.Interfaces.RealOutput qvRef(quantity="Thermics.VolumeFlow") "Reference volume flow" annotation(
			Placement(
				transformation(extent={{50,50},{70,70}}),
				iconTransformation(
					origin={-50,200},
					extent={{-10,-10},{10,10}},
					rotation=90)),
			Dialog(
				group="Volume Flow",
				tab="Results",
				visible=false));
		Real qv(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow of heating system" annotation(Dialog(
			group="Volume Flow",
			tab="Results",
			visible=false));
		parameter Real ALH(quantity="Geometry.Area")=300 "Heated (living) area" annotation(Dialog(tab="Parameters I"));
		parameter Integer nFloors=2 "Number of Floors" annotation(Dialog(tab="Parameters I"));
		parameter Real QHeatNormLivingAreaVar(quantity="Thermics.HeatFlowSurf")=15 "Area specific heating power, 15 W/m² for space heating" annotation(Dialog(tab="Parameters I"));
		parameter Boolean defineQCool=false "If enabled, nominal cooling power is used for model definition, else heating power is used" annotation(Dialog(
			group="Parameter Configuration",
			tab="Parameters I"));
		parameter Boolean useAsWallPanels=false "If enabled, calculation methods for wall-mounted systems are used, else system refers to ceiling systems" annotation(Dialog(
			group="Parameter Configuration",
			tab="Parameters I"));
		parameter Real TReturnInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=294.15 "Initial return temperature of heating system" annotation(Dialog(
			group="Initial Temperatures",
			tab="Parameters I"));
		parameter Real TRefCooling(quantity="Basics.Temp")=298.15 "Reference Cooling Temperature" annotation(Dialog(
			group="Initial Temperatures",
			tab="Parameters I"));
		parameter Real TRef(quantity="Basics.Temp")=294.15 "Reference heating temperature" annotation(Dialog(
			group="Initial Temperatures",
			tab="Parameters I"));
		parameter Real cpMed(
			quantity="Thermodynamics.SpecHeatCapacity",
			displayUnit="kJ/(kg·K)")=4177 "Specific heat capacity of heating medium" annotation(Dialog(
			group="Heating Medium",
			tab="Parameters I"));
		parameter Real rhoMed(
			quantity="Basics.Density",
			displayUnit="kg/m³")=1000 "Density of heating medium" annotation(Dialog(
			group="Heating Medium",
			tab="Parameters I"));
		parameter Real APanel(
			quantity="Geometry.Area",
			displayUnit="m²")=0.7*ALH "Panel surface area" annotation(Dialog(
			group="Panal Characteristics",
			tab="Parameters I"));
		parameter Integer nPanel=1 "Number of panels" annotation(Dialog(
			group="Panal Characteristics",
			tab="Parameters I"));
		parameter Real VHeatCoolMedium(
			quantity="Geometry.Volume",
			displayUnit="l")=0.001 "Heating medium volume of each panel" annotation(Dialog(
			group="Panal Characteristics",
			tab="Parameters I"));
		parameter Real QHeatNorm(
			quantity="Basics.Power",
			displayUnit="Nm/s")=QHeatNormLivingAreaVar*nFloors if not defineQCool "Normal installed heating power per m2 and panel" annotation(Dialog(
			group="Heating",
			tab="Parameters II"));
		parameter Real alphaHeat(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=6.7 "Heat transmission coefficient for heating (init: ceiling)" annotation(Dialog(
			group="Heating",
			tab="Parameters II"));
		parameter Real THeatNorm(
			quantity="Thermics.Temp",
			displayUnit="°C")=308.14999999999998 "Normal flow temperature of heating system" annotation(Dialog(
			group="Heating",
			tab="Parameters II"));
		parameter Real deltaTSubnormal(
			quantity="Thermics.TempDiff",
			displayUnit="K")=8 if not defineQCool "Subnormal temperature of cooling panel" annotation(Dialog(
			group="Heating",
			tab="Parameters II"));
		parameter Real deltaTOverHeating(
			quantity="Thermics.TempDiff",
			displayUnit="K")=16.7 if not defineQCool "Heating over temperature of cooling panel" annotation(Dialog(
			group="Heating",
			tab="Parameters II"));
		parameter Real QCoolNorm(
			quantity="Basics.Power",
			displayUnit="W")=47.32 if defineQCool "Normal installed cooling power per m2 and panel" annotation(Dialog(
			group="Cooling",
			tab="Parameters II"));
		parameter Real alphaCool(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=8.92 "Heat transmission coefficient for cooling (init: ceiling - equals floor heating)" annotation(Dialog(
			group="Cooling",
			tab="Parameters II"));
		parameter Real TCoolNorm(
			quantity="Thermics.Temp",
			displayUnit="°C")=285.9278 "Normal return temperature of heating system" annotation(Dialog(
			group="Cooling",
			tab="Parameters II"));
		parameter Real TZoneNorm(
			quantity="Thermics.Temp",
			displayUnit="°C")=294.2611 "Normal zone temperature for heating system" annotation(Dialog(
			group="Zone",
			tab="Parameters II"));
		GreenCity.Interfaces.Thermal.DefineVolumeFlow ToReturn "Specifies a Thermal Volume Flow Connector" annotation(Placement(transformation(extent={{175,-5},{155,-25}})));
		GreenCity.Interfaces.Thermal.ExtractVolumeFlow FromFlow "Extracts the characteristics of a Thermal Volume Flow Connector" annotation(Placement(transformation(extent={{75,30},{95,10}})));
		VolumeFlowControllerBuildingHC_DZ volumeFlowControllerBuildingHC_DZ1 annotation(Placement(transformation(extent={{130,45},{100,75}})));
	initial equation
		TReturn=TReturnInit;
		TFlow=FromFlow.TMedium;
		qv=FromFlow.qvMedium;
		ECool=0;
	equation
		TFlow=FromFlow.TMedium;
		qv=FromFlow.qvMedium;
		if volumeFlowControllerBuilding_DZ1.qvRef > 0 then
			cpMed*rhoMed*VHeatCoolMedium*nPanel*der(TReturn)=-CalcPower(aHeat,aCool,kappa,TZone,TReturn)*APanel*nPanel+cpMed*rhoMed*qv*(TFlow-TReturn)+cpMed*rhoMed*VHeatCoolMedium*nPanel*der(TZone);
			QHeatCool=CalcPower(aHeat,aCool,kappa,TZone,TReturn)*APanel*nPanel;
		else
			der(TReturn) = 0;
			QHeatCool = 0;
		end if;
		ToReturn.TMedium=TReturn;
		
		der(ECool)=min(QHeatCool,0);
	equation
		connect(measureThermal2.PipeOut,Return) annotation(Line(
			points={{110,-15},{105,-15},{55,-15},{50,-15}},
			color={190,30,45}));
		connect(FromFlow.Pipe,Flow) annotation(Line(
			points={{75,20},{70,20},{55,20},{50,20}},
			color={190,30,45},
			thickness=0.0625));
		connect(ToReturn.Pipe,measureThermal2.PipeIn) annotation(Line(
			points={{155,-15},{150,-15},{135,-15},{130,-15}},
			color={190,30,45}));
		connect(FromFlow.qvMedium,ToReturn.qvMedium) annotation(Line(
			points={{95,15},{100,15},{180,15},{180,-10},{175,-10}},
			color={0,0,127},
			thickness=0.0625));
		connect(TZone,volumeFlowControllerBuildingHC_DZ1.TAct) annotation(Line(
			points={{245,50},{240,50},{135,50},{130,50}},
			color={0,0,127},
			thickness=0.0625));
		connect(realExpression1.y,volumeFlowControllerBuildingHC_DZ1.TRef) annotation(Line(
			points={{189,70},{184,70},{135,70},{130,70}},
			color={0,0,127},
			thickness=0.0625));
		connect(FromFlow.TMedium,volumeFlowControllerBuildingHC_DZ1.TFlow) annotation(Line(
			points={{95,25},{100,25},{125,25},{125,40.3},{125,45.3}},
			color={0,0,127},
			thickness=0.0625));
		connect(measureThermal2.TMedium,volumeFlowControllerBuildingHC_DZ1.TReturn) annotation(Line(
			points={{120,-5},{120,0},{120,40.3},{120,45.3}},
			color={0,0,127},
			thickness=0.0625));
		connect(volumeFlowControllerBuildingHC_DZ1.qvRef,qvRef) annotation(Line(
			points={{100,60},{95,60},{65,60},{60,60}},
			color={0,0,127},
			thickness=0.0625));
	annotation(
		statAnaRefVar(flags=2),
		__esi_protocols(transient={
								Protocol(var=time),
								Protocol(var=statAnaRefVar)}),
		__esi_solverOptions(
			solver="CVODE",
			typename="ExternalCVODEOptionData"),
		Icon(
			coordinateSystem(extent={{-200,-200},{200,200}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAG4AAABuCAYAAADGWyb7AAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAOvAAADrwBlbxySQAABhxJREFUeF7tmD2IXVUQx+1ttROMkYiii6KYRkhKtdtKTGlhYZFGSWMn
NoKgCEEMIUrAIqAEhBgxSkBkMX4tQtxgISLZdCskcYsVUlz5X94s886bN/fed+Z83Jv5wZCXd86Z
e2Z+zHsvuadxRomLGykubqQsiNu9utVsrh9rLt9/0KOCgAs4CZkThw3SYY/yEcqbE+eTVm/ADWdO
nHTAo57g9BJ37fiJ5ubGlWbv+vZsp2MNeoseo9eSAwSnU9zOxUuzVScX6LnkgqOKg32nDNLkcVRx
GF2nDOh96IOjirtz6/ZsxckNvvNCHxxVnFMWzYeLqxjNh4urGM2Hi6sYzYeLqxjNh4urGM2Hi6sY
zYeLqxjNh4urGM2Hi6sYzYeLqxjNh4urGM2Hi6sYzUdSccjx85HNpRG7juizB3H0hV/E98Ow2rds
fQiojQcnubguuvZY5CDQzD702de1R1ofjThctCtP7Droe1dLcWCovEmJA117LHIQlvImLQ70ydW1
J3adsBQHhshzcUvoe2dLeZMWB/rk69pjkQNYigN95U1WHOjaF7tOWMqbtDjQJ2fXHoscwFIc6CNv
VOIokI//XQttL91NWhsa1Myu0PbRWleuUYnjWOazyoVmImKxyCHBXYQ1ZxMHrHJa3s2q6SnkcRdh
zdnFWeS1vl+tk0d1SvVmFQesclvfz0qcpTzqldSz7OKAVX7LO1o23SoX9UnqVxFxwOIZ1ve0nBYL
eVSfVGcxccDiGZb3tBQHJi0u9jmWd3VxA7B4jsV90WQXNwCrZ62ag4RZSwOTFgcsnjU0R0phhIvr
wZAcOYRZPIO7COtbEEf/KRpuTInFs/rmSCnNShiBmnhw5sSRNIpc9G36MqTCJHJIswQ1kYvqxOWS
BlJJAylyaz6KihvSdImh58c0bUDzUUzcVKSBVLk1H0XE5ZYGxjZtQPORVVysMLBqjpTiUqH5yCau
pDTg4lYkVhqIyeHiVqTktAE0OEWTJy0utukg9jwY29RpPrKJi8VKXKqpS5FX83FXiQOppsPFLQE5
7rap03yMRhywlJcCa3maj9H8OCEscqWYDsIyt+ZjThweSJusC7MSB8YgzwLNx4I4HpZYNDvEQl4K
rPpHeaR82cSBVPJWzZmqTmCRl+4n3TOrOJBK3qqkqhPE5uYuwlzZxQFrcWCKU0d3k+5YTFyKqYuR
l4LYPtJ5KU8RccBaHIgRl6Le2Lx0XspTTBxIMXWrYl2vRQ8ph5TLxc2wrNeqf5RHyldcnJW82FxW
9Vr2jnJJOYuKA5biYrCq17Jv3EWYdxLiYqcNWNRr3TfKJ+WdjLghSPVZ1GvdM7onBaeION5oqelD
RKwqPqxP+vvQHlj3jO4g3aWYOGp42Hi+xpHeA8v2LtvPoRqlevvUH56TzvTJswzKT8Ep9lFJzeUN
1hred7/0noZU65D6+d6YPBJ0XspT9DuON7lvw8MznD7nJXitq9ROZ2LzhPC8Ya4qfpxwGX0Izww9
H2JRM88Rm4vQ8hUXF0MosDTWfeMuwryjFjd1NB8urmI0Hy6uYjQfLq5iNB8urmI0Hy6uYjQfLq5i
NB8urmI0Hy6uYjQfLi4j/9253fx240zz09/vt3/euPVDG3hfQvPh4jLy7952c/K7B8U4++Nzzbd/
vNGKJDQfLi4zECSJQ7xy/tHm3csHmi9/f7WdQs2Hi8sMpkqShnjti0eatdNPta/P/fqi6sPFZeav
f75uxayfe7w59NHTzZGza83rFw7ty7v3vcP7rzUfLq4ApzfWWjH4WHz+0ydaWZi0N796uJXp4ioF
vyoxcZB13wfPthN3+OMn29f0URklbu/69mzFsQS/Lt/+5mArDFNHol7+7LHmgZPPtK9PfR4h7ubG
ldmKY833f761L4wHJg8fmZ98eGDBB0cVd+34idmKYw1+7kv/NMAUIs6/9NCCD44qDrFz8dJs1bFm
Z3dr/4cKBT46T72zKA3B2ReH7zNpMwKTh49N/86zB/IwefhOw8ejNGkUnM6J86gnOHPiNtePiQc8
ygfccObE7V7dEg95lA+44cyJA9jgk1dPwEUoDSyIc8aBixspLm6UNM3/+2HoyJxxWk0AAAAASUVO
RK5CYII=",
								extent={{-203,200},{203,-200}})}),
		experiment(
			StopTime=1,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.002,
			__esi_SolverOptions(
				solver="CVODE",
				typename="ExternalCVODEOptionData"),
			__esi_MinInterval="9.999999999999999e-10",
			__esi_MaxInterval="0.002",
			__esi_AbsTolerance="1e-6"));
end CoolingSystem;
