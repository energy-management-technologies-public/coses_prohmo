﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo.Consumer.Submodels;
model HeatingAndCoolingSystem "Heating and Cooling System"
	import GreenCity.GreenBuilding.AirConditioning.*;
	import GreenCity.GreenBuilding.*;
	import GreenCity.*;
	GreenCity.Interfaces.Thermal.VolumeFlowIn Flow "Flow pipe" annotation(Placement(
		transformation(extent={{-95,-20},{-75,0}}),
		iconTransformation(extent={{-210,90},{-190,110}})));
	GreenCity.Interfaces.Thermal.VolumeFlowOut Return "Return pipe" annotation(Placement(
		transformation(extent={{-95,-55},{-75,-35}}),
		iconTransformation(extent={{-210,-110},{-190,-90}})));
	Modelica.Blocks.Interfaces.BooleanOutput Heating "Heating Mode" annotation(
		Placement(
			transformation(
				origin={-90,25},
				extent={{-10,10},{10,-10}},
				rotation=-180),
			iconTransformation(
				origin={-150,-196.7},
				extent={{-10,-10},{10,10}},
				rotation=270)),
		Dialog(
			tab="Results",
			visible=false));
	Modelica.Blocks.Interfaces.BooleanOutput Cooling "Cooling Mode" annotation(
		Placement(
			transformation(extent={{-80,0},{-100,20}}),
			iconTransformation(
				origin={-100,-197},
				extent={{-10,-9.699999999999999},{10,10.3}},
				rotation=270)),
		Dialog(
			tab="Results",
			visible=false));
	input Modelica.Blocks.Interfaces.RealInput TRef_Heating(
		quantity="Thermics.Temp",
		displayUnit="°C") "Reference Temperature" annotation(
		Placement(
			transformation(
				origin={80,50},
				extent={{-20,20},{20,-20}},
				rotation=-180),
			iconTransformation(
				origin={50,-196.7},
				extent={{-20,-20},{20,20}},
				rotation=90)),
		Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
	input Modelica.Blocks.Interfaces.RealInput TRef_Cooling(
		quantity="Thermics.Temp",
		displayUnit="°C") "Reference Temperature" annotation(
		Placement(
			transformation(
				origin={80,85},
				extent={{-20,20},{20,-20}},
				rotation=-180),
			iconTransformation(
				origin={0,-196.7},
				extent={{-20,-20},{20,20}},
				rotation=90)),
		Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
	input Modelica.Blocks.Interfaces.RealInput TZone(
		quantity="Thermics.Temp",
		displayUnit="°C") "Temperature of heated zone" annotation(
		Placement(
			transformation(
				origin={80,20},
				extent={{-20,20},{20,-20}},
				rotation=-180),
			iconTransformation(
				origin={150,-196},
				extent={{-20,-20},{20,20}},
				rotation=90)),
		Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
	protected
		function CalcPower "CalcPower"
			input Real alphaHeat;
			input Real alphaCool;
			input Real kappa;
			input Real TRoom;
			input Real TWater;
			output Real QHeatCool;
			algorithm
				if (TWater>=TRoom) then
					QHeatCool:=((1/kappa)+(1/alphaHeat))^(-1)*(TWater-TRoom);
				else
					QHeatCool:=((1/kappa)+(1/alphaCool))^(-1)*(TWater-TRoom);
				end if;
			annotation(viewinfo[0](
				recentpage=7,
				typename="ClassSymbolInfo"));
		end CalcPower;
		Curve2D TOverLog(
			f=":928:
789CC4937D50D3751CC767164280C5669885603F2EC792D1608C6DFCF88DF79EC79E980FDC250C0BC53BE361B73461CE534CEE82F3212D5272AD716E3E541ADA
E9CE0938343D943AC92B1059A5761D5B7182E74EA43AA07E772CFDA3EB7AD2FADE7DFF7EDDFBFD79BD1F6230188FD37F26E3FF7BC199FEFEEB3FB3F05FF12A3A
2D923D81245C89FEAAC13A91FAC0B9873E5CFC49553681D887633C1D1FA441F451DC9BD609EE03E31EAC1C1D097B9E45CD701D9D331D3F2C1D4B2F29CB04E5BE
A6F54DF2EF3BB7A9990B5B200DAC9FD4B2CF97F1903CCB7B7463131F36B2D12F5C95034DC672BD6F5274DFB8C5A16DC14BA105D8CFD2D1BD66618BED533AAF20
D2AF084F98EB393BAB487C332C32F826A97FCD3D52B1FE357B5F062EBEC510786FF3611DDB9A77CA21449B2BA67BE0622E28A2D55BECA35063DDC1B920049E2B
0DCF37D749FF31F7CEA517CB077A797773A9F731E5B3378823F7CC4383DDB070FE503E6C5FC6714BCAA47834E82AFFD627437F7716DBFC8BFC6F737FEB51DDB7
3294AA14226AF6EBA91E2309A7FFFA670B7A253820652D5D345D8AE63CB3DD50278331643A281A9423DEE9A27C6225463B4AD9E63AD55FE68EC586B730B7F231
E58D08D56B18A6194C2AE20FE0163E72BE255A16F1488E5456823D7E8632E2930A3DAB6BFC656D6A308F36D279357FCADD7361DDA9E9816C6CAE66FBDF59998B
DAB4EA6465B7045DE7EA4FB6E8E93B297DB1CA28392C167DE10B2D0A14D77BC5A11415FAF78DCF23B6ABE13ADB15E51CD260732B393894AB05CF738CCEABFB43
AEA4E5CA5A019183DAC009DA5312CF3713FC3BB940F6F697E89E6538BD4920964DC831277C8DBEAF12735D1FBF32CA53C3B12199F65783EA4D8D67A399DA88C7
BAA99CED7A742675D3790DBFE336ADF6CEAD2A17C21EF4BCCD594EE1A9E2AC69AE382912E357240D27CA712B858A5D31A2C012F76D17D7ADC2AB96060587D4E0
FD13BDEB633A0BF06EC1CBD6B64C1D4C7DC6AB09BBF590AE4AB2F0460CA83D6954B4938560EB6EB25AF34D77B90982A2799ABD224CED42821BB738EF3D1D90C2
1C9748EF438EF1F1CB5FE8034A4C5AF7D23B5183C1DF9FA8CA2F40C19329F45EB45874F83CB943AB8FECC680FA69253BD7CC2904C575D0FB31816D3A47445DBD
C73BBEB6D06D1C10A3B2A288209F015A33095EC023C336C2EBC8EF51A068D7F7E186332A38472BBFCB7943838E91AEC11B9416EB9CEDA7072EEB90E17A2C4652
6A88F86C0455168F1F934D70A4077B383D261C3872F3EB65B685F8150000FFFF",
			x(
				mono=1,
				interpol=3,
				extra=true,
				mirror=false,
				cycle=false)={0,5,10,15,20,25,30,35,40,45,
						50,55,60,65},
			y(
				mono=1,
				interpol=1,
				extra=true,
				mirror=false,
				cycle=false)={0,5,10,15,20,25,30,35,40,45,
						50,55,60,65,70,75}) "Logarithmic over temperature of heating system" annotation(
			Placement(transformation(extent={{-10,10},{10,30}})),
			Dialog(
				group="Temperature",
				tab="Results"));
	public
		Real TFlow(
			quantity="Thermics.Temp",
			displayUnit="°C") "Flow temperature of heating system" annotation(Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
		Real TReturn(
			quantity="Thermics.Temp",
			displayUnit="°C") "Return temperature" annotation(Dialog(
			group="Temperature",
			tab="Results",
			visible=false));
		output Modelica.Blocks.Interfaces.RealOutput QHeatCool(
			quantity="Basics.Power",
			displayUnit="kW") "Heat power of heating system" annotation(
			Placement(
				transformation(extent={{95,-45},{115,-25}}),
				iconTransformation(extent={{186.7,40},{206.7,60}})),
			Dialog(
				group="Heating Power",
				tab="Results",
				visible=false));
	protected
		Real QCoolingNorm(
			quantity="Basics.Power",
			displayUnit="Nm/s")=QHeatNorm*AZone/0.7*deltaTSubnormal/deltaTOverHeating "Calculated nominal cooling power" annotation(Dialog(
			group="Heating Power",
			tab="Results"));
	public
		output Modelica.Blocks.Interfaces.RealOutput qvRef(quantity="Thermics.VolumeFlow") "Reference volume flow" annotation(
			Placement(
				transformation(extent={{-85,35},{-65,55}}),
				iconTransformation(
					origin={-50,200},
					extent={{-10,-10},{10,10}},
					rotation=90)),
			Dialog(
				group="Volume Flow",
				tab="Results",
				visible=false));
		GreenCity.Utilities.Thermal.MeasureThermal measureThermal2 "Measurement of pipe temperature and volume flow" annotation(Placement(transformation(
			origin={-5,-45},
			extent={{-10,-10},{10,10}},
			rotation=-180)));
		Real qv(
			quantity="Thermics.VolumeFlow",
			displayUnit="l/min") "Volume flow of heating system" annotation(Dialog(
			group="Volume Flow",
			tab="Results",
			visible=false));
	protected
		Real aCool(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=alphaCool*((QCoolingNorm/alphaCool)^(1/1.1))^0.1 "Cooling heat transmission factor" annotation(Dialog(
			group="Heat Transmission",
			tab="Results"));
		Real aHeat(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=alphaHeat "Heating transmission factor" annotation(Dialog(
			group="Heat Transmission",
			tab="Results"));
		Real kappa(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=(((TZoneNorm-TReturnCoolNorm)/QCoolingNorm)-(1/aCool))^(-1) "Heat transition coefficient" annotation(Dialog(
			group="Heat Transmission",
			tab="Results"));
	public
		Real ECool(
			quantity="Basics.Energy",
			displayUnit="kWh") "Cold output of heating system" annotation(Dialog(
			group="Energy",
			tab="Results",
			visible=false));
		Real EHeat(
			quantity="Basics.Energy",
			displayUnit="kWh") "Heat output of heating system" annotation(Dialog(
			group="Energy",
			tab="Results",
			visible=false));
		parameter Real AZone(quantity="Geometry.Area")=300 "Heated (living) area" annotation(Dialog(tab="Parameters I"));
		parameter Integer nFloors=2 "Number of Floors" annotation(Dialog(tab="Parameters I"));
		parameter Real qvMax(quantity="Thermics.VolumeFlow")=0.00025 "Maximum volume flow" annotation(Dialog(tab="Parameters I"));
		parameter Real TReturnInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=303.15 "Initial return temperature of heating system" annotation(Dialog(
			group="Temperatures",
			tab="Parameters I"));
		parameter Real TFlowHeatNorm(quantity="Basics.Temp")=308.15 "Normal flow temperature of heating system" annotation(Dialog(
			group="Temperatures",
			tab="Parameters I"));
		parameter Real TReturnHeatNorm(quantity="Basics.Temp")=301.15 "Normal return temperature of heating system" annotation(Dialog(
			group="Temperatures",
			tab="Parameters I"));
		parameter Real TReturnCoolNorm(quantity="Basics.Temp")=293.15 "Normal return temperatuer for cooling" annotation(Dialog(
			group="Temperatures",
			tab="Parameters I"));
		parameter Real TZoneNorm(
			quantity="Thermics.Temp",
			displayUnit="°C")=294.15 "Normal zone temperature for heating system" annotation(Dialog(
			group="Temperatures",
			tab="Parameters I"));
		parameter Real deltaTSubnormal(
			quantity="Thermics.TempDiff",
			displayUnit="K")=8 "Subnormal temperature of cooling panel" annotation(Dialog(
			group="Temperatures",
			tab="Parameters I"));
		parameter Real deltaTOverHeating(
			quantity="Thermics.TempDiff",
			displayUnit="K")=16.7 "Heating over temperature of cooling panel" annotation(Dialog(
			group="Temperatures",
			tab="Parameters I"));
		parameter Real cpMed(
			quantity="Thermodynamics.SpecHeatCapacity",
			displayUnit="kJ/(kg·K)")=4177 "Specific heat capacity of heating medium" annotation(Dialog(
			group="Heating Medium",
			tab="Parameters I"));
		parameter Real rhoMed(
			quantity="Basics.Density",
			displayUnit="kg/m³")=1000 "Density of heating medium" annotation(Dialog(
			group="Heating Medium",
			tab="Parameters I"));
		parameter Real n=1.1 "Heating system exponent" annotation(Dialog(
			group="HeatingCoolingSystem",
			tab="Parameters I"));
		parameter Real APanel(
			quantity="Geometry.Area",
			displayUnit="m²")=0.7*AZone "Panel surface area" annotation(Dialog(
			group="HeatingCoolingSystem",
			tab="Parameters I"));
		parameter Integer nPanel=1 "Number of panels" annotation(Dialog(
			group="HeatingCoolingSystem",
			tab="Parameters I"));
		parameter Real VHeatCoolMedium(
			quantity="Geometry.Volume",
			displayUnit="l")=0.1 "Heating medium volume of each panel" annotation(Dialog(
			group="HeatingCoolingSystem",
			tab="Parameters I"));
		parameter Real QHeatNorm(
			quantity="Basics.Power",
			displayUnit="Nm/s")=15 "Area specific heating power, 15 W/m² for space heating" annotation(Dialog(
			group="HeatingCoolingSystem",
			tab="Parameters I"));
		parameter Real alphaHeat(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=6.7 "Heat transmission coefficient for heating (init: ceiling)" annotation(Dialog(
			group="HeatingCoolingSystem",
			tab="Parameters I"));
		parameter Real alphaCool(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=8.92 "Heat transmission coefficient for cooling (init: ceiling - equals floor heating)" annotation(Dialog(
			group="HeatingCoolingSystem",
			tab="Parameters I"));
		GreenCity.Interfaces.Thermal.DefineVolumeFlow ToReturn "Specifies a Thermal Volume Flow Connector" annotation(Placement(transformation(extent={{40,-35},{20,-55}})));
		GreenCity.Interfaces.Thermal.ExtractVolumeFlow FromFlow "Extracts the characteristics of a Thermal Volume Flow Connector" annotation(Placement(transformation(extent={{-60,0},{-40,-20}})));
		VolumeFlowControllerBuildingHC_DZ volumeFlowControllerBuildingHC_DZ1(
			QHeatMax(displayUnit="Nm/s")=AZone*QHeatNorm*2,
			QCoolMax(displayUnit="Nm/s")=self.QHeatMax,
			deltaTupHeat=0.01,
			deltaTlowHeat=-0.01,
			qvMax(displayUnit="m³/s")=qvMax) annotation(Placement(transformation(extent={{5,30},{-25,60}})));
	initial equation
		TReturn=TReturnInit;
		TFlow=FromFlow.TMedium;
		qv=FromFlow.qvMedium;
		ECool=0;
		EHeat=0;
	equation
		TFlow=FromFlow.TMedium;
		qv=FromFlow.qvMedium;
		if volumeFlowControllerBuildingHC_DZ1.Heating then
			cpMed*rhoMed*VHeatCoolMedium*der(TReturn-TZone)=-abs(((TOverLog(min(max((TReturn-TZone),0),65),min(max((TFlow-TZone),5),75)))/(TOverLog(min(max((TReturnHeatNorm-TZoneNorm),5),55),min(max((TFlowHeatNorm-TZoneNorm),5),75)))))^n*QHeatNorm*AZone+cpMed*rhoMed*qv*(TFlow-TReturn);
			QHeatCool=cpMed*rhoMed*qv*(TFlow-TReturn);
			der(EHeat)=QHeatCool;
			der(ECool)=0;
		elseif volumeFlowControllerBuildingHC_DZ1.Cooling then
			cpMed*rhoMed*VHeatCoolMedium*nPanel*der(TReturn)=-CalcPower(aHeat,aCool,kappa,TZone,TReturn)*APanel*nPanel+cpMed*rhoMed*qv*(TFlow-TReturn)+cpMed*rhoMed*VHeatCoolMedium*nPanel*der(TZone);
			//QHeatCool=CalcPower(aHeat,aCool,kappa,TZone,TReturn)*APanel*nPanel;
			QHeatCool=cpMed*rhoMed*qv*(TFlow-TReturn);
			der(ECool)=QHeatCool;
			der(EHeat)=0;
		else
			if TReturn > TRef_Cooling then
				der(TReturn) = -1e-4;
			elseif TReturn < TRef_Heating then
				der(TReturn) = 1e-4;
			else
				der(TReturn) = 0;
			end if;
			QHeatCool = 0;
			der(EHeat)=0;
			der(ECool)=0;
		end if;
		ToReturn.TMedium=TReturn;
	equation
		connect(measureThermal2.PipeOut,Return) annotation(Line(
			points={{-15,-45},{-20,-45},{-80,-45},{-85,-45}},
			color={190,30,45}));
		connect(FromFlow.Pipe,Flow) annotation(Line(
			points={{-60,-10},{-65,-10},{-80,-10},{-85,-10}},
			color={190,30,45},
			thickness=0.0625));
		connect(ToReturn.Pipe,measureThermal2.PipeIn) annotation(Line(
			points={{20,-45},{15,-45},{10,-45},{5,-45}},
			color={190,30,45}));
		connect(FromFlow.qvMedium,ToReturn.qvMedium) annotation(Line(
			points={{-40,-15},{-35,-15},{45,-15},{45,-40},{40,-40}},
			color={0,0,127},
			thickness=0.0625));
		connect(volumeFlowControllerBuildingHC_DZ1.qvRef,qvRef) annotation(Line(
			points={{-25,45},{-30,45},{-70,45},{-75,45}},
			color={0,0,127},
			thickness=0.0625));
		connect(TZone,volumeFlowControllerBuildingHC_DZ1.TAct) annotation(Line(
			points={{80,20},{75,20},{10,20},{10,35},{5,35}},
			color={0,0,127},
			thickness=0.0625));
		connect(volumeFlowControllerBuildingHC_DZ1.TReturn,measureThermal2.TMedium) annotation(Line(
			points={{-5,30.3},{-5,25.3},{-5,-30},{-5,-35}},
			color={0,0,127},
			thickness=0.0625));
		connect(volumeFlowControllerBuildingHC_DZ1.TFlow,FromFlow.TMedium) annotation(Line(
			points={{0,30.3},{0,25.3},{0,-5},{-35,-5},{-40,-5}},
			color={0,0,127},
			thickness=0.0625));
		connect(volumeFlowControllerBuildingHC_DZ1.Heating,Heating) annotation(Line(
			points={{-20,30.3},{-20,25.3},{-20,25},{-85,25},{-90,25}},
			color={255,0,255},
			thickness=0.0625));
		connect(volumeFlowControllerBuildingHC_DZ1.Cooling,Cooling) annotation(Line(
			points={{-15,30.3},{-15,25.3},{-15,10},{-85,10},{-90,10}},
			color={255,0,255},
			thickness=0.0625));
		connect(volumeFlowControllerBuildingHC_DZ1.TRef_heat,TRef_Heating) annotation(Line(
			points={{5,50},{10,50},{75,50},{80,50}},
			color={0,0,127},
			thickness=0.0625));
		connect(TRef_Cooling,volumeFlowControllerBuildingHC_DZ1.TRef_cold) annotation(Line(
			points={{80,85},{75,85},{10,85},{10,55},{5,55}},
			color={0,0,127},
			thickness=0.0625));
	annotation(
		statAnaRefVar(flags=2),
		__esi_protocols(transient={
																																																																																																																																																																								Protocol(var=time),
																																																																																																																																																																								Protocol(var=statAnaRefVar)}),
		__esi_solverOptions(
			solver="CVODE",
			typename="ExternalCVODEOptionData"),
		Icon(
			coordinateSystem(extent={{-200,-200},{200,200}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAWJQAAFiUBSVIk8AAAAE9JREFUeF7twQENAAAAwqD3T20ONyAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4FQN4XgAAdMZTtsAAAAASUVORK5C
YII=",
								extent={{-200,-200},{200,200}}),
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAWJQAAFiUBSVIk8AAAAE9JREFUeF7twQENAAAAwqD3T20ONyAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4FQN4XgAAdMZTtsAAAAASUVORK5C
YII=",
								extent={{-200,-200},{200,200}}),
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAWJQAAFiUBSVIk8AAAAE9JREFUeF7twQENAAAAwqD3T20ONyAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4FQN4XgAAdMZTtsAAAAASUVORK5C
YII=",
								extent={{-200,-200},{200,200}}),
							Text(
								textString="Heating",
								fontSize=14,
								extent={{-169.7,239.3},{183.6,9.300000000000001}}),
							Text(
								textString="and",
								fontSize=14,
								extent={{-129.9,76},{143.5,9.4}}),
							Text(
								textString="Cooling",
								fontSize=14,
								extent={{-156.3,-0.2},{173.7,-90.2}}),
							Text(
								textString="System",
								fontSize=14,
								extent={{-226.1,-73.59999999999999},{250.5,-190.2}})}),
		experiment(
			StopTime=1,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.002,
			__esi_Solver(
				bEffJac=false,
				bSparseMat=false,
				typename="CVODE"),
			__esi_SolverOptions(
				solver="CVODE",
				typename="ExternalCVODEOptionData"),
			__esi_MinInterval="9.999999999999999e-10",
			__esi_MaxInterval="0.002",
			__esi_AbsTolerance="1e-6"));
end HeatingAndCoolingSystem;
