﻿// CP: 65001
// SimulationX Version: 4.2.3.69223
within CoSES_ProHMo.Storage;
model StationaryBattery_GC "Green City Stationary battery system, slightly adapted"
	GreenCity.Interfaces.Electrical.DefineCurrentDC DCConnection "Specifies current, extracts voltage of a DC connector" annotation(Placement(transformation(extent={{330,-85},{350,-65}})));
	Modelica.Blocks.Tables.CombiTable2D RsCalc(
		tableOnFile=true,
		tableName=RsTable,
		fileName=ImpedanceFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	Modelica.Blocks.Tables.CombiTable2D R1Calc(
		tableOnFile=true,
		tableName=R1Table,
		fileName=ImpedanceFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	Modelica.Blocks.Tables.CombiTable2D R2Calc(
		tableOnFile=true,
		tableName=R2Table,
		fileName=ImpedanceFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	Modelica.Blocks.Tables.CombiTable2D P1Calc(
		tableOnFile=true,
		tableName=CPEP1Table,
		fileName=ImpedanceFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	Modelica.Blocks.Tables.CombiTable2D P2Calc(
		tableOnFile=true,
		tableName=CPEP2Table,
		fileName=ImpedanceFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	Modelica.Blocks.Tables.CombiTable2D T1Calc(
		tableOnFile=true,
		tableName=CPET1Table,
		fileName=ImpedanceFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	Modelica.Blocks.Tables.CombiTable2D T2Table(
		tableOnFile=true,
		tableName=CPET2Table,
		fileName=ImpedanceFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	Modelica.Blocks.Tables.CombiTable1D VocCalc(
		tableOnFile=true,
		tableName=VocTable,
		fileName=VocFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	Modelica.Blocks.Tables.CombiTable1D QRealMin(
		tableOnFile=true,
		tableName=QMinTable,
		fileName=QRealFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	Modelica.Blocks.Tables.CombiTable1D QRealMax(
		tableOnFile=true,
		tableName=QMaxTable,
		fileName=QRealFile) annotation(Placement(transformation(extent={{-10,-10},{10,10}})));
	GreenCity.Interfaces.Electrical.DC DC "DC connection to battery converter" annotation(Placement(
		transformation(extent={{385,-85},{405,-65}}),
		iconTransformation(extent={{190,-10},{210,10}})));
	GreenCity.Interfaces.Battery.BatteryBus ControlBus "Control interface to battery converter" annotation(Placement(
		transformation(extent={{195,-40},{215,-20}}),
		iconTransformation(extent={{-10,-210},{10,-190}})));
	protected
		Real Vrs(
			quantity="Electricity.Voltage",
			displayUnit="V") "Actual voltage drop of ohmic resistance" annotation(Dialog(
			group="Voltage",
			tab="Results 1"));
		Real VZARC[2](
			quantity="Electricity.Voltage",
			displayUnit="V") "Actual voltage drop of zarc-elements" annotation(Dialog(
			group="Voltage",
			tab="Results 1"));
		Real VImpedance(
			quantity="Electricity.Voltage",
			displayUnit="V") "Overall actual impedance of battery string" annotation(Dialog(
			group="Voltage",
			tab="Results 1"));
		Real V[2,3](
			quantity="Electricity.Voltage",
			displayUnit="V") "Voltage drop value array for ZARC-elements" annotation(Dialog(
			group="Voltage",
			tab="Results 1"));
	public
		Real VBattery(
			quantity="Electricity.Voltage",
			displayUnit="V") "Actual battery output voltage" annotation(Dialog(
			group="Voltage",
			tab="Results 1",
			visible=false));
	protected
		Real VModule(
			quantity="Electricity.Voltage",
			displayUnit="V") "Actual module voltage" annotation(Dialog(
			group="Voltage",
			tab="Results 1"));
		Real SOCNom(
			quantity="Basics.RelMagnitude",
			displayUnit="%") "Actual nominal state of charge" annotation(Dialog(
			group="State of Charge",
			tab="Results 1"));
	public
		Real SOCAct(
			quantity="Basics.RelMagnitude",
			displayUnit="%") "Actual real state of charge" annotation(Dialog(
			group="State of Charge",
			tab="Results 1",
			visible=false));
		Real TBattery(
			quantity="Thermics.Temp",
			displayUnit="°C") "Battery unit temperature" annotation(Dialog(
			group="Temperature",
			tab="Results 1",
			visible=false));
	protected
		Real IString(
			quantity="Electricity.Current",
			displayUnit="A") "Actual current in each battery string" annotation(Dialog(
			group="Current",
			tab="Results 1"));
	public
		Real IBattery(
			quantity="Electricity.Current",
			displayUnit="A") "Actual battery current" annotation(Dialog(
			group="Current",
			tab="Results 1",
			visible=false));
	protected
		Real R[2,3](
			quantity="Electricity.Resistance",
			displayUnit="Ohm") "Ohmic resistance value array for ZARC-elements" annotation(Dialog(
			group="Resistance",
			tab="Results 1"));
		Real C[2,3](
			quantity="Electricity.Capacitance",
			displayUnit="F") "Capacity value arry for ZARC-elements" annotation(Dialog(
			group="Resistance",
			tab="Results 1"));
		Real omega[2] "Auxiliary variable" annotation(Dialog(
			group="Resistance",
			tab="Results 1"));
		Real cb[2] "Auxiliary variable" annotation(Dialog(
			group="Resistance",
			tab="Results 1"));
	public
		Real PBattery(
			quantity="Basics.Power",
			displayUnit="kW") "Actual battery output power" annotation(Dialog(
			group="Electrical Power",
			tab="Results 1",
			visible=false));
	protected
		Real QActNom(
			quantity="Electricity.Echarge",
			displayUnit="Ah") "Actual nominal electrical charge" annotation(Dialog(
			group="Capacity",
			tab="Results 1"));
		Real QActReal(
			quantity="Electricity.Echarge",
			displayUnit="Ah") "Actual real electrical charge" annotation(Dialog(
			group="Capacity",
			tab="Results 1"));
		Real QMaxAct(
			quantity="Electricity.Echarge",
			displayUnit="Ah") "Actual maximum capacity" annotation(Dialog(
			group="Capacity",
			tab="Results 1"));
		Real QReal(
			quantity="Electricity.Echarge",
			displayUnit="Ah")=if useStandardParameter then QNom else QR "Real battery capacity" annotation(Dialog(
			group="Capacity",
			tab="Results 1"));
		Real QMinAct(
			quantity="Electricity.Echarge",
			displayUnit="Ah") "Actual minimum capacity" annotation(Dialog(
			group="Capacity",
			tab="Results 1"));
	public
		Real EBattCharge(
			quantity="Basics.Energy",
			displayUnit="kWh") "Electrical energy for battery charging" annotation(Dialog(
			group="Energy",
			tab="Results 2",
			visible=false));
		Real EBattDischarge(
			quantity="Basics.Energy",
			displayUnit="kWh") "Discharged electrical energy" annotation(Dialog(
			group="Energy",
			tab="Results 2",
			visible=false));
		Real SOHCurrent(
			quantity="Basics.RelMagnitude",
			displayUnit="%") "State of health due to temperature and Ah-aging" annotation(Dialog(
			group="State of Health",
			tab="Results 2",
			visible=false));
		Real SOHCyclization(
			quantity="Basics.RelMagnitude",
			displayUnit="%") "State of health due to temperature and SOC-swing-aging" annotation(Dialog(
			group="State of Health",
			tab="Results 2",
			visible=false));
		Real SOFCurrent(
			quantity="Basics.RelMagnitude",
			displayUnit="%") "State of function due to temperature and Ah-aging as well as actual temperature" annotation(Dialog(
			group="State of Function",
			tab="Results 2",
			visible=false));
		Real SOFCyclization(
			quantity="Basics.RelMagnitude",
			displayUnit="%") "State of function due to temperature and SOC-swing-aging as well as actual temperature" annotation(Dialog(
			group="State of Function",
			tab="Results 2",
			visible=false));
		parameter Boolean UseBat=true "Use Battery" annotation(Dialog(tab="Model Initialization"));
		parameter Boolean useStandardParameter=true "If true, standard parameter for model initialization are used, else battery model must be specially parameterized" annotation(Dialog(
			group="Model Configuration",
			tab="Model Initialization"));
		parameter Integer nSer=14 if not useStandardParameter "Number of battery units (cells or modules) connected in series" annotation(Dialog(
			group="Battery Dimensions",
			tab="Model Initialization"));
		parameter Integer nPar=1 if not useStandardParameter "Number of battery units (cells or modules) connected in parallel - number of strings" annotation(Dialog(
			group="Battery Dimensions",
			tab="Model Initialization"));
	protected
		parameter Integer nSeries(quantity="Basics.Unitless")=if useStandardParameter then (max(integer(ceil(VBattNominal/VModuleNominal)),1)) else nSer "Number of battery cells/modules connected in series" annotation(Dialog(
			group="Battery Dimensions",
			tab="Model Initialization"));
		parameter Integer nParallel=if useStandardParameter then (max(integer(ceil(EMaxNominal/(nSeries*VModuleNominal)/CModuleNominal)),1)) else nPar "Number of parallel connected battery cells/modules" annotation(Dialog(
			group="Battery Dimensions",
			tab="Model Initialization"));
	public
		parameter Real VModuleNominal(
			quantity="Electricity.Voltage",
			displayUnit="V")=3.6 if useStandardParameter "Nominal standard battery Module voltage" annotation(Dialog(
			group="Nominal Parameters",
			tab="Model Initialization"));
		parameter Real VBattNominal(
			quantity="Electricity.Voltage",
			displayUnit="V")=48 if useStandardParameter "Nominal battery voltage" annotation(Dialog(
			group="Nominal Parameters",
			tab="Model Initialization"));
		parameter Real CModuleNominal(
			quantity="Electricity.Echarge",
			displayUnit="Ah")=144000 if useStandardParameter "Nominal standard battery Module capacity" annotation(Dialog(
			group="Nominal Parameters",
			tab="Model Initialization"));
		parameter Real EMaxNominal(
			quantity="Basics.Energy",
			displayUnit="kWh")=7200000 if useStandardParameter "Nominal maximum battery energy content" annotation(Dialog(
			group="Nominal Parameters",
			tab="Model Initialization"));
		parameter Real TAmbient(
			quantity="Thermics.Temp",
			displayUnit="°C")=291.14999999999998 "Ambient temperature of battery" annotation(Dialog(
			group="Initialization",
			tab="Model Initialization"));
		parameter Real SOCInit(
			quantity="Basics.RelMagnitude",
			displayUnit="%")=0.5 "Initial state of charge of battery" annotation(Dialog(
			group="Initialization",
			tab="Model Initialization"));
		parameter Real RTransfer(
			quantity="Electricity.Resistance",
			displayUnit="mOhm")=0.001 "Transfer resistance of battery unit connectors" annotation(Dialog(
			group="Transfer Resistance",
			tab="Impedance Characteristics"));
		parameter String ImpedanceFile=GreenCity.Utilities.Functions.getModelDataDirectory()+"\\battery\\LiIon\\impedance\\ZARC.txt" "File name for ZARC-Model-components modeling impedance characteristic of battery unit" annotation(Dialog(
			group="Impedance Data",
			tab="Impedance Characteristics"));
		parameter String RsTable="Rs" "Table name for ohmic resistance characteristic of battery unit (ZARC-Model)" annotation(Dialog(
			group="Impedance Data",
			tab="Impedance Characteristics"));
		parameter String R1Table="R1" "Table name for R1-characteristic of battery unit (ZARC-Model)" annotation(Dialog(
			group="Impedance Data",
			tab="Impedance Characteristics"));
		parameter String R2Table="R2" "Table name for R1-characteristic of battery unit (ZARC-Model)" annotation(Dialog(
			group="Impedance Data",
			tab="Impedance Characteristics"));
		parameter String CPET1Table="CPE_T1" "Table name for CPE-T1-characteristic of battery unit (ZARC-Model)" annotation(Dialog(
			group="Impedance Data",
			tab="Impedance Characteristics"));
		parameter String CPET2Table="CPE_T2" "Table name for CPE-T2-characteristic of battery unit (ZARC-Model)" annotation(Dialog(
			group="Impedance Data",
			tab="Impedance Characteristics"));
		parameter String CPEP1Table="CPE_P1" "Table name for CPE-P1-characteristic of battery unit (ZARC-Model)" annotation(Dialog(
			group="Impedance Data",
			tab="Impedance Characteristics"));
		parameter String CPEP2Table="CPE_P2" "Table name for CPE-P2-characteristic of battery unit (ZARC-Model)" annotation(Dialog(
			group="Impedance Data",
			tab="Impedance Characteristics"));
	protected
		Curve pFunction(
			x={0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,
						0.95,1},
			y[1](
				mono=0,
				interpol=1,
				extra=true,
				mirror=false,
				cycle=false)={22.01,15.795,11.579,8.696999999999999,6.662,5.218,4.16,3.354,2.714,2.181,
						1.697,1}) "Exponential factor for impedance calculation" annotation(
			Placement(transformation(extent={{-10,-10},{10,10}})),
			Dialog(
				group="Impedance Data",
				tab="Impedance Characteristics"));
	public
		parameter String VocFile=GreenCity.Utilities.Functions.getModelDataDirectory()+"\\battery\\LiIon\\voltage\\Open_circuit_voltage.txt" "File name of open-circuit-voltage of battery unit" annotation(Dialog(
			group="Open-Circuit-Voltage Characteristics",
			tab="Voltage Characteristics"));
		parameter String VocTable="U_SOC" "Table name of open-circuit-voltage of battery unit" annotation(Dialog(
			group="Open-Circuit-Voltage Characteristics",
			tab="Voltage Characteristics"));
		parameter Real alphaVoc(
			quantity="Electricity.Voltage",
			displayUnit="mV")=0.00008 "Voltage reduction because of temperaturedifferences (TBattery-25°C), LiIon: 0.08mV/K, PbO2: 0.23mV/K" annotation(Dialog(
			group="Open-Circuit-Voltage Characteristics",
			tab="Voltage Characteristics"));
		parameter Real VMin(
			quantity="Electricity.Voltage",
			displayUnit="V")=3 "Minimum voltage of battery unit (cells or modules)" annotation(Dialog(
			group="Voltage Boundaries",
			tab="Voltage Characteristics"));
		parameter Real VMax(
			quantity="Electricity.Voltage",
			displayUnit="V")=4.2 "Maximum voltage of battery unit (cells or modules)" annotation(Dialog(
			group="Voltage Boundaries",
			tab="Voltage Characteristics"));
		parameter Real CHeat(
			quantity="Thermics.HeatCapacity",
			displayUnit="J/K")=219.6 "Heat capacity of battery units (cells or modules)" annotation(Dialog(
			group="Temperature Behavior",
			tab="Thermal Characteristics"));
		parameter Real gThermal(
			quantity="Thermics.HeatCond",
			displayUnit="W/K")=0.297 "Heat conductance of battery units (cells or modules) vs. ambience" annotation(Dialog(
			group="Temperature Behavior",
			tab="Thermal Characteristics"));
		parameter Real TMin(
			quantity="Thermics.Temp",
			displayUnit="°C")=263.14999999999998 "Minimum temperature for battery characteristics" annotation(Dialog(
			group="Temperature Boundaries",
			tab="Thermal Characteristics"));
		parameter Real TMax(
			quantity="Thermics.Temp",
			displayUnit="°C")=323.14999999999998 "Maximum temperature for battery characteristics" annotation(Dialog(
			group="Temperature Boundaries",
			tab="Thermal Characteristics"));
		parameter Real QR(
			quantity="Electricity.Echarge",
			displayUnit="Ah")=141922.8 if not useStandardParameter "Real measured 1C capacity at 25°C" annotation(Dialog(
			group="Capacity",
			tab="Capacity Characteristics"));
		parameter Real QN(
			quantity="Electricity.Echarge",
			displayUnit="Ah")=144000 if not useStandardParameter "Nominal capacity of battery unit" annotation(Dialog(
			group="Capacity",
			tab="Capacity Characteristics"));
	protected
		parameter Real QNom(
			quantity="Electricity.Echarge",
			displayUnit="C")=if useStandardParameter then ((EMaxNominal/(nSeries*VModuleNominal))/nParallel) else QN "Nominal battery capacity" annotation(Dialog(
			group="Capacity",
			tab="Capacity Characteristics"));
	public
		parameter String QRealFile=GreenCity.Utilities.Functions.getModelDataDirectory()+"\\battery\\LiIon\\capacity\\real_capacity.txt" "File name for temperature related relative minimum and maximum capacities" annotation(Dialog(
			group="Capacity Boundaries",
			tab="Capacity Characteristics"));
		parameter String QMaxTable="Qmax" "Table name for temperature related relative maximum capacity" annotation(Dialog(
			group="Capacity Boundaries",
			tab="Capacity Characteristics"));
		parameter String QMinTable="Qmin" "Table name for temperature related relative minimum capacity" annotation(Dialog(
			group="Capacity Boundaries",
			tab="Capacity Characteristics"));
	initial equation
		assert(((SOCInit<=1) and (SOCInit>=0)), "SOCInit out of limits");
		
		TBattery=TAmbient;
		
		for i in 1:3 loop
			V[1,i]=R[1,i]*IString;
			V[2,i]=R[2,i]*IString;
		end for;
					
		QActNom=(SOCInit-1)*QNom;
		QActReal=(SOCInit-1)*(QMaxAct-QMinAct)-(QReal-QMaxAct);
			
		EBattCharge=0;
		EBattDischarge=0;
	equation
		if UseBat then
			RsCalc.u1=min((max(TBattery,TMin)),TMax);
			R1Calc.u1=min((max(TBattery,TMin)),TMax);
			R2Calc.u1=min((max(TBattery,TMin)),TMax);
			T1Calc.u1=min((max(TBattery,TMin)),TMax);
			T2Table.u1=min((max(TBattery,TMin)),TMax);
			P1Calc.u1=min((max(TBattery,TMin)),TMax);
			P2Calc.u1=min((max(TBattery,TMin)),TMax);
			RsCalc.u2=min((max(SOCAct,0)),1);
			R1Calc.u2=min((max(SOCAct,0)),1);
			R2Calc.u2=min((max(SOCAct,0)),1);
			T1Calc.u2=min((max(SOCAct,0)),1);
			T2Table.u2=min((max(SOCAct,0)),1);
			P1Calc.u2=min((max(SOCAct,0)),1);
			P2Calc.u2=min((max(SOCAct,0)),1);
			VocCalc.u[1]=min((max(SOCAct,0)),1)*100;
			
			for i in 1:3 loop
				(R[1,i])=(R1Calc.y)/3;
				(R[2,i])=(R2Calc.y)/3;
			end for;
			
			omega[1]=(1/(T1Calc.y*R1Calc.y))^(1/P1Calc.y);
			omega[2]=(1/(T2Table.y*R2Calc.y))^(1/P2Calc.y);
			
			cb[1]=3/(omega[1]*R1Calc.y);
			cb[2]=3/(omega[2]*R2Calc.y);
			
			C[1,1]=cb[1];
			C[1,2]=cb[1]/(pFunction(P1Calc.y));
			C[1,3]=cb[1]*(pFunction(P1Calc.y));
			C[2,1]=cb[2];
			C[2,2]=cb[2]/(pFunction(P2Calc.y));
			C[2,3]=cb[2]*(pFunction(P2Calc.y));
			
			for i in 1:2 loop
				for j in 1:3 loop
					der(V[i,j])=(R[i,j]*IString-V[i,j])/(R[i,j]*C[i,j]);
				end for;
			end for;
			
			for i in 1:2 loop
				VZARC[i]=V[i,1]+V[i,2]+V[i,3];
			end for;
				
			Vrs=IString*RsCalc.y;
			VImpedance=Vrs+VZARC[1]+VZARC[2];
			VBattery=(VocCalc.y[1]+alphaVoc*(TBattery-298.15))*nSeries+VImpedance*nSeries+RTransfer*(nSeries+1)*IString;
			IString*nParallel=DCConnection.I;
			ControlBus.Voc=(VocCalc.y[1]+alphaVoc*(TBattery-298.15))*nSeries;
			
			VModule=(VocCalc.y[1]+alphaVoc*(TBattery-298.15))+VImpedance;
			
			CHeat*nSeries*nParallel*der(TBattery)=Vrs^2/RsCalc.y+(V[1,1]^2/R[1,1]+V[1,2]^2/R[1,2]+V[1,3]^2/R[1,3])-gThermal*(TBattery-TAmbient);
			
			QRealMin.u[1]=min((max(TBattery,TMin)),TMax);
			QRealMax.u[1]=min((max(TBattery,TMin)),TMax);
		
		
			QMaxAct=QRealMax.y[1]*QReal;
			QMinAct=QRealMin.y[1]*QReal;
					
			SOHCurrent=1;
			SOHCyclization=1;
			
			SOFCurrent=(QMaxAct-QMinAct)/QNom;
			SOFCyclization=(QMaxAct-QMinAct)/QNom;
		
			
			min((max(DCConnection.V,VMin*nSeries)),VMax*nSeries)=(VocCalc.y[1]+alphaVoc*(TBattery-298.15))*nSeries+VImpedance*nSeries+RTransfer*(nSeries+1)*IString;
			der(QActNom)=IString;
			der(QActReal)=IString;
			SOCNom=(QActNom+QNom)/QNom;
			SOCAct=1+((QActReal+(QReal-QMaxAct))/(QMaxAct-QMinAct));
				
			ControlBus.VIC=min((max(DCConnection.V,VMin*nSeries)),VMax*nSeries);
			ControlBus.VICMin=VMin*nSeries;
			ControlBus.VICMax=VMax*nSeries;
			
			IBattery=-IString*nParallel;
			PBattery=VBattery*IBattery;
			ControlBus.SOC=SOCAct;
			der(EBattCharge)=min(PBattery,0);
			der(EBattDischarge)=max(PBattery,0);
		else
			RsCalc.u1=0;
			R1Calc.u1=0;
			R2Calc.u1=0;
			T1Calc.u1=0;
			T2Table.u1=0;
			P1Calc.u1=0;
			P2Calc.u1=0;
			RsCalc.u2=0;
			R1Calc.u2=0;
			R2Calc.u2=0;
			T1Calc.u2=0;
			T2Table.u2=0;
			P1Calc.u2=0;
			P2Calc.u2=0;
			VocCalc.u[1]=0;
			
			for i in 1:3 loop
				(R[1,i])=0;
				(R[2,i])=0;
			end for;
			
			omega[1]=0;
			omega[2]=0;
			
			cb[1]=0;
			cb[2]=0;
			
			C[1,1]=0;
			C[1,2]=0;
			C[1,3]=0;
			C[2,1]=0;
			C[2,2]=0;
			C[2,3]=0;
			
			for i in 1:2 loop
				for j in 1:3 loop
					der(V[i,j])=0;
				end for;
			end for;
			
			for i in 1:2 loop
				VZARC[i]=0;
			end for;
				
			Vrs=0;
			VImpedance=0;
			VBattery=0;
			IString*nParallel=DCConnection.I;
			ControlBus.Voc=0;
			VModule=0;
			der(TBattery)=0;
			QRealMin.u[1]=0;
			QRealMax.u[1]=0;
			QMaxAct=0;
			QMinAct=0;
			SOHCurrent=0;
			SOHCyclization=0;
			SOFCurrent=0;
			SOFCyclization=0;
			IString=0;
			der(QActNom)=0;
			der(QActReal)=0;
			SOCNom=0;
			SOCAct=0;	
			ControlBus.VIC=0;
			ControlBus.VICMin=0;
			ControlBus.VICMax=0;
			IBattery=0;
			PBattery=0;
			ControlBus.SOC=0;
			der(EBattCharge)=0;
			der(EBattDischarge)=0;
		end if;
	equation
		connect(DCConnection.FlowCurrent,DC) annotation(Line(
			points={{350,-75},{355,-75},{390,-75},{395,-75}},
			color={247,148,29}));
	annotation(
		Icon(
			coordinateSystem(extent={{-200,-200},{200,200}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAG4AAABuCAYAAADGWyb7AAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAOvAAADrwBlbxySQAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAQbSURB
VHhe7d0/aBNhHMbx7LqKg4JBqShaDApdBOnm6qZjHN2Ko0srjoIB6SAUrYUOQq1FURwCFhERXaKD
uFgqmEHqEGuhBvrntb9rLrz33ntvc+lxeZ/j+cKPJrk/afLpXVpsY0kxyAgHGuFAi8FtrTTU+vyo
Wrtf4ngwYiEmZhE4WcG2MWfwY+JF4Hik+TtioxeBs23A8Wf0eoJr16tqs7motleXO2uyrJPnVp5j
ea5tBjJ6e8JtLC10lrK8kufcZqHnhBN9NphsR56eE04OXTaY5Lk3PfSccNvtVmcJyzt5zTM99Jxw
bLC5PAjncS4Pwnmcy4NwHufyIJzHuTwI53EuD8J5nMsjc7jp6Wk1OjoajFwueuPj46pUKgUjl7PM
5ZE53MTERPeByOWiRzjQCAca4UAjHGiEA41woBEONMKBRjjQCAca4UAjHGiEA41woBEONMKBVni4
sbGx7r+MNxrxv7BELQlOHmd4++Jif7/K7/LIDS6LB+JjhAONcKARDjTCgUY40AgHGuFAIxxohAON
cKARDjTCgUY40AgHGuFA8xJO7jDtVKvV7iecBFer1azbyiwv7761YqvVsi63jSTbmbel3Ye5fpi5
b316gXM9XteYHnpOuPCO+50kONeE28gnbltuG0k/0sPb0u7DXD/M3Lc+vcD1O6aHXuSauaJtZ2mG
cPH104zpoRe5Zq4od552yuVy946T4CqVSmy7cMJ3apDfS7Ett42kv9tDeFvafZjrh5n71qcXONfj
dY3poeeE6yf9qzMJTr6yi1IvcP0+XpcH4fYZ4UAjHGiEA41woBEONMKBRjjQCAca4UAjHGiEA41w
oBUejn9KnD6XR25wRS0JLotcHoTbZ4QDjXCgEQ40woFGONAIBxrhQCMcaIQDjXCgEQ40woFGONAI
BxrhQCsMHP876exyeWQOx5L7+++nav75EBm5LSmXB+Fy7HPzoZp8e8w6C1+uBsvbG6udtQnnTb/X
vlrRwrn+7JSaej8cAEouD8Ll3MzHi1Y0mUszw+rKkzPB5Xffbzs9CJdzAnL3TVldnj2rhqcq6trc
aXWnfjzAuvlyKLgthHR5EC7n9NOlgI08OqcO3BsJIOU64TxOTpdyShSko5MXgtc2+Tj04Hxwutw3
3Pbq7vtqsWz79msuOC3eeH4yOG0KknwUODny5Prj+pGYh54TbrNZnN/x9y3bNykhnlx++upwzEPP
CdeuVztLWNbJD98mnEx4xDVmD8Y89JxwMhtLC52lLOvkO0wT7tbrE+rFi0NWC73uta2d1zPbyjJy
5Mlpk6952ffpRy0Ak9c0OT3ajrRw9PY84jj+jF7k2vp8/K32OH6M2OhF4LZWGtaNOIMfsdGLHn87
yQo88vwZsTDRpBgcw4hwoBEOMqX+A/y4mo4ymGy+AAAAAElFTkSuQmCC",
								extent={{-200,200},{203,-203}})}),
		experiment(
			StopTime=1,
			StartTime=0,
			Interval=0.002,
			__esi_MaxInterval="0.002"));
end StationaryBattery_GC;
