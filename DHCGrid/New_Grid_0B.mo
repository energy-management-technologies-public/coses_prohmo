﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo.DHCGrid;
model New_Grid_0B "NewModel1"
	package Medium = Buildings.Media.Water;
	parameter Modelica.SIunits.MassFlowRate m_flow_nominal(displayUnit="kg/min")=0.5 "Nominal mass flow rate";
	parameter Modelica.SIunits.PressureDifference dp_nominal=200000 "Nominal pressure difference";
	inner Modelica.Fluid.System system(
		energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
		m_flow_start=0,
		p_start(displayUnit="Pa")=Medium.p_default,
		T_start(displayUnit="K")=Medium.T_default) annotation(Placement(transformation(extent={{65,120},{85,140}})));
	Real rho(quantity="Basics.Density") "Density of medium";
	parameter Real cp=4181 "specific heat capacity";
	parameter Real qv_min(quantity="Thermics.VolumeFlow")=1.666666666666667e-05 "Minimum flowrate for the pumps";
	Real THotPip(quantity="Basics.Temp") "Temperature of hot pipe on the grid" annotation(Dialog(
		group="Temperature and flow",
		tab="Results",
		visible=false));
	Real TColPip(quantity="Basics.Temp") "Temperature of cold pipe on the grid" annotation(Dialog(
		group="Temperature and flow",
		tab="Results",
		visible=false));
	Real DeltaTGrid(quantity="Basics.Temp") "Temperature difference between hot and cold pipe of the grid" annotation(Dialog(
		group="Temperature and flow",
		tab="Results",
		visible=false));
	Real QProHea(quantity="Basics.Power") "Heat Demand (Power) requested from the prosumer" annotation(Dialog(
		group="Demand",
		tab="Results",
		visible=false));
	Real QProCoo(quantity="Basics.Power") "Cooling Demand (Power) requested from the prosumer" annotation(Dialog(
		group="Demand",
		tab="Results",
		visible=false));
	Real DemHeaCooOff "Indicates what demand is present if any (1 heat, -1 cooling, 0 no demand)" annotation(Dialog(
		group="Demand",
		tab="Results",
		visible=false));
	Modelica.Fluid.Interfaces.FluidPort_a port_a_Hot(redeclare package Medium = Medium) "Generic fluid connector at design inlet" annotation(Placement(
		transformation(extent={{-135,40},{-115,60}}),
		iconTransformation(extent={{-210,40},{-190,60}})));
	Modelica.Fluid.Fittings.TeeJunctionIdeal teeJunctionIdeal1(redeclare package Medium = Medium) "Splitting/joining component with static balances for an infinitesimal control volume" annotation(Placement(transformation(extent={{45,40},{65,60}})));
	Modelica.Fluid.Interfaces.FluidPort_a port_Pro_Hot(redeclare package Medium = Medium) "Generic fluid connector at design inlet" annotation(Placement(
		transformation(
			origin={240,90},
			extent={{-10,-10},{10,10}}),
		iconTransformation(extent={{-60,190},{-40,210}})));
	Modelica.Fluid.Interfaces.FluidPort_b port_b_Hot(redeclare package Medium = Medium) "Generic fluid connector at design outlet" annotation(Placement(
		transformation(extent={{230,40},{250,60}}),
		iconTransformation(extent={{186.7,40},{206.7,60}})));
	Modelica.Fluid.Sensors.VolumeFlowRate FlowSens_GridColdPipe_Gri_Hot1(
		redeclare package Medium = Medium,
		V_flow(displayUnit="l/min")) annotation(Placement(transformation(extent={{10,40},{30,60}})));
	Modelica.Fluid.Sensors.TemperatureTwoPort TemSenColdPipe_Gri_Hot1(redeclare package Medium = Medium) annotation(Placement(transformation(extent={{-25,40},{-5,60}})));
	Modelica.Fluid.Sensors.MassFlowRate massFlowRate_Gri_Hot1(
		redeclare package Medium = Medium,
		m_flow(displayUnit="kg/min")) annotation(Placement(transformation(extent={{-55,40},{-35,60}})));
	Modelica.Fluid.Sensors.VolumeFlowRate FlowSens_GridColdPipe_Gri_Hot2(
		redeclare package Medium = Medium,
		V_flow(displayUnit="l/min")) annotation(Placement(transformation(extent={{145,40},{165,60}})));
	Modelica.Fluid.Sensors.TemperatureTwoPort TemSenColdPipe_Gri_Hot2(redeclare package Medium = Medium) annotation(Placement(transformation(extent={{110,40},{130,60}})));
	Modelica.Fluid.Sensors.MassFlowRate massFlowRate_Gri_Hot2(redeclare package Medium = Medium) annotation(Placement(transformation(extent={{80,40},{100,60}})));
	Modelica.Fluid.Sensors.VolumeFlowRate FlowSens_GridColdPipe_Pro_Hot(
		redeclare package Medium = Medium,
		V_flow(displayUnit="l/min")) annotation(Placement(transformation(extent={{125,80},{145,100}})));
	Modelica.Fluid.Sensors.TemperatureTwoPort TemSenColdPipe_Pro_Hot(redeclare package Medium = Medium) annotation(Placement(transformation(extent={{90,80},{110,100}})));
	Modelica.Fluid.Sensors.MassFlowRate massFlowRate_Pro_Hot(
		redeclare package Medium = Medium,
		m_flow(displayUnit="kg/min")) annotation(Placement(transformation(extent={{60,80},{80,100}})));
	Modelica.Fluid.Interfaces.FluidPort_a port_a_Cold(redeclare package Medium = Medium) "Generic fluid connector at design inlet" annotation(Placement(
		transformation(extent={{-135,-65},{-115,-45}}),
		iconTransformation(extent={{-210,-60},{-190,-40}})));
	Modelica.Fluid.Fittings.TeeJunctionIdeal teeJunctionIdeal2(redeclare package Medium = Medium) "Splitting/joining component with static balances for an infinitesimal control volume" annotation(Placement(transformation(extent={{45,-65},{65,-45}})));
	Modelica.Fluid.Interfaces.FluidPort_a port_Pro_Cold(redeclare package Medium = Medium) "Generic fluid connector at design inlet" annotation(Placement(
		transformation(
			origin={240,-15},
			extent={{-10,-10},{10,10}}),
		iconTransformation(extent={{40,190},{60,210}})));
	Modelica.Fluid.Interfaces.FluidPort_b port_b_Cold(redeclare package Medium = Medium) "Generic fluid connector at design outlet" annotation(Placement(
		transformation(extent={{230,-65},{250,-45}}),
		iconTransformation(extent={{186.7,-60},{206.7,-40}})));
	Modelica.Fluid.Sensors.VolumeFlowRate FlowSens_GridColdPipe_Gri_Cold1(
		redeclare package Medium = Medium,
		V_flow(displayUnit="l/min")) annotation(Placement(transformation(extent={{10,-65},{30,-45}})));
	Modelica.Fluid.Sensors.TemperatureTwoPort TemSenColdPipe_Gri_Cold1(redeclare package Medium = Medium) annotation(Placement(transformation(extent={{-25,-65},{-5,-45}})));
	Modelica.Fluid.Sensors.MassFlowRate massFlowRate_Gri_Cold1(redeclare package Medium = Medium) annotation(Placement(transformation(extent={{-55,-65},{-35,-45}})));
	Modelica.Fluid.Sensors.VolumeFlowRate FlowSens_GridColdPipe_Gri_Cold2(
		redeclare package Medium = Medium,
		V_flow(displayUnit="l/min")) annotation(Placement(transformation(extent={{145,-65},{165,-45}})));
	Modelica.Fluid.Sensors.TemperatureTwoPort TemSenColdPipe_Gri_Cold2(redeclare package Medium = Medium) annotation(Placement(transformation(extent={{110,-65},{130,-45}})));
	Modelica.Fluid.Sensors.MassFlowRate massFlowRate_Gri_Cold2(redeclare package Medium = Medium) annotation(Placement(transformation(extent={{80,-65},{100,-45}})));
	Modelica.Fluid.Sensors.VolumeFlowRate FlowSens_GridColdPipe_Pro_Cold(
		redeclare package Medium = Medium,
		V_flow(displayUnit="l/min")) annotation(Placement(transformation(extent={{125,-25},{145,-5}})));
	Modelica.Fluid.Sensors.TemperatureTwoPort TemSenColdPipe_Pro_Cold(redeclare package Medium = Medium) annotation(Placement(transformation(extent={{90,-25},{110,-5}})));
	Modelica.Fluid.Sensors.MassFlowRate massFlowRate_Pro_Cold(
		redeclare package Medium = Medium,
		m_flow(displayUnit="kg/min")) annotation(Placement(transformation(extent={{60,-25},{80,-5}})));
	Buildings.Fluid.FixedResistances.Pipe pip1(
		redeclare package Medium = Medium,
		energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
		p_start(displayUnit="Pa")=system.p_start,
		T_start(displayUnit="degC")=system.T_start,
		m_flow_nominal=m_flow_nominal,
		thicknessIns(displayUnit="mm")=0.01,
		lambdaIns=0.01,
		diameter(displayUnit="mm")=0.022,
		length(displayUnit="mm")=0.58) annotation(Placement(transformation(extent={{185,-25},{205,-5}})));
	Buildings.Fluid.FixedResistances.Pipe pip2(
		redeclare package Medium = Medium,
		energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
		p_start(displayUnit="Pa")=system.p_start,
		T_start(displayUnit="degC")=system.T_start,
		m_flow_nominal=m_flow_nominal,
		thicknessIns(displayUnit="mm")=0.01,
		lambdaIns=0.01,
		diameter(displayUnit="mm")=0.022,
		length(displayUnit="mm")=0.39) annotation(Placement(transformation(extent={{-100,40},{-80,60}})));
	Buildings.Fluid.FixedResistances.Pipe pip3(
		redeclare package Medium = Medium,
		energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
		p_start(displayUnit="Pa")=system.p_start,
		T_start(displayUnit="degC")=system.T_start,
		m_flow_nominal=m_flow_nominal,
		thicknessIns(displayUnit="mm")=0.01,
		lambdaIns=0.01,
		diameter(displayUnit="mm")=0.022,
		length(displayUnit="mm")=0.39) annotation(Placement(transformation(extent={{-100,-65},{-80,-45}})));
	Buildings.Fluid.FixedResistances.Pipe pip4(
		redeclare package Medium = Medium,
		energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
		p_start(displayUnit="Pa")=system.p_start,
		T_start(displayUnit="degC")=system.T_start,
		m_flow_nominal=m_flow_nominal,
		thicknessIns(displayUnit="mm")=0.01,
		lambdaIns=0.01,
		diameter(displayUnit="mm")=0.022,
		length(displayUnit="mm")=1.27) annotation(Placement(transformation(extent={{185,-65},{205,-45}})));
	equation
		connect(TemSenColdPipe_Pro_Cold.port_b,FlowSens_GridColdPipe_Pro_Cold.port_a) annotation(Line(
			points={{110,-15},{115,-15},{120,-15},{125,-15}},
			color={0,127,255},
			thickness=0.0625));
		connect(massFlowRate_Pro_Cold.port_b,TemSenColdPipe_Pro_Cold.port_a) annotation(Line(
			points={{80,-15},{85,-15},{90,-15}},
			color={0,127,255},
			thickness=0.0625));
		connect(teeJunctionIdeal2.port_3,massFlowRate_Pro_Cold.port_a) annotation(Line(
			points={{55,-45},{55,-40},{55,-15},{60,-15}},
			color={0,127,255},
			thickness=0.0625));
		connect(TemSenColdPipe_Pro_Hot.port_b,FlowSens_GridColdPipe_Pro_Hot.port_a) annotation(Line(
			points={{110,90},{115,90},{120,90},{125,90}},
			color={0,127,255},
			thickness=0.0625));
		connect(massFlowRate_Pro_Hot.port_b,TemSenColdPipe_Pro_Hot.port_a) annotation(Line(
			points={{80,90},{85,90},{90,90}},
			color={0,127,255},
			thickness=0.0625));
		connect(teeJunctionIdeal1.port_3,massFlowRate_Pro_Hot.port_a) annotation(Line(
			points={{55,60},{55,65},{55,90},{60,90}},
			color={0,127,255},
			thickness=0.0625));
		connect(TemSenColdPipe_Gri_Cold2.port_b,FlowSens_GridColdPipe_Gri_Cold2.port_a) annotation(Line(
			points={{130,-55},{135,-55},{140,-55},{145,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(massFlowRate_Gri_Cold2.port_b,TemSenColdPipe_Gri_Cold2.port_a) annotation(Line(
			points={{100,-55},{105,-55},{110,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(teeJunctionIdeal2.port_2,massFlowRate_Gri_Cold2.port_a) annotation(Line(
			points={{65,-55},{70,-55},{75,-55},{80,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(FlowSens_GridColdPipe_Gri_Cold1.port_b,teeJunctionIdeal2.port_1) annotation(Line(
			points={{30,-55},{35,-55},{40,-55},{45,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(TemSenColdPipe_Gri_Cold1.port_b,FlowSens_GridColdPipe_Gri_Cold1.port_a) annotation(Line(
			points={{-5,-55},{0,-55},{5,-55},{10,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(massFlowRate_Gri_Cold1.port_b,TemSenColdPipe_Gri_Cold1.port_a) annotation(Line(
			points={{-35,-55},{-30,-55},{-25,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(TemSenColdPipe_Gri_Hot2.port_b,FlowSens_GridColdPipe_Gri_Hot2.port_a) annotation(Line(
			points={{130,50},{135,50},{140,50},{145,50}},
			color={0,127,255},
			thickness=0.0625));
		connect(massFlowRate_Gri_Hot2.port_b,TemSenColdPipe_Gri_Hot2.port_a) annotation(Line(
			points={{100,50},{105,50},{110,50}},
			color={0,127,255},
			thickness=0.0625));
		connect(teeJunctionIdeal1.port_2,massFlowRate_Gri_Hot2.port_a) annotation(Line(
			points={{65,50},{70,50},{75,50},{80,50}},
			color={0,127,255},
			thickness=0.0625));
		connect(FlowSens_GridColdPipe_Gri_Hot1.port_b,teeJunctionIdeal1.port_1) annotation(Line(
			points={{30,50},{35,50},{40,50},{45,50}},
			color={0,127,255},
			thickness=0.0625));
		connect(TemSenColdPipe_Gri_Hot1.port_b,FlowSens_GridColdPipe_Gri_Hot1.port_a) annotation(Line(
			points={{-5,50},{0,50},{5,50},{10,50}},
			color={0,127,255},
			thickness=0.0625));
		connect(massFlowRate_Gri_Hot1.port_b,TemSenColdPipe_Gri_Hot1.port_a) annotation(Line(
			points={{-35,50},{-30,50},{-25,50}},
			color={0,127,255},
			thickness=0.0625));
		connect(pip2.port_a,port_a_Hot) annotation(Line(
			points={{-100,50},{-105,50},{-120,50},{-125,50}},
			color={0,127,255},
			thickness=0.0625));
		connect(pip2.port_b,massFlowRate_Gri_Hot1.port_a) annotation(Line(
			points={{-80,50},{-75,50},{-60,50},{-55,50}},
			color={0,127,255},
			thickness=0.0625));
		connect(FlowSens_GridColdPipe_Pro_Hot.port_b,port_Pro_Hot) annotation(Line(
			points={{145,90},{150,90},{235,90},{240,90}},
			color={0,127,255},
			thickness=0.0625));
		connect(FlowSens_GridColdPipe_Gri_Cold2.port_b,pip4.port_a) annotation(Line(
			points={{165,-55},{170,-55},{180,-55},{185,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(pip4.port_b,port_b_Cold) annotation(Line(
			points={{205,-55},{210,-55},{235,-55},{240,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(pip3.port_b,massFlowRate_Gri_Cold1.port_a) annotation(Line(
			points={{-80,-55},{-75,-55},{-60,-55},{-55,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(pip3.port_a,port_a_Cold) annotation(Line(
			points={{-100,-55},{-105,-55},{-120,-55},{-125,-55}},
			color={0,127,255},
			thickness=0.0625));
		connect(FlowSens_GridColdPipe_Pro_Cold.port_b,pip1.port_a) annotation(Line(
			points={{145,-15},{150,-15},{180,-15},{185,-15}},
			color={0,127,255},
			thickness=0.0625));
		connect(pip1.port_b,port_Pro_Cold) annotation(Line(
			points={{205,-15},{210,-15},{235,-15},{240,-15}},
			color={0,127,255},
			thickness=0.0625));
		connect(FlowSens_GridColdPipe_Gri_Hot2.port_b,port_b_Hot) annotation(Line(
			points={{165,50},{170,50},{235,50},{240,50}},
			color={0,127,255},
			thickness=0.0625));
	annotation(
		__Dymola_Commands(file="modelica://Buildings/Resources/Scripts/Dymola/Fluid/Movers/Examples/ClosedLoop_y.mos" "Simulate and plot"),
		Icon(
			coordinateSystem(extent={{-200,-200},{200,200}}),
			graphics={
				Line(
					points={{-149.8,49.8},{150.2,49.8}},
					color={139,0,0},
					thickness=8),
				Line(
					points={{-49.8,149.8},{-49.8,49.8}},
					color={139,0,0},
					thickness=8),
				Line(
					points={{-149.8,-50.2},{150.2,-50.2},{50.2,-50.2},{50.2,149.8}},
					color={0,0,255},
					thickness=8)}),
		Diagram(coordinateSystem(extent={{-100,-100},{160,160}})),
		Documentation(
			info="<html>
<p>Basic test of model
<a href=\"modelica://Buildings.Fluid.FixedResistances.PlugFlowPipe\">
Buildings.Fluid.FixedResistances.PlugFlowPipe</a>.
This test includes an inlet temperature step under a constant mass flow rate.
</p>
</html>",
			revisions="<html>
<ul>
<li>September 8, 2017 by Bram van der Heijde<br/>First implementation</li>
</ul>
</html>"),
		experiment(
			StopTime=1,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.001,
			__esi_Solver(
				bEffJac=false,
				bSparseMat=true,
				typename="CVODE"),
			__esi_SolverOptions(
				solver="CVODE",
				typename="ExternalCVODEOptionData"),
			__esi_MinInterval="9.999999999999999e-10",
			__esi_AbsTolerance="1e-6"));
end New_Grid_0B;
