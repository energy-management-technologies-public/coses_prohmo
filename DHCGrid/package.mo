﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo;
package DHCGrid "bidirectional district heating and cooling grid"
	annotation(dateModified="2023-05-08 13:45:54Z");
end DHCGrid;
