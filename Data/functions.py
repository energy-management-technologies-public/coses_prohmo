import pandas as pd
import json
from datetime import datetime

def generators(fname, tabs):
    if fname:
        filename = 'Generators\\' + fname + '.xlsx'

        output = '#1\n'
        for t in tabs:
            df = pd.read_excel(filename, sheet_name=t, index_col=0).round(3)
            output += df.iloc[:df.index.get_loc('x'), :df.columns.get_loc('x')].to_string(index=False, header=False
                                                                                          ).replace('NaN', '')
            output += '\n\n'

        text_file = open('Generators\\' + fname + '.txt', 'w')
        text_file.write(output)
        text_file.close()


def weather(year, t_res, location='MIMO01'):
    if year:
        # Link (general link + required data + station + time horizon):
        # general link:
        #   https://www.meteo.physik.uni-muenchen.de/request-beta/data/
        # + required data:
        #   var=air_temperature_2m+downwelling_shortwave_flux+diffuse_downwelling_flux+wind_speed_30m+
        #       wind_from_direction_30m+relative_humidity_2m
        # + station 'MIM01': Munich city, 'MIM03': Garching:
        #   &station=MIM03
        # + time horizon:
        #   &start=2022-01-01T00-00-00&end=2022-12-31T23-59-59

        data = json.load(open('Weather\\' + year + '_' + location + '.json'))

        df = pd.DataFrame()
        df['unix_time'] = data['time']
        df['t_air'] = data[location]['air_temperature_2m']
        df['t_air'] = df['t_air'] - 273.15  # [°C]
        df['rad_abs'] = data[location]['downwelling_shortwave_flux']  # [W/m²]
        df['rad_dif'] = data[location]['diffuse_downwelling_flux']  # [W/m²]
        df['rad_dir'] = df['rad_abs'] - df['rad_dif']  # [W/m²]
        df['rel_hum'] = data[location]['relative_humidity_2m']  # [%]
        df['wind_speed'] = data[location]['wind_speed_30m']  # [m/s]
        df['wind_dir'] = data[location]['wind_from_direction_30m']  # [-]
        df['datetime'] = pd.to_datetime(df['unix_time'], unit='s')
        df.set_index('datetime', inplace=True)
        unix_time = df['unix_time']

        df = df[df['t_air'] < 50]  # remove columns with wrong temperature
        df = df[df['rad_abs'] < 1400]  # remove columns with wrong irradiation
        df = df[df['rad_dir'] >= 0]  # remove columns with wrong irradiation

        df_avg = df.resample(t_res + 'T').mean().round(1)
        df_avg = df_avg.fillna(method='ffill')
        df_avg['wind_dir'] = df_avg['wind_dir'].astype('int')
        df_avg['unix_time'] = unix_time.loc[df_avg.index]
        daily_average_df = df_avg['t_air'].resample('D').mean().round(1)

        df_avg = df_avg.merge(daily_average_df, left_on=df_avg.index.date, right_on=daily_average_df.index.date,
                              suffixes=('', '_daily_average'))
        df_avg = df_avg[
            ['unix_time', 't_air', 't_air_daily_average', 'rad_dir', 'rad_dif', 'wind_speed', 'wind_dir', 'rel_hum']]

        weather_str = ('#1\n' +
                       'double T_StartEnd (1,3)\n' +
                       '0\t' + str(df_avg.iloc[0, 0]) + '\t' + str(df_avg.iloc[-1, 0]) + '\n\n' +
                       'double Weather (' + str(len(df_avg)) + ',8)\n' +
                       df_avg.to_string(index=False, header=False))

        text_file = open('Weather\\Weather_' + year + '_Munich_' + t_res + 'min.txt', 'w')
        text_file.write(weather_str)
        text_file.close()


def elprice(year, file, p_avg_buy, p_avg_sell, file_type='csv'):
    # Electric price data from smard.de
    if year:
        df = pd.read_csv(file, delimiter=';')
        format_string = "%d.%m.%Y %H:%M"
        for i in df.index:
            date = datetime.strptime(df.loc[i, 'Datum'] + ' ' + df.loc[i, 'Anfang'], format_string)
            df.loc[i, 'unixtime'] = date.timestamp()
        df = df.set_index('unixtime')

        if year == '2021':
            start = 1608760800
            end = 1641002400
        elif year == '2022':
            start = 1640296800
            end = 1672538400
        else:
            start = 0
            end = 0

        price = df[start:end].iloc[:, 4].str.replace(',', '.').astype(float) / 1000
        p_avg = price.mean()

        delta_buy = p_avg_buy - p_avg
        if p_avg_sell == 0:
            delta_sell = 0
        else:
            delta_sell = p_avg_sell - p_avg

        buy = (price + delta_buy).round(3).rename("Price buy [EUR/kWh]")
        sell = (price + delta_sell).round(3).rename("Price sell [EUR/kWh]")

        df_price = pd.concat([buy, sell], axis=1)
        df_price.index = df_price.index.astype(int)

        if file_type == 'csv':
            df_price.to_csv('Prices\\Prices_' + year + '.csv')
        else:
            price_str = ('#1\n' +
                           'double T_StartEnd (1,3)\n' +
                           '0\t' + str(start) + '\t' + str(end) + '\n\n' +
                           'double Weather (' + str(len(df_price)) + ',8)\n' +
                           df_price.to_string(index=False, header=False))

            text_file = open('Prices\\Prices_' + year + '.txt', 'w')
            text_file.write(price_str)
            text_file.close()
