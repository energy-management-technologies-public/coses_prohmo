import pandas as pd
import openpyxl
import functions

# generator
gen_type = ''

# weather
year_weather = ''
time_resolution = '15'  # time resolution of output data in minutes

if gen_type == 'ASHP':
    filename = 'CHA10'
    tabs = ['EfficiencyHeat_TSource', 'EfficiencyHeat_Modulation', 'EfficiencyCold_TSource',
                'EfficiencyCold_Modulation', 'StartUpHeat', 'CoolDownHeat', 'DeIcing', 'StartUpCold', 'CoolDownCold']
elif gen_type == 'BHP':
    filename = 'Ratiotherm WPGridF14'
    tabs = ['EfficiencyHeat_TSource', 'EfficiencyHeat_Modulation', 'EfficiencyCold_TSource',
                'EfficiencyCold_Modulation', 'StartUpHeat', 'CoolDownHeat', 'StartUpCold', 'CoolDownCold']
elif gen_type == 'GSHP':
    filename = 'BWS1_10'
    tabs = ['EfficiencyHeat', 'StartUpHeat', 'CoolDownHeat']
elif gen_type == 'CB':
    filename = 'WolfCGB14'
    tabs = ['Efficiency', 'StartUp', 'CoolDown']
elif gen_type == 'CHP':
    filename = 'NeoTower2'
    tabs = ['Efficiency', 'StartUp', 'CoolDown']
else:
    filename = ''
    tabs = ['']

# el. prices
year_price = ''
file_price = ''
p_avg_buy = 0.3  # average price for buying [€ / kWh]
p_avg_sell = 0  # average price for selling [€ / kWh], if 0: use market price

functions.generators(filename, tabs)

functions.weather(year_weather, time_resolution)

functions.elprice(year_price, file_price, p_avg_buy, p_avg_sell)
