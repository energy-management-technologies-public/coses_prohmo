﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo.CodeExport;
package VeriStandSiL "Software in the Loop models for VeriStand"
	import CoSES_Models.CoSES_VeriStand.*;
	annotation(dateModified="2022-02-15 08:30:30Z");
end VeriStandSiL;
