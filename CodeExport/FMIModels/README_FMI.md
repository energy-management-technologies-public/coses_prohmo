# FMI Code Export for Benchmarking Energy Management Systems

This folder contains FMUs to benchmark Energy Management Systems (EMS). The Python code template demonstrates the communication between EMS and FMU. **To install the environment, use conda and the "FMIModels.yml" file.**
An example of the integration of these FMUs into a Gym framework can be found here: https://github.com/ULudo/DRL-Building-Energy-Ctr



# IMPORTANT
FMUs from the current version of SimulationX require the data files of the FMU at the source directory. Therefore, *COPY* the folder "Data" to "C:\Users\Public\Documents\SimulationX 4.2\Modelica\CoSES_ProHMo\Data".
Sorry for the inconvenience, we hope to prevent the error with the next version.

