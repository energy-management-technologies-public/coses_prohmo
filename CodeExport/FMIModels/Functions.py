import os
import sys
from pyfmi import load_fmu
import pandas as pd
from datetime import datetime, timedelta
import time

# from datetime import datetime


def call_ems(input_timeseries, parameters, t_start, state, setpoints, comptime):
    # replace this by individual code
    start = time.time()
    for i in range(t_start, t_start + parameters['ems_output_horizon'] * 60, parameters['dt']):
        setpoints.loc[i, :] = 0
    end = time.time()
    comptime += end - start
    return setpoints, comptime


def call_fmi(parameters, state, log=False, print_log=False):
    # Model type:
    # - model exchange (ME): model exchange uses a python solver, which in the case of PyFMI is Assimulo
    # - co-simulation (CS): use the solver included in the FMU
    fmu_model_type = 'CS'

    # The log is created next to this file: 0: log nothing, 2: log error messages (default), 7: log everything
    log_level = 2

    # load FMI model
    model = load_fmu(parameters['model_name'], kind=fmu_model_type, log_level=log_level)

    # resets the FMU pack to its original state (the environment must be reinitialized after this call)
    # model.reset()

    # set parameters
    model = set_parameters(model, parameters)

    if print_log:
        model.get_log()

    # initializes the model and computes initial values for all variables
    opts = model.simulate_options()
    opts['ncp'] = 1
    if not log:
        # Redirect standard output to a null device
        sys.stdout = open(os.devnull, 'w')
    res = model.simulate(start_time=0, final_time=0, options=opts)
    sys.stdout = sys.__stdout__
    if parameters['model_name'] == 'House1.fmu':
        state['SOC_HW'] = ''
    col = get_columns(parameters)
    results = get_state(res, parameters)
    state = pd.Series(results[0][1:(len(state.index) + 1)], index=col[1:(len(state.index) + 1)])
    return model, state, results, col


def load_input_timeseries(input_timeseries, parameters):
    # load prediction data
    path = 'C:\\Users\\Public\\Documents\\SimulationX 4.2\\Modelica\\CoSES_ProHMo\\Data\\'

    cons_path = 'Consumption\\Simplified\\SF' + parameters['model_name'][-5] + '_' + str(parameters['year']) + '.txt'
    data_lines = read_data(path + cons_path)

    load = []
    for line in data_lines:
        parts = line.split()  # data is tab-separated
        # Getting the values
        time, p_el, p_heat, p_dhw, pv_norm = map(float, parts)
        p_cold = 0 if p_heat > 0 else -p_heat
        p_heat = p_heat if p_heat > 0 else 0
        load.append([time, pv_norm, p_el, p_heat, p_dhw, p_cold])
    load_df = pd.DataFrame(load, columns=['time', 'PV_norm', 'P_elDem', 'P_heatDem', 'P_DHWDem', 'P_coolingDem']
                           ).set_index('time')

    if parameters['weather_file']:
        weather_path = parameters['weather_file']
    else:
        weather_path = path + 'Weather\\Weather_' + str(parameters['year']) + '_Munich_15min.txt'
    data_lines = read_data(weather_path)

    t_air = []
    for line in data_lines[1:]:
        time, temperature = map(float, line.split()[0:2])
        t_air.append([time, temperature])
    t_air_df = pd.DataFrame(t_air, columns=['time', 'T_air']).set_index('time')

    input_timeseries = pd.concat([load_df, t_air_df], axis=1)

    if parameters['el_tariff'] == 'constant':
        input_timeseries['Price_elBuy'] = parameters['p_el_purchase']
        input_timeseries['Price_elSell'] = parameters['p_el_feedin']
    else:
        if parameters['p_el_file']:
            price_path = parameters['p_el_file']
        else:
            price_path = path + 'Prices\\Prices_' + str(parameters['year']) + '.txt'
        data_lines = read_data(price_path)

        price = []
        for line in data_lines:
            time, price_el = map(float, line.split()[0:2])
            price.append([time, price_el])
        price_df = pd.DataFrame(price, columns=['time', 'Price_elSell']).set_index('time')
        price_df['Price_elBuy'] = price_df['Price_elSell'] + parameters['p_el_grid']

        input_timeseries = pd.concat([load_df, price_df], axis=1).ffill()

    if parameters['year'] == 2020:
        input_timeseries['time'] = (input_timeseries.index - 1577833200) / 60
    elif parameters['year'] == 2021:
        input_timeseries['time'] = (input_timeseries.index - 1609455600) / 60
    elif parameters['year'] == 2022:
        input_timeseries['time'] = (input_timeseries.index - 1640991600) / 60
    input_timeseries = input_timeseries.set_index('time').sort_index().loc[((parameters['sim_period'][0] - 1) * 1440):
                                                                           (parameters['sim_period'][1] * 1440), :]

    return input_timeseries


def read_data(path):
    # Reading the consumption and pv file
    with open(path, 'r') as file:
        lines = file.readlines()

    # Skipping the non-data lines
    data_lines = [line for line in lines if line[0].isdigit()]
    return data_lines


def run_fmi(model, t_start, parameters, setpoints, state, results, col, simtime, log=True):
    print('Current simulation time: ' + f'{(t_start / 1440):.0f}' + ' / ' + str(parameters['sim_period'][1]) + ' d')
    if not log:
        # Redirect standard output to a null device
        sys.stdout = open(os.devnull, 'w')
    opts = model.simulate_options()
    opts['ncp'] = int(parameters['dt'] / parameters['dt_sim_res'])
    opts['initialize'] = False
    new_results = []
    for t in range(t_start, t_start + parameters['ems_output_horizon'] * 60, parameters['dt']):
        model = set_inputs(model, setpoints, t, parameters)
        res = model.simulate(start_time=t * 60, final_time=(t + parameters['dt']) * 60, options=opts)
        new_results.extend(get_state(res, parameters, opts['ncp']))
        simtime += res.detailed_timings['total']
    sys.stdout = sys.__stdout__
    state = pd.Series(new_results[-1][1:(len(state.index) + 1)], index=col[1:(len(state.index) + 1)])
    results.extend(new_results)
    return state, results, simtime


def postprocessing(parameters, simtime, comptime, results, col, input_timeseries, setpoints):
    res = pd.DataFrame(results, columns=col).set_index('time')
    dt = res.index[1] - res.index[0]
    dt_ts = input_timeseries.index[1] - input_timeseries.index[0]

    e_el_tot = res['P_el_purchase'].sum() * dt / 60 + res['P_el_feedin'].sum() * dt / 60
    pr_el_tot = 0
    discomf_heat = 0
    discomf_dhw = 0
    discomf_cooling = 0

    for t in res.index[1:]:
        pr_el_tot += (input_timeseries.loc[(int((t - 1) / dt_ts)) * dt_ts, 'Price_elBuy'] *
                      (res.loc[t, 'P_el_purchase']) * dt / 60)
        pr_el_tot += (input_timeseries.loc[(int((t - 1) / dt_ts)) * dt_ts, 'Price_elSell'] *
                      (res.loc[t, 'P_el_feedin']) * dt / 60)
        discomf_heat += max(0.0, t_set(t) - res.loc[t, 't_room']) * dt  # temperature below setpoint (20°C / 17°C)
        discomf_dhw += max(0.0, 45.0 - res.loc[t, 't_dhw']) * (res.loc[t, 'v_dhw'] - res.loc[t - dt, 'v_dhw']) * dt
        # temperature below setpoint (45°C) when there is flow at the tap
        discomf_cooling += max(0.0, res.loc[t, 't_room'] - 24.0) * dt  # temperature is above setpoint (24°C)
    e_gas_tot = 0

    result = pd.Series({
        'Simulation time': simtime,  # simulation time [s]
        'EMS computation time': comptime,  # computation time of the EMS [s]
        'E_el_tot': e_el_tot,  # total consumed electricity [kWh]
        'Price_el_tot': pr_el_tot,  # total costs of electricity [EUR]
        'E_gas_tot': 0,  # total consumed gas [kWh]
        'Price_gas_tot': 0  # total costs of gas
    })

    if parameters['model_name'] == 'House1.fmu' and parameters['use_chp']:
        result['CHP_start'] = res['CHP_start'].iloc[-1]         # number of starts of the CHP []
        result['CHP_runtime'] = res['CHP_runtime'].iloc[-1]     # total operation time of the CHP [h]
        e_gas_tot += res['CHP_P_gas'].sum() * dt / 60

    if parameters['model_name'] != 'House1.fmu' and parameters['use_hp']:
        result['HP_start'] = res['HP_start'].iloc[-1]       # number of starts of the heat pump []
        result['HP_runtime'] = res['HP_runtime'].iloc[-1]   # total operation time of the heat pump [h]

    if parameters['model_name'] != 'House3.fmu' and parameters['use_cb']:
        result['CB_start'] = res['CB_start'].iloc[-1]       # number of starts of the condensing boiler []
        result['CB_runtime'] = res['CB_runtime'].iloc[-1]   # total operation time of the condensing boiler [h]
        e_gas_tot += res['CB_P_gas'].sum() * dt / 60

    result[['E_gas_tot', 'Price_gas_tot']] = [e_gas_tot, e_gas_tot * parameters['p_gas_purchase']]

    if parameters['use_bat']:
        result['Bat_cycles'] = res['Bat_cycles'].iloc[-1]  # number of charging and discharging cycles of the battery []

    print('Simulation time: ' + f'{simtime:.0f}' + ' s\n'
          'Computation time (EMS): ' + f'{comptime:.0f}' + ' s\n'
          'Final results:\n'
          '\tTotal costs\t\t\t\t\t' + f'{pr_el_tot:.2f}' + ' EUR\n'
          '\tTotal energy use\t\t\t' + f'{e_el_tot:.0f}' + ' kWh\n'
          '\tdiscomfort (heating)\t\t' + f'{discomf_heat:.0f}' + ' K h\n'
          '\tdiscomfort (cooling)\t\t' + f'{discomf_cooling:.0f}' + ' K h\n'
          '\tdiscomfort (DHW)\t\t\t' + f'{discomf_dhw:.0f}' + ' K l')

    if parameters['model_name'] == 'House1.fmu' and parameters['use_chp']:
        optime = res['CHP_runtime'].iloc[-1] / 3600
        print('\tCHP (number of starts)\t\t' + str(result['CHP_start']) + '\n'
              '\tCHP (operation time)\t\t\t' + f'{optime:.1f}' + ' h')

    if parameters['model_name'] != 'House1.fmu' and parameters['use_hp']:
        optime = res['HP_runtime'].iloc[-1]
        print('\tHP (number of starts)\t\t' + str(result['HP_start']) + '\n'
              '\tHP (operation time)\t\t\t' + f'{optime:.1f}' + ' h')

    if parameters['model_name'] != 'House3.fmu' and parameters['use_cb']:
        optime = res['CB_runtime'].iloc[-1] / 3600
        print('\tCB (number of starts)\t\t' + str(result['CB_start']) + '\n'
              '\tCB (operation time)\t\t\t' + f'{optime:.1f}' + ' h')

    if parameters['use_bat']:
        print('\tBattery (number of cycles)\t' + str(result['Bat_cycles']))

    # write results
    with pd.ExcelWriter('Results//' + parameters['result_file'] + '.xlsx') as result_writer:
        pd.Series(parameters).to_excel(result_writer, 'Parameters', merge_cells=False)
        result.to_excel(result_writer, 'GenResults', merge_cells=False)
        res.to_excel(result_writer, 'SimResults', merge_cells=False)

    return result


def set_parameters(model, parameters):
    init_unix_time = (datetime(parameters['year'], 1, 1) + timedelta(days=parameters['sim_period'][0] - 1) -
                      timedelta(hours=parameters['timezone'] - 1)).timestamp()
    params = {
        'InitUnixTime': init_unix_time,  # [s]
        'MinimumControlEMS': parameters['min_control'],  # [bool]
        'dt_ems': parameters['dt'] * 60,  # [s]
        'TRefHeating': parameters['t_room_heating'] + 273.15,  # [K]
        'TRefCooling': parameters['t_room_cooling'] + 273.15,  # [K]
        'Tnight': parameters['t_night_time_red'] + 273.15,  # [K]
        'NightTimeReductionStart_h': parameters['time_night_start'],  # [h]
        'NightTimeReductionEnd_h': parameters['time_night_stop'],  # [h]
        'ActivateCooling': parameters['cooling'],  # [bool]
        'nPeople': parameters['number_inh'],  # [-]
        'AdditionalInsulation': parameters['AdditionalInsulation'] / 100,  # [m]
        'HeatedArea': parameters['building_size'],  # [m^2]
        'YearlyElecConsumption': parameters['e_el_year'],  # [kWh]
        'VStorage': parameters['v_ts'] / 1000,  # [m^3]
        'UseBat': parameters['use_bat'],  # [bool]
        'PMaxBat': parameters['p_bat'] * 1000,  # [W]
        'EMaxBat': parameters['e_bat'] * 3.6e6,  # [J]
        'UsePV': parameters['use_pv'],  # [bool]
        'PVPeak': parameters['p_pv'] * 1000,  # [W]
        'StandardControlTRoom': parameters['st_cont_temp'],
        'StandardControlBattery': parameters['st_cont_bat'],
        'LowLevelControl_Battery': parameters['ll_cont_bat']
    }
    if parameters['weather_file']:
        params['weatherfile'] = parameters['weather_file']  # [str]
    else:
        params['weatherfile'] = ('C:\\Users\\Public\\Documents\\SimulationX 4.2\\Modelica\\CoSES_ProHMo\\Data\\'
                                 + 'Weather\\Weather_Munich_15min.txt')  # [str]
        # params['weatherfile'] = (os.getcwd()[:-20] + 'Data\\Weather\\Weather_Munich_15min.txt')  # [str]
    if parameters['model_name'] != 'House1.fmu':
        params['UseHP'] = parameters['use_hp']  # [bool]
        params['PHeatNom'] = parameters['p_hp_heating'] * 1000  # [W]
        params['PColdNom'] = parameters['p_hp_cooling'] * 1000  # [W]
        params['PAuxMax'] = parameters['p_aux'] * 1000  # [W]
        params['COPNom'] = parameters['cop_hp']  # [-]
        params['EERNom'] = parameters['eer_hp']  # [-]
        params['StandardControlHP'] = parameters['st_cont_hp']
        params['StandardControlHP3WV'] = parameters['st_cont_hp3wv']
    if parameters['model_name'] == 'House1.fmu':
        params['UseCHP'] = parameters['use_chp']  # [bool]
        params['PelNom'] = parameters['p_chp'] * 1000  # [kW]
        params['StandardControlCHP'] = parameters['st_cont_chp']
    if parameters['model_name'] != 'House3.fmu':
        params['UseCB'] = parameters['use_cb']  # [bool]
        params['PCB'] = parameters['p_cb'] * 1000  # [kW]
        params['UseST'] = parameters['use_st']  # [bool]
        params['ST_CollectorSurface'] = parameters['a_st']  # [m^2]
        params['StandardControlCB'] = parameters['st_cont_cb']
        params['StandardContolST'] = parameters['use_st']
    model.set(list(params), list(params.values()))
    return model


def set_inputs(model, setpoints, t, parameters):
    inputs = ({
        'StandardControlTRoom': parameters['st_cont_temp'],
        'TRefHeatingIn': setpoints.loc[t, 'TRefHeating'],
        'StandardControlBattery': parameters['st_cont_bat'],
        'BatPCharge': setpoints.loc[t, 'Bat_p']
        })
    if parameters['cooling']:
        inputs['TRefCoolingIn'] = setpoints.loc[t, 'TRefCooling'],
    if parameters['model_name'] == 'House1.fmu':
        inputs['StandardControlCHP'] = parameters['st_cont_chp']
        inputs['Pel_CHP'] = setpoints.loc[t, 'CHP_p']
    if parameters['model_name'] != 'House3.fmu':
        inputs['StandardControlCB'] = parameters['st_cont_cb']
        inputs['CBModulation'] = setpoints.loc[t, 'CB_mod'] / 100
        inputs['StandardContolST'] = parameters['use_st']
    if parameters['model_name'] != 'House1.fmu':
        inputs['StandardControlHP'] = parameters['st_cont_hp']
        inputs['StandardControlHP3WV'] = parameters['st_cont_hp3wv']
        inputs['HPMode'] = True  # setpoints.loc[t, 'HP_mode']
        inputs['HP3WV'] = setpoints.loc[t, 'HP3WV'] / 100
        inputs['HPAuxModulation'] = setpoints.loc[t, 'HP_aux_mod'] / 100
        inputs['HPModulation'] = setpoints.loc[t, 'HP_mod'] / 100

    model.set(list(inputs), list(inputs.values()))
    return model


def get_columns(parameters):
    col = ['time', 't_sto', 't_room', 'soc_ts', 'soc_bat']

    if parameters['model_name'] == 'House1.fmu':
        col.append('soc_hws')

    col.extend(['P_el_feedin', 'P_el_purchase', 'PV_P_el', 'Dem_P_el', 'Dem_P_heat', 'Dem_P_cold', 'Dem_P_DHW'])

    if parameters['model_name'] == 'House1.fmu' and parameters['use_chp']:
        col.extend(['CHP_start', 'CHP_runtime', 'CHP_P_gas', 'CHP_P_el', 'CHP_P_heat'])

    if parameters['model_name'] != 'House1.fmu' and parameters['use_hp']:
        col.extend(['HP_start', 'HP_runtime', 'HP_P_el', 'HP_P_heat', 'HP_P_cold', 'HP_COP', 'SetpointHP',
                    'SetpointHP3WV', 'SetpointHPAux', 'ActSetpointHP', 'ActSetpointHP3WV', 'ActSetpointHPAux'])

    if parameters['model_name'] != 'House3.fmu' and parameters['use_cb']:
        col.extend(['CB_start', 'CB_runtime', 'CB_P_gas', 'CB_P_heat', 'ActSetpointCB'])

    if parameters['model_name'] != 'House3.fmu' and parameters['use_st']:
        col.append('ST_P_heat')

    if parameters['use_bat']:
        col.extend(['Bat_cycles', 'Bat_P_charge', 'Bat_P_discharge'])

    col.extend(['t_dhw', 'v_dhw'])
    return col


def get_state(res, parameters, ncp=1):
    results_ncp = []
    dt = parameters['dt'] / ncp / 60  # simulation time step size [h]
    for n in range(1, ncp + 1):
        t_sto = []
        for i in range(1, 11):
            t_sto.append(res['TS_S_TM_BT_' + str(i)][n] - 273.15)

        results = [
            res['time'][n] / 60,  # time [min]
            t_sto,  # storage temperature (of level 1 to 10) [°C]
            res['HS_S_TM_Room'][n] - 273.15,  # room temperature [°C]
            res['TS_SOC_BT'][n] * 100,  # state of charge of the thermal storage [%]
            res['SOC_Bat'][n] * 100  # state of charge of the battery [%]
            ]
        if parameters['model_name'] == 'House1.fmu':
            results.append(res['TS_SOC_HWS'][n] * 100)  # state of charge of the hot water storage (only SF1) [%]

        results.extend([
            (res['E_el_feedin'][n] - res['E_el_feedin'][n - 1]) / 3.6e6 / dt,  # total fed in electricity [kW]
            (res['E_el_purchase'][n] - res['E_el_purchase'][n - 1]) / 3.6e6 / dt,  # total consumed electricity [kW]
            (res['PV_E'][n] - res['PV_E'][n - 1]) / 3.6e6 / dt,  # total PV generation [kW]
            (res['HS_E_DemElec'][n] - res['HS_E_DemElec'][n - 1]) / 3.6e6 / dt,  # total electric demand [kW]
            (res['HS_E_DemHeatHC'][n] - res['HS_E_DemHeatHC'][n - 1]) / 3.6e6 / dt,  # total heat demand (heating) [kW]
            (res['HS_E_DemCold'][n] - res['HS_E_DemCold'][n - 1]) / 3.6e6 / dt,  # total cooling demand [kW]
            (res['HS_E_DemHeatHW'][n] - res['HS_E_DemHeatHW'][n - 1]) / 3.6e6 /dt  # total heat demand (DHW) [kW]
        ])

        if parameters['model_name'] == 'House1.fmu' and parameters['use_chp']:
            results.extend([
                res['CHP_starts'][n],  # number of starts of the CHP []
                res['CHP_runtime'][n] / 3600,  # total operation time of the CHP [h]
                (res['CHP_E_gas'][n] - res['CHP_E_gas'][n - 1]) / 3.6e6 / dt,  # total gas consumption CHP [kW]
                (res['CHP_E_el'][n] - res['CHP_E_el'][n - 1]) / 3.6e6 / dt,  # total electricity generation CHP [kW]
                (res['CHP_E_heat'][n] - res['CHP_E_heat'][n - 1]) / 3.6e6 / dt  # total heat generation CHP [kW]
            ])

        if parameters['model_name'] != 'House1.fmu' and parameters['use_hp']:
            results.extend([
                res['HP_starts'][n],  # number of starts of the heat pump []
                res['HP_runtime'][n] / 3600,  # total operation time of the heat pump [h]
                (res['HP_E_elec'][n] - res['HP_E_elec'][n - 1]) / 3.6e6 / dt,  # total electricity consumption HP [kW]
                (res['HP_E_heat_Tot'][n] - res['HP_E_heat_Tot'][n - 1]) / 3.6e6 / dt,  # total heat generation HP [kW]
                (res['HP_E_cooling'][n] - res['HP_E_cooling'][n - 1]) / 3.6e6 / dt,  # total cooling generation HP [kW]
                res['HP_COP'][n],  # COP [-]
                res['HPModulation'][n],  # setpoint of the heat pump [%]
                res['HPAuxModulation'][n],  # setpoint of the heat pump [%]
                res['HP3WV_out'][n],  # setpoint of the 3WV
                res['Setpoint_Act_HP'][n] * 100,  # actual setpoint of the heat pump [%]
                res['Setpoint_Act_HP_3WV'][n],  # actual setpoint of the 3WV
                res['Setpoint_Act_HP_Aux'][n] * 100  # actual setpoint of the aux. heater of the heat pump [%]
            ])

        if parameters['model_name'] != 'House3.fmu' and parameters['use_cb']:
            results.extend([
                res['CB_starts'][n],  # number of starts of the condensing boiler []
                res['CB_runtime'][n] / 3600,  # total operation time of the condensing boiler [h]
                (res['CB_E_gas'][n] - res['CB_E_gas'][n - 1]) / 3.6e6 / dt,  # total gas consumption CB [kW]
                (res['CB_E_heat'][n] - res['CB_E_heat'][n - 1]) / 3.6e6 / dt,  # total heat generation CB [kW]
                res['Setpoint_Act_CB'][n] * 100  # actual setpoint of the condensing boiler [%]
            ])

        if parameters['model_name'] != 'House3.fmu' and parameters['use_st']:
            results.append((res['ST_E_heat'][n] - res['ST_E_heat'][n - 1]) / 3.6e6 / dt)  # total ST heat gen. [kW]

        if parameters['use_bat']:
            results.extend([
                res['Bat_cylces'][n],  # number of charging and discharging cycles of the battery []
                (res['BatECharge'][n] - res['BatECharge'][n - 1]) / 3.6e6 / dt,  # total battery charging [kW]
                (res['BatEDischarge'][n] - res['BatEDischarge'][n - 1]) / 3.6e6 / dt  # total battery discharging [kW]
            ])

        results.extend([
            res['HS_S_TM_HW_VL'][n] - 273.15,  # DHW temperature [°C]
            res['HS_S_V_HW'][n] * 1000,  # DHW flow [l]
            ])

        results_ncp.append(results)

    return results_ncp


def t_set(t):
    if 6 <= int((t / 60) % 24) <= 23:
        return 20.0  # minimum temperature setpoint from 6:00 to 23:00
    else:
        return 17.0  # minimum temperature setpoint from 23:00 to 6:00
