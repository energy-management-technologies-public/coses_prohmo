from Functions import *
import pandas as pd

# Define Parameters
params = {
    'model_name': 'House2.fmu',
    'result_file': 'res_file',  # name of the result file
    # general EMS parameters
    'year': 2022,               # use 2021 for training and 2022 for benchmarking
    'timezone': 1,              # UTC timezone (Munich = +1)
    'min_control': True,        # use minimum control (can be switched off during training)
    'sim_period': [1, 1],       # simulation period [start day, end day] (1 = 01.01., 365 = 31.12.) [d]
    'dt': 15,                   # timestep size of the EMS [min]
    'ems_output_horizon': 24,   # time horizon for which the EMS generates setpoints [h]
    'ems_pred_horizon': 48,     # time horizon for which the EMS optimizes [h]
    'dt_sim_res': 5,            # time step size for plotting simulation results (must be a decimal of dt!) [min]
    'el_tariff': 'variable',    # 'constant' or 'variable' electricity tariff
    'p_el_purchase': 0.35,      # purchase price for electricity for fixed tariffs [EUR / kWh]
    'p_el_feedin': 0.10,        # feed in revenue for electricity for fixed tariffs [EUR / kWh]
    'p_el_grid': 0.20,          # grid tariff, only var. tariffs (p_el_purchase = p_el_feedin + p_el_grid) [EUR / kWh]
    'p_el_file': '',            # path to price file, leave empty for default (= spot market Germany)
    'p_gas_purchase': 0.10,     # purchase price for gas [EUR / kWh]
    'weather_file': '',         # path to weather file, leave empty for default (= Munich)
    # standard control
    'st_cont_temp': True,       # use standard control for the room temperature
    'st_cont_hp': True,         # use standard control for the heat pump
    'st_cont_hp3wv': True,      # use standard control for the 3 way valve of the heat pump
    'st_cont_chp': True,        # use standard control for the CHP
    'st_cont_cb': True,         # use standard control for the condensing boiler
    'st_cont_bat': True,        # use standard control for the battery
    'll_cont_bat': True,        # use low level control for the battery (battery runs so that p_bat = p_grid_set)
    # consumption parameters
    'cooling': False,           # house can be cooled (only house 2 and 3)
    'building_size': 300,       # heated living area of the building [m2]
    'number_inh': 6,            # number of inhabitants [-]
    'AdditionalInsulation': 0,  # Additional Insulation [cm]
    'e_el_year': 7000,          # yearly electricity consumption [kWh]
    't_room_heating': 21,       # set room temperature for heating [deg C]
    't_room_cooling': 24,       # set room temperature for cooling [deg C]
    't_night_time_red': 21,     # temperature during night time reduction (if not used, keep it at 21) [deg C]
    'time_night_start': 23,     # time, when set room temperature should be reduced [h]
    'time_night_stop': 6,       # time, when set room temperature should be back to normal [h]
    # component parameters
    'v_ts': 785,                # size of the thermal storage [l]
    'use_hp': True,             # use heat pump (only house 2 and 3)
    'p_hp_heating': 5.75,       # nominal heating power of the heat pump at A2/W35 (not maximum power!) [kW]
    'p_hp_cooling': 6.01,       # nominal cooling power of the heat pump at A35/W18 (not maximum power!) [kW]
    'p_aux': 9,                 # power of the electric auxiliary heater of the heat pump [kW]
    'cop_hp': 4.65,             # COP of the heat pump at A2/W35 [-]
    'eer_hp': 5.92,             # EER of the heat pump at A35/W18 [-]
    'use_chp': False,           # use CHP (only house 1)
    'p_chp': 2,                 # nominal electric power of the CHP [kW]
    'use_cb': False,            # use condensing boiler (only house 1 and 2)
    'p_cb': 14,                 # nominal thermal power of the condensing boiler [kW]
    'use_st': False,            # use solar thermal (only house 1 and 2)
    'a_st': 2,                  # size of the solar thermal collectors [m^2]
    'use_pv': True,             # use PV panels
    'p_pv': 10,                 # peak electric power of the PV system [kWp]
    'use_bat': False,           # use battery storage
    'e_bat': 10,                # size of the battery storage [kWh]
    'p_bat': 10                 # maximum charging power of the battery [kW]
    }

setpoints = pd.DataFrame({
    'TRefHeating': [],  # set value for room temperature during heating [deg C] (standard control: 21deg C)
    'TRefCooling': [],  # set value for room temperature during cooling [deg C] (standard control: 24deg C)
    'HP_mode': [],      # heat pump mode (0 = heating, 1 = cooling) [0 / 1]
    'HP_mod': [],       # set value for heat pump modulation [%]
    'HP_aux_mod': [],   # set value for auxiliary heater modulation of the heat pump [%]
    'HP3WV': [],        # set value for the 3way valve [%] (0%: only bottom, 50%: dt/2 bottom and dt/2 top, ...)
    'CHP_p': [],        # set value for the electric power of the CHP [kW_el]
    'CB_mod': [],       # set value for modulation of the condensing boiler [%]
    'Bat_p': []         # set value for the (dis)charging power of the battery (>0: charging, <0: discharging) [kW]
    })                  # index: time [min]

state = pd.Series({
    'TSto': [],         # storage temperature (of level 1 to 10) [deg C]
    'TRoom': [],        # room temperature [deg C]
    'SOC_TS': [],       # state of charge of the thermal storage [%]
    'SOC_Bat': []       # state of charge of the battery [%]
})

input_timeseries = pd.DataFrame({
    'T_air': [],         # air temperature [deg C]
    'PV_norm': [],       # normalized PV generation [kW/kWp]
    'P_elDem': [],       # electric demand [kW]
    'P_heatDem': [],     # heating demand [kW]
    'P_DHWDem': [],      # domestic hot water demand [kW]
    'P_coolingDem': [],  # cooling demand [kW]
    'Price_elSell': [],  # electricity price for selling [EUR / kWh]
    'Price_elBuy': [],   # electricity price for buying [EUR / kWh]
    'time': []           # index: time [min]
})

input_timeseries = load_input_timeseries(input_timeseries, params)
model, state, results, col = call_fmi(params, state, log=False)
simtime = 0
comptime = 0

for t_start in range(0, (params['sim_period'][1] - params['sim_period'][0] + 1) * 1440,
                     params['ems_output_horizon'] * 60):
    t_stop_pred = t_start + params['ems_pred_horizon'] * 60
    setpoints, comptime = call_ems(input_timeseries.loc[t_start:t_stop_pred, :], params, t_start, state, setpoints,
                                   comptime)
    state, results, simtime = run_fmi(model, t_start, params, setpoints, state, results, col, simtime, log=False)

result = postprocessing(params, simtime, comptime, results, col, input_timeseries, setpoints)
