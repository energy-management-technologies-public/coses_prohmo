﻿// CP: 65001
// SimulationX Version: 4.2.3.69223
within CoSES_ProHMo.CodeExport.VeriStandHiL;
model SimX_SF1_PHIL_Ore "Power Hardware in the Loop model for SF1"
	output SignalBlocks.OutputPin STM_HCVLaM_Set_degC "Set temperature Heating Circuit after the mixing valve" annotation(
		Placement(
			transformation(extent={{250,-95},{260,-85}}),
			iconTransformation(
				origin={171,-75},
				extent={{-19.3,-20},{20.7,20}},
				rotation=180)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin SFW_HCRLbM_Set_l_per_min "SFW_HCRLbM_Set_l_per_min" annotation(
		Placement(
			transformation(extent={{250,-65},{260,-55}}),
			iconTransformation(
				origin={171,-125},
				extent={{-19.3,-20},{20.7,20}},
				rotation=180)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin STM_HCRL_Set_degC "STM_HCRL_Set_degC" annotation(
		Placement(
			transformation(extent={{250,-145},{260,-135}}),
			iconTransformation(
				origin={171,-25},
				extent={{-19.3,-20},{20.7,20}},
				rotation=180)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin E_DHW_Set_kWh "E_DHW_kWh" annotation(
		Placement(
			transformation(extent={{250,-160},{260,-150}}),
			iconTransformation(
				origin={75,-172},
				extent={{-20,-19.7},{20,20.3}},
				rotation=90)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin P_elConsumption_kW "electric consumption of the house" annotation(
		Placement(
			transformation(extent={{250,-80},{260,-70}}),
			iconTransformation(
				origin={125,-172},
				extent={{-20,-19.7},{20,20.3}},
				rotation=90)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin T_cellarIs_degC "Cellar temperature" annotation(
		Placement(
			transformation(
				origin={255,-30},
				extent={{-5,5},{5,-5}},
				rotation=270),
			iconTransformation(
				origin={171,25},
				extent={{-19.3,-20},{20.7,20}},
				rotation=180)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin T_roomIs_degC "T_roomIs_degC" annotation(
		Placement(
			transformation(extent={{250,-50},{260,-40}}),
			iconTransformation(
				origin={171,75},
				extent={{-19.3,-20},{20.7,20}},
				rotation=180)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin T_roofIs_degC "Roof temperature" annotation(
		Placement(
			transformation(
				origin={255,-15},
				extent={{-5,5},{5,-5}},
				rotation=270),
			iconTransformation(
				origin={171,125},
				extent={{-19.3,-20},{20.7,20}},
				rotation=180)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin T_ambient_degC "Ambient temperature" annotation(
		Placement(
			transformation(extent={{250,30},{260,40}}),
			iconTransformation(
				origin={-75,175},
				extent={{-20,-20},{20,20}},
				rotation=270)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin Modeltime_s "Model time in seconds" annotation(
		Placement(
			transformation(extent={{250,45},{260,55}}),
			iconTransformation(
				origin={-125,175},
				extent={{-20,-20},{20,20}},
				rotation=270)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin DirectRadiation_W_per_m2 "Direct radiation" annotation(
		Placement(
			transformation(
				origin={255,20},
				extent={{-5,5},{5,-5}},
				rotation=270),
			iconTransformation(
				origin={25,175},
				extent={{-20,-20},{20,20}},
				rotation=270)),
		Dialog(
			tab="Output Signals",
			visible=false));
	output SignalBlocks.OutputPin DiffuseRadiation_W_per_m2 "Diffuse radiation" annotation(
		Placement(
			transformation(
				origin={255,5},
				extent={{-5,5},{5,-5}},
				rotation=270),
			iconTransformation(
				origin={-25,175},
				extent={{-20,-20},{20,20}},
				rotation=270)),
		Dialog(
			tab="Output Signals",
			visible=false));
	input SignalBlocks.InputPin STM_HCVLaM_degC "Temperature Heating Circuit after the mixing valve" annotation(
		Placement(
			transformation(extent={{-100,-25},{-90,-15}}),
			iconTransformation(extent={{-195,105},{-155,145}})),
		Dialog(
			tab="Input Signals",
			visible=false));
	input SignalBlocks.InputPin SFW_HCRLbM_l_per_min "Flow Water Heating Circuit before mixing valve" annotation(
		Placement(
			transformation(extent={{-100,-45},{-90,-35}}),
			iconTransformation(extent={{-195,55},{-155,95}})),
		Dialog(
			tab="Input Signals",
			visible=false));
	input SignalBlocks.InputPin STM_CCVL_degC "Temperature Cooling Circuit" annotation(
		Placement(
			transformation(extent={{-105,-180},{-95,-170}}),
			iconTransformation(extent={{-195,5},{-155,45}})),
		Dialog(
			tab="Input Signals",
			visible=false));
	GreenCity.Utilities.Electrical.Grid grid1(
		useA=true,
		useB=false,
		useC=false) "Electrical power grid for connection of maximum six 3-phase AC components" annotation(Placement(transformation(extent={{195,-120},{235,-80}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow defineVolumeFlow1 annotation(Placement(transformation(extent={{-10,-50},{10,-30}})));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow extractVolumeFlow1 annotation(Placement(transformation(
		origin={0,-65},
		extent={{-10,-10},{10,10}},
		rotation=-180)));
	Consumer.DHW_demand dHW_demand1(
		Load(tableID "External table object"),
		WeeklyData=false,
		File="C:\\Users\\gu62xur\\Downloads\\ProHMo_Orestis\\CodeExport\\VeriStandSiL\\Data\\SF1_WholeYear.txt",
		DHWfactor=1) annotation(Placement(transformation(extent={{70,-140},{90,-120}})));
	GreenCity.Interfaces.Thermal.DefineVolumeFlow defineVolumeFlow2 annotation(Placement(transformation(extent={{-10,-135},{10,-115}})));
	GreenCity.Interfaces.Thermal.ExtractVolumeFlow extractVolumeFlow2 annotation(Placement(transformation(
		origin={0,-150},
		extent={{-10,-10},{10,10}},
		rotation=-180)));
	Modelica.Blocks.Sources.RealExpression STM_HCVLaM_K(y=var_STM_HCVLaM_K) "Set output signal to a time varying Real expression" annotation(Placement(transformation(extent={{-50,-25},{-30,-5}})));
	Modelica.Blocks.Sources.RealExpression STM_CCVL_K(y=var_STM_CCVL_K) "Set output signal to a time varying Real expression" annotation(Placement(transformation(extent={{-50,-185},{-30,-165}})));
	Modelica.Blocks.Sources.RealExpression SFW_HCRLbM_m3_per_s(y=var_SFW_HCRLbM_m3_per_s) "Set output signal to a time varying Real expression" annotation(Placement(transformation(extent={{-50,-55},{-30,-35}})));
	Modelica.Blocks.Sources.RealExpression modeltime_s(y=time) annotation(Placement(transformation(extent={{90,45},{110,65}})));
	Real var_STM_CCVL_K "Variable" annotation(Dialog(
		tab="Input Signals",
		visible=false));
	Real var_STM_HCVLaM_K "Variable" annotation(Dialog(
		tab="Input Signals",
		visible=false));
	Real var_SFW_HCRLbM_m3_per_s "Variable" annotation(Dialog(
		tab="Input Signals",
		visible=false));
	parameter String File="C:\\Users\\Public\\Documents\\SimulationX 4.2\\Modelica\\CoSES ProHMo\\CodeExport\\VeriStandSiL\\Data\\SF1_WholeYear.txt" "File of all parameters of SF1 (weather, presence, Pel, DHW)" annotation(Dialog(tab="Parameters"));
	parameter Integer SF1_TypeDay=1 "1 - WSC, 2 - WSS, 3 - WWS, 4 - WWC, 5 - TWC, 6 - SSS, 7 - TWS, 8 - TSS, 9 - SWC, 10 - SSC, 11 - TSC, 12 - SWS, 13 - coldest" annotation(Dialog(tab="Parameters"));
	parameter Integer SF1_InitializationTime_h=1 "Additional time before the experiment in order to reach initial conditions for the experiment" annotation(Dialog(tab="Parameters"));
	parameter Integer InitTime=24-SF1_InitializationTime_h "Initial time of the day" annotation(Dialog(tab="Parameters"));
	parameter Integer InitDay=integer(mod(SF1_TypeDay*3-2, 31)) "InitialDay" annotation(Dialog(tab="Parameters"));
	parameter Integer InitMonth=integer((SF1_TypeDay*3-2)/31)+1 "Initial Month" annotation(Dialog(tab="Parameters"));
	parameter Real TLiving_Init=if SF1_TypeDay == 1 then 21
	elseif SF1_TypeDay == 2 then 21
	elseif SF1_TypeDay == 3 then 21
	elseif SF1_TypeDay == 4 then 21
	elseif SF1_TypeDay == 5 then 21
	elseif SF1_TypeDay == 6 then 21
	elseif SF1_TypeDay == 7 then 21
	elseif SF1_TypeDay == 8 then 21
	elseif SF1_TypeDay == 9 then 22.46
	elseif SF1_TypeDay == 10 then 23
	elseif SF1_TypeDay == 11 then 21.77
	elseif SF1_TypeDay == 12 then 22.99
	else 21 "Initial Temperature" annotation(Dialog(tab="Parameters"));
	parameter Real TRoof_Init=if SF1_TypeDay == 1 then 5.68
	elseif SF1_TypeDay == 2 then 4.52
	elseif SF1_TypeDay == 3 then 6.66
	elseif SF1_TypeDay == 4 then 6.11
	elseif SF1_TypeDay == 5 then 12.65
	elseif SF1_TypeDay == 6 then 14.17
	elseif SF1_TypeDay == 7 then 16.69
	elseif SF1_TypeDay == 8 then 11.78
	elseif SF1_TypeDay == 9 then 22.05
	elseif SF1_TypeDay == 10 then 22.88
	elseif SF1_TypeDay == 11 then 21.03
	elseif SF1_TypeDay == 12 then 23.83
	else 5.62 "Initial Temperature" annotation(Dialog(tab="Parameters"));
	parameter Real TCellar_Init=if SF1_TypeDay == 1 then 8.66
	elseif SF1_TypeDay == 2 then 8.42
	elseif SF1_TypeDay == 3 then 11.15
	elseif SF1_TypeDay == 4 then 10.56
	elseif SF1_TypeDay == 5 then 14.57
	elseif SF1_TypeDay == 6 then 11
	elseif SF1_TypeDay == 7 then 16.09
	elseif SF1_TypeDay == 8 then 9.78
	elseif SF1_TypeDay == 9 then 17.44
	elseif SF1_TypeDay == 10 then 16.61
	elseif SF1_TypeDay == 11 then 17.25
	elseif SF1_TypeDay == 12 then 18.15
	else 8.00 "Initial Temperature" annotation(Dialog(tab="Parameters"));
	parameter Real SF1_T_roomSet_degC=21 "T_roomSet_degC" annotation(Dialog(tab="Parameters"));
	parameter Real SF1_T_roomNightSet_degC=21 "T_roomNightSet_degC" annotation(Dialog(tab="Parameters"));
	parameter Real SF1_NightTimeReductionStart_h=23 "NightTimeReductionStart_h" annotation(Dialog(tab="Parameters"));
	parameter Real SF1_NightTimeReductionEnd_h=7 "NightTimeReductionEnd_h" annotation(Dialog(tab="Parameters"));
	parameter Integer SF1_HeatingSystem=0 "0 - Radiator Heating, 1 - Space Heating" annotation(Dialog(tab="Parameters"));
	parameter Integer SF1_nPeople=6 "nPeople" annotation(Dialog(tab="Parameters"));
	parameter Integer SF1_nFloors=2 "nFloors" annotation(Dialog(tab="Parameters"));
	parameter Integer SF1_nApartments=1 "nApartments" annotation(Dialog(tab="Parameters"));
	parameter Real SF1_HeatedArea_m2=300 "HeatedArea_m2" annotation(Dialog(tab="Parameters"));
	parameter Real SF1_YearlyElecConsumption_kWh=6500 "YearlyElecConsumption_kWh" annotation(Dialog(tab="Parameters"));
	parameter Real ElFactor=SF1_YearlyElecConsumption_kWh/5175 "ElFactor" annotation(Dialog(tab="Parameters"));
	parameter Real SF1_V_DHWperDay_l=300 "V_DHWperDay_l" annotation(Dialog(tab="Parameters"));
	parameter Real DHWFactor=SF1_V_DHWperDay_l/300 "Factor, with which the DHW demand gets multiplied" annotation(Dialog(tab="Parameters"));
	parameter Integer SF1_ST_CollectorType=0 "0 - Flat Plate Collector, 1 - Compound Parabolic Collector" annotation(Dialog(tab="Parameters"));
	parameter Real SF1_ST_CollectorSurface_m2=2 "ST_CollectorSurface_m2" annotation(Dialog(tab="Parameters"));
	parameter Real SF1_ST_CollectorVolume_m3=1.7 "ST_CollectorVolume_m3" annotation(Dialog(tab="Parameters"));
	Consumer.SimpleHeatingCoolingBuilding simpleHeatedBuilding1(
		cellar(
			CoolLoadFactorPerson(tableID "External table object"),
			CoolLoadFactorLigth(tableID "External table object"),
			CoolLoadFactorMachine(tableID "External table object"),
			AppliedLoadFactorLight(tableID "External table object"),
			AppliedLoadFactorMachine(tableID "External table object"),
			PelDIN(tableID "External table object"),
			NumberPersonDIN(tableID "External table object"),
			NumberPerson(tableID "External table object"),
			ElectricalPower(tableID "External table object"),
			ReactivePower(tableID "External table object"),
			BaseLoad(tableID "External table object"),
			NormLoad(tableID "External table object"),
			MachineLoad(tableID "External table object"),
			LightLoad(tableID "External table object"),
			InnerLoad(tableID "External table object")),
		livingZone(
			CoolLoadFactorPerson(tableID "External table object"),
			CoolLoadFactorLight(tableID "External table object"),
			CoolLoadFactorMachine(tableID "External table object"),
			AppliedLoadFactorLight(tableID "External table object"),
			AppliedLoadFactorMachine(tableID "External table object"),
			PelDIN(tableID "External table object"),
			NumberPersonDIN(tableID "External table object"),
			NumberPerson(tableID "External table object"),
			ElectricalPower(tableID "External table object"),
			ReactivePower(tableID "External table object"),
			BaseLoad(tableID "External table object"),
			NormLoad(tableID "External table object"),
			MachineLoad(tableID "External table object"),
			LightLoad(tableID "External table object"),
			InnerLoad(tableID "External table object")),
		roof(
			CoolLoadFactorPerson(tableID "External table object"),
			CoolLoadFactorLigth(tableID "External table object"),
			CoolLoadFactorMachine(tableID "External table object"),
			AppliedLoadFactorLight(tableID "External table object"),
			AppliedLoadFactorMachine(tableID "External table object"),
			PelDIN(tableID "External table object"),
			NumberPersonDIN(tableID "External table object"),
			NumberPerson(tableID "External table object"),
			ElectricalPower(tableID "External table object"),
			ReactivePower(tableID "External table object"),
			BaseLoad(tableID "External table object"),
			NormLoad(tableID "External table object"),
			MachineLoad(tableID "External table object"),
			LightLoad(tableID "External table object"),
			InnerLoad(tableID "External table object")),
		PelTable(tableID "External table object"),
		presenceTable(tableID "External table object"),
		heatingAndCoolingSystem1(volumeFlowControllerBuildingHC_DZ1(deltaTCoolBound=2)),
		redeclare replaceable parameter GreenCity.Utilities.BuildingData.ParameterSelectionSCB.AgeOfBuilding.To2001 buildingAge,
		nFloors=1,
		nAp=1,
		nPeople=5,
		ALH=170,
		livingTZoneInit=294.15,
		qvMaxLivingZone=0.0001666666666666667,
		TRefCooling=296.15,
		TFlowCooling=289.15,
		roofTZoneInit=294.15,
		cellarTZoneInit=294.15,
		newWindows=false) annotation(Placement(transformation(extent={{80,-75},{120,-35}})));
	Environment.Environment environment1(
		WeatherData(tableID "External table object"),
		InputFile="C:\\Users\\gu62xur\\Downloads\\ProHMo_Orestis\\Data\\Weather_2021_Munich_15min.txt",
		DayTimeInit=10,
		MonthDayInit=02,
		MonthInit=3,
		Init(tableID "External table object")) annotation(Placement(transformation(extent={{140,-30},{170,0}})));
	Modelica.Blocks.Sources.BooleanExpression booleanExpression1(y=true) annotation(Placement(transformation(extent={{165,-140},{145,-120}})));
	equation
		// enter your equations here
		// Heating
		STM_HCRL_Set_degC = extractVolumeFlow1.TMedium - 273.15;
		SFW_HCRLbM_Set_l_per_min = simpleHeatedBuilding1.qv_RefHeating * 60000;
		P_elConsumption_kW = simpleHeatedBuilding1.Pel / 1000;
		STM_HCVLaM_Set_degC = simpleHeatedBuilding1.T_Ref - 273.15;
		T_roofIs_degC = simpleHeatedBuilding1.TZone[3] - 273.15;
		T_roomIs_degC = simpleHeatedBuilding1.TZone[2] - 273.15;
		T_cellarIs_degC = simpleHeatedBuilding1.TZone[1] - 273.15;
		
		// DHW
		E_DHW_Set_kWh = dHW_demand1.E_DHW / 3600000;
		
		
		// weather
		Modeltime_s = modeltime_s.y;
		T_ambient_degC = environment1.TAmbient - 273.15;
		DirectRadiation_W_per_m2 = environment1.RadiationDirect;
		DiffuseRadiation_W_per_m2 = environment1.RadiationDiffuse;
		
		// inputs
		var_STM_CCVL_K = STM_CCVL_degC +  273.15;
		var_SFW_HCRLbM_m3_per_s = SFW_HCRLbM_l_per_min / 60000;
		var_STM_HCVLaM_K = STM_HCVLaM_degC + 273.15;
	equation
		connect(extractVolumeFlow2.Pipe,dHW_demand1.ReturnHotWater) annotation(Line(
			points={{10,-150},{15,-150},{65,-150},{65,-135},{70,-135}},
			color={190,30,45},
			thickness=0.0625));
		connect(defineVolumeFlow2.Pipe,dHW_demand1.FlowHotWater) annotation(Line(
			points={{10,-125},{15,-125},{65,-125},{70,-125}},
			color={190,30,45}));
		connect(dHW_demand1.TRef,dHW_demand1.TPipe) annotation(
			Line(
				points={{75,-120},{75,-110},{85,-110},{85,-120}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(dHW_demand1.TRef,defineVolumeFlow2.TMedium) annotation(
			Line(
				points={{75,-120},{75,-110},{-15,-110},{-15,-120},{-10,-120}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(dHW_demand1.qv_DHW,defineVolumeFlow2.qvMedium) annotation(
			Line(
				points={{80,-120},{80,-105},{-20,-105},{-20,-130},{-10,-130}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(STM_CCVL_K.y,dHW_demand1.TColdWater) annotation(Line(
			points={{-29,-175},{-24,-175},{85,-175},{85,-144.7},{85,-139.7}},
			color={0,0,127},
			thickness=0.0625));
		connect(STM_HCVLaM_K.y,defineVolumeFlow1.TMedium) annotation(Line(
			points={{-29,-15},{-24,-15},{-15,-15},{-15,-35},{-10,-35}},
			color={0,0,127},
			thickness=0.0625));
		connect(SFW_HCRLbM_m3_per_s.y,defineVolumeFlow1.qvMedium) annotation(Line(
			points={{-29,-45},{-24,-45},{-15,-45},{-10,-45}},
			color={0,0,127},
			thickness=0.0625));
		connect(extractVolumeFlow1.Pipe,simpleHeatedBuilding1.PipeOut) annotation(Line(
			points={{10,-65},{15,-65},{75,-65},{75,-60},{80,-60}},
			color={190,30,45},
			thickness=0.0625));
		connect(defineVolumeFlow1.Pipe,simpleHeatedBuilding1.PipeIn) annotation(Line(
			points={{10,-40},{15,-40},{75,-40},{75,-45},{80,-45}},
			color={190,30,45}));
		connect(environment1.UnixTime,simpleHeatedBuilding1.UnixTime) annotation(Line(
			points={{140,-20},{135,-20},{110,-20},{110,-30},{110,-35}},
			color={0,0,127},
			thickness=0.0625));
		connect(environment1.EnvironmentConditions,simpleHeatedBuilding1.EnvironmentConditions) annotation(Line(
			points={{140,-10},{135,-10},{105,-10},{105,-30},{105,-35}},
			color={192,192,192},
			thickness=0.0625));
		connect(dHW_demand1.EnvironmentConditions,environment1.EnvironmentConditions) annotation(Line(
			points={{89.7,-125},{94.7,-125},{135,-125},{135,-10},{140,-10}},
			color={192,192,192},
			thickness=0.0625));
		connect(dHW_demand1.UnixTime,environment1.UnixTime) annotation(
			Line(
				points={{89.66667175292969,-135},{94.7,-135},{130,-135},{130,-20},{140,-20}},
				color={0,0,127},
				thickness=0.0625),
			__esi_AutoRoute=false);
		connect(booleanExpression1.y,dHW_demand1.DemandMode) annotation(Line(
			points={{144,-130},{139,-130},{94.7,-130},{89.7,-130}},
			color={255,0,255},
			thickness=0.0625));
		connect(simpleHeatedBuilding1.Grid3,grid1.LVGridA) annotation(Line(
			points={{110,-75},{110,-80},{110,-85},{190,-85},{195,-85}},
			color={247,148,29},
			thickness=0.0625));
	annotation(
		__esi_viewinfo[0](
			staticBlocks[0](
				text="{\\\\rtf1\\\\ansi\\\\ansicpg1252\\\\deff0\\\\nouicompat\\\\deflang1031{\\\\fonttbl{\\\\f0\\\\fnil\\\\fcharset0 Tahoma;}}
{\\\\*\\\\generator Riched20 10.0.18362}\\\\viewkind4\\\\uc1 
\\\\pard\\\\f0\\\\fs20 Typeday:\\\\par
Winter - Transission - Summer\\\\par
Saturday/Sunday/Holiday - Weekday\\\\par
Cloudy - Sunny\\\\par
\\\\par
1 - WSS\\\\par
2 - WWS\\\\par
3 - WSC\\\\par
4 - WWC\\\\par
5 - TSS\\\\par
6 - TWS\\\\par
7 - TWC\\\\par
8 - TSC\\\\par
9 - SWC\\\\par
10 - SSC\\\\par
11 - SWS\\\\par
12 - SSS\\\\par
13 - coldest\\\\par
}
",
				border(
					left=5,
					top=5,
					right=5,
					bottom=5),
				position(
					left=285,
					top=30,
					right=510,
					bottom=345),
				frame(
					style=0,
					width=0,
					color=0),
				clrBack=14806254,
				index=0,
				typename="TextBlock"),
			staticBlocks[1](
				text="{\\\\rtf1\\\\ansi\\\\ansicpg1252\\\\deff0\\\\nouicompat\\\\deflang1031{\\\\fonttbl{\\\\f0\\\\fnil\\\\fcharset0 Tahoma;}}
{\\\\*\\\\generator Riched20 10.0.18362}\\\\viewkind4\\\\uc1 
\\\\pard\\\\f0\\\\fs20 HeatingSystem:\\\\par
0 - Radiator Heating\\\\par
1 - Space Heating\\\\par
}
",
				border(
					left=5,
					top=5,
					right=5,
					bottom=5),
				position(
					left=510,
					top=30,
					right=645,
					bottom=90),
				frame(
					style=0,
					width=0,
					color=0),
				clrBack=14806254,
				index=1,
				typename="TextBlock"),
			staticBlocks[2](
				text="{\\\\rtf1\\\\ansi\\\\ansicpg1252\\\\deff0\\\\nouicompat\\\\deflang1031{\\\\fonttbl{\\\\f0\\\\fnil\\\\fcharset0 Tahoma;}}
{\\\\*\\\\generator Riched20 10.0.18362}\\\\viewkind4\\\\uc1 
\\\\pard\\\\f0\\\\fs20 ST_CollectorType:\\\\par
0 - Flat Plate Collector\\\\par
1 - Compound Parabolic Collector\\\\par
}
",
				border(
					left=5,
					top=5,
					right=5,
					bottom=5),
				position(
					left=645,
					top=30,
					right=855,
					bottom=90),
				frame(
					style=0,
					width=0,
					color=0),
				clrBack=14806254,
				index=2,
				typename="TextBlock"),
			typename="ModelInfo"),
		__esi_viewinfo[1](
			fMin=1,
			fMax=100,
			nf=100,
			kindSweep=1,
			expDataFormat=0,
			expNumberFormat="%.7lg",
			expMatrixNames={
							"A","B","C","D","E"},
			typename="AnaLinSysInfo"),
		__esi_viewinfo[2](
			showAll=false,
			hideInfinity=true,
			fMin=0,
			fMax=1000000,
			TMin=0,
			TMax=1000000,
			animGain=-10,
			animFreq=0,
			formatAbsDev="%.4lf",
			formatArgDev="%.2lf",
			formatEnergy="%.4lf",
			minNormDev=0.0001,
			minNormEnergy=1e-06,
			minAbsEnergy=1e-12,
			scaleEnergy=0.01,
			typename="AnaEFEWInfo"),
		__esi_viewinfo[3](
			projectName="SimX_SF1_PHIL_Ore",
			projectPath="C:\\Users\\gu62xur\\Downloads\\SimX_Veristand",
			projectType=17,
			saveOutputsApproach=1,
			inputs[0](
				port="STM_HCVLaM_degC",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			inputs[1](
				port="SFW_HCRLbM_l_per_min",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			inputs[2](
				port="STM_CCVL_degC",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[0](
				port="STM_HCVLaM_Set_degC",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[1](
				port="SFW_HCRLbM_Set_l_per_min",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[2](
				port="STM_HCRL_Set_degC",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[3](
				port="E_DHW_Set_kWh",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[4](
				port="P_elConsumption_kW",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[5](
				port="T_cellarIs_degC",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[6](
				port="T_roomIs_degC",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[7](
				port="T_roofIs_degC",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[8](
				port="T_ambient_degC",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[9](
				port="Modeltime_s",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[10](
				port="DirectRadiation_W_per_m2",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			outputs[11](
				port="DiffuseRadiation_W_per_m2",
				interpolation=true,
				paramTypeSPCK=3,
				typename="CEPort"),
			showAdditionalLibPage=false,
			useCodeOptimization=false,
			m_x64=false,
			solverMode=1,
			typename="CodeExportInfo"),
		Icon(
			coordinateSystem(extent={{-175,-175},{175,175}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAOvAAADrwBlbxySQAAACVJREFUaEPtwTEBAAAAwqD1T+1nCiAAAAAAAAAAAAAAAAAAgKsBOHwA
AQdXZHsAAAAASUVORK5CYII=",
								extent={{-100,-100},{100,100}}),
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAALEAAACSCAIAAACFc7wKAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAScQAAEnEB89x6jgAAJZJJREFUeF7tnQdYFEcbgO/oUgSkSVN6t2AXFVGMAlGDoKjRKCpRY8HI
bwU1iNQoEkBBA6JIEaQqRUDa3dkLGmKLJRoTjYpKUXr7v70Zjn4ehHJ33PvMw7M7O3ssO+99883u
3kFq5MGjNTwnGt+8eVNSUoJXePCcOH369IwZMxYsWJCZmYmrBjwD14ny8nJ/f38NDQ0SiSQoKGhs
bBwbG9vQ0IA3D2AGqBOvXr3atGmTsrIymUy2sbGxsLDg4+PT09P7+eefS0tLcaOBykB0oqCgACSQ
lJQUExNbv359bm5uamqqra2tgICAkpKSg4PD8+fPcdMByYBzIi0tDYYJISEhVVXVAwcOXL58+Sqd
7OxsiBwyMjISEhKzZ8++ffs23mHgMbCcCAgI0NXVhWFCX18/LCwMVLhy5QqNDlrw9vaGUAHGTJgw
IT4+Hu82wBgoThQXFzs5OUECAUJAGEhISLh27RoECSQEAlYvXboUEhICgQTGEbDn8OHDdXV1+CUG
DAPCiYcPH65YsWLIkCHQ05AuZGVlgRDQ/diFFkAlBA+IEPPmzQN7YHxxdnZ+/fo1fqEmqopLH8Ql
5e5x/efKDVzFRXC/E3l5eVOmTIF0Ulpa2tXVlUqlwjDRoRAIpEV6erq9vb2IiAhkGIsXLy4sLMQv
19hYW1F5Pzo+YLi++2DJ0CmmD84m1dfU4m1cATc70dDQEBERYWBgAOEBEgh/f38ID4wEggmgBTSj
UCguLi6QXoAZU6dOhXr0sm/v3ou2WLCfPMiLJOMpKBWobXjd90jNp89oKxfAtU5UVlbCtGLYsGEw
BEyfPj0mJqaz8aIzQAvAz89PW1sbrBo1ahQYBp59/ONZ0rerXEmCXiRpb5KcB5/UYUW17G3OJS9e
4t/N4XCnE5ABrF69GsI+9KW1tXVmZmZXhUCgmWpUVJSJiQk/P//w4cPd3NzKP30quv8ozcHRU0DK
nTTYhyTnSZbylpBLWPjd6xsF+Ag4GS504saNG7NmzRIVFRUXF9+yZQskENCv3RCCAfgE6YWdnZ2g
oKCcnNyGDRue//lnyZ9/5Tu7/SyrdIAkCtHCizTEXVAywtzqRR6tkcOvj3ObEzBGGBoawntaRUXl
0KFDLa9AdBuUdebn52/duhVyVeDruXOvXrlaXVJaeDLqiLaRG2mQN0kWygF+iaBRY38Lj675VI4P
iAPhHidqamqOHj2qrq5OJpPHjBkTGRkJAeO/hIc2gFswlHh6esIIAs7Br0hOTq6vq3uenX9ysvkB
kpgnaQgEjAN84ocUhlF/8v787zt8ZJwGlzjx6tUrJycneXl5EMLKygp6q2eFQIAT8LJhYWETJkyA
X6SjoxMSGlJVVfWmoDDOeilo4UFknfIeJEkYR9IcNhc/e4GPj6PgBieePHmycOFCmDEKCQk5ODjk
5eVBz+Fu7GnAs5s3byYkJJibm5NIJEhjd+7cWVRUVPrynyzHXV6DZCDrBC08SdKgSPRs61fXbuGj
5Bw42wmYGcIwP3nyZJhwQgLh6uoKAz+khLgDewekRXZ29sqVKyUkJISFhZcuXfrHH39UFpfe8Av2
U9F0I4vRs04ZyDOOj5zwODm9vroGHzEnwMFOQAJx6tQpdE9LT08vMDAQuuq/Z5Qscv36dRhKduzY
ISsrCzNeMzOznJycmvKKBzGJx0aPO8AnBk6AGW4k0cNK6jd+Ca54/xEUxofO3nCkE/X19cXFxfv3
71dQUID+QJekbt26BZ2Ee6xPAP8gJvn6+oKRkHXCfCc6OhpM/YtyOXLWPA9BSRhBYByBQcRHUiFn
+97iP1801Nfjv4GN4Twn6urqHj58uG7dOmlpaYjbdnZ2GRkZECF6PKNkBbAQfnVERAR4KSgoqKmp
6efn9/bdu7eF91NWb/CRHupBliKudZIkD5DEE+1WvrlTWF/L7jdHOMyJ6upqSCBgZjFo0CAI2ps3
b87Nze3tBII5KL1ITEycP3++qKjokCFDHB0dIb0oe/Um39ntsKKaBx+hBQwl+0nCJ01nPbuQDUMM
O48jHOMEpJMVFRVxcXFjxoyBd6S+vr6XlxeK3rhz+hWUdf7www+KiopweIsWLYKaitKyO8dOBRmO
9hSUQunFfpJIoIbRvYjYyo/FDfVsqgVnOAEJxLt3744cOQIZJYwXpqamoaGhkOX1WUbJCijrhCwH
fIUsZ+rUqSkpKeDx0/SLp8y+8hIZgi9qkcQPSitfOnCw9MXfDWz5wA4HOFFbW/vkyZPt27fLycmJ
i4vDwBEbGwvvwj7OKFkBZsJwYAEBAePGjQN3dXR0wk6GQTr87+3f4m2W+0jIe5KJW6nupMHuAoNT
Vm14V3ifDdMLdneisrLyxo0by5YtgwQCwvKaNWv6MaNkBTgwmAHBBGTOnDliYmKqqqpubm7Pnj0r
evgky3HHYRU1D35pL5KcJ0kK5iMxXy/858r12opKtkov2NqJT58+nT9/fsaMGSIiIvCec3FxoVKp
EKLx6WdXUNaZnp4OKkMiLCUlBSpDTcnrN9cPHz1qNMpTEAYRWZR1/jrG5EFcUsX7j+yTXrCpE5BA
FBUVnTx5csSIERAhIBTDHA9sgOCMTzzbA0cLc6KtW7dqaWlB1mlhYXHx4sXy8vKHcclhJjO8hGVQ
egFZp5+K1rWDAWV/v2KTqxfs6AQkEE+fPoVkDQKvhIQEnM3w8HAYQZgnEJdpOXiJbUBZsI+PD8yV
IL2YNGnS2bi4jx8//pmVd8bKxkdiqCcZZ52eQkNydu57/+hxPRtknWznRFVVFYzHMKmTkZGBwLt0
6VIYPtrf5HxA9cRLdG7S4mooUniFnQAnYNQIDg6GuRIkyGpqar6+vn+9fPn2/qNUh82+CmqefETW
SU8vRBMWr3h17WZdVTU+F/0EezkBCQQE2G+++QbeVRByHR0dMzMz4ZziE9yCCqrqE8r/8AqNBsuN
+aRb1Ei8TqeYOqGQGoBX+o/Lly7dvn377Nmztra2ILqkpOS2bdsKCwtLXr+l7PEI0Nb3FICsE6UX
ImETpz9Jzagu+9SPWSe7ONHQ0PD+/fvIyMjJkydDRjl27Fh3d3c4oZ1llJ9p2pVURbxCo5XRDBto
/I+ornidRntD+7qBwtfvTly9cvnKZSLCgdlZWVkODg4qKioQMOzs7PLy8sref7gbGv7rxMleQjL0
i1qybiTRX1S07544/en1Gzgn+Oz0LWzhRF1d3cuXLyGLVFdXh/nbzJkzjx8/jgZjdGaha2F0QMuI
IppZI4WMRpCrtAt11EENv5GhEm39k7ap9qZEHU2sTeToe65fvQJaoOVr167BvMnZ2dnIyAgC4bRp
02BYLC0tfXrhYuTsuT7iCujqBcxRD8ko5+12+/DoSb9o0f9OQAJx584dCKfy8vIQWq2trc+cOQPB
tmUCcZd6rJoi07KD/6EubSwgVV+SBVdg4Ki7KdLwhFxLHQyZ5n2ad+3VwQ1/kuspgqALag/1/RIz
6HGiOTVGj3cEBASYmJjAfMrY2PjXkJBXr1+/unnn3PK1B2WViUfAiXtmEp58UknL17y+WVBbWYXP
VF/Rz05UVFTAhG3hwoUQTocPH7527VqY1nf4lNQ/tCX1FCGQA63+RVnVeIfU8Ae5kqYIg0jDIzJI
UHdL5C11Ts1lyYZn5Ian4IQwagwyVVKVX1JXoNX2UDMzk6JP+Pv7w4BFx9PTM+BIUFB4XEJaNm5D
kJuZHBmEm7BAwImY1Ja704HgB3/gqVOn5syZIyUlpaSktGfv3sePH394+jxnx15/NR2wAQYRmKm6
kUROzrD4t88/H9BvTkBULC4uTkpKmjVrFiQQo0aN2rNnD4VC6SyBACCHgBQBft6jHnpOWddQwA8e
NBSS628Kgh+NN0kQORpu8CE/QJdqKhFFXtHsIGCU0fTxq7QjI8rvp82blsweo6ysTMIICAioqmtp
TbG2XbvL+3hkAu7ZlAif1RNxExYwmu8YEI/2bAWEQAiE586dW7lyJaQXEDDWr18P8aO8uPRmwLFj
o8fTs07iMT4vxWFPktP7ON/sHycggfj777+PHj06evRoUVFRGFkDAwPhpDC/JHWddq6OKtZwn1x7
VbyOIlJ3XaTxFqnxCt0GiBm/kwkzrpMaL9HluE/EiTraIFAHnIB98au0JvHYnh++YqjQAUKikiNn
L//p13hCix5yAgFZZ05Ozo4dO3R0dISEhKysrNLS0iorKx/Fn48wtyIeyRGXprn61Hzu648F9IMT
tbW1jx492rlzp6qqqrS09Ny5c8PDw1tmlEz4lza//q4AEQbukhsvkyBIEMvtC2y9QoKfsAwDyp/U
DXj/NmRGrB2Nuo/Mxy+rZmRmBj0DWFjMnjrJUGMw2kYSklSbviYoBdonnfTahJogZkweMXwIbkWS
GKo5xhRvoPO98+HIVPyrOgTeBgAMVOPHjwctIL2IiIyA+dera7diFiy8sH5rxfsP+Kz1IX3tRHV1
9eXLl2FKBvMLRUXFVatWQe5969atNpekGEDwh9yQUWC2WU8VaXhAbrxGgoyhlQdtyh90LR7hxBO/
XGsyAhYp0XuTzCcwdMzq/UdScQ9SKLkJZ/z22I1QpG8mkcRUDOy8QYq2JATtsh2F25C0pi/3iMYb
WAalFyEhIbNnz4aQOWzYMB8fn+fPn79/9KT83ft+uUrRp05ARpmcnGxpaQnvCQiYTk5O2dnZHV6S
YvCO+lVjPqmByl97RQKVuiuiRIT4o50E7cs9cuNVUi1F/C3NAnJSVMpohn9Q96AXD12NRw2ygLDW
ujBUySDnQnLgjgWY5Q6Ov7SaDCP+uxMAumcGqZWtra0snQ0bNhQUFPTX16H0kRPoklRoaCiER5ia
T5w40dvbGwIGRE58YjoHwgPklbXXBqPAQOQK9EGBlQKpRsMdcsNv9FTjKgn0KqGNYVzqOPadHO5O
PgHhMTZO7h6B4fGJmZloKyv0iBMIiBYZGRkbN27U0NAQFBS0sbHJy8vrFy36won6+vpnz555eHhA
AgE5Ngy0J06cgFPASgLB4CHNrfqyTMNjPCK06ftOC9hwmxhlam+IQ4QooLYKBqlHl2ji/gQt+PiV
VUaazbdesWKjo+NWd/fA8PCkL/nRg04AcE6oVCpMYo2MjPj5+c3MzGJjYyG44vPYV/S6E2A6BEZI
INAlqcWLFycmJkJNZwkEE6BHqy7JNVJJxOWHNn3fWXlEpKK1tMFPqDvwq7Qiyn3+WE1Z3KfN8PML
qaqOMjOzXm6/xckzKDKpMzV61gkAZl6QbsMsDOZiEC1ghPXz8ysqKsJns0/oXSdACJhuzZs3DxII
LS2tLVu2ZGZmMrkCwQQI+B9ok+uuihPJRJuOZ1KekRsppJqrUp04QaOmBXtss/5mhpZsezMIBETE
1cfOsdtw8FgS3qMVPe4EAEMqelIL0hgR+pcn7dq16+HDh/ic9j696ARMtSMiIkxNTQUEBMaNG+fl
5dW9p6RuUSOLaGbElYbfiW4m4gTzGUfL0jRDgbGjkqr4D3Up40poM9TUlCivnTtXrbK3tTE3H6et
KSODexkjOEjaePba4ETcvgW94QQCQml6ejrMyyC+ontmUInPbC/TW068e/cOskhtbW0IgDAuQgIB
UZH5JakOeUudA4khFGLScU2cuPBwlQQ93bbvOyvQHnJSWACNYJlCvFQdRZRxH6QFVColLSUqKtjH
a9cuB4dV1vOmajS7MUhe9+vtEe2GkN5zAoAEPD8/f/fu3Xp6emQy2dzcPC0traam1z962itO3L9/
H/JnCHog+KJFi2JiYsD67j1mDUGCcXHiAdWzmDK+/ooA7mYWChEkbpNrL4nXU4Qhx4S5KLxI60yz
w7SGQslLTgh1c7Sapo47XHCo3szd7W6y9qoTAGgBmXhAQMCUKVNAiwkTJgQFBfX2f5boeSdgBmVt
bQ0TTjk5OUggLly4AELgP/E/g253ET3NSqiAwHCDVEUd2ma6gYk6AAmvnd1iu8VLvv1fUBqubQEl
O/n4vrlNsUJWa6xjCN7CoLedAMAJGHDPnDkzZ84cmLUNHz4c0osXL3rxmy160ona2loICZAw89G/
vHjfvn3wJ7FyBYJF4C1ODB/Q2U/oF7aZz0jv00eZZ+Tay+Id5BDAiTXKZDLRl2SyoOL43VG4ugXZ
yaddLCXp/U0SUtT7as8ZvIFBHzgBoItaycnJkF6gJ7VWrFgBE1d83nuaHnPi7du3hw8fhok1hDjI
KyHEgeDdSCA6AzKAKppcw2NiegkBoP6KEGhBDCLt56VgzE3iZhgsEKuPydXUIR1c3s4I/3403Qmw
gl9Qx3SzW2BkGg4XFEpu4tljbpvsRuKr22JDDWz2nr2ItjbTN04gQIKLFy86OzurqKjAPM7S0jIl
JQWf/R6lZ5yAULZp0yY4VhgybGxsYmNjITx0L4HoDOJG+RV+mH3A8PGaattA4SdsQGkjyHGDuDWK
b4rSiHukDfeIZ2qIZ20ekuvvC5bSjNprkRBk19ShZD5+GfURk2bOnEvHysrCdMpoTUV8F0xMTmu+
U3iv3O/oEug9Bsk7+tDsxIkTIXn/9OkT7oYeogecgO6HmdLgwYMhrDk4OGRkZMD4141LUkx4Q7Es
o+o/o2xGl6UhP4C0EWJAXYFwFU2hjiLWUEhcva6/KVh3RxhHi0JyMXXCE8r/yqnDQY4GqsAnml47
LRKDnX8wH83sXjlJSFR2hMXmn053eIOzj50A4J0GARhUgPSCn59fV1fXw8Pj1atXuDN6gv/qRHx8
PMyRIJSpq6vv3bs3Ozu7e5ekugTxsN0N4drrEq9pttDNL6gO9QVC6CGaD7Qpdb+JICcYj2fCuAO5
CESXjh7PzIw87rt34wa7r4yVlNBdUgIBAQE1NS0TU5vV29x9jkW0f1wK0fdOAKAFBAw48wsXLoTc
QllZee3atffu3cNd8p/pvhMVFRXHjh1D/9Zg/PjxR44cQd9Oig+8N3lNs26k8DFub0LwIK5o3SX/
Q10Kq1iLFk58EWpmZmJUiJ+fn1sTxJNzAUdOnY7vzAZEdurZMH+8i9vh4PDELtw/+y9AGIbwnJ6e
7ujoCEM2BGkYsikUCu6b/0Y3nYAEwsXFRUtLC8IXDL/R0dEQ0Ho2gWDCZ6rWC+o6vEIHIkT9JSH0
GDdQRjOENJN1JzgR0ALegTk5OTB2aGtrwzR1xowZMGWF2R/upO7SHScgAf7uu+/k5eUlJCTs7e1h
jgTO9mwCwZwqytA2FyKJyNHiMz8QOSAJ5W4nEPBWhPB89OjRyZMnwwg+cuRIX1/f4uJi3FXdostO
gJiQ3aDHrHfv3p2VlQW29qUQQPsZBEQIcKJl/b+0+UU0c7zC1UB4BqKiohYsWADRAhK7nTt3Pn36
FHdY1+mCE5WVleHh4SYmJjDhNDIyOnjwIAxg4Ck+tH4FwsZnigZeoQN+dPZcLvcB70noiPPnzzM+
Z7t48WJI9nHPdRFWnXj37t1PP/0EMx8IUDBuhYURV4v7LIHg8UWQFuiiFqQXYmJilpaWEMW78Uky
lpx4/Pjx6tWrFRQU0Je6ZWRk9GVGyYN1UKcwbo5MnDgxJCSkvLxrnwb4shN5eXnz58+H2c7QoUMD
AgJevnwJU+E+TiB4sAjkm5Dv//333yDHwoULIVpoamru27fv33//xd3JAsycqK+vj4mJgQRCRERk
9OjRYN/Hjx/r6up+//13+N34KHiwE9AvEMLBgOrqanjrbt++HT0IDmEeeg3365fo1IkPHz4cOnTI
wMBAUFBw3rx5kE5WVFTA4ASi8JxgW5AT6J9fwrsXFiC0q6qqwjxx7ty5+fn5qHOZ07ETL168cHJy
UlFRgQgBqSxIwHi8h+cEO9PSCQDewyUlJbGxsRMmTICuhJAPy1VVX/igegdOFBQUwFAkISGhqKgI
Q9GzZ89afsqA5wQ708YJRGVlJdRDsIeQD4EfIgfkAHhbR7R1AmYvM2fOFBYWhp1PnjxZVFQEEuBt
dHhOsDMdOgFAmIe3ur29PcwV5OXlYRBg8qRWsxOQlYAEI0eOBJtgaguvDn61n93ynGBnOnMCgGAP
Hri6uiopKYmKii5ZsuTu3bt4W2uwE2/fvoXWkEBA6zVr1kCvd3YrhecEO8PECQD6rqysLDw8HAYB
ISEhMzOzzMzMlokBAjsRFRWlp6cHTri7u8Psts140RKeE+wMcycQMI7ABGTWrFn8/PwrVqx4/Pgx
3tAEdgK62d/fH6z5/Pkz86uhPCfYGVacAKAT7927d/DgQZiGlJaW4tomsBPgAcQQJuGBAc8JdoZF
JwAmPd6cY7IIzwl2hnUnmMBzgqvgOcG1XO3uLWeeEzzawnOCm2m444eXugLPCW6mPrE7DxjznOBm
ahOm46WuwHOCa7l7Lbs2cXpF9ia8zjI8J7iW2oSptQnT6hKm4XWW4QYnfvzxx+kWt5gX+CNx64HB
JRq1Ln4KlOr4ybiKZbjBCdtlN6HXYzoHtuKmA4aas5Nq40yg1MWZ4CqW4R4n8EpHDDQnypOt62In
1+Ji8rCwax/S5wYnjMctgV5nlDUbiP/m0rIGCmo5ECh6mF0bM7E6ZmINKrET8AaW4QYngKwmoPvt
lhMf+ISFtRvP4tqsLNRsIFAbPa4uenxN9LimMgZvYBkucYJBSyc2bungC0q5m5oo4+YSObY2ckxl
5Fi8jWW4wYmcXGpuLg0VUMF2aRZaWLshllHPKL0hIptQEzGq6vTo6tOjmopxdcQovK0rcLYT+fn5
0PfdKHh/buHqpfyakyNqwtsUw6qs9bhFV+BsJ7rXwbBLatM/4+ECnuX9Wn1SvypMrzrMoGWpCTPA
LboIBzvR7Xd89/ZiTx6k+lSG6laHdFAqTozHjboIpzrRbSEAtOPmbXfRi0DJvHgHbeI4Ko9rVR3X
bF+qj2viFl2HfZ1gdBgUVGO9wL59ZXvMreKYt0H1jDaooE2cRcVRtc5KZZA6btR12NqJyOjLeXmX
UIddukQsbHL0hUoocNCoWXtQH0O50dG/IgbQC6I2LRc4i2vXrlb6q1cGqHVYKgKG43YtqKEdwEtM
YWsnWi5kZGA5mDPDkoL6mEljtInRhnljtuXBxVMVh9U6Lb4dOFGVsuEj7She6RxucwJ1MPOWjDao
GWOBs7if9HOFjwaTUuZrhJs2Uf7z8AofNbzSOZzhBKOgms5gNNu5cyeuYgFWXpkNuZ8WUuGuybRo
3LlwGrem0R5lR9ErtZ7cysdVncCdTuB11ujGLmxCxX4tKOWumqh83q8NPytcNT656rao1KkrTK06
NKn8Jw1i1VXn7S9z8f6dwAFOIL44dqCubVPwNqaw3pLd+OyiTRRnLVTKnbU/ueh+ctEqd9FpWfkZ
Vl20iQWiRvuN73y8fydwVZxAXL9ZzLyx7ZLjqEFCQgKsMm/MzpTv0Cvfrl2+QxeVzzt0KrbrXqXm
le3UY1S2L+8pHfw3opZwoROoZU5O269OZsB4NSiMVbSJ4/i8VfezU1PZqlPpT/xTgX+D1jdXtilb
dcvdvvBt0ZzhBPxkcd4BQLO5NsxuZ0CD8PBw+IlekLHAiXzaqPd5oz4qsIxrwZWNuk2V+mWbmtuU
bdR9vf8b3KgTuM0J+GOYN7t27Ro0+O233+AnaslY4CzuRf1yiZJf4ru6bK0eLuuab3q9/3U7qqzJ
PFG2rqkB0UYft+gcjnECxgJWeu6LbaABaoMWGAVt5SxK7fXvhXnDz7JVRPnkMg9voIMqy7ZM/evQ
Bry8Sr9ktX5J8DbcohPY2ok2HdayhlHZhtDQULzUEfQd8QXvBw8ezF14ExVUw4mUfmtUsly/7FuD
0m8Ny5Y2jx3PAveWQc23hkXfm8IqbC2mr0L59K1+ifMi1KxD2NcJ5nSoRXJyMl7qCCYmcS4PIoJK
FuiX2BiU2hgU2zSPC+XB+6ASysOQg7BKXyaaET8XGDza1CqitIFTnQC61MdcKQTiysWM4q91Pyww
vkyjFc9t1uKjlS7Uo+ViK91SK116jd5dC0Pm333PwU5kZmZCN69Zn47XO4eLhWBQbKpTPNMQr7Sj
ZLpOialu+d7Nd80M7q2wxLWdwMFOAEeCs1F/f7HgHbiahxP1Xn/TwZcL/HX/91uJ8bBwx0T/jgmO
HEzgbCeAhIQEk5kZbQxoWVz2feGyHTdxc+HXVy52+mGWAuMvCwFwvBM8WOT2AVe89CV4TjTDcxTB
c4JHW3hO8GgL2zmRnBzq4bjI1sbY2Hg0gbHxGNuFdlu8Qttfjsq7EH/UyYreavToyXNstwal4S3N
XIwNdLY3x23Mljr6RKJ/Bp5x6sD6Raa4viMsLb/b6X0qLSuP3px9SE+P8d//w6qlU0xM8JHCoVp9
v+vn8PSL7Y812XvJpPFjcLsOGDdu3KIly7b7njyPdyBgJyeSQ3+0G2+or64kIyYmiv/NP4GomJiM
srrBmIlLnE60/AewuSmRHos1cCOxYaPsvNp7kxHuvspMEbeRm2C74zi6nJF61NFy9BBc3xEiIoPl
lTX0Zi1zizt/kb5Lv5Me87PTQtPxWsMUpSUl+Pn58ZHCoYpIwbHqG8xZ4RmWmt3yybqz28eLCvPh
dh1AJpPFxCXkVHQnmK8+3OQF2zgBRhvryonxkcn4cNtCJvOLy+uP+9aHoUWvOoERllAw3RiYkNHv
4SI9xHmpuY6MmBBDhfaISAwdPnPr8XNZDC2+5EQTZAFx1XErfJEV7OFEsvdiY3VJfj7sg8ZUO0eP
E4hQjwMbp6qjehKZT0BGd8LWE2ivHnNCfpLdVo8g/AsB372bpqkqDMJbhaXnu0T3b6wghDBTFxfB
Pgw1mLZkq88vwehgfV2sTRQHC6NNJOEhKos9EtJz0Y6tnNBc6k3fAxNy/Ijzmkl4G4k8WNnohxD6
TmzhRMhGw6EQIdDBaSxyj4hLycR/FPR8zoX4024bp6CtJD4Rac0F3vTO7zEnlGdvPBTV4kpPfnZG
YrDTLHV5EbRdzGJP5LlsvLEfiPH+zkyHIcSU7/YGnknNzMtHZy8vL/t8wlFXOxUFcdSAJD5+07GU
i/RY0coJoy0x9D2aoFIuxP36fZMVIvJq89zpMZgNnEj2nKchLdxkxOKDCVmUFmMKnfzMc76LcO+T
xWQ0HYKJ2l5zgiA/aO1INSncQG+l39kLX3gCvtdID9n2zbihguhIhk5ddSAsNa/dCco+vXWWqjQO
FkIWexLS6Y8eMnWCRstJi3eZhbeKK2rZB9L/xv53IsntaxWpJiVMt8VfaPcHA1RqTnoC/ha72LPx
afQ3ba86QTu+YZS6NG6gtexQbHo/pRTpx/4311ge96uh9c4jcR2dIFp+1vn4uFh8is5l5qP3FTMn
KLmZJw7YSqMhUkhKaaZTBHqEtd+dSNpvNZyhhNbq4MwcCv3AWKCVE2QBIVEpuaFtkZeVYgzDXXLi
nJetvrIYbjB168nETu8i9C7pwf+bz1BC0Xy9b2QG3sIKrZwQlJDFZ4WOgoL8EElidBQUlZy4+MeQ
VDTcsIETwWvU5cWwEqSv9p7rYJLdGa2cYAVWnTh38tCSMbpDBPjxcU3edCIxo6M3Zx8QfWCJqUZT
r46ycz6e2JUDaeVEZwgJi0+Z9X1wEt6n/504Yj9MjnExwsI1pdX0mjk95gSf0CDxwVLSDCQlxIQZ
kyCSmo3b6ZScflKCFrl/0bSmaRfJeMmeX9uPj0xgyQkymSwoJDnMyNYdacE1ToipGFrvi0xvS3yQ
y7KpCrhNZ04wQc1kXciZtHZJb9/Rg07orzuBzwoi5Vyc3//MtNBGMll0qI4N3Yp+dyLOeYaipBA6
LpLButCs3O7lEz19zWr48ElLHX3PJF3IoxCH2W+kBmy2HNF0oKpzNvpFdyWxYZZjAtTc9PhDy7AV
pEFy2vPdQIp+d4IauFJ1SFOgIFvuv9BhoKDkXghdP0IIIaWsuSYIKnvMCaVZ67xOpea0Ijc3D3L3
/rQBE7F/kYkaPlDymKWuIZ0EikS3udpKEvgUWexFl62+4AQxy0/85bsmKSRV9dcdYwMnCCmUmqUw
WB+W1X6uRcnNClmrjwd4URnVlYFEkx5zosO5KNsQsX9BsxSqFo5+Zzo61ETXuWpy+BobyXwXnq6z
4ESS/0pt3EBSVW8tWzhBo8U7z1CQwNdkiFHvVHZrKyh5mSfW6eHNJDEZZfsj9PoB4gQMH5vmGDHG
OdU5P/q3HT+oCa5fazRddSWRZu2ORZesvuhEfuY5/5U6eDs7OQFWuMxQYCQVoIXlDx6ncyF65+Xl
5lw86WbJEIIkJKlo7kJ8GhwYKE7A0QZunD1ShnF7UHWs1Uaf6MQLcH6As0fXzx6GLz1BJOHXWxmY
lIEH4FZOGG6KRHtgsjNTgnaZNwUJEllSxXA9KMEmTgDxLmYKUozD7xA+AXGFYfbN38c0cJwAUgM2
fDVKVqDz+8aEDgJCwrN3nzlPHzbotHKCGWR+6WEGdCPYyAkg/sgKU3lJCRFhwRYPBwCwJigsLiVr
Zt8UIRADygkgJWKv/RQtxUEiQgIC/ORmOfj4+ECGQWIj5tgHJae1uub3ZSfIxMmVlFOxXO/W9I31
7OQEnfj4wJ3Lpk6eLCWJkJKSnmwybfnuI8QHFFqTmxr9s/0o3E7J0GTFoZZP3CAyI3x+sNLBbTRm
LNtz4gK9Pu3Ydtsparhe75utv8SwvxN0UlLCPZ0WfT1TRVkZH73kyJGjrNftPd7iXjIDiL6KctK4
XQfIyg41M1+9Nzix5X8wYDsnePQ7PCd4tIXnBI+28Jzg0ZZ+doKnBRvSb04UFhbynGBPKBRK/zjx
9OnTOzzYkoKCAnjHFhUV4d7qFl12Aqitra3hwcbA+xZ3VbfojhM8uBueEzzawnOCR1t4TvBoTWPj
/wE+rRa6sslftgAAAABJRU5ErkJggg==",
								extent={{-80.2,-92.5},{83.09999999999999,36.7}}),
							Text(
								textString="SF1 HiL",
								extent={{-76.59999999999999,100.2},{80.09999999999999,33.5}})}),
		Documentation(info="<HTML><HEAD>
<META http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"> 
<TITLE>House 2 with ST, CB and ASHP (not yet validated)</TITLE> 
<STYLE type=\"text/css\">
table>tbody>tr:hover,th{background-color:#edf1f3}html{font-size:90%}body,table{font-size:1rem}body{font-family:'Open Sans',Arial,sans-serif;color:#465e70}h1,h2{margin:1em 0 .6em;font-weight:600}p{margin-top:.3em;margin-bottom:.2em}table{border-collapse:collapse;margin:1em 0;width:100%;}tr{border:1px solid #beccd3}td,th{padding:.3em 1em; border-width: 1px; border-style: solid; border-color: rgb(190, 204, 211);}td{word-break:break-word}tr:nth-child(even){background-color:#fafafa}th{font-family:'Open Sans Semibold',Arial,sans-serif;font-weight:700;text-align:left;word-break:keep-all}.lib-wrap{width:70%}@media screen and (max-width:800px){.lib-wrap{width:100%}}td p{margin:.2em}
</STYLE>
         
<META name=\"GENERATOR\" content=\"MSHTML 11.00.10570.1001\"></HEAD> 
<BODY>
<H1>House 2 with ST, CB and ASHP</H1>
<HR>

<P>This model is a digital twin of the House&nbsp;2 in the CoSES laboratory (<A 
href=\"https://doi.org/10.1109/PESGM41954.2020.9281442\">https://doi.org/10.1109/PESGM41954.2020.9281442</A>)</P>
<P><BR></P>
<P>The house consists of the following components and default conditions:</P>
<UL>
  <LI>Air Source Heat Pump (ASHP): Wolf CHA10<BR>nominal heating power / COP     
          (A2/W35):   5.75 kW_th&nbsp;/ 4.65<BR>electric auxiliary heater: 6     
          kW<BR>nominal cooling   power / EER (A35/W18): 6.01 kW_th / 5.92</LI>
  <LI>Condensing Boiler: Wolf CGB20, nominal thermal power: 20 kW_th</LI>
  <LI>Solar Thermal emulator of variable size, up to 9 kW_th</LI>
  <LI>PV system of variable size</LI>
  <LI>Thermal Storage: Wolf&nbsp;BSP 800,&nbsp;content: 785 l&nbsp;</LI>
  <LI>Domestic Hot Water (DHW) preparation: Fresh water station            
  (simplified)</LI>
  <LI>Consumption - House parameters:<BR>Construction year: 2007 to 2009 (EnEv   
              2007)<BR>Additional insulation: roof + walls + floor<BR>Number of  
       floors:         2<BR>Number       of apartements: 2<BR>Number of 
  inhabitants:         10<BR>Living       area: 500m²<BR>Heating     
  system:&nbsp;floor         heating&nbsp;(supply / return       
  temperatur:&nbsp;40        /&nbsp;30&nbsp;°C)</LI></UL>
<P>Four boolean inputs can activate a predefined standard control for simple     
 simulation results or during a lead time of the simulation. The standard     
control  of each element is as follows:</P>
<UL>
  <LI>ASHP:<BR>Switched on at 100 % modulation,               
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;     T_Storage,top             
  &lt;&nbsp;T_Start,HP,high&nbsp;or T_Storage,middle &lt;             
  T_Start,HP,low<BR>Switched   off,     when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;        
       T_Storage,top &gt; T_Stop,HP,high and T_Storage,middle&nbsp;&gt;          
     T_Stop,HP,low<BR><BR></LI>
  <LI>Auxiliary Heater (standard control is active, when ASHP standard control   
            is active):<BR>Switch on at 100%, when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;  
   ASHP   =     on     and (T_Storage,top &lt;&nbsp;T_Start,Aux,high&nbsp;or     
    T_Storage,middle   &lt;     T_Start,Aux,low)<BR>Switch off,       
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;   ASHP =     off&nbsp;or       
  (T_Storage,top&nbsp;&gt;&nbsp;T_Stop,Aux,high&nbsp;and             
  T_Storage,middle&nbsp;&gt; T_Stop,Aux,low)<BR><BR></LI>
  <LI>ASHP 3way valve:<BR>Domestic hot water should be produced at a higher      
         priority<BR>Switch to the top ports (DHW), when:<BR>&nbsp;&nbsp;&nbsp;  
             T_Storage,top &lt;&nbsp;T_Start,HP,high<BR>Switch to the middle 
  ports             (heating), when:<BR>&nbsp;&nbsp;&nbsp;             
  T_Storage,top&nbsp;&gt;&nbsp;T_Stop,HP,high<BR><BR></LI>
  <LI>Condensing Boiler:<BR>The condensing boiler should only operate, if the    
             ASHP cannot provide the required heat.<BR>Switched on at 100 %      
   modulation,             when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; T_Storage,top &lt;  
       T_Start,CB and             SOC_Storage &lt; SOCminCB<BR>Switched off,     
      when:<BR>&nbsp;&nbsp;&nbsp;&nbsp;           T_Storage,bottom &gt; 
  T_Stop,CB or         SOC_Storage &gt;   SOCmaxCB<BR><BR></LI>
  <LI>Solar Thermal pump:<BR>The control is&nbsp;based on Green City's           
        'SolarController'<BR>Solar thermal pump switched on,                 
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_Storage,in) &gt;            
       deltaTonST<BR>Solar thermal pump switched off,                 
  when:<BR>&nbsp;&nbsp;&nbsp;&nbsp; (T_Collector - T_Storage,in)&nbsp;&lt;       
            deltaToffST<BR>The pump is controlled with the following             
      conditions:<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &lt;        
           deltaTFlowReturnLow, the reference volume flow is at  minimum         
          level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; if (T_Flow - T_Return) &gt;         
          deltaTFlowReturnMax, the reference volume flow is at  maximum          
         level.<BR>&nbsp;&nbsp;&nbsp;&nbsp; Between the two temperature  
  boundaries the                 reference volume flow is linearly  
  interpolated.<BR></LI></UL>
<P><BR></P>
<DIV class=\"lib-wrap\">
<TABLE class=\"type\">
  <TBODY>
  <TR>
    <TH>Symbol:</TH>
    <TD colspan=\"3\"><IMG width=\"350\" height=\"273\" src=\"data:image/png;base64,&#10;iVBORw0KGgoAAAANSUhEUgAAAV4AAAERCAYAAAAzJc+jAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAEumSURBVHhe7Z0HmBRV1oYHBAQJSjBgQCSoIIIZUdewqwimFUVEwF1d0V0FBWEwLIirIAoiKGkIQ5CckRyHmc49mSBgwJUV06+uLgICKnz/Pbeququqq3u6mc593uc5z0xXjm+fvnXvrSwwTAzZl9sJ2fnqB438bHTK3ad+YJjMg8XLxIh8ZGdlIStIBMiYYTIIFi8TUywzXobJcFi8DMMwcYbFy8QAKmboBCrGpYzXqqiBy3iZTIbFyzAME2dYvExMUTJeJftlGEaBxcvEgX3I7aQrauCnbUyGw+Jl4ohfwFzGy2QyLF4mphgfrnGRA8MQLF4mxuiKGbiIgWEkLF4mfuRn+7JfLmpgMhkWLxMXDEUOnPkyGQ6Ll4kpmnA5w2UYPyxehmGYOMPiZeJGfjaX7zIMweJlYoIsYtCV5ZJ0NeHSOC7mZTIZFi8TEwxypdoMetNyR+hMhsPiZWKEviP0bPFJQ6nXyxkvk8mweBmGYeIMi5eJGdrDNArOcBnGD4uXiQ2Gcl0qXuB+GhhGg8Wb9ujKWkmEuma7WZ1yhRJjQ0DNBbFeznoZRoHFm+YE1C7QPeiicbGqXZAo8dJ6fV8sFsG1KZhkgMWb5hgFKLJffZYrZBhL8VqJTwsWIJPJsHjTHBYgwyQfLF4mbdHXqpARw7IO/oJjIoHFy8SERIuI1m9eh9UwhkkELN50J0Q5LmWE8XjgFVC2HA8s9pvFyyQLLN40h2QTVK4xfLhmJAHiFcSzqEFPotbLpA4s3nQnlFzTXLyJgIs4mHBg8aY9Sqc0ATe+kK6x85pYEn/xhsz0YwkXcTBhwOLNEAJ+/sZYhCQbw/pMEXMR7ctFpwT9xOeiBqYiWLxMWhJM/Jx5MskAi5dhokjCijiYlILFm+Yk6id/wosaEkUCiziY1IHFy6QllplnHGpxcBEHEw4sXibNyNe9cigwOBllkgEWbwaRSQ0KuKyVSWZYvBkCicj8c9dqWLRJ1HoThaXw49ZQhUkVWLyZQqIq9iesQYF1kUPs1stFHEz4sHgzCC5qiD2JWi+TWrB4mfRkXy6yY55VM8zJweLNEDItA6T1GrJsNdKviINJRVi8mUKiKvZnWIMCLmpgwoHFmyEkKgNMXOaZILiIgwkDFi+TtiTqoZ5hnWpwUQOjh8XLpCUkQLPsrIYxTCJg8WYIJJ2AhC9OfRckYr1W62DxMskCizftSVTF/sQ3KMikestMasHizRAsM884kKj1JgqrrJozbcYMi5dJTyyKGigTjfmXABdxMGHA4s0YElWxPzHrtcy041G2LOCiBqYiWLwZQqJ+8idqvWLF6GR4oafytmV2IJMMsHgzhURV7E9kgwKR4eozz7j83E9UEQeTUrB4MwTKPPUSipeMErXeRJHIIg4mdWDxMmmHvow17pkmF3EwYcDiZdILKl7wWY6k1wnxSDYNmW4iijiYlILFm0Ek6ml7PNcb8FNfSDAeu2lZxMAwQWDxZggkBnPmZTUs2sR7vQECZPEySQiLN1OweMATD/HGe720bEN2bYp0Wy+TmrB4M4hMKGpIFJzxMpHA4mWYKMDiZSKBxcukFYksamDxMuHC4k13LMpYNagIIGaySNR6DeQj21CnlmGSAxZvmhMyEwshx8qSqPUaSax4tbLt+Owrk0qweNOdUJKLpQATtV4D8ROvLOLQfdOQdLV95GIIxgyLN+1RmqwGiE7ILysrW6gpViRqvXriK16fXGkf9aaN2xcNkyqweDOEgCpdcRJSvNcrM0/9+kwROwHq+x3Wf7FwXw1MICxehmGYOMPiTXMSlQHyeo3BRQ2MHhZvpkDdFZp+7+Znx6Gsldcbn/UyKQWLN0OgjCygnDEOD314vQJ+uMaYYPFmCuLmNz/YiktDBl5vfNbLpBQs3kyCpKAve4yXDXi96giGUWDxMgzDxBkWb4ZgWfYYB3i9DBMIizdTsHjaHhd4vQwTAIs3Q6BMzFDuqEY8nvLzerkeL2OExcswDBNnWLwMwzBxhsWbMeg7cYnnT2Beb3zWy6QSLN4Mgcoe5c2ve/izLzcbsfYBrzc+62VSCxZvhkBCUDwgMjKtZVV+tiKJGMLrpX9jv14mtWDxZgqUgakiIDkoP4E7xT4T4/XGZ71MSsHiZRiGiTMs3jTHn3VZR6x+AvN6jcFFDYweFm+moHvYo8H94saARK2XSSlYvBkCZWQmH8TloQ+vV8AP1xgTLN5MQdz83C8u98fLJAcs3kyCpKAve4yXDXi96giGUWDxMgzDxBkWL8MwTJxh8WYMiepDgNcbn/UyqQSLN0OwfNoeB3i9DBMIizdT2JeL7ERkXbxehgmAxZshUCZm/vkbj5/AvN74rJdJLVi8DMMwcYbFyzAME2dYvBkD1y5I7/UyqQSLN0Ogskd58+s6ceE3QUSfRK2XSS1YvBkCCUHxgMjItL4E8rMVScQQXi/9G/v1MqkFizdToAxMFQHJQfkJzG+CiDqJWi+TUrB4GYZh4gyLl2EYJs6weDMEf9mjjriWeerg9TIZDos37bGu3qRFgCSiBq9XH7FbL5OKsHgzBMtMLA7wehkmEBYvwzBMnGHxZiD0DjD6+RvvckdeL8MosHjTHPrpq3/nF8lAE0EsfxbzeuOzXiY1YfGmOYabPj/b+OLFGD5t5/XGZ71MasLiTXv0T9uzxSeNfcjtFK+n/Lze2K2XSUVYvKlMGxF0BsMNmj4a8HpDR7TWy6QtdJkwqcqTIqqJMN/4VkHT0fTRgNcbPKK5XiZtoUuFSVW+EVFPhPnmtwqajqaPBrze4BHN9TJpC10qTCoTTjYWiyyM1xsYsVgvk5bQ5cKkMuFkY7HIwni9gRGL9TJpCV0uTKoTKhuLZRbG643Pepm0gy4ZJtUJlY3FMgvj9cZnvUzaQZcMkw5YZWPxyMJ4vfFZL5NW0GXDpANW2Vg8sjBeb3zWy6QVdNkw6YI+GzvZLIyau/paYFlHQCusCtcbuq/ak411Nx8Mb3/pPWgW8yuhb2UWJtE4zkxGw+JNJ/TZWERZ2MmKUX2JY4XrjY14H3lnf8j1Uv8JVvMFi7Cb9Z70cWYYBRZvukHZ1ynq3zDQui6sVJCxQq43NuKVHc9YrlfpH8FqngojXPuG3F+GCQ2LN92g7Otc9W8FREW6apxz2zz8HnS9MRRvwP5WQrpahCPfCI4zw5hh8WYokf4MDyuCCiuG4jURrS8T7saRiSUs3kwk5MMmEcEEWuGDN7XMN4AQ4u2UK3LUKBFyvwK3LfSXz0k8dGOYMGHxZiChhBPOr+xQWaV1phgf8QbfrhASDfFlEm5xL8NECos34whRBhq2aSIVaTzEG3wdFe1WUGGzeZkYweLNOKIh3hCySpR4gxYzhFFkECzrjWI2zjB6WLwZR3x+9huJwzqDyTOcL5MUEO/x48exf/9+lJSU4LPPPsOxY8fUMUwqwuLNOCqqbhWLh0qxF2+wcuuwaickuXhJsm63G3a7HQUFBTJcLhcOHz6sTsGkGizeDCT8KlfRknByizfow8YkKeP98MMPYbPZfNLVwuv1qlMwqQaLNxMJ8SQ/dJysiBNRvBEuwbctWeryOp3OAOlSOBwOLnJIUVi8GUr4WW+ICDsjDCHek4koZqLBj0OwOsnxh8p1rcRLQmZSk5QRr/HnIN0UQcoqk+TnYfIThaa1WlSYtSaneEN++STRdXTgwAFZpmuW7rfffqtOwaQaqSFeqioU4uYmKXMTz5MjKpmvGsFdlWzirWh7YvGAsXKQfIuKiqR0ScLff/+9OiZa0DFJvv0OhBKGilshRu170+Ae/TEyb0dkxy9h4tUfqOzsbDVrMu8MfRY7Q2WSkYhXPVj5vnXolmmSuG9edXiuJiI6c76y0MD5LZedskQv+7W+4JNHvOYb1CqidtMyMSBQvDJ5iNVJM/nCT+B2REJixEs7o/t2kAdO3TmDRGk69YAqmZn1jlqKV9xAvmF6cZsOpEG8vptOFYW67oBtCrbstKC&#10;ykrT61k8G8Ya3DbG6f2OO/oGpthPqte5PEujcBElu5PFRz506n0xCtGtbv3z9ObZch394hYkMEdG2q9OJ8N232jZaEel2y1H+L+fsXP3ytWNksR2+cSoVrDch4g0QJW2kfufU/2k6442g3jy+aRUClhdwMgIvKm2cb17DcOXA+tZN22e6ICyXnZaoxzyCMJwLSYhlmM5lTDDcBMEihc+h6ZqkJEVerjRc7Jt2Pmg4/W+4X2gaObHpHhHz+a5/+dl/fGh+w/1gsQ7jMtTzr84TsP4Itl2MMHxxBNz7ek56u/3z0HCDm3zjjNthGBfGepNQvPSRdkjsWLb1Tek7OSoBy6OdM9zQpoOiG+eb1zCcxRsMeRHRhRMq9CdHot54VmE4ltFH3jhW69VHwPamFlbnJPCaFviuY3E+1OE0r7L7YdwjPoJP61tHmPdT5NsevnhPZrsD5qHhvul085u2Qz8unPUmpqiBdsa3A/RRHHDTQeiUnS3SfP3G+wlLvOIEatPQeN/y5TjtgCkXhJzXcCIqEG+wZWcQIYUWcDzowrOYjiJmxy7EOn2hv3FSl8AbXcVwTQt013FgchMoB22+cEQSsI4w76fIt90kPBqun07HyWx3wDyG5evmN2+Hblw4603YwzX9jet/uKZBG2o6uPobxncGFQJ2VN05X/mS72Ap+Nctvu3FBSjnDfNCsVr2tR1nY+bMcmV8UkPHVdtuYxgvlHBQjpHVsgJ/tgdfb0zES+fIal36MF1DKY3cX/MxFxiuaYH+Ohb/G5ObQDn45jMtn+438/0QsA7D8Arup4i23Sw89Tr0LVwgplUmPYntpr+6eYxJoe4YBWyH6fhVsN6EidcA7az+IFQW80GNJhbLzsp6FXXqvIFGjUYmuYCjK8CgWW/AsuIp3hDrUkN/j6YL8ub27aNOAPrj65OX/CCOUwhxmM+LFJK6fP24YOswDA8hXkGk265N708WVPlqy9AtO+Ltlv/6l2VMCnXHSGDcDuO4itbL4o0Ui2WTeLVIdgEHlaUI/fVaMaaLXR8BCwohw6iepxDbJEN3YzBMAkkO8aY4evEmu4CN2YU59BlQaEItJ1Dg8RFv6H1j6TLJA4s3CliJV4vkE3AICfoihKQo47ecRwureeMh3lD7Ff4XCsPEAxZvFLASrjmSSsD68qcoh3VxRezFGzrbPdngLJmJDSzeKGAl2mBRrdpruP76aeqcCSQG8g1eMyL24g1Vdn3yweJlYgOLNwpYCdYqKOvt2nURPv/8J3XOBFNhsUH4EfrBXKzFG6qYoTLB4mViA4s3ClhJVh9JJ1wTlfqZHlZVCBYvw+hh8UYBK9lSJLtwrQhLwhHLMsbijWLmbgwWLxMbWLxRIB2EyzBM/GDxRgEWLsMwkcDijQIsXIZhIoHFyzAME2dYvAzDMHGGxcswDBNnWLwMk2ScOHFC/Y9JV1i8TELYu3cvunXrhhtuuAHLly9XhzIEizf9YfEyccfhcOCmm25CrVq1cNZZZ+Hcc8/FlClTcPToUXWKzMZSvL8fxW/bcnF0YSf85h6OE4e/U0cwqQiLl4kbJJRZs2ahSZMmOP300/Haa69h7ty5uPLKK6WEBwwYgG+++UadOnMJEK+Q7tF5t+BoTnMcmXABjowXMb0tTvy4V52ASTVYvExcOHz4MIYNG4aaNWuiRYsWmDhxIsrKyrBt2zasXLkSnTt3RpUqVXDvvfdi9+7d6lyZiVm8v7nfFNJtoUhXF0dn36hOwaQaLF4m5nzxxRf429/+hqpVq6JDhw5YsmQJysvL4XK5ZLFDcXExCgoK5DQ1atTAVVddha1bt6pzZx5m8VLxglm6UrwiAz5x9IA6FZNKsHiZmEJSvf3220Gdzjz88MPYvHmzzHSdTqeUrhaFhYXwer2y+KFhw4Y477zzZDHE8ePH1SUpHP/1V3y2KQ+7l3+A346kZ5mwWby/bvi7tXgnt1SnYFINS/Eae6gK97Up1AOV1puT+dXHFgR9IaV+OSqGac09XdG0QV5yqHZZaL0/VsuJlDD2M4NZtmwZLr74YtSpUwcDBw6E2+2WItYLVx8k3tLSUkyaNAnNmzdHvXr18Morr+DgwYPKAoWEP16+GuOatsLIRo2R989X8csPPyrj0gizeKks92huO6N0p16K37bPVKdgUo1A8QYVYiTEWrz+8VKquj5h6bPhTQhhrse8nPBg8VpBtRPGjRuH+vXr4/zzz8c777yDkpISKVYr4eqDih8oI166dCluvvlm+aX4+OOPY/+XX8plb+g7EIPFsNezamJEjfpY0esJ/PfTz+S4dCHg4Zrg+DelODrzGl+m+/uu+eoYwn8t05s4Ai5j36vLjcmG/z4xDvclIVFxQQXIN6GY7vcMIFC8QV61np/tF4xBUnRyfCeVDqAx+/SdXP2rZmh69aTm+7JR7eAbhSgJIV7zxREg3iD7E3I5+m21mCaXXjPTKRvZVvuZ4Xz33Xfo168fqlWrhnbt2mHevHlSpJTtWonWKqgYgjJfKpbo3r27fOh22x//iJKyUvy0/UMs7Hg/XsuqgWFZp4mojfdvuRP78h3qFqQ+VuINje5apmvXZF79veuDrmXftW28F3z3t+neii6qJ7LFfaS/xzIEy6IG5f1VxpPlF5p6wNQTQsOV86w/eaZMUJ5k08mXw/zConUq/9Ny/ELzhe8CCHKRqASIV2C1P0GXY7ggTctXt9m/Os549Wzfvh133323FCX9Xbt2rWV5brhBxRIk7Gxxc9auXRuXtmqF1evX48Bn+7D+yT4Ydkodmfm+nlULk1pfid1LluPE8UillXxUSrym61p+tpKnQaqmebRx6l/r5Mh/b/rvN33SpVueOekyYN7ezCDEwzX14Gonh06CPGhiuPirfYv6v031B9AoJCsZGk+8wPdNbXEiAi4S7eTqtk/Fcl0S0/4EWU7g/LrtMW+z+I/Fq5CXl4fLLrsM1atXxzPPPCNlS8ULVkKNJLRyXyquoIYWDRo2xNjx43Dohx9R/Pa7GHXGOSL7VeQ7+pwmcL01GscOqGXCKUrlxCs+6YobzNezTCQC7hvj/HIaWgBd72JabX5/cqRDTqPMa3nvme4Z/bYpGNedKYQQr4L/QJFkxAESglT8SH/FQTOITDuAsRZv8BNluS4d/v2xXk7g/LrpzNts2s9M5Pfff8eMGTPQuHFjNGjQAMOHD5fCpVoKdrvdUqaRhlbuS8UWV199NU455RRkv/Qi/ve//2HvouUY1/RS/CurhpDvaXijRj2sefJZHNz/tbqFqUdlxeu/l0JcnzphKvMHJiEB17tvuTRKy4IpdPeH+KxOIjFOp0TQ+yuDiEC89L+QknYi5UnphGzfQdQfQNMJN5xklaAn1eJEGKYNfaIqK17ztsoLRzsA5m0272eGcejQIQwZMkRmuS1btkRubq6sn+vxeCwFWpmgDJqWvW7dOnndUZ3gLl274tPPPsP/uYow8/rbfPKl7Hd+5y74tmyHuqWpRaXFq30OuF716K/dEPeC1T2qE7DVvHSPaTKu6H4Muu40x/rhmv4byneABTTOdyLoxOm/3YwHUPum0w664ZuPZgp2Uq1OhGHa0Ccq4EQH3Z8Qy9HPo99GiwvZt5+P64emP//5z3/Qs2dPue+33norVq1aJVuhaY0iYhVU7EASfvLJJ2Xme+VVV8EuRP/zZ/uw/KFeeL1KLfnQjSQ8ue112Lt2o7rFqUPlxUuXsLgmhViDSo+uZd88Qe6FIPeo4R6T90rgvL4Ex7AeK0Lfz+lKhRkvEwZ0FOuIaCQiA6pWUkZLvYqRdHv16iVbnVFRgJUoox1UfFFUVCQfvA0dOhRnnHEGzmncGPMWL8bR//6E/BeG4M1ajWTm+2pWdYw5pxlKJk7Db78cUbc++YmGeBUhmn6NGZIQ/bjIxKtMry5HVyvBkFzp5jMMD1gPi5c5WegoapHGAiYhLBaCo2IF6tRm0KBBoLLcaDxEiyRIviR/KnqgXs2osUXdunUxcswY/Ph/36FciHbsuc2FfOmhW028WbMh8gYNwS8//Ffdk+QmcvEyqQaLNxroxatFmgn44MGDePPNN2WjiAsvvBBjhOTCbRQRq6&#10;CqZiTfRYsWyQyc6g7/rXdvfPHFfuxbuwnT2rWX5b0Uw6rUxtKHH8UPH32i7lHywuJNf0gRTGUxS1cfaSDgL7/8En379sWpp56K9u3bY/78+bI8N5JGEbEK7aHbli1bZGMLetDXsVMnlO/8ED9QY4t7HsDwrLqyoQWV+8668Xbsy7Ore5acsHjTH1IDU1nMsrWKFBUwld3ec889UmjUZSPVKiDRnWyjiFgFPXSjL4Lnn38edWrXRus2l2F93hb874svseGp5/BmjQbyoRuV+45r1ho7Zs3Hid+NHfAkCyze9IeUwFQWs2RDRTUR14tIcqhXsDVr1shOyqmTG2oUQWWrJDgr8SVDaOXNI0eOlH1EUA9nE6dOwXf/2Y/Ct97BmHOa4nWR+b6WdSrebnQ+XG+OxtGfkq9bRRZv+kMqYCqLWa7BgrLeriI+F5HE/PLLL/KhFYmLWou98cYbshYB1SZwCPnaHVRlzJ/x0mebI3FlvfrQHrpRow5qbEFfGs8PysZXX32NT5Z9gMmXXSOLHai12/BT62F172fw4yfJ1ckOizf9IR0kMRZVTQxVXHTVWgxVVZThVnUYlaotJ1F9xVy1Ro9ZsOZIEeESX3/9NV5++WX5ap62bdti6tSpsriBHqJpLdGcDpuQrVa+64TLsRXFjqXif60Or12IuFCGJsR4BW0j1SWmMmjK2KnPCCqb7t6zJz7auxf7852Ye0snWe5LD92o3HfenX/GV95i9QgkHhZv+kNaSGLCEa9/vJSqoZ6hWbBKo4+4iTeFhEt8+OGH8gHVaaedhj/96U+yP12Srv4hms1RhN32N7DTMUbKlz7vso/E97ZbxHjKgp0ocJSgxDEfZY5Z6rBAQcY6qAyaikXoTRb0Zou6IvPtcOMNyLPZ8P2ej7Hq0SfVcl/lodvkttdiz9IPcMLU8XoiYPGmP6SHJCYy8frHKcNzhYh9DdWI/GyRBeca5zFUKjeuS1/xO1vMZxCvYT4xVwoLl6BObqj/W/pp/thjj8kuGZVWYkpRgt2hNAMucJThP46/4YC9rfq5BF/b/4zDtotQaF+BfMd2eByrxfg2+I+9tzqfUy5DL8Z4hFvIt6S4WGbr1KH6mWeeKev8zpw7Fz/s2w/H0NcxulETVb7V8c5ZTVA4ZgKO/ZzYTnZYvOlPCohXk5suwsp4xXBTlqr0pKabh8Zbzi8/GMbJ9ufasszLFdskRqekcKnT8jlz5sgXUJKYBg8eLMtJ6SEVydLlyMNO+zsociyXki1wlOJj+xD8Zq+ND0WmW2AvwUFXC5woOQU7bOPk52/ddwGFWfjIPlQtctAaWFh0miPk6HK55TpJkF6vR/7vdrngNE3ndmvThBEeN4pE0DLoM+3P+PHj0bp1a1kX+Y2Rb+H7b/8PO6bPwsRmbXyZ71u1GmJj30H4nxBzomDxpj8pIF6//CQG6ZnEbCFkX7eVNJ9eynKQuS17iHGU4arL12fCWnS6WoxJIeES1Gk5vePsrLPOkq3RtDdFUO0ARXhOKc5P7C/gB/vNUr5bHJ8K4b6DE6XV8HNRK/y7oA+ObWsAfJSFr+wPYZ/Ick/srgqUZ2G7LUdM/xHKHDOw1z5QlgX7y4EdcArheoVk7QVbsXHjRqxfv07ERmzatBn5NjtcHq8UsJzeXoAtmzdi7TqaZn2FsTlvq1i+kLcQtl3MT8UlVGyycOFC3PyHP8iWbn/v8ww++XQvPl+7CTOvv0XKdxg1tsg6DQvvewhfFZUKC6oHK46weNOfNBCvabxEN1xtX27VK1llxGv14C6V+Oyzz9C7d2/Zwfgtt9wiG0VodWG1h2gUVI5L8a39Lhy2N8OnQsK7Ct7E0bKzgI/F5eMVISSL7Vn4vfBU5f99WTheUh07CiYIab+EX+wX4BPHC0LiVPNBKfN1ur3w2DZh/sQx6PdEd1x33XW4/PI2Iq7FjTd1wqN9+uLtqfOwqcAOL30RrJqJfzz0R7RpQ9NcLqIt2ra7AldcoUS7dm3V4Uo8+PRQLN0ksmeXv4yZJEzy3bBhgyzLpn2/o2NHeIuL8d32D7G8Wy+MqF5fNjP+p/gyXdPneRw/9qt6xOIHizf9SX/xiin1b8wwjKNl6eaXmaxW1CDLcP3jAooaLNebGtBT/7vuukuW59Kbf0lEJKTAnsUoW3TLcl2PfR2Ous+Wgj3ibozfPbWAMkW6J3aI2FkF2CY+0zD6XFQVRzznAkVZ+MZ2vyyi0GpCkHSdW9djTL8/o8XZdVHN9OtBxinV0OD8lujWbzjW2EXmu2ICOl7eMHC6ING605OYs84Lrzvw4Z5Sdu2Ub7Zo2KAB2rS9HMtXr8aB/V9ha/bLGFHjDLzX9BLsXb85IW+0YPGmPxkgXoGQqD9DDRznu2F9y1WQslXH0U2qH28sbhDLS4EWab/99huWLFkis8ZGjRrhueeek9ktFS9YtUSjamNUxktFBHa7E/vsTwEfikuGokTI9aMqOPFvU3wmgiRcLKYRGfHhoqbw2lcjz7Eb+Y5yKfGi4kIsGv0MmtRSjl/ds67GA4/3w4BB2cju1x+PdbkFDU9Tj23Nc/HEiDlwCFGPf+sV9H22HwYOHITn+/TGrZc3QlWa5pTT0PzG+/Ds8wMxcMDz6Ne/P0aMm4F1W11wWewXhdbD2ahRo3DRRRfh7HPOwXuTJuL7L79C8bsTsHvhcvWoxR8Wb/qT5OJNEego0oO1JG4SfODAAYwePRrnCMFQJzdvvfWWFI+/PNcYVLa70/4efrRdj//Zr8L3rlvxo6MDjntq4ARluXuFYD/XCVcf+0TsFiHkfMjWEp84XsZu+wjsczyJva6XUOb4AIMfukwRa9Va+OPTk+Ap+xA7dm7HjvJt8GxZiSG970Dbyy7BpZe1wwPZ72KLowil4guCstXy7TtRYluD7C4tlWy5egPc0ucduEq2Y3t5mZymqNAja2RY7ZsW9BCPpn3//fdxzTXXyF8Az/brh8///W/1qCUGFm/6w+KNBnQUtUhCAe/fvx/PPvusFMu1114rW3WRcOhpv748Vx9Uo8HrWCtrMPxsa60UIYgM9kRJVZzYI6QaTLpaiPFyHq3owV0Nx2xn4jP3yygqWIt+nRor4q1SAy07/xVvT56OxavXo8AusmxPITwiy960Ya1sBLFmwxaxnZSBK9tG4+0blqLfvc1V8dbHTU+NwBa7sUw3nNBeK7Ry5Urcf//9qFmzJu657z5s275NPXrxh8Wb/pAqmMpCR9EcSSJgKkZ44IEHUKNGDdnJDb0pwro8NzCoDi5VIXM43PjU+SJ+La0PlFaQ7WpB43cJ+Yrpj5adjS8dj6DIvgx2JzU9diJ3aHc0ri6kSeIUUfeCFrj6D7ehc5cueHzQIIyfNQcbNhegsLgExZS96rYrmuKl0BpbUIfuTz/9tHzoRhkwdQiUCMzi/cc/VmPsWA+OHPlNHcKkOqQIprKYpauPBAqYssV27drJtzQ89dRTshUXCcaqPNcqlAdrxVK+Oxzj8IvtPFl2K4sSrGSrDxLvJ+IAOLLwvf02OOxObHXshE0sz+kpgjtvGd58pjPatqyPU6r6BSyjZi2c0+QiXHPDfXh+5ESsFtvidvu/KKItXi2o3Jd+Bbz++uvy5Z3UT8XkyZNx+PBh9YjGB7N4mzYdi+7dl8i/LOD0gNTAVBa9aINFHAV87Ngx2cdCkyZNZC9dw4YNk+WZVKZrJRzrcImf+xuw2z4cP9qvw4niasBOsSOU8VqJ1hwk3j1KccNvu2vjZ9dl2G/vgZ32sXA5NsPlLoTbtQGLZg7DM890Q5e7O+K6VpegwRnVUUUn4VPPuwSPDJuCArdXtkSjbYuVeKnYhY4T/SKgToKosQXV96UHq1TnOV5Yiffzz3/CN98cRP/+61nAaQApgaksesFWFDHuFvL777/Hiy++KMtzqfYCvflXK8+1kk2wUPpkGIEfbdfiJ9s1+J/rKhwubAp4hVA/FlFR1kvi3S7Eu13s9G4RJVk4YmuMnwquwTbX+ygs2Ymy8u3YVk7b5kDBpnVCwtMxevRQvND3IVzf9ExUV+V71hX3I2dFgWyNRtsWK/FqoTW2WLp0KTp27Cj7IqYXe+7du1c9y&#10;rElmHg1SMDXXz8Nl146nuWbopAKmMqiSbWioKw3hk2Kd+3ahQcffFC+fZeEoXVyE055bmAovY45HQXId5TK7iC/cdwHFAuZ0sMyTa5m4WrDPxHTFWfhkPMi7C3oLyVeYp8rstx85K1aiNGjRmLEiDcxZspC5ItsVjYV9haiSGTlRd48zBs5CNeeXk2Kt/Y5l+ClqRtQKL48qKw31uKl0BpbaJ3s0DvmbrzxRvk51oQS74oVe3DFFTm4//4FKC//Rg5jUo8spV4q1W1VsgslLOrOmt9YGhHK8q1aeyn1Ya3q4laAoT5vMILV89VDDSzCeONqKKwkq48YC5cgIdBreeip/F/+8hdDeW6wmgsVhdKxjUs2fvDaVuJQ8UXAZ2KHyoR4twm5Up1dynwpSLbaX8qIqTaDyHR/9raBy7ZFNh2mLNpTWIr1U19A8zNOxaliW89ocRNGLshDaUkpiulBmogykQVvWvw2/nh2DXk91jn/Krz2/mYhXqWjnniIVwsqnqFfC9RVZsOGDWVVPKoVQnWiY4WVeKlogYWbPujE65eNoQWXQDaRFXLSD4sMTexmocWom0YfJyPek0AvWX3EQbi//vorZs6ciWbNmslGEdQLF/1Ujqw8N3hQfV6XYwt+clwnW6n9vu00HHM3BEpERkst1XYJCe8R8ZH4TEUKNKxUBDUdpibFYtg3rvvkgzq7wyu2TWSt6+fgkavrSalmZVVBq5t64Y0xuVgwbxEWLpqDmZOGo/f9V6F+FRpfHa3ufQ5Lt7jgVR+wxVO8FFTXmY7nhAkTZGdC1FcxPYCjutGxwCze+AjX4l4JaKxE50MLZVqzK5TpKnk/ZQCW4jUecBITjQs1jXICKKOl1l6+zFbtJ0Fbfsy6aQyyLcb9ov+15WrbqIlfP8y0n1bbo64v37c9YrhwTDyFS/z3v//Fq6++KmstXHLJJcjJyZHVxyItzw0WJEsqathvfwQH7ZfiP/bHZQ9k39v/qNTpFVnt8fyasnmwfOjmUst092bht6K6smOdo8VnAu4sfG2/XywrXyzXA2+xG8um/BO3Xnq+79jWOaORrEnQuPE5OLNBXZwih1dF49Z3441Zq+Eu8ogvAGW7NPE+e3dTZf4q9dCh9xsxEy8FfZnRL4gFCxbIvi3ol8UTTzyBzz+P/kk2izc+mK57IkC8pvES5R7S7mt/nyhMKCrOeOngq/8bDmqFstOETajDTPPEqptGa/HqMCzLnPGGsT1yuP8LhrqF7ERHMk7CJehBDxUpUP1cEgH1unXy5bnWoXULSb2LeR2rZHUwu92N/zmvxu/FtfCZsx922d/CsZJGUrjfO/6Ig9uay+IIGlZaMBuFthXYZXsTX9geRbF9oZS5wymy10IXls+ZiH6P9cDN11yK02rV8ku4bj20aXcTHvzb8xgzfRnsLi9culZoiniXYUCXVjhN7H+NOmfjtqffiql4KbT6vtS3RdeuXWW57x133CGlHE1ORrzy2lSPn795u/na1t+TZizuFcN9FeReIrTpDNMzobAu4zVJzCdbXwYrMB1kv+wEapbom0930mLRTWPwbTFeLDTct4++4cHFG3R7zBdYbbEtF4s54iBcgjJaekMEZV30tJ1EQEKIpnT9QV1DFskiB5ujFOWO6Thkb4aP7S/Lsl+bvQiHnM3xa+HpKLW9j52OsfittA5+K64nRLsIeY4P1UYYTpk9a8t1Cvl6xH54CvLxwaI5GDdunGzSTEE/6ectWI4tNrd86KaXrjavvSAPS+fkYMzoUXh77DjMWLRabJ9YRwXNhKMRVOxA5eYDBgyQfftStbPly5dHLVONeDl0Pequc31SYriGffecFXRta/eGLnzXuWm8/voXyHVyEUPYWBc1+LA6Gep0QWUnCCFeOU6M8GfP/nFBRSf+CxgXqXjV9Srohou5Ki1ew7JjBz3Qoe4btc68KbOhLIwq/ofbKKIyQZ3cfGR/VdbHpfJakjH9/c7eEYdsF8Pt2CAkW46vHA/jhLMaShzzxGelrFl5UGfeRiFK8WXh8XhB5ai0HxT0v9IZeiiROuGm+WgeMb1W/huvoG2koDcaU31p6gPj3XffxcGDlX97RaTiDXlv0DWr/k/TBb9M9feEiuE6txivg8UbGaHFayEU30mmk+I70CQvcxmpfpn6/5Vpjd+k6ji5TP920Lp866dt0Y0LKGoIuS3KsnwXp2FZwcUbdHsMF6QgDuKlN//STU5P1ukBD3Vart38VmKIRVAxQaFjpSx20F7po3SU/qKs60tvmKAsuFQI97/2DuLvHDE+Og/5kjHoy4HK1OnhptbJDmXBX375pXrWTo7oipc+0vUtrvNs3TUbgP4eVTFc5xbjNbT1me8LJighxevPSnXoDq7yLad802WLk0snn4ZpF4FfnKblixPlv1ACx/mya9NJ9K9PbJepm0arbTEum/5XpxHz6tcpt1MMD5xHYLU9pgus47V3IavzNPVT9KEbuW/fvvIllFRljHrTohueyhathBC7oFe7e4RM/Q/vbOJzkWOx7EzHLiSsDKdWb+tl+XBglpteQcU7dC6ok5377rtPFv9Qnxg7d+5Uz17kRFzUIK9R/zUr7wX9vUP3G13zvnvOCtN1T4QlXmPior//meDQM3imkmRlvSqynTfQqNFIkf2Uq0OjA93Ud955p8ymHnnkEdn/ApUxxqY89+SCMmHKfP3DqAN1JSPWT5euQcU8dE62bNmC/v37y3fXXX/99fIFoifDyZQVh0pKFGnqf9VZEY54/evQihUCRRvOuhgWbxQg8WoRTQFTFnXZZZfJ6mJUtPDpp59GrX4uR3SDHrbRQ89///vf8hdJ06ZNccEFF2DatGmy74xIOBnxGqAM2CBeJtlg8UYBvXijIWB68++kSZPkAxt6CSU9UKObkfphoBv8ZFuiccQu6JzYbDbZ9zGdK3o9PmW99EuFWr399JO/r4WKMIs34m4hQ4rXnLly0UAiYPFGASvxahGpgKkXrIEDB8ry3JtvvlkWKWh8/fXXLN4kDU289BJRjT179sj6vqeeeip69eols+FwMIuXmgxzt5DpBYs3ClgJ1xzhCHj37t3o0qWLfEBD5bkff/yxOkaBxZu8YSVegr5IBw0aJBtb3HrrrbImREVYiZe7hUwvWLxRwEq0waJatddkl35m6OalV5zXq1cPQ4cOlcUKZli8yRvBxEtQR+pjxoyRD92aN2+OxYsXq2OsCSZeDe4WMvVh8UYBK8FaBWW9XbsuMtxEVD939uzZstcreiAzb948/P777+pYIyze5I1Q4iXonK5evVr2kUwPS99++238+OOP6lgjocTL3UKmBxGKN9IqJ9q0ynCrQnylDq1V/cAKMNWltcZiewMI0YAiTKwkqw8r4WrQzUjCpb5e6cYNBYs3eaMi8Wrs2LEDnTt3lj2cUY2H48ePq2P8WImXu4VML2IgXv94YwMKvYg1lFZmySXeyLGSLUUo4WqUl5fL/l3D6eWKxZu8Ea54iS+++EK+Wojq/1phFm/FwtXuL134Wj5p95jV+IruZysqmse8Lcq0fhdo0HTafWexzDQnpuL1j1OGp2+3kDeIiEy4JwOLN3kjEvFWhFm8FWO6ZrXr2nCz6e8LDfN8AtO9FEhF81iMlyjbpG0SNbwwbV5GcRLi1SSkiyAH3ZjxiuGmk5ou3UIqx6FfzISrweJN3kgu8RI0zPhLLrHiFWjTBaxDnUcdbujnWp0i3YhtGa/FyUjHbiHr1L4JWRcPjZlwNVi8yRvJJ166RYxZpfX1rLtftdBd24FUNI9pvGlZStNm4xeCMo96X4l5tG0MbI6cPsS2qMGHbjgJU1wN/ovCPy6o6MR/AeMiFa+6XgX9dlZevMZlx45Yild2KxmHriWTO4Q8nR4oXVhajQ8eqSte03zmazuAiuax3haNCsWbgPsqEcRfvGJKWf5kNQ8tSzc/XSi+A08nQTdOnkBtGXI+7WQqyzeL13DRGZZ1kkUNCbhAIhGvfBuvywmPWxfis7lTcTktdbhDPZ3FQOipFAXOInyyZSxKty4SAo7s9UnJJ97AYQkXL90nNF3AOtR5EnRfJYIEiFcgDqj/AggcF/pnihKZ&#10;1C2kRjjipY7DCz0uFArR5hfYsG7TVixbuxlzV2zA1EXrsWrDVkPn4i4h3IL8fMwZNAjr582Du6jIsLzMCTvyXNvx5fpB2FMwCwWuyDojSi7x6pMPP4kVrzHBofs1wAEsXiYStJoMsegWUk8o8VKG6xWypb9LhWjHzF6DAeNW4PFRK/Dw8KW4d+hidBq6HCNnrfW9l0xmusXFWDZgAKbUr48FI0eiQIjXRa8R8nqV4gd1+ekeNocbXsdW/LT2MfzgGQuHywt7BN1aJl68anKghpWv4ide/bYosjWKlqDpNBGry2TxMpFgrkoWKwEHEy+9IsfjcmDJmi14ceIH6PbGcnQcsgS3D16Kjq8sx51CuHeKv3e9vgZvSfFSZiykUlKCZaNGYXXdutgibpL5TZpgbseOWCh+EaxZsgQ2jwcuIeJMEHC+qwR78nKApe1wdFUXuBz5sp9hq2mtIrHiZVINFm8U0Is3lgK2Ei9lrwXihh/1/hp0HbYMdwjB3jFkKToPXYbOr4i/QxbL6DR4Me7610pfxusUUl01YQJWnHceXEK6ThF2EfTG5HVVqmDhuediwaOPYp0QMGXFzri/7SJ+QQ/UXI4CHFpxL04suhKHl3aEy741w8VrzlwDiy6Yk4fFGwWsxKtFNAVsFi8JdFNePl7KWYVOr64QWe4ydHpliRCuEp1IuPRXCPjOV4SIKeOduRZejxt2IdL53bohT9xQJF4Srk0EydehDtssYslFF2HJK6/AJqan4gezcNIh8l1l+GjjGBxfdLUIId5lqnidnPEysYHFGwWshGuOaAhYL14qXigosOGFiSvR8ZUVUq6dXxHZrRDtneL/jiLrvXOoGP7qB1LAdw1ZiHteW463RWYsM14h0bVz5mBx8+ZStvooEEEiJgG7RaysUQOLH38cBUIwlClbiSdVQ5Huezi+4Ar8vuBKEVfg8OLbWLxMTGHxRgEr0QaLYN1ChoMmXqr25XY5RPa6Wj4wI9HKDFdktbeLrJeKGboNX4Jnxy7Hq1NX4725azFjyQbMX7kJG/MK5AM4qrPrKivDstGjsapePSlcynBJuFTsQEEZMEmY5LtOxOKnn5blvulS33eruxxe23r8vPguYE4r/DrvKhyf2wZHl96plPGyeJkYweKNAlaCtQrKeivTpFgTL0l35fo8PDxsqXyARmW5VLZ716tL8fd3lmPs7DVYsW6LLPul7FZ5+KbV4/VLk2o12IuKsOSppzBDZLXT774b82+7DUvPOQdrhWgp4yUBk3yp6OGD2rWxctw4+cBNL51UjHxXKXZumoYfFnfFb++3wrE57WTg/Zb4Nm9EitVqYFKNtBKvVh9XC6UmiukhQYX1DSvC/NCB6vlai1aLygpXg8RLN7nX7cDo91eLzJay3WXyYVqvN5djwvy12JJfgEKP0mCCGktQdisz3CDhEhnslnXrMLVbN6yaNUsIyYWVs2djQb9+WNCmjcx0Sb4kYRLwqp494dy2zXJZqRFO5ItMd8+6t/H7zNY4Pr0Fjs1qI+Jy8X8zfLPkL7DRW5IjyHYpWLxMJKSNeGUDCss6f0bB+usTVka8/vkU2f81QLbRFK4GiZckarfbkD1+Of74zyXoKMRLRQqrNuTJRhOU1VqJIVRQjYX8zZtRsGmTFLG7pAT24mKsX7sWC/r2xbJGjfzZ78MPw5Gy4nXK4oU9a0bi12nN8WtuCxzNbS1CZLy5l+L4tKZCyKPFNDvEtIF1pUMFi5eJhPQQb8hK33pR6lvP6Iar84fXK5JJ2GLerKx7fbKNhXA1SLyUxW7Nz8ez767EzS8vw3PvrcDmPCXL1bdIizRIvrJBhfaZyoALC2VRxMqpU7GkdWspX0/KildksS4vPlr9Jo5NboJjORfi6JSWIoR8Rfw66Tz8OO8uFLq2ysYU+vmMy7EOFi8TCWkh3sAWOXpIlLqiAV9WbBKvGKctI7CVjR6jePUZb6yEq+EXbwGeGbsSXYctUTPdyDt1CTekgMvKsGbhQixr2hTeLl1SUryy4xuXBwemt8ev48/CkYlNDPHbuAb4au5DIts17ZvTDbuz4pocLF4mEjJEvCZRSvmaxBt2U0WTyMV8tWMsXA1NvHlCvH9/eymG566sdKYbdpSW4oPXX8f6xx+XTYotp0niKBDy3ONZh1+mXoNj75yDI+8K4eri19EN8PWch2Er+lDtLIgyXSf2r30dpXnLKmxMweJlIiE9ihpIlGEVNQh8kq2MeHXLiyOaeLfk5eG5nM0YPH0znA67HGYlg2gG1d/NW7cOH4wfn5INKQrcJfh0yWAcHdUUR0aKGNXMGG+ei0M5N6Hcvg75bvpicSLfuw0/TfwDvpv2ZzGsLGCZ+mDxMpGQJg/XlN6YDLIU8lQ+GkUZnYw3weLdvBnZM1x4eEIJ3l++CUVuKhKwFkK0gnotWz9/Pha89FJK9mBmcxVi29oZ+HlUOxx5/UL88kZzfwynvy1w7F/n4Iext8GTvwFbi3Zgxwfj5bQ/jrsdHocNdmfwIh0WLxMJaSJeQpWvVgTgEyeJUjfcJ9joibdjx9kx7ZVMw1fGKzLe7Klb8MCUT/DkFA/mf7ARhW6lNZuVFCoVQihukeFSE+OZ992HJQ88AHd5ufW0SRxSmi43fhh1K44MvgCHh7aQcUjEkX9egF9eaSo+t8RRMe6nEdfju3c64uBrrXDs5bPxw8g/oGjjUiHv4Jl+IsVLr3yna/Cnn46oQ5hkJ43Emzi0B2vx6BZS1su1FWDo9A14aPKH6D79Uzye48bkRRthL9iqlvn6Baw1oNBLIpJwFxfLfhrm9O6NSeKLa2XXrnCl4sM1Eq+I//7rBvwysAkOvXgxDr7YEkdebIEfRt+H/w1ui8ODLpLDf8luKkLI+YXmODzwQhx480/YXrBGlhNbLZsiEeIl4V500VicfvoI1KjxesyfMTDRg8UbFFOmLCLYAzxzVbJYdwvpddmFaDegR045ekz/BI+I6JFThpembsK8FRthy98qhGuX9XoXrtyE5Ws3wx1BM1+qVkZVyVxCuhtWrMBMIducqlUxTRyDFd27w52y1cnc+HT6YBx+thkO9rsEh/s0xQ/DOssvpo9mD8OhZ5vL4Qf7tcTB/uJv/0txsM9FOPL6n/BJwWrkJ4l49cLVX3cs3tSBxRsF9OLV3wix6haSihs2bs7D05O24uFpn6DntD3oOf1jdJuyB70meNE/ZxPenbceH6zdhH9O+gD9xq9Bfn6BbGqslwVlxiRZqsNLD8yo4YRsPCGGbVy1CgteeAGTWrTAFCHcXBFTRSzs1i0lxWtze/Hh4hxsXzYd3794Cw491QJHnmiCL955CltKd8O9dTO+y75BDG+JQ4Nvw8EnhZz/cQkOPXEh/u9f96LQaYMtwWW8VsLVX28s3tSBxRsFzDeBPqIpYE28StbrxPSlG9Ezpxg9Znwq5PsheubuwSO5H6O7kHD3SeXo9Z4dj7xrQ7eJZRg9l7Jem5C2P/Ml4dqEZKnXsc3r12Pt3LlY+vbbmNWjhxTuRCFaynK1mEziHTgQ7hSsTkZFDe78zfhi+GPYN+IJHHy0GX557GJ8OfFF5HtL4CgqxWezR+FQjwtxLPdFHB31KA78RUhYTPfdgNtRmLcBdi89VLRu0RZL8YYSrhYs3tSCxRsFrG4Ec0RDwHrxyoYNDhvGzt+EnpPL8ch0Id/c3ULAu5SY/hF6zdiLXkLCVBTRa2IRZi7bKIStFCXYhSTmPfccpt5+O6bcdhtyrroKkxo3xsRq1ZAjBEsZrpblauKdcvXVsl8HEraVfJI7nLB5vPh4/GAc7tIEB7pfjEMPXogvhvREXsk2FAopH8y+Bz/fdx6+mTgE+9ctwoEHm+FAj8tw+MGL8J++HeHYuA52sQyr5cdCvOEIVwsWb2rB4o0CVjdCsIhGt5AUdLO7nUKidhsmLNyEv0xwo9u0j9BDiLbX9D1KBjxVhJBwLyFkegjXO8eJpWu2wOP1yBdcjr/4YpnFkmi14oTpIqRkRZB0NflOveQSrJkzJ6V7JqOst6CkHHveexU/d74IB7Ifwv5RA1BYXCKP6X+G9cHR+y9F8eI5KJ49BYduP0+I+BL8dPv&#10;52DnoL2J+8WtBfGlZLjvK4n344cWoXv11y2vIKli8qQWLNwpY3QhWQTdHZVq4mcVLQeW99A41eqg2cMoW9JhUgq6Td8vih14zhIRzFQn3oOw3pxTvL98Mr1d5s3DO5ZdjhpCqltGSYEm4eglTTYap110n30DsKhE/yXWyScWg8muS7+5Rr+Lr+ztg15zpKNyxE/l2B8rE+G8/mAu3+GLaPmksDnRqix/uvQp7b24B98zJsJcEb0QRbfF+881BPPnkKtSrN0J+WVtdT/pg8aYWLN4oYHUj6KOywtWwEi8FVTGjNwxT5zm5i9fjhamb8NhEN7qOL8HDk3eiJ5UB55RjvMiMqXhCe6V7Ttu2Urz64gStiIHKdyc2bow5f/87tqxfL6uV6deZ0iEy14KiEhS+n4vdzz4B74xpsBcWKxmx2wubtxBFSxdhx9xZ2NP9HuwaMRS2UiFd03HXR7TFqxGugFm8qQWLNwpY3QgU0RKuRjDxauFyOVHodiBfSHXZms2YuGA9+k7chK6TtmPEnC1w2vNlzQYqoyXxTmjZUkpWK2qYWrUqJtSujUkiE57z1FOyaMFZVCSrllmtL9XDVloO+8YN2C3EW1hQoBQl0DiSqNhvZ14eCieOQ5EQqm9ckIiVeDWCC7ifCGO1Rxm+1plWwzUCGwMFNCYKoKJ5zOtUpvW3GNWg6bSeAiNF38tgasLijQJ62VJEW7gaFYlXC6qXShlwsceBN2aswYBJ65C3laqTKfKQD9eEaKZ364acK65Azo03YkbXrpj/wgv4YMIE5G3eLN/JJl/tHqRMMz1CHEuPR3xhueAU0jSPl8Nc1DtZxccg1uLVCCZguuacb/3JVNfcLElz0/pYidc0XqKsW1s19QBo8HBEsHgZQayFqxGueLWgqmMr1uVh3aY8ywYUdhEFJAwxzqk2mKDsljJifeu3jI4wj0O8xKthFnCtWsPCEC9Bwyz6pNaImXgF2nRW66Bm+qYsWWKaVumGVZsudV85z+KNArEWrkak4qUI1WRYNp5QI11eYJmoiLd4NTQB1679BorfuT0M8ZLjgrwWS4sKxRtqHtN407Lkm2LMRQwkV912GoolLMTbKTefM14mfpjFa7eLrNZbhPLt27FdRHlZMdz081kvBBFub7E6zTaUlRTBJadxwlOoDt+2DdtMQcPKS0tQ6BHZr255qRgulxuFRaUoKxf7pu6vb9+c+mnFcXW6UVxaLo+n+ZjI40LHubwcxYVeWaNEmzdR4tWjSMmguTDEaxpvlY0aqGge63VqWIk35HazeJlEYxCvyFA9HifWLpiCwc/3x7N9+2HwsAlYXeD2Z7hiGrfLhpVzJuClfs/huX4D8dqoKVhn88Dr3IolM94Tw/uj//PPY8CAAf4Qn/sPHIghb4/FovVb4C0qNEgmZcLpgld8uTjsmzBv+mi8OnQAnu8v9nfQILw86j3MW7VV7FsxvB61DFdI152/GhPfeAXPi+PyvP6YqNFfzD9kyBBMmbcMtsIi+VJRmjd1xKsfVpFErahoHqt1qlBxAk1nKdMg283iZRKNUbwulBQ7MfPVHqiv/qxrcMl9yM0rRrEqAxJJkXcrJmbfj9PUaZpe1xOzt5ag3L0eb/2jI2qpwy2jbn20uv4u9H9rLjY7vELiOqkleTjdHhS5bZg/4V946O4OaH5eHeO+nVYfTdvdhG59/oUlG+zwej1wuYtQuCkXXS472zitKapUrYbGzS7HA/94DcsLxJeY+KJLpHiphRt1C7lj3B0ViFd5wOWfJp7iNT4Qo8zXtx00v24ekquhqMGXIWvbz+Jl4oiVeGf9qycaqkJodOn9mG4l3kFdUEedpln7XpijiffpTqitDq9Z90w0u6QVWrdqhVYiLjynEaqo406/8A94afJyuDzi57VJcMkYTpdbyHALZrzxLK4+v77ch6ys6jjzvGZo3bo1Wl9yAerXUPYtK6sOrr27P+aut8NDdXs35+KBNo3VcVVR/9yLcKmYh45JK/G3ZZOGqK4el6zTLsbToxaisKwQzgSIV9+kmLqFtH64pu2nEsaaBLESr36diiANopXQdDp56h+umdavFE8oy8rOVrJjKWcxzLjM1IHFm0JYive1XjhTvWDPat3FUryTXngA9eQ0VdCiw6M+8Y58pjPqyuHV0faOpzBno5CWvUAK5IPZE/HorW1Rp6oYX7U22ncfglUFLt9P6+QNJzzeQqzKHYybm9eVxyXrtHNx219fwKwla2Gz22DbuhQ5Q5/EtY3OUMYL+XZ+djQ2e8pQvGU6Hmx7njq8ProPnY58kT3bCvLFvE5sXT0OPdqfh6pyfD10eupt5JWVybL1eInXqg8HesDLDShSBxZvClGReM9u0w3zvbuxe3s5yoQMysq3Y9fOQsx85WGcLqcJFK+SCVfHFZ36YGGBkLb4ye12e1FS5sLcUX3QulZVuezm7XtgypoCeDzJ3UGO0yV++heswpBef0Bd+tLIOg1XdxmAxVs8KCnyin1zw+0pRGmRC1Oe74YmDWujVq0aaNK5D5ZtFsO3zhTiPV/uc1ZWAzw2aim2f/Qxdu7Yjh07duGjj+14s8cNqCHH18Fdfd5FfllpXMQbqtOc6Is3MFtO1ewyGWHxphAVibd+05vx8nu5yJ2cg0mTJmFSzmRMmzoOA3veohYpBBdvuzufxoKthSiUVcvcKCl3Y9HYfrhcFW+zax9Gzqr8pBcvSXXT/NG4+4qz5HZXbXwFnh+7CIVUT1n3gFA2IsnfgBXLFmPxYhEr16HAKfZ/sz7jrYs7nhiCSdNyMTknB5OnTsV7w3ujfZPqqHpKHfFl9ADeXbAFRYXimIhzEivxhtNLGWe8qQWLN4WoSLwVR3DxXnl3P6zw7MTObeUoL9+GgtXz0efeDjj9FBp/Kq68fxCW54uf8e5kbsnmQmGxA7PfegaX11O+MBpefjtGzd0Mr8cTUC1OlgV7hWyFlL3iC0V5uKYXb/A4pdbVeGb4CpRuLxVCV85JtMUbjnC1YPGmFizeFKIi8VapegpOrVkTtUTU1EWN6qeoD8qClfFWQb1GTXBF+xtwww0dZLRpeaH6czoLNRq2w7PvLoSTHq4ldbUyId6SfEx79XG0rKqI9/xr7sJ7i20yU6+oPrIzQLxVUK36qeIY1lKOZS3x99Tq6ri6OOuSP2Hw5KXwFkf/4Rp3C5nesHhTiIrE2+jiznh32UZsWrsaq1atwqrVa7Fx3XKM7HO3T7DW4g0SVarhrObX4S8vT8UGm1e2grMSVvIEideGmcOeRKuainjPufJOjFmw1VK8TrcXZduo7HYHtovM1estNon3DDzwwnis2rgJa1avwpq167Fq+Sz07XwZasjy4yw0vvmvmLvBgyKPM6riDd4pjnWweFMLFm8KUeHDtcsexGzHdmwvEZmb+PlcWFSCbWVOTH25awW1GqrirGZX4r5H/oq/PvooevXsiZ5/eRT/GPwaZq3cCI/I6CJ5WWYiw1NUiKUTX0KHJop4azS7AUOmrkSR12sQL3WNaVu/AMOzn8Pf//53PN3ndczf4ERR3gx09Ym3AR59Yz4KhZyLxXLpmBaXbYd90Qhcf35NOU2VM9pj6Mw1Qvhe+VYPs3j3OcVN9q8QsUKdUMeJ74FO2vjsg8i6ehWyTh2BrKrcLWS6wOJNISoS78lUJ9PKeK++dwDWlHyEj3Z9iJ07d8rYXl4mMrnUajLs9BTBvmIyHr7pAnlMsmqchbv7jMImt8hmvdREmF7w6UZpaTEWjHgMZ8sybBEt/ozcNS6U5c/S1Wqojx6vz4azuES2BqR53d4SFG6cgrta1pfTVDmlLbJzlsNdWnhy4lWjk5hOwyBeLSoQcGTiDayxUPnuIplIYPGmELEUL9VqmJ/nla2wzDJLpXCK4+L1bEXO4B5oWls5LrUuuA7PjJyOPFcRtpVvF1GMDfNG4b4rmqhl39Vxx7PvYItnG0q2zNAVNTTAX99ajLJdu8U8ZSin/hrEvPOG/RUX1KuiTFPvWgydEV7Gq5crkZvjH0eR/ZEyPFh1MvwA3KYTcFVTt5CRiVcvVqVFWOW6i2QigcWbQlRevFkB4lWqmVVLG/FSuNxeuLcuxS&#10;uPdcSZagu1qg0uwq13P4Qnej+FJ/76ENq3aCiHUzHLedf2wOSVm+EtLjWV8dZEm1vvx+O9n8Tf/vY4/vZEbzzW635c3vg0dfypaNNlIJbbPSgUx82qjDeUeIn8Ff7xWULEJLZg4jWIeqaxDLhm9ZbqNmUhO1vtE0EKVd+0lj6TUC3EKodp01YgXvX/fLX1mKHTG5Ogff0wqMNztVZoJHlfa7XA+S2XnUaweFMIK/HOHNrdJ9XTW9xj2VfDhIH34lR1mibXPOLrq+HNp273NX9t9ccnMS9NxEut19zeQni2rsTwPl3Rqkl9VK+i7Kc+atQ9Ezfe8zimLNkAT5EHLg/VapiGP1/aKGBac1Sv0wiX3NkbOSsKUFTsFtmuda2GisRLWay+WCFXfLYSr7nIQpOifAj3SF9xHm/CMvE/IZvYqvIzdEBDUpNZrZV4lfmUpJfGB+6zrykvLUd89i1X6/yG/g8lXjGPYflqhh2wjcGWnUaweFMIs3gLC51YMnEw7upwPa69pj3u6j4Qi/K9MvuSAqKf3e58zH0nG3dcdx2ubX8juj4xBMsKClHs2oxpbzyH29tfh+va34KefYdjRb47BWouhB/UAs/jsmP5oul4NftR3Hv3jbihQwd0uOVm3N6zN4a+NxsbC1woEtKlcmynS0yftxCDet6HDu3b44Ybb8SNpujQoT06d34Ig0dNx/ICap7slVXJ6JyclHgF+kyWihsCxCuGaeO1afQYxEUYZCUkp/5P0/nFV5F4K854/WvUTR9KvL7hStGG6l1asb+YI9Sy0wgWbwqhiZducE0u1AKLbn6viGB1VZWGAuo0bpechvrptRpunjfVg/ZROz5WEdggRGTLHo/ltObQH7NYijdbHRdsGaHFSx/p57qQXbY2zEpm+mEs3ljD4k0hvvrqKxQUFMiXWdJNLoOyLdPNbw4abjVNsOHpGHJf7cq+avtrpw5zAqal4eIXhTqdVVjNq52XvXv3qmdLobLitSoDDoDEpZOTvqhBIsZ3ys5Gtk/OZpkpIvTLOwzx+ooN6GMnY1GDr1xWt9xIxBts2WkEizeF+PHHH7F7927s2rVL/uVInqBzQvHtt9+qZ0uhMmW85nJdGhcMf9eJQlq+h2saJFL9Qyr67J9ezmOwbBjiFf/7HpSZpvVvi78bx4jEG2LZ6QKLN4WgG/L48eMcSRzmYoKKxGvIaNXGFHIZJiEHy5YtIZEZxBtlDBKNMrFcdhLB4mWYGBJKvOZ6vFpmR+I1jAtWxBAMFm/Sw+JlmBhiLi4IFvqf05+HOQ9FqOIHJnlh8TJMDKlIvOaqYcTW5dbTWgWLNzVh8TIMw8QZFi/DMEycYfEyDMPEGRYvwzBMXAH+H0uz+KayEP6LAAAAAElFTkSuQmCC\"></TD></TR>
  <TR>
    <TH>Identifier:</TH>
    <TD colspan=\"3\">CoSES_Models.Houses.SF2</TD></TR>
  <TR>
    <TH>Version:</TH>
    <TD colspan=\"3\">1.0</TD></TR>
  <TR>
    <TH>File:</TH>
    <TD colspan=\"3\">SF2.mo</TD></TR>
  <TR>
    <TH>Connectors:</TH>
    <TD>Electrical Low-Voltage AC Three-Phase Connector</TD>
    <TD>lV3Phase1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the condensing       
                                              boiler</TD>
    <TD>StandardControlCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Environment Conditions Connector</TD>
    <TD>environmentConditions1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the heat pump</TD>
    <TD>StandardControlHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard Control for the 3-way valve to switch the connection to the   
                                                    thermal storage</TD>
    <TD>StandardControlHP3WV</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                                                   pump</TD>
    <TD>StandardControlST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                                                    supply and return is 
      constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>DTH_HEXin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                                                    negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Air source heat pump modulation in percent</TD>
    <TD>HPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switches between the inlet of the HP into the TS (true: upper storage  
                                                     connection)</TD>
    <TD>SwitchHPTSPort</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Air source heat pump - modulation of the auxiliary heater (percent)</TD>
    <TD>HPAuxModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating mode (true) or cooling mode (false)</TD>
    <TD>HPMode</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Parameters:</TH>
    <TD>Volume flow generation and electrical power calculation</TD>
    <TD>pump2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature controller wiring for heating system</TD>
    <TD>heatingUnitFlowTemperature1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW_demand</TD>
    <TD>dHW_demand1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                                              components</TD>
    <TD>grid1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar Thermal</TD>
    <TD>WolfCRK12</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Specifies a Thermal Volume Flow Connector</TD>
    <TD>defineVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Extracts the characteristics of a Thermal Volume Flow Connector</TD>
    <TD>extractVolumeFlow1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Measurement of pipe temperature and volume flow</TD>
    <TD>measureThermal1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Electrical power grid for connection of maximum six 3-phase AC         
                                              components</TD>
    <TD>grid2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat storage with variable temperature profile with 5 inlet ports</TD>
    <TD>WolfBSP800</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Change between inlet ports of thermal storage</TD>
    <TD>switchPort1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>WolfCHA10_GC</TD>
    <TD>wolfCHA1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Connection of 1-phase to 3-phase AC components</TD>
    <TD>phaseTap7</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>TColdWater</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heated 3-zone-building with changeable heating system</TD>
    <TD>simpleHeatedBuilding1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>qvSTpump_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Boolean expression</TD>
    <TD>HP_EVU</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Logical Switch</TD>
    <TD>logicalSwitch1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Boolean expression</TD>
    <TD>SwitchSignal_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature difference between flow and return temperature</TD>
    <TD>CBDeltaT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature of CB</TD>
    <TD>CBTmax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature of CB</TD>
    <TD>CBTmin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum power of the auxiliary heater</TD>
    <TD>PAuxMax</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal heat output at A2/W35 (not maximum heat output)</TD>
    <TD>PHeatNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal cooling power at A35/W18 (not maximum cooling output)</TD>
    <TD>PCoolingNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal COP at A2/W35</TD>
    <TD>COPNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Nominal EER at A35/W18</TD>
    <TD>EERNom</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, solar thermal collector is CPC collector, else, solar thermal 
                                                      collector is a flat plate  
           collector</TD>
    <TD>CPC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inclination angle of solar thermal collector</TD>
    <TD>alphaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Orientation angle of solar thermal collector</TD>
    <TD>betaModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in series</TD>
    <TD>nSeries</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of solar thermal collectors in parallel</TD>
    <TD>nParallel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Effective surface area of solar thermal collector</TD>
    <TD>AModule</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Absorber volume</TD>
    <TD>VAbsorber</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature within the thermal storage</TD>
    <TD>Tmax_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature level to calculate the stored energy (e.g. return          
                                             temperature of consumption)</TD>
    <TD>T0_TS</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, the temperature profile at simulation begin within the        
                                               storage is linear, if false the 
      profile       is       defined       by a             temperature          
                 vector</TD>
    <TD>TSLinearProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of upmost heat storage layer at simulation begin</TD>
    <TD>TSTupInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of lowmost heat storage layer at simulation begin</TD>
    <TD>TSTlowInit</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Vector of temperature profile of the layers at simulation begin,       
                                                element 1 is at lowest layer</TD>
    <TD>TSTLayerVector</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Number of people living in the building</TD>
    <TD>nPeople</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nFloors</TD>
    <TD>nFloors</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>nApartments</TD>
    <TD>nApartments</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heated (living) area (e.g. 50m² per person)</TD>
    <TD>ALH</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, use standard area-specific heating power, else define it      
                                                 manually</TD>
    <TD>UseStandardHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Area-specific heating power - modern radiators: 14 - 15 W/m²; space    
                                                   heating: 15 W/m²</TD>
    <TD>QHeatNormLivingArea</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating system exponent - radiator: 1.3; floor heating: 1 - 1.1</TD>
    <TD>n</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal flow temperature - radiator: 55 - 75°C; floor heating: 35 -     
                                                       45°C</TD>
    <TD>TFlowHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Normal return temperature - radiator: 45 - 65°C; floor heating: 28 -   
                                                          35°C</TD>
    <TD>TReturnHeatNorm</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference indoor temperature</TD>
    <TD>TRef</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximumg flow rate in Living Zone</TD>
    <TD>qvMaxLivingZone</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TLiving_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TRoof_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Initial Temperature</TD>
    <TD>TCellar_Init</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If the presence is used, individual presence data has to be provided,  
                                                     else standart presence is 
      used</TD>
    <TD>UseIndividualPresence</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with presence timeseries (presence in %; 0% - no one is at home;  
                                                     100% - everyone is at 
    home)</TD>
    <TD>PresenceFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If individual electricity consumption is used, individual consumption  
                                                     data ha to be provided, 
      else       standart       load       profiles       are       used</TD>
    <TD>UseIndividualElecConsumption</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>File with electric consumption time series (consumption in kW)</TD>
    <TD>ElConsumptionFile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Table with electric consumption time series (consumption in W)</TD>
    <TD>ElConsumptionTable</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>YearlyElecConsumption_kWh</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>ElFactor</TD>
    <TD>ElFactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, night time reduction is activated, else temperature is        
                                               constant</TD>
    <TD>ActivateNightTimeReduction</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionStart</TD>
    <TD>NightTimeReductionStart</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>NightTimeReductionEnd</TD>
    <TD>NightTimeReductionEnd</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at night</TD>
    <TD>Tnight</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, presence will be used to define the temperature (if less      
                                                 people are at home, less rooms 
      are       heated       and the             average             temperature 
                  will             decrease)</TD>
    <TD>VariableTemperatureProfile</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature, when noone is at home (TRefSet = TMin + (TRef -   
                                                    TMin) * Presence(t))</TD>
    <TD>TMin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true: DHW consumption data repeats weekly</TD>
    <TD>WeeklyData</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Data File</TD>
    <TD>File</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>DHW Table Name</TD>
    <TD>Table</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>V_DHWperDay_l</TD>
    <TD>V_DHWperDay_l</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Factor, with which the DHW consumption gets multiplied</TD>
    <TD>DHWfactor</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Photovoltaic Modules</TD>
    <TD>photovoltaic1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>PV to AC Grid Inverter</TD>
    <TD>pV2ACInverter1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>HydraulicSwitch between thermal storage, district heating and heat     
                                                  generatrion</TD>
    <TD>hydraulicSwitch_TS_DH_HG1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBControlIn</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>CBin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>HPin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch between two Real signals</TD>
    <TD>switch5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Set output signal to a time varying Real expression</TD>
    <TD>AUXin_StandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the CB turns on</TD>
    <TD>TStartCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the CB stops</TD>
    <TD>TStopCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the heat pump turns on</TD>
    <TD>TStartHPhigh</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the top, at which the heat pump stops</TD>
    <TD>TStopHPhigh</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the bottom, at which the heat pump turns on</TD>
    <TD>TStartHPlow</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature at the bottom, at which the heat pump stops</TD>
    <TD>TStopHPlow</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the auxiliary heater turns   
                                                  on</TD>
    <TD>TStartAUXhigh</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the bottom, at which the auxiliary heater      
                                                 turns on</TD>
    <TD>TStopAUXhigh</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the top, at which the auxiliary heater         
                                       stops</TD>
    <TD>TStartAUXlow</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum Temperature, at the bottom, at which the auxiliary heater      
                                               stops</TD>
    <TD>TStopAUXlow</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Required temperature difference between upper layer and DHW set        
                                               temperature</TD>
    <TD>deltaTLowDHW</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on temperature difference between collector and storage         
                                              temperature</TD>
    <TD>deltaTonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-off-temperature difference between collector and storage        
                                               temperature</TD>
    <TD>deltaToffST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum temperature difference between flow and return, qv=qvMin</TD>
    <TD>deltaTFlowReturnLowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum temperature difference between flow and return, qv=qvMax</TD>
    <TD>deltaTFlowReturnUpST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Minimum volume flow of circulation pump</TD>
    <TD>qvMinST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Maximum volume flow of circulation pump</TD>
    <TD>qvMaxST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inclination angle of the PV system</TD>
    <TD>alphaPV</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Orientation angle of the PV system</TD>
    <TD>betaPV</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Installed peak power of the PV system</TD>
    <TD>PVPeak</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>WolfCGB20 validated with CoSES mearusements</TD>
    <TD>wolfCGB20_GC1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>Results:</TH>
    <TD>If true, standard control will be used to control the condensing       
                                              boiler</TD>
    <TD>StandardControlCB</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the CB modulation</TD>
    <TD>CBinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the heat pump modulation</TD>
    <TD>HPinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard control value of the auxiliary heater modulation</TD>
    <TD>AUXinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the heat pump</TD>
    <TD>StandardControlHP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switches between the inlet of the HP into the TS (true: upper storage  
                                                     connection)</TD>
    <TD>HP3WVinStandardControl</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Standard Control for the 3-way valve to switch the connection to the   
                                                    thermal storage</TD>
    <TD>StandardControlHP3WV</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switch-on/off of solar thermal pump</TD>
    <TD>CPonST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvRefST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal collector temperature</TD>
    <TD>TCollectorST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature of solar thermal heat storage connection</TD>
    <TD>TStorageSTConnection</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature solar thermal</TD>
    <TD>TFlowST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature solar thermal</TD>
    <TD>TReturnST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>If true, standard control will be used to control the solar thermal    
                                                   pump</TD>
    <TD>StandardControlST</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>PV power</TD>
    <TD>PV_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Produced energy of the PV system</TD>
    <TD>PV_E</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler flow temperature</TD>
    <TD>CB_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler return temperature</TD>
    <TD>CB_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow condensing boiler</TD>
    <TD>CB_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas flow condensing boiler</TD>
    <TD>CB_S_FG</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Demanded fuel volume</TD>
    <TD>CB_VFuel</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of condensing boiler</TD>
    <TD>CB_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas power of condensing boiler</TD>
    <TD>CB_P_gas_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of condensing boiler</TD>
    <TD>CB_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Gas input of condensing boiler</TD>
    <TD>CB_E_gas_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating efficiency</TD>
    <TD>CB_Efficiency</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump flow temperature (output of the outdoor unit)</TD>
    <TD>HP_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump return temperature</TD>
    <TD>HP_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump flow temperature - Low temperature</TD>
    <TD>HP_S_TM_HC_TL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat pump flow temperature - High temperature</TD>
    <TD>HP_S_TM_HW_TH</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow heat pump </TD>
    <TD>HP_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of the heat pump </TD>
    <TD>HP_P_heat_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of the auxiliary heater</TD>
    <TD>HP_P_heat_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Total heat output power of the heat pump</TD>
    <TD>HP_P_heat_Tot</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cooling output of the heat pump</TD>
    <TD>HP_P_cooling</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Total electricity demand of heat pump</TD>
    <TD>HP_P_elec_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of the heat pump </TD>
    <TD>HP_E_heat_HP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of the auxiliary heater</TD>
    <TD>HP_E_heat_Aux</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Total heat output power of the heat pump</TD>
    <TD>HP_E_heat_Tot</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Cooling output of the heat pump</TD>
    <TD>HP_E_cooling</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Consumed electric energy of heat pump</TD>
    <TD>HP_E_elec_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Coefficiency of performance</TD>
    <TD>HP_COP</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy Efficiency Ratio (Cooling)</TD>
    <TD>HP_EER</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal flow temperature</TD>
    <TD>ST_S_TM_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Solar thermal return temperature</TD>
    <TD>ST_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow solar thermal</TD>
    <TD>ST_S_FW_HC</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output power of solar thermal</TD>
    <TD>ST_P_heat_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat output of solar thermal</TD>
    <TD>ST_E_heat_produced</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature consumption side</TD>
    <TD>TS_S_TM_HC_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature consumption side</TD>
    <TD>TS_S_TM_HC_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature producer side</TD>
    <TD>TS_S_TM_PS_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature producer side</TD>
    <TD>TS_S_TM_PS_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Flow temperature to fresh water station</TD>
    <TD>TS_S_TM_HC_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Return temperature to fresh water station</TD>
    <TD>TS_S_TM_HC_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 1</TD>
    <TD>TS_S_TM_BT_1</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 2</TD>
    <TD>TS_S_TM_BT_2</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 3</TD>
    <TD>TS_S_TM_BT_3</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 4</TD>
    <TD>TS_S_TM_BT_4</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 5</TD>
    <TD>TS_S_TM_BT_5</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 6</TD>
    <TD>TS_S_TM_BT_6</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 7</TD>
    <TD>TS_S_TM_BT_7</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 8</TD>
    <TD>TS_S_TM_BT_8</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 9</TD>
    <TD>TS_S_TM_BT_9</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Thermal storage temperature 10</TD>
    <TD>TS_S_TM_BT_10</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Energy in thermal storage</TD>
    <TD>TS_E_Storage_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>State of charge of the thermal storage</TD>
    <TD>TS_SOC_BT</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature before mixing unit</TD>
    <TD>HS_S_TM_VL_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat Sink flow temperature after mixing unit</TD>
    <TD>HS_S_TM_VL_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature</TD>
    <TD>HS_S_TM_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink flow temperature hot water</TD>
    <TD>HS_S_TM_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heat sink return temperature hot water</TD>
    <TD>HS_S_TM_HW_RL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Temperature in the house</TD>
    <TD>HS_S_TM_Room</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow after mixing unit</TD>
    <TD>HS_S_FW_HC_aM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow before mixing unit</TD>
    <TD>HS_S_FW_HC_bM</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow domestic hot water consumption</TD>
    <TD>HS_S_FW_HW_VL</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating power</TD>
    <TD>HS_P_DemHeatHC_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water power</TD>
    <TD>HS_P_DemHeatHW_is</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating Energy</TD>
    <TD>HS_E_DemHeatHC_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Domestic hot water energy</TD>
    <TD>HS_E_DemHeatHW_consumed</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - set temperature (power is constant)</TD>
    <TD>CBIn_TSet</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Condensing boiler input - heat power (temperature difference between   
                                                    supply and return is 
      constant)</TD>
    <TD>CBIn_P</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Outlet temperature of district heating heat exchanger</TD>
    <TD>TDH_HEXout</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Inlet temperature of district heating heat exchanger</TD>
    <TD>DTH_HEXin</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Volume flow to heat exchanger</TD>
    <TD>qv_HEX</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of solar thermal pump</TD>
    <TD>qvSTpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Reference volume flow of district heating pump - positive: feed in -   
                                                    negative: extraction</TD>
    <TD>qvDHpump</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Air source heat pump modulation in percent</TD>
    <TD>HPModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Switches between the inlet of the HP into the TS (true: upper storage  
                                                     connection)</TD>
    <TD>SwitchHPTSPort</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Air source heat pump - modulation of the auxiliary heater (percent)</TD>
    <TD>HPAuxModulation</TD>
    <TD>&nbsp;</TD></TR>
  <TR>
    <TH>&nbsp;</TH>
    <TD>Heating mode (true) or cooling mode (false)</TD>
    <TD>HPMode</TD>
    <TD>&nbsp;</TD></TR></TBODY></TABLE></DIV>
<H2>Description:</H2>
<P>&nbsp;</P></BODY></HTML>
"),
		experiment(
			StopTime=5184000,
			StartTime=0,
			Tolerance=1e-06,
			Interval=0.01,
			__esi_Solver(
				iMode=1,
				typename="FixStep"),
			__esi_SolverOptions(
				solver="FixStep",
				typename="ExternalFixStepOptionData"),
			__esi_MinInterval="0.01",
			__esi_AbsTolerance="1e-6"));
end SimX_SF1_PHIL_Ore;
