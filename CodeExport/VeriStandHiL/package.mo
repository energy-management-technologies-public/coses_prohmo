﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within CoSES_ProHMo.CodeExport;
package VeriStandHiL "Hardware in the Loop models for VeriStand"
	annotation(dateModified="2022-02-15 08:29:34Z");
end VeriStandHiL;
