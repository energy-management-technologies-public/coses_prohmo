﻿// CP: 65001
// SimulationX Version: 4.2.3.69223
within CoSES_ProHMo;
package CodeExport "Code Export for VeriStand, Dymola or other systems"
	annotation(dateModified="2023-05-03 12:45:51Z");
end CodeExport;
