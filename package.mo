﻿// CP: 65001
// SimulationX Version: 4.2.1.68046
within ;
package CoSES_ProHMo "CoSES thermal Prosumer House Model"
	constant String LibraryPath=classDirectory() "Path of the library" annotation(Dialog(visible=false));
	annotation(
		dateModified="2023-08-04 14:53:33Z",
		Icon(graphics={
			Text(
				textString="CoSES",
				fontSize=10,
				lineColor={0,75,192},
				extent={{-136.1,89.90000000000001},{137.2,6.6}}),
			Text(
				textString="ProHMo",
				fontSize=10,
				lineColor={0,75,192},
				extent={{-193.5,9.9},{193.2,-80.09999999999999}})}));
end CoSES_ProHMo;
