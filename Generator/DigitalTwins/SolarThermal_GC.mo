﻿// CP: 65001
// SimulationX Version: 4.2.3.69223
within CoSES_ProHMo.Generator.DigitalTwins;
model SolarThermal_GC "Green City Solar Thermal, slightly adapted"
	GreenCity.Interfaces.Thermal.VolumeFlowIn ReturnST "Return port of solar thermal collector" annotation(Placement(
		transformation(extent={{590,-315},{610,-295}}),
		iconTransformation(extent={{190,-110},{210,-90}})));
	GreenCity.Interfaces.Thermal.VolumeFlowOut FlowST "Flow port of solar thermal collector" annotation(Placement(
		transformation(extent={{590,-230},{610,-210}}),
		iconTransformation(extent={{190,90},{210,110}})));
	GreenCity.Interfaces.Environment.EnvironmentConditions EnvironmentConditions "Ambient Conditions Connection" annotation(Placement(
		transformation(extent={{250,-260},{270,-240}}),
		iconTransformation(extent={{-210,-10},{-190,10}})));
	protected
		function angleMinMax "angleMinMax"
			input Real angle;
			input Real angleMax;
			input Real angleMin;
			output Real angleOut;
			algorithm
				angleOut:=min(angleMax,(max(angleMin,angle)));
			annotation(__esi_Impure=false);
		end angleMinMax;
		function normalVector3D
			input Real alpha_inc;
			input Real alpha_or;
			output Real vector[3];
			algorithm
				vector:={sin(alpha_inc)*cos(alpha_or),sin(alpha_inc)*sin(alpha_or),cos(alpha_inc)};
			annotation(__esi_Impure=false);
		end normalVector3D;
		function normalVector2D_inc
			input Real alpha;
			output Real vector2D[2];
			algorithm
				vector2D:={-sin(alpha),cos(alpha)};
			annotation(__esi_Impure=false);
		end normalVector2D_inc;
		function normalVector2D_or
			input Real alpha;
			input Real inclination;
			output Real vector2D[2];
			algorithm
				if abs(inclination)<1e-10 then
					vector2D:={0,1};
				else	
					vector2D:={-sin(alpha),cos(alpha)};
				end if;
			annotation(__esi_Impure=false);
		end normalVector2D_or;
		function diffAngle
			input Real vector1[3];
			input Real vector2[3];
			output Real angle;
			protected
				Real value;
				Real minMax;
			algorithm
				value:=(vector1[1]*vector2[1]+vector1[2]*vector2[2]+vector1[3]*vector2[3])/(sqrt(vector1[1]*vector1[1]+vector1[2]*vector1[2]+vector1[3]*vector1[3])*sqrt(vector2[1]*vector2[1]+vector2[2]*vector2[2]+vector2[3]*vector2[3]));
				minMax:=max(min(value,1),-1);
				angle:=acos(minMax);
		end diffAngle;
		function diffAngle2D
			input Real vector1[2];
			input Real vector2[2];
			output Real angle;
			protected
				Real value;
				Real minMax;
			algorithm
				value:=(vector1[1]*vector2[1]+vector1[2]*vector2[2])/(sqrt(vector1[1]*vector1[1]+vector1[2]*vector1[2])*sqrt(vector2[1]*vector2[1]+vector2[2]*vector2[2]));
				minMax:=max(min(value,1),-1);
				angle:=acos(minMax);
			annotation(__esi_Impure=false);
		end diffAngle2D;
		function vector_inc
			input Real vector[3];
			output Real inc_vect;
			algorithm
				inc_vect:=acos(min(max(vector[3],-1),1));
			annotation(__esi_Impure=false);
		end vector_inc;
		function vector_or
			input Real vector[3];
			input Real inc_vect;
			output Real or_vect;
			algorithm
				if (abs(inc_vect)<1e-10) then
					or_vect:=0;
				else
					if (vector[2]<0) then	
						or_vect:=2*pi-acos(vector[1]/max(sin(inc_vect),1e-10));
					else
						or_vect:=acos(vector[1]/max(sin(inc_vect),1e-10));
					end if;
				end if;
			annotation(__esi_Impure=false);
		end vector_or;
	public
		GreenCity.Interfaces.Thermal.DefineVolumeFlow ToFlow "Specifies a volume flow port" annotation(Placement(transformation(extent={{540,-230},{560,-210}})));
		GreenCity.Interfaces.Thermal.ExtractVolumeFlow FromReturn "Extracts the characteristics of a volume flow connector" annotation(Placement(transformation(extent={{555,-315},{535,-295}})));
		output Modelica.Blocks.Interfaces.RealOutput TCollector(
			quantity="Thermics.Temp",
			displayUnit="°C") "Temperature of solar thermal collector" annotation(
			Placement(
				transformation(
					origin={280,-355},
					extent={{-10,-10},{10,10}},
					rotation=-90),
				iconTransformation(
					origin={0,-200},
					extent={{-10,-10},{10,10}},
					rotation=-90)),
			Dialog(
				group="Temperature",
				tab="Results 1",
				visible=false));
	protected
		Real TFlow(
			quantity="Thermics.Temp",
			displayUnit="°C") "Flow temperature" annotation(Dialog(
			group="Temperature",
			tab="Results 1"));
		Real TReturn(
			quantity="Thermics.Temp",
			displayUnit="°C") "Return temperature" annotation(Dialog(
			group="Temperature",
			tab="Results 1"));
		Real qv(
			quantity="Thermics.VolumeFlow",
			displayUnit="m³/h") "Volume flow" annotation(Dialog(
			group="Volume Flow",
			tab="Results 1"));
	public
		Real SolarInclination(
			quantity="Geometry.Angle",
			displayUnit="°") "Inclination angle of solar radiation" annotation(Dialog(
			group="Alignment",
			tab="Results 1",
			visible=false));
	protected
		Real STVector[3] "Normal vector of solar thermal collector" annotation(Dialog(
			group="Alignment",
			tab="Results 1"));
	public
		Real SolarOrientation(
			quantity="Geometry.Angle",
			displayUnit="°") "Orientation angle of solar radiation" annotation(Dialog(
			group="Alignment",
			tab="Results 1",
			visible=false));
	protected
		Real STInclinationVector[2] "Inclination vector of solar thermal collector" annotation(Dialog(
			group="Alignment",
			tab="Results 1"));
		Real STOrientationVector[2] "Orientation vector of solar thermal collector" annotation(Dialog(
			group="Alignment",
			tab="Results 1"));
		Real SolarInclinationVector[2] "Inclination vector of solar radiation" annotation(Dialog(
			group="Alignment",
			tab="Results 1"));
		Real SolarOrientationVector[2] "Orientation vector of solar radiation" annotation(Dialog(
			group="Alignment",
			tab="Results 1"));
		Real alphaLongitudinal(
			quantity="Geometry.Angle",
			displayUnit="°") "Longitudinal differential angle" annotation(Dialog(
			group="Alignment",
			tab="Results 1"));
		Real alphaTransversal(
			quantity="Geometry.Angle",
			displayUnit="°") "Transversal differential angle" annotation(Dialog(
			group="Alignment",
			tab="Results 1"));
	public
		Real QSolarThermal(
			quantity="Basics.Power",
			displayUnit="kW") "Actual heat power supply" annotation(Dialog(
			group="Heating Power",
			tab="Results 1",
			visible=false));
		Real etaTotal(
			quantity="Basics.RelMagnitude",
			displayUnit="%") "Overall efficiency solar thermal collector" annotation(Dialog(
			group="Efficiency",
			tab="Results 2",
			visible=false));
	protected
		Real IAMTrans "Actual transversal incidence angle modifier (IAM)" annotation(Dialog(
			group="Efficiency",
			tab="Results 2"));
		Real IAMLong "Actual longitudinal incidence angle modifier (IAM)" annotation(Dialog(
			group="Efficiency",
			tab="Results 2"));
	public
		Real EST(
			quantity="Basics.Energy",
			displayUnit="kWh") "Solar thermal heat output" annotation(Dialog(
			group="Energy",
			tab="Results 2",
			visible=false));
		parameter Boolean UseST=true "Use Solar Thermal Panel" annotation(Dialog(tab="Solar Collector - Geometry"));
		parameter Boolean CPC=true "If true, solar thermal collector is CPC collector, else, solar thermal collector is a flat plate collector" annotation(Dialog(
			group="Collector Configuration",
			tab="Solar Collector - Geometry"));
		parameter Real alphaModule(
			quantity="Geometry.Angle",
			displayUnit="°")=0.6108652381980153 "Inclination angle of solar thermal collector" annotation(Dialog(
			group="Alignment",
			tab="Solar Collector - Geometry"));
		parameter Real betaModule(
			quantity="Geometry.Angle",
			displayUnit="°")=3.1415926535897931 "Orientation angle of solar thermal collector" annotation(Dialog(
			group="Alignment",
			tab="Solar Collector - Geometry"));
		parameter Integer nSeries=1 "Number of solar thermal collectors in series" annotation(Dialog(
			group="Dimensions",
			tab="Solar Collector - Geometry"));
		parameter Integer nParallel=1 "Number of solar thermal collectors in parallel" annotation(Dialog(
			group="Dimensions",
			tab="Solar Collector - Geometry"));
		parameter Real AModule(
			quantity="Geometry.Area",
			displayUnit="m²")=1.312 "Effective surface area of solar thermal collector" annotation(Dialog(
			group="Dimensions",
			tab="Solar Collector - Geometry"));
		parameter Real VAbsorber(
			quantity="Geometry.Volume",
			displayUnit="l")=0.0015 "Absorber volume" annotation(Dialog(
			group="Dimensions",
			tab="Solar Collector - Geometry"));
		parameter Real etaOptical(
			quantity="Basics.RelMagnitude",
			displayUnit="%")=0.644 "Optical efficiency of solar thermal collector" annotation(Dialog(
			group="Thermal Efficiency",
			tab="Solar Collector - Thermal Parameters"));
		parameter Real a1(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=0.749 "Linear heat transmission coefficient" annotation(Dialog(
			group="Thermal Efficiency",
			tab="Solar Collector - Thermal Parameters"));
		parameter Real a2(
			quantity="Basics.Unitless",
			displayUnit="-")=0.005 "Quadratic heat heat transmission coefficient [W/m2*K]" annotation(Dialog(
			group="Thermal Efficiency",
			tab="Solar Collector - Thermal Parameters"));
	protected
		Curve IAM_CPC_Trans(
			x(
				quantity="Geometry.Angle",
				displayUnit="°")={-1.570796327,-1.221730476,-1.047197551,-0.8726646260000001,-0.698131701,-0.34906585,0,0.34906585,0.698131701,0.8726646260000001,
						1.047197551,1.221730476,1.570796327} "Angle",
			y[1](
				mono=0,
				interpol=4,
				extra=true,
				mirror=false,
				cycle=false,
				approxTol=0.1)={0,1.14,1.05,0.98,1.02,1.01,1,1.01,1.02,0.98,
						1.05,1.14,0} "IAM factor") "Transversal Incidence Angle Modifier (IAM) for CPC-collectors" annotation(Placement(transformation(extent={{-10,10},{10,30}})));
		Curve IAM_CPC_Long(
			x(
				quantity="Geometry.Angle",
				displayUnit="°")={-1.570796327,-1.221730476,-1.047197551,-0.8726646260000001,-0.698131701,-0.34906585,0,0.34906585,0.698131701,0.8726646260000001,
						1.047197551,1.221730476,1.570796327} "Angle",
			y[1](
				mono=0,
				interpol=4,
				extra=true,
				mirror=false,
				cycle=false,
				approxTol=0.1,
				quantity="Basics.Unitless",
				displayUnit="-")={0,0.76,0.89,0.95,0.98,1,1,1,0.98,0.95,
						0.89,0.76,0} "IAM factor") "Longitudinal Incidence Angle Modifier (IAM) for CPC-collectors" annotation(Placement(transformation(extent={{-10,10},{10,30}})));
		Curve IAM_Flat_Plate(
			x(
				quantity="Geometry.Angle",
				displayUnit="°")={-1.570796327,-1.221730476,-1.047197551,-0.8726646260000001,-0.698131701,-0.34906585,0,0.34906585,0.698131701,0.8726646260000001,
						1.047197551,1.221730476,1.570796327} "Angle",
			y[1](
				mono=0,
				interpol=4,
				extra=true,
				mirror=false,
				cycle=false,
				approxTol=0.1,
				quantity="Basics.Unitless",
				displayUnit="-")={0,0.64,0.8,0.9,0.9399999999999999,0.97,1,0.97,0.9399999999999999,0.9,
						0.8,0.64,0} "IAM factor") "Incidence Angle Modifier (IAM) for flat plate collectors" annotation(Placement(transformation(extent={{-10,10},{10,30}})));
	public
		parameter Real alpha=0.95 "Absorption coefficient of solar thermal collector" annotation(Dialog(
			group="Collector Propertiers",
			tab="Solar Collector - Thermal Parameters"));
		parameter Real tau=0.95 "Transmission coefficient of solar thermal collector" annotation(Dialog(
			group="Collector Propertiers",
			tab="Solar Collector - Thermal Parameters"));
		parameter Real eps=0.05 "Emission coefficient of solar thermal collector" annotation(Dialog(
			group="Collector Propertiers",
			tab="Solar Collector - Thermal Parameters"));
		constant Real sigma=0.0000000567 "Stefan-Boltzmann-constant" annotation(Dialog(
			group="Collector Propertiers",
			tab="Solar Collector - Thermal Parameters"));
		parameter Real CCollector(
			quantity="Basics.Unitless",
			displayUnit="-")=9180 "Surface area specific heat capacity of solar thermal collector [J/(m2*K)]" annotation(Dialog(
			group="Collector Propertiers",
			tab="Solar Collector - Thermal Parameters"));
		parameter Real gThermal(
			quantity="Thermics.HeatTransmCoeff",
			displayUnit="W/(m²·K)")=23.6 "Thermal conductance between absorber and heat carrier medium" annotation(Dialog(
			group="Collector Propertiers",
			tab="Solar Collector - Thermal Parameters"));
		parameter Real TCollectorInit(
			quantity="Thermics.Temp",
			displayUnit="°C")=293.14999999999998 "Initial temperature of solar thermal collector" annotation(Dialog(
			group="Initialization",
			tab="Solar Collector - Thermal Parameters"));
		parameter Real epsDirt=0 "Dirt correction factor (0: fully irradiated, 1: fully shaded)" annotation(Dialog(
			group="Outer Characteristics",
			tab="Efficiency Correction"));
		parameter Real epsShading=0 "Shading correction factor (0: fully irradiated, 1: fully shaded)" annotation(Dialog(
			group="Outer Characteristics",
			tab="Efficiency Correction"));
		parameter Real epsCover=0 "Coverage correction factor (0: fully irradiated, 1: fully shaded)" annotation(Dialog(
			group="Outer Characteristics",
			tab="Efficiency Correction"));
		parameter Real cpMed(
			quantity="Thermics.SpecHeatCapacity",
			displayUnit="kJ/(kg·K)")=3680 "Specific heat capacity of heat carrier medium (Glykol(38%)-water)" annotation(Dialog(
			group="Heat Carrier Medium",
			tab="Heat Transfer"));
		parameter Real rhoMed(
			quantity="Basics.Density",
			displayUnit="kg/m³")=1113.1 "Density of heat carrier medium (Glykol(38%)-water)" annotation(Dialog(
			group="Heat Carrier Medium",
			tab="Heat Transfer"));
	initial equation
		assert(alphaModule>=0 and alphaModule<=pi,"alphaModule must be within 0° and 180°");
		assert(betaModule>=0 and betaModule<=2*pi,"betaModule must be within 0° and 360°");		
		
		TCollector=TCollectorInit;
		
		if qv>0 then
			TFlow=((TCollector+FromReturn.TMedium)/2);
		else	
			TFlow=TCollectorInit;
		end if;
		
		EST=0;
	equation
		if UseST then
			if CPC then
				IAMTrans=IAM_CPC_Trans(angleMinMax(alphaTransversal,pi/2,0));
				IAMLong=IAM_CPC_Long(angleMinMax(alphaLongitudinal,pi/2,0));
			else
				IAMTrans=IAM_Flat_Plate(angleMinMax(alphaTransversal,pi/2,0));
				IAMLong=IAM_Flat_Plate(angleMinMax(alphaLongitudinal,pi/2,0));
			end if;		
			
			STVector=normalVector3D(alphaModule,betaModule);
			
			SolarInclination=vector_inc(EnvironmentConditions.RadiationVector);
			SolarOrientation=vector_or(EnvironmentConditions.RadiationVector,SolarInclination);
			SolarInclinationVector=normalVector2D_inc(SolarInclination);
			SolarOrientationVector=normalVector2D_or(SolarOrientation,SolarInclination);
			
			STInclinationVector=normalVector2D_inc(alphaModule);
			STOrientationVector=normalVector2D_or(betaModule,alphaModule);
			
			alphaLongitudinal=diffAngle2D(SolarInclinationVector,STInclinationVector);
			alphaTransversal=diffAngle2D(SolarOrientationVector,STOrientationVector);
			
			etaTotal=IAMTrans*IAMLong*etaOptical*alpha*tau;
			CCollector*der(TCollector)=(((EnvironmentConditions.RadiationDirect+0.25*EnvironmentConditions.RadiationDiffuse)*etaTotal+0.75*EnvironmentConditions.RadiationDiffuse*0.5*(cos(alphaModule)+1)*etaOptical*alpha*tau)*(1-epsDirt)*(1-epsShading)*(1-epsCover))-eps*sigma*(TCollector^4-EnvironmentConditions.TAmbient^4)-a1*(TCollector-EnvironmentConditions.TAmbient)-a2*(TCollector-EnvironmentConditions.TAmbient)^2-cpMed*rhoMed*VAbsorber*nSeries*der(TFlow)/(AModule*nSeries)-cpMed*rhoMed*qv*(TFlow-TReturn)/(AModule*nSeries);
			
			TReturn=FromReturn.TMedium;
			qv=FromReturn.qvMedium/nParallel;
			cpMed*rhoMed*VAbsorber*nSeries*der(TFlow)=gThermal*AModule*nSeries*(TCollector-TFlow)-cpMed*rhoMed*qv*(TFlow-TReturn);
			ToFlow.TMedium=TFlow;
			ToFlow.qvMedium=qv*nParallel;
			
			QSolarThermal=cpMed*rhoMed*qv*nParallel*(TFlow-TReturn);
			der(EST)=QSolarThermal;
		
		else
			IAMTrans=0;
			IAMLong=0;
			STVector=0;
			SolarInclination=0;
			SolarOrientation=0;
			SolarInclinationVector=0;
			SolarOrientationVector=0;
			STInclinationVector=0;
			STOrientationVector=0;
			alphaLongitudinal=0;
			alphaTransversal=0;
			etaTotal=0;
			der(TCollector)=0;
			TReturn=FromReturn.TMedium;
			qv=0;
			der(TFlow)=0;
			ToFlow.TMedium=TFlow;
			ToFlow.qvMedium=FromReturn.qvMedium;
			QSolarThermal=0;
			der(EST)=0;
		end if;
	equation
		connect(FromReturn.Pipe,ReturnST) annotation(Line(
			points={{555,-305},{560,-305},{595,-305},{600,-305}},
			color={190,30,45}));
		connect(ToFlow.Pipe,FlowST) annotation(Line(
			points={{560,-220},{565,-220},{595,-220},{600,-220}},
			color={190,30,45}));
	annotation(
		__esi_viewinfo[0](
			minOrder=0.5,
			maxOrder=12,
			mode=0,
			minStep=0.01,
			maxStep=0.1,
			relTol=1e-05,
			oversampling=4,
			anaAlgorithm=0,
			bPerMinStates=true,
			bPerScaleRows=true,
			bPerScaleCols=true,
			typename="AnaStatInfo"),
		Icon(
			coordinateSystem(extent={{-200,-200},{200,200}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAOvAAADrwBlbxySQAAAE9JREFUeF7twQENAAAAwqD3T20ONyAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4FQN4XgAAdMZTtsAAAAASUVORK5C
YII=",
								extent={{-200,-200},{200,200}}),
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAADUQAAA1EBvbkFJwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAADBvSURB
VHhe7X15dFzllafB2iXLuw3Y2NhA2CEsCfsSAiEJayCQsIVAIJAEmBAgGMLuVZa17/u+l0pLqapU
e5VKJVWVSrss2fR0z0lPJ5053T3Tczoz3X3mj9/c+33vyaXyM1oseQH5nN+B9/RevVff7937u9/9
7n21Ivyfa+NVSY5NO95xn3dpwL31iv/j3LgTyzjzQVz91bX5Er9rw463mUOFzpn/HBt33EUH/2Pk
ycs46/Bn+8Yddyq0yn/ODRfd7dy08z81Dl7G2Qjikg1WkMsmTTuXLfcrh4v/ZNm8OXEFa67WAd7L
dyJ4z455IUCo3rINLZfdANfdDy7jVOHybx3HH4M1eQUHVJF/OPr7i/Dvf7kQ//HPW+eNsrx12LZz
C3x/+tdlnCr88V/gefuTGRwyXOdd2r+CI7DwnWy5CyWX8ecvLkBU1Lmoc/m1b2YZSwMi2XXZTTMI
Zm5XhO9gsKvVIm4+uPaaZHyYWaB9I8tYMrC7nsHnpotxPMHfOXmCv/+9NXht18doGziM36dkzor3
DmbBMvVHzZtextzhuuehGVwyloTgnz+7Ck/+4jXktBjwrTvvmRXfvvte1HuCmje9jLnjlBH8o0dX
48Xf/l7zJpaxdDhlBN/87dV490C65k0sY+lwSgj+61+2Ys3aWGQ2tmvexDKWDqeE4O62jUhMioP7
v/0PzZtYxtLhlBD88+eTcd8jD2vewDKWFktO8HjgPERFn4tig03zBhYDz/7qvyAuPgF3ff8h2L74
k+YxX1csKcH//k9b8cB9q5bUektNLpxzzjlYsWKFQHR0DF56exd6/+F/aR7/dcOSEvyH36/Gxs3r
0D44pXnxkwWTuGb9BkHspVGxuDY6bpro1evW42Blo+Z5XycsGcGH9q5BXFz0krrmB3/yrCAzhiy4
fO1Wca97V5+H81dGTxO9/ZJLUfs1zoEvOsH/8vdb8PNnk5G8OhEZ9XoUtHWjjNyo1sVPBuXdnmnX
/GbShhn3a924A79JWo+Ec84Vf+fjbrvvAdiO/oPmZ32VMSeCXTdv1yQzHP/7z1uQl74WmzfH45Ir
LkGjNyQuwMmNhKQkFLR3H3fxhYJd89oNGwV5N8bEwxFxvypa1m/HQ3HJOFex5qjoaPz8t+9+rfR5
TgQXrtuKq69ahV2/S0Zt6Xo4ujai17YJ+voNyE1biyceTcaqVTHYdN4GvH8oB96//5cZF/nNR7sR
l5CAXF3XjP0Lxfd//FNBWCJZaOP6bTPuVQtFa7fM0OfkNWuxr7RW87O/apgTwbbbHyBLzMDt370H
Oy/dhlWrE8gaVuK8CzbgupuuwVO/eFUsIvT88Z81L8JYLJKLOq3TrvkPyZtm3OdsYH0+b2XUNNE7
vnH5tKf5qmJuLpoOCj+Jl/x0/rEZ++aCkyWZH6DktesEOXfEJs64x7lCS595/uz4r/+oec2zHQsi
+KmXX8Njz704Y99ccTIk3/+jHwtS1p+7Eu2kr5H3yXh91QY8k7gWXRsu0vy7ikh9jo6JwSvv/uEr
p88LIvjHL72KR559Yca++WAhJOfrTThnxTmEFdhPrjbyHhlZay5A9LnnYivNgVdHx+DtVRth1zgu
HKzP10TMn1OrmzTv4WzEaSGYwdUdHF3PRQNdf/cXERgxAQ/HJ8+4NxVGstgtMXF4+o67seu99/DQ
gw8iKT4eF9K+Ayd4IMIRqc+XXHk1mvqGNO/nbMJpI5jBkaxp4u80/xYOvh4POicwTuR6v5eQjG9s
2ozPdr2PyooK9PX1Yf/evbjj9tsRRS795vgk1K67UPNcFeYNO/BK4jrEh+nz/Y/9GM6//YvmfZ0N
OK0EzwVZTR1ioNk1swuOvDfGZ8mbERcVhc9e+zX27t6DyclJfPHFFwJ2ux1vv/U7XHPllVhJ7vvx
hDUwzFGf+ZpMdExMLH753kdnpT6f0QRbjvw9Nm/ZKgb5OSIm8r4YzTQPTo6Kxq9++DC55l3w+XzT
5KpgwltaWvDcs8/ivA0bkETHcyQ9mz4XRugzJ1fSalo07/VMxRlN8Pd+9KQY2EuiYsT0JvK+OIP1
bXK9395xMT7+4ANBIhPq8YyjstyPsuI+mI1DOHr0qNg/PDyMspISPKjo89YF6vM3rr4OrcFxzXs+
03DGEsz6zIMZTe65VFlIiMSvyArXxMVjz29/h7TUVBw5coS0dwK5OT5BcENdAPlZPXDah2dY9GLo
87n0X562cQCodf9nCs5IgjuHj2C1ktB4jUiMvB8Grx7FEjnvPvU0/kDWOzQ0hPHxIygq6icr7cfh
w0eE5Rbn9aCtJTCDYBWLos+xsWJGcKbq8xlHMA/Ubd99QAwe65+WTlrImnbGxuPhG7+FP+zaBbPZ
LMisqxtAfq4PweBhSaB1GAVZHrgUC56aPAKXdRBWQxAhcrG8b6H6XBChz+s2bjojiwoXRDD3GFXZ
fTP2LRZ49YkHjF3hidzmjxPXYMvqNfj83d+jIC9PEGUyjSA32werZURshwYOoyi7B401fYL8I1NH
0FjpRUmGnWBFWaYVfT3HXPdC9JljgE8ogg/X58uvuwEdS1TgsBAsiOClQkvfCBISk8RA/X7Vxhn3
oCJ19fkiW/XRi7/Ap598Qm55HH7/YdLdPjQ2DBCZbJVHUFHiQ1lBD8bHpgSBXW0BFGfa4bYP4vDE
FOoKHWit8kwTrGJR9Jnu73uPP3VG6PMZQ7D3v/9PfPOW28QA3RaTMOP6KjpJHzdFx+KF73wXu8g1
u91uobvFxX6UECYmjgiSWpuCwjX3+8bEtq9nVJDb2dIntsfHJlGVa0N7Xc80sey+R4cmpiPuhegz
T9nC9Tk2Lg6vf7T7tOrzGUPwq+99JAZlDVlNKwUykffA+E58Mq6+YCs+ff8D1NbUCmttaAgJ6w34
JwQxTvuIINdkCInt0ZFJlOc6UVfiwRS5aSawtdqDisxuDAblA9DvGURtlhHVaZ1oyjNhODgq9p+M
Pl8dps8bz79AVLZofe+lxhlBcKXVi+hoWUf1KWla5PUZ75HLToyOweevv4GUffuIrCl0W0aRk01z
XbPU0sEQ6W6uF/VVfTRlIt0lNJDulmU7MRySD4CtK4Dy9G64zEGxPRwaR3WmEboSK3osATTmGKEv
6RZ/U3Ey+rw5TJ+vuuFbp1yfTzvB3O1wyRVXiQH4QdyqGddVUbduGxJpoN549Ed4n1yz3+9HIDiJ
3Nx+NNSz7h4lF3sUlaV9KMn3YmxU6q6pI0iu2QGPfUhsB/tHUZ5hEa6ZrZ/dclORFXXZRoyPyMi7
q9aBpmzDNLnhWCx95mLBU6XPp53gp199XXxxjkS1FhLYEq6LS8Qd37gcH73/Ptra2sQct6SEgqYi
v9BgHny9bgAF2R74vNLt9vWy7jrQ3iR1d2J8CjUFdtQSOMjifcamHlSlGeAnF83bfncItYfaYNPJ
4OsIPQBOvRuGMiMcOicmJ2SOe6H6fH9c0gx9fuPjvZpjspg4rQSzLp1LlsBfOn3N+TOuqeIlevo3
JiZi9+/eRmZGhrDWxqZB5JDu9vdJt+t2joqMVVe71N2xUdLdPDdqij0iouZz9LVelGdaMOCX+up1
hFCZ1gVLq1dsswU3ZHWitdAkiOVzTNUWNKa2oLPIgObUJhhLj1k2S4Rer1+QPl8Vps+bt1yIwnaL
5vgsBk4bwfYv/oQt23eIL/lUwuoZ11PBi/E8Jdr17PP46MMPMTIyAqttDNk5/TAape4ODU2ikHS3
tvKY7jZW+VBKujs4IB8Ah3kAZekW2I0yozUyyLprgq7UJtKbjLZSC+rTOzAakh6gx9iLhoMt8Bhk
pO1pd6M5pQGT48dWqhiLpc/XfOtmdA4f1Ryrk8FpI1gtWt++MgbdpFOR12Tt4sX6J2jqxLrb3d2N
gQHWXT9F0AOCyKmpo6gq70dxnldEyzzg3RQ9F5Fr5owVb4cC42S5VhE5C62mSLq5xI6aLBPGhpXA
q40ekEPt8Nn8YnvIP4zGQzoYK2SGjC26I78N7dk6sc3HTAzT/NvixVBPQOxbLH1+6KfPLWoX5mkh
OKWiQXyhqHPOES4r8nqMh8mqd6xbj93vvScsZJKCqJLSIAoL/RhTkhft+pBwzV6PdLv+vnEUZTmh
b+Ds1RektUdIdx2oybOTBssHwKTrFa65zyXdecA7JMg1NzjFNuusLrcDumzS+jEZeNnqrWgh6x1w
Sw8w5Auh7WAt9Pur0La/Es7qTrGfsRB9borQ53iSpHf2p2mO3Xxxygk2jf8t1ilF6y/T0xt5LQa7
uJiVK/HJK7/E7s8+o6DqMJpbhkh3++HzSavzeMaQl+1Fh35AbHPGqrzAg6oi0l2x0PAF2up7RUoy
2CcfAJ9rkMg1wqxTdHeUdJci5pYCo4io+RxTjRUNqTqEepXAy9YvXLNLZxfbh0cn0JHeAEN2E0aD
w+jRUVS+rxxjQRmpMxaqz/kR+nz+hdtRanRojuNcccoJ/s6Dj4qbv/IECwn69duxjua7L3/v+yJb
5fV6YXeMC901GKTujgxPoSivF9XkntXkRVNNH4pJd9VFBKclhNIMK2wGaXWjw4dRnWVGSwnpLp9D
Lr69nKZIpLtqYsNr7hPkuttlFD0aGoUurQldRe3yHLpOd2k7WW8NRv2SUF+HAx1McOAYwXzcUdJ1
oc+lpQvS503nLo4+n1KCP0jPEzccR5pTfQJ9upW068btF+GTDz5AQ0MDQiHS3bwAqqul7jKqK/0i
oTFMARYPqMXIuuuEvVu63UEiuTzLBl2V1F0OolpK7SKwGqUAi49xdPhQQ67Z2y11l0luSGulKZFJ
kM+Edha0o5UIHie95WO87eT+D1Sjz6g8AP5BdB6ogL20FV8opPbUdcK4rwjdB0sR7JJWv1B9fiFh
rWis4zE7lzzaY8+/OG99PmUEc6F8YtIqcbNvrZrZLKbit0kbsComFp+98SYOHjiACZqvlpbT/LbA
j1EledHZPihcs8clrS7gZ911QVcndZenRbVFTlTl2skFywfArPcJ19xLUyPeHvANoyatA8Y6hzhn
anIKrXnkqrPaMEEumI+xN9mFaw46lcCrNwR9Sg1slTRVIjKnDk/CmFUPY3oNJkeVB6DRiK69RfDW
tsNV1ADznnyM9UsJEZ+5CPrMlajvpWZpjrEWTgnBvJBw/a13iBu86QTNYjX0RMfTU/rW4z/GB++/
TxHzAFpah8k1+8lNKwNI/2Vy23Ry0CbGj6CisAeVhZ7p5EV7ow9l5Jr9vXLZsN8zhAoil5Ma4pyx
STTkGNGcZ6SASp5jrrcL1zzQowRezgCaiFxnk01sT1Kw1ZHZBENW0/Q0iQOrjv3lGPZKCRh0+ojc
YnjrOsT2eCBEBOch1C2DNxUno88sazyGjPO3bUeZefauzVNC8Osf7xE3tYqeWn4iIz/ftnEHrohL
wAPXfhMf7nqftNYAp4t114+ODqm7bMFF+WSJZX1iesSW11JHU6RsF4JkxXyM2zYodNfSIa1ujKZO
1dndaCqyKUEU6W6lHbVkvUMB6QF6rX7UE7mcseLt8aFx0t1mGAqP6a6l3CCsd7hPPgB+s0cEVj69
VWwfHh6F6WA5bDm1NKWaEi7eTRZs2VeAw0PyOky4v14Pf10rJvzBRdNnXoHrGv0bzXFnLDnBXBzA
aTm+mY9PsJDwbOJanL8qGbvfeRe5OTkYGppCbn4AlZXHdLemKoCC3F4MDUoLsnUPoTDTBatJDvpQ
aALl2eRWKzzT5+jKnaS7ZrGgwMc4DX2oPtQBj6lfbI8MjKIxXY/OUqM8hwg1FHWKwGpsUHkADG60
HqiBz+AS2xw5d6RUwlpMc2LSXNZ3W34DjCmlGB+QgZZfbxbWO9AlPcB4MAT7vlxCDhz7suHck0kk
Sy+0EH02RejzyqgoPPXyrzT1eUkJ9vzxn3D5ddeLm7gvNmnGZ6rIVtpNdj3/Aj7+8CMMDY+gvDKE
vIIARkakCzV0DgnX7HRItzsQnEBRthvNtT5hYWyddSVuVJLucpqSj7G095NrNqHHJleNQv0jpLud
MFTbhfVz8kJf0IWmzDaMjygZL52DXHMj/Hb5AAz3D5Ll1sFa3iGuw9ZpyqEpUloNDo/Ih6a32SRc
80C39AAjXj/Me/PRU94ktPrI5CRcmaVwpORhkix9cnQMzs/TMdDcLo5XsRB95tbZcH1OJCPh9t1w
DpaU4BfefEdceCO5lA6Nmw1vN+FsFX/J1rYRZJFr9njkAPp8rLu9aG2WTzwvNFQWeVFBc97xcSXw
au5DaVgJjt87jIp0GvhGqbuHSTcbc01oyiXdVTTU0ugUrjngkZ8bdAcFufYG6Xb5uM7sZnRmNtL/
y4SHs7YL7fsrMOhRAi83Td32lcBTI8maHBuHNa0MNsKUco6vqgW23VkYccq594jdDednaRjusoht
xuFAEBO9JD0031+IPmeSkfA7SnisGRfuuBhNPtl2s2QEcx8vh/b8dHGZTeRnMh5IWI3LN58/3W7i
9kwQuQHo2yRRnLEqKuxDeWmfsmjwBXSNNEXKcsPfp+SMncMoybDBpJdWx5FzTa4VDYXW6eRFZ7VT
WG+oX35unz1I5LbCrpNuly1Yl6FDRz7pLp3Dlmet6kJrSq3IWvExAYtXkOvVSWLYgk2pFbBk1wgr
ZQt3lzShe28Bxvqk1xikAMtK5PoblQdgaAQuctHezCIcZa2mc4KV9fB8egA9n+xDH0XHU6TNnHNn
fX7ggQdEV8eJEkLhYH3mZju1W3L9ps3CZS8Jwfa/+bN4ivhCP4rXXkhgPeZ2k89/9Wvs27OH5rsT
yC0IknseEEEUa2JdTZB01ycK6HiAHNZh0l03urvkoA8PHRa621jOust6SLpb6UIl6e6QkvBwm/yk
u51wdcmOB54HN6S3ob2YdJc0l88zlBigO9SM0QEpAT5jD3QHauHtkBHwGO3vOFgNS2GLOJ61117U
BOOBUowFlIxXh1VMi4Id8gGYID22ke56civF8QxvThlcezJwmDSZjxlqMwhyQ3V07W4bej8/gMGa
evE3xq9//WvExcYiKSbmhCndcLxB1s5jrpLM5bxLQjD3DvMFtq2MFhP2yM+b0W5CrtnnI72sGhSB
FQdY/OV4tSiXXLPdFlYlmeNBY7VPEMkZrPpSDypy7CJLxcfYDH7hmj0WxYL8o6hJN6CjinVXBlH6
InLVGaS7SvLCpXeh6WAT+izyARgJDKM1tQ7dZaruHoEpl6ZIh6opWlZqvFotwjUHTfIBGPUFSXcL
4SltVM6Zgiu7HPYDpLtktXxMoLFNumazTH6M03f2fH4Q/sJycc5Rmlf37UtFqLxK/L29vR0xROwv
f/lLrCaSuR47chzDUUmBWawSdD1D+s3/jaPo3Hrb94479qQIzmxoE25lJSFvzfFPHbuSyHaTto5R
4Zp5asRfjtd5uQS2ufFYlWRlSS/K8kl3lYUGQ6tfuOZet3S7Qd8oKjLM6Kzj7BWdc3gKjflmMefl
uS8fY21xo45cs98pHwCe9zK51jppdZy86MxpQXtGo8g58z5Xg4lccyVCTlk4wPNeJtdd1SbPIa21
pleQ7pZiijSY9/XV6ck1Z2PYrmS8XF44Pk+Dv7JRbB8Zn4A3NRu9KZk4QkEX7xuoqEbvJ3sw4ekR
laKXXXYZnnnmGSTExuGjWV5VwdPMyxUN3hYVI/bdEpMgtut2XnPc8QsmmN/Qvun8C8QHv0BTn8jP
YUS2m3h7SXdzg2jWSaK4QqOEuxOK+8VqEO/TtwRRSLrr8ypTF8+IqG82tkrd5WqN2jwb6gusInnB
BBvqXKhOM4isFR/jdw0QuXrYWqTuTowehi6zFe25bUSsfGjsdWboKGoOeWTyYsDRJ8jtaTKL7cmx
CZjSqmDOqMbUBHkNsjxPhY6st4CiZyXyJlKZXJ7zinNGRuE6QK46rQBH6AHic/yl1fB8loJxjwy8
Rk0WQe5QQ7PYfv755wXB1199Nb6fpD2O4eApE495DEXfbFgla7eKqRa3/Wi59gUT/N1HHhcX4ohO
q1ksst2kj6YheUUDKCkfEMuBTEx93QDycnwIBqTblVWSbpg6ZbSrVknWl7qnExF6rpIk6w0p1Rqe
7oAg19Ep3S6v+zZmtkNfKHWXzzFWmNCc2oxhvxJ4mXsFuT16h9geHxpFx6EamPObhMvlcxylOnTt
L8Wokn4MGOww7SmAv00W6R0md2zbnw83uWeZ8CDdza+keW+GSG7wMUOdJCGfpmBIpwRewQH4SHv9
WXk4SvdWQcFmLLnkF372M1wQnzDrqyi4KCJqhXTN7DlXr1qFq2JltvBpctWLRvAnOcXyKaKLqG+d
C0dku4nJZEZlzSBy8oNiQYG/rNk8szuBkxpFOT2or5S6y2go70FZtgMjFGDxMVylUZ5uhlOtkgyO
oSajC20VNhpgqbttJeSqKbBSqzXcHaTl5Jp9ZiXwGhgl3W2AqahNkMLnmAsoqk6txnhIPjR97Tbh
mgNd8gEY84dg3lckcs7qddy5VSKwOhxSivxaOoVrHjIqgVd/gHQ3Ff35peI6Ryn69mfmwrcnBVND
wwgEAti0aRNeeeUVRNMMJOME/dAquFDiInLJKrk/+MEP8Oabb4rtdyii5mkoW3PkefMmmN9Fqb5i
gSO5yPMZ3G6ybe067P69bDfpMIwik1wzLwXylxfdCbncnRAUliyqNcp8KMnrma7WMLYFUJJph9cp
B3DAP4byzG601UjdlVWSFlklSS6Yj7G1eoRr7rMrOWOaHzaltsBcfaxaw5CnR1t6IyaU5IW7iT6T
XHPQ3iu2RyiIMuwvg7NcrhodmZiENaMSltTS6YWG/oZ24ZqHrErGy+MjcjPQXy6j4iPk0r2HcuHd
n4Epctu8L1RdJ1zzmFUGXkzQzTffjJ0XXojnV80+NXosPlmMeTR5xYsu3IY8GteioiLccP31WLUy
Cm0UmLVvOD44mxfBXLV/y733iwvdcIKFhBntJh9/QvPdEUFuU4t0j7JKkoIm0l21O6FNJ7sTuCuB
t/t7RwW5HUqV5CQdV1dgR12+DYeVhIex0SNcs79HTl2C3kHUHdLDXC+tjhMerdlt0Oe0HauQrLcI
16xWaww4+wW5bgqueJuTHKYMctWsu0ryoqe6DSbS3WG39ADDDi8su3PQV0sPAG1zsOU6mAfPoXwR
UPG+QAXNd0l3x5wy8BqzOYjcvRisk4HX3r17kZycjEceegiXJa7SlLhwHKIx5RwDW24sRduff/65
IJdx4MABxNG+B+kB0Dp3XgS/tTtFkHuit85FtptYbR7kk+4Wl4WmdbepkbsTfPD3y8HgKkkmt6td
ah2nHyvyXKgtdk8nL9rrvCjP6EawT7pzry0oSmBt7dLq2IIbszqhK+hSziHdrTST9ZLu9is5Y6tP
kOtRqjUmaBrUcahWTItUrXaU6YX1jvYpGS+aGrHu9uuUB2BkDLaUQjgzy8gbyIRHb1E1nLszMd6n
rDV3dcNNuhtqVgKvwSFyyzRFysgRbtrhcCApKQm/+c1vEEfTx6pZctGcwgxfcHjxxRenyVXx+OOP
C/LZTUeeP2eCG3oGxFyLL/LBCUL5yHaTmvohZOcFEQxKC7Ko3Qkmac0jw6S7eV7UlJPuKgmPxqpe
lJLuDilVkk7zgOhOcBjlAI6ExlGTaURrmVVJRHC1BldJtosFBT7GY/CS7jbDa1QiV9LW1kMNMJLu
qsmL7iK9SGiMBZXAq9OBzn2l6O9Q5q7BIZgPFMNZUCc1lOAuqCHdzcOEkrwY0Bth/zwdgwYZeXNS
w7MnDX0Uo4hzpqYooMoXgdXkQEgsH15PLvXhhx/GWgqQ3op4saoWvhsrG/Q4Z33jDTccRy6joKAA
Wy+4AOUaD8ucCOa3zl15/U3iQid66xx3CIa3mxi6pO5abVK3mGTuTuDImZ981t3q8j6U5JLuEtF8
jLmTuxNkVyBvyypJC0XObnmO0N2Z3QmOdi/pbptYCuTtwf5hUd9sIgvmc9g6DQXkqtNId5WEh0dn
JddchYBVyRn3hchyy8mCZSUlW6c1uxrdrLtktXxMf5NBuOZBs5LxoqmSgyzXV1ortZqmRd70AvTs
I90dlp4mVNcEL7nmMYtcaXrjjTdwARFx9x134Oak1ZoSF47PkzdPk7th3TpkZmZqEsx45513Fh5F
/+LtXeJC3CzGdVSR50S2m1gpyOH5bkPTMA2YqrszuxM69CHhmr1uVXfHZnQncDFdbSFXSdqmkxeR
3QlB7xDpbtu07vKiQWtOu9BedaHB0chVknXT1Ro87+VSHF5M4O0p0mdTZi3M6dVi7sv7emraYdxb
eEx36b+WPRQBV8n3ggjdTS2A+xDNdxXd9Vc2wP3ZwWO6a3cSufsQqqoT27ywwNmqV15+GWtj46Cb
JVvF2axkGm8edy6xfe+99zSJDUf11m8c9zlfSnDvn/8ZpTYToqKkBuw7wSL1zXFJuGjDRnxAUTO3
m7DVFpWGpoOoFlEleaw7occzJkpgO8OrJPPdqOEqSVV3G1h3LQj6pDVwCU54dwInLxqzDdDls+7K
hAd3J3DUPOiTD0DA4Sdy6+FslhbEiwYdafUwsu7SOWx5zsp2obvDvUrGy+IR5Pqa5QMgdPdg0Uzd
La6FYw/prk/RXTN5HSJ3oEEntnka1LuHpkjpUnd5QWH79u34Gc1346KjxYtetMZRBVu2mp1iPPro
o5qERsJwzc3HfdZxBDvvvQ/2fzPB8n/r0fmvhdh+pcxWPRynHaUx+IbjKWBYR26E9UB8SXLBYoAo
uOIS2PZ2GezIKkkvuWefyDHzgDVV0xRJdCcoUxdrSHQncL6Zt7mXV3YFyu4EPqejwip0d7pK0uST
3Qmd0oLGh8ZEEV1XIekuX4e02lzchnauklR01290k+6Woa9NWawfGIbpQAkc+eG6Wwcr6S5XafAx
A+1m0t0MhNqMYvswaaubdNeXreguoT+7EL2fp4jEBh/z5JNP4pprrsEVl1yKR+aQrWJt5jFfSfPj
Sy65RIypFqGRMH3ztuM+6ziCHffdju7/KBN47PX7xIUuWBmtGaGF446kNWLCzpmZW265BZ2dskCc
ic7P6yPtDaKfIufqiv4ZVZK8WsTdCU6lO2GISC7PskJXSbqrJBVaRHeCkbRaCbwMPtE45u1WUoaB
EaU7wSTIF7pb2EEEN2Jcqdbo0duFa+YSHN7mKkkDV0mWcLUG6+6UKMMxHyyZLr3x64yku7kIGdVF
gwDse7JE5MzX4WVAb2YhPKS7k4PyoQmRFbNrHjXJjFd+fr4Yk6d/8lNsS0jUXJAJh6hXo1kKR8Xx
8QnYR/GMFplamBfB5n8vRXySTGpfSATzXCzy2EjwHHh7fCISExLE0/fYY4+JMhW7fRR52T5RqVGY
453uCozsTmD3XFfkRHWu7ViVZER3Aueba9NII2tltQYvNLRyd0LWse4EZ4tduOaAUq0x6A1Cn1IN
R7VsKOOFBlN2HYxppLtqlWS9QbjmQZrn8vZoT7/Q3d4KmTM+QvNi16Ei0t786YWGQG0zXJ+lYtQm
A69x0l/vp/swUFEjtvv7+7FhwwaxFBhDMYrWgkw4eLE/vBies1xaRJ4I87bgA93vYNsV0kUzbqcI
+kQ1zip4teN3qzYgmQKKZJoKxNPUiqPHYGAUvUSs2orCqCv3iu4EdaGho4mrJC3we6XuTncntMhB
5+QFV2o053Udq5KstSndCfIBCLoCogSWgytxDncnZDSgK7tREMv7uEqyk6ske5TAy+4lcovQ2yAf
ACbdlkquOr1UZKVYq31lDbBT1DzmlUHgiNUpyA3WyQeAI+fefTRFOpQtlgPZwu+55x7cdddd2LJp
M35xgqxfOHixn8eZg6o777xTk8Qvw7wJZhj/WoI3836G5HWJ4uLcY/R4/OpZa4i4bOfppHWIopvl
if2FF16IrKwsMRgqKiiwaq2XkaqskrTA0i4HfYy7E7LNaC6m+a6i1Z1VdtSR9Q75lcCrW3YnuJTu
BF73bU1vhiFf0V06x1LWMaM7gV00dyfwOi9vTwyOwJRSBntu7bRWe4obYNmbj/F+GXiFDFbS3Uya
9yqBV2gI7r0Z8GUVibmu0N3cYnhJdw/7ZZbso48+EjHJDx94ANckJs9ailO8dqsYWyaXc9TZ2dma
JH4Z5kSwkwh2/D+aE0agIPCxIFh9/QKH8HOpIWKLvzVxtaY+G9tlvrm6wCnqm7k7QS40kO6Wye4E
bv/kY13GPtGd4DEpa7UUXDWK7gSj1FA6p6u4E7pDjSKxwcdwlUZ4dwInNQwplbAVtQhSmFB7Xj3M
KSWYCMmHhleLusk1DxCpvD3uHyDdzYa3kHRXxART6M0ugWdvOhEt44bBljZRhjNikBkvfpdXXFwc
Xn31VSTQeLGuao2NCrE4oywkREWtxIcffqhJ4GyYE8Hu++9AP6qOQ/MXB8UN1NfX4777ZPDF2E76
PFuNL0NLn/llotxX1N7gg9UQEBrMA2Tv8svuBLuMQkNksVzfrFZJcsJDn29AS6Z+ujvB1eoUrrnf
Kj0C11fpU2phq6SHiSyZgyhzDrlqrtZQqySbjMI1h2yyYG+0N4BurpIsa5xOXrjSi+FMyRdrvXxM
sL5VuOYRi7LU6PGS7h5AsFRWZ/DLXK699loROa9OTMKuE7wiKhxPkUdUx5PP0yJvLlgUgrlBLBQK
ieiQQ3j1xvgnbyoWqM8TE5IkFbpyB02JZOTKWstvw2HtZQ3mfd0NsjshqHQnDPQMoPlgI2x1MnLl
RYOOLO5O4CpJeY6rrkt0Jwy6lcBLdCcUTXcnTI1PiEoNO1drKMkLX2UTbKS7ox6Z8x51eOD87BAC
1U3ynFGKK/anw5eaRVotz+HAiOe8t9x4E+6imYXWOISDX5t87grpmq+66koUFhZqkjcXLCrBDF7T
/OCDD7BWWT5cLH02NdM8OcMIe4cP+lKrsF61SpIbt7kE1tEqa5PZglu5SjJXL6xU6G7FzO6EoNUr
uhO8zUrOeHgMptRy2ESVpDzHU8pVkvkYo6kQHxMy2WEj3eV1Xt6epCDKvS9LTIs4ecHu2l9QJnVX
WWhg78bZqpdeegmb4uJP+FsTKnjqyVNQnhIlJSUiNTVVk7i5YtEJVuF0OvHCCy8gOmpx9JlTk22V
pMGHOtGQ3YV+p3TVskpSj44S7k6QAZGxtIt0V/bv8jG9Bs+M7oRx0tbOg1WwFDSJcxi2ggYKrI51
JwQ6uUoyD8F26QFEd8LeHPTkh1VJ5nKVZPp0leRgawfp7n4Md8jAa3BwEFu2bBHkxkRFzUm2vh8n
G/QYvLqkRdp8sGQEq+B8K4f36k2fjD7z/Jm1lgnhAWQ49B40HmqdrpJ0t7tFAbvPLF0oLw2y7lrU
7gQKosx55KpTqzChJC96W8wzuhO4I7B7XwE8JQ3yHLJO0Z3AVZLDiu42cZXkIQyblYyX14eez1IQ
KK4QWs37eIXom9/8Ji4h9/zUHBbw99C48Bix9XJMo0XYfLHkBKtgfb74Ylkrzbg1JmHWHpy56LMk
WIfBviH4HX5ZJVmrFMlNTKIjuxkd3J2gJDx4IZ9dc8ihBF6efnTtK0FPtayRmiKttqWTqz5UMp28
8FXrlO4EJfByeeEUVZINYvsIHdebkgUfV0mOy3N4SpNAD+eTTzyBHQlJs2areMGGF25Yd7du3Tpd
nXGyOGUEM5ZCn9lyOWPFNVZMLqcjRZUkWZG1yii7E3qlO5dVkhXoaZJTF9GdcKgC1izSXU54sO6W
c5VkPka9UkMHLRHdCWTB7gPZ6EmXVZJs4f4i+kzSXW4/4WN6enqwZs0avP7664gl16xVFxWJu2KV
nAId//HHH2uStRCcUoJVLLY+czqyz9KHgNMvXDAPcqgnSLpbK7ryeZurJDtTq9HNVZIiecHdCc2i
O2E8oCw1Gh2iOyGglx7gWHdChah45HN6SYO5O2G6SrK9S3QnDOll4MXywfd1//33Y+Padfj1HLJV
79K0iceB8dxzz2kStVCcFoJVtLa24o47ZGM4g8ttZ6skZJxIn3mAVfi6ZGDFC/pCdwtaiOCq6YUG
n94Cw95j3QnjokqyEO6iehGocSTN5a/2/bnT3QlBXafoThhSGscmfP4Z3Qm8jxfZOdf83bvvxk30
MM62gF+/bptYSGDXzJUdWiSdDE4rwSq09LnuJPWZI2ieFnG+mdd6uYB9wK4s1qvdCZX66eSFLbMS
1tSS6WoN2Z2QNd2dMNbjE22f/eVysV6rOyG83YRfRTHbL6My+ddFxwtyV69ejfT0dE2STgb6y284
7rqnnGCG3+8X+szaxZ+p6vNshd9fps9cqWEtbYOluHU6qBLZq7QqdGdyd4JMePRU6ck1F2DEo6Q8
le6EfiKZt6eIQHeK0p2gLDRMdyf0KEV+SrvJs88+i8S42dtNGK8lymYxjprfeustTYIWCq6yvOHG
G+ZWsnMqCFbB+vzTn/5UuF7+7NWLkN8OB0fNbL1DLknmgNmlVEkqi/WDwxHdCWqVJOluv0x4DBvM
M7oTGDPbTWbPVlWs2ypaS5jcH/7wh5okLQTsBe69917hFXgcGnZcddy1TyvBKrT0mRudI+8tErPp
M7d78pzXQ1YbNDph3l8El1IlyVod2Z3Aq0XcnTDYqWS8KLji7gS/0p3A++bbbsI1z/x9mAROYbJE
aZE1H+Tm5opS2di4WKyMWin0PCcn58zQ4C8Df/mdO+mmFKIXQ599OrNYUGDLdeQeew1Sf2OH7E6w
yIzXWC9XSWagr0StkjwMb9rM7oT5tpswuL2TLZf1evfu3ZqEzRWcp2bNX7t2rfhMfmBSUlKm/37G
E8xYCn1mUjktqUa/nNSw7smBr/pYlaQ7NT+iO6FO6q5bJjwY8203yaUHgH8ml78HpzDDyZovuKpy
xw75xl6O3N9///3jjjkrCFYRqc/8Q9FsqXPR57tPUB+mgjsD2XqDrV3kqqfQW1wjdHe6StJokd0J
TTLwYnC7CUe/jz788JzaTfjtOPzic3bN/FBEkjFXqAEUj0FiYqJmZ4OKs4pgFTqdDrfffru4NmM+
+nzRl+izt7hOrBbZ92SS7nJ3gtIWGhyAZ/ch9OWWTOvufNtNGA/yL44TuevXrxOeRIuQLwMHUPze
Dr73mNgY8f9ax4XjrCRYhbY+f/nc88v0mS13sMuKQEObyDczkVx+05s5sztBbTd55JFH5txucpAe
Lr5HJnguBevh4Lz0U089JQMoIpeLBziA0jo2Emc1wQzW57fffhuraKD5XhZDn8MRau0k13wQo1ZZ
rcHgB2I+7SbcgLf6HCkr7DW0iNCCGkBxHZcaQHHnoNaxJ8JZT7CKpdJnttpBIlndnm+7CeP2mERh
ufMpWGcr37lTBlDryKVzClTruNnwlSFYBevzbbfJX1BjfIP0+US/HB6O2fSZMTo6KqJWXiiZS7sJ
g7su2fq44G7//v2aJIQjPICKT4gXmTGt4+aKrxzBKhZbn5lgtd3kykvn1m7Cr4uKVX6PYbaC9fAA
iqtUuX76ZGqxVHxlCWao+sway/fJ75HiF5PMS59pGrJt2za89tprgvBnn3kGW8nSZ2vbYV3mn59l
6+VCd63BZ6gBFFs4k3v11VcvKMI+Eb7SBKvgKQ3rM+sg3+/6c6OEpc4WHHFF6LcTkxEdFTVdOMft
JnN569yv6AHh623evFkz4lUDqPXr14uHgGu3TjarpYWvBcEq+KVrC9FnriHbniC9wEtEnNYx4eC3
zvH7qtgitQrW+VUWOy+W8sGJEvYykccsFr5WBKtgveO6J5Vo1mdeeI/83uHgLNV+Inq2qJx1fOtK
Wanyk5/8ZMZgc1fgzbfcLP7GAdTTTz894+9Lga8lwYyF6vNseDJhtXDNV155rGA9PICKio4SVaZz
nS6dLL62BKtYqD5roZC0md+dwflhLljnSJ5rrHhbDaAyMjI0iVgqfO0JVtHc3Ixbb71VfB/GZXPU
ZxX81jkue+VzuZqSc9QbNsgA6vzzz8enn36qScBSY5ngCITrM79o7O7YxFn1mXFPbJIgk2vLduy4
SJzPARTPo7UG/lRhmWANqPrMrpW/32z6vDf5POGa+VgGz5cjA6zThWWCvwT8GxLh+rxBQ5+5aD9B
yVZx0fqpDKDmgmWC54C6ujrccIPMDzNYn/nXYnhs+NdkeB9notLS0jQH+XRimeB5gGudeJmQvzPr
M/c/8/8vdrvJYmKZ4HmCV5jC32bAeOKJJzQH90zAMsELAC/p8fe+++67RQ3yYqz6LBWWCV4AVIK5
FllrUM8kLBO8AHz1CL7v9mWCw3A2EWyOJHjTxVjh3nrFX8N3uq68Fv3/r2KZYAVnC8HFBYWwXXT1
DILt26/CCtfmS/rDdzJ6PnnuOJKXCT5zCWZy9Y/9ZAaHDONNdxHBG3a8HfkHhuuKa4S7VmG982ax
guK65V54brvva4POq74lvrfpuluFCzwTEWm5Kpp/9jJWWDZvTqSNP0f+cRlnN2wXX4finBz2PitW
2DfuuNO5aed/ah24jLMPjvMuRfUHHwn3LQjmf4LkZUs+68GWW/X+sdowhV75j901a7LrvEv73Vsu
/zetD1jGmQfbtithvPFOobnsllVyi4qK8P8ByGqjKKSs0RoAAAAASUVORK5CYII=",
								extent={{-200,-200},{200,200}})}),
		experiment(
			StopTime=1,
			StartTime=0,
			Interval=0.002,
			__esi_MaxInterval="0.002"));
end SolarThermal_GC;
