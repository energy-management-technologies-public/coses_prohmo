﻿// CP: 65001
// SimulationX Version: 4.2.3.69223
within CoSES_ProHMo.Generator.DigitalTwins;
model Photovoltaic_GC "Green City Photovoltaic Modules, slightly adapted"
	GreenCity.Interfaces.Electrical.DC DC "DC connection to PV-converter" annotation(Placement(
		transformation(extent={{330,-25},{350,-5}}),
		iconTransformation(extent={{190,-10},{210,10}})));
	GreenCity.Interfaces.Environment.EnvironmentConditions EnvironmentConditions "Ambient Conditions Connection" annotation(Placement(
		transformation(extent={{80,-35},{100,-15}}),
		iconTransformation(extent={{-210,-10},{-190,10}})));
	output Modelica.Blocks.Interfaces.RealOutput MPP(
		quantity="Electricity.Voltage",
		displayUnit="V") "Control voltage for MPP-Tracker" annotation(
		Placement(
			transformation(
				origin={205,-135},
				extent={{-10,-10},{10,10}},
				rotation=-90),
			iconTransformation(
				origin={0,-200},
				extent={{-10,-10},{10,10}},
				rotation=-90)),
		Dialog(
			group="Voltage",
			tab="Results 1",
			visible=false));
	protected
		Real VMPP(
			quantity="Electricity.Voltage",
			displayUnit="V") "Actual max-power-point-voltage" annotation(Dialog(
			group="Voltage",
			tab="Results 1"));
		Real Vth(
			quantity="Electricity.Voltage",
			displayUnit="V") "Threashold-voltage (pv-diode-model)" annotation(Dialog(
			group="Voltage",
			tab="Results 1"));
		Real Voc(
			quantity="Electricity.Voltage",
			displayUnit="V") "Actual open-cicuit-voltage" annotation(Dialog(
			group="Voltage",
			tab="Results 1"));
	public
		Real VPV(
			quantity="Electricity.Voltage",
			displayUnit="V") "Actual photovoltaic-DC-voltage" annotation(Dialog(
			group="Voltage",
			tab="Results 1",
			visible=false));
	protected
		Real VTemp(
			quantity="Electricity.Voltage",
			displayUnit="V") "Temperature specific voltage increase" annotation(Dialog(
			group="Voltage",
			tab="Results 1"));
	public
		Real TModule(
			quantity="Thermics.Temp",
			displayUnit="°C") "Module temperature" annotation(Dialog(
			group="Temperature",
			tab="Results 1",
			visible=false));
	protected
		Real PVVector[3] "Normal vector of photovoltaics system" annotation(Dialog(
			group="Alignment",
			tab="Results 1"));
		Real RadiationAngle(
			quantity="Geometry.Angle",
			displayUnit="°") "Angle of incidence into photovoltaics system plane" annotation(Dialog(
			group="Alignment",
			tab="Results 1"));
	public
		Integer nSer "Number of connected modules in series" annotation(Dialog(
			group="Alignment",
			tab="Results 1",
			visible=false));
		Integer nPar "Number of connected modules in parallel" annotation(Dialog(
			group="Alignment",
			tab="Results 1",
			visible=false));
	protected
		Real RadiationEffective(
			quantity="Thermics.HeatFlowSurf",
			displayUnit="W/m²") "Effective solar radiation into photovoltaics system plane" annotation(Dialog(
			group="Power",
			tab="Results 1"));
		Real PelMax(
			quantity="Basics.Power",
			displayUnit="W") "Maximum effective electrical energy" annotation(Dialog(
			group="Power",
			tab="Results 1"));
	public
		Real PPV(
			quantity="Basics.Power",
			displayUnit="W") "Actual photovoltaic-DC-power" annotation(Dialog(
			group="Power",
			tab="Results 1",
			visible=false));
	protected
		Real IMPP(
			quantity="Electricity.Current",
			displayUnit="A") "Actual max-power-point-current" annotation(Dialog(
			group="Current",
			tab="Results 1"));
		Real Isc(
			quantity="Electricity.Current",
			displayUnit="A") "Actual short-circuit-current" annotation(Dialog(
			group="Current",
			tab="Results 1"));
	public
		Real IPV(
			quantity="Electricity.Current",
			displayUnit="A") "Actual photovoltaic-DC-current" annotation(Dialog(
			group="Current",
			tab="Results 1",
			visible=false));
	protected
		Real ITemp(
			quantity="Electricity.Current",
			displayUnit="A") "Temperature specific current increase" annotation(Dialog(
			group="Current",
			tab="Results 1"));
		Real Ron(
			quantity="Electricity.Resistance",
			displayUnit="Ohm") "Forward state-on differential resistance (closed diode resistance) (pv-diode-model)" annotation(Dialog(
			group="Resistance and Conductance",
			tab="Results 1"));
		Real Goff(
			quantity="Electricity.Conductance",
			displayUnit="1/Ohm") "Backward state-off conductance (opened diode conductance) (pv-diode-model)" annotation(Dialog(
			group="Resistance and Conductance",
			tab="Results 1"));
	public
		Real EPV(
			quantity="Basics.Energy",
			displayUnit="kWh") "Photovoltaics energy output" annotation(Dialog(
			group="Energy",
			tab="Results 1",
			visible=false));
		parameter Boolean UsePV=true "Use PV System" annotation(Dialog(tab="Photovoltaics Dimensions"));
		GreenCity.Interfaces.Electrical.DefineCurrentDC DCConnection "Specifies current, extracts voltage of a DC connector" annotation(Placement(transformation(extent={{285,-25},{305,-5}})));
		parameter Real alphaModule(
			quantity="Geometry.Angle",
			displayUnit="°")=0.6108652381980153 "Inclination angle of photovoltaics system" annotation(Dialog(
			group="System Alignment",
			tab="Photovoltaics Dimensions"));
		parameter Real betaModule(
			quantity="Geometry.Angle",
			displayUnit="°")=3.1415926535897931 "Orientation angle of photovoltaics system" annotation(Dialog(
			group="System Alignment",
			tab="Photovoltaics Dimensions"));
		parameter Boolean useStandardParameters=true "If true, installed peak power is used for model parameterization, else detailed module number has to be parameterized" annotation(Dialog(
			group="System Configuration",
			tab="Photovoltaics Dimensions"));
		parameter Real PPeak(
			quantity="Basics.Power",
			displayUnit="kW")=10000 if useStandardParameters "Installed peak power" annotation(Dialog(
			group="System Configuration",
			tab="Photovoltaics Dimensions"));
		parameter Integer nSeries=1 if not useStandardParameters "Number of photovoltaics modules in series" annotation(Dialog(
			group="System Configuration",
			tab="Photovoltaics Dimensions"));
		parameter Integer nParallel=1 if not useStandardParameters "Number of photovoltaics modules in parallel" annotation(Dialog(
			group="System Configuration",
			tab="Photovoltaics Dimensions"));
	protected
		constant Real PMin(
			quantity="Thermics.HeatFlowSurf",
			displayUnit="W/m²")=0 "Constant minimum solar radiation into photovoltaics system plane" annotation(Dialog(
			group="Electrical Efficiency",
			tab="Power Behavior"));
		constant Real PMax(
			quantity="Thermics.HeatFlowSurf",
			displayUnit="W/m²")=1353 "Constant maximum solar radiation into photovoltaics system plane" annotation(Dialog(
			group="Electrical Efficiency",
			tab="Power Behavior"));
	public
		parameter Real PNominal1000(
			quantity="Basics.Power",
			displayUnit="W")=165 "Nominal pv power output at 1000 W/m2 vertical irradiation" annotation(Dialog(
			group="Nominal Power",
			tab="Power Behavior"));
		parameter Real VocNominal1000(
			quantity="Electricity.Voltage",
			displayUnit="V")=43.6 "Nominal open-circuit voltage at 1000 W/m2 vertical irradiation" annotation(Dialog(
			group="Nominal Voltage Characteristics",
			tab="Power Behavior"));
		parameter Real VMPPNominal1000(
			quantity="Electricity.Voltage",
			displayUnit="V")=35.1 "Nominal maximum-power-point voltage at 1000 W/m2 vertical irradiation" annotation(Dialog(
			group="Nominal Voltage Characteristics",
			tab="Power Behavior"));
		parameter Real VMaxSystem(
			quantity="Electricity.Voltage",
			displayUnit="V")=1000 "Maximum System Voltage" annotation(Dialog(
			group="Nominal Voltage Characteristics",
			tab="Power Behavior"));
	protected
		parameter Real MaxVoltageDev(
			quantity="Basics.RelMagnitude",
			displayUnit="%")=0.05 "Maximum voltage deviation between 0 and 1000 W/m2 vertical irridation" annotation(Dialog(
			group="Nominal Voltage Characteristics",
			tab="Power Behavior"));
	public
		parameter Real IMPPNominal1000(
			quantity="Electricity.Current",
			displayUnit="A")=4.70 "Nominal maximum-power-point current at 1000 W/m2 vertical irradiation" annotation(Dialog(
			group="Nominal Current Characteristics",
			tab="Power Behavior"));
		parameter Real IscNominal1000(
			quantity="Electricity.Current",
			displayUnit="A")=5.27 "Nominal short-circuit current at 1000 W/m2 vertical irradiation" annotation(Dialog(
			group="Nominal Current Characteristics",
			tab="Power Behavior"));
		parameter Real TNoct(
			quantity="Thermics.Temp",
			displayUnit="°C")=320.25 "Nominal operation cell temperature" annotation(Dialog(
			group="Nominal Cell Operation Conditions",
			tab="Temperature Behvior"));
		parameter Real PNoct(
			quantity="Basics.Power",
			displayUnit="W")=800 "Nominal radiation into module plane" annotation(Dialog(
			group="Nominal Cell Operation Conditions",
			tab="Temperature Behvior"));
		parameter Real TNominal(
			quantity="Thermics.Temp",
			displayUnit="°C")=293.14999999999998 "Nominal ambient temperature" annotation(Dialog(
			group="Standard Conditions",
			tab="Temperature Behvior"));
		parameter Real Tstc(
			quantity="Thermics.Temp",
			displayUnit="°C")=298.14999999999998 "Standard test temperature - reference temperature for temperature coefficients" annotation(Dialog(
			group="Standard Conditions",
			tab="Temperature Behvior"));
		parameter Real PTemperatureRelative(
			quantity="Basics.RelMagnitude",
			displayUnit="%")=-0.0047999999999999996 "Percental power output reduction due to module temperature increasing per K" annotation(Dialog(
			group="Temperature",
			tab="Correction Factors"));
		parameter Boolean TempFactorPercent=false "Temperature factors in percent per K" annotation(Dialog(
			group="Temperature",
			tab="Correction Factors"));
		parameter Real VTemperature(
			quantity="Electricity.Voltage",
			displayUnit="mV")=-0.153 if not TempFactorPercent "Absolute output voltage reduction due to module temperature increasing per K" annotation(Dialog(
			group="Temperature",
			tab="Correction Factors"));
		parameter Real ITemperature(
			quantity="Electricity.Current",
			displayUnit="mA")=0.00242 if not TempFactorPercent "Absolute output current increase due to module temperature increasing per K" annotation(Dialog(
			group="Temperature",
			tab="Correction Factors"));
		parameter Real VTemperaturePercent(
			quantity="Basics.RelMagnitude",
			displayUnit="%")=-0.0043600000000000002 if TempFactorPercent "Percental output voltage reduction due to module temperature increasing per K" annotation(Dialog(
			group="Temperature",
			tab="Correction Factors"));
		parameter Real ITemperaturePercent(
			quantity="Basics.RelMagnitude",
			displayUnit="%")=0.00051499999999999994 if TempFactorPercent "Percental output current increase due to module temperature increasing per K" annotation(Dialog(
			group="Temperature",
			tab="Correction Factors"));
		parameter Real PowerDegradation(
			quantity="Basics.RelMagnitude",
			displayUnit="%")=0.0 "Actual constant power degradation due to module aging" annotation(Dialog(
			group="Aging",
			tab="Correction Factors"));
	protected
		function PVConfig "PVConfig"
			input Real PPeak;
			input Real PNomMod;
			input Real VMax;
			input Real VMaxMod;
			output Integer nSeries;
			output Integer nParallel;
			protected
				Integer nModules;
				Integer nMaxSeries;
				Integer value;
				Integer rest;
			algorithm
				nModules:=integer(PPeak/PNomMod);
				nMaxSeries:=integer(VMax/VMaxMod);
				if nModules>nMaxSeries then
					rest:=1;
					value:=nMaxSeries;
					while rest>0 loop
						rest:=mod(nModules,value);
						if rest>0 then
							value:=value-1;
						end if;
					end while;
					nSeries:=value;
					nParallel:=integer(nModules/value);
				else
					nSeries:=nModules;
					nParallel:=1;
				end if;
		end PVConfig;
		function angleMinMax "angleMinMax"
			input Real angle;
			input Real angleMax;
			input Real angleMin;
			output Real angleOut;
			algorithm
				angleOut:=min(angleMax,(max(angleMin,angle)));
			annotation(__esi_Impure=false);
		end angleMinMax;
	public
		parameter Real epsDirt=0.0 "Dirt correction factor (0: fully irradiated, 1: fully shaded)" annotation(Dialog(
			group="Outer Influences",
			tab="Correction Factors"));
	protected
		function normalVector
			input Real alpha_inc;
			input Real alpha_or;
			output Real vector[3];
			algorithm
				vector:={sin(alpha_inc)*cos(alpha_or),sin(alpha_inc)*sin(alpha_or),cos(alpha_inc)};
			annotation(__esi_Impure=false);
		end normalVector;
	public
		parameter Real epsShading=0.0 "Shading correction factor (0: fully irradiated, 1: fully shaded)" annotation(Dialog(
			group="Outer Influences",
			tab="Correction Factors"));
	protected
		function diffAngle
			input Real vector1[3];
			input Real vector2[3];
			output Real angle;
			protected
				Real value;
				Real minMax;
			algorithm
				value:=(vector1[1]*vector2[1]+vector1[2]*vector2[2]+vector1[3]*vector2[3])/(sqrt(vector1[1]*vector1[1]+vector1[2]*vector1[2]+vector1[3]*vector1[3])*sqrt(vector2[1]*vector2[1]+vector2[2]*vector2[2]+vector2[3]*vector2[3]));
				minMax:=max(min(value,1),-1);
				angle:=acos(minMax);
			annotation(__esi_Impure=false);
		end diffAngle;
	public
		parameter Real epsCover=0.0 "Coverage correction factor (0: fully irradiated, 1: fully shaded)" annotation(Dialog(
			group="Outer Influences",
			tab="Correction Factors"));
	initial equation
		assert(alphaModule>=0 and alphaModule<=pi,"alphaModule must be within 0° and 180°");
		assert(betaModule>=0 and betaModule<=2*pi,"betaModule must be within 0° and 360°");
		EPV=0;
	equation
		if UsePV then
			if useStandardParameters then
				(nSer, nPar) = PVConfig(PPeak,PNominal1000,VMaxSystem,VocNominal1000);
			else
				nSer=max(1,nSeries);
				nPar=max(1,nParallel);
			end if;
			
			assert(VPV<=VMaxSystem,"Maximum System Voltage exceeds Limits");
			PVVector=normalVector(alphaModule,betaModule);
			RadiationAngle=angleMinMax(diffAngle(EnvironmentConditions.RadiationVector,PVVector),pi/2,0);
			RadiationEffective=((EnvironmentConditions.RadiationDirect+0.25*EnvironmentConditions.RadiationDiffuse)*cos(RadiationAngle)+0.75*EnvironmentConditions.RadiationDiffuse*0.5*(cos(alphaModule)+1))*(1-epsDirt)*(1-epsShading)*(1-epsCover);
			TModule=EnvironmentConditions.TAmbient+((RadiationEffective*(TNoct-TNominal))/PNoct);
					
			if TempFactorPercent then
				ITemp=ITemperaturePercent*IMPPNominal1000;
				VTemp=VTemperaturePercent*VMPPNominal1000;
			else
				ITemp=ITemperature;
				VTemp=VTemperature;
			end if; 
			
			IMPP=max(0,PNominal1000*min((max(RadiationEffective*(1+PowerDegradation+PTemperatureRelative*(TModule-Tstc)),PMin)),PMax)/1000/max(VMPP,1e-10)+ITemp*(TModule-Tstc));
			VMPP=max(0,VMPPNominal1000*(1-MaxVoltageDev*(1-min((max(RadiationEffective*(1+PowerDegradation+PTemperatureRelative*(TModule-Tstc)),PMin)),PMax)/1000))+VTemp*(TModule-Tstc));
			Voc=max(0,VocNominal1000*(1-MaxVoltageDev*(1-min((max(RadiationEffective*(1+PowerDegradation+PTemperatureRelative*(TModule-Tstc)),PMin)),PMax)/1000))+VTemp*(TModule-Tstc));
			Isc=max(0,IscNominal1000*min((max(RadiationEffective*(1+PowerDegradation+PTemperatureRelative*(TModule-Tstc)),PMin)),PMax)/1000+ITemp*(TModule-Tstc));
			
			Vth=VMPP*nSer;
			Ron=((Voc-VMPP)*nSer)/(max(IMPP,1e-10)*nPar);
			Goff=((Isc-IMPP)*nPar)/(max(VMPP,1e-10)*nSer);
			VPV=min(max(DCConnection.V,1e-10),Voc*nSer);
			if (DCConnection.V>Vth) then
				IPV=min(max((IMPP*nPar-(VPV-VMPP*nSer)/Ron),0),Isc*nPar);
			else
				IPV=min(max((IMPP*nPar+(VMPP*nSer-VPV)*Goff),0),Isc*nPar);
			end if;
			DCConnection.I=-IPV;
			MPP=VMPP*nSer;
			PPV=VPV*IPV;
			der(EPV)=PPV;
		else
			nSer=0;
			nPar=0;
			PVVector=0;
			RadiationAngle=0;
			RadiationEffective=0;
			TModule=0;
			ITemp=0;
			VTemp=0;
			IMPP=0;
			VMPP=0;
			Voc=0;
			Isc=0;
			Vth=0;
			Ron=0;
			Goff=0;
			VPV=0;
			IPV=0;
			DCConnection.I=0;
			MPP=0;
			PPV=0;
			der(EPV)=0;
		end if;
	equation
		connect(DCConnection.FlowCurrent,DC) annotation(Line(
			points={{305,-15},{310,-15},{335,-15},{340,-15}},
			color={247,148,29}));
	annotation(
		Icon(
			coordinateSystem(extent={{-200,-200},{200,200}}),
			graphics={
							Bitmap(
								imageSource="iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAADUQAAA1EBvbkFJwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAClwSURB
VHhe7X1ndFVXliYG5SyhAIgkkk0wwTbJGIMxOAFV4OzGOKdyKNvgMkFkJCGhnLNAOQckJAQIhIEq
p+rVM13dq6urw6yq7nLXrK6a6VldM9W9+see/e2nI+497wBPDySBEWt9C+nqvhvOd3be57xR1n+9
EXMDzkXGfHZh3MyvL0yc/X/PR0yjEdz6YK7+2Bs146ve8Jjt4LCPTvu/cxExD/PJ/6J/eAS3Hb7r
iYhZ2Uer49/58KmrzkdO+0/DySO4HcFcQmCFXIg0HxyR3O8dpv/2dFSU/yjYXNMJl+6ZRt+sjhkQ
vmZURE+mxrvvo95V60cwVLhnsRN/AGzyKDhU+h/+9vOp9KffTaL/+P3EAaM0N4wmT4umn/7230Yw
VPj1H+iL7fttHAK942Z+OQoemPUgJNddcoHvfjWBPDxGU3XvV+aHGcHggEnuvfsBG8HgdpT1AABV
ayJuIJh/bxDtycg3P8gIBg1Q1zY+I6eTM8GP3DjBTzwWQu/t3Eet3/41fZ6UcV3sOJpJp//m18aH
HoHr6F29wcYlMCgEv7YlkJ578z3KbuygxStXXxdLVq2hmi++MT70CFzHkBG8+YfB9PonnxsfYgSD
hyEjeOmSYPpJYprxIUYweBgSgv/4u4kUEupNGXVtxocYweBhSAjubo0g/wAfuvA//qfxIUYweBgS
gl/bGkRrf7DR+AAjGFwMOsG/+HoceXiOpqKOs8YHuBnY8qOPycfXj1Y9tZHO/f2/GM+5UzGoBP/p
XyfS42sDB1V6S7p66a677qJRo0YJPL286N2de43n3okYVIJjPw+miKgwavvzvzHe/EZx+Z//N4WM
DRdiZ3p4072ePv1Eh0VEslPXavzcnYRBIzglPoR8fDwHVTWvf2GLkOnFElwWOlGeNT54HI0b49FP
9Iw586jlm18YP38n4KYT/IffRNNrW4IoKNif0mtaKL+1m0pZjZpufiMo6/6iXzX/OCDc9rynwmPo
bf8w8r1rtPx99OjR9MiGTXekF+8Swb1LpxjJtOL/fBdNuWmhFBXlSzNmz6C6Sz+XGyC54RcQQPlt
3U43dxdQzaHhEULe/V6+dE57XoWGsZNpnU8A3dUnzd6+vrQtPsV4ze8rXCK4IGwizZsbSDu3BVFV
yVg6dzKCLp+NpJaacMpJDaVnfhhEgYFeFDkunHalZNOl3/zBdpMP9h4mHz8/ymk6aTvuLp549kUh
zJ8ltI5J1J9XR15oNM3x9JbPABHjJ4hmMV37+waXCD674nGWxHRa8ehqmjZzMgUG+3HoM4bGTQin
BQ/cS8+/+a4UES7++vfGmwA3i+TC9jP9qjk2KNL2nNcCpHxnYASFjh4jn4VUL1y2gk791T8a7/N9
gWsqmk+yfgglv6av/tJ2zBXcKMmYQEGhYULQQ97+tmd0FZ3hU+lVv1Dy7JskY8aMoWffeNdJ63xf
4BbBz7/1Hm16+XXbMVdxIySv2/yskDKWpbBt7BTKCYmmz1kqe7TndQUVYZNoFU8SXA/wDwyiQ/nH
jPe9neEWwZjxP9jyqu3YQOAOyXktXaxW7xLVeoRDoRMsieGeXuTt4UFTvH0pPWSC7ZldRUrweIrx
8OonOnpqDFWd/9L4DLcjhoVgAN0d8K6Vt30t9P7j7ygoJFQI2OgbJM+0hv+fNSGakpKS6IknniAP
luol7DFXh13f6dJxJiKGtgWGU7Cyz6y+Vz6+ns7+7T8bn+d2wrARDCSUVFGXC04O7oeBHz/Gk06y
5B4MiiIfltw9u2OprLyayirqKTc3lz39ueTJMe+LfiFia/X3uB6gFZ72DabRfdKMtOc7n++RsMz0
XLcDhpVgV5BZf0IkCqo5k9VwI9veIA9Pev4HP6TcvAL6PK5QcCC5mFpPnKTY2FiKGDuWQjw9xWu+
Wox8LRxj+7yY42uQDASzY5dS0WB8vlsdtzTBp3/5G4qKniiD/DJLJcha6htAc6dMpYKCAtoZl9tP
MFBcXk/Hq5uosamFtr68lXy9vWkG2+dcdsb0d3IFetpz1rwF1PLtXxmf9VbFLU3wY5ufk4GdwU4Q
7OSnAeEUyKTFxcXRwaQcG7n7jhZTUmap/LyDkVNcSTW1dbTyoYdoNGuANb6B1MzSr7/b9WBKe+K5
bpe05y1LMOwzBhTxaknoRHGe/Fiatr74EmXnXiFWISWrxOlYQ/MJamSkJKfQlMmTyZc//xaThcmi
v+P1ANNgTXsiCvg07qjx2W8l3JIEt//FL8XuYSDfCxgrqnm+jz89MGcOdXZ20t6kK7YXOMrkxh6x
kxuXWkwJGWXy86HUEmpoaqVtn26j4MBAivLyllBLf09XkC9pzytlyUj25Is6eozvcSvgliMYHuuD
jz4ug4f6LpIYbzPJoSwxGenpVHy8lkor6igx85iQd4RJjE8rtpG7O6GQkjWJ3pVQQOd7L1BTcwtt
3ryZPNkLn+/tJw6V/r7Xg0p7hlnSnguWPkgn//vfGd9pOOEWwVhjVN7zU9uxmwVUnzBosHlVPPio
83rxQL795luUnJHXT1hGQSXVN7VRS9tJG5EA1DXssPVYcnYZS3Ip7YwvpPzSaiopKaX7Fi2iMXyf
TX7BEn7p7309dLF9RtoT9Wg8M9Kez7/9o1sq7ekWwYOFxp/9N/LzD5DBQgoStnI6e8EPPfAA5VlC
IgXY2MraFiasRqQWx+Bo7WeHy3refg6hEtLtEp2WV0GXL/+UDhw4SFERkRTEYRWSHe6EVZVa2jMg
KIgO5JYa33GoccsQfOmf/hctXPagDNCDXn5y3z/zD6WIwCDKzMikXVpIlMBqOS7N4TUjBi4pr6Pq
+jY6VtVkO29HXAGlZDs7YFmF5eJ5x/M1Gjisevutt8jPx5em8oTKdjPtmRoynqZZ0p6Tps0Y9uU3
twzB7+7YK4MSwuoY4UwOD7IHhySffPwxxSXbQ6I9iUUsqXbS4GT1nDvPJLf0O1cAbPGuPulWSEgr
oYMpdimvaWil9vYOWrduHY3h+65gj9mVWrOOs4a05/I164Yt7XlLEHz8zCXyZBWJATkQFCW2LdrL
hx59eBU1NjXbiABSDRKZwjYWqhi2N6uwip2xOkl6ZORX2s6LNUwOoOhYDR1i0uHEZWfn0KyZM8X2
b2Ut0s3Po4/J9aDSnmP6pBlpzze27RRNZRqDwcKwE4yEwYzZc2UQnvQJlPttZKcnOjyCKioq6HhV
IxWW1QoxIAJE7u37WQHEICyyHktnYi98cVGyW3Cs1HHT5DjKhMceuXJNTJRzrA1+8pOfUEhwMIV7
etN+nnj6uLiC42yfl7DJwfsBaDVKrx26bs9hJ/ildz+UF0dKEJ5scvB48mTJ2bVrFx1IdKjmvUlF
Iln1zR1UVmm3sSBPD4mArIJymRSIgUv4s5ks1eUs0bDX1vMwOeJZZVuPQd1X8LlwxBoam+mlF18i
L9Ywc338JOmij48rQNoTxRJF9My591KzG00TA8WwEoy+qNFMJuLINHZQ2pngCJaWDU8+RamZV0Ii
YG9iIZ0+08NecxMlZR3vP56a7Qh9rOceSS928qThgF26fLnfMQPwOVMGDE6ZCrPwf15xFTU0NNDy
5cvFPj/hF0StbqQ9ERV8wDG9nyXtidZflENN43MzMGwE9/zqtxQ9JUZe9HlWybgParxTJ0RTbm4+
D649LErNKRPnCj/DrpZW1LNj1EapuRW28+AZJ2bYSQNJhWXVPEkKKKe4WlT+oRSHZOuTI8EwOZBU
qaprYXNRT0lJR2nixImS9gRZ7nSTIO25wSeoP+3p7eNDH+2PN47TjWLYCFZN61PGeIkTgxovujMO
HzpMu+PtXjPs62HNxmYUVNHZnvNCtIqBQaRJIpOzSm2eNFR+z/leajnR2S+p6niSYXJAS6jfD7KK
b25tpzfffJOC/fxpIjuDGW6GVUh7zrWkPcdPmkIlneeN4+UuhoXgpGO18kIeHELgJTGjAz086cXn
X2CpqrYNsCntCGQVHJe/wSGCfc4trmE13Er7mCTreabJASLLKhtE1ZeU19LR7OO0h+1ubpFdGwC4
t5pACoWl1ZR89Cgd+nwnPfnkk6K2F/r4U40b3SRIrMCBixztKEtCqu97cCV1/uXfG8duoBhygrt+
8Q8U1te0jsqOqvHOjImRkKi8upHVbnn/YKaxarZKGQApA0nWY7CxvRe+sMXAV5scupQjrPrZz35G
+aW1tuOYGPrkiE0ooPz8fErdFUsV5RWiQfLzC2nhwoUStz/D5sadbhI97enB2mzL+x9fsxXZFQw5
wY+s/6G8ACoysF+o8fp7e9NRlojdCbC9TEBOOR2rrKem1pOUzD9bBxhesJ52hB3NL6ni//P7YuBa
OsJ2s6yizqXJAQmGd17AtrmgzJH2hC1Py3UUNKzIL2CvO3YPZWVmUlyyI7uG83HPw4fjKCIiQrTR
Dje7SZBcQVkSYwSg2/NgnvtpzyEleHdarjy0D3uRaFtFjdd3zBh6/fXXnQr4WYXVdKq7RyREOVdX
9Xr5mNVZQkx77vwFaus4ZbO9jpy0XSL1a8LBqmaHCjE0QijrualZRZSYcIRS4hIoM9sxGRVwnY7O
bqpraKa33nqLfNlxmsr2Gasq9PF0BYgqrGnPmFmzqfbi9RsUdQwZwWiU9w8IlIf9tC+pfy/brYX3
zqeMLHtIBGTlH5dB25tUTEVsswuO1VF90wm2lXbpk7SjFtuCpEL+THx6mXjKiGcPJBexnbZntQCo
cOvkAIqOo/WnWcqSKEfi2OGUIsrJzqFUlt7CAj7vsJ3g5KwykXr8nMQ/V1ZV05o1a6SbZKVvILW4
EVZhjFCWtHd7PkVnfvlPxjE2YUgIRnpu0fKH5CEf6FsshhpvkJ8fpaWl86DYB+soe73WzBJQ03CC
zpzpEXWqjsVybGxKO+rZqoz8Cvrmm2+cbGw8q3pdSqG+kdnCz1DvSJAgv11XV0+JbHdhf/cm2LWN
I1livw5Cqx728tPSMyhm6lTy5rDqVf9Qt7pJkPZ8yS+EPEY57LMXmzTk7l1Jew4JwR/ui5MHC2Qn
pJ5tDGq8yFZ9+umnlJhuJ9KUdoSNy2EPF5WhdCYLUpnC8S/iUut5gKO7w35N2PRC1gC5JTUi2Uh1
Ig422ViTCaipa6S4gwcp9WgyVVTX2f4G6Tc5chl5x+Q5ALT17ty5k4KCgijM04sS3OwmKWeztlRL
e2ZcJ+056ASjOQCBPB5oH4cDmMEx3r60Yvlyams/yc5Nfb9qc3i9V2JOBQy61VnCeec4joWNxc/q
uCnt6LjmlWOw5winLqEWzGrbei60gcp5K8SnMlkZGZSybz81NDSy6m6kbJZq9Tzw8nUVL8kSzWyU
VjRSd3c3vfDCC9JNcg+PgTvdJADSuVMsac/ZC++n5q/Ni9wHleAvfv2vdM+CRfIQa70D5Fqo8Yby
TM7KyhGJxAxHZqmkvIGaWjpshAFH2OvVM0sgMrekWmwvvFeo0fj0UkkpWs8DrGlHhWK2sShCoIac
3KfyEV7p3jnafArY3qbs3C2qOTbB4SvAth/jOLqRnxfOoPUzV0uW5LH9F2nPOU7FpWW0ZMkSSVU+
4RvkVjeJKe258aWtTmnPQSX4lR9vl5uHcxAPO4IaL5ICn332mVMBv5Zt7En2QuEQqWP72MEypR31
Aj7iZrGxPFGsx48wYbok4XfrNZHqROtPW0eX7Twgj0Oiw3v3Si9YfF9IpABf4NTpHonbVQEDz2bN
einoyRIQ3dl1mpKTk2ncuHHkz2EVdilwJ6xSaU+1GkO6PQ9f6fYcNIKxjnc0h0DIzEClIJAf7+VN
69auMxbwM9lrxs+QKGSXIJXHqxps58nfebD0An5aXjllF1WLNEGiIfGQstQcu401TQ6gtrGNKmpb
HGXJPvudmlVMR5OSKPnQYcrOsTuBAGwsngNAyxDqzw0s0bpHj0SJ7lPAOURkgIlRzn7EBx98QL6+
vjSBx8fdbhK923P85ClUf/nPB4fgnr/7jibFTJcbbfZ1FBI2+AXTOOx8k2knF9AdG8zw83021lr7
NaUd9QI+JKWGbexXX33FRNvtqS5JALzfuFSH1MEXwASBjS4pKaHU3bFUVFhEOw7bwzhTsgSJkjM9
53iSVfabBIf9N/sU1t8T2LzAPq9fv1403APe/m51k0ADoJdNFTHCx42n8/p+0YwbJnjT1jfkBpPY
EcDKAEgwVv8dZS90pxYSyWBpjk1cagllFlSyZBeK14sKECQ7j+2u9TzAVMCHNOVxSARvG143juHz
TpLE0qpCIgVIZFvbCUroIzc990rqE4CKh+q3HsOEzCmEl+8Ij0pZA6XnV1FFTVM/2QowD8qpVID9
h0+QUVBBubl5NPue2ZL2fIFDo4F2k7zGPg7GXqns5jlLnM65IYLhtiMgH8PAmiDUeBEaPPPMM9Ta
1iHEqZeGOjOlHfWwI4UdE9hYJDusx02SpMeksJVw3k6wNrCeB5gmR1Yefz4ujlKPJFILEw2v+XCf
hOPZUq5iY/E39Tve7+SpM2KjrWTCdBy5TiYNEwwTYx977aEhoRTCY7fHxa0qCllNq1gZa7nwf+FY
Z0/dbYKxQ3vk+AlyYQT1+Owq30CaPGkSZWQ5JBeZIcxwSBgcFPViCnhZ62ABkEI4X2jDgQpFDAuJ
TM6221jT5ABqG1ql2xJ2D5kxHJPWH31yJBdQVmYWpe7ZR0VF6O/Kl2vmsgZBLNva3ulUrUJYhkll
PYbrQtugQoXcNn6Gl4+atPU8QJ8cACpjxRxVVLB9fvXVV6WbZCaHVaXX6CaBpE/tS22iSocdhtAq
BNusn+s2wY/+4Gm5AXadgxuPGi9ivkPsqFgL+JjhiGObWjtsIRBaYXUnBRJg9XoxGCj0f/311042
NoXtnT5YUKcHeULgZzhzIBnOEFKR1vN2xhdQIavkZGSr8jgk0mrSx6qbqJOlModDMaWB4CzpKh7Q
NQNMDjQQmgDtx4udJ4fmU0B9V1ZW08MPPyyh0GqfQGM3CRr8MPawv9gJ4XE+D/n+gptF8P7sIrmB
2nUO7rs/Sl5btjgV8KGWkV2CE4IKDrxQFPAhKdbzAN0hAWBjM/qqRqgeYcDTOFQySZIeZqHf6lT3
WWn9wTOo47n57GyxWkxPSaWkVHtIhOdEbhw/x6WViZefg/pzXXM/2QqmZAly03klyL6Vy2fRNnSY
Sc/It1fKAJPZqKxFGZWfNyWNJk+eLGlPrHhU3STYhgLEYgJMYW25adMmMZOQXmQOdT4HTDD2olRb
LHzEwTe8uUU+/nT33XdTQoqdXIdnaX8JqFvMcCT5rcf1bkdA96Qxw9tYdbafdM3GOlSz4/OIn1FS
rKlvobTUVEref5Dy8u3nOz7jnCxBHIvqkdIOgPgUWiYNNtXqSUPDwFzAy1frqxTwvqpypoBJq/wU
jF3J8Rr66KOPpM34Kd8g6mAfRzULQJUfOnRIdjuICg+nuV6+dFbjEhgQwVgstmzNOrnBfX2FBFSL
fLy9JVtlfVgAEqkPFmZyEr9scrZjhoM09F0d5ZlvPc80OQAQBJslMXCfikeeWZckUwEfJFdWVkoB
v66uju2m3cs3JUsQvyIc2sVqPb+kRrQQJil6q63nAabJkcefgQ8Cc4EIAaSjfp2YYXfg9MkB4Fqo
O4++azRlhIyXDCHGHti6dSubGY48GO++69j2Agkmnc8BEfzp4SS5kNp1DjVe7zFjRDV3dJ6yzXCo
Sz3tqBfw8QLozoCNPaDbWMNgwZO25rIL2alpbe9im2m3sY7J4ewBFxWXSgE/Ly9PcuNYmKbsOK5r
SjvqyRJMGmigipoW2/PhvZwyaZpPAXXd2NIulTKkbq3nmt73CGvEkJAQ2sg29xD7OBh7qOYF8+f3
k6swa8YMKglzdsxcJrj24rfk4+vYy2J3nyuPTIw/24iZE6Lpow8/oqamFiko5LINwmy1PqwMlkEi
MbPRyYHMEuwyBjyLY0SnwZLJYT+G9tqTrD4R3lhXNZhafxwF/AQp4OfkOSYTJiQ0QeGxerGx1vMB
aBBMFtsxll6sdkzKQgzsWNqamAktZLexV3vfCraxJRWNErcjQsAx1JdVClRhX1KBtO6O8/IWLxnL
fEBuoL+/pD11gvfs2eO+F41eoTmLHhBy9V3nZNmGfwi766NpyZx5rFIO09keR3eGdZAhUVBD1pfQ
1w0hBj3JmkC3sTJYmiQBaK/dzY4UfkZTnWr90dtr9ycVUk5OXwGfB0Mv4GOSICyy9lGbVLyeLMFz
oXIEiQbJ1nNNkwM+Bbxs/IyJjJAKXj4iBet5kO733/9QMl1ovl9mKRt++OGHTuQqVE6628YN4BLB
b27fKRfHLLpatwLKYUs5DkY77NqVD0tFprmlTZIPiPUS2dZaX0JPOwIYMBTcJTPF9hkrFXAcuWt9
ciDBoXvSiJ272GtG9UfFvY6QiFUl2104JKYCPrxyDDi0DjQKtAGyatbzAJNE5jNJiAokQmB7i+eE
x647YPDo9TAL9zzR0SXJDhUh4PiR5DTyY235BnvP8HEw9ugWeWT1aidSrei4d6kTL9ck+PJ3v6eS
s13S9YebuFK8xrINJNLH+gfQs5ufptraOvEidTJMXq940n3OEl4eg4d4VF/CgnP0wQIy+lp/8Heo
fBT+rQX85HR7SIRzdUcOzwmJrKxrlb+r47ClyiNX0D3pg8kl1Nx2kjXYuX6yFEyTA90syqeAs4kE
Cyb43LlzJdlRhT421oxQzVFRUZSdnW0kVqFr4YNOfDgRfH7NWur59y46/f9qqP3fCmjKHEe2aqOP
Y9c5V6Dql7DPs6InyrLQlpZWsXe7EwpYoqv6JUwBA6sX8OGktLEThb5mNcMBPWcMmGJSxL9ZWVlS
wK+udvZ6Qa6VRCAlBx7+cVHX0k3C0iiZtTy7jcXnjDaWpTG/rI41UJ3E0TiGSpc1jQk4Urf2Y8iA
vf322+TFjmst2935fRUjEIz1WyZSrXCJ4HNrV1D3f5QKNn24Vm4wYYynW/2/2EQU9hlbJ9w/Zy4l
sJPTw7Nbt7EmSQKsa5FQEoQzdeJkt1N7LQZL75NCAb+4uEQK+FVVVdTE3qtqqgMgebpWwaTTPWkU
BiDRcKqsx02TA560ys5hImYXVVHriU4nG3u1yXE4PlGygTsDIyW5gbEHnn76aSOhOgZE8Kk/lZBv
gGNj7clMMFo79XNdBRLjC3z8Hfb54VVUXFRMTc1tQpoMRGGF02CZYlLUjVFIgI1VXqfYPENIlJev
CvgZlJzhGHB8HloE/VqwtfpnTGYDZgKdGYhnkWuO5fuhsU93wEyTA8+GxAwmpooQcNzU+pOUXkhT
pkyhpT4BVMyOFXLMkNyZM2fKRnAmQnUMWIITuz+jybMdKhrA3hTuLNlQkGWVbJ8jg4LopRdeoPr6
Bjpz9pyEDdaXNaUdQRAK7/gf3ilSnvDUYfOQJ7aem5zJNjopiVIOxTkV8CG18Amq6lttjptJxYNE
a9kRahYTDOuk9DjWNDkQOahsFTaFwcSE14zUp/U8JFw2btwoTfRNrPXQI430o4+Pj2g9E5kmDJhg
oPOPxfTj3FcoMNSx8QhmFpLd7vQWAf39RbDPEyfR9m3bpCYLwjCrMegZhg5IU9hRVd8mCRbEperY
3iNs43PzpICPl96hFfBTWRrh0ECVQprxWXSIpOZeadEF8BymZAmqYll8PqpkKv0I505PO+qTA0AG
C7lx2GlVlpQJG7tXpDWRBQCrL5VAvfHGG04kXguuOVlM8Ln/KnNC/leOfTWwfRD+Hzvaw+2dagDp
L+KXEfs8d55IXNuJDglzrGuCgavFpKr1RxXeMfBoQkcBH3GvWlSuAJWvF/CRE4eNParZWFNmCVoF
vWPqd0wOTDBk46znOSaHs0Sn88TF3zCR0UhYwva9oqqWwsPDaT2PRVbIBBo9yqGaFy9ebCTxWnCJ
4AvrHqIvqdwJDb86KsTCYXnwQccOOcDdHu73FgGwz9jZzs/Tix5/dC0dKzvGDtEJUWkYFKhNkyTp
KhEqs7PrVH8Bv7jUkFm6io2FRKsYeF+SoyasnCUFUwEfGqW5rYNNRYNoIPxuNSXWcxMzim2TA0Cl
bDXHtlgI3zZ2KkWzrwNyg0OCKS0tzUjitXBTCL506RL9/Oc/lweIjo6WYyhfwT6701ukILu9in0O
pi0vvUSNTU2ybghOitVWAo6YVFOJKQWUB9W8Zx91dHTIIm7rlocmFa8X8KFmOzpPi421OkEyOQwS
iYmnrgmn71hVg/SW6e21ek4a2MkTctu27VJIQIoRe5bIWLIJxCIBE4HXw00lGPjyyy9p+/bt5O/v
sM9YaIZlke7sVAMo+4xFajMnTZKNUU51d4tEKYkwqVlrAb8gv0C6NeAQwcZCMpHwQCuQ9TOmHi0A
3Z2Ie5FJUzVk9GDpk8NUwIdpaecwDl4zIgQcw0QxttemZlJgQIB0w8Tx5FbkPvbYY07EuYqbTrDC
qVOnJFZDOg3nRLB9xkIq/dqu4op9vovuY/uclpomPVNYbwQHRR+snGsU8EEubKxOsEkiHZ70ld+R
0+4+c5a1QavtPEk7Gm2sI/yBuUBiBgkaVLv0QsKRtEKaP38+TfP2lX3C8PU/UM0TJkwQ38FEnisY
NIIVsBUSHhznAYs8fd3eqQZACwpWJfp7sX1eu06u39nVbRuwRI4fU1NSKGX/Iduu8ApYAQF7igZA
SDS6K5Bn1luAUPTQ88doqWlobpdJgtIk/AEQqJw7KzA5dE8a4R9SrdayJBaVYwktvo7Aui0inNd9
+/YZiXMVg04w8O2331JiYqJ08cuDs1eIbnx3llQqiH1mRyQymO3zn22h5pYW9prrJeFQXFwsBXws
89wZZw+J9HVDULPtnd10hm2sVeVi8E0SDadMEYM8NOJYEIbigvU8TCD0mFmPwW9QrT9qeycUUXJy
csmbJ+x2jkDQ16yE4fnnnzeSNhAMCcEKOO+dd94hL34ZfA6rDWFfsf2ffk9XILuxK/s8ebLkZo8f
r6A4DolQJUrKuDK4gCmzBDuOhD5spcTA+VhDxM5Z31oi67mmAj5WOTa1dkpZUqUuMVFSc5xtLCaM
7km3tLZTTEwM3c9aCf1TATwmUM2zZ892OVt1LQwpwQrY1Pupp57qn6lIeyKg1+/rKlD8hn1Gs/iy
BQsp9sefUGtrm1MxwJxZsnvSIKm394JUjqznXW1ypLGNVaRhcsCZQi1Zb6/F5NDt7v6kAnr22Wel
ORHZqoVsvuBU+fv7ydcFmQgbKIaFYAXMUORVFdHLvfxkj2j9/q4CocW8fvvM8fOx41Tf1CLZIqQw
9RUUpmQJ4lIsfVE9UyAWE8BkY01hFsqYCItgn9XfZHJo3jmcLmxl7MHaB+bmR6yJ1Di89957RrLc
wbASDMCbPXjwoHTx41o3mvZEFg3bEmHb/qiQEHrt1Vepra1N1g1Z88pXSztCypVE4nx4vbDPaZb2
H8CUdgShyEzhZ0grVD6aAdF4bz0PSM/Mo8iICHrML0gaI9BuDOldsWKFkSh3cXL+MqcxGlKCFc6f
Py9d/J4ejoXN+H7CG0l7Wu3zrMlTZIV9W1s75RRVcUhTQAXsxer20FTAB7G1jSekvVZt7wTPGO23
1vMA2Fj9GLozkJhBZkwdi0sppEfXrKGxnl50InwKzfLwFrs7duxYyszMNBI1UKB7Bk4avhpYH5th
IVihublZZjGuC+DlkY/Vn8lVwHF5zDdI7POShYsoPT2dLvLzYqd4KxGOtKOdIDhZVmcJSY5KjrlP
nT5ry4gBek4agCcNPwATCTGwtP5waLabnUD0VuHLNdUaIhC8Y8cOI1kDBbJeiFigEY5PmOE0JsNK
sALaWOFd4vo3I+2J7Yzmin32pg1PPEmVlVVsa5tFlUIVq/DFCthY3ZPGkheU90CWSoDgGk5pR4RZ
WrYKcXZHx0nZohg7HWBBPFYBgogNGzYYyRoIEIouXbpUxgwE7927d/ht8LWAGq017el9g2lPZZ8j
xT6HSumthb1t7JCnd12aGvjgpCnVDMcJJMOpqmu0V44A0+RIZ21w//33y7elYkUCumIguViOgglt
Is0VoC8LEwSJESwkf/nll/v/dksTrIDF0ZL25MHA/W407SnbBrIEefH1ZrJ9xvcdtp1olyQJiIC6
NtlYU5gF+4pFdNbtnUyTYw/bfXjHMBVYu4VvUcW7YAkKlptYCRsIsDsAGuFBLr6OQE9r3hYEK2CJ
CfaExD0BxI1oZdGf11XAPq/ri5+XL14syRHUn9FRooc/prQjPGk04+FnLHGF14wGf5T8rOcBySlp
5IdtgwPCZUG8WoVvlbaBAE3t06c7dlGYNWvWVbs8biuCAZX2HD9+vNwbA4W9HW8k7YmF6nP67PP6
J5+UTc+q65r6ux7RaeHcwOdQw9ZjaKaHWcEXfFgnSFpWocT7qHFjUQA2pYE2WrBggZGUawFOInbQ
g90OCwujjz/+2Hiewm1HsMLly5cl7ent7WgCRIoPXYcoL+rP7wpgn6H2Qzl0iQoNlf0mYZ+r2Ns2
2dgUJlcPs9Bei3AIKh7SjBowmgexNxY2EIeTuNo7QMgJCDAvN7kakBRCQcLPz0/e+bnnnpNQyHSu
FbctwQpdXV2S9sSg4VmwL4i730sIOOxzmFR2Zk+bJkmYrq5T/TEwgHDKKe3IpOqeNLouc9l5Qtvr
3qBIimXgGYFrLTfRAUcTGgvviLadlJQU43km3PYEKxQVFYktUgN4vxd2knPfPkPaHrPYZ1Sm8AWX
6InWbSwkGRJtPQYUFJVSNBODr7aVBfF8LZC0+pFrLzdRQD5ahT0IGRH2mM67Fr43BAMq7RnGKhbP
pdKeCEn0d3IV6P3G1+sFsFrcsH49NTY2yXJPq8NlykmjgI9ODKj8Nr7/Yi8/sbuuLDeBJ4yoAVW3
wMBAUc3uVpa+VwQr9Pb22tKeQaPHSFnSnS/QAJR9DuHrRYeHS2jS2tYmTXUFZVdSmApYirNr127J
VmGCYBc7PAcIvt5yE1w7lCcowh44U1hmYzrPVXwvCVZoaWmhhx5ybGMMYHOYzBtIe2KpjrLPd0+b
TvHx8fTFxYv98bNCRlYuhXFs+px/iFTHvPv8A3ytrYkEALHwnDlz5Lx58+bJtU3nDRTfa4IVkCWa
xg6TIhplydobWI0hbTV9+e3lS5bIrni19c0cSpVSYUkFLVu2TL6aDxNilqejkICY1eT1Iux5/PHH
RWIjIyNl/w39nBvBHUEwoNKeAQGOPS2Q9sQm2+4soFOQbxtl+xzk60tPb9os2w2///4HQjwSMNh1
Dk4Vwho9EaHCHqRhkV6EzUWixXrOzcAdQ7DCuXPn6MUXX3RKe7pblkS70SdsY4M4FJoYEUkBTNY7
rMbRvK86SvXlJrDDU6dOFfIh7QOJhweKO45gBazGWLTIsZc1sMDTh4puIO2JDNWzbHNX+QVJMQTx
OCYR7qEGG98sg7AHxILgm1UevBbuWIIVsBpjgpb2RF+yPgYDAUIzkIst/HF9+AAovkNVBwcHS/7Z
3bBnoLjjCQaQ9oRzgz0w8D4q7XnajbIk7DImCqQUhfdPPvlEYl8V9tysjg1XMUKwBXracyKrWTTE
6eNxNSChEsYxNz6PQgLCHVwHLbAHDhwwEjDYGCHYgPLy8n5yAKQ9UcPVx0XHGh//fucNJENyb3bY
M1CMEHwV9Kc9wxz7Ylwv7al2nQNga7E/9mCEPQPFCMHXwcWLFx2rMTyvnvZELRr7hcHOwjuGt2wa
7OHACMEuAmnPtWsdOwwBSHuq7w1GZgzHrpatGk6MEDxAwAvG9gp4b3jL6ptOsDnKzcof30yMEOwG
9u/fL++NFCP+B1555RXjAA83Rgh2A6j84L2PHDkinYw3o6d5sDBCsBtQBN/IyvuhgmsEr10xQrAF
txPBp3SCI6fTqAsTZ//RerB3znz68r+OjRDch9uF4KL8Ajo7dZ6N4J4pc2lUb9SML60HgYv7X3Yi
eYTgW5dgkNuy6QUbh0DnAw8zweEx2/U/AL2z7xV1rXBm5VLZFKV32Rr64sG1dwza5y6W9+5asFxU
4K0IXXIVGl55i0adjory51++0/84gtsbZ6cvoKLsbGifUaN6ImJWno+c9p+mE0dw++HcuJlUsdvR
Vy0E45+QPCLJtz0gueW7HF8+YiMY/6CuYZN7x8388kL0Pf9uusAIbj2cnTyHOu9fKTYXalmRW1hY
SP8ffqYOIbBTlTAAAAAASUVORK5CYII=",
								extent={{-200,-200},{200,200}})}),
		experiment(
			StopTime=1,
			StartTime=0,
			Interval=0.002,
			__esi_MaxInterval="0.002"));
end Photovoltaic_GC;
